package com.proclapp;

import android.app.Application;
import android.content.Context;
import android.graphics.Typeface;
import androidx.multidex.MultiDex;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.ads.MobileAds;
import com.proclapp.fcm.FCMProcessClass;
import com.proclapp.socket.SocketEmitType;
import com.proclapp.socket.SocketIOConnectionHelper;
import com.proclapp.utils.NetworkConnectivity;
import com.proclapp.utils.Preferences;
import com.proclapp.utils.SnackBarView;
import com.proclapp.utils.ToastView;

import io.fabric.sdk.android.Fabric;


/*facebook
 * account
 * id : prismetric@yahoo.com
 * password : p12345678
 *
 *
 * FCM :
 *  id : pris.and.dev.1@gmail.com
 *  password : prismetric1
 * */

public class AppClass extends Application {

    public static Typeface lato_regular,
            lato_light,
            lato_bold,
            lato_semibold,
            lato_medium,
            lato_heavy,
            lato_black;

    public static SnackBarView snackBarView;
    public static ToastView toastView;
    public static Preferences preferences;
    public static FCMProcessClass fcmProcessClass;
    private static NetworkConnectivity networkConnectivity;
    private SocketIOConnectionHelper socketIOConnectionHelper;

    public static boolean isInternetConnectionAvailable() {
        boolean flag = false;
        if (networkConnectivity.isNetworkAvailable()) {
            flag = true;
        } else {
            flag = false;
        }
        return flag;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());

        lato_regular = Typeface.createFromAsset(getAssets(), "Lato-Regular.ttf");
        lato_light = Typeface.createFromAsset(getAssets(), "Lato-Light.ttf");
        lato_bold = Typeface.createFromAsset(getAssets(), "Lato-Bold.ttf");
        lato_semibold = Typeface.createFromAsset(getAssets(), "Lato-Semibold.ttf");
        lato_medium = Typeface.createFromAsset(getAssets(), "Lato-Medium.ttf");
        lato_heavy = Typeface.createFromAsset(getAssets(), "Lato-Heavy.ttf");
        lato_black = Typeface.createFromAsset(getAssets(), "Lato-Black.ttf");

        snackBarView = new SnackBarView();
        toastView = new ToastView(this);
        preferences = new Preferences(getApplicationContext());
        networkConnectivity = new NetworkConnectivity(getApplicationContext());
        MobileAds.initialize(this, getString(R.string.admob_app_id));
        fcmProcessClass = new FCMProcessClass(getApplicationContext());
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public void createSocketConnection() {
        if (socketIOConnectionHelper == null)
            socketIOConnectionHelper = new SocketIOConnectionHelper(this);
        else {
            socketIOConnectionHelper.recheckConnection();
        }
    }

    public void socketNull() {
        socketIOConnectionHelper = null;
    }

    public void setAppListerner() {
        if (socketIOConnectionHelper != null)
            socketIOConnectionHelper.setAppListerner();
    }

    public void removeAppListerner() {
        if (socketIOConnectionHelper != null)
            socketIOConnectionHelper.removeAppListerner();
    }

    public void removeNewMessageListerner() {
        if (socketIOConnectionHelper != null)
            socketIOConnectionHelper.removeNewMessageListerner();
    }

    public void setEmitData(SocketEmitType type, Object object, SocketIOConnectionHelper.OnSocketAckListerner onSocketAckListerner) {
        this.setEmitData(type, object, onSocketAckListerner, false);
    }

    public void setEmitData(SocketEmitType type, Object object, SocketIOConnectionHelper.OnSocketAckListerner onSocketAckListerner, boolean isProgress) {
        if (socketIOConnectionHelper != null)
            socketIOConnectionHelper.setEmitData(type, object, onSocketAckListerner, isProgress);
    }

    public void setOnSocketResponseListerner(SocketIOConnectionHelper.OnSocketResponseListerner onRideResponseListerner) {
        if (socketIOConnectionHelper != null)
            socketIOConnectionHelper.setOnRideResponseListerner(onRideResponseListerner);
    }

    public void removeSocketConnection() {
        if (socketIOConnectionHelper != null)
            socketIOConnectionHelper.disconnectAllConnection();
        socketIOConnectionHelper = null;
    }

}
