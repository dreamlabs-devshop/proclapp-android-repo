package com.proclapp;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;

import com.proclapp.home.HomeActivity;
import com.proclapp.more.settings.ChangePasswordActivity;
import com.proclapp.profile.EditProfileActivity;
import com.proclapp.signup.StartupActivity;
import com.proclapp.utils.ScreenCode;
import com.proclapp.utils.Utils;

import java.security.MessageDigest;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        Utils.setStatusBarColor(getWindow(), this, R.color.color_02263A);

        getHashKey();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if (AppClass.preferences.getToken().isEmpty()) {

                    startActivity(new Intent(SplashActivity.this, StartupActivity.class));
                    finish();

                } else {

                    if (!AppClass.preferences.getScreeCode().isEmpty()) {

                        if (AppClass.preferences.getScreeCode().equalsIgnoreCase(ScreenCode.NormalScreen)) {
                            startActivity(new Intent(SplashActivity.this, HomeActivity.class));
                        } else if (AppClass.preferences.getScreeCode().equalsIgnoreCase(ScreenCode.ChangeProfile)) {
                            startActivity(new Intent(SplashActivity.this, EditProfileActivity.class));
                        } else if (AppClass.preferences.getScreeCode().equalsIgnoreCase(ScreenCode.UpdatePassword)) {
                            startActivity(new Intent(SplashActivity.this, ChangePasswordActivity.class));
                        }

                    } else {
                        startActivity(new Intent(SplashActivity.this, HomeActivity.class));
                    }
                    finish();
                }
            }
        }, 3000);
    }

    private void getHashKey() {

        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    getApplicationContext().getPackageName(),
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

   /* Test Card - Success
        Card Number: 4084 0840 8408 4081
        Expiry Date: any date in the future
        CVV: 408

        Test Card - Fail
        Card Number: 4084 0800 0000 5408
        Expiry Date: any date in the future
        CVV: 001*/
