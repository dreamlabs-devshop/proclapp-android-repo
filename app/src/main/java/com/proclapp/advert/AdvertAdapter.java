package com.proclapp.advert;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.interfaces.RecyclerClickListener;
import com.proclapp.model.AllPostsModel;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.AlertDialogInterface;
import com.proclapp.utils.DateTimeUtils;
import com.proclapp.utils.DialogUtils;
import com.proclapp.utils.ImageLoadUtils;
import com.proclapp.utils.StringUtils;
import com.proclapp.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;

public class AdvertAdapter extends RecyclerView.Adapter<AdvertAdapter.AdvertHolder> {

    private Context context;
    private ArrayList<AllPostsModel> advertList;
    private RecyclerClickListener clickListener;

    AdvertAdapter(Context context, ArrayList<AllPostsModel> advertList, RecyclerClickListener clickListener) {
        this.context = context;
        this.advertList = advertList;
        this.clickListener = clickListener;
    }

    @NonNull
    @Override
    public AdvertHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_advert, viewGroup, false);
        return new AdvertHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final AdvertHolder holder, int i) {

        AllPostsModel advertData = advertList.get(i);

        if (AppClass.preferences.getUserId().equalsIgnoreCase(advertList.get(i).getAuthorId())) {
            holder.img_option.setVisibility(View.VISIBLE);
        } else {
            holder.img_option.setVisibility(View.GONE);
        }
        holder.txt_artical_category.setText(context.getString(R.string.sponsored));

        holder.txt_user_name.setText(advertData.getAuthor());
        holder.txt_title.setText(advertData.getPostTitle());
        holder.txt_description.setText(Utils.fromHtml(advertData.getPostDescription()));
        holder.txt_date.setText(DateTimeUtils.changeDateTimeFormat("yyyy-MM-dd HH:mm:ss", "dd MMM", advertData.getPostDate()));
        holder.txt_user_place.setText(advertData.getProfessionalQualification());
        holder.txt_like_count.setText(advertData.getPostLikeCount());

        if (StringUtils.isNotEmpty(advertData.getPostCommentCount()))
            holder.txt_comments_count.setText(advertData.getPostCommentCount());
        else
            holder.txt_comments_count.setText("0");

        if (StringUtils.isNotEmpty(advertData.getIsPostLiked()) &&
                advertData.getIsPostLiked().equalsIgnoreCase("true")) {
            holder.txt_like_count.setSelected(true);
        } else {
            holder.txt_like_count.setSelected(false);
        }

        ImageLoadUtils.imageLoad(context,
                holder.img_user_profile,
                advertData.getProfileImage(),
                R.drawable.menu_user_ph);

        if (advertData.getImages().size() == 1) {

            ImageLoadUtils.imageLoad(context,
                    holder.iv_advert_single_image,
                    advertData.getImages().get(0).getImage_name(),
                    R.drawable.post_img_ph);

            holder.ll_multiple_image.setVisibility(View.GONE);
            holder.iv_advert_single_image.setVisibility(View.VISIBLE);

        } else if (advertData.getImages().size() == 2) {

            ImageLoadUtils.imageLoad(context,
                    holder.iv_advert_one,
                    advertData.getImages().get(0).getImage_name(),
                    R.drawable.post_img_ph);

            ImageLoadUtils.imageLoad(context,
                    holder.iv_advert_two,
                    advertData.getImages().get(1).getImage_name(),
                    R.drawable.post_img_ph);

            holder.ll_multiple_image.setVisibility(View.VISIBLE);
            holder.iv_advert_single_image.setVisibility(View.GONE);
            holder.fl_image.setVisibility(View.GONE);
            holder.iv_advert_three.setVisibility(View.GONE);

        } else if (advertData.getImages().size() == 3) {

            ImageLoadUtils.imageLoad(context,
                    holder.iv_advert_one,
                    advertData.getImages().get(0).getImage_name(),
                    R.drawable.post_img_ph);

            ImageLoadUtils.imageLoad(context,
                    holder.iv_advert_two,
                    advertData.getImages().get(1).getImage_name(),
                    R.drawable.post_img_ph);

            ImageLoadUtils.imageLoad(context,
                    holder.iv_advert_three,
                    advertData.getImages().get(2).getImage_name(),
                    R.drawable.post_img_ph);

            holder.ll_multiple_image.setVisibility(View.VISIBLE);
            holder.iv_advert_single_image.setVisibility(View.GONE);
            holder.img_article_shadow.setVisibility(View.GONE);
            holder.txt_image_count.setVisibility(View.GONE);

        } else if (advertData.getImages().size() > 3) {

            ImageLoadUtils.imageLoad(context,
                    holder.iv_advert_one,
                    advertData.getImages().get(0).getImage_name(),
                    R.drawable.post_img_ph);

            ImageLoadUtils.imageLoad(context,
                    holder.iv_advert_two,
                    advertData.getImages().get(1).getImage_name(),
                    R.drawable.post_img_ph);

            ImageLoadUtils.imageLoad(context,
                    holder.iv_advert_three,
                    advertData.getImages().get(2).getImage_name(),
                    R.drawable.post_img_ph);

            holder.ll_multiple_image.setVisibility(View.VISIBLE);
            holder.iv_advert_single_image.setVisibility(View.GONE);
            holder.img_article_shadow.setVisibility(View.VISIBLE);
            holder.txt_image_count.setVisibility(View.VISIBLE);
            holder.txt_image_count.setText("+" + (advertData.getImages().size() - 2));


        }
        Linkify.addLinks(holder.txt_description, Linkify.ALL);
        holder.img_user_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickListener.onItemClick(holder.getAdapterPosition(), "profile");
            }
        });

        holder.ll_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickListener.onItemClick(holder.getAdapterPosition(), "profile");
            }
        });

        holder.ll_option.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickListener.onItemClick(holder.getAdapterPosition(), "option");
            }
        });

        holder.txt_like_count.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.callAPILikeUnlike(holder.txt_like_count.isSelected() ? "0" : "1", holder.getAdapterPosition());
            }
        });


    }

    @Override
    public int getItemCount() {
        return advertList.size();
    }

    public class AdvertHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public ImageView
                img_user_profile,
                img_option,
                img_article,
                img_article_shadow;
        private TextView
                txt_artical_category,
                txt_user_name,
                txt_user_place,
                txt_date,
                txt_comments_count,
                txt_like_count,
                txt_title,
                txt_description,
                txt_image_count;
        private LinearLayout
                ll_option,
                ll_name,
                ll_multiple_image;

        private FrameLayout fl_image;


        private AppCompatImageView
                iv_advert_single_image,
                iv_advert_one,
                iv_advert_two,
                iv_advert_three;


        AdvertHolder(@NonNull View itemView) {
            super(itemView);
            initViews();
            setTypeFace();
            setClickListener();

        }

        @Override
        public void onClick(View view) {

        }

        private void initViews() {

            txt_artical_category = itemView.findViewById(R.id.txt_artical_category);
            txt_user_name = itemView.findViewById(R.id.txt_user_name);
            txt_user_place = itemView.findViewById(R.id.txt_user_place);
            txt_date = itemView.findViewById(R.id.txt_date);
            txt_comments_count = itemView.findViewById(R.id.txt_comments_count);
            txt_like_count = itemView.findViewById(R.id.txt_like_count);

            txt_image_count = itemView.findViewById(R.id.txt_image_count);
            txt_title = itemView.findViewById(R.id.txt_title);
            txt_description = itemView.findViewById(R.id.txt_description);
            ll_option = itemView.findViewById(R.id.ll_option);

            ll_name = itemView.findViewById(R.id.ll_name);
            ll_multiple_image = itemView.findViewById(R.id.ll_multiple_image);

            img_user_profile = itemView.findViewById(R.id.img_user_profile);
            img_option = itemView.findViewById(R.id.img_option);
            img_article = itemView.findViewById(R.id.img_article);
            img_article_shadow = itemView.findViewById(R.id.img_article_shadow);
            iv_advert_single_image = itemView.findViewById(R.id.iv_advert_single_image);
            iv_advert_one = itemView.findViewById(R.id.iv_advert_one);
            iv_advert_two = itemView.findViewById(R.id.iv_advert_two);
            iv_advert_three = itemView.findViewById(R.id.iv_advert_three);

            fl_image = itemView.findViewById(R.id.fl_image);


        }

        private void setTypeFace() {

            txt_artical_category.setTypeface(AppClass.lato_regular);
            txt_user_name.setTypeface(AppClass.lato_bold);
            txt_user_place.setTypeface(AppClass.lato_regular);
            txt_date.setTypeface(AppClass.lato_regular);
            txt_title.setTypeface(AppClass.lato_bold);
            txt_description.setTypeface(AppClass.lato_regular);
            txt_comments_count.setTypeface(AppClass.lato_regular);
            txt_like_count.setTypeface(AppClass.lato_regular);


        }

        private void setClickListener() {
            ll_option.setOnClickListener(this);
            txt_like_count.setOnClickListener(this);
            img_user_profile.setOnClickListener(this);
            ll_name.setOnClickListener(this);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    clickListener.onItemClick(getAdapterPosition(), "item_view");
                }
            });

        }


        private void callAPILikeUnlike(String status, int pos) {


            if (status.equalsIgnoreCase("0")) {
                advertList.get(getAdapterPosition()).setIsPostLiked("false");
                int count = Integer.parseInt(advertList.get(getAdapterPosition()).getPostLikeCount()) - 1;
                advertList.get(getAdapterPosition()).setPostLikeCount("" + count);
                notifyDataSetChanged();
            } else {
                advertList.get(getAdapterPosition()).setIsPostLiked("true");
                int count = Integer.parseInt(advertList.get(getAdapterPosition()).getPostLikeCount()) + 1;
                advertList.get(getAdapterPosition()).setPostLikeCount("" + count);
                notifyDataSetChanged();
            }

            HashMap<String, String> request = new HashMap<>();
            request.put("user_id", AppClass.preferences.getUserId());
            request.put("post_id", advertList.get(pos).getPostId());
            request.put("like_unlike", status);

            ApiCall.getInstance().likeUnlike((Activity) context, request, new IApiCallback() {
                @Override
                public void onSuccess(Object data) {

                }

                @Override
                public void onFailure(boolean isFalseFromService, String errorMessage) {
                    DialogUtils.openDialog((Activity) context, errorMessage, context.getString(R.string.ok), "", new AlertDialogInterface() {
                        @Override
                        public void onNegativeBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }

                        @Override
                        public void onPositiveBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }
                    });
                }
            }, false);

        }

    }
}
