package com.proclapp.advert;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.home.AnswerEditDeleteDialog;
import com.proclapp.home.question.AnswerDetailsActivity;
import com.proclapp.home.question.AnswerListAdapter;
import com.proclapp.home.question.PostAnswerActivity;
import com.proclapp.home.question.SortByForAnswerDialog;
import com.proclapp.interfaces.RecyclerClickListener;
import com.proclapp.model.AllPostsModel;
import com.proclapp.model.AnswerModel;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.AlertDialogInterface;
import com.proclapp.utils.DateTimeUtils;
import com.proclapp.utils.DialogUtils;
import com.proclapp.utils.ImageLoadUtils;
import com.proclapp.utils.Logger;
import com.proclapp.utils.StringUtils;
import com.proclapp.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;

public class AdvertDetailActivity extends AppCompatActivity implements View.OnClickListener {

    public static boolean isUpdated = false;
    public TextView
            txt_artical_category,
            txt_user_name,
            txt_user_place,
            txt_date,
            txt_comments_count,
            txt_like_count,
            txt_title,
            txt_description,
            txt_total_answer_count,
            txt_post,
            tv_url,
            txt_image_count;
    public ImageView
            img_user_profile,
            img_option,
            img_article,
            img_back,
            img_sort_answer,
            img_article_shadow;
    public LinearLayout
            ll_option,
            ll_name;
    private AllPostsModel advertData;
    private RecyclerView
            rv_all_images,
            rv_answer_list;
    private ArrayList<AnswerModel> answerList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advert_detail);
        Utils.setStatusBarColor(getWindow(), this, R.color.white);
        initViews();
        setTypeFace();
        setClickListener();

        setAdvertData();

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isUpdated)
            callAPIGetAnswerList();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.txt_like_count:
                callAPILikeUnlike(txt_like_count.isSelected() ? "0" : "1");
                break;

            case R.id.img_back:
                onBackPressed();
                break;

            case R.id.txt_post:
                startActivity(new Intent(this, PostAnswerActivity.class)
                        .putExtra("questionDetails", getIntent().getSerializableExtra("advertDetails")));
                break;


            case R.id.img_sort_answer:

                new SortByForAnswerDialog(this,
                        advertData.getPostId(),
                        new SortByForAnswerDialog.UpdateAnswerListBySorting() {
                            @Override
                            public void updateData(ArrayList<AnswerModel> answer_lists) {

                                if (answer_lists != null) {
                                    answerList.clear();
                                    answerList.addAll(answer_lists);

                                    if (answerList.size() > 1) {
                                        txt_total_answer_count.setText(String.format("%s %s", String.valueOf(answerList.size()), getString(R.string.discussions)));
                                    } else {
                                        txt_total_answer_count.setText(String.format("%s %s", String.valueOf(answerList.size()), getString(R.string.discussion)));
                                    }

                                    rv_answer_list.setVisibility(View.VISIBLE);
                                    rv_answer_list.getAdapter().notifyDataSetChanged();
                                }

                                if (answer_lists.size() == 0) {
                                    rv_answer_list.setVisibility(View.GONE);
                                    txt_total_answer_count.setText(String.format("0 %s", getString(R.string.discussion)));
                                    img_sort_answer.setVisibility(View.INVISIBLE);
                                }
                            }
                        }).show();

                break;

            case R.id.ll_option:

                if (AppClass.preferences.getUserId().equalsIgnoreCase(advertData.getAuthorId())) {

                    new AnswerEditDeleteDialog(this, advertData, tag1 -> {

                        if (tag1.equalsIgnoreCase("delete")) {
                            DialogUtils.openDialog(this, getString(R.string.are_you_sure_you_want_to_delete_this_advert), getString(R.string.delete), getString(R.string.cancel), new AlertDialogInterface() {
                                @Override
                                public void onNegativeBtnClicked(DialogInterface alrt) {
                                    alrt.dismiss();
                                }

                                @Override
                                public void onPositiveBtnClicked(DialogInterface alrt) {
                                    alrt.dismiss();
                                    finish();
                                }
                            });
                        } else {
                            startActivity(new Intent(this, PostAdvSummaryActivity.class)
                                    .putExtra("advertdata", advertData));
                        }

                    }).show();

                    break;

                }

        }

    }

    private void initViews() {

        txt_artical_category = findViewById(R.id.txt_artical_category);
        txt_user_name = findViewById(R.id.txt_user_name);
        txt_user_place = findViewById(R.id.txt_user_place);
        txt_date = findViewById(R.id.txt_date);
        txt_comments_count = findViewById(R.id.txt_comments_count);
        txt_like_count = findViewById(R.id.txt_like_count);
        txt_post = findViewById(R.id.txt_post);
        tv_url = findViewById(R.id.tv_url);

        txt_image_count = findViewById(R.id.txt_image_count);
        txt_title = findViewById(R.id.txt_title);
        txt_description = findViewById(R.id.txt_description);
        txt_total_answer_count = findViewById(R.id.txt_total_answer_count);
        ll_option = findViewById(R.id.ll_option);

        ll_name = findViewById(R.id.ll_name);

        img_user_profile = findViewById(R.id.img_user_profile);
        img_option = findViewById(R.id.img_option);
        img_article = findViewById(R.id.img_article);
        img_article_shadow = findViewById(R.id.img_article_shadow);
        img_back = findViewById(R.id.img_back);
        img_sort_answer = findViewById(R.id.img_sort_answer);

        rv_all_images = findViewById(R.id.rv_all_images);
        rv_answer_list = findViewById(R.id.rv_answer_list);


    }

    private void setTypeFace() {

        txt_artical_category.setTypeface(AppClass.lato_regular);
        txt_user_name.setTypeface(AppClass.lato_medium);
        txt_user_place.setTypeface(AppClass.lato_regular);
        txt_date.setTypeface(AppClass.lato_regular);
        txt_title.setTypeface(AppClass.lato_bold);
        txt_description.setTypeface(AppClass.lato_regular);
        tv_url.setTypeface(AppClass.lato_regular);
        txt_comments_count.setTypeface(AppClass.lato_regular);
        txt_like_count.setTypeface(AppClass.lato_regular);
        txt_total_answer_count.setTypeface(AppClass.lato_regular);
        txt_post.setTypeface(AppClass.lato_regular);


    }

    private void setClickListener() {
        ll_option.setOnClickListener(this);
        txt_like_count.setOnClickListener(this);
        img_user_profile.setOnClickListener(this);
        ll_name.setOnClickListener(this);
        img_back.setOnClickListener(this);
        txt_post.setOnClickListener(this);
        img_sort_answer.setOnClickListener(this);

    }

    private void setAdvertData() {
        try {

            advertData = (AllPostsModel) getIntent().getSerializableExtra("advertDetails");

            if (AppClass.preferences.getUserId().equalsIgnoreCase(advertData.getAuthorId())) {
                ll_option.setVisibility(View.VISIBLE);
            } else {
                ll_option.setVisibility(View.GONE);
            }

            txt_artical_category.setText(getString(R.string.sponsored));

            txt_user_name.setText(advertData.getAuthor());
            txt_title.setText(advertData.getPostTitle());
            txt_description.setText(Utils.fromHtml(advertData.getPostDescription()));
            txt_date.setText(DateTimeUtils.changeDateTimeFormat("yyyy-MM-dd HH:mm:ss", "dd MMM", advertData.getPostDate()));
            txt_user_place.setText(advertData.getProfessionalQualification());
            txt_like_count.setText(advertData.getPostLikeCount());

            if (StringUtils.isNotEmpty(advertData.getAdvert_url()))
                tv_url.setText(advertData.getAdvert_url());

            if (StringUtils.isNotEmpty(advertData.getPostCommentCount()))
                txt_comments_count.setText(advertData.getPostCommentCount());
            else
                txt_comments_count.setText("0");

            if (StringUtils.isNotEmpty(advertData.getIsPostLiked()) &&
                    advertData.getIsPostLiked().equalsIgnoreCase("true")) {
                txt_like_count.setSelected(true);
            } else {
                txt_like_count.setSelected(false);
            }

            rv_all_images.setLayoutManager(new GridLayoutManager(this, 3));
            rv_all_images.setAdapter(new AdvertImagesAdapter(this, advertData.getImages()));

            ImageLoadUtils.imageLoad(this,
                    img_user_profile,
                    advertData.getProfileImage(),
                    R.drawable.menu_user_ph);

            setUpDiscussionList();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setUpDiscussionList() {

        rv_answer_list.setLayoutManager(new LinearLayoutManager(this));
        rv_answer_list.setAdapter(new AnswerListAdapter(this, "advert", answerList, new RecyclerClickListener() {
            @Override
            public void onItemClick(int pos, String tag) {

                if (tag.equalsIgnoreCase("edit")) {
                    startActivity(new Intent(AdvertDetailActivity.this, PostAnswerActivity.class)
                            .putExtra("questionDetails", advertData)
                            .putExtra("openfor", "edit")
                            .putExtra("postAnswer", answerList.get(pos)));
                } else if (tag.equalsIgnoreCase("see_answer")) {
                    startActivity(new Intent(AdvertDetailActivity.this, AnswerDetailsActivity.class)
                            .putExtra("questionDetails", advertData)
                            .putExtra("answerLists", answerList.get(pos).getPostAnswersId()));
                } else if (tag.equalsIgnoreCase("delete")) {
                    if (answerList.size() > 1) {
                        txt_total_answer_count.setText(answerList.size() + " " + getString(R.string.answer) + "S");
                    } else {
                        txt_total_answer_count.setText(answerList.size() + " " + getString(R.string.answer));
                    }

                }

            }
        }));

        callAPIGetAnswerList();
    }

    private void callAPIGetAnswerList() {

        isUpdated = false;

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("post_id", advertData.getPostId());

        Logger.d("request:" + request);

        ApiCall.getInstance().getAnswerList(this, request, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {

                answerList.clear();
                answerList.addAll((ArrayList<AnswerModel>) data);

                if (answerList.size() > 1) {
                    txt_total_answer_count.setText(String.format("%s %s", String.valueOf(answerList.size()), getString(R.string.comment)));
                } else {
                    txt_total_answer_count.setText(String.format("%s %s", String.valueOf(answerList.size()), getString(R.string.comment)));
                }

                img_sort_answer.setVisibility(View.VISIBLE);
                rv_answer_list.setVisibility(View.VISIBLE);
                rv_answer_list.getAdapter().notifyDataSetChanged();


            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {

                rv_answer_list.setVisibility(View.GONE);
                txt_total_answer_count.setText(String.format("0 %s", getString(R.string.comment)));
                img_sort_answer.setVisibility(View.INVISIBLE);


            }


        }, true);

    }

    private void callAPILikeUnlike(String status) {

        int count;
        if (status.equalsIgnoreCase("0")) {
            count = Integer.parseInt(advertData.getPostLikeCount()) - 1;
            advertData.setPostLikeCount("" + count);
            txt_like_count.setSelected(false);

        } else {
            count = Integer.parseInt(advertData.getPostLikeCount()) + 1;
            advertData.setPostLikeCount("" + count);
            txt_like_count.setSelected(true);
        }
        txt_like_count.setText("" + count);
        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("post_id", advertData.getPostId());
        request.put("like_unlike", status);

        ApiCall.getInstance().likeUnlike(this, request, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {

            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {
                DialogUtils.openDialog(AdvertDetailActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                    @Override
                    public void onNegativeBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }

                    @Override
                    public void onPositiveBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }
                });
            }
        }, false);

    }

    private void callAPIDeletePost() {

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("post_id", advertData.getPostId());

        ApiCall.getInstance().deletePost(this,
                request, new IApiCallback() {
                    @Override
                    public void onSuccess(Object data) {
                        AdvertFragment.isEditedOrDeleted = true;
                        AppClass.snackBarView.snackBarShow(AdvertDetailActivity.this, (String) data);
                        onBackPressed();
                    }

                    @Override
                    public void onFailure(boolean isFalseFromService, String errorMessage) {


                        DialogUtils.openDialog(AdvertDetailActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                            @Override
                            public void onNegativeBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }

                            @Override
                            public void onPositiveBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }
                        });
                    }


                }, true);

    }


}
