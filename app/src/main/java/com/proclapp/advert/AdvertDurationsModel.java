package com.proclapp.advert;

import java.io.Serializable;

public class AdvertDurationsModel implements Serializable {

    private String
            advert_time_slot_id = "",
            no_of_days = "",
            time_in_min = "",
            amount = "";

    private boolean selcted = false;

    public String getAdvert_time_slot_id() {
        return advert_time_slot_id;
    }

    public String getNo_of_days() {
        return no_of_days;
    }

    public String getTime_in_min() {
        return time_in_min;
    }

    public String getAmount() {
        return amount;
    }

    public boolean isSelcted() {
        return selcted;
    }

    public void setSelcted(boolean selcted) {
        this.selcted = selcted;
    }
}
