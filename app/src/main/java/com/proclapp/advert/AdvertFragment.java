package com.proclapp.advert;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.home.AnswerEditDeleteDialog;
import com.proclapp.model.AllPostsModel;
import com.proclapp.profile.OtherUserProfileActivity;
import com.proclapp.profile.ProfileActivity;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.AlertDialogInterface;
import com.proclapp.utils.DialogUtils;
import com.proclapp.utils.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;

public class AdvertFragment extends Fragment implements View.OnClickListener, SwipyRefreshLayout.OnRefreshListener {

    public static boolean isEditedOrDeleted = false;
    private ImageView img_add_new_advert;
    private SwipyRefreshLayout sr_advert_list;
    private Activity activity;
    private TextView tv_no_data;
    private RecyclerView rv_advert_list;
    private ArrayList<AllPostsModel> advertList = new ArrayList<>();
    private int offset = 0;
    private boolean isServiceCalling = false, isLastPage = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_advert, container, false);

        activity = getActivity();

        initView(view);
        initListener();


        return view;
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.img_add_new_advert:

                startActivity(new Intent(activity, PostAdvActivity.class));

                break;
        }
    }

    private void initView(View view) {
        img_add_new_advert = view.findViewById(R.id.img_add_new_advert);
        rv_advert_list = view.findViewById(R.id.rv_advert_list);
        sr_advert_list = view.findViewById(R.id.sr_advert_list);
        tv_no_data = view.findViewById(R.id.tv_no_data);

        setupRecyclerView();

    }

    private void initListener() {
        img_add_new_advert.setOnClickListener(this);
        sr_advert_list.setOnRefreshListener(this);
    }

    @Override
    public void onRefresh(final SwipyRefreshLayoutDirection direction) {
        offset = direction == SwipyRefreshLayoutDirection.TOP ? 0 : offset + 20;
        advertList();
    }

    private void setupRecyclerView() {

        LinearLayoutManager layoutManager = new LinearLayoutManager(activity);
        rv_advert_list.setLayoutManager(layoutManager);
        rv_advert_list.setAdapter(new AdvertAdapter(activity, advertList, (pos, tag) -> {

            if (tag.equalsIgnoreCase("item_view")) {

                if (StringUtils.isNotEmpty(advertList.get(pos).getAdvert_url())) {
                    String url = advertList.get(pos).getAdvert_url().contains("http") ?
                            advertList.get(pos).getAdvert_url() : "https://" + advertList.get(pos).getAdvert_url();
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    activity.startActivity(browserIntent);

                } else {

                    startActivity(new Intent(activity, AdvertDetailActivity.class)
                            .putExtra("advertDetails", advertList.get(pos)));
                }

            } else if (tag.equalsIgnoreCase("profile")) {

                if (AppClass.preferences.getUserId().equalsIgnoreCase(advertList.get(pos).getAuthorId())) {
                    activity.startActivity(new Intent(activity, ProfileActivity.class));

                } else {
                    if (advertList.get(pos).getIs_user().equalsIgnoreCase("1")) {

                        activity.startActivity(new Intent(activity, OtherUserProfileActivity.class)
                                .putExtra("user_id", advertList.get(pos).getAuthorId()));
                    }
                }

            } else if (tag.equalsIgnoreCase("option")) {

                if (AppClass.preferences.getUserId().equalsIgnoreCase(advertList.get(pos).getAuthorId())) {

                    new AnswerEditDeleteDialog(activity, advertList.get(pos), tag1 -> {

                        if (tag1.equalsIgnoreCase("delete")) {
                            DialogUtils.openDialog(activity, activity.getString(R.string.are_you_sure_you_want_to_delete_this_advert), activity.getString(R.string.delete), activity.getString(R.string.cancel), new AlertDialogInterface() {
                                @Override
                                public void onNegativeBtnClicked(DialogInterface alrt) {
                                    alrt.dismiss();
                                }

                                @Override
                                public void onPositiveBtnClicked(DialogInterface alrt) {
                                    alrt.dismiss();
                                    callAPIDeletePost(pos);
                                }
                            });
                        } else {
                            activity.startActivity(new Intent(activity, PostAdvSummaryActivity.class)
                                    .putExtra("advertdata", advertList.get(pos)));
                        }

                    }).show();

                }

            }

        }));

        sr_advert_list.post(() -> {
            sr_advert_list.setRefreshing(true);
            onRefresh(SwipyRefreshLayoutDirection.TOP);
        });

        rv_advert_list.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int firstVisibleItemPosition = layoutManager.findLastVisibleItemPosition();

                if (!isServiceCalling && !isLastPage) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0) {
                        sr_advert_list.post(() -> {
                            sr_advert_list.setRefreshing(true);
                            onRefresh(SwipyRefreshLayoutDirection.BOTTOM);
                        });

                    }
                }

            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        if (isEditedOrDeleted) {
            sr_advert_list.post(() -> {
                sr_advert_list.setRefreshing(true);
                onRefresh(SwipyRefreshLayoutDirection.TOP);
            });
        }
    }

    void advertList() {


        ApiCall.getInstance().advertList(activity, AppClass.preferences.getUserId(), String.valueOf(offset), new IApiCallback() {
            @Override
            public void onSuccess(Object data) {

                isEditedOrDeleted = false;
                if (offset == 0)
                    advertList.clear();
                advertList.addAll((ArrayList<AllPostsModel>) data);

                    sr_advert_list.setRefreshing(false);
                    isLastPage = false;
                    isServiceCalling = false;
                    rv_advert_list.setVisibility(View.VISIBLE);
                    tv_no_data.setVisibility(View.GONE);
                    rv_advert_list.getAdapter().notifyDataSetChanged();




            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {


                    sr_advert_list.setRefreshing(false);
                    isLastPage = true;
                    isServiceCalling = false;



                if (errorMessage.equalsIgnoreCase("No data found")) {

                    if (offset == 0) {
                        rv_advert_list.setVisibility(View.GONE);
                        tv_no_data.setVisibility(View.VISIBLE);
                    }

                } else {

                    DialogUtils.openDialog(activity, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                        @Override
                        public void onNegativeBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }

                        @Override
                        public void onPositiveBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }
                    });
                }


            }


        }, false);

    }

    private void callAPIDeletePost(int pos) {

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("post_id", advertList.get(pos).getPostId());

        ApiCall.getInstance().deletePost(activity,
                request, new IApiCallback() {
                    @Override
                    public void onSuccess(Object data) {
                        advertList.remove(pos);
                        rv_advert_list.getAdapter().notifyDataSetChanged();

                    }

                    @Override
                    public void onFailure(boolean isFalseFromService, String errorMessage) {


                        DialogUtils.openDialog(activity, errorMessage, activity.getString(R.string.ok), "", new AlertDialogInterface() {
                            @Override
                            public void onNegativeBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }

                            @Override
                            public void onPositiveBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }
                        });
                    }


                }, true);

    }

}
