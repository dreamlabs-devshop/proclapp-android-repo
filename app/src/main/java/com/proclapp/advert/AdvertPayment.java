package com.proclapp.advert;

import android.graphics.Bitmap;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.utils.Utils;

public class AdvertPayment extends AppCompatActivity {
    private WebView webview;
    private RelativeLayout rl_progress_bar;
    private ImageView img_back;
    private String advertId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);
        Utils.setStatusBarColor(getWindow(), this, R.color.white);
        advertId = getIntent().getStringExtra("advert_id");
        initViews();
        loadWebView();
    }

    private void initViews() {
        webview = findViewById(R.id.webview);
        rl_progress_bar = findViewById(R.id.rl_progress_bar);
        img_back = findViewById(R.id.img_back);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }


    private void loadWebView() {

        String url = "http://35.181.75.245/ws/v1/api/do_advert_payment?user_id=" + AppClass.preferences.getUserId() + "&adv=" + advertId;


        if (isFinishing()) {
            onBackPressed();
        } else {

            webview.getSettings().setLoadsImagesAutomatically(true);
            webview.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
            webview.getSettings().setJavaScriptEnabled(true);
            webview.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
            webview.getSettings().setDomStorageEnabled(true);

            WebViewClient webViewClient = new WebViewClient() {

                @Override
                public void onPageStarted(WebView view, String url, Bitmap favicon) {
                    super.onPageStarted(view, url, favicon);
                    rl_progress_bar.setVisibility(View.VISIBLE);
                }

                @Override
                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(view, url);
                    rl_progress_bar.setVisibility(View.GONE);
                    if (url.equalsIgnoreCase("http://35.181.75.245/ws/v1/api/payment_success")) {
                        setResult(RESULT_OK);
                        finish();
                    } else if (url.equalsIgnoreCase("http://35.181.75.245/ws/v1/api/payment_failed")) {
                        setResult(RESULT_CANCELED);
                        finish();
                    }

                }
            };

            webview.setWebViewClient(webViewClient);

            webview.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
            webview.loadUrl(url);
        }

    }


}
