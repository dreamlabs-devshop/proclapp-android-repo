package com.proclapp.advert;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.utils.StaticData;
import com.proclapp.utils.Utils;

public class PostAdvActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView img_back,
            img_company,
            img_product;
    private TextView txt_select_category,
            txt_select_category_make_request_adv,
            txt_company,
            txt_product;
    private Button btn_next;
    private boolean is_company_selected = false;
    private boolean is_product_selected = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_adv);
        Utils.setStatusBarColor(getWindow(), this, R.color.white);
        initView();
        initStyle();
        initListener();
    }

    private void initView() {

        img_back = findViewById(R.id.img_back);
        img_company = findViewById(R.id.img_company);
        img_product = findViewById(R.id.img_product);

        txt_select_category = findViewById(R.id.txt_select_category);
        txt_select_category_make_request_adv = findViewById(R.id.txt_select_category_make_request_adv);
        txt_company = findViewById(R.id.txt_company);
        txt_product = findViewById(R.id.txt_product);

        btn_next = findViewById(R.id.btn_next);
    }

    private void initStyle() {

        txt_select_category.setTypeface(AppClass.lato_regular);
        txt_select_category_make_request_adv.setTypeface(AppClass.lato_regular);
        txt_company.setTypeface(AppClass.lato_medium);
        txt_product.setTypeface(AppClass.lato_medium);
        btn_next.setTypeface(AppClass.lato_semibold);
    }

    private void initListener() {

        img_back.setOnClickListener(this);
        img_company.setOnClickListener(this);
        img_product.setOnClickListener(this);
        btn_next.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.img_back:
                onBackPressed();
                break;

            case R.id.img_company:

                if (is_company_selected) {
                    is_company_selected = false;
                    img_company.setImageResource(R.drawable.company_unselected);
                } else {
                    is_company_selected = true;
                    img_company.setImageResource(R.drawable.company_selected);
                }

                if (is_product_selected) {
                    is_product_selected = false;
                    img_product.setImageResource(R.drawable.product_unselected);
                }

                break;

            case R.id.img_product:

                if (is_product_selected) {
                    is_product_selected = false;
                    img_product.setImageResource(R.drawable.product_unselected);
                } else {
                    is_product_selected = true;
                    img_product.setImageResource(R.drawable.product_selected);
                }

                if (is_company_selected) {
                    is_company_selected = false;
                    img_company.setImageResource(R.drawable.company_unselected);
                }

                break;

            case R.id.btn_next:

                if (is_company_selected) {
                    StaticData.post_adv_category = getResources().getString(R.string.company);
                }

                if (is_product_selected) {
                    StaticData.post_adv_category = getResources().getString(R.string.product);
                }

                if (is_company_selected || is_product_selected) {
                    startActivity(new Intent(PostAdvActivity.this, PostAdvDescriptionAndImagesActivity.class));
                } else {
                    AppClass.snackBarView.snackBarShow(PostAdvActivity.this, getResources().getString(R.string.select_any_category));
                }
                break;
        }
    }
}
