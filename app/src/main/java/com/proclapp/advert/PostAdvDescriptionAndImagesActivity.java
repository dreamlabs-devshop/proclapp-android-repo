package com.proclapp.advert;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.model.PostAdvUploadModel;
import com.proclapp.utils.StaticData;
import com.proclapp.utils.Utils;

public class PostAdvDescriptionAndImagesActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView img_back;
    private TextView
            txt_add_description,
            txt_add_desc_product_company;
    private EditText
            edt_title,
            et_url,
            edt_add_description;
    private Button btn_next;

    private PostAdvUploadAdapter postAdvUploadAdapter;
    private RecyclerView rv_images;
    private TextView
            txt_upload_images,
            txt_upload_images_max;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_adv_description);
        Utils.setStatusBarColor(getWindow(), this, R.color.white);
        initView();
        initStyle();
        initListener();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (StaticData.is_post_adv_summary_image_edit) {
            StaticData.is_post_adv_summary_image_edit = false;
            postAdvUploadAdapter = new PostAdvUploadAdapter(this, StaticData.postAdvUploadModelList);
            rv_images.setAdapter(postAdvUploadAdapter);
        }
    }

    private void initView() {

        txt_add_description = findViewById(R.id.txt_add_description);
        txt_add_desc_product_company = findViewById(R.id.txt_add_desc_product_company);

        edt_title = findViewById(R.id.edt_title);
        edt_add_description = findViewById(R.id.edt_add_description);
        et_url = findViewById(R.id.et_url);

        btn_next = findViewById(R.id.btn_next);
        img_back = findViewById(R.id.img_back);

        //images

        txt_upload_images = findViewById(R.id.txt_upload_images);
        txt_upload_images_max = findViewById(R.id.txt_upload_images_max);

        rv_images = findViewById(R.id.rv_images);

        GridLayoutManager layoutManager = new GridLayoutManager(this, 3);
        rv_images.setLayoutManager(layoutManager);


        // postAdvUploadModelList = new ArrayList<>();
        PostAdvUploadModel advUploadModel = new PostAdvUploadModel();
        //  postAdvUploadModelList.add(advUploadModel);

        StaticData.postAdvUploadModelList.clear();
        StaticData.postAdvUploadModelList.add(advUploadModel);
        postAdvUploadAdapter = new PostAdvUploadAdapter(this, StaticData.postAdvUploadModelList);
        rv_images.setAdapter(postAdvUploadAdapter);

        txt_upload_images.setTypeface(AppClass.lato_regular);
        txt_upload_images_max.setTypeface(AppClass.lato_regular);
    }

    private void initStyle() {

        txt_add_description.setTypeface(AppClass.lato_regular);
        txt_add_desc_product_company.setTypeface(AppClass.lato_regular);
        edt_title.setTypeface(AppClass.lato_regular);
        et_url.setTypeface(AppClass.lato_regular);
        edt_add_description.setTypeface(AppClass.lato_regular);
        btn_next.setTypeface(AppClass.lato_semibold);
    }

    private void initListener() {

        img_back.setOnClickListener(this);
        btn_next.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.img_back:
                onBackPressed();
                break;

            case R.id.btn_next:

                if (edt_title.getText().toString().isEmpty()) {
                    AppClass.snackBarView.snackBarShow(PostAdvDescriptionAndImagesActivity.this, getResources().getString(R.string.enter_title));

                } else if (et_url.getText().toString().isEmpty()) {
                    AppClass.snackBarView.snackBarShow(PostAdvDescriptionAndImagesActivity.this, getResources().getString(R.string.enter_url));

                } else if (!et_url.getText().toString().isEmpty() &&
                        !Patterns.WEB_URL.matcher(et_url.getText().toString()).matches()) {
                    AppClass.snackBarView.snackBarShow(PostAdvDescriptionAndImagesActivity.this, getResources().getString(R.string.enter_valid_url));

                } else if (edt_add_description.getText().toString().isEmpty()) {
                    AppClass.snackBarView.snackBarShow(PostAdvDescriptionAndImagesActivity.this, getResources().getString(R.string.enter_description));

                } else {

                    StaticData.post_adv_title = edt_title.getText().toString();
                    StaticData.post_adv_description = edt_add_description.getText().toString();
                    StaticData.post_adv_url = et_url.getText().toString();

                    StaticData.is_post_adv_summary_image_edit = false;
                    //startActivity(new Intent(PostAdvDescriptionActivity.this, PostAdvUploadImageActivity.class));

                    if (StaticData.postAdvUploadModelList.size() == 1) {

                        if (StaticData.postAdvUploadModelList.get(0).getImage() == null) {
                            AppClass.snackBarView.snackBarShow(this, getResources().getString(R.string.select_image));
                        } else {
                            startActivity(new Intent(this, PostAdvDurationActivity.class));
                        }

                    } else {
                        startActivity(new Intent(this, PostAdvDurationActivity.class));
                    }
                }

                break;
        }
    }
}
