package com.proclapp.advert;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.interfaces.RecyclerClickListener;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.Logger;
import com.proclapp.utils.StringUtils;
import com.proclapp.utils.Utils;

import java.util.ArrayList;

public class PostAdvDurationActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView img_back;
    private TextView
            txt_select_duration,
            txt_to_keep_advertisement_live_till_selected_timeslot,
            txt_amount,
            txt_payable_amount,
            txt_days,
            txt_time_min,
            txt_amount_text;
    private RecyclerView rv_duration_list;
    private Button btn_confirm_pay;
    private int offset = 0;
    private ArrayList<AdvertDurationsModel> advertDurationsList = new ArrayList<>();

    private String days = "", amount = "", time = "", advertTimeSlotId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_adv_duration);
        Utils.setStatusBarColor(getWindow(), this, R.color.white);
        initView();
        initStyle();
        initListener();
        setupRecyclerView();
    }

    private void initView() {
        img_back = findViewById(R.id.img_back);
        txt_select_duration = findViewById(R.id.txt_select_duration);
        txt_to_keep_advertisement_live_till_selected_timeslot = findViewById(R.id.txt_to_keep_advertisement_live_till_selected_timeslot);
        txt_amount = findViewById(R.id.txt_amount);
        txt_payable_amount = findViewById(R.id.txt_payable_amount);
        txt_days = findViewById(R.id.txt_days);
        txt_time_min = findViewById(R.id.txt_time_min);
        txt_amount_text = findViewById(R.id.txt_amount_text);
        btn_confirm_pay = findViewById(R.id.btn_confirm_pay);
        rv_duration_list = findViewById(R.id.rv_duration_list);
    }

    private void initStyle() {
        txt_to_keep_advertisement_live_till_selected_timeslot.setTypeface(AppClass.lato_regular);
        txt_select_duration.setTypeface(AppClass.lato_regular);
        txt_amount.setTypeface(AppClass.lato_semibold);
        txt_payable_amount.setTypeface(AppClass.lato_semibold);
        txt_days.setTypeface(AppClass.lato_heavy);
        txt_time_min.setTypeface(AppClass.lato_heavy);
        txt_amount_text.setTypeface(AppClass.lato_heavy);
        btn_confirm_pay.setTypeface(AppClass.lato_semibold);
    }

    private void initListener() {
        img_back.setOnClickListener(this);
        btn_confirm_pay.setOnClickListener(this);
    }

    private void setupRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(PostAdvDurationActivity.this);
        rv_duration_list.setLayoutManager(layoutManager);
        rv_duration_list.setAdapter(new PostAdvDurationAdapter(PostAdvDurationActivity.this, advertDurationsList, "", new RecyclerClickListener() {
            @Override
            public void onItemClick(int pos, String tag) {

                days = advertDurationsList.get(pos).getNo_of_days();
                time = advertDurationsList.get(pos).getTime_in_min();
                amount = advertDurationsList.get(pos).getAmount();
                advertTimeSlotId = advertDurationsList.get(pos).getAdvert_time_slot_id();

                txt_amount.setText("$" + amount);

                Logger.e("Day " + days + " " + time + " " + amount);

            }
        }));

        getDurationsAndAmount();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.img_back:
                onBackPressed();
                break;

            case R.id.btn_confirm_pay:
                if (StringUtils.isNotEmpty(advertTimeSlotId)) {
                    startActivity(new Intent(PostAdvDurationActivity.this, PostAdvSummaryActivity.class)
                            .putExtra("days", days)
                            .putExtra("time", time)
                            .putExtra("amount", amount)
                            .putExtra("advertTimeSlotId", advertTimeSlotId));
                } else {
                    AppClass.snackBarView.snackBarShow(this, "Please select duration");
                }
                break;
        }
    }

    private void getDurationsAndAmount() {

        ApiCall.getInstance().advertTimeSlot(this, AppClass.preferences.getUserId(), String.valueOf(offset), new IApiCallback() {
            @Override
            public void onSuccess(Object data) {

                if (offset == 0)
                    advertDurationsList.clear();
                advertDurationsList.addAll((ArrayList<AdvertDurationsModel>) data);
                rv_duration_list.getAdapter().notifyDataSetChanged();

            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {

            }
        }, true);

    }

}
