package com.proclapp.advert;

import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.interfaces.RecyclerClickListener;

import java.util.ArrayList;

/**
 *
 */

public class PostAdvDurationAdapter extends RecyclerView.Adapter<PostAdvDurationAdapter.MyViewHolder> {

    private Activity activity;
    private ArrayList<AdvertDurationsModel> advertDurationsLists;
    private RecyclerClickListener clickListener;
    private int oldPos = -1;
    private String day = "";

    public PostAdvDurationAdapter(Activity activity, ArrayList<AdvertDurationsModel> advertDurationsLists, String day, RecyclerClickListener clickListener) {
        this.activity = activity;
        this.advertDurationsLists = advertDurationsLists;
        this.day = day;
        this.clickListener = clickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(activity).inflate(R.layout.row_post_adv_duration, parent, false);
        return new MyViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        AdvertDurationsModel advertDurationsdata = advertDurationsLists.get(holder.getAdapterPosition());

        holder.txt_days.setText(advertDurationsdata.getNo_of_days());
        holder.txt_time.setText(advertDurationsdata.getTime_in_min());
        holder.txt_amount.setText(String.format("$%s", advertDurationsdata.getAmount()));


        if (advertDurationsdata.isSelcted()) {
            holder.iv_amount_select.setSelected(true);
            oldPos = holder.getAdapterPosition();
        } else {
            holder.iv_amount_select.setSelected(false);
        }


        holder.iv_amount_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (oldPos >= 0) {
                    advertDurationsLists.get(oldPos).setSelcted(false);
                }

                advertDurationsLists.get(holder.getAdapterPosition()).setSelcted(true);
                clickListener.onItemClick(holder.getAdapterPosition(), "select");

                oldPos = holder.getAdapterPosition();
                notifyDataSetChanged();
            }
        });


    }

    @Override
    public int getItemCount() {
        return advertDurationsLists.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView
                txt_days,
                txt_time,
                txt_amount;

        private ImageView iv_amount_select;

        public MyViewHolder(View itemView) {
            super(itemView);

            txt_days = itemView.findViewById(R.id.txt_days);
            txt_time = itemView.findViewById(R.id.txt_time);
            txt_amount = itemView.findViewById(R.id.txt_amount);

            iv_amount_select = itemView.findViewById(R.id.iv_amount_select);

            txt_days.setTypeface(AppClass.lato_regular);
            txt_time.setTypeface(AppClass.lato_regular);
            txt_amount.setTypeface(AppClass.lato_regular);

        }
    }

}
