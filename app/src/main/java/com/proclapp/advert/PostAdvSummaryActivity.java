package com.proclapp.advert;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.dialog.AlertDialog;
import com.proclapp.home.HomeActivity;
import com.proclapp.interfaces.RecyclerClickListener;
import com.proclapp.model.AllPostsModel;
import com.proclapp.model.PostAdvUploadModel;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.StaticData;
import com.proclapp.utils.StringUtils;
import com.proclapp.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;

import okhttp3.RequestBody;

public class PostAdvSummaryActivity extends AppCompatActivity implements View.OnClickListener {

    private final int PAYMENT_REQUEST = 123;
    private ImageView img_back;
    private TextView
            txt_summary,
            txt_category,
            txt_category_value,
            txt_category_edit,
            txt_title_description,
            et_url,
            txt_title_description_edit,
            txt_images,
            txt_images_edit,
            txt_duration,
            txt_duration_edit,
            txt_duration_value,
            txt_amount,
            txt_payable_amount;

    private EditText txt_title_value,
            txt_description_value;
    private String advertId = "";
    private RecyclerView rv_summary_images;
    private Button btn_paynow;
    private PostAdvSummaryImagesAdapter postAdvSummaryImagesAdapter;
    private String days = "", amount = "", time = "", advertTimeSlotId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_adv_summary);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        Utils.setStatusBarColor(getWindow(), this, R.color.white);

        initView();
        initStyle();
        initListener();

        setData();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK ) {
            new AlertDialog(PostAdvSummaryActivity.this, "", getString(R.string.payment_success), getString(R.string.ok), "", new AlertDialog.AlertInterface() {
                @Override
                public void onNegativeBtnClicked(Dialog dialog) {
                    dialog.dismiss();
                }

                @Override
                public void onPositiveBtnClicked(Dialog dialog) {
                    dialog.dismiss();
                    finishAffinity();
                    startActivity(new Intent(PostAdvSummaryActivity.this, HomeActivity.class));
                }
            }).show();
        }
        if (resultCode == RESULT_CANCELED) {
            new AlertDialog(PostAdvSummaryActivity.this, "", getString(R.string.payment_fail_purchase), getString(R.string.try_again), getString(R.string.cancel), new AlertDialog.AlertInterface() {
                @Override
                public void onNegativeBtnClicked(Dialog dialog) {
                    dialog.dismiss();
                }

                @Override
                public void onPositiveBtnClicked(Dialog dialog) {
                    dialog.dismiss();
                    finishAffinity();
                    startActivityForResult(new Intent(PostAdvSummaryActivity.this, AdvertPayment.class)
                            .putExtra("advert_id", advertId), PAYMENT_REQUEST);
                }
            }).show();
        }


    }

    private void setData() {
        txt_category_value.setText(StaticData.post_adv_category);
        txt_title_value.setText(StaticData.post_adv_title);
        txt_description_value.setText(StaticData.post_adv_description);
        et_url.setText(StaticData.post_adv_url);
        txt_title_value.setEnabled(false);
        txt_description_value.setEnabled(false);

        if (getIntent() != null &&
                getIntent().hasExtra("advertdata") &&
                getIntent().getSerializableExtra("advertdata") != null) {
            AllPostsModel model = (AllPostsModel) getIntent().getSerializableExtra("advertdata");
            StaticData.post_adv_category = model.getCategoryLabel();
            StaticData.post_adv_title = model.getPostTitle();
            StaticData.post_adv_description = model.getPostDescription();
            txt_category_value.setText(model.getCategoryLabel());
            txt_title_value.setText(model.getPostTitle());
            txt_description_value.setText(model.getPostDescription());
            advertId = model.getPostId();
            if (StringUtils.isNotEmpty(model.getAdvert_url()))
                et_url.setText(model.getAdvert_url());

            days = model.getNoOfDays();
            amount = model.getAmount();
            advertTimeSlotId = model.getAdvertTimeSlotId();
            txt_duration_value.setText(days + " days of $" + amount);
            txt_amount.setText("$" + amount);
            advertTimeSlotId = model.getAdvertTimeSlotId();
            StaticData.postAdvUploadModelList.clear();
            for (int i = 0; i < model.getImages().size(); i++) {
                PostAdvUploadModel postAdvUploadModel = new PostAdvUploadModel();
                postAdvUploadModel.setImageId(model.getImages().get(i).getPost_img_id());
                postAdvUploadModel.setImageUrl(model.getImages().get(i).getImage_name());
                StaticData.postAdvUploadModelList.add(postAdvUploadModel);
            }
            StaticData.postAdvUploadModelList.add(new PostAdvUploadModel());

        }

    }

    private void initView() {

        img_back = findViewById(R.id.img_back);

        txt_summary = findViewById(R.id.txt_summary);
        txt_category = findViewById(R.id.txt_category);
        txt_category_value = findViewById(R.id.txt_category_value);
        txt_category_edit = findViewById(R.id.txt_category_edit);
        txt_title_description = findViewById(R.id.txt_title_description);
        txt_title_description_edit = findViewById(R.id.txt_title_description_edit);
        et_url = findViewById(R.id.et_url);
        txt_title_value = findViewById(R.id.txt_title_value);
        txt_description_value = findViewById(R.id.txt_description_value);
        txt_images = findViewById(R.id.txt_images);
        txt_images_edit = findViewById(R.id.txt_images_edit);
        txt_duration = findViewById(R.id.txt_duration);
        txt_duration_edit = findViewById(R.id.txt_duration_edit);
        txt_duration_value = findViewById(R.id.txt_duration_value);
        txt_amount = findViewById(R.id.txt_amount);
        txt_payable_amount = findViewById(R.id.txt_payable_amount);

        rv_summary_images = findViewById(R.id.rv_summary_images);
        LinearLayoutManager layoutManager = new LinearLayoutManager(PostAdvSummaryActivity.this, LinearLayoutManager.HORIZONTAL, false);
        rv_summary_images.setLayoutManager(layoutManager);

        postAdvSummaryImagesAdapter = new PostAdvSummaryImagesAdapter(PostAdvSummaryActivity.this, StaticData.postAdvUploadModelList);
        rv_summary_images.setAdapter(postAdvSummaryImagesAdapter);

        btn_paynow = findViewById(R.id.btn_paynow);

        days = getIntent().getStringExtra("days");
        amount = getIntent().getStringExtra("amount");
        advertTimeSlotId = getIntent().getStringExtra("advertTimeSlotId");

        txt_duration_value.setText(String.format("%s days of $%s", days, amount));
        txt_amount.setText(String.format("$%s", amount));


    }

    private void initStyle() {
        txt_summary.setTypeface(AppClass.lato_regular);
        txt_category.setTypeface(AppClass.lato_regular);
        txt_category_value.setTypeface(AppClass.lato_regular);
        txt_category_edit.setTypeface(AppClass.lato_regular);
        txt_title_description.setTypeface(AppClass.lato_regular);
        txt_title_description_edit.setTypeface(AppClass.lato_regular);
        txt_title_value.setTypeface(AppClass.lato_regular);
        txt_description_value.setTypeface(AppClass.lato_regular);
        txt_images.setTypeface(AppClass.lato_regular);
        txt_images_edit.setTypeface(AppClass.lato_regular);
        txt_duration.setTypeface(AppClass.lato_regular);
        txt_duration_edit.setTypeface(AppClass.lato_regular);
        txt_duration_value.setTypeface(AppClass.lato_regular);
        txt_amount.setTypeface(AppClass.lato_regular);
        txt_payable_amount.setTypeface(AppClass.lato_regular);

    }

    private void initListener() {

        img_back.setOnClickListener(this);
        btn_paynow.setOnClickListener(this);
        txt_category_edit.setOnClickListener(this);
        txt_images_edit.setOnClickListener(this);
        txt_duration_edit.setOnClickListener(this);
        txt_title_description_edit.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.txt_category_edit:

                new DialogCategory(PostAdvSummaryActivity.this).show();

                break;

            case R.id.txt_images_edit:

                StaticData.is_post_adv_summary_image_edit = true;
                new DialogImageEdit(PostAdvSummaryActivity.this).show();

                break;

            case R.id.txt_duration_edit:

                new DialogEditDuration(PostAdvSummaryActivity.this).show();

                break;

            case R.id.btn_paynow:

                if (AppClass.isInternetConnectionAvailable()) {
                    if (getIntent() != null &&
                            getIntent().hasExtra("advertdata")) {
                        updateAdvert();
                    } else
                        addAdvert();
                } else {
                    AppClass.snackBarView.snackBarShow(this, getString(R.string.nonetwork));
                }

                break;

            case R.id.img_back:

                onBackPressed();

                break;

            case R.id.txt_title_description_edit:

                if (txt_title_description_edit.getText().toString().equalsIgnoreCase(getResources().getString(R.string.save))) {

                    txt_title_description_edit.setText(getResources().getString(R.string.edit));

                    txt_title_value.setEnabled(false);
                    txt_description_value.setEnabled(false);
                    et_url.setEnabled(false);

                } else {
                    txt_title_description_edit.setText(getResources().getString(R.string.save));

                    txt_title_value.setEnabled(true);
                    txt_description_value.setEnabled(true);
                    et_url.setEnabled(true);
                    txt_title_value.requestFocus();
                    txt_title_value.setSelection(txt_title_value.getText().length());
                }
                break;
        }
    }

    private void addAdvert() {

        HashMap<String, RequestBody> request = new HashMap<>();
        request.put("user_id", ApiCall.getInstance().getRequestBodyOfString(AppClass.preferences.getUserId()));
        request.put("category", ApiCall.getInstance().getRequestBodyOfString(txt_category_value.getText().toString().equalsIgnoreCase("Product") ? "2" : "1"));
        request.put("post_title", ApiCall.getInstance().getRequestBodyOfString(txt_title_value.getText().toString().trim()));
        request.put("post_description", ApiCall.getInstance().getRequestBodyOfString(txt_description_value.getText().toString().trim()));
        request.put("url", ApiCall.getInstance().getRequestBodyOfString(et_url.getText().toString().trim()));
        request.put("advert_time_slot_id", ApiCall.getInstance().getRequestBodyOfString(advertTimeSlotId));


        ArrayList<String> imagePaths = new ArrayList<>();

        if (StaticData.postAdvUploadModelList.size() < 6) {

            for (int i = 0; i < StaticData.postAdvUploadModelList.size() - 1; i++) {
                imagePaths.add(StaticData.postAdvUploadModelList.get(i).getImage().getAbsolutePath());
            }
        } else {
            for (int i = 0; i < StaticData.postAdvUploadModelList.size(); i++) {
                imagePaths.add(StaticData.postAdvUploadModelList.get(i).getImage().getAbsolutePath());
            }
        }

        ApiCall.getInstance().addAdvert(this, request, imagePaths, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {

                new AlertDialog(PostAdvSummaryActivity.this, "", getString(R.string.advert_added), getString(R.string.ok), "", new AlertDialog.AlertInterface() {
                    @Override
                    public void onNegativeBtnClicked(Dialog dialog) {
                        dialog.dismiss();
                    }

                    @Override
                    public void onPositiveBtnClicked(Dialog dialog) {
                        dialog.dismiss();
                        advertId = (String) data;
                        startActivityForResult(new Intent(PostAdvSummaryActivity.this, AdvertPayment.class)
                                .putExtra("advert_id", advertId), PAYMENT_REQUEST);
                    }
                }).show();

            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {

            }
        }, true);

    }

    private void updateAdvert() {

        HashMap<String, RequestBody> request = new HashMap<>();
        request.put("user_id", ApiCall.getInstance().getRequestBodyOfString(AppClass.preferences.getUserId()));
        request.put("advert_id", ApiCall.getInstance().getRequestBodyOfString(advertId));
        request.put("category", ApiCall.getInstance().getRequestBodyOfString(txt_category_value.getText().toString().equalsIgnoreCase("Product") ? "2" : "1"));
        request.put("post_title", ApiCall.getInstance().getRequestBodyOfString(txt_title_value.getText().toString().trim()));
        request.put("post_description", ApiCall.getInstance().getRequestBodyOfString(txt_description_value.getText().toString().trim()));
        request.put("url", ApiCall.getInstance().getRequestBodyOfString(et_url.getText().toString().trim()));
        request.put("advert_time_slot_id", ApiCall.getInstance().getRequestBodyOfString(advertTimeSlotId));
        request.put("delete_image", ApiCall.getInstance().getRequestBodyOfString(StaticData.post_adv_delete_image_id));


        ArrayList<String> imagePaths = new ArrayList<>();

        if (StaticData.postAdvUploadModelList.size() < 6) {

            for (int i = 0; i < StaticData.postAdvUploadModelList.size() - 1; i++) {
                if (!StringUtils.isNotEmpty(StaticData.postAdvUploadModelList.get(i).getImageId()))
                    imagePaths.add(StaticData.postAdvUploadModelList.get(i).getImage().getAbsolutePath());
            }
        } else {
            for (int i = 0; i < StaticData.postAdvUploadModelList.size(); i++) {
                if (!StringUtils.isNotEmpty(StaticData.postAdvUploadModelList.get(i).getImageId()))
                    imagePaths.add(StaticData.postAdvUploadModelList.get(i).getImage().getAbsolutePath());
            }
        }

        ApiCall.getInstance().updateAdvert(this, request, imagePaths, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {

                AdvertFragment.isEditedOrDeleted = true;
                AppClass.snackBarView.snackBarShow(PostAdvSummaryActivity.this, getString(R.string.advert_updated));
                new Handler().postDelayed(() -> onBackPressed(), 500);

            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {

            }
        }, true);

    }

    class DialogCategory extends Dialog {

        private Activity activity;

        private TextView txt_select_category,
                txt_company,
                txt_product;

        private LinearLayout
                ll_product,
                ll_company;
        private ImageView
                img_company,
                img_product;

        private DialogCategory(Activity a) {
            super(a);
            this.activity = a;
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.dialog_change_post_adv_category);

            txt_select_category = findViewById(R.id.txt_select_category);
            txt_company = findViewById(R.id.txt_company);
            txt_product = findViewById(R.id.txt_product);
            ll_product = findViewById(R.id.ll_product);
            ll_company = findViewById(R.id.ll_company);
            img_company = findViewById(R.id.img_company);
            img_product = findViewById(R.id.img_product);

            getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

            txt_select_category.setTypeface(AppClass.lato_regular);
            txt_company.setTypeface(AppClass.lato_regular);
            txt_product.setTypeface(AppClass.lato_regular);

            if (StaticData.post_adv_category.equalsIgnoreCase(getResources().getString(R.string.product))) {
                img_company.setImageResource(R.drawable.company_unselected);
                img_product.setImageResource(R.drawable.product_selected);
            } else {
                img_company.setImageResource(R.drawable.company_selected);
                img_product.setImageResource(R.drawable.product_unselected);
            }

            ll_company.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    img_company.setImageResource(R.drawable.company_selected);
                    txt_category_value.setText(activity.getResources().getString(R.string.company));
                    StaticData.post_adv_category = txt_category_value.getText().toString();

                    dismiss();
                }
            });

            ll_product.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    img_product.setImageResource(R.drawable.product_selected);
                    txt_category_value.setText(activity.getResources().getString(R.string.product));
                    StaticData.post_adv_category = txt_category_value.getText().toString();
                    dismiss();
                }
            });

        }


    }

    class DialogImageEdit extends Dialog {

        private Activity activity;

        private RecyclerView rv_images;
        private TextView txt_done;

        private DialogImageEdit(Activity a) {
            super(a);
            this.activity = a;
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.dialog_summay_image_edit);

            setCancelable(false);
            rv_images = findViewById(R.id.rv_images);
            txt_done = findViewById(R.id.txt_done);
            txt_done.setTypeface(AppClass.lato_semibold);

            getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

            GridLayoutManager layoutManager = new GridLayoutManager(PostAdvSummaryActivity.this, 3);
            rv_images.setLayoutManager(layoutManager);

            if (StaticData.postAdvUploadModelList.size() == 6) {
                if (StaticData.postAdvUploadModelList.get(5).getImage() == null) {
                    StaticData.postAdvUploadModelList.remove(5);
                }
            }

            PostAdvUploadAdapter postAdvUploadAdapter = new PostAdvUploadAdapter(PostAdvSummaryActivity.this, StaticData.postAdvUploadModelList);
            rv_images.setAdapter(postAdvUploadAdapter);


            txt_done.setOnClickListener(view -> {
                dismiss();
                postAdvSummaryImagesAdapter.notifyDataSetChanged();
            });
        }


    }

    class DialogEditDuration extends Dialog {

        private Activity activity;

        private RecyclerView rv_duration_list;
        private TextView
                txt_done,
                txt_days,
                txt_time_min,
                txt_amount_text;

        private int offset = 0;
        private ArrayList<AdvertDurationsModel> advertDurationsList = new ArrayList<>();

        private DialogEditDuration(Activity a) {
            super(a);
            this.activity = a;
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.dialog_summay_duration_edit);

            rv_duration_list = findViewById(R.id.rv_duration_list);
            txt_done = findViewById(R.id.txt_done);

            txt_days = findViewById(R.id.txt_days);
            txt_time_min = findViewById(R.id.txt_time_min);
            txt_amount_text = findViewById(R.id.txt_amount_text);

            txt_done.setTypeface(AppClass.lato_semibold);
            txt_days.setTypeface(AppClass.lato_semibold);
            txt_time_min.setTypeface(AppClass.lato_semibold);
            txt_amount_text.setTypeface(AppClass.lato_semibold);

            getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

            txt_done.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dismiss();
                    txt_duration_value.setText(days + " days of $" + amount);
                    txt_amount.setText("$" + amount);
                }
            });

            setupRecyclerView();

        }

        private void setupRecyclerView() {
            LinearLayoutManager layoutManager = new LinearLayoutManager(PostAdvSummaryActivity.this);
            rv_duration_list.setLayoutManager(layoutManager);
            rv_duration_list.setAdapter(new PostAdvDurationAdapter(PostAdvSummaryActivity.this, advertDurationsList, days, new RecyclerClickListener() {
                @Override
                public void onItemClick(int pos, String tag) {
                    days = advertDurationsList.get(pos).getNo_of_days();
                    time = advertDurationsList.get(pos).getTime_in_min();
                    amount = advertDurationsList.get(pos).getAmount();
                    advertTimeSlotId = advertDurationsList.get(pos).getAdvert_time_slot_id();
                }
            }));

            getDurationsAndAmount();

        }

        private void getDurationsAndAmount() {

            ApiCall.getInstance().advertTimeSlot(activity, AppClass.preferences.getUserId(), String.valueOf(offset), new IApiCallback() {
                @Override
                public void onSuccess(Object data) {

                    if (offset == 0)
                        advertDurationsList.clear();
                    advertDurationsList.addAll((ArrayList<AdvertDurationsModel>) data);

                    for (int i = 0; i < advertDurationsList.size(); i++) {

                        if (advertDurationsList.get(i).getNo_of_days().equalsIgnoreCase(days)) {
                            advertDurationsList.get(i).setSelcted(true);
                        }

                    }

                    rv_duration_list.getAdapter().notifyDataSetChanged();

                }

                @Override
                public void onFailure(boolean isFalseFromService, String errorMessage) {

                }
            }, true);

        }

    }
}
