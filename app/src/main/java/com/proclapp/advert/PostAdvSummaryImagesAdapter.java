package com.proclapp.advert;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import androidx.recyclerview.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.proclapp.R;
import com.proclapp.model.PostAdvUploadModel;
import com.proclapp.utils.ImageLoadUtils;
import com.proclapp.utils.StringUtils;

import java.util.ArrayList;

/**
 *
 */

public class PostAdvSummaryImagesAdapter extends RecyclerView.Adapter<PostAdvSummaryImagesAdapter.MyViewHolder> {

    private Activity activity;
    private ArrayList<PostAdvUploadModel> postAdvUploadModelList;
    private DisplayMetrics metrics;

    PostAdvSummaryImagesAdapter(Activity activity, ArrayList<PostAdvUploadModel> postAdvUploadModelList) {
        this.activity = activity;
        this.postAdvUploadModelList = postAdvUploadModelList;

        metrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_post_adv_upload_image, parent, false);
        return new MyViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        if (postAdvUploadModelList.get(position).getImage() != null) {
            Bitmap myBitmap = BitmapFactory.decodeFile(postAdvUploadModelList.get(position).getImage().getAbsolutePath());
            holder.img_adv_upload.setImageBitmap(myBitmap);
            holder.img_adv_upload.setVisibility(View.VISIBLE);
        } else if (StringUtils.isNotEmpty(postAdvUploadModelList.get(position).getImageUrl())) {
            ImageLoadUtils.imageLoad(activity, holder.img_adv_upload,postAdvUploadModelList.get(position).getImageUrl());
            holder.img_adv_upload.setVisibility(View.VISIBLE);
        } else {
            holder.img_adv_upload.setVisibility(View.GONE);
        }


        if (postAdvUploadModelList.size() > 3) {
            if (position == 2) {
                holder.img_overlap.setVisibility(View.VISIBLE);

                if (postAdvUploadModelList.size() == 4) {
                    if (postAdvUploadModelList.get(3).getImage() == null) {
                        holder.img_overlap.setVisibility(View.GONE);
                        holder.txt_count.setVisibility(View.GONE);
                    }
                } else if (postAdvUploadModelList.size() == 6) {
                    if (postAdvUploadModelList.get(5).getImage() != null) {
                        holder.txt_count.setText("+" + (postAdvUploadModelList.size() - 2));
                        holder.txt_count.setVisibility(View.VISIBLE);
                    } else {
                        holder.txt_count.setText("+" + (postAdvUploadModelList.size() - 3));
                        holder.txt_count.setVisibility(View.VISIBLE);
                    }
                } else {
                    holder.txt_count.setText("+" + (postAdvUploadModelList.size() - 3));
                    holder.txt_count.setVisibility(View.VISIBLE);
                }

            } else {
                holder.img_overlap.setVisibility(View.GONE);
                holder.txt_count.setVisibility(View.GONE);
            }
        } else {
            holder.img_overlap.setVisibility(View.GONE);
            holder.txt_count.setVisibility(View.GONE);
        }

        holder.img_delete.setVisibility(View.GONE);

    }

    @Override
    public int getItemCount() {
        if (postAdvUploadModelList.size() > 3) {
            return 3;
        } else {
            return postAdvUploadModelList.size();
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView
                img_adv_upload,
                img_delete,
                img_overlap;

        TextView txt_count;

        public MyViewHolder(View itemView) {
            super(itemView);
            img_adv_upload = itemView.findViewById(R.id.img_adv_upload);
            img_delete = itemView.findViewById(R.id.img_delete);
            img_overlap = itemView.findViewById(R.id.img_overlap);
            txt_count = itemView.findViewById(R.id.txt_count);


        }
    }

}
