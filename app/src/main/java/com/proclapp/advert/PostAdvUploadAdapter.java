package com.proclapp.advert;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.proclapp.R;
import com.proclapp.model.PostAdvUploadModel;
import com.proclapp.utils.ImageLoadUtils;
import com.proclapp.utils.ImagePickerUtils.ImagePickerBottomSheet;
import com.proclapp.utils.StaticData;
import com.proclapp.utils.StringUtils;

import java.io.File;
import java.util.ArrayList;

/**
 *
 */

public class PostAdvUploadAdapter extends RecyclerView.Adapter<PostAdvUploadAdapter.MyViewHolder> {

    private Activity activity;
    // private ArrayList<PostAdvUploadModel> postAdvUploadModelList;
    private DisplayMetrics metrics;

    PostAdvUploadAdapter(Activity activity, ArrayList<PostAdvUploadModel> postAdvUploadModelList) {
        this.activity = activity;
        //this.postAdvUploadModelList = postAdvUploadModelList;

        metrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_post_adv_upload_image, parent, false);
        return new MyViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        if (StaticData.postAdvUploadModelList.get(position).getImage() != null) {
            Bitmap myBitmap = BitmapFactory.decodeFile(StaticData.postAdvUploadModelList.get(position).getImage().getAbsolutePath());
            holder.img_adv_upload.setImageBitmap(myBitmap);
        } else if (StringUtils.isNotEmpty(StaticData.postAdvUploadModelList.get(position).getImageUrl())) {
            ImageLoadUtils.imageLoad(activity, holder.img_adv_upload, StaticData.postAdvUploadModelList.get(position).getImageUrl());
        } else {
            holder.img_adv_upload.setImageResource(R.drawable.add_nw_img);
        }

        if (StaticData.postAdvUploadModelList.size() == 1) {
            holder.img_delete.setVisibility(View.GONE);
        } else if (StaticData.postAdvUploadModelList.size() == 6 && StaticData.postAdvUploadModelList.get(5).getImage() != null) {
            holder.img_delete.setVisibility(View.VISIBLE);
        } else {
            if (position == (StaticData.postAdvUploadModelList.size() - 1)) {
                holder.img_delete.setVisibility(View.GONE);
            } else {
                holder.img_delete.setVisibility(View.VISIBLE);
            }
        }

        holder.img_adv_upload.setOnClickListener(view -> new ImagePickerBottomSheet((AppCompatActivity) activity, false, new ImagePickerBottomSheet.ImagePickListener() {
            @Override
            public void onPickImage(File imageFile) {

                PostAdvUploadModel model = new PostAdvUploadModel();
                model.setImage(imageFile);
                StaticData.postAdvUploadModelList.set(holder.getAdapterPosition(), model);

                if (StaticData.postAdvUploadModelList.size() < 6) {
                    PostAdvUploadModel advUploadModel = new PostAdvUploadModel();
                    StaticData.postAdvUploadModelList.add(advUploadModel);
                    //StaticData.postAdvUploadModelList.add(advUploadModel);
                }

                notifyDataSetChanged();


            }
        }).show(((AppCompatActivity) activity).getSupportFragmentManager(), ""));

        holder.img_delete.setOnClickListener(view -> {

            if (StringUtils.isNotEmpty(StaticData.postAdvUploadModelList.get(holder.getAdapterPosition()).getImageUrl())) {
                StaticData.post_adv_delete_image_id = StaticData.post_adv_delete_image_id.equalsIgnoreCase("") ?
                        StaticData.postAdvUploadModelList.get(holder.getAdapterPosition()).getImageId() : StaticData.post_adv_delete_image_id + "," + StaticData.postAdvUploadModelList.get(holder.getAdapterPosition()).getImageId();

            }


            if (StaticData.postAdvUploadModelList.size() == 6 && holder.getAdapterPosition() == 5) {
                StaticData.postAdvUploadModelList.get(5).setImage(null);

            } else {

                if (StaticData.postAdvUploadModelList.size() == 6 && StaticData.postAdvUploadModelList.get(5).getImage() != null) {
                    PostAdvUploadModel advUploadModel = new PostAdvUploadModel();
                    StaticData.postAdvUploadModelList.add(advUploadModel);
                    //StaticData.postAdvUploadModelList.add(advUploadModel);
                }

                StaticData.postAdvUploadModelList.remove(holder.getAdapterPosition());
                //StaticData.postAdvUploadModelList.remove(position);
            }


            notifyDataSetChanged();

        });

    }

    @Override
    public int getItemCount() {
        return StaticData.postAdvUploadModelList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView
                img_adv_upload,
                img_delete;

        public MyViewHolder(View itemView) {
            super(itemView);
            img_adv_upload = itemView.findViewById(R.id.img_adv_upload);
            img_delete = itemView.findViewById(R.id.img_delete);


        }
    }

}
