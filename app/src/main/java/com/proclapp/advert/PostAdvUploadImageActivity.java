package com.proclapp.advert;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.model.PostAdvUploadModel;
import com.proclapp.utils.StaticData;
import com.proclapp.utils.Utils;


public class PostAdvUploadImageActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView img_back;
    private TextView
            txt_upload_images,
            txt_upload_images_max;
    private RecyclerView rv_images;

    private Button btn_next;
    private PostAdvUploadAdapter postAdvUploadAdapter;
    //private ArrayList<PostAdvUploadModel> postAdvUploadModelList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_adv_upload_image);
        Utils.setStatusBarColor(getWindow(), this, R.color.white);
        initView();
        initStyle();
        initListener();

    }

    private void initView() {
        img_back = findViewById(R.id.img_back);
        txt_upload_images = findViewById(R.id.txt_upload_images);
        txt_upload_images_max = findViewById(R.id.txt_upload_images_max);
        btn_next = findViewById(R.id.btn_next);
        rv_images = findViewById(R.id.rv_images);

        GridLayoutManager layoutManager = new GridLayoutManager(PostAdvUploadImageActivity.this, 3);
        rv_images.setLayoutManager(layoutManager);


       // postAdvUploadModelList = new ArrayList<>();
        PostAdvUploadModel advUploadModel = new PostAdvUploadModel();
      //  postAdvUploadModelList.add(advUploadModel);

        StaticData.postAdvUploadModelList.clear();
        StaticData.postAdvUploadModelList.add(advUploadModel);
        postAdvUploadAdapter = new PostAdvUploadAdapter(PostAdvUploadImageActivity.this, StaticData.postAdvUploadModelList);
        rv_images.setAdapter(postAdvUploadAdapter);
    }

    private void initStyle() {
        txt_upload_images.setTypeface(AppClass.lato_regular);
        txt_upload_images_max.setTypeface(AppClass.lato_regular);
        btn_next.setTypeface(AppClass.lato_semibold);
    }

    private void initListener() {
        img_back.setOnClickListener(this);
        btn_next.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(StaticData.is_post_adv_summary_image_edit){
            StaticData.is_post_adv_summary_image_edit = false;
            postAdvUploadAdapter = new PostAdvUploadAdapter(PostAdvUploadImageActivity.this, StaticData.postAdvUploadModelList);
            rv_images.setAdapter(postAdvUploadAdapter);
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.img_back:

                onBackPressed();

                break;

            case R.id.btn_next:

                /*StaticData.postAdvUploadModelList.clear();
                StaticData.postAdvUploadModelList.addAll(postAdvUploadModelList);*/

                if (StaticData.postAdvUploadModelList.size() == 1) {

                    if(StaticData.postAdvUploadModelList.get(0).getImage()==null) {
                        AppClass.snackBarView.snackBarShow(PostAdvUploadImageActivity.this, getResources().getString(R.string.select_image));
                    }else {
                        startActivity(new Intent(PostAdvUploadImageActivity.this, PostAdvDurationActivity.class));
                    }

                } else {
                    startActivity(new Intent(PostAdvUploadImageActivity.this, PostAdvDurationActivity.class));
                }

                break;
        }
    }
}
