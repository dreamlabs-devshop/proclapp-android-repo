package com.proclapp.chat;

import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.socket.SocketEmitType;
import com.proclapp.socket.SocketEmitterType;
import com.proclapp.socket.SocketIOConnectionHelper;
import com.proclapp.utils.ImageLoadUtils;
import com.proclapp.utils.Logger;
import com.proclapp.utils.NetworkChangeReceiver;
import com.proclapp.utils.StringUtils;
import com.proclapp.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;

public class ChatActivity extends AppCompatActivity implements
        View.OnClickListener,
        SocketIOConnectionHelper.OnSocketResponseListerner {

    private ImageView
            img_back,
            img_emoji,
            img_send;
    private CircleImageView img_user_profile;
    private TextView txt_user_name;
    private RecyclerView rv_chat;
    private EmojiconEditText et_message;
    private String friendId;
    private String userName = "", userImage = "";
    private ArrayList<ChatModel> messagesList = new ArrayList<>();
    private int offset = 0;
    private boolean
            isServiceCalling = false,
            isLastPage = false;
    private RelativeLayout ll_root;
    private EmojIconActions emojAction;
    private NetworkChangeReceiver changeReceiver;
    private ProgressBar progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        offset = 0;

        Utils.setStatusBarColor(getWindow(), this, R.color.white);

        initView();
        initStyle();
        initListener();
        setupEmojiKeyboard();

        changeReceiver = new NetworkChangeReceiver(intent -> {
            ((AppClass) getApplication()).createSocketConnection();
            ((AppClass) getApplication()).setAppListerner();
            ((AppClass) getApplication()).setOnSocketResponseListerner(this);
            getConversationMsg();
            seenAllMessage();
        });
        registerReceiver(changeReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        try {
            JSONObject jsonObject = new JSONObject(intent.getStringExtra("sender"));
            friendId = jsonObject.getString("user_id");
            userImage = jsonObject.getString("profile_image");
            userName = jsonObject.getString("firstname") + " " + jsonObject.getString("lastname");

            txt_user_name.setText(userName);

            ImageLoadUtils.imageLoad(this,
                    img_user_profile,
                    userImage,
                    R.drawable.user_chat_ph);

            getConversationMsg();
            seenAllMessage();

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        ((AppClass) getApplication()).createSocketConnection();
        ((AppClass) getApplication()).setAppListerner();
        ((AppClass) getApplication()).setOnSocketResponseListerner(this);
    }

    private void initView() {

        img_back = findViewById(R.id.img_back);
        img_emoji = findViewById(R.id.img_emoji);
        img_send = findViewById(R.id.img_send);
        img_user_profile = findViewById(R.id.img_user_profile);
        txt_user_name = findViewById(R.id.txt_user_name);
        rv_chat = findViewById(R.id.rv_chat);
        et_message = findViewById(R.id.et_message);
        progress = findViewById(R.id.progress);


        if (getIntent().hasExtra("sender")) {
            try {
                JSONObject jsonObject = new JSONObject(getIntent().getStringExtra("sender"));
                friendId = jsonObject.getString("user_id");
                userImage = jsonObject.getString("profile_image");
                userName = jsonObject.getString("firstname") + " " + jsonObject.getString("lastname");

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {

            friendId = getIntent().getStringExtra("friend_id");
            userImage = getIntent().getStringExtra("profileimage");
            userName = getIntent().getStringExtra("name");
        }
        txt_user_name.setText(userName);

        ImageLoadUtils.imageLoad(this,
                img_user_profile,
                userImage,
                R.drawable.user_chat_ph);

        setupRecyclerView();
    }


    private void initStyle() {

        txt_user_name.setTypeface(AppClass.lato_bold);
        et_message.setTypeface(AppClass.lato_regular);
    }

    private void initListener() {

        img_back.setOnClickListener(this);
        img_emoji.setOnClickListener(this);
        img_send.setOnClickListener(this);
    }

    private void setupEmojiKeyboard() {
        ll_root = findViewById(R.id.ll_root);
        emojAction = new EmojIconActions(this, ll_root, et_message, img_emoji);
        emojAction.ShowEmojIcon();
        emojAction.setIconsIds(R.drawable.emoji, R.drawable.emoji);
        emojAction.setKeyboardListener(new EmojIconActions.KeyboardListener() {
            @Override
            public void onKeyboardOpen() {
            }

            @Override
            public void onKeyboardClose() {
            }
        });

        et_message.setOnClickListener(view -> emojAction.closeEmojIcon());
    }


    private void setupRecyclerView() {
        final LinearLayoutManager layoutManager = new LinearLayoutManager(ChatActivity.this, LinearLayoutManager.VERTICAL, true);
        rv_chat.setLayoutManager(layoutManager);
        rv_chat.setAdapter(new ChatAdapter(ChatActivity.this, messagesList));

        rv_chat.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int firstVisibleItemPosition = layoutManager.findLastVisibleItemPosition();

                if (!isServiceCalling && !isLastPage && messagesList.size() >= offset) {


                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0) {

                        getConversationMsg();


                    }
                }
            }
        });

        getConversationMsg();
        seenAllMessage();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.img_back:
                onBackPressed();
                break;

            case R.id.img_send:
                if (AppClass.isInternetConnectionAvailable())
                    sendMessage();
                else
                    AppClass.toastView.showMessage(getString(R.string.nonetwork));
                break;
        }
    }

    private void getConversationMsg() {
        isServiceCalling = true;
        ApiCall.getInstance().getConversationMsg(ChatActivity.this, AppClass.preferences.getUserId(), friendId, String.valueOf(offset), new IApiCallback() {
            @Override
            public void onSuccess(Object data) {
                progress.setVisibility(View.GONE);
                if (offset == 0)
                    messagesList.clear();

                messagesList.addAll((ArrayList<ChatModel>) data);
                rv_chat.getAdapter().notifyDataSetChanged();

                isServiceCalling = false;
                isLastPage = false;

                offset += 20;

            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {
                progress.setVisibility(View.GONE);
                isServiceCalling = false;
                isLastPage = true;
            }
        }, false);

    }


    private void sendMessage() {

        if (!StringUtils.isNotEmpty(et_message.getText().toString().trim())) {
            Toast.makeText(this, getString(R.string.nonetwork), Toast.LENGTH_SHORT).show();
            return;
        }

        try {

            JSONObject sendMessage = new JSONObject();
            sendMessage.put("to_user_id", friendId);
            sendMessage.put("message", et_message.getText().toString());
            sendMessage.put("msg_type", "text");

            et_message.setText("");
            ((AppClass) getApplication()).setEmitData(SocketEmitType.send_msg, sendMessage, (type, object) -> runOnUiThread(() -> {

            }));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void seenAllMessage() {

        try {

            JSONObject sendMessage = new JSONObject();
            sendMessage.put("user_id", AppClass.preferences.getUserId());
            sendMessage.put("to_user_id", friendId);

            et_message.setText("");
            ((AppClass) getApplication()).setEmitData(SocketEmitType.msg_seen_all, sendMessage, (type, object) -> {
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public void onSocketResponse(final SocketEmitterType type, final Object object) {
        Logger.e("ConversationResponse " + object.toString());

        runOnUiThread(() -> {


            if (type.NAME.equalsIgnoreCase(SocketEmitterType.new_msg.NAME)) {

                MessageActivity.isMessageSend = true;

                ChatModel messageData = new Gson().fromJson(object.toString(), ChatModel.class);
                if (messageData.getTo_user_id().equalsIgnoreCase(friendId) ||
                        messageData.getTo_user_id().equalsIgnoreCase(AppClass.preferences.getUserId())) {

                    if (!messagesList.contains(messageData)) {
                        messagesList.add(0, messageData);
                        rv_chat.getAdapter().notifyDataSetChanged();
                    }
                }

            }

        });
    }


}
