package com.proclapp.chat;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.proclapp.R;
import com.proclapp.utils.DateTimeUtils;

import java.util.ArrayList;

public class ChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private ArrayList<ChatModel> messagesList;

    public ChatAdapter(Context context, ArrayList<ChatModel> messagesList) {
        this.context = context;
        this.messagesList = messagesList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        if (viewType == 0) {
            View view = LayoutInflater.from(context).inflate(R.layout.row_message_send, parent, false);
            return new ChatMessageSendViewHolder(view);
        }

        View view = LayoutInflater.from(context).inflate(R.layout.row_message_receive, parent, false);
        return new ChatMessageReceiveViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {

        try {
            ChatModel messageData = messagesList.get(holder.getAdapterPosition());

            boolean isToday = DateTimeUtils.getCurrentDate("yyyy-MM-dd").equalsIgnoreCase(DateTimeUtils.changeDateTimeFormat("yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd", messageData.getSent_at()));

            String msgDate = DateTimeUtils.changeDateTimeFormat("yyyy-MM-dd HH:mm:ss", isToday ? "hh:mm a" : "hh:mm a, MMM dd", DateTimeUtils.convertUTCtoLocalTimeZone(messageData.getSent_at(),"yyyy-MM-dd HH:mm:ss"));

            if (holder instanceof ChatMessageReceiveViewHolder) {
                ((ChatMessageReceiveViewHolder) holder).tv_message.setText(messageData.getMessage());
                ((ChatMessageReceiveViewHolder) holder).tv_time.setText(msgDate);

            } else if (holder instanceof ChatMessageSendViewHolder) {
                ((ChatMessageSendViewHolder) holder).tv_message.setText(messageData.getMessage());
                ((ChatMessageSendViewHolder) holder).tv_time.setText(msgDate);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemViewType(int position) {
        if (messagesList.get(position).getType().equalsIgnoreCase("sent")) {
            return 0;
        } else {
            return 1;
        }
    }


    @Override
    public int getItemCount() {
        return messagesList.size();
    }
}
