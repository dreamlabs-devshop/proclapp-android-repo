package com.proclapp.chat;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.proclapp.R;

public class ChatMessageSendViewHolder extends RecyclerView.ViewHolder {


    public TextView tv_message, tv_time;

    public ChatMessageSendViewHolder(View itemView) {
        super(itemView);
        tv_message= itemView.findViewById(R.id.tv_message);
        tv_time= itemView.findViewById(R.id.tv_time);

    }
}
