package com.proclapp.chat;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ChatModel implements Serializable {

    private String
            chat_id,
            message_id,
            offline_id,
            group_id,
            user_id,
            to_user_id,
            type,
            message,
            document,
            document_thumb,
            msg_type,
            sent_at,
            delivered_at,
            seen_at,
            deleted_at,
            edited_at,
            blocked;

    @SerializedName("sender")
    private ChatSenderReceiverModel senderData;

    @SerializedName("receiver")
    private ChatSenderReceiverModel receiverData;


    public String getChat_id() {
        return chat_id;
    }

    public String getMessage_id() {
        return message_id;
    }

    public String getOffline_id() {
        return offline_id;
    }

    public String getGroup_id() {
        return group_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public String getTo_user_id() {
        return to_user_id;
    }

    public String getType() {
        return type;
    }

    public String getMessage() {
        return message;
    }

    public String getDocument() {
        return document;
    }

    public String getDocument_thumb() {
        return document_thumb;
    }

    public String getMsg_type() {
        return msg_type;
    }

    public String getSent_at() {
        return sent_at;
    }

    public String getDelivered_at() {
        return delivered_at;
    }

    public String getSeen_at() {
        return seen_at;
    }

    public String getDeleted_at() {
        return deleted_at;
    }

    public String getEdited_at() {
        return edited_at;
    }

    public String getBlocked() {
        return blocked;
    }

    public ChatSenderReceiverModel getSenderData() {
        return senderData;
    }

    public ChatSenderReceiverModel getReceiverData() {
        return receiverData;
    }

    @Override
    public boolean equals(Object obj) {
        return message_id.equals(((ChatModel) obj).message_id);
    }

    public class ChatSenderReceiverModel implements Serializable {

        private String
                user_id,
                name,
                email,
                last_seen,
                about_my_profession,
                profile_image,
                phone,
                phone_code,
                date,
                i_hve_blocked,
                is_blocked_me;


        public String getUser_id() {
            return user_id;
        }

        public String getName() {
            return name;
        }

        public String getEmail() {
            return email;
        }

        public String getLast_seen() {
            return last_seen;
        }

        public String getAbout_my_profession() {
            return about_my_profession;
        }

        public String getProfile_image() {
            return profile_image;
        }

        public String getPhone() {
            return phone;
        }

        public String getPhone_code() {
            return phone_code;
        }

        public String getDate() {
            return date;
        }

        public String getI_hve_blocked() {
            return i_hve_blocked;
        }

        public String getIs_blocked_me() {
            return is_blocked_me;
        }
    }
}
