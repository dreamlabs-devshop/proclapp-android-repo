package com.proclapp.chat;

import java.io.Serializable;

public class ConversationModel implements Serializable {

    private String
            chat_id,
            message_id,
            offline_id,
            user_id,
            to_user_id,
            type,
            message,
            document,
            document_thumb,
            location,
            msg_type,
            sent_at,
            firstname,
            lastname,
            email,
            profile_image,
            name,
            phone,
            address,
            about_my_profession,
            blocked,
            unread_msg,
            profile_image_thumb;


    public String getChat_id() {
        return chat_id;
    }

    public String getMessage_id() {
        return message_id;
    }

    public String getOffline_id() {
        return offline_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public String getTo_user_id() {
        return to_user_id;
    }

    public String getType() {
        return type;
    }

    public String getMessage() {
        return message;
    }

    public String getDocument() {
        return document;
    }

    public String getDocument_thumb() {
        return document_thumb;
    }

    public String getLocation() {
        return location;
    }

    public String getMsg_type() {
        return msg_type;
    }

    public String getSent_at() {
        return sent_at;
    }

    public String getName() {
        return name;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getEmail() {
        return email;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public String getPhone() {
        return phone;
    }

    public String getAddress() {
        return address;
    }

    public String getAbout_my_profession() {
        return about_my_profession;
    }

    public String getBlocked() {
        return blocked;
    }

    public String getProfile_image_thumb() {
        return profile_image_thumb;
    }

    public String getUnread_msg() {
        return unread_msg;
    }
}
