package com.proclapp.chat;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import com.proclapp.R;

public class FriendListForChatActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend_list_for_chat);
    }
}
