package com.proclapp.chat;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.home.friends.FriendsActivity;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.socket.SocketEmitType;
import com.proclapp.socket.SocketIOConnectionHelper;
import com.proclapp.utils.Logger;
import com.proclapp.utils.Utils;

import org.json.JSONObject;

import java.util.ArrayList;

public class MessageActivity extends AppCompatActivity implements
        View.OnClickListener,
        SwipyRefreshLayout.OnRefreshListener,
        MessgaeBroadCastReveiver.MessageListener {

    public static boolean isMessageSend = false;
    private ImageView img_back,
            img_search,
            img_new_chat;
    private TextView txt_message,
            txt_no_messages_yet,
            tv_retry,
            txt_inbox_empty;
    private Button btn_create_new;
    private RecyclerView rv_messages_list;
    private ArrayList<ConversationModel> conversationList = new ArrayList<>();
    private SwipyRefreshLayout sr_conversation;
    private MessgaeBroadCastReveiver messageReceiver;
    private LinearLayout ll_no_msg;
    private View nonetwork;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);
        Utils.setStatusBarColor(getWindow(), this, R.color.white);
        initView();
        initStyle();
        initListener();
        setupRecyclerView();
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.img_back:
                onBackPressed();
                break;


            case R.id.tv_retry:
                sr_conversation.post(() -> {
                    sr_conversation.setRefreshing(true);
                    onRefresh(SwipyRefreshLayoutDirection.TOP);
                });
                break;

            case R.id.btn_create_new:
            case R.id.img_new_chat:
                startActivity(new Intent(this, FriendsActivity.class)
                        .putExtra("openfrom", "chat")
                );
                break;
        }
    }

    @Override
    public void onRefresh(SwipyRefreshLayoutDirection direction) {
        if (direction == SwipyRefreshLayoutDirection.TOP) {
//            getConversation();
            if (AppClass.isInternetConnectionAvailable()) {
                callGetConversationService();
                nonetwork.setVisibility(View.GONE);
                rv_messages_list.setVisibility(View.VISIBLE);
            } else {
                sr_conversation.setRefreshing(false);
                nonetwork.setVisibility(View.VISIBLE);
                rv_messages_list.setVisibility(View.GONE);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        sr_conversation.post(() -> {
            sr_conversation.setRefreshing(true);
            onRefresh(SwipyRefreshLayoutDirection.TOP);
        });

        if (messageReceiver != null) {
            unregisterReceiver(messageReceiver);
        }

        IntentFilter filter = new IntentFilter("ReviewAdded");
        messageReceiver = new MessgaeBroadCastReveiver(this);
        registerReceiver(messageReceiver, filter);
    }

    private void initView() {

        img_back = findViewById(R.id.img_back);
        img_search = findViewById(R.id.img_search);
        img_new_chat = findViewById(R.id.img_new_chat);

        ll_no_msg = findViewById(R.id.ll_no_msg);
        tv_retry = findViewById(R.id.tv_retry);

        btn_create_new = findViewById(R.id.btn_create_new);

        txt_message = findViewById(R.id.txt_message);
        txt_no_messages_yet = findViewById(R.id.txt_no_messages_yet);
        txt_inbox_empty = findViewById(R.id.txt_inbox_empty);

        rv_messages_list = findViewById(R.id.rv_messages_list);
        sr_conversation = findViewById(R.id.sr_conversation);
        nonetwork = findViewById(R.id.nonetwork);

        sr_conversation.setOnRefreshListener(this);
        sr_conversation.setColorSchemeColors(getResources().getColor(R.color.color_0c6984), getResources().getColor(R.color.black));

    }

    private void initStyle() {
        txt_message.setTypeface(AppClass.lato_regular);
        txt_no_messages_yet.setTypeface(AppClass.lato_black);
        txt_inbox_empty.setTypeface(AppClass.lato_regular);
        btn_create_new.setTypeface(AppClass.lato_semibold);
    }

    private void initListener() {
        img_back.setOnClickListener(this);
        img_search.setOnClickListener(this);
        img_new_chat.setOnClickListener(this);
        tv_retry.setOnClickListener(this);
    }

    private void setupRecyclerView() {

        rv_messages_list.setLayoutManager(new LinearLayoutManager(MessageActivity.this));
        rv_messages_list.setAdapter(new MessageListAdapter(MessageActivity.this, conversationList));

    }


    private void getConversation() {
        ((AppClass) getApplication()).createSocketConnection();
        ((AppClass) getApplication()).setAppListerner();
//        ((AppClass) getApplication()).setOnSocketResponseListerner(this);
        ((AppClass) getApplication()).setEmitData(SocketEmitType.get_conversations, new JSONObject(), new SocketIOConnectionHelper.OnSocketAckListerner() {
            @Override
            public void onSocketAck(SocketEmitType type, final Object object) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        Logger.e("Response " + object.toString());

                    }
                });
            }
        });
    }

    private void callGetConversationService() {

        ApiCall.getInstance().getConversationList(this, AppClass.preferences.getUserId(), new IApiCallback() {
            @Override
            public void onSuccess(Object data) {
                isMessageSend = false;

                conversationList.clear();
                conversationList.addAll((ArrayList<ConversationModel>) data);


                rv_messages_list.setVisibility(View.VISIBLE);
                ll_no_msg.setVisibility(View.GONE);
                img_new_chat.setVisibility(View.VISIBLE);
                sr_conversation.setRefreshing(false);
                rv_messages_list.getAdapter().notifyDataSetChanged();


            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {
                rv_messages_list.setVisibility(View.GONE);
                ll_no_msg.setVisibility(View.VISIBLE);
                img_new_chat.setVisibility(View.GONE);
                sr_conversation.setRefreshing(false);

            }
        }, false);

    }

    @Override
    public void onMessageReceive(Intent intent) {
        if (AppClass.isInternetConnectionAvailable()) {
            callGetConversationService();
            nonetwork.setVisibility(View.GONE);
            rv_messages_list.setVisibility(View.VISIBLE);
        } else {
            rv_messages_list.setVisibility(View.GONE);
            nonetwork.setVisibility(View.VISIBLE);
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(messageReceiver);
    }
}
