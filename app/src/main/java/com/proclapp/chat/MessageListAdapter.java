package com.proclapp.chat;

import android.app.Activity;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.utils.DateTimeUtils;
import com.proclapp.utils.ImageLoadUtils;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 *
 */

public class MessageListAdapter extends RecyclerView.Adapter<MessageListAdapter.MyViewHolder> {

    private Activity activity;
    private ArrayList<ConversationModel> conversationList;

    public MessageListAdapter(Activity activity, ArrayList<ConversationModel> conversationList) {
        this.activity = activity;
        this.conversationList = conversationList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_messages, parent, false);
        return new MyViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        if (!conversationList.get(position).getUnread_msg().equalsIgnoreCase("0")) {
            holder.txt_message_unread_count.setVisibility(View.VISIBLE);
        } else {
            holder.txt_message_unread_count.setVisibility(View.GONE);
        }


        holder.txt_user_name.setText(conversationList.get(position).getName());
        holder.txt_message_value.setText(conversationList.get(position).getMessage());
        holder.txt_message_unread_count.setText(conversationList.get(position).getUnread_msg());
        holder.txt_message_time.setText(DateTimeUtils.changeDateTimeFormat("yyyy-MM-dd HH:mm:ss", "dd MMM", conversationList.get(position).getSent_at()));
        holder.ll_main_msg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.startActivity(new Intent(activity, ChatActivity.class));
            }
        });

        ImageLoadUtils.imageLoad(activity, holder.img_user_pic, conversationList.get(position).getProfile_image(), R.drawable.user_chat_ph);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.startActivity(new Intent(activity, ChatActivity.class)
                        .putExtra("friend_id", conversationList.get(holder.getAdapterPosition()).getTo_user_id())
                        .putExtra("profileimage", conversationList.get(holder.getAdapterPosition()).getProfile_image())
                        .putExtra("name", conversationList.get(holder.getAdapterPosition()).getName()));
            }
        });

    }

    @Override
    public int getItemCount() {
        return conversationList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        CircleImageView img_user_pic;
        TextView
                txt_user_name,
                txt_message_value,
                txt_message_time,
                txt_message_unread_count;
        LinearLayout ll_main_msg;

        public MyViewHolder(View itemView) {
            super(itemView);

            img_user_pic = itemView.findViewById(R.id.img_user_pic);

            txt_user_name = itemView.findViewById(R.id.txt_user_name);
            txt_message_value = itemView.findViewById(R.id.txt_message_value);
            txt_message_time = itemView.findViewById(R.id.txt_message_time);
            txt_message_unread_count = itemView.findViewById(R.id.txt_message_unread_count);
            ll_main_msg = itemView.findViewById(R.id.ll_main_msg);


            txt_user_name.setTypeface(AppClass.lato_bold);
            txt_message_value.setTypeface(AppClass.lato_semibold);
            txt_message_time.setTypeface(AppClass.lato_regular);
            txt_message_unread_count.setTypeface(AppClass.lato_regular);

        }
    }

}
