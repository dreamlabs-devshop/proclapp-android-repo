package com.proclapp.chat;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class MessgaeBroadCastReveiver extends BroadcastReceiver {

    MessageListener messageListener;

    public MessgaeBroadCastReveiver(MessageListener messageListener) {
        this.messageListener = messageListener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        messageListener.onMessageReceive(intent);
    }


    public interface MessageListener {
        void onMessageReceive(Intent intent);
    }

}
