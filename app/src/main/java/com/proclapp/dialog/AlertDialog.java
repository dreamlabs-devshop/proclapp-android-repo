package com.proclapp.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.proclapp.R;
import com.proclapp.utils.StringUtils;


public class AlertDialog extends Dialog implements View.OnClickListener {

    private Context context;
    private String title;
    private String message;
    private String positiveBtnText;
    private String negativeBtnText;
    private AlertInterface alertInterface;
    private View view;
    private TextView
            tvTitle,
            tvMessage,
            tvCancel,
            tvOhk;

    public AlertDialog(@NonNull Context context, String title, String message, String positiveBtnText, String negativeBtnText, AlertInterface alertInterface) {
        super(context);
        this.context = context;
        this.title = title;
        this.message = message;
        this.positiveBtnText = positiveBtnText;
        this.negativeBtnText = negativeBtnText;
        this.alertInterface = alertInterface;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_alert);
//        getWindow().setBackgroundDrawableResource(R.drawable.dialog_background);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        setCancelable(false);

        view = findViewById(R.id.view);
        tvTitle = findViewById(R.id.tv_title);
        tvMessage = findViewById(R.id.tv_message);
        tvCancel = findViewById(R.id.tv_cancel);
        tvOhk = findViewById(R.id.tv_ohk);
        tvCancel.setOnClickListener(this);
        tvOhk.setOnClickListener(this);

        tvMessage.setText(message);
        tvOhk.setText(positiveBtnText);

        if (StringUtils.isNotEmpty(title)) {
            tvTitle.setText(title);
        }

        if (StringUtils.isNotEmpty(negativeBtnText)) {
            tvCancel.setText(negativeBtnText);
            tvCancel.setVisibility(View.VISIBLE);
            view.setVisibility(View.VISIBLE);
        } else {
            tvCancel.setVisibility(View.GONE);
            view.setVisibility(View.GONE);
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_ohk:
                alertInterface.onPositiveBtnClicked(this);
                break;

            case R.id.tv_cancel:
                alertInterface.onNegativeBtnClicked(this);
                break;
        }
    }

    public interface AlertInterface {
        void onNegativeBtnClicked(Dialog dialog);

        void onPositiveBtnClicked(Dialog dialog);
    }

}
