package com.proclapp.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.proclapp.R;
import com.proclapp.home.question.PostAnswerActivity;
import com.proclapp.home.voice_video.VoiceRecordingActivityNew;
import com.proclapp.model.AllPostsModel;
import com.zyf.vc.ui.RecorderActivity;

public class ExpertAnswerDialog extends Dialog implements View.OnClickListener {

    private LinearLayout
            ll_answer,
            ll_make_request_voice,
            ll_make_request_video;
    private ImageView iv_close;
    private AllPostsModel allPostsModel;
    private Context context;

    public ExpertAnswerDialog(@NonNull Context context, AllPostsModel allPostsModel) {
        super(context);
        this.context = context;
        this.allPostsModel = allPostsModel;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_expert_answer);

        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        setCancelable(false);

        initViews();
        setListener();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.iv_close:
                dismiss();
                break;

            case R.id.ll_answer:
                dismiss();
                context.startActivity(new Intent(context, PostAnswerActivity.class)
                        .putExtra("openform", "expert_reply")
                        .putExtra("questionDetails", allPostsModel));
                break;

            case R.id.ll_make_request_voice:
                dismiss();
                context.startActivity(new Intent(context, VoiceRecordingActivityNew.class)
                        .putExtra("openfor", "expert_reply")
                        .putExtra("postId", allPostsModel.getPostId())
                        .putExtra("requestedUserId", allPostsModel.getAuthorId()));
                break;

            case R.id.ll_make_request_video:
                dismiss();
                context.startActivity(new Intent(context, RecorderActivity.class)
                        .putExtra("postId", allPostsModel.getPostId())
                        .putExtra("requestedUserId", allPostsModel.getAuthorId())
                        .putExtra("openfor", "expert_reply"));
                break;

        }
    }

    private void initViews() {

        ll_answer = findViewById(R.id.ll_answer);
        ll_make_request_voice = findViewById(R.id.ll_make_request_voice);
        ll_make_request_video = findViewById(R.id.ll_make_request_video);
        iv_close = findViewById(R.id.iv_close);

    }


    private void setListener() {

        ll_answer.setOnClickListener(this);
        ll_make_request_voice.setOnClickListener(this);
        ll_make_request_video.setOnClickListener(this);
        iv_close.setOnClickListener(this);

    }




}
