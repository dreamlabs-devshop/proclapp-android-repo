package com.proclapp.expert;

import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.proclapp.R;
import com.proclapp.profile.OtherUserProfileActivity;
import com.proclapp.utils.ImageLoadUtils;

import java.util.ArrayList;

public class ExpertAdapter extends RecyclerView.Adapter<ExpertAdapter.ExpertViewHolder> {
    AppCompatActivity context;
    ArrayList<ExpertModel> expertList;

    public ExpertAdapter(AppCompatActivity context, ArrayList<ExpertModel> expertList) {
        this.context = context;
        this.expertList = expertList;
    }

    @NonNull
    @Override
    public ExpertAdapter.ExpertViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_expert, viewGroup, false);
        return new ExpertAdapter.ExpertViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ExpertViewHolder holder, int i) {

        ExpertModel expertData = expertList.get(holder.getAdapterPosition());

        holder.txt_user_name.setText(expertData.getFirstname() + " " + expertData.getLastname());

        ImageLoadUtils.imageLoad(context,
                holder.img_user_profile,
                expertData.getProfileImage(),
                R.drawable.profile_pic_ph);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, OtherUserProfileActivity.class)
                        .putExtra("user_id", expertData.getUserId()));
            }
        });

    }

    @Override
    public int getItemCount() {
        return expertList.size();
    }

    public class ExpertViewHolder extends RecyclerView.ViewHolder {

        ImageView img_user_profile;
        TextView txt_user_name;

        public ExpertViewHolder(@NonNull View itemView) {
            super(itemView);

            img_user_profile = itemView.findViewById(R.id.img_user_profile);
            txt_user_name = itemView.findViewById(R.id.txt_user_name);

            DisplayMetrics metrics = new DisplayMetrics();
            context.getWindowManager().getDefaultDisplay().getMetrics(metrics);

            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams((metrics.widthPixels * 104) / 480, (metrics.heightPixels * 104) / 800);
            //img_user_profile.setLayoutParams(layoutParams);


        }
    }
}
