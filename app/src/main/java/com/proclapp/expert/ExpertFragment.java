package com.proclapp.expert;

import android.content.DialogInterface;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.home.HomeActivity;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.AlertDialogInterface;
import com.proclapp.utils.DialogUtils;
import com.proclapp.utils.Logger;
import com.proclapp.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class ExpertFragment extends Fragment {

    RecyclerView rv_expert_list;
    ArrayList<ExpertModel> expertList = new ArrayList<>();
    HomeActivity homeActivity;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_expert, container, false);
        initViews(view);
        setUpRecyclerView();
        return view;
    }

    private void initViews(View view) {


        homeActivity = (HomeActivity) getActivity();
        rv_expert_list = view.findViewById(R.id.rv_expert_list);

    }

    private void setUpRecyclerView() {

        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 3);
        rv_expert_list.setLayoutManager(layoutManager);
        rv_expert_list.setAdapter(new ExpertAdapter(homeActivity, expertList));

        getExpertList();
    }


    private void getExpertList() {

        ApiCall.getInstance().getExpertList(getActivity(), AppClass.preferences.getUserId(), new IApiCallback() {
            @Override
            public void onSuccess(Object data) {
                JsonArray response = (JsonArray) data;

                ArrayList<ExpertModel> tempExpertList = new Gson().fromJson(response.toString(),
                        new TypeToken<List<ExpertModel>>() {
                        }.getType());


                expertList.clear();
                expertList.addAll(tempExpertList);
                rv_expert_list.getAdapter().notifyDataSetChanged();

                Logger.e("response " + tempExpertList.size());

            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {

                if (StringUtils.isNotEmpty(errorMessage)) {
                    DialogUtils.openDialog(getActivity(), errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                        @Override
                        public void onNegativeBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }

                        @Override
                        public void onPositiveBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }
                    });
                }

            }
        }, true);


    }

}
