package com.proclapp.expert;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ExpertModel implements Serializable {

    private final static long serialVersionUID = -8159975969390977848L;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("firstname")
    @Expose
    private String firstname;
    @SerializedName("lastname")
    @Expose
    private String lastname;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("cover_image")
    @Expose
    private String coverImage;
    @SerializedName("about_me")
    @Expose
    private String aboutMe;
    @SerializedName("about_my_profession")
    @Expose
    private String aboutMyProfession;
    @SerializedName("profile_image")
    @Expose
    private String profileImage;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("phone_code")
    @Expose
    private String phoneCode;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("media_type")
    @Expose
    private String mediaType;
    @SerializedName("media_id")
    @Expose
    private String mediaId;
    @SerializedName("password_updated")
    @Expose
    private String passwordUpdated;
    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("member_since")
    @Expose
    private String memberSince;
    @SerializedName("notification_status")
    @Expose
    private String notificationStatus;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("is_expert")
    @Expose
    private String isExpert;
    @SerializedName("profile_updated")
    @Expose
    private String profileUpdated;
    @SerializedName("professional_qualification")
    @Expose
    private String professionalQualification;
    @SerializedName("professional_experience")
    @Expose
    private String professionalExperience;
    @SerializedName("total_followers")
    @Expose
    private String totalFollowers;
    @SerializedName("total_following")
    @Expose
    private Integer totalFollowing;
    private boolean isExpertSelected = false;

    public String getUserId() {
        return userId;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getCoverImage() {
        return coverImage;
    }

    public String getAboutMe() {
        return aboutMe;
    }

    public String getAboutMyProfession() {
        return aboutMyProfession;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public String getGender() {
        return gender;
    }

    public String getPhone() {
        return phone;
    }

    public String getPhoneCode() {
        return phoneCode;
    }

    public String getAddress() {
        return address;
    }

    public String getMediaType() {
        return mediaType;
    }

    public String getMediaId() {
        return mediaId;
    }

    public String getPasswordUpdated() {
        return passwordUpdated;
    }

    public String getToken() {
        return token;
    }

    public String getMemberSince() {
        return memberSince;
    }

    public String getNotificationStatus() {
        return notificationStatus;
    }

    public String getDate() {
        return date;
    }

    public String getStatus() {
        return status;
    }

    public String getIsExpert() {
        return isExpert;
    }

    public String getProfileUpdated() {
        return profileUpdated;
    }

    public String getProfessionalQualification() {
        return professionalQualification;
    }

    public String getProfessionalExperience() {
        return professionalExperience;
    }

    public String getTotalFollowers() {
        return totalFollowers;
    }

    public Integer getTotalFollowing() {
        return totalFollowing;
    }

    public boolean isExpertSelected() {
        return isExpertSelected;
    }

    public void setExpertSelected(boolean expertSelected) {
        isExpertSelected = expertSelected;
    }

    public String getName() {
        return getFirstname() + " " + getLastname();
    }

}
