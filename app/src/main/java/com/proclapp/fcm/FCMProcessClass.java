package com.proclapp.fcm;

import android.content.Context;

import com.google.firebase.iid.FirebaseInstanceId;
import com.proclapp.AppClass;

/**
 * Created by WINDOWS-D20 on 6/30/2016.
 */

public class FCMProcessClass {

    private Context context;

    public FCMProcessClass(Context context) {
        this.context = context;
    }

    /**
     * Get FCM Token from server and Store in Preference
     */
    public void getFCMTokenFromServer() {
        String FcmToken = FirebaseInstanceId.getInstance().getToken();
        AppClass.preferences.storeFCMToken(FcmToken);
    }
}
