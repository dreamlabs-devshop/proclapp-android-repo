/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.proclapp.fcm;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.proclapp.R;
import com.proclapp.home.HomeActivity;
import com.proclapp.utils.Logger;

import org.json.JSONObject;

import java.util.Date;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";


    /**
     * Issues a notification to inform the user that server has sent a message.
     */
    public static NotificationManager notificationManager;

    private static int getNotificationIcon() {
        boolean useWhiteIcon = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.drawable.notification_icon : R.drawable.app_icon;
    }


    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START message_receive]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // TODO(developer): Handle FCM messages here.
        // If the application is in the foreground handle both data and notification messages here.
        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.

        //Log.d(TAG, "Notification Message Body: " + remoteMessage.getNotification().getBody());
        // Log.d(TAG, "From: " + remoteMessage.getFrom());

        Logger.d(TAG + " onMessageReceived: " + remoteMessage.getData().toString());

        Map<String, String> data = remoteMessage.getData();

        showNotification(data);
    }

    // check app is in background or foreground
    public boolean isAppInForeground() {
        boolean isForeground = false;
        try {
            isForeground = new CheckAppBackgroundForegroundAsync().execute(getApplicationContext()).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return isForeground;
    }

    private void showNotification(Map<String, String> data) {


        Date now = new Date();
        long uniqueId = now.getTime();
        int icon = R.drawable.app_icon;
        long when = System.currentTimeMillis();

        Intent notificationIntent = null;
        notificationIntent = new Intent(getApplicationContext(), HomeActivity.class);
        notificationIntent.putExtra("openfrom", "notification");
        notificationIntent.putExtra("data", new JSONObject(data).toString());
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);


        String messageName = getResources().getString(R.string.app_name);

        if (data.get("notification_type").equals("11")) {

            try {
                Intent b_intent = new Intent();
                b_intent.setAction("ReviewAdded");
                sendBroadcast(b_intent);

                JSONObject jsonObject = new JSONObject(data.get("sender"));
                messageName = jsonObject.getString("firstname");

            } catch (Exception e) {
                e.printStackTrace();
                messageName = getResources().getString(R.string.app_name);
            }


        }


        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        String noti_channel_id = "10";
        createNotificationChannel(notificationManager, noti_channel_id);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getApplicationContext(), noti_channel_id)
                .setSmallIcon(getNotificationIcon())
                .setColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary))
                .setContentTitle(messageName)
/*
                .setStyle(new NotificationCompat.BigTextStyle().bigText(data.get("message")))
*/
                .setContentText(data.get("message"))
                .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                .setWhen(when)
                .setAutoCancel(true);

        mBuilder.setDefaults(Notification.DEFAULT_SOUND);
        mBuilder.setContentIntent(pendingIntent);


        notificationManager.notify((int) uniqueId, mBuilder.build());

    }

    private void createNotificationChannel(NotificationManager notificationManager, String noti_channel_id) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(noti_channel_id, getString(R.string.notification), NotificationManager.IMPORTANCE_DEFAULT);
            // Configure the notification channel.
            notificationChannel.setDescription(getString(R.string.notification));
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
            notificationChannel.enableVibration(true);
            notificationManager.createNotificationChannel(notificationChannel);
        }
    }
}