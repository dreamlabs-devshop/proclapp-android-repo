package com.proclapp.fcm;

import android.app.Activity;

import com.google.gson.JsonObject;
import com.proclapp.AppClass;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.DeviceUtils;

import java.util.HashMap;

/**
 * Created by Admin on 21-Apr-16.
 */
public class RegisterFCMToken {

    private Activity activity;

    public RegisterFCMToken(Activity activity) {
        this.activity = activity;

        callRegisterService();
    }

    private void callRegisterService() {

        try {


            HashMap<String, String> requests = new HashMap<>();
            requests.put("user_id", AppClass.preferences.getUserId());
            requests.put("device_type", DeviceUtils.getDeviceType());
            requests.put("device_token", AppClass.preferences.getFCMToken());
            requests.put("device_id", DeviceUtils.getDeviceIdWithoutPermission(activity));


            ApiCall.getInstance().updateDeviceToken(activity, requests, new IApiCallback() {
                @Override
                public void onSuccess(Object data) {

                    try {

                        JsonObject response = (JsonObject) data;

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(boolean isFalseFromService, String errorMessage) {

                }
            }, true);


        } catch (Exception e) {
            e.printStackTrace();
        }


    }

}
