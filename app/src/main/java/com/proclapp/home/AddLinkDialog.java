package com.proclapp.home;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;
import android.util.Patterns;
import android.view.View;
import android.view.WindowManager;

import com.proclapp.R;
import com.proclapp.utils.KeyBoardUtils;
import com.proclapp.utils.StringUtils;

public class AddLinkDialog extends Dialog implements View.OnClickListener {

    private Context context;
    private OnViewClicked clicked;

    private AppCompatEditText et_link;

    public AddLinkDialog(@NonNull Context context, OnViewClicked clicked) {
        super(context);
        this.context = context;
        this.clicked = clicked;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_add_link);

        getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        initViews();

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.tv_ok) {

            if (!StringUtils.isNotEmpty(et_link.getText().toString())) {
                et_link.setError(context.getString(R.string.please_enter_url));
                et_link.requestFocus();
                return;
            }

            if (!Patterns.WEB_URL.matcher(et_link.getText().toString()).matches()) {
                et_link.setError(context.getString(R.string.enter_url));
                et_link.requestFocus();
                return;
            }

            KeyBoardUtils.closeSoftKeyboard((Activity) context);
            clicked.onClick("ok", et_link.getText().toString().trim() + " ");
        }
        dismiss();
    }

    private void initViews() {

        et_link = findViewById(R.id.et_link);
        AppCompatTextView tv_cancle = findViewById(R.id.tv_cancle);
        AppCompatTextView tv_ok = findViewById(R.id.tv_ok);

        tv_cancle.setOnClickListener(this);
        tv_ok.setOnClickListener(this);

    }

    public interface OnViewClicked {

        void onClick(String tag, String link);

    }

}
