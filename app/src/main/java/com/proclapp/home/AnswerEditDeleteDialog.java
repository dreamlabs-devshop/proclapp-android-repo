package com.proclapp.home;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.model.AllPostsModel;
import com.proclapp.utils.AlertDialogInterface;
import com.proclapp.utils.DialogUtils;
import com.proclapp.utils.StringUtils;

public class AnswerEditDeleteDialog extends Dialog implements View.OnClickListener {

    private AllPostsModel advertData;
    private Activity activity;
    private TextView
            txt_edit,
            txt_delete;
    private CardView card_view_option;
    private ImageView img_close;
    private RelativeLayout rl_main;
    private OnItemClick onItemClick;
    private String openFrom = "";

    public AnswerEditDeleteDialog(Activity a, OnItemClick onItemClick) {
        super(a);
        this.activity = a;
        this.onItemClick = onItemClick;
    }

    public AnswerEditDeleteDialog(Activity a, String openFrom, OnItemClick onItemClick) {
        super(a);
        this.activity = a;
        this.openFrom = openFrom;
        this.onItemClick = onItemClick;
    }


    public AnswerEditDeleteDialog(Activity a, AllPostsModel advertData, OnItemClick onItemClick) {
        super(a);
        this.activity = a;
        this.advertData = advertData;
        this.onItemClick = onItemClick;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_answer_edit_delete);


        getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        initViews();


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.rl_main:
            case R.id.img_close:
                dismiss();
                break;

            case R.id.txt_delete:
                dismiss();
                if (advertData != null) {
                    onItemClick.onClick("delete");
                } else {
                    DialogUtils.openDialog(activity, StringUtils.isNotEmpty(openFrom) ? activity.getString(R.string.are_you_sure_you_want_to_delete_this_answer) : activity.getString(R.string.are_you_sure_you_want_to_delete_this_comment), activity.getString(R.string.delete), activity.getString(R.string.cancel), new AlertDialogInterface() {
                        @Override
                        public void onNegativeBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }

                        @Override
                        public void onPositiveBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                            onItemClick.onClick("delete");
                        }
                    });
                }
                break;

            case R.id.txt_edit:
                dismiss();
                onItemClick.onClick("edit");
                break;

        }
    }

    private void initViews() {
        txt_edit = findViewById(R.id.txt_edit);
        txt_delete = findViewById(R.id.txt_delete);
        img_close = findViewById(R.id.img_close);
        rl_main = findViewById(R.id.rl_main);
        card_view_option = findViewById(R.id.card_view_option);
        card_view_option.setBackgroundResource(R.drawable.rounded_bg_option);

        txt_edit.setTypeface(AppClass.lato_medium);
        txt_delete.setTypeface(AppClass.lato_medium);

        txt_edit.setOnClickListener(this);
        txt_delete.setOnClickListener(this);
        img_close.setOnClickListener(this);
        rl_main.setOnClickListener(this);

        if (openFrom.equalsIgnoreCase("comment")) {
            txt_edit.setVisibility(View.VISIBLE);
        }

    }


    public interface OnItemClick {
        void onClick(String tag);
    }

}