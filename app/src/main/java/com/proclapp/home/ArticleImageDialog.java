package com.proclapp.home;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.proclapp.R;
import com.proclapp.utils.ImageLoadUtils;

public class ArticleImageDialog extends Dialog {

    private Activity activity;

    private ImageView img_article;
    private String image_name;

    public ArticleImageDialog(Activity a, String image_name) {
        super(a);
        this.activity = a;
        this.image_name = image_name;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_article_image);

        img_article = findViewById(R.id.img_article);

        getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        //getWindow().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(activity,R.color.color_28black)));
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ImageLoadUtils.imageLoad(activity,
                img_article,
                image_name,
                R.drawable.menu_user_ph);

        img_article.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dismiss();
            }
        });

    }


}