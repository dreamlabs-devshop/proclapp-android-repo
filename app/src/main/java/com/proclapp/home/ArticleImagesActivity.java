package com.proclapp.home;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.proclapp.R;
import com.proclapp.home.adapter.ArticleImagesAdapter;
import com.proclapp.model.AllPostsModel;

import java.util.ArrayList;

public class ArticleImagesActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView img_back;
    private RecyclerView rv_article_images;
    private ArticleImagesAdapter articleImagesAdapter;
    private ArrayList<AllPostsModel.Images> images;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article_images);

        images = (ArrayList<AllPostsModel.Images>) getIntent().getSerializableExtra("images");

        initView();
        initListener();
    }

    private void initView() {

        img_back = findViewById(R.id.img_back);
        rv_article_images = findViewById(R.id.rv_article_images);

        GridLayoutManager layoutManager = new GridLayoutManager(ArticleImagesActivity.this, 3);
        rv_article_images.setLayoutManager(layoutManager);

        articleImagesAdapter = new ArticleImagesAdapter(ArticleImagesActivity.this, images);
        rv_article_images.setAdapter(articleImagesAdapter);
    }

    private void initListener() {

        img_back.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.img_back:

                onBackPressed();

                break;
        }
    }
}
