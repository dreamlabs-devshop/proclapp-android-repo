package com.proclapp.home;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CalendarView;
import android.widget.ImageView;
import android.widget.TextView;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.DateTimeUtils;

import java.util.Calendar;

public class CarryOverDialog extends Dialog {

    private String date = "";
    private Activity activity;
    private TextView txt_select_date, tv_ok;
    private ImageView img_close;
    private CardView card_view_carry_over;
    private CalendarView calendar_carryover;
    private String postId = "";
    private int pos;
    private OnCarryOverSuccessListener successListener;

    public CarryOverDialog(Activity a, String postId, int pos, OnCarryOverSuccessListener successListener) {
        super(a);
        this.activity = a;
        this.postId = postId;
        this.pos = pos;
        this.successListener = successListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_carryover);

        txt_select_date = findViewById(R.id.txt_select_date);
        tv_ok = findViewById(R.id.tv_ok);
        img_close = findViewById(R.id.img_close);
        calendar_carryover = findViewById(R.id.calendar_carryover);
        card_view_carry_over = findViewById(R.id.card_view_carry_over);
        card_view_carry_over.setBackgroundResource(R.drawable.rounded_bg_carryover);

        getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;


        calendar_carryover.setFirstDayOfWeek(Calendar.SUNDAY);
        calendar_carryover.setMinDate(System.currentTimeMillis());
        txt_select_date.setTypeface(AppClass.lato_medium);

        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        tv_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                carryOverPost();
            }
        });

        date = DateTimeUtils.getDateInStringFromMilis(calendar_carryover.getDate(), "yyyy-Mm-dd");
        calendar_carryover.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView calendarView, int i, int i1, int i2) {
                date = DateTimeUtils.changeDateTimeFormat("yyyy-M-d", "yyyy-MM-dd", i + "-" + (i1 + 1) + "-" + i2);
            }
        });

    }


    private void carryOverPost() {

        ApiCall.getInstance().carryoverPost(activity, AppClass.preferences.getUserId(), postId, date, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {
                dismiss();
                AppClass.snackBarView.snackBarShow(activity, (String) data);
                successListener.onSuccess(pos);
            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {
                AppClass.snackBarView.snackBarShow(activity, errorMessage);
            }
        }, true);

    }

    public interface OnCarryOverSuccessListener {
        void onSuccess(int pos);
    }


}