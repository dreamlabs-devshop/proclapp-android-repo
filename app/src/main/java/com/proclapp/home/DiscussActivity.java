package com.proclapp.home;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ScaleDrawable;
import android.net.Uri;
import android.os.Bundle;

import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.home.adapter.ArticleImagesAdapter;
import com.proclapp.home.adapter.DiscussionAdapter;
import com.proclapp.home.adapter.PostArticleImagesAdapter;
import com.proclapp.home.question.SortByForAnswerDialog;
import com.proclapp.interfaces.RecyclerClickListener;
import com.proclapp.linkpreview.LinkPreviewCallback;
import com.proclapp.linkpreview.SourceContent;
import com.proclapp.linkpreview.TextCrawler;
import com.proclapp.model.AllPostsModel;
import com.proclapp.model.AnswerModel;
import com.proclapp.model.PostAdvUploadModel;
import com.proclapp.profile.OtherUserProfileActivity;
import com.proclapp.profile.ProfileActivity;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.AlertDialogInterface;
import com.proclapp.utils.DateTimeUtils;
import com.proclapp.utils.DialogUtils;
import com.proclapp.utils.ExpandableTextView;
import com.proclapp.utils.ImageLoadUtils;
import com.proclapp.utils.ImagePickerUtils.ImagePickerBottomSheet;
import com.proclapp.utils.KeyBoardUtils;
import com.proclapp.utils.Logger;
import com.proclapp.utils.StringUtils;
import com.proclapp.utils.Utils;

import org.apache.commons.lang3.SystemUtils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

public class DiscussActivity extends AppCompatActivity implements View.OnClickListener, LinkPreviewCallback {

    public static boolean isDiscussionPost = false;

    private ImageView
            img_back,
            img_upload_post,
            img_upload_post_delete,
            img_photos,
            img_article,
            iv_single_image,
            img_article_shadow,
            img_follow,
            img_link;
    private TextView
            txt_user_name,
            txt_user_place,
            txt_date,
            txt_image_count,
    /* txt_description,*/
    txt_discuss_tool,
            txt_discuss,
            txt_decline,
            txt_carry_over,
            txt_flash,
            txt_question_value,
            txt_comments_count,
            txt_share_count,
            txt_total_answer_count,
            tv_seemore,

    txt_like_count;
    private FrameLayout fl_image;
    private EditText edt_write_ur_answer;
    private CircleImageView img_user_profile;
    private AllPostsModel articleData;
    private DisplayMetrics metrics;
    private RecyclerView
            rv_images,
            rv_all_images;
    private ArticleImagesAdapter articleImagesAdapter;
    private ArrayList<PostAdvUploadModel> imagesList = new ArrayList<>();
    private PostArticleImagesAdapter postArticleImagesAdapter;
    private RecyclerView rv_all_comments;
    private ArrayList<AnswerModel> discussionList = new ArrayList<>();

    private LinearLayout
            ll_user_information,
            ll_discuss,
            ll_decline,
            ll_carryover,
            ll_flash;

    private ExpandableTextView txt_description;
    private ImageView img_sort_answer;
    private TextCrawler textCrawler;
    private ViewGroup dropPreview;
    private Bitmap[] currentImageSet;
    private int countBigImages = 0;
    private String currentTitle = "", currentUrl = "", currentCannonicalUrl = "", currentDescription = "";
    private LinearLayout linearLayout;
    private String postId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discuss);
        Utils.setStatusBarColor(getWindow(), this, R.color.white);
        initView();
        increaseDrawableSizes();
        initListener();


        setUpDiscussionList();
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (isDiscussionPost)
            callAPIGetDiscussion();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.img_back:
                onBackPressed();
                break;

            case R.id.fl_image:
                startActivity(new Intent(DiscussActivity.this, ArticleImagesActivity.class)
                        .putExtra("images", articleData.getImages()));
                break;

            case R.id.drop_preview:
                if (articleData.getPostLink() != null && StringUtils.isNotEmpty(articleData.getPostLink().getLink())) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(articleData.getPostLink().getLink()));
                    startActivity(browserIntent);
                }
                break;

            case R.id.img_photos:

                new ImagePickerBottomSheet(DiscussActivity.this, new ImagePickerBottomSheet.ImagePickListener() {
                    @Override
                    public void onPickImage(File imageFile) {

                        rv_images.setVisibility(View.VISIBLE);
                        PostAdvUploadModel model = new PostAdvUploadModel();
                        model.setImage(imageFile);

                        imagesList.add(model);
                        postArticleImagesAdapter.notifyDataSetChanged();

                    }
                }).show(getSupportFragmentManager(), "");

                break;


            case R.id.txt_discuss_tool:
            case R.id.ll_discuss:
                startActivity(new Intent(this, PostDiscussionActivity.class)
                        .putExtra("postId", articleData.getPostId()));
                break;

            case R.id.img_follow:

                KeyBoardUtils.closeSoftKeyboard(this);
                callAPIChangeFollowUnfollowStatus(img_follow.isSelected() ? "0" : "1", articleData.getAuthorId());
                break;

            case R.id.img_user_profile:
            case R.id.ll_user_information:
                if (AppClass.preferences.getUserId().equalsIgnoreCase(articleData.getAuthorId())) {
                    startActivity(new Intent(this, ProfileActivity.class));

                } else {

                    startActivity(new Intent(this, OtherUserProfileActivity.class)
                            .putExtra("user_id", articleData.getAuthorId()));
                }

                break;

            case R.id.txt_like_count:
                callAPILikeUnlike(txt_like_count.isSelected() ? "0" : "1");
                break;

            case R.id.ll_carryover:
                if (view.isSelected()) {
                    AppClass.snackBarView.snackBarShow(this, "Already carryover");
                } else {
                    new CarryOverDialog(this,
                            articleData.getPostId(),
                            0, (carryOverPos) -> {


                    }).show();
                }
                break;

            case R.id.ll_decline:
                callAPIDeclinePost();
                break;

            case R.id.img_sort_answer:

                new SortByForAnswerDialog(this,
                        articleData.getPostId(),
                        new SortByForAnswerDialog.UpdateAnswerListBySorting() {
                            @Override
                            public void updateData(ArrayList<AnswerModel> answer_lists) {

                                if (answer_lists != null) {
                                    discussionList.clear();
                                    discussionList.addAll(answer_lists);

                                    if (discussionList.size() > 1) {
                                        txt_total_answer_count.setText(String.format("%s %s", String.valueOf(discussionList.size()), getString(R.string.comments)));
                                    } else {
                                        txt_total_answer_count.setText(String.format("%s %s", String.valueOf(discussionList.size()), getString(R.string.comment)));
                                    }

                                    rv_all_comments.setVisibility(View.VISIBLE);
                                    rv_all_comments.getAdapter().notifyDataSetChanged();
                                }

                                if (answer_lists.size() == 0) {
                                    rv_all_comments.setVisibility(View.GONE);
                                    txt_total_answer_count.setText(String.format("0 %s", getString(R.string.comments)));
                                    img_sort_answer.setVisibility(View.INVISIBLE);
                                }
                            }
                        }).show();

                break;


            case R.id.ll_flash:

                startActivity(new Intent(this, FlashActivity.class)
                        .putExtra("post_id", articleData.getPostId()));

                break;


            case R.id.tv_seemore:
                tv_seemore.setText(txt_description.isExpanded() ? R.string.read_more : R.string.read_less);
                txt_description.toggle();
                break;


            case R.id.iv_single_image:
                startActivity(new Intent(this, ImagePagerActivity.class)
                        .putExtra("imagesUrl", articleData.getImages())
                        .putExtra("clickedpos", 0));
                break;


        }
    }

    private void increaseDrawableSizes() {
        txt_like_count.setCompoundDrawables(getDrawableAndResize(R.drawable.selector_like_post), null, null, null);
        txt_comments_count.setCompoundDrawables(getDrawableAndResize(R.drawable.comments), null, null, null);
        txt_discuss.setCompoundDrawables(getDrawableAndResize(R.drawable.discuss), null, null, null);
        txt_decline.setCompoundDrawables(getDrawableAndResize(R.drawable.decline), null, null, null);
        txt_carry_over.setCompoundDrawables(getDrawableAndResize(R.drawable.carryover), null, null, null);
        txt_flash.setCompoundDrawables(getDrawableAndResize(R.drawable.flash), null, null, null);
    }

    private Drawable getDrawableAndResize(int drawableId) {
        Drawable drawable = ContextCompat.getDrawable(this, drawableId);
        drawable.setBounds(0,
                0,
                (int) (drawable.getIntrinsicWidth() * 1.4),
                (int) (drawable.getIntrinsicHeight() * 1.4));

        return drawable;
    }


    private void initView() {


        metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        img_back = findViewById(R.id.img_back);
        img_upload_post = findViewById(R.id.img_upload_post);
        img_upload_post_delete = findViewById(R.id.img_upload_post_delete);
        img_photos = findViewById(R.id.img_photos);
        img_link = findViewById(R.id.img_link);
        img_follow = findViewById(R.id.img_follow);
        img_sort_answer = findViewById(R.id.img_sort_answer);
        img_user_profile = findViewById(R.id.img_user_profile);
        img_article = findViewById(R.id.img_article);
        img_article_shadow = findViewById(R.id.img_article_shadow);
        iv_single_image = findViewById(R.id.iv_single_image);

        txt_discuss_tool = findViewById(R.id.txt_discuss_tool);
        txt_total_answer_count = findViewById(R.id.txt_total_answer_count);
        txt_user_name = findViewById(R.id.txt_user_name);
        txt_user_place = findViewById(R.id.txt_user_place);
        txt_date = findViewById(R.id.txt_date);
        txt_question_value = findViewById(R.id.txt_question_value);
        txt_image_count = findViewById(R.id.txt_image_count);
        txt_description = findViewById(R.id.txt_description);
        txt_comments_count = findViewById(R.id.txt_comments_count);
        txt_share_count = findViewById(R.id.txt_share_count);
        txt_like_count = findViewById(R.id.txt_like_count);
        txt_discuss = findViewById(R.id.txt_discuss);
        txt_decline = findViewById(R.id.txt_decline);
        txt_carry_over = findViewById(R.id.txt_carryover);
        txt_flash = findViewById(R.id.txt_flash);
        tv_seemore = findViewById(R.id.tv_seemore);


        rv_all_comments = findViewById(R.id.rv_all_comments);
        rv_images = findViewById(R.id.rv_images);
        rv_all_images = findViewById(R.id.rv_all_images);

        ll_user_information = findViewById(R.id.ll_user_information);
        ll_discuss = findViewById(R.id.ll_discuss);
        ll_decline = findViewById(R.id.ll_decline);
        ll_carryover = findViewById(R.id.ll_carryover);
        ll_flash = findViewById(R.id.ll_flash);
        dropPreview = findViewById(R.id.drop_preview);

        edt_write_ur_answer = findViewById(R.id.edt_write_ur_answer);

        fl_image = findViewById(R.id.fl_image);

        GridLayoutManager layoutManager = new GridLayoutManager(DiscussActivity.this, 3);
        rv_all_images.setLayoutManager(layoutManager);

        rv_images.setLayoutManager(new LinearLayoutManager(DiscussActivity.this
                , LinearLayoutManager.HORIZONTAL, false));

        postArticleImagesAdapter = new PostArticleImagesAdapter(DiscussActivity.this, imagesList);
        rv_images.setAdapter(postArticleImagesAdapter);


        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams((metrics.widthPixels * 124) / 480, (metrics.heightPixels * 124) / 800);
        img_article.setLayoutParams(params);
        img_article.setScaleType(ImageView.ScaleType.CENTER_CROP);
        img_article_shadow.setLayoutParams(params);

        articleData = (AllPostsModel) getIntent().getSerializableExtra("articleDetails");
        postId = articleData.getPostId();

        if (getIntent().hasExtra("notiType")) {


            startActivity(new Intent(DiscussActivity.this, SeeDiscussionActivity.class)
                    .putExtra("postId", postId)
                    .putExtra("answerLists", getIntent().getStringExtra("answerId")));
        }

        setData();
    }

    private void setUpDiscussionList() {

        rv_all_comments.setLayoutManager(new LinearLayoutManager(this));
        rv_all_comments.setAdapter(new DiscussionAdapter(this, discussionList, new RecyclerClickListener() {
            @Override
            public void onItemClick(int pos, String tag) {

                if (tag.equalsIgnoreCase("edit")) {
                    startActivity(new Intent(DiscussActivity.this, PostDiscussionActivity.class)
                            .putExtra("postId", articleData.getPostId())
                            .putExtra("discuss", discussionList.get(pos)));
                }

            }
        }));

        callAPIGetDiscussion();
    }


    private void initListener() {

        img_back.setOnClickListener(this);
        img_upload_post.setOnClickListener(this);
        img_upload_post_delete.setOnClickListener(this);
        img_photos.setOnClickListener(this);
        img_photos.setOnClickListener(this);
        img_follow.setOnClickListener(this);
        img_sort_answer.setOnClickListener(this);
        img_upload_post_delete.setOnClickListener(this);
        img_link.setOnClickListener(this);
        fl_image.setOnClickListener(this);
        txt_discuss_tool.setOnClickListener(this);
        txt_like_count.setOnClickListener(this);
        ll_user_information.setOnClickListener(this);
        ll_discuss.setOnClickListener(this);
        ll_decline.setOnClickListener(this);
        ll_flash.setOnClickListener(this);
        ll_carryover.setOnClickListener(this);
        dropPreview.setOnClickListener(this);
        tv_seemore.setOnClickListener(this);
        iv_single_image.setOnClickListener(this);
    }

    private boolean validation() {

        boolean isValid = true;

        if (edt_write_ur_answer.getText().toString().trim().isEmpty()) {
            isValid = false;
            AppClass.snackBarView.snackBarShow(DiscussActivity.this, getResources().getString(R.string.vanswer), ContextCompat.getColor(DiscussActivity.this, R.color.red));
            edt_write_ur_answer.requestFocus();
        }

        return isValid;
    }

    private void setData() {

        try {
            txt_user_name.setText(articleData.getAuthor());
            txt_question_value.setText(Html.fromHtml(articleData.getPostTitle()));
            txt_description.setText(Html.fromHtml(articleData.getPostDescription()));
            //txt_date.setText(DateTimeUtils.changeDateTimeFormat("yyyy-MM-dd HH:mm:ss", "dd MMM", articleData.getPostDate()));
            txt_date.setText(DateTimeUtils.calculateTimeBetweenTwoDates(this, articleData.getPostDate(), false));
            txt_user_place.setText(articleData.getProfessionalQualification());

            if (StringUtils.isNotEmpty(articleData.getPostLikeCount()))
                txt_like_count.setText(articleData.getPostLikeCount());
            else
                txt_like_count.setText("0");

            if (StringUtils.isNotEmpty(articleData.getPostShareCount()))
                txt_share_count.setText(articleData.getPostShareCount());
            else
                txt_share_count.setText("0");


            if (StringUtils.isNotEmpty(articleData.getTotal_answers()))
                txt_comments_count.setText(articleData.getTotal_answers());
            else
                txt_comments_count.setText("0");

            if (articleData.getIsPostLiked().equalsIgnoreCase("true")) {
                txt_like_count.setSelected(true);
            } else {
                txt_like_count.setSelected(false);
            }


            if (articleData.getIs_carryover_expire().equalsIgnoreCase("true")) {
                ll_carryover.setVisibility(View.GONE);
            } else {
                ll_carryover.setVisibility(View.VISIBLE);
                if (articleData.getIs_carryover().equalsIgnoreCase("true")) {
                    ll_carryover.setSelected(true);
                } else {
                    ll_carryover.setSelected(false);
                }

            }


            /*txt_description.post(new Runnable() {
                @Override
                public void run() {
                    if (txt_description.getLineCount() <= 6) {
                        tv_seemore.setVisibility(View.GONE);
                    }

                }
            });*/

            if (articleData.getImages().size() > 0) {

                if (articleData.getImages().get(0).getImage_name().isEmpty()) {

                    img_article.setVisibility(View.GONE);
                    img_article_shadow.setVisibility(View.GONE);
                    txt_image_count.setVisibility(View.GONE);

                } else {

                    ImageLoadUtils.imageLoad(DiscussActivity.this,
                            img_article,
                            articleData.getImages().get(0).getImage_name());

                    img_article.setVisibility(View.VISIBLE);

                    if (articleData.getImages().size() > 1) {
                        img_article_shadow.setVisibility(View.VISIBLE);
                        txt_image_count.setVisibility(View.VISIBLE);
                        txt_image_count.setText("+" + (articleData.getImages().size() - 1));
                    } else {
                        img_article_shadow.setVisibility(View.GONE);
                        txt_image_count.setVisibility(View.GONE);
                    }
                }
            } else {
                img_article_shadow.setVisibility(View.GONE);
                txt_image_count.setVisibility(View.GONE);
                img_article.setVisibility(View.GONE);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

        if (articleData.getIs_following().equalsIgnoreCase("true")) {
            img_follow.setSelected(true);
        } else {
            img_follow.setSelected(false);
        }

        textCrawler = new TextCrawler();

        if (articleData.getPostLink() != null &&
                !StringUtils.isNotEmpty(articleData.getPostLink().getLink_title())) {
            addLink(articleData.getPostLink().getLink());
        } else {
            View mainView = getLayoutInflater().inflate(R.layout.link_preview_main_view, null);
//            linearLayout = mainView.findViewById(R.id.external);
            View linkPreview = getLayoutInflater().inflate(R.layout.layout_link_preview, null);
            dropPreview.removeAllViews();
            dropPreview.addView(linkPreview);
            AppCompatTextView tvLinkPreviewTitle = linkPreview.findViewById(R.id.tv_linkpreview_title);
            AppCompatTextView tvLinkPreviewDescription = linkPreview.findViewById(R.id.tv_linkpreview_description);
            AppCompatTextView tvLinkPreviewLink = linkPreview.findViewById(R.id.tv_link);
            AppCompatImageView ivLinkPreviewImage = linkPreview.findViewById(R.id.iv_url_image);
            AppCompatImageView ivLinkPreviewClose = linkPreview.findViewById(R.id.iv_close);

            tvLinkPreviewTitle.setText(articleData.getPostLink().getLink_title());
            tvLinkPreviewDescription.setText(articleData.getPostLink().getLink_description());
            tvLinkPreviewLink.setText(articleData.getPostLink().getLink());

            ivLinkPreviewClose.setVisibility(View.GONE);

            if (StringUtils.isNotEmpty(articleData.getPostLink().getLink_image())) {

                ImageLoadUtils.imageLoad(this,
                        ivLinkPreviewImage,
                        articleData.getPostLink().getLink_image());

            }
        }


        if (articleData.getImages().size() == 1) {

            iv_single_image.setVisibility(View.VISIBLE);
            rv_all_images.setVisibility(View.GONE);

            ImageLoadUtils.imageLoad(this,
                    iv_single_image,
                    articleData.getImages().get(0).getImage_name(),
                    R.drawable.upload_img_ph);

        } else {
            iv_single_image.setVisibility(View.GONE);
            rv_all_images.setVisibility(View.VISIBLE);
            articleImagesAdapter = new ArticleImagesAdapter(DiscussActivity.this, articleData.getImages());
            rv_all_images.setAdapter(articleImagesAdapter);
        }
        ImageLoadUtils.imageLoad(this,
                img_user_profile,
                articleData.getProfileImage(),
                R.drawable.user_img_ph);
    }

    private void callAPIGetDiscussion() {

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("post_id", postId);

        ApiCall.getInstance().getAnswerList(DiscussActivity.this, request, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {

                discussionList.clear();
                discussionList.addAll((ArrayList<AnswerModel>) data);

                if (discussionList.size() > 1) {
                    txt_total_answer_count.setText(String.format("%s %s", String.valueOf(discussionList.size()), getString(R.string.comments)));
                } else {
                    txt_total_answer_count.setText(String.format("%s %s", String.valueOf(discussionList.size()), getString(R.string.comment)));
                }

                img_sort_answer.setVisibility(View.VISIBLE);
                rv_all_comments.setVisibility(View.VISIBLE);
                rv_all_comments.getAdapter().notifyDataSetChanged();

                isDiscussionPost = false;

            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {

                rv_all_comments.setVisibility(View.GONE);
                txt_total_answer_count.setText(String.format("0 %s", getString(R.string.comments)));
                img_sort_answer.setVisibility(View.INVISIBLE);

            }


        }, true);

    }

    private void callAPIChangeFollowUnfollowStatus(final String type, String otherUserId) {

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("follower_id", otherUserId);
        request.put("type", type);

        ApiCall.getInstance().changeFollowUnfollowStatus(this, request, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {

                if (type.equalsIgnoreCase("1")) {
                    img_follow.setSelected(true);
                } else {
                    img_follow.setSelected(false);
                }

            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {

            }
        }, true);

    }

    private void callAPILikeUnlike(final String status) {

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("post_id", articleData.getPostId());
        request.put("like_unlike", status);

        ApiCall.getInstance().likeUnlike(this, request, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {
                String response = (String) data;

                int count;

                if (status.equalsIgnoreCase("0")) {
                    txt_like_count.setSelected(false);
                    count = Integer.parseInt(articleData.getPostLikeCount()) - 1;

                } else {
                    txt_like_count.setSelected(true);
                    count = Integer.parseInt(articleData.getPostLikeCount()) + 1;


                }
                articleData.setPostLikeCount(String.valueOf(count));
                txt_like_count.setText(String.valueOf(count));
            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {
                DialogUtils.openDialog(DiscussActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                    @Override
                    public void onNegativeBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }

                    @Override
                    public void onPositiveBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }
                });
            }
        }, true);

    }

    private void callAPIDeclinePost() {

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("post_id", articleData.getPostId());
        request.put("status", "1");

        Logger.d("request:" + request);

        ApiCall.getInstance().declinePost(this,
                request, new IApiCallback() {
                    @Override
                    public void onSuccess(Object data) {

                        finish();

                    }

                    @Override
                    public void onFailure(boolean isFalseFromService, String errorMessage) {


                        DialogUtils.openDialog(DiscussActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                            @Override
                            public void onNegativeBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }

                            @Override
                            public void onPositiveBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }
                        });
                    }


                }, true);

    }


    private void showHideImage(View image, View parent, boolean show) {
        if (show) {
            image.setVisibility(View.VISIBLE);
            parent.setPadding(5, 5, 5, 5);
            parent.setLayoutParams(new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 2f));
        } else {
            image.setVisibility(View.GONE);
            parent.setPadding(5, 5, 5, 5);
            parent.setLayoutParams(new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 3f));
        }
    }


    private void addLink(String link) {
        if (StringUtils.isNotEmpty(link))
            textCrawler.makePreview(this, link.startsWith("http") ? link : "https://" + link);

    }

    @Override
    public void onPre() {
        KeyBoardUtils.closeSoftKeyboard(this);

        currentImageSet = null;
        currentTitle = currentDescription = currentUrl = currentCannonicalUrl = "";


        View mainView = getLayoutInflater().inflate(R.layout.link_preview_main_view, null);

        linearLayout = mainView.findViewById(R.id.external);

        getLayoutInflater().inflate(R.layout.loading, linearLayout);

        dropPreview.addView(mainView);
    }

    @Override
    public void onPos(SourceContent sourceContent, boolean b) {
        linearLayout.removeAllViews();

        if (b || sourceContent.getFinalUrl().equals("")) {

            View failed = getLayoutInflater().inflate(R.layout.failed, linearLayout);

            TextView titleTextView = failed.findViewById(R.id.text);
            titleTextView.setText(String.format("%s\n%s", getString(R.string.failed_preview), sourceContent.getFinalUrl()));

            failed.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
//                    dropPreview.removeAllViews();
                    currentTitle = currentDescription = currentUrl = currentCannonicalUrl = "";
                }
            });

        } else {

            currentImageSet = new Bitmap[sourceContent.getImages().size()];

            View linkPreview = getLayoutInflater().inflate(R.layout.layout_link_preview, linearLayout);

            AppCompatTextView tvLinkPreviewTitle = linkPreview.findViewById(R.id.tv_linkpreview_title);
            AppCompatTextView tvLinkPreviewDescription = linkPreview.findViewById(R.id.tv_linkpreview_description);
            AppCompatTextView tvLinkPreviewLink = linkPreview.findViewById(R.id.tv_link);
            AppCompatImageView ivLinkPreviewImage = linkPreview.findViewById(R.id.iv_url_image);
            AppCompatImageView ivLinkPreviewClose = linkPreview.findViewById(R.id.iv_close);

            tvLinkPreviewTitle.setText(sourceContent.getTitle());
            tvLinkPreviewDescription.setText(sourceContent.getDescription());
            tvLinkPreviewLink.setText(sourceContent.getCannonicalUrl());

            ivLinkPreviewClose.setVisibility(View.GONE);

            if (!isFinishing() &&
                    sourceContent.getImages().size() > 0) {

                ImageLoadUtils.imageLoad(this,
                        ivLinkPreviewImage,
                        sourceContent.getImages().get(0));

            }

        }

        currentTitle = sourceContent.getTitle();
        currentDescription = sourceContent.getDescription();
        currentUrl = sourceContent.getUrl();
        currentCannonicalUrl = sourceContent.getCannonicalUrl();
    }

}
