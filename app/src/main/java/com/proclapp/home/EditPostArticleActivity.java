package com.proclapp.home;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.home.adapter.EditPostArticleImagesAdapter;
import com.proclapp.linkpreview.LinkPreviewCallback;
import com.proclapp.linkpreview.SourceContent;
import com.proclapp.linkpreview.TextCrawler;
import com.proclapp.model.AllPostsModel;
import com.proclapp.model.CategoryModel;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.AlertDialogInterface;
import com.proclapp.utils.DialogUtils;
import com.proclapp.utils.ImageLoadUtils;
import com.proclapp.utils.ImagePickerUtils.ImagePickerBottomSheet;
import com.proclapp.utils.KeyBoardUtils;
import com.proclapp.utils.Logger;
import com.proclapp.utils.StaticData;
import com.proclapp.utils.StringUtils;
import com.proclapp.utils.Utils;
import com.vincent.filepicker.Constant;
import com.vincent.filepicker.activity.NormalFilePickActivity;
import com.vincent.filepicker.filter.entity.NormalFile;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.RequestBody;

public class EditPostArticleActivity extends AppCompatActivity implements View.OnClickListener, LinkPreviewCallback {

    private static final int PDF_REQUEST = 1;
    ArrayList<CategoryModel> categoryList = new ArrayList<>();
    private ImageView img_back,
            img_photos,
            img_link,
            img_add_user,
            img_dropdown,
            img_upload_post,
            img_upload_post_delete;
    private TextView txt_create_article,
            txt_post;
    private Spinner spinner_select_category;
    private EditText edt_article,
            edt_title,
            edt_add_additional_info;

    private FrameLayout fl_image;

    private String type = "";
    private RelativeLayout rl_spinner;
    private View view;

    private DisplayMetrics metrics;
    private String pdfPath = "";
    private File imagePath;
    private File docPath;

    private ArrayAdapter<String> spinnerArrayAdapter;

    private EditPostArticleImagesAdapter editPostArticleImagesAdapter;
    private RecyclerView rv_images;

    // doc file
    private RelativeLayout rl_doc;
    private ImageView
            img_doc_upload_post,
            img_doc_upload_post_delete;
    private TextView txt_doc_name;
    private String post_category = "";

    private AllPostsModel postData;
    private ArrayList<AllPostsModel.Images> imagesList = new ArrayList<>();

    private TextCrawler textCrawler;
    private ViewGroup dropPreview;
    private Bitmap[] currentImageSet;
    private int countBigImages = 0;
    private String currentTitle = "", currentUrl = "", currentCannonicalUrl = "", currentDescription = "", currentImageUrl = "";
    private LinearLayout linearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_post_article);

        Utils.setStatusBarColor(getWindow(), this, R.color.white);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        textCrawler = new TextCrawler();
        type = getIntent().getStringExtra("type");
        postData = (AllPostsModel) getIntent().getSerializableExtra("postData");

        initView();
        initStyle();
        initListener();
        callAPICategoryList();


    }

    private void initView() {

        metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        img_back = findViewById(R.id.img_back);
        img_photos = findViewById(R.id.img_photos);
        img_link = findViewById(R.id.img_link);
        img_add_user = findViewById(R.id.img_add_user);
        img_dropdown = findViewById(R.id.img_dropdown);
        img_upload_post = findViewById(R.id.img_upload_post);
        img_upload_post_delete = findViewById(R.id.img_upload_post_delete);

        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams((metrics.widthPixels * 132) / 480, (metrics.heightPixels * 132) / 800);
        img_upload_post.setLayoutParams(layoutParams);
        layoutParams.setMargins(15, 15, 15, 15);
        img_upload_post.setScaleType(ImageView.ScaleType.CENTER_CROP);

        rl_spinner = findViewById(R.id.rl_spinner);
        view = findViewById(R.id.view);
        fl_image = findViewById(R.id.fl_image);

        rv_images = findViewById(R.id.rv_images);
        rv_images.setLayoutManager(new LinearLayoutManager(EditPostArticleActivity.this
                , LinearLayoutManager.HORIZONTAL, false));

        txt_create_article = findViewById(R.id.txt_create_article);
        txt_post = findViewById(R.id.txt_post);

        spinner_select_category = findViewById(R.id.spinner_select_category);

        edt_article = findViewById(R.id.edt_article);
        edt_title = findViewById(R.id.edt_title);
        edt_add_additional_info = findViewById(R.id.edt_add_additional_info);

        dropPreview = findViewById(R.id.drop_preview);

        rl_doc = findViewById(R.id.rl_doc);
        img_doc_upload_post = findViewById(R.id.img_doc_upload_post);
        img_doc_upload_post_delete = findViewById(R.id.img_doc_upload_post_delete);
        txt_doc_name = findViewById(R.id.txt_doc_name);

        if (type.equalsIgnoreCase(StaticData.question)) {

            rl_spinner.setVisibility(View.VISIBLE);
            view.setVisibility(View.VISIBLE);
            img_photos.setVisibility(View.GONE);
            edt_title.setVisibility(View.GONE);
            edt_add_additional_info.setVisibility(View.VISIBLE);
            edt_article.setHint(getString(R.string.write_ur_question));
            img_link.setVisibility(View.GONE);
            txt_create_article.setText(getResources().getString(R.string.ask_question));


            edt_article.setText(postData.getPostTitle());

            if (StringUtils.isNotEmpty(postData.getPostDescription()))
                edt_add_additional_info.setText(postData.getPostDescription());

            post_category = postData.getCategoriesId();

        } else if (type.equalsIgnoreCase(StaticData.article)) {

            rl_spinner.setVisibility(View.VISIBLE);
            view.setVisibility(View.VISIBLE);
            edt_title.setVisibility(View.VISIBLE);
            img_photos.setVisibility(View.VISIBLE);
            edt_article.setHint(getString(R.string.write_ur_article_here));
            edt_add_additional_info.setVisibility(View.GONE);
            img_link.setVisibility(View.VISIBLE);
            txt_create_article.setText(getString(R.string.create_article));

            edt_title.setText(postData.getPostTitle());
            edt_article.setText(Html.fromHtml(postData.getPostDescription()));

            post_category = postData.getCategoriesId();

            try {

                imagesList.addAll(postData.getImages());
                editPostArticleImagesAdapter = new EditPostArticleImagesAdapter(EditPostArticleActivity.this,
                        postData.getImages(),
                        new EditPostArticleImagesAdapter.UpdateListOnDelete() {
                            @Override
                            public void updateList(ArrayList<AllPostsModel.Images> images_list) {

                                Logger.d("list1===" + images_list.size());

                                //imagesList.clear();
                                imagesList = new ArrayList<>();
                                imagesList.addAll(images_list);

                                Logger.d("list2===" + imagesList.size());
                                editPostArticleImagesAdapter.notifyDataSetChanged();
                            }

                        });
                rv_images.setAdapter(editPostArticleImagesAdapter);


                rv_images.setVisibility(View.VISIBLE);

                if (StringUtils.isNotEmpty(postData.getPostLink().getLink()))
                    addLink(postData.getPostLink().getLink());


            } catch (Exception e) {
                e.printStackTrace();
            }


        } else if (type.equalsIgnoreCase(StaticData.research_paper)) {

            rl_spinner.setVisibility(View.VISIBLE);
            view.setVisibility(View.VISIBLE);
            img_photos.setVisibility(View.GONE);
            edt_title.setVisibility(View.GONE);
            edt_article.setHint(getString(R.string.write_ur_title));
            edt_add_additional_info.setVisibility(View.GONE);
            img_link.setVisibility(View.VISIBLE);

            txt_create_article.setText(getResources().getString(R.string.upload_research_paper));

            edt_article.setText(Html.fromHtml(postData.getPostTitle()));
            post_category = postData.getCategoriesId();

            rl_doc.setVisibility(View.VISIBLE);
            txt_doc_name.setText(postData.getFiles().getOriFileName());
            img_doc_upload_post.setImageResource(R.drawable.pdf);

        }

    }

    private void initStyle() {

        txt_create_article.setTypeface(AppClass.lato_regular);
        txt_post.setTypeface(AppClass.lato_regular);
        edt_article.setTypeface(AppClass.lato_regular);
        edt_add_additional_info.setTypeface(AppClass.lato_regular);
        edt_title.setTypeface(AppClass.lato_regular);
        txt_doc_name.setTypeface(AppClass.lato_regular);

    }

    private void initListener() {

        img_back.setOnClickListener(this);
        img_photos.setOnClickListener(this);
        img_link.setOnClickListener(this);
        img_add_user.setOnClickListener(this);
        txt_post.setOnClickListener(this);
        img_dropdown.setOnClickListener(this);
        img_upload_post_delete.setOnClickListener(this);
        img_doc_upload_post_delete.setOnClickListener(this);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        EditPostArticleImagesAdapter.builder.setLength(0);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.img_back:

                onBackPressed();

                break;

            case R.id.img_photos:

                new ImagePickerBottomSheet(EditPostArticleActivity.this, false, new ImagePickerBottomSheet.ImagePickListener() {
                    @Override
                    public void onPickImage(File imageFile) {

                        rv_images.setVisibility(View.VISIBLE);
                        AllPostsModel.Images model = new AllPostsModel.Images();
                        model.setImage(imageFile);

                        imagesList.add(model);

                        editPostArticleImagesAdapter.updateList(imagesList);
                        editPostArticleImagesAdapter.notifyDataSetChanged();

                    }
                }).show(getSupportFragmentManager(), "");

                break;

            case R.id.img_upload_post_delete:

                fl_image.setVisibility(View.GONE);
                img_upload_post.setImageResource(R.drawable.upload_img_ph);

                break;

            case R.id.img_doc_upload_post_delete:

                rl_doc.setVisibility(View.GONE);
                txt_doc_name.setText("");

                break;

            case R.id.img_link:

                if (type.equalsIgnoreCase(StaticData.research_paper)) {
                    selectPdf();
                } else {
                    new AddLinkDialog(this, new AddLinkDialog.OnViewClicked() {
                        @Override
                        public void onClick(String tag, String link) {

                            addLink(link);

                        }
                    }).show();
                }

                break;

            case R.id.img_add_user:

                break;

            case R.id.txt_post:

                KeyBoardUtils.closeSoftKeyboard(EditPostArticleActivity.this);

                if (type.equals(StaticData.question)) {

                    if (validationQA()) {
                        callAPIAddQuestionAnswer();
                    }
                }

                if (type.equals(StaticData.research_paper)) {

                    if (validationRP()) {
                        callAPIResearchPaper();
                    }
                }

                if (type.equals(StaticData.article)) {

                    if (validationAP()) {
                        callAPIArticlePost();
                    }
                }

                break;

            case R.id.img_dropdown:

                spinner_select_category.performClick();

                break;

        }
    }

    private boolean validationQA() {

        boolean isValid = true;

        if (post_category.equals("")) {

            isValid = false;
            AppClass.snackBarView.snackBarShow(EditPostArticleActivity.this, getResources().getString(R.string.select_category), ContextCompat.getColor(EditPostArticleActivity.this, R.color.red));

        } else if (edt_article.getText().toString().trim().isEmpty()) {

            isValid = false;
            edt_article.requestFocus();
            AppClass.snackBarView.snackBarShow(EditPostArticleActivity.this, getResources().getString(R.string.please_enter_question), ContextCompat.getColor(EditPostArticleActivity.this, R.color.red));

        } /*else if (edt_add_additional_info.getText().toString().trim().isEmpty()) {

            isValid = false;
            edt_add_additional_info.requestFocus();
            AppClass.snackBarView.snackBarShow(EditPostArticleActivity.this, getResources().getString(R.string.please_add_additional_info), ContextCompat.getColor(EditPostArticleActivity.this, R.color.red));

        }*/

        return isValid;
    }

    private boolean validationAP() {

        boolean isValid = true;

        if (post_category.equals("")) {

            isValid = false;
            AppClass.snackBarView.snackBarShow(EditPostArticleActivity.this, getResources().getString(R.string.select_category), ContextCompat.getColor(EditPostArticleActivity.this, R.color.red));

        } else if (edt_title.getText().toString().trim().isEmpty()) {

            isValid = false;
            edt_title.requestFocus();
            AppClass.snackBarView.snackBarShow(EditPostArticleActivity.this, getResources().getString(R.string.please_enter_title), ContextCompat.getColor(EditPostArticleActivity.this, R.color.red));

        } else if (edt_article.getText().toString().trim().isEmpty()) {

            isValid = false;
            edt_article.requestFocus();
            AppClass.snackBarView.snackBarShow(EditPostArticleActivity.this, getResources().getString(R.string.please_enter_question), ContextCompat.getColor(EditPostArticleActivity.this, R.color.red));

        }/* else if (edt_add_additional_info.getText().toString().trim().isEmpty()) {

            isValid = false;
            edt_add_additional_info.requestFocus();
            AppClass.snackBarView.snackBarShow(EditPostArticleActivity.this, getResources().getString(R.string.please_add_additional_info), ContextCompat.getColor(EditPostArticleActivity.this, R.color.red));

        }*/

        return isValid;
    }

    private boolean validationRP() {

        boolean isValid = true;

        if (post_category.equals("")) {

            isValid = false;
            AppClass.snackBarView.snackBarShow(EditPostArticleActivity.this, getResources().getString(R.string.select_category), ContextCompat.getColor(EditPostArticleActivity.this, R.color.red));

        } else if (edt_article.getText().toString().trim().isEmpty()) {

            isValid = false;
            edt_article.requestFocus();
            AppClass.snackBarView.snackBarShow(EditPostArticleActivity.this, getResources().getString(R.string.please_enter_title), ContextCompat.getColor(EditPostArticleActivity.this, R.color.red));

        }/* else if (pdfPath.isEmpty()) {
            isValid = false;
            AppClass.snackBarView.snackBarShow(EditPostArticleActivity.this, getResources().getString(R.string.select_any_research_paper), ContextCompat.getColor(EditPostArticleActivity.this, R.color.red));
        }*/

        return isValid;
    }

    private void callAPIAddQuestionAnswer() {

        RequestBody rbuserid = RequestBody.create(MediaType.parse("multipart/form-data"), AppClass.preferences.getUserId());
        RequestBody post_title = RequestBody.create(MediaType.parse("multipart/form-data"), edt_article.getText().toString().trim());
        RequestBody post_description = RequestBody.create(MediaType.parse("multipart/form-data"), edt_add_additional_info.getText().toString().trim());
        RequestBody postCategory = RequestBody.create(MediaType.parse("multipart/form-data"), post_category);
        RequestBody postId = RequestBody.create(MediaType.parse("multipart/form-data"), postData.getPostId());


        HashMap<String, RequestBody> request = new HashMap<>();
        request.put("user_id", rbuserid);
        request.put("post_id", postId);
        request.put("post_title", post_title);
        request.put("post_description", post_description);
        request.put("post_category", postCategory);


        ApiCall.getInstance().editPostOther(EditPostArticleActivity.this, request, pdfPath, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {

                String response = (String) data;

                StaticData.isEdited = true;
                //AppClass.snackBarView.snackBarShow(EditPostArticleActivity.this, response);
                DialogUtils.openDialog(EditPostArticleActivity.this, response, getString(R.string.ok), "", new AlertDialogInterface() {
                    @Override
                    public void onNegativeBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }

                    @Override
                    public void onPositiveBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                        finish();
                    }
                });
            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {
                if (StringUtils.isNotEmpty(errorMessage)) {
                    DialogUtils.openDialog(EditPostArticleActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                        @Override
                        public void onNegativeBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }

                        @Override
                        public void onPositiveBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }
                    });
                }

            }
        }, true);

    }

    private void callAPIArticlePost() {

        RequestBody rbuserid = RequestBody.create(MediaType.parse("multipart/form-data"), AppClass.preferences.getUserId());
        RequestBody post_title = RequestBody.create(MediaType.parse("multipart/form-data"), edt_title.getText().toString().trim());
        RequestBody post_description = RequestBody.create(MediaType.parse("multipart/form-data"), edt_article.getText().toString().trim());
        RequestBody postCategory = RequestBody.create(MediaType.parse("multipart/form-data"), post_category);
        RequestBody postId = RequestBody.create(MediaType.parse("multipart/form-data"), postData.getPostId());

        RequestBody deleteImg = RequestBody.create(MediaType.parse("multipart/form-data"), "");


        if (EditPostArticleImagesAdapter.builder != null) {
            if (!EditPostArticleImagesAdapter.builder.toString().isEmpty()) {
                Logger.d("delete images id====" + EditPostArticleImagesAdapter.builder.toString());
                deleteImg = RequestBody.create(MediaType.parse("multipart/form-data"), EditPostArticleImagesAdapter.builder.toString());
            }
        }


        HashMap<String, RequestBody> request = new HashMap<>();
        request.put("user_id", rbuserid);
        request.put("post_id", postId);
        request.put("post_title", post_title);
        request.put("post_description", post_description);
        request.put("post_category", postCategory);
        request.put("delete_img", deleteImg);
        request.put("post_link", ApiCall.getInstance().getRequestBodyOfString(currentUrl));
        request.put("link_title", ApiCall.getInstance().getRequestBodyOfString(currentTitle));
        request.put("link_description", ApiCall.getInstance().getRequestBodyOfString(currentDescription));
        request.put("link_image", ApiCall.getInstance().getRequestBodyOfString(currentImageUrl));
        ArrayList<String> pathList = new ArrayList<>();

        /*imagesList.clear();
        imagesList.addAll(editPostArticleImagesAdapter.getUpdatedList());*/

        Logger.d("imagelist==" + imagesList.size());

        for (int i = 0; i < imagesList.size(); i++) {
            if (imagesList.get(i).getPost_img_id().isEmpty()) {
                Logger.d("imagelist==" + imagesList.get(i).getImage().getAbsolutePath());
                pathList.add(imagesList.get(i).getImage().getAbsolutePath());
            }
        }

        ApiCall.getInstance().editPostForArticle(EditPostArticleActivity.this, request, pathList, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {

                String response = (String) data;
                StaticData.isEdited = true;
                // AppClass.snackBarView.snackBarShow(EditPostArticleActivity.this, response);
                DialogUtils.openDialog(EditPostArticleActivity.this, response, getString(R.string.ok), "", new AlertDialogInterface() {
                    @Override
                    public void onNegativeBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }

                    @Override
                    public void onPositiveBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                        EditPostArticleImagesAdapter.builder.setLength(0);
                        finish();
                    }
                });
            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {
                if (StringUtils.isNotEmpty(errorMessage)) {
                    DialogUtils.openDialog(EditPostArticleActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                        @Override
                        public void onNegativeBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }

                        @Override
                        public void onPositiveBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }
                    });
                }

            }
        }, true);

    }

    private void callAPIResearchPaper() {

        RequestBody rbuserid = RequestBody.create(MediaType.parse("multipart/form-data"), AppClass.preferences.getUserId());
        RequestBody post_title = RequestBody.create(MediaType.parse("multipart/form-data"), edt_article.getText().toString().trim());
        RequestBody post_description = RequestBody.create(MediaType.parse("multipart/form-data"), edt_add_additional_info.getText().toString().trim());
        RequestBody postCategory = RequestBody.create(MediaType.parse("multipart/form-data"), post_category);
        RequestBody postId = RequestBody.create(MediaType.parse("multipart/form-data"), postData.getPostId());

        HashMap<String, RequestBody> request = new HashMap<>();
        request.put("user_id", rbuserid);
        request.put("post_id", postId);
        request.put("post_title", post_title);
        request.put("post_description", post_description);
        request.put("post_category", postCategory);


        ApiCall.getInstance().editPostOther(EditPostArticleActivity.this, request, pdfPath, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {

                String response = (String) data;

                //AppClass.snackBarView.snackBarShow(EditPostArticleActivity.this, response);

                DialogUtils.openDialog(EditPostArticleActivity.this,
                        response, getString(R.string.ok), "", new AlertDialogInterface() {
                            @Override
                            public void onNegativeBtnClicked(DialogInterface alrt) {
                            }

                            @Override
                            public void onPositiveBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                                finish();
                            }
                        });


            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {
                if (StringUtils.isNotEmpty(errorMessage)) {
                    DialogUtils.openDialog(EditPostArticleActivity.this,
                            errorMessage, getString(R.string.ok),
                            "", new AlertDialogInterface() {
                                @Override
                                public void onNegativeBtnClicked(DialogInterface alrt) {
                                    alrt.dismiss();
                                }

                                @Override
                                public void onPositiveBtnClicked(DialogInterface alrt) {
                                    alrt.dismiss();
                                }
                            });
                }

            }
        }, true);

    }

    private void selectPdf() {
        Intent intent = new Intent(this, NormalFilePickActivity.class);
        intent.putExtra(Constant.MAX_NUMBER, 1);
        intent.putExtra(NormalFilePickActivity.SUFFIX, new String[]{"txt", "xlsx", "pptx", "xls", "doc", "docx", "pdf"});
        startActivityForResult(intent, PDF_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {

            if (requestCode == PDF_REQUEST) {
                ArrayList<NormalFile> list = data.getParcelableArrayListExtra(Constant.RESULT_PICK_FILE);
                if (list != null && !list.isEmpty()) {

                    pdfPath = list.get(0).getPath();
                    docPath = new File(pdfPath);

                    String extension = docPath.getAbsolutePath().substring(docPath.getAbsolutePath().lastIndexOf("."));

                    rl_doc.setVisibility(View.VISIBLE);
                    txt_doc_name.setText(docPath.getName());
                    img_doc_upload_post.setImageResource(R.drawable.pdf);

                    Logger.d("extension==" + extension);
                }
            }
        }
    }

    private void callAPICategoryList() {

        if (AppClass.isInternetConnectionAvailable()) {

            HashMap<String, String> request = new HashMap<>();
            request.put("user_id", AppClass.preferences.getUserId());

            ApiCall.getInstance().getCategories(EditPostArticleActivity.this, request, new IApiCallback() {
                @Override
                public void onSuccess(Object data) {

                    categoryList.clear();
                    categoryList.addAll((ArrayList<CategoryModel>) data);

                    ArrayList<String> spinnerCategory = new ArrayList<>();
                    ArrayList<String> spinnerCategory_id = new ArrayList<>();

                    spinnerCategory.add(0, "--Select Category--");
                    spinnerCategory_id.add(0, "");

                    for (int i = 0; i < categoryList.size(); i++) {
                        spinnerCategory.add(i + 1, categoryList.get(i).getCategory_name());
                        spinnerCategory_id.add(i + 1, categoryList.get(i).getCategory_id());
                    }

                    setSpinnerSelectCategory(spinnerCategory, spinnerCategory_id);

                }

                @Override
                public void onFailure(boolean isFalseFromService, String errorMessage) {
                    if (StringUtils.isNotEmpty(errorMessage)) {
                        DialogUtils.openDialog(EditPostArticleActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                            @Override
                            public void onNegativeBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }

                            @Override
                            public void onPositiveBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }
                        });
                    }
                }
            }, true);


        } else {
            AppClass.snackBarView.snackBarShow(EditPostArticleActivity.this, getString(R.string.nonetwork));
        }


    }


    private void setSpinnerSelectCategory(ArrayList<String> spinnerArray, final ArrayList<String> spinnerCategory_id) {

        spinnerArrayAdapter = new ArrayAdapter<String>
                (this, android.R.layout.simple_spinner_item, spinnerArray) {


            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

                TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                tv.setTypeface(AppClass.lato_regular);
                tv.setTextSize(14);
                if (position == 0) {
                    tv.setTextColor(getResources().getColor(R.color.color_969696));
                } else {
                    tv.setTextColor(getResources().getColor(R.color.color_333333));
                }

                return tv;
            }

            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    return false;
                } else {
                    return true;
                }
            }

            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

                TextView tv = (TextView) super.getView(position, convertView, parent);
                tv.setTypeface(AppClass.lato_regular);
                tv.setTextSize(14);


                return tv;
            }
        };

        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_select_category.setAdapter(spinnerArrayAdapter);
        spinner_select_category.setPopupBackgroundResource(R.color.white);

        spinner_select_category.setSelection(spinnerArrayAdapter.getPosition(postData.getCategoryName()));

        spinner_select_category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                post_category = spinnerCategory_id.get(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }


    private void addLink(String link) {
        if (StringUtils.isNotEmpty(link))
            textCrawler.makePreview(this, link.startsWith("http") ? link : "https://" + link);

        img_link.setVisibility(View.GONE);
    }

    @Override
    public void onPre() {
        KeyBoardUtils.closeSoftKeyboard(this);

        currentImageSet = null;
        currentTitle = currentDescription = currentUrl = currentCannonicalUrl = currentImageUrl = "";


        View mainView = getLayoutInflater().inflate(R.layout.link_preview_main_view, null);

        linearLayout = mainView.findViewById(R.id.external);

        getLayoutInflater().inflate(R.layout.loading, linearLayout);

        dropPreview.addView(mainView);
    }

    @Override
    public void onPos(SourceContent sourceContent, boolean b) {
        linearLayout.removeAllViews();

        if (b || sourceContent.getFinalUrl().equals("")) {

            View failed = getLayoutInflater().inflate(R.layout.failed, linearLayout);

            TextView titleTextView = failed.findViewById(R.id.text);
            titleTextView.setText(String.format("%s\n%s", getString(R.string.failed_preview), sourceContent.getFinalUrl()));

            failed.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    dropPreview.removeAllViews();
                    currentTitle = currentDescription = currentUrl = currentCannonicalUrl = currentImageUrl = "";
                }
            });

        } else {

            currentImageSet = new Bitmap[sourceContent.getImages().size()];

            View linkPreview = getLayoutInflater().inflate(R.layout.layout_link_preview, linearLayout);

            AppCompatTextView tvLinkPreviewTitle = linkPreview.findViewById(R.id.tv_linkpreview_title);
            AppCompatTextView tvLinkPreviewDescription = linkPreview.findViewById(R.id.tv_linkpreview_description);
            AppCompatTextView tvLinkPreviewLink = linkPreview.findViewById(R.id.tv_link);
            AppCompatImageView ivLinkPreviewImage = linkPreview.findViewById(R.id.iv_url_image);
            AppCompatImageView ivLinkPreviewClose = linkPreview.findViewById(R.id.iv_close);

            tvLinkPreviewTitle.setText(sourceContent.getTitle());
            tvLinkPreviewDescription.setText(sourceContent.getDescription());
            tvLinkPreviewLink.setText(sourceContent.getCannonicalUrl());

            ivLinkPreviewClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dropPreview.removeAllViews();
                    img_link.setVisibility(View.VISIBLE);
                    currentTitle = currentDescription = currentUrl = currentCannonicalUrl = currentImageUrl = "";
                }
            });

            if (sourceContent.getImages().size() > 0) {

                ImageLoadUtils.imageLoad(this,
                        ivLinkPreviewImage,
                        sourceContent.getImages().get(0));

            }

            if (StringUtils.isNotEmpty(sourceContent.getImages().get(0)))
                currentImageUrl = sourceContent.getImages().get(0);
            currentTitle = sourceContent.getTitle();
            currentDescription = sourceContent.getDescription();
            currentUrl = sourceContent.getUrl();
            currentCannonicalUrl = sourceContent.getCannonicalUrl();


        }


    }

}
