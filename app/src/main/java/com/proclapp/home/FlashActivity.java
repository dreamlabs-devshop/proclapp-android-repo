package com.proclapp.home;

import android.content.DialogInterface;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.home.adapter.FlashToUserAdapter;
import com.proclapp.model.FollowerFollowingModel;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.AlertDialogInterface;
import com.proclapp.utils.DialogUtils;
import com.proclapp.utils.Logger;

import java.util.ArrayList;
import java.util.HashMap;

public class FlashActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView img_back;
    private TextView txt_flash_your_post_to,
            txt_flash_user_to_remind,
            txt_done;
    private EditText edt_search;
    private RecyclerView rv_flash_to_user;
    private Button btn_done;
    private FlashToUserAdapter flashToUserAdapter;

    private ArrayList<FollowerFollowingModel> userLists = new ArrayList<>();
    private String post_id = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flash);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        post_id = getIntent().getStringExtra("post_id");

        initView();
        initStyle();
        initListener();


    }

    private void initView() {

        img_back = findViewById(R.id.img_back);
        txt_flash_your_post_to = findViewById(R.id.txt_flash_your_post_to);
        txt_flash_user_to_remind = findViewById(R.id.txt_flash_user_to_remind);
        txt_done = findViewById(R.id.txt_done);
        edt_search = findViewById(R.id.edt_search);
        rv_flash_to_user = findViewById(R.id.rv_flash_to_user);
        btn_done = findViewById(R.id.btn_done);

        setupRecyclerView();

        edt_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                flashToUserAdapter.getFilter().filter(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    private void initStyle() {
        txt_flash_your_post_to.setTypeface(AppClass.lato_regular);
        txt_flash_user_to_remind.setTypeface(AppClass.lato_regular);
        txt_done.setTypeface(AppClass.lato_regular);
        edt_search.setTypeface(AppClass.lato_regular);
        btn_done.setTypeface(AppClass.lato_semibold);
    }

    private void setupRecyclerView() {

        GridLayoutManager layoutManager = new GridLayoutManager(FlashActivity.this, 3);
        rv_flash_to_user.setLayoutManager(layoutManager);

        flashToUserAdapter = new FlashToUserAdapter(FlashActivity.this, userLists);
        rv_flash_to_user.setAdapter(flashToUserAdapter);
        callAPIGetUserList();
    }

    private void initListener() {

        img_back.setOnClickListener(this);
        txt_done.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.img_back:

                onBackPressed();

                break;

            case R.id.txt_done:

                StringBuilder stringBuilder = new StringBuilder();
                String prefix = "";

                for (int i = 0; i < userLists.size(); i++) {
                    if (userLists.get(i).isSelected()) {
                        stringBuilder.append(prefix + userLists.get(i).getUserId());
                        prefix = ",";
                    }
                }

                if (stringBuilder.length() == 0) {
                    AppClass.snackBarView.snackBarShow(FlashActivity.this, getString(R.string.vuser));
                } else {
                    callAPIFlashPostToUser(stringBuilder.toString());
                }
                break;
        }
    }

    private void callAPIGetUserList() {

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());

        ApiCall.getInstance().getUserList(FlashActivity.this,
                request, new IApiCallback() {
                    @Override
                    public void onSuccess(Object data) {

                        userLists.clear();
                        userLists.addAll((ArrayList<FollowerFollowingModel>) data);
                        rv_flash_to_user.getAdapter().notifyDataSetChanged();

                    }

                    @Override
                    public void onFailure(boolean isFalseFromService, String errorMessage) {



               /* DialogUtils.openDialog(QuestionDetailsActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                    @Override
                    public void onNegativeBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }

                    @Override
                    public void onPositiveBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }
                });*/
                    }


                }, true);

    }

    private void callAPIFlashPostToUser(String user) {

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("post_id", post_id);
        request.put("user", user);
        Logger.d("request:" + request);

        ApiCall.getInstance().flashPostToUser(FlashActivity.this,
                request, new IApiCallback() {
                    @Override
                    public void onSuccess(Object data) {

                        DialogUtils.openDialog(FlashActivity.this, data.toString(), getString(R.string.ok), "", new AlertDialogInterface() {
                            @Override
                            public void onNegativeBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }

                            @Override
                            public void onPositiveBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                                finish();
                            }
                        });
                    }

                    @Override
                    public void onFailure(boolean isFalseFromService, String errorMessage) {


                        DialogUtils.openDialog(FlashActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                            @Override
                            public void onNegativeBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }

                            @Override
                            public void onPositiveBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }
                        });
                    }


                }, true);

    }
}
