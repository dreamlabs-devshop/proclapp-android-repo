package com.proclapp.home;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentTabHost;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.share.Share;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.chat.ChatActivity;
import com.proclapp.chat.MessageActivity;
import com.proclapp.fcm.RegisterFCMToken;
import com.proclapp.home.fragment.HomeFragment;
import com.proclapp.home.friends.FriendsActivity;
import com.proclapp.home.question.QuestionDetailsActivity;
import com.proclapp.home.voice_video.VVExpertReplyActivity;
import com.proclapp.model.AllPostsModel;
import com.proclapp.model.CategoryModel;
import com.proclapp.profile.ProfileActivity;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.root.BaseContainerFragment;
import com.proclapp.root.Root_Tab_Advert;
import com.proclapp.root.Root_Tab_Expert;
import com.proclapp.root.Root_Tab_Home;
import com.proclapp.root.Root_Tab_More;
import com.proclapp.root.Root_Tab_Voice;
import com.proclapp.signup.StartupActivity;
import com.proclapp.utils.AlertDialogInterface;
import com.proclapp.utils.DialogUtils;
import com.proclapp.utils.Logger;
import com.proclapp.utils.Preferences;
import com.proclapp.utils.StaticData;
import com.proclapp.utils.StringUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class HomeActivity extends AppCompatActivity implements TabHost.OnTabChangeListener, View.OnClickListener {

    private static final String TAB_1_TAG = "Voice";
    private static final String TAB_2_TAG = "Expert";
    private static final String TAB_3_TAG = "Home";
    //private static final String TAB_4_TAG = "Advert";
    private static final String TAB_5_TAG = "More";
    static ImageView iv1, iv2, iv3, iv5;
    static RelativeLayout fram1, fram2, fram3, fram5;
    static TextView tv_title1, tv_title2, tv_title3, tv_title5;
    private final int REQUEST_PERMISSION_ALL = 10;
    public FragmentTabHost mTabHost;
    public int currentTabPosition;
    public TabWidget
            tabsWidget;
    View tabview1 = null;
    View tabview2 = null;
    View tabview3 = null;
    //View tabview4 = null;
    View tabview5 = null;
    private ImageView img_profile,
            img_search,
            img_message,
            img_friend;
    private String[] PERMISSIONS_LIST = {Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO};
    private FrameLayout fl_friend,
            fl_notification,
            fl_message;
    private String notiType = "";
    private ArrayList<String> categories = new ArrayList<>();

    private TextView
            txt_friend_count,
            txt_notification_count,
            txt_message_count;


    public static View createTabView1(final Context context, int d) {
        View view = LayoutInflater.from(context).inflate(R.layout.tabs_bg, null);
        fram1 = (RelativeLayout) view.findViewById(R.id.back_framlayout);
        tv_title1 = (TextView) view.findViewById(R.id.tv_title);
        tv_title1.setText(R.string.voice);
        tv_title1.setTypeface(AppClass.lato_regular);
        iv1 = (ImageView) view.findViewById(R.id.imageView1);
        iv1.setBackgroundResource(d);
        return view;
    }

    public static View createTabView2(final Context context, int d) {
        View view = LayoutInflater.from(context).inflate(R.layout.tabs_bg, null);
        fram2 = (RelativeLayout) view.findViewById(R.id.back_framlayout);
        tv_title2 = (TextView) view.findViewById(R.id.tv_title);
        tv_title2.setText(R.string.mentors);
        tv_title2.setTypeface(AppClass.lato_regular);
        iv2 = (ImageView) view.findViewById(R.id.imageView1);
        iv2.setBackgroundResource(d);
        return view;
    }

    public static View createTabView3(final Context context, int d) {
        View view = LayoutInflater.from(context).inflate(R.layout.tabs_bg, null);
        fram3 = (RelativeLayout) view.findViewById(R.id.back_framlayout);
        tv_title3 = (TextView) view.findViewById(R.id.tv_title);
        tv_title3.setText(R.string.home);
        tv_title3.setTypeface(AppClass.lato_bold);
        iv3 = (ImageView) view.findViewById(R.id.imageView1);
        iv3.setBackgroundResource(d);
        return view;
    }

//    public static View createTabView4(final Context context, int d) {
//        View view = LayoutInflater.from(context).inflate(R.layout.tabs_bg, null);
//        fram4 = (RelativeLayout) view.findViewById(R.id.back_framlayout);
//        tv_title4 = (TextView) view.findViewById(R.id.tv_title);
//        tv_title4.setText(R.string.advert);
//        tv_title4.setTypeface(AppClass.lato_regular);
//        iv4 = (ImageView) view.findViewById(R.id.imageView1);
//        iv4.setBackgroundResource(d);
//
//        return view;
//    }

    public static View createTabView5(final Context context, int d) {
        View view = LayoutInflater.from(context).inflate(R.layout.tabs_bg, null);
        fram5 = (RelativeLayout) view.findViewById(R.id.back_framlayout);
        tv_title5 = (TextView) view.findViewById(R.id.tv_title);
        tv_title5.setText(R.string.more);
        tv_title5.setTypeface(AppClass.lato_regular);

        iv5 = (ImageView) view.findViewById(R.id.imageView1);
        iv5.setBackgroundResource(d);
        return view;
    }

    public static boolean hasSelfPermission(Activity activity, String[] permissions) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }

        for (String permission : permissions) {
            if (activity.checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        // Get FCM token
        if (AppClass.preferences.getFCMToken().equals("")) {
            AppClass.fcmProcessClass.getFCMTokenFromServer();
        }

        initTabView();
        initTabListener();
        initView();
        if (!AppClass.preferences.isSkip()) {

            new RegisterFCMToken(HomeActivity.this);
        }


        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        checkPermission();
        MobileAds.initialize(this, getString(R.string.admob_app_id));
        AdView mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
                Logger.e("Error Code " + i);

            }
        });

        if (getIntent().hasExtra("openfrom") &&
                getIntent().getStringExtra("openfrom").equalsIgnoreCase("notification")) {
            notificationRedirection(getIntent());
        }

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent.hasExtra("openfrom") &&
                intent.getStringExtra("openfrom").equalsIgnoreCase("notification")) {
            notificationRedirection(intent);
        }
    }

    /* @Override
    protected void onResume() {
        super.onResume();

    }*/

    private void initView() {
        img_profile = findViewById(R.id.img_profile);
        img_search = findViewById(R.id.img_search);
        fl_friend = findViewById(R.id.fl_friend);
        fl_notification = findViewById(R.id.fl_notification);
        fl_message = findViewById(R.id.fl_message);
        txt_friend_count = findViewById(R.id.txt_friend_count);
        txt_notification_count = findViewById(R.id.txt_notification_count);
        txt_message_count = findViewById(R.id.txt_message_count);


        img_profile.setOnClickListener(this);
        img_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, SearchActivity.class));
            }
        });
        fl_friend.setOnClickListener(this);
        fl_message.setOnClickListener(this);
        fl_notification.setOnClickListener(this);
    }

    private void initTabView() {

        tabsWidget = (TabWidget) findViewById(android.R.id.tabs);
        mTabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
        mTabHost.setup(HomeActivity.this, getSupportFragmentManager(), R.id.realtabcontent);
        mTabHost.setOnTabChangedListener(this);
        mTabHost.getTabWidget().setDividerDrawable(null);
        mTabHost.getTabWidget().setStripEnabled(false);

        tabview1 = createTabView1(mTabHost.getContext(), R.drawable.tab_bottom_voice);
        tabview2 = createTabView2(mTabHost.getContext(), R.drawable.tab_bottom_expert);
        tabview3 = createTabView3(mTabHost.getContext(), R.drawable.tab_bottom_home);
        //tabview4 = createTabView4(mTabHost.getContext(), R.drawable.tab_bottom_advert);
        tabview5 = createTabView5(mTabHost.getContext(), R.drawable.tab_bottom_more);

        mTabHost.addTab(mTabHost.newTabSpec(TAB_3_TAG).setIndicator(tabview3), Root_Tab_Home.class, null);
        mTabHost.addTab(mTabHost.newTabSpec(TAB_1_TAG).setIndicator(tabview1), Root_Tab_Voice.class, null);
        mTabHost.addTab(mTabHost.newTabSpec(TAB_2_TAG).setIndicator(tabview2), Root_Tab_Expert.class, null);
        //mTabHost.addTab(mTabHost.newTabSpec(TAB_4_TAG).setIndicator(tabview4), Root_Tab_Advert.class, null);
        mTabHost.addTab(mTabHost.newTabSpec(TAB_5_TAG).setIndicator(tabview5), Root_Tab_More.class, null);

        mTabHost.setCurrentTab(0);
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();

        if (mTabHost.getCurrentTab() != 0) {
            mTabHost.setCurrentTab(0);
        } else {
            super.onBackPressed();
        }

    }

    private void initTabListener() {


        tabview1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (StringUtils.isNotEmpty(AppClass.preferences.getUserId())) {

                    mTabHost.setCurrentTab(1);

                    tv_title1.setTypeface(AppClass.lato_bold);
                    tv_title2.setTypeface(AppClass.lato_regular);
                    tv_title3.setTypeface(AppClass.lato_regular);
                    //tv_title4.setTypeface(AppClass.lato_regular);
                    tv_title5.setTypeface(AppClass.lato_regular);
                } else {
                    finishAffinity();
                    startActivity(new Intent(HomeActivity.this, StartupActivity.class));
                }
            }
        });

        tabview2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (StringUtils.isNotEmpty(AppClass.preferences.getUserId())) {

                    mTabHost.setCurrentTab(2);

                    tv_title1.setTypeface(AppClass.lato_regular);
                    tv_title2.setTypeface(AppClass.lato_bold);
                    tv_title3.setTypeface(AppClass.lato_regular);
                    //tv_title4.setTypeface(AppClass.lato_regular);
                    tv_title5.setTypeface(AppClass.lato_regular);
                } else {
                    finishAffinity();
                    startActivity(new Intent(HomeActivity.this, StartupActivity.class));
                }
            }
        });

        tabview3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mTabHost.getCurrentTab() == 0) {
                    ((BaseContainerFragment) getSupportFragmentManager().findFragmentByTag(TAB_3_TAG)).replaceFragment(new HomeFragment(), false);
                } else {
                    mTabHost.setCurrentTab(0);
                }

                tv_title1.setTypeface(AppClass.lato_regular);
                tv_title2.setTypeface(AppClass.lato_regular);
                tv_title3.setTypeface(AppClass.lato_bold);
                //tv_title4.setTypeface(AppClass.lato_regular);
                tv_title5.setTypeface(AppClass.lato_regular);
            }
        });

//        tabview4.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                if (StringUtils.isNotEmpty(AppClass.preferences.getUserId())) {
//
//
//                    mTabHost.setCurrentTab(3);
//
//                    tv_title1.setTypeface(AppClass.lato_regular);
//                    tv_title2.setTypeface(AppClass.lato_regular);
//                    tv_title3.setTypeface(AppClass.lato_regular);
//                    tv_title4.setTypeface(AppClass.lato_bold);
//                    tv_title5.setTypeface(AppClass.lato_regular);
//
//                } else {
//                    finishAffinity();
//                    startActivity(new Intent(HomeActivity.this, StartupActivity.class));
//                }
//            }
//        });

        tabview5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (StringUtils.isNotEmpty(AppClass.preferences.getUserId())) {

                    mTabHost.setCurrentTab(3);

                    tv_title1.setTypeface(AppClass.lato_regular);
                    tv_title2.setTypeface(AppClass.lato_regular);
                    tv_title3.setTypeface(AppClass.lato_regular);
                    //tv_title4.setTypeface(AppClass.lato_regular);
                    tv_title5.setTypeface(AppClass.lato_bold);

                } else {
                    finishAffinity();
                    startActivity(new Intent(HomeActivity.this, StartupActivity.class));
                }
            }
        });
    }

    @Override
    public void onTabChanged(String s) {

    }

    @Override
    public void onClick(View view) {

        if (StringUtils.isNotEmpty(AppClass.preferences.getUserId())) {

            switch (view.getId()) {

                case R.id.img_profile:
                    if (!AppClass.preferences.getToken().isEmpty()) {
                        startActivity(new Intent(HomeActivity.this, ProfileActivity.class));
                    } else {
                        startActivity(new Intent(HomeActivity.this, StartupActivity.class));
                    }
                    break;


                case R.id.fl_message:
                    startActivity(new Intent(HomeActivity.this, MessageActivity.class));
                    break;

                case R.id.fl_friend:
                    startActivity(new Intent(HomeActivity.this, FriendsActivity.class)
                            .putExtra("openfrom", "home"));
                    break;

                case R.id.fl_notification:
                    startActivity(new Intent(HomeActivity.this, NotificationActivity.class));
                    break;
            }
        } else {
            finishAffinity();
            startActivity(new Intent(HomeActivity.this, StartupActivity.class));
        }
    }

    private void callAPIGetProfile() {

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("profile_id", AppClass.preferences.getUserId());


        ApiCall.getInstance().getProfile(this, request, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {
                JsonObject response = (JsonObject) data;

                setData(response);
                Logger.e("UserData " + response.get("firstname").getAsString());

            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {

            }
        }, false);

    }

    private void setData(JsonObject userData) {

        try {

            AppClass.preferences.storeUserId(userData.get("user_id").getAsString());
            AppClass.preferences.storeFirstName(userData.get("firstname").getAsString());
            AppClass.preferences.storeUSerLastName(userData.get("lastname").getAsString());
            AppClass.preferences.storeEmailId(userData.get("email").getAsString());
            AppClass.preferences.storeAddress(userData.get("address").getAsString());
            AppClass.preferences.storeGender(userData.get("gender").getAsString());
            AppClass.preferences.storeMobileNumber(userData.get("phone").getAsString());
            AppClass.preferences.storeCountryCode(userData.get("phone_code").getAsString());
            AppClass.preferences.storeAboutMe(userData.get("about_me").getAsString());
            AppClass.preferences.storeAboutMyProfession(userData.get("about_my_profession").getAsString());
            AppClass.preferences.storeProfileImage(userData.get("profile_image").getAsString());
            AppClass.preferences.storeCoverImage(userData.get("cover_image").getAsString());
            AppClass.preferences.storeMemberSince(userData.get("member_since").getAsString());
            AppClass.preferences.storeIsExpert(userData.get("is_expert").getAsString());
            AppClass.preferences.storeFollowers(userData.get("followers").getAsString());
            AppClass.preferences.storeFollowing(userData.get("total_following").getAsString());
            AppClass.preferences.setValue(Preferences.TOTAL_FRIEND, userData.get("total_friends").getAsString());
            AppClass.preferences.setValue(Preferences.UNREAD_NOTIFICATION, userData.get("unread_notifications").getAsString());
            AppClass.preferences.setValue(Preferences.UNREAD_MESSAGE, userData.get("unread_messages").getAsString());
            AppClass.preferences.setValue(Preferences.FRIEND_REQUESTS, userData.get("friend_requests").getAsString());

            if (StringUtils.isNotEmpty(userData.get("friend_requests").getAsString()) &&
                    Integer.parseInt(userData.get("friend_requests").getAsString()) > 0) {
                txt_friend_count.setVisibility(View.VISIBLE);
                txt_friend_count.setText(userData.get("friend_requests").getAsString());
            } else {
                txt_friend_count.setVisibility(View.GONE);
            }
            if (StringUtils.isNotEmpty(userData.get("unread_messages").getAsString()) &&
                    Integer.parseInt(userData.get("unread_messages").getAsString()) > 0) {
                txt_message_count.setVisibility(View.VISIBLE);
                txt_message_count.setText(userData.get("unread_messages").getAsString());
            } else {
                txt_message_count.setVisibility(View.GONE);
            }
            if (StringUtils.isNotEmpty(userData.get("unread_notifications").getAsString()) &&
                    Integer.parseInt(userData.get("unread_notifications").getAsString()) > 0) {
                txt_notification_count.setVisibility(View.VISIBLE);
                txt_notification_count.setText(userData.get("unread_notifications").getAsString());
            } else {
                txt_notification_count.setVisibility(View.GONE);
            }


            ArrayList<CategoryModel> tempCategoryList = new Gson().fromJson(userData.getAsJsonArray("interest_categories").toString(),
                    new TypeToken<List<CategoryModel>>() {
                    }.getType());

            for (int i = 0; i < tempCategoryList.size(); i++) {
                categories.add(tempCategoryList.get(i).getCategory_id());
            }

            AppClass.preferences.storeCategoriesId(categories.toString());

            JsonArray professionalQualification = userData.getAsJsonArray("professional_qualification");

            for (int i = 0; i < professionalQualification.size(); i++) {

                if (professionalQualification.get(i).getAsJsonObject().get("is_current_job").getAsString().equalsIgnoreCase("1")) {

                    AppClass.preferences.storeCurrentJob(professionalQualification.get(i).getAsJsonObject().get("job_title").getAsString());
                    break;
                }

            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @TargetApi(Build.VERSION_CODES.M)
    private boolean checkPermission() {

        if (hasSelfPermission(HomeActivity.this, PERMISSIONS_LIST)) {
            return true;
        } else {
            requestPermissions(PERMISSIONS_LIST, REQUEST_PERMISSION_ALL);
            return false;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_PERMISSION_ALL) {
            boolean allPermissionsGranted = true;
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    allPermissionsGranted = false;
                    break;
                }
            }

            if (allPermissionsGranted) {

            } else {
                StaticData.openSettingsDialog(HomeActivity.this);
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    private void notificationRedirection(Intent intent) {

        try {
            JSONObject data = new JSONObject(intent.getStringExtra("data"));

            if (data.has("notification_type")) {

                notiType = data.getString("notification_type");

                if (data.get("notification_type").equals("11")) {
                    startActivity(new Intent(this, ChatActivity.class)
                            .putExtra("sender", data.getString("sender")));

                } else if (data.has("post_type")) {

                    String answerId = "";
                    if (data.has("post_answers_id"))
                        answerId = data.getString("post_answers_id");


                    getPostDetails(data.getString("post_id"), answerId);
                }

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void getPostDetails(String postId, String answerId) {

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("post_id", postId);

        ApiCall.getInstance().getPostDetails(this, request, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {

                AllPostsModel model = (AllPostsModel) data;


                if (StringUtils.isNotEmpty(model.getPostId())) {

                    String openFor = "";

                    if (model.getPostType().equalsIgnoreCase("2")) {
                        openFor = "qa";
                    } else if (model.getPostType().equalsIgnoreCase("4")) {
                        openFor = "rp";
                    }

                    Intent intent = null;


                    if (model.getPostType().equalsIgnoreCase("2") ||
                            model.getPostType().equalsIgnoreCase("4")) {

                        intent = new Intent(HomeActivity.this, QuestionDetailsActivity.class);
                        intent.putExtra("openfrom", openFor);
                        intent.putExtra("questionDetails", (AllPostsModel) data);

                        if (notiType.equalsIgnoreCase("11") ||
                                notiType.equalsIgnoreCase("23") ||
                                notiType.equalsIgnoreCase("6")) {
                            intent.putExtra("notiType", notiType);
                            intent.putExtra("answerId", answerId);
                        }

                        startActivity(intent);

                    } else if (model.getPostType().equalsIgnoreCase("1")) {
                        intent = new Intent(HomeActivity.this, DiscussActivity.class);
                        intent.putExtra("articleDetails", (AllPostsModel) data);

                        if (notiType.equalsIgnoreCase("11") ||
                                notiType.equalsIgnoreCase("23") ||
                                notiType.equalsIgnoreCase("6")) {
                            intent.putExtra("notiType", notiType);
                            intent.putExtra("answerId", answerId);
                        }

                        startActivity(intent);

                    } else if (model.getPostType().equalsIgnoreCase("3")) {
                        intent = new Intent(HomeActivity.this, VVExpertReplyActivity.class);
                        intent.putExtra("vvdata", (AllPostsModel) data);
                        if (notiType.equalsIgnoreCase("11  ") ||
                                notiType.equalsIgnoreCase("23") ||
                                notiType.equalsIgnoreCase("6")) {
                            intent.putExtra("notiType", notiType);
                            intent.putExtra("answerId", answerId);
                        }

                        startActivity(intent);
                    }
                }

            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {
                DialogUtils.openDialog(HomeActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                    @Override
                    public void onNegativeBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }

                    @Override
                    public void onPositiveBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }
                });
            }
        }, true);


    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!AppClass.preferences.isSkip())
            callAPIGetProfile();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable @org.jetbrains.annotations.Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == NewPostDialog.PICK_VIDEO_REQUEST) {
                Intent intent = new Intent(this, SharedVideo.class);
                intent.putExtra("source", "home");
                intent.setData(data.getData());
                if (data.getData() == null) {
                    Toast.makeText(this, "there is no data", Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    Log.e("Check here", data.getData().toString());
                    startActivity(intent);
                }
            }
        }
    }
}
