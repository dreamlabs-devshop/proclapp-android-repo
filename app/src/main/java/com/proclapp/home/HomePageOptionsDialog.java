package com.proclapp.home;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.home.voice_video.SelectExpertActivity;
import com.proclapp.model.AllPostsModel;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.AlertDialogInterface;
import com.proclapp.utils.DialogUtils;
import com.proclapp.utils.Logger;
import com.proclapp.utils.StaticData;
import com.proclapp.utils.StringUtils;
import com.proclapp.utils.Utils;

import java.util.HashMap;

public class HomePageOptionsDialog extends Dialog {

    private Activity activity;

    private TextView txt_edit,
            txt_share,
            txt_delete,
            txt_request_expert;

    private CardView card_view_option;

    private ImageView img_close;
    private RelativeLayout rl_main;

    private String postId;
    private AllPostsModel postData;
    private UpdateListData updateListData;

    public HomePageOptionsDialog(Activity a, AllPostsModel postData, String postId, UpdateListData updateListData) {
        super(a);
        this.activity = a;
        this.postId = postId;
        this.postData = postData;
        this.updateListData = updateListData;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_home_page_option);

        Logger.e("PostId " + postId);

        txt_edit = findViewById(R.id.txt_edit);
        txt_share = findViewById(R.id.txt_share);
        txt_delete = findViewById(R.id.txt_delete);
        txt_request_expert = findViewById(R.id.txt_request_expert);

        card_view_option = findViewById(R.id.card_view_option);
        card_view_option.setBackgroundResource(R.drawable.rounded_bg_option);

        img_close = findViewById(R.id.img_close);
        rl_main = findViewById(R.id.rl_main);


        getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        //getWindow().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(activity,R.color.color_28black)));
        //getWindow().setBackgroundDrawableResource(R.drawable.trans_bg);

        txt_edit.setTypeface(AppClass.lato_medium);
        txt_share.setTypeface(AppClass.lato_medium);
        txt_delete.setTypeface(AppClass.lato_medium);
        txt_request_expert.setTypeface(AppClass.lato_medium);

        if (postData.getPostType().equalsIgnoreCase("3")) {
            txt_edit.setVisibility(View.GONE);
        }

        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        rl_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        txt_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String type = "";

                switch (postData.getPostType()) {
                    case "1":
                        type = StaticData.article;
                        break;
                    case "2":
                        type = StaticData.question;
                        break;
                    case "4":
                        type = StaticData.research_paper;
                        break;
                }

                dismiss();
                activity.startActivity(new Intent(activity, EditPostArticleActivity.class)
                        .putExtra("type", type)
                        .putExtra("postData", postData));
            }
        });

        txt_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dismiss();
                sharePost();
//                callAPISharePost();
            }
        });

        txt_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dismiss();
                DialogUtils.openDialog(activity, activity.getString(R.string.are_you_sure_you_want_to_delete_this_post), activity.getString(R.string.delete), activity.getString(R.string.cancel), new AlertDialogInterface() {
                    @Override
                    public void onNegativeBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }

                    @Override
                    public void onPositiveBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                        callAPIDeletePost();
                    }
                });


            }
        });

        txt_request_expert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
                activity.startActivity(new Intent(activity, SelectExpertActivity.class)
                        .putExtra("openfrom", "request_expert")
                        .putExtra("postId", postId));
            }
        });

    }

    private void callAPIDeletePost() {

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("post_id", postId);

        Logger.d("request:" + request);

        ApiCall.getInstance().deletePost(activity,
                request, new IApiCallback() {
                    @Override
                    public void onSuccess(Object data) {

                        updateListData.updateData(postId);

                        /*DialogUtils.openDialog(activity, data.toString(), activity.getString(R.string.ok), "", new AlertDialogInterface() {
                            @Override
                            public void onNegativeBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }

                            @Override
                            public void onPositiveBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();

                            }
                        });*/
                    }

                    @Override
                    public void onFailure(boolean isFalseFromService, String errorMessage) {


                        DialogUtils.openDialog(activity, errorMessage, activity.getString(R.string.ok), "", new AlertDialogInterface() {
                            @Override
                            public void onNegativeBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }

                            @Override
                            public void onPositiveBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }
                        });
                    }


                }, true);

    }

    private void callAPISharePost() {

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("post_id", postId);

        Logger.d("request:" + request);

        ApiCall.getInstance().sharePost(activity, request, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {

                //updateListData.updateData(postId);

                        /*DialogUtils.openDialog(activity, data.toString(), activity.getString(R.string.ok), "", new AlertDialogInterface() {
                            @Override
                            public void onNegativeBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }

                            @Override
                            public void onPositiveBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();

                            }
                        });*/
            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {


                DialogUtils.openDialog(activity, errorMessage, activity.getString(R.string.ok), "", new AlertDialogInterface() {
                    @Override
                    public void onNegativeBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }

                    @Override
                    public void onPositiveBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }
                });
            }


        }, true);

    }

    private void sharePost() {

        String shareBody = "";

        if (postData.getPostType().equalsIgnoreCase("1")) {
            //Article
            shareBody = "Article Title:\n" + postData.getPostTitle() + "\n Article:\n" + Utils.fromHtml(postData.getPostDescription());

            if (!postData.getImages().isEmpty() && StringUtils.isNotEmpty(postData.getImages().get(0).getImage_name()))
                shareBody = shareBody + "\n" + postData.getImages().get(0).getImage_name();


        } else if (postData.getPostType().equalsIgnoreCase("2")) {
            //Question
            shareBody = postData.getPostTitle();

        } else if (postData.getPostType().equalsIgnoreCase("3")) {
            //vv
            if (postData.getVvType().equalsIgnoreCase("1")) {
                shareBody = postData.getAudioPost().getAudioUrl();
            } else {
                shareBody = postData.getVideosPost().getVideoUrl();
            }
        } else {
            //RP
            shareBody = postData.getPostTitle() + "\n" + postData.getFiles().getFileName();
        }


        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
        activity.startActivity(Intent.createChooser(sharingIntent, activity.getString(R.string.share_using)));

    }


    public interface UpdateListData {
        void updateData(String postId);
    }

}