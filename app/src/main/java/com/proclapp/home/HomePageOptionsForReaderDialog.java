package com.proclapp.home;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.home.voice_video.SelectExpertActivity;
import com.proclapp.model.AllPostsModel;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.AlertDialogInterface;
import com.proclapp.utils.DialogUtils;
import com.proclapp.utils.StringUtils;
import com.proclapp.utils.Utils;

public class HomePageOptionsForReaderDialog extends Dialog implements View.OnClickListener {

    private Activity activity;

    private TextView txt_suggest_edit,
            txt_share,
            txt_compare_answer,
            txt_request_expert,
            txt_broadcast_to_all,
            txt_mute_conversation,
            txt_report_abuse;

    private CardView card_view_option;

    private String postId;
    private AllPostsModel postData;

    private ImageView img_close;
    private RelativeLayout rl_main;

    public HomePageOptionsForReaderDialog(Activity a, AllPostsModel postData, String postId) {
        super(a);
        this.activity = a;
        this.postId = postId;
        this.postData = postData;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_home_page_option_reader);

        getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        initViews();
        setClickListener();
        setTypeFace();

    }

    @Override
    public void onClick(View view) {
        dismiss();
        switch (view.getId()) {

            case R.id.txt_suggest_edit:
                activity.startActivity(new Intent(activity, SuggestEditActivity.class)
                        .putExtra("postData", postData));
                break;

            case R.id.txt_report_abuse:
                DialogUtils.openDialog(activity, activity.getString(R.string.report_message), activity.getString(R.string.ok), activity.getString(R.string.cancel), new AlertDialogInterface() {
                    @Override
                    public void onNegativeBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }

                    @Override
                    public void onPositiveBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                        reportAbuse();
                    }
                });

                break;

            case R.id.txt_request_expert:
                activity.startActivity(new Intent(activity, SelectExpertActivity.class)
                        .putExtra("openfrom", "request_expert")
                        .putExtra("postId", postId));
                break;

            case R.id.txt_share:
                sharePost();
                break;

            case R.id.txt_broadcast_to_all:
                DialogUtils.openDialog(activity, activity.getString(R.string.broadcast_message), activity.getString(R.string.yes), activity.getString(R.string.cancel), new AlertDialogInterface() {
                    @Override
                    public void onNegativeBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }

                    @Override
                    public void onPositiveBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                        broadcastPost();
                    }
                });
                break;


        }
    }

    private void initViews() {

        txt_suggest_edit = findViewById(R.id.txt_suggest_edit);
        txt_share = findViewById(R.id.txt_share);
        txt_compare_answer = findViewById(R.id.txt_compare_answer);
        txt_request_expert = findViewById(R.id.txt_request_expert);
        txt_broadcast_to_all = findViewById(R.id.txt_broadcast_to_all);
        txt_mute_conversation = findViewById(R.id.txt_mute_conversation);
        txt_report_abuse = findViewById(R.id.txt_report_abuse);

        card_view_option = findViewById(R.id.card_view_option);
        card_view_option.setBackgroundResource(R.drawable.rounded_bg_option);

        img_close = findViewById(R.id.img_close);
        rl_main = findViewById(R.id.rl_main);

        if (postData.getPostType().equalsIgnoreCase("4")) {
            txt_suggest_edit.setVisibility(View.INVISIBLE);
            txt_compare_answer.setVisibility(View.GONE);
            txt_request_expert.setVisibility(View.GONE);
            txt_broadcast_to_all.setVisibility(View.GONE);
            txt_mute_conversation.setVisibility(View.GONE);
            txt_report_abuse.setVisibility(View.VISIBLE);

        } else if (postData.getPostType().equalsIgnoreCase("1")) {
            txt_suggest_edit.setVisibility(View.INVISIBLE);
        } else if (postData.getPostType().equalsIgnoreCase("3")) {
            txt_suggest_edit.setVisibility(View.INVISIBLE);
            txt_compare_answer.setVisibility(View.GONE);
            txt_request_expert.setVisibility(View.GONE);
            txt_broadcast_to_all.setVisibility(View.GONE);
            txt_mute_conversation.setVisibility(View.GONE);
            txt_report_abuse.setVisibility(View.GONE);
        } else {
            txt_suggest_edit.setVisibility(View.VISIBLE);
        }

    }

    private void setTypeFace() {

        txt_suggest_edit.setTypeface(AppClass.lato_medium);
        txt_share.setTypeface(AppClass.lato_medium);
        txt_compare_answer.setTypeface(AppClass.lato_medium);
        txt_request_expert.setTypeface(AppClass.lato_medium);
        txt_broadcast_to_all.setTypeface(AppClass.lato_medium);
        txt_mute_conversation.setTypeface(AppClass.lato_medium);
        txt_report_abuse.setTypeface(AppClass.lato_medium);

    }

    private void setClickListener() {

        img_close.setOnClickListener(this);
        rl_main.setOnClickListener(this);
        txt_suggest_edit.setOnClickListener(this);
        txt_share.setOnClickListener(this);
        txt_compare_answer.setOnClickListener(this);
        txt_request_expert.setOnClickListener(this);
        txt_broadcast_to_all.setOnClickListener(this);
        txt_mute_conversation.setOnClickListener(this);
        txt_report_abuse.setOnClickListener(this);
    }

    private void reportAbuse() {

        String userId = AppClass.preferences.getUserId();

        ApiCall.getInstance().postReportAbuse(activity, userId, postId, "", new IApiCallback() {
            @Override
            public void onSuccess(Object data) {
                DialogUtils.openDialog(activity, (String) data, activity.getString(R.string.ok), "", new AlertDialogInterface() {
                    @Override
                    public void onNegativeBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }

                    @Override
                    public void onPositiveBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }
                });
            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {
                DialogUtils.openDialog(activity, errorMessage, activity.getString(R.string.ok), "", new AlertDialogInterface() {
                    @Override
                    public void onNegativeBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }

                    @Override
                    public void onPositiveBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }
                });
            }
        }, true);

    }

    private void sharePost() {

        String shareBody = "";

        if (postData.getPostType().equalsIgnoreCase("1")) {
            //Article
            shareBody = postData.getPostTitle() + "\n " + Utils.fromHtml(postData.getPostDescription());

            if (!postData.getImages().isEmpty() && StringUtils.isNotEmpty(postData.getImages().get(0).getImage_name()))
                shareBody = shareBody + "\n" + postData.getImages().get(0).getImage_name();


        } else if (postData.getPostType().equalsIgnoreCase("2")) {
            //Question
            shareBody = postData.getPostTitle() ;

        } else if (postData.getPostType().equalsIgnoreCase("3")) {
            //vv
            if (postData.getVvType().equalsIgnoreCase("1")) {
                shareBody = postData.getAudioPost().getAudioUrl();
            } else {
                shareBody = postData.getVideosPost().getVideoUrl();
            }
        } else {
            //RP
            shareBody =  postData.getPostTitle()+ "\n" + postData.getFiles().getFileName();
        }

        shareBody = shareBody+"\n\n"+activity.getString(R.string.play_store_app_url);

        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
        activity.startActivity(Intent.createChooser(sharingIntent, activity.getString(R.string.share_using)));

    }

    private void broadcastPost() {
        String userId = AppClass.preferences.getUserId();
        ApiCall.getInstance().broadCastPost(activity, userId, postData.getPostId(), new IApiCallback() {
            @Override
            public void onSuccess(Object data) {

                AppClass.snackBarView.snackBarShow(activity, (String) data);

            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {
                DialogUtils.openDialog(activity, errorMessage, activity.getString(R.string.ok), "", new AlertDialogInterface() {
                    @Override
                    public void onNegativeBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }

                    @Override
                    public void onPositiveBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }
                });
            }
        }, true);

    }


}