package com.proclapp.home;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;

import com.proclapp.R;
import com.proclapp.home.adapter.ImageShowAdapter;
import com.proclapp.model.AllPostsModel;
import com.proclapp.utils.Utils;
import com.yarolegovich.discretescrollview.DiscreteScrollView;

import java.util.ArrayList;

public class ImagePagerActivity extends AppCompatActivity {

    DiscreteScrollView rv_images;
    AppCompatImageView ivClose;
    ArrayList<AllPostsModel.Images> images = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_pager);
        Utils.setStatusBarColor(getWindow(), this, R.color.black);
        rv_images = findViewById(R.id.rv_images);
        images = (ArrayList<AllPostsModel.Images>) getIntent().getSerializableExtra("imagesUrl");
        int scrollToPos = getIntent().getIntExtra("clickedpos", 0);
        rv_images.setAdapter(new ImageShowAdapter(this, images));
        ivClose = findViewById(R.id.iv_close);
        ivClose.setOnClickListener(view -> onBackPressed());
        rv_images.scrollToPosition(scrollToPos);

    }


}
