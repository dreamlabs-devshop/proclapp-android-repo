package com.proclapp.home;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.cardview.widget.CardView;

import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.advert.PostAdvActivity;
import com.proclapp.home.voice_video.SelectCategoryActivity;
import com.proclapp.utils.StaticData;

public class NewPostDialog extends Dialog {
    public static int PICK_VIDEO_REQUEST = 1;

    private Activity activity;

    private TextView txt_ask_your_question,
            txt_write_an_article,
            txt_make_request_voice_video,
            txt_add_advert,
            txt_add_video,
            txt_upload_research_paper;

    private LinearLayout ll_ask_question,
            ll_write_article,
            ll_add_advert,
            ll_make_request_voice_video,
            ll_upload_research_paper,
            ll_add_video,
            ll_main;

    private ImageView img_close;
    private CardView card_view_new_post;

    public NewPostDialog(Activity a) {
        super(a);
        this.activity = a;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_new_post);

        txt_ask_your_question = findViewById(R.id.txt_ask_your_question);
        txt_write_an_article = findViewById(R.id.txt_write_an_article);
        txt_make_request_voice_video = findViewById(R.id.txt_make_request_voice_video);
        txt_upload_research_paper = findViewById(R.id.txt_upload_research_paper);
        txt_add_advert = findViewById(R.id.txt_add_advert);
        txt_add_video = findViewById(R.id.txt_add_video);

        img_close = findViewById(R.id.img_close);
        card_view_new_post = findViewById(R.id.card_view_new_post);
        card_view_new_post.setBackgroundResource(R.drawable.rounded_bg_new_post);

        ll_ask_question = findViewById(R.id.ll_ask_question);
        ll_add_advert = findViewById(R.id.ll_add_advert);
        ll_write_article = findViewById(R.id.ll_write_article);
        ll_make_request_voice_video = findViewById(R.id.ll_make_request_voice_video);
        ll_upload_research_paper = findViewById(R.id.ll_upload_research_paper);
        ll_add_video = findViewById(R.id.ll_add_video);
        ll_main = findViewById(R.id.ll_main);

        getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        //getWindow().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(activity,R.color.color_28black)));
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getWindow().getAttributes().windowAnimations = R.style.DialogAnimation_right;


        txt_ask_your_question.setTypeface(AppClass.lato_medium);
        txt_write_an_article.setTypeface(AppClass.lato_medium);
        txt_make_request_voice_video.setTypeface(AppClass.lato_medium);
        txt_upload_research_paper.setTypeface(AppClass.lato_medium);

        ll_ask_question.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.startActivity(new Intent(activity, PostArticleActivity.class)
                        .putExtra("type", StaticData.question));
                dismiss();
            }
        });

        ll_write_article.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.startActivity(new Intent(activity, PostArticleActivity.class).putExtra("type", StaticData.article));
                dismiss();
            }
        });

        ll_make_request_voice_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                activity.startActivity(new Intent(activity, SelectCategoryAndExpert.class));
                dismiss();
            }
        });


        ll_upload_research_paper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.startActivity(new Intent(activity, PostArticleActivity.class).putExtra("type", StaticData.research_paper));
                dismiss();
            }
        });

        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dismiss();
            }
        });

        ll_add_advert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.startActivity(new Intent(activity, PostAdvActivity.class));
            }
        });

        ll_add_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent videoIntent = new Intent(Intent.ACTION_PICK);
                videoIntent.setType("video/*");
                activity.startActivityForResult(Intent.createChooser(videoIntent, "Select Video"), PICK_VIDEO_REQUEST);
            }
        });
    }
}