package com.proclapp.home;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.home.adapter.NotificationAdapter;
import com.proclapp.home.question.QuestionDetailsActivity;
import com.proclapp.home.voice_video.VVExpertReplyActivity;
import com.proclapp.interfaces.RecyclerClickListener;
import com.proclapp.model.AllPostsModel;
import com.proclapp.model.NotificationModel;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.AlertDialogInterface;
import com.proclapp.utils.DialogUtils;
import com.proclapp.utils.StringUtils;
import com.proclapp.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class NotificationActivity extends AppCompatActivity implements View.OnClickListener, SwipyRefreshLayout.OnRefreshListener {

    private RecyclerView rv_notification_list;
    private ArrayList<NotificationModel> notificationList = new ArrayList<>();
    private ImageView img_back;
    private SwipyRefreshLayout sr_notification;

    private RelativeLayout rl_progress;

    private int offset = 0;
    private boolean isServiceCalling = false, isLastPage = false;
    private String notiType = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        Utils.setStatusBarColor(getWindow(), this, R.color.white);
        initViews();
        sr_notification.setOnRefreshListener(this);
        sr_notification.setColorSchemeColors(getResources().getColor(R.color.color_0c6984), getResources().getColor(R.color.black));
        setUpRecyclerView();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.img_back:
                onBackPressed();
                break;

        }
    }

    @Override
    public void onRefresh(SwipyRefreshLayoutDirection direction) {
        offset = direction == SwipyRefreshLayoutDirection.TOP ? 0 : offset + 20;
        getNotificationList();
    }

    private void initViews() {
        rv_notification_list = findViewById(R.id.rv_notification_list);
        sr_notification = findViewById(R.id.sr_notification);
        img_back = findViewById(R.id.img_back);
        rl_progress = findViewById(R.id.rl_progress);

        img_back.setOnClickListener(this);

    }

    private void setUpRecyclerView() {

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        rv_notification_list.setLayoutManager(layoutManager);
        rv_notification_list.setAdapter(new NotificationAdapter(this, notificationList, new RecyclerClickListener() {
            @Override
            public void onItemClick(int pos, String tag) {
                notiType = notificationList.get(pos).getNotificationType();
                if (tag.equalsIgnoreCase("post")) {
                    if (StringUtils.isNotEmpty(notificationList.get(pos).getPost_id()))
                        getPostDetails(notificationList.get(pos).getPost_id(), notificationList.get(pos).getPostAnswersId());
                } else {
                    updatePostFromSuggestion(notificationList.get(pos).getNotificationId(), tag);
                }
            }
        }));

        sr_notification.post(new Runnable() {
            @Override
            public void run() {
                sr_notification.setRefreshing(true);
                onRefresh(SwipyRefreshLayoutDirection.TOP);
            }
        });


        rv_notification_list.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                int visibleItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int firstVisibleItemPosition = layoutManager.findLastVisibleItemPosition();

                if (!isServiceCalling && !isLastPage) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0) {
                        sr_notification.post(new Runnable() {
                            @Override
                            public void run() {
                                sr_notification.setRefreshing(true);
                                onRefresh(SwipyRefreshLayoutDirection.BOTTOM);
                            }
                        });

                    }
                }
            }
        });


    }

    private void getNotificationList() {

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("offset", String.valueOf(offset));

        ApiCall.getInstance().getNotificationList(this, request, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {

                JsonArray response = (JsonArray) data;


                ArrayList<NotificationModel> tempNotificationList = new Gson().fromJson(response.toString(),
                        new TypeToken<List<NotificationModel>>() {
                        }.getType());

                if (offset == 0) {
                    notificationList.clear();
                }
                notificationList.addAll(tempNotificationList);
                rv_notification_list.getAdapter().notifyDataSetChanged();

                sr_notification.setRefreshing(false);
                isLastPage = false;
                isServiceCalling = false;
                rv_notification_list.setVisibility(View.VISIBLE);

            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {
                sr_notification.setRefreshing(false);
                isLastPage = true;
                isServiceCalling = false;
                if (StringUtils.isNotEmpty(errorMessage)) {

                    if (errorMessage.equalsIgnoreCase("No notifications.") && offset != 0) {

                    } else {

                        DialogUtils.openDialog(NotificationActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                            @Override
                            public void onNegativeBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }

                            @Override
                            public void onPositiveBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }
                        });
                    }
                }
            }
        }, false);


    }

    private void getPostDetails(String postId, String answerId) {
        rl_progress.setVisibility(View.VISIBLE);
        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("post_id", postId);

        ApiCall.getInstance().getPostDetails(this, request, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {
                rl_progress.setVisibility(View.GONE);
                AllPostsModel model = (AllPostsModel) data;


                if (StringUtils.isNotEmpty(model.getPostId())) {

                    String openFor = "";

                    if (model.getPostType().equalsIgnoreCase("2")) {
                        openFor = "qa";
                    } else if (model.getPostType().equalsIgnoreCase("4")) {
                        openFor = "rp";
                    }

                    Intent intent = null;


                    if (model.getPostType().equalsIgnoreCase("2") ||
                            model.getPostType().equalsIgnoreCase("4")) {

                        intent = new Intent(NotificationActivity.this, QuestionDetailsActivity.class);
                        intent.putExtra("openfrom", openFor);
                        intent.putExtra("questionDetails", (AllPostsModel) data);

                        if (notiType.equalsIgnoreCase("11") ||
                                notiType.equalsIgnoreCase("23") ||
                                notiType.equalsIgnoreCase("6")) {
                            intent.putExtra("notiType", notiType);
                            intent.putExtra("answerId", answerId);
                        }

                        startActivity(intent);

                    } else if (model.getPostType().equalsIgnoreCase("1")) {
                        intent = new Intent(NotificationActivity.this, DiscussActivity.class);
                        intent.putExtra("articleDetails", (AllPostsModel) data);

                        if (notiType.equalsIgnoreCase("11") ||
                                notiType.equalsIgnoreCase("23") ||
                                notiType.equalsIgnoreCase("6")) {
                            intent.putExtra("notiType", notiType);
                            intent.putExtra("answerId", answerId);
                        }

                        startActivity(intent);

                    } else if (model.getPostType().equalsIgnoreCase("3")) {
                        intent = new Intent(NotificationActivity.this, VVExpertReplyActivity.class);
                        intent.putExtra("vvdata", (AllPostsModel) data);
                        if (notiType.equalsIgnoreCase("11  ") ||
                                notiType.equalsIgnoreCase("23") ||
                                notiType.equalsIgnoreCase("6")) {
                            intent.putExtra("notiType", notiType);
                            intent.putExtra("answerId", answerId);
                        }

                        startActivity(intent);
                    }
                }

            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {
                rl_progress.setVisibility(View.GONE);
                DialogUtils.openDialog(NotificationActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                    @Override
                    public void onNegativeBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }

                    @Override
                    public void onPositiveBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }
                });
            }
        }, false);


    }


    private void updatePostFromSuggestion(String notificationId, String status) {

        ApiCall.getInstance().updatePostFromSuggestion(this, AppClass.preferences.getUserId(), notificationId, status, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {

                sr_notification.setRefreshing(true);
                onRefresh(SwipyRefreshLayoutDirection.TOP);

            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {
                if (StringUtils.isNotEmpty(errorMessage)) {
                    DialogUtils.openDialog(NotificationActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                        @Override
                        public void onNegativeBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }

                        @Override
                        public void onPositiveBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }
                    });
                }
            }
        }, true);


    }


}
