package com.proclapp.home;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import com.github.barteksc.pdfviewer.PDFView;
import com.proclapp.R;

public class PdfReader extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf_reader);

        Toolbar pdf_toolbar = findViewById(R.id.pdf_toolbar);
        PDFView pdfView = findViewById(R.id.pdfView);

        setSupportActionBar(pdf_toolbar);
        getSupportActionBar().setTitle("PDF Reader");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Uri uri = Uri.parse(getIntent().getStringExtra("file"));

        // Log.d("PATH",urlPath);

        pdfView.fromUri(uri).load();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}