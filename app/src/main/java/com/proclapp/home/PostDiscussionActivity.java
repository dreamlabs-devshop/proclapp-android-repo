package com.proclapp.home;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;

import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chinalwb.are.AREditText;
import com.chinalwb.are.AREditor;
import com.chinalwb.are.ArePreferences;
import com.chinalwb.are.styles.toolbar.IARE_Toolbar;
import com.chinalwb.are.styles.toolitems.ARE_ToolItem_AlignmentCenter;
import com.chinalwb.are.styles.toolitems.ARE_ToolItem_AlignmentLeft;
import com.chinalwb.are.styles.toolitems.ARE_ToolItem_AlignmentRight;
import com.chinalwb.are.styles.toolitems.ARE_ToolItem_At;
import com.chinalwb.are.styles.toolitems.ARE_ToolItem_Bold;
import com.chinalwb.are.styles.toolitems.ARE_ToolItem_FontColor;
import com.chinalwb.are.styles.toolitems.ARE_ToolItem_FontSize;
import com.chinalwb.are.styles.toolitems.ARE_ToolItem_Hr;
import com.chinalwb.are.styles.toolitems.ARE_ToolItem_Image;
import com.chinalwb.are.styles.toolitems.ARE_ToolItem_Italic;
import com.chinalwb.are.styles.toolitems.ARE_ToolItem_Link;
import com.chinalwb.are.styles.toolitems.ARE_ToolItem_ListBullet;
import com.chinalwb.are.styles.toolitems.ARE_ToolItem_ListNumber;
import com.chinalwb.are.styles.toolitems.ARE_ToolItem_Quote;
import com.chinalwb.are.styles.toolitems.ARE_ToolItem_Strikethrough;
import com.chinalwb.are.styles.toolitems.ARE_ToolItem_Subscript;
import com.chinalwb.are.styles.toolitems.ARE_ToolItem_Superscript;
import com.chinalwb.are.styles.toolitems.ARE_ToolItem_Underline;
import com.chinalwb.are.styles.toolitems.ARE_ToolItem_Video;
import com.chinalwb.are.styles.toolitems.IARE_ToolItem;
import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.model.AnswerModel;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.AlertDialogInterface;
import com.proclapp.utils.DialogUtils;
import com.proclapp.utils.KeyBoardUtils;
import com.proclapp.utils.StringUtils;
import com.proclapp.utils.Utils;

import java.util.HashMap;

import okhttp3.RequestBody;

public class PostDiscussionActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int REQ_WRITE_EXTERNAL_STORAGE = 10000;
    private String postId = "";

    private IARE_Toolbar mToolbar;
    private AREditText mEditText;
    private AREditor arEditor;
    private TextView txt_post;
    private ImageView img_back;
    private AnswerModel answerModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_discussion);

        Utils.setStatusBarColor(getWindow(), this, R.color.white);

        initViews();
        setClickListener();
        addEditorOptions();

        new ArePreferences(this).storeUserId(AppClass.preferences.getUserId());
        new ArePreferences(this).storeToken(AppClass.preferences.getToken());

        if (getIntent() != null)
            postId = getIntent().getStringExtra("postId");

        if (getIntent() != null && getIntent().getSerializableExtra("discuss") != null) {
            answerModel = (AnswerModel) getIntent().getSerializableExtra("discuss");

            mEditText.fromHtml(answerModel.answer);

        }


    }

    @Override
    public void onClick(View view) {

        KeyBoardUtils.closeSoftKeyboard(this);

        switch (view.getId()) {

            case R.id.img_back:
                onBackPressed();
                break;

            case R.id.txt_post:

                txt_post.setEnabled(false);

                if (StringUtils.isNotEmpty(mEditText.getText().toString().trim())) {

                    if (AppClass.isInternetConnectionAvailable()) {
                        if (answerModel != null)
                            callAPIEditAnswer();
                        else {
                            if (getIntent().getStringExtra("isPostVV") != null)
                                callAPIAddVVComment();
                            else
                                callAPIAddAnswer();
                        }
                    } else {
                        AppClass.snackBarView.snackBarShow(PostDiscussionActivity.this, getString(R.string.nonetwork));
                    }
                }

                break;
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQ_WRITE_EXTERNAL_STORAGE) {
            String html = this.arEditor.getHtml();
            Utils.saveHtml(this, html);
            return;
        }


        mToolbar.onActivityResult(requestCode, resultCode, data);

    }

    private void initViews() {
        mToolbar = this.findViewById(R.id.areToolbar);
        mEditText = this.findViewById(R.id.yView);
        txt_post = findViewById(R.id.txt_post);
        img_back = findViewById(R.id.img_back);
    }

    @SuppressLint("ClickableViewAccessibility")
    private void setClickListener() {

        txt_post.setOnClickListener(this);
        img_back.setOnClickListener(this);


        mEditText.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {
                if (mEditText.hasFocus()) {
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    if ((event.getAction() & MotionEvent.ACTION_MASK) == MotionEvent.ACTION_SCROLL) {
                        v.getParent().requestDisallowInterceptTouchEvent(false);
                        return true;
                    }
                }
                return false;
            }
        });

    }

    private void addEditorOptions() {


        IARE_ToolItem size = new ARE_ToolItem_FontSize();
        IARE_ToolItem bold = new ARE_ToolItem_Bold();
        IARE_ToolItem italic = new ARE_ToolItem_Italic();
        IARE_ToolItem underline = new ARE_ToolItem_Underline();
        IARE_ToolItem strikethrough = new ARE_ToolItem_Strikethrough();
        IARE_ToolItem quote = new ARE_ToolItem_Quote();
        IARE_ToolItem listNumber = new ARE_ToolItem_ListNumber();
        IARE_ToolItem listBullet = new ARE_ToolItem_ListBullet();
        IARE_ToolItem hr = new ARE_ToolItem_Hr();
        IARE_ToolItem link = new ARE_ToolItem_Link();
        IARE_ToolItem subscript = new ARE_ToolItem_Subscript();
        IARE_ToolItem superscript = new ARE_ToolItem_Superscript();
        IARE_ToolItem left = new ARE_ToolItem_AlignmentLeft();
        IARE_ToolItem center = new ARE_ToolItem_AlignmentCenter();
        IARE_ToolItem right = new ARE_ToolItem_AlignmentRight();
        IARE_ToolItem image = new ARE_ToolItem_Image();
        IARE_ToolItem video = new ARE_ToolItem_Video();
        IARE_ToolItem at = new ARE_ToolItem_At();
        IARE_ToolItem color = new ARE_ToolItem_FontColor();

        mToolbar.addToolbarItem(image);
        mToolbar.addToolbarItem(bold);
        mToolbar.addToolbarItem(italic);
        mToolbar.addToolbarItem(underline);
        mToolbar.addToolbarItem(link);
        mToolbar.addToolbarItem(color);
        mToolbar.addToolbarItem(size);
        mToolbar.addToolbarItem(quote);
        mToolbar.addToolbarItem(listNumber);
        mToolbar.addToolbarItem(listBullet);
        mToolbar.addToolbarItem(left);
        mToolbar.addToolbarItem(center);
        mToolbar.addToolbarItem(right);
        mToolbar.addToolbarItem(at);
//        mToolbar.addToolbarItem(hr);
//        mToolbar.addToolbarItem(strikethrough);
//        mToolbar.addToolbarItem(video);

        mEditText.setToolbar(mToolbar);

    }


    private void callAPIAddVVComment() {

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("post_id", postId);
        request.put("answer", mEditText.getText().toString());

        ApiCall.getInstance().addAnswerPost(PostDiscussionActivity.this, request, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {

                DiscussActivity.isDiscussionPost = true;

                String response = (String) data;

                AppClass.snackBarView.snackBarShow(PostDiscussionActivity.this, "Comment added");

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        finish();
                        txt_post.setEnabled(true);
                    }
                }, 1000);


            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {

                txt_post.setEnabled(true);
                if (StringUtils.isNotEmpty(errorMessage)) {
                    DialogUtils.openDialog(PostDiscussionActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                        @Override
                        public void onNegativeBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }

                        @Override
                        public void onPositiveBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }
                    });
                }

            }
        }, true);

    }


    private void callAPIAddAnswer() {

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("post_id", postId);
        request.put("answer", mEditText.getHtml());

        ApiCall.getInstance().addAnswerPost(PostDiscussionActivity.this, request, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {

                DiscussActivity.isDiscussionPost = true;

                String response = (String) data;

                AppClass.snackBarView.snackBarShow(PostDiscussionActivity.this, "Discussion added");

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        finish();
                        txt_post.setEnabled(true);
                    }
                }, 1000);


            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {

                txt_post.setEnabled(true);
                if (StringUtils.isNotEmpty(errorMessage)) {
                    DialogUtils.openDialog(PostDiscussionActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                        @Override
                        public void onNegativeBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }

                        @Override
                        public void onPositiveBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }
                    });
                }

            }
        }, true);

    }

    private void callAPIEditAnswer() {

        HashMap<String, RequestBody> request = new HashMap<>();
        request.put("user_id", ApiCall.getInstance().getRequestBodyOfString(AppClass.preferences.getUserId()));
        request.put("post_answers_id", ApiCall.getInstance().getRequestBodyOfString(answerModel.postAnswersId));
        request.put("answer", ApiCall.getInstance().getRequestBodyOfString(mEditText.getHtml()));


        ApiCall.getInstance().editAnswer(PostDiscussionActivity.this, request, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {

                DiscussActivity.isDiscussionPost = true;

                String response = (String) data;

                AppClass.snackBarView.snackBarShow(PostDiscussionActivity.this, response);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        finish();
                        txt_post.setEnabled(true);
                    }
                }, 1000);


            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {

                txt_post.setEnabled(true);
                if (StringUtils.isNotEmpty(errorMessage)) {
                    DialogUtils.openDialog(PostDiscussionActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                        @Override
                        public void onNegativeBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }

                        @Override
                        public void onPositiveBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }
                    });
                }

            }
        }, true);

    }


}
