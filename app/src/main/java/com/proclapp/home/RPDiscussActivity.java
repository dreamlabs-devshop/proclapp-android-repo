package com.proclapp.home;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.home.question.AnswerListAdapter;
import com.proclapp.home.question.PostAnswerActivity;
import com.proclapp.interfaces.RecyclerClickListener;
import com.proclapp.model.AllPostsModel;
import com.proclapp.model.AnswerModel;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.DateTimeUtils;
import com.proclapp.utils.ImageLoadUtils;
import com.proclapp.utils.Logger;

import java.util.ArrayList;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class RPDiscussActivity extends AppCompatActivity implements View.OnClickListener {


    public static boolean isAnswerAdded = false;
    private AllPostsModel researchPaperDetails;

    private ImageView
            img_back,
            img_doc;
    private TextView
            txt_post,
            txt_user_name,
            txt_user_place,
            txt_date,
            txt_description,
            txt_doc_name,
            txt_doc_size;
    private CircleImageView img_user_profile;
    private EditText edt_write_ur_answer;
    private RecyclerView rv_answer_list;
    private ArrayList<AnswerModel> answerLists = new ArrayList<>();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rpdiscuss);
        initView();
        researchPaperDetails = (AllPostsModel) getIntent().getSerializableExtra("researchPaperDetails");

        setData();

        setUpRecyclerView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isAnswerAdded)
            callAPIGetAnswer();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.txt_post:
                startActivity(new Intent(this, RPPostDiscussActivity.class)
                        .putExtra("oprnFrom", "rp")
                        .putExtra("researchPaperDetails", researchPaperDetails));

                break;

        }
    }

    private void initView() {
        img_back = findViewById(R.id.img_back);
        img_doc = findViewById(R.id.img_doc);

        txt_post = findViewById(R.id.txt_post);
        txt_user_name = findViewById(R.id.txt_user_name);
        txt_user_place = findViewById(R.id.txt_user_place);
        txt_date = findViewById(R.id.txt_date);
        txt_description = findViewById(R.id.txt_description);
        txt_doc_name = findViewById(R.id.txt_doc_name);
        txt_doc_size = findViewById(R.id.txt_doc_size);
        rv_answer_list = findViewById(R.id.rv_answer_list);

        img_user_profile = findViewById(R.id.img_user_profile);
        edt_write_ur_answer = findViewById(R.id.edt_write_ur_answer);

        img_back.setOnClickListener(this);
        txt_post.setOnClickListener(this);
    }

    private void setData() {
        txt_user_name.setText(researchPaperDetails.getAuthor());
        txt_description.setText(Html.fromHtml(researchPaperDetails.getPostTitle()));
        txt_date.setText(DateTimeUtils.changeDateTimeFormat("yyyy-MM-dd HH:mm:ss", "dd MMM", researchPaperDetails.getPostDate()));
        txt_doc_name.setText(researchPaperDetails.getFiles().getOriFileName());
        txt_doc_size.setText(researchPaperDetails.getFiles().getFileSize());


        ImageLoadUtils.imageLoad(RPDiscussActivity.this,
                img_user_profile,
                researchPaperDetails.getProfileImage(),
                R.drawable.menu_user_ph);
    }

    private void setUpRecyclerView() {
        rv_answer_list.setLayoutManager(new LinearLayoutManager(this));
        rv_answer_list.setAdapter(new AnswerListAdapter(RPDiscussActivity.this,"rp" ,answerLists,  new RecyclerClickListener() {
            @Override
            public void onItemClick(int pos, String tag) {
                if (tag.equalsIgnoreCase("edit")){
                    startActivity(new Intent(RPDiscussActivity.this, PostAnswerActivity.class)
                            .putExtra("researchPaperDetails", researchPaperDetails)
                            .putExtra("openfor", "edit")
                            .putExtra("postAnswer", answerLists.get(pos)));
                }else {

                }
            }
        }));

        callAPIGetAnswer();

    }

    private void callAPIGetAnswer() {

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("post_id", researchPaperDetails.getPostId());

        Logger.d("request:" + request);

        ApiCall.getInstance().getAnswerList(RPDiscussActivity.this,
                request, new IApiCallback() {
                    @Override
                    public void onSuccess(Object data) {

                        answerLists.clear();
                        answerLists.addAll((ArrayList<AnswerModel>) data);
                        rv_answer_list.setVisibility(View.VISIBLE);

                        rv_answer_list.getAdapter().notifyDataSetChanged();

                        isAnswerAdded = false;


                    }

                    @Override
                    public void onFailure(boolean isFalseFromService, String errorMessage) {

                        rv_answer_list.setVisibility(View.GONE);



               /* DialogUtils.openDialog(QuestionDetailsActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                    @Override
                    public void onNegativeBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }

                    @Override
                    public void onPositiveBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }
                });*/
                    }


                }, true);

    }


}
