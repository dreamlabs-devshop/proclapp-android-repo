package com.proclapp.home;

import android.content.DialogInterface;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.model.AllPostsModel;
import com.proclapp.model.AnswerModel;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.AlertDialogInterface;
import com.proclapp.utils.DateTimeUtils;
import com.proclapp.utils.DialogUtils;
import com.proclapp.utils.ImageLoadUtils;
import com.proclapp.utils.KeyBoardUtils;
import com.proclapp.utils.StringUtils;

import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class RPPostDiscussActivity extends AppCompatActivity implements View.OnClickListener {


    private ImageView
            img_back,
            img_doc;
    private TextView
            txt_post,
            txt_user_name,
            txt_user_place,
            txt_date,
            txt_description,
            txt_doc_name,
            txt_doc_size;
    private CircleImageView img_user_profile;
    private EditText edt_write_ur_answer;
    private RecyclerView rv_all_comments;
    private AllPostsModel researchPaperDetails;

    private String oprnFrom = "";

    private AnswerModel answerModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rppost_discuss);
        edt_write_ur_answer = findViewById(R.id.edt_write_ur_answer);
        oprnFrom = getIntent().getStringExtra("oprnFrom");

//        if (oprnFrom.equalsIgnoreCase("rp")) {
        researchPaperDetails = (AllPostsModel) getIntent().getSerializableExtra("researchPaperDetails");
        /*} else {
            answerModel = (AnswerModel) getIntent().getSerializableExtra("answerLists");
            edt_write_ur_answer.setEnabled(false);
            edt_write_ur_answer.setText(answerModel.answer);

        }*/

        initView();
        setData();
    }


    private void initView() {
        img_back = findViewById(R.id.img_back);
        img_doc = findViewById(R.id.img_doc);

        txt_post = findViewById(R.id.txt_post);
        txt_user_name = findViewById(R.id.txt_user_name);
        txt_user_place = findViewById(R.id.txt_user_place);
        txt_date = findViewById(R.id.txt_date);
        txt_description = findViewById(R.id.txt_description);
        txt_doc_name = findViewById(R.id.txt_doc_name);
        txt_doc_size = findViewById(R.id.txt_doc_size);
        rv_all_comments = findViewById(R.id.rv_all_comments);

        img_user_profile = findViewById(R.id.img_user_profile);


        img_back.setOnClickListener(this);
        txt_post.setOnClickListener(this);

        txt_doc_name.setTypeface(AppClass.lato_heavy);
        txt_doc_size.setTypeface(AppClass.lato_regular);
        txt_user_name.setTypeface(AppClass.lato_medium);
        txt_user_place.setTypeface(AppClass.lato_regular);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.img_back:

                onBackPressed();

                break;

            case R.id.txt_post:

                KeyBoardUtils.closeSoftKeyboard(RPPostDiscussActivity.this);

                if (validation()) {

                    if (AppClass.isInternetConnectionAvailable()) {
                        callAPIAddAnswer();
                    } else {
                        AppClass.snackBarView.snackBarShow(RPPostDiscussActivity.this, getString(R.string.nonetwork));
                    }
                }

                break;
        }
    }

    private void setData() {
        txt_user_name.setText(researchPaperDetails.getAuthor());
        txt_description.setText(Html.fromHtml(researchPaperDetails.getPostTitle()));
        txt_date.setText(DateTimeUtils.changeDateTimeFormat("yyyy-MM-dd HH:mm:ss", "dd MMM", researchPaperDetails.getPostDate()));
        txt_doc_name.setText(researchPaperDetails.getFiles().getOriFileName());
        txt_doc_size.setText(researchPaperDetails.getFiles().getFileSize());

        txt_user_place.setText(researchPaperDetails.getProfessionalQualification());

        ImageLoadUtils.imageLoad(RPPostDiscussActivity.this,
                img_user_profile,
                researchPaperDetails.getProfileImage(),
                R.drawable.menu_user_ph);
    }

    private boolean validation() {

        boolean isValid = true;

        if (edt_write_ur_answer.getText().toString().trim().isEmpty()) {
            isValid = false;
            AppClass.snackBarView.snackBarShow(RPPostDiscussActivity.this
                    , getResources().getString(R.string.vanswer)
                    , ContextCompat.getColor(RPPostDiscussActivity.this, R.color.red));
            edt_write_ur_answer.requestFocus();
        }

        return isValid;
    }


    private void callAPIAddAnswer() {

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("post_id", researchPaperDetails.getPostId());
        request.put("answer", edt_write_ur_answer.getText().toString().trim());

        ApiCall.getInstance().addAnswerPost(RPPostDiscussActivity.this, request, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {

                String response = (String) data;

                //AppClass.snackBarView.snackBarShow(PostArticleActivity.this, response);

                finish();
                RPDiscussActivity.isAnswerAdded = true;

                /*DialogUtils.openDialog(PostAnswerActivity.this, response, getString(R.string.ok), "", new AlertDialogInterface() {
                    @Override
                    public void onNegativeBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }

                    @Override
                    public void onPositiveBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                        finish();
                    }
                });*/
            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {
                if (StringUtils.isNotEmpty(errorMessage)) {
                    DialogUtils.openDialog(RPPostDiscussActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                        @Override
                        public void onNegativeBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }

                        @Override
                        public void onPositiveBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }
                    });
                }

            }
        }, true);

    }

}
