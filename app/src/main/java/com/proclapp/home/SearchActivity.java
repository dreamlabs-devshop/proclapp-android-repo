package com.proclapp.home;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.home.adapter.SearchResultAdapter;
import com.proclapp.home.adapter.SearchResultAdapterUser;
import com.proclapp.home.fragment.ArticleFragment;
import com.proclapp.home.fragment.MenuFragment;
import com.proclapp.home.fragment.QuestionFragment;
import com.proclapp.home.fragment.ResearchPaperFragment;
import com.proclapp.home.adapter.SectionsPagerAdapter;
import com.proclapp.home.fragment.SearchPostFragment;
import com.proclapp.home.fragment.SearchUserFragment;
import com.proclapp.home.fragment.VideoVoiceFragment;
import com.proclapp.model.AllPostsModel;
import com.proclapp.model.AnswerModel;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

public class SearchActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView img_back;
    private TextView txt_search,
            txt_choose_from_below;
    //            txt_no_data,
//            txt_nothing_search;
    private EditText edt_search;
    private ProgressBar pb_search;
    public ArrayList<AllPostsModel> allPostsLists = new ArrayList<>();
    public ArrayList<AnswerModel.SearchedUser> allUsersLists = new ArrayList<>();
    private Call<JsonObject> allPostServiceCalling;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        Utils.setStatusBarColor(getWindow(), this, R.color.white);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        initView();
        initStyle();
        initListener();
        initViewPager();
    }

    private void initView() {

        img_back = findViewById(R.id.img_back);

        txt_search = findViewById(R.id.txt_search);
        txt_choose_from_below = findViewById(R.id.txt_choose_from_below);
//        txt_nothing_search = findViewById(R.id.txt_nothing_search_post);
//        txt_no_data = findViewById(R.id.txt_no_data_post);
        edt_search = findViewById(R.id.edt_search);
        pb_search = findViewById(R.id.pb_search);
        textChangeListener();
    }

    private void initViewPager() {
        SectionsPagerAdapter adapter = new SectionsPagerAdapter(getSupportFragmentManager());

        adapter.addFragment(new SearchPostFragment());
        adapter.addFragment(new SearchUserFragment());

        ViewPager viewPager = findViewById(R.id.search_view_pager);
        viewPager.setAdapter(adapter);

        TabLayout tabLayout = findViewById(R.id.search_tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabTextColors(getResources().getColor(android.R.color.darker_gray), getResources().getColor(R.color.color_2fc0d1));

        tabLayout.getTabAt(0).setText(getResources().getString(R.string.posts));
        tabLayout.getTabAt(1).setText(getResources().getString(R.string.users));

    }

    private void initStyle() {
        txt_search.setTypeface(AppClass.lato_regular);
        txt_choose_from_below.setTypeface(AppClass.lato_regular);
//        txt_nothing_search.setTypeface(AppClass.lato_regular);
        edt_search.setTypeface(AppClass.lato_semibold);
    }


    private void textChangeListener() {

        edt_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                TextView txt_nothing_search = findViewById(R.id.txt_nothing_search_post);
                TextView txt_no_data = findViewById(R.id.txt_no_data_post);
                RecyclerView rv_search_list_post = findViewById(R.id.rv_search_list_post);

                TextView txt_nothing_search_user = findViewById(R.id.txt_nothing_search_user);
                TextView txt_no_data_user = findViewById(R.id.txt_no_data_user);
                RecyclerView rv_search_list_user = findViewById(R.id.rv_search_list_user);

                if (charSequence.length() > 0) {
                    txt_nothing_search.setVisibility(View.GONE);
                    rv_search_list_post.setVisibility(View.VISIBLE);
                    txt_no_data.setVisibility(View.GONE);

                    txt_nothing_search_user.setVisibility(View.GONE);
                    rv_search_list_user.setVisibility(View.VISIBLE);
                    txt_no_data_user.setVisibility(View.GONE);

                    callAPIGetPosts(charSequence.toString());
                } else {
                    txt_nothing_search.setVisibility(View.VISIBLE);
                    rv_search_list_post.setVisibility(View.GONE);

                    txt_nothing_search_user.setVisibility(View.VISIBLE);
                    rv_search_list_user.setVisibility(View.GONE);

                    pb_search.setVisibility(View.GONE);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    private void initListener() {

        img_back.setOnClickListener(this);
    }

    private void callAPIGetPosts(String title) {
        TextView txt_no_data_post = findViewById(R.id.txt_no_data_post);
        RecyclerView rv_search_list_post = findViewById(R.id.rv_search_list_post);

        TextView txt_no_data_user = findViewById(R.id.txt_no_data_user);
        RecyclerView rv_search_list_user = findViewById(R.id.rv_search_list_user);

        if (allPostServiceCalling != null && allPostServiceCalling.isExecuted())
            allPostServiceCalling.cancel();

        pb_search.setVisibility(View.VISIBLE);

        allPostServiceCalling = ApiCall.getInstance().getPosts(SearchActivity.this, AppClass.preferences.getUserId(), title, "", new IApiCallback() {
            @Override
            public void onSuccess(Object data) {

                try {

                    txt_no_data_post.setVisibility(View.GONE);
                    txt_no_data_user.setVisibility(View.GONE);
                    ArrayList<AllPostsModel> tempFilteredList = new Gson().fromJson(((JsonObject) data).getAsJsonArray("posts_list").toString(),
                            new TypeToken<List<AllPostsModel>>() {
                            }.getType());

                    ArrayList<AnswerModel.SearchedUser> tempUsersList = new Gson().fromJson(((JsonObject) data).getAsJsonArray("users_list").toString(),
                            new TypeToken<List<AnswerModel.SearchedUser>>() {
                            }.getType());


                    for (int i = 0; i < tempFilteredList.size(); i++) {
                        if (tempFilteredList.get(i).getsFriend().equalsIgnoreCase("1") ||
                                tempFilteredList.get(i).getsExpert().equalsIgnoreCase("1") ||
                                tempFilteredList.get(i).getTrTopics().equalsIgnoreCase("1")) {
                            tempFilteredList.remove(i);
                            i--;
                        }

                    }

                    allPostsLists.clear();
                    allPostsLists.addAll(tempFilteredList);

                    allUsersLists.clear();
                    allUsersLists.addAll(tempUsersList);

                    rv_search_list_post.setAdapter(new SearchResultAdapter(SearchActivity.this, allPostsLists));
                    rv_search_list_post.getAdapter().notifyDataSetChanged();

                    rv_search_list_user.setAdapter(new SearchResultAdapterUser(SearchActivity.this, allUsersLists));
                    rv_search_list_user.getAdapter().notifyDataSetChanged();
                    pb_search.setVisibility(View.GONE);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {
                if (errorMessage.equalsIgnoreCase("No posts")) {

                    txt_no_data_post.setVisibility(View.VISIBLE);
                    txt_no_data_post.setText(getString(R.string.no_result_found_for, edt_search.getText().toString()));
                    rv_search_list_post.setVisibility(View.GONE);

                    txt_no_data_user.setVisibility(View.VISIBLE);
                    txt_no_data_user.setText(getString(R.string.no_result_found_for, edt_search.getText().toString()));
                    rv_search_list_user.setVisibility(View.GONE);

                    pb_search.setVisibility(View.GONE);
                    allPostsLists.clear();
                    allUsersLists.clear();
                    rv_search_list_post.getAdapter().notifyDataSetChanged();
                    rv_search_list_user.getAdapter().notifyDataSetChanged();

                } else {

                   /* DialogUtils.openDialog(SearchActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                        @Override
                        public void onNegativeBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }

                        @Override
                        public void onPositiveBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }
                    });*/
                }

            }
        }, false);

    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.img_back:
                onBackPressed();
                break;
        }
    }
}
