package com.proclapp.home;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.expert.ExpertModel;
import com.proclapp.home.adapter.SelectExpertAdapter;
import com.proclapp.home.adapter.VoiceVideoCategoryAdapter;
import com.proclapp.home.voice_video.SelectExpertActivity;
import com.proclapp.home.voice_video.SelectVoiceVideoActivity;
import com.proclapp.model.CategoryModel;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.AlertDialogInterface;
import com.proclapp.utils.DialogUtils;
import com.proclapp.utils.Logger;
import com.proclapp.utils.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SelectCategoryAndExpert extends AppCompatActivity {
    private ImageView img_back;
    private TextView txt_next;
    private RecyclerView rv_select_category;
    private VoiceVideoCategoryAdapter categoryAdapter;
    private ArrayList<CategoryModel> categoryList = new ArrayList<>();
    private ArrayList<ExpertModel> expertList = new ArrayList<>();
    private RecyclerView rv_select_expert;
    private SelectExpertAdapter selectExpertAdapter;

    private String categoryId;
    private String expertId;

    private ArrayList<String> expertIdList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_category_and_expert);

        initView();

        setUpRecyclerViews();

        callAPICategoryList();

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        txt_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (categoryId == null) {
                    Toast.makeText(SelectCategoryAndExpert.this, getResources().getString(R.string.select_category), Toast.LENGTH_SHORT).show();
                } else if (expertId == null) {
                    Toast.makeText(SelectCategoryAndExpert.this, getResources().getString(R.string.select_a_mentor), Toast.LENGTH_SHORT).show();

                } else
                    startActivity(new Intent(SelectCategoryAndExpert.this, SelectVoiceVideoActivity.class)
                            .putExtra("categoryId", categoryId)
                            .putExtra("expertId", expertId));
            }
        });
    }

    private void initView() {
        rv_select_expert = findViewById(R.id.rv_select_expert);
        rv_select_category = findViewById(R.id.rv_select_category);
        img_back = findViewById(R.id.img_back);
        txt_next = findViewById(R.id.txt_next);
    }

    private void setUpRecyclerViews() {
        GridLayoutManager layoutManager = new GridLayoutManager(SelectCategoryAndExpert.this, 3);
        GridLayoutManager layoutManager2 = new GridLayoutManager(SelectCategoryAndExpert.this, 3);

        rv_select_expert.setLayoutManager(layoutManager);
        rv_select_category.setLayoutManager(layoutManager2);

        SelectExpertAdapter selectExpertAdapter = new SelectExpertAdapter(SelectCategoryAndExpert.this, expertList, expertId -> {
            SelectCategoryAndExpert.this.expertId = expertId;
            expertIdList.clear();
            expertIdList.add(expertId);
        });

        rv_select_expert.setAdapter(selectExpertAdapter);

        getExpertList();
    }

    private void callAPICategoryList() {

        if (AppClass.isInternetConnectionAvailable()) {

            HashMap<String, String> request = new HashMap<>();
            request.put("user_id", AppClass.preferences.getUserId());

            ApiCall.getInstance().getCategories(SelectCategoryAndExpert.this, request, new IApiCallback() {
                @Override
                public void onSuccess(Object data) {

                    categoryList.clear();
                    categoryList.addAll((ArrayList<CategoryModel>) data);

                    setCategoryList();

                }

                @Override
                public void onFailure(boolean isFalseFromService, String errorMessage) {
                    if (StringUtils.isNotEmpty(errorMessage)) {
                        DialogUtils.openDialog(SelectCategoryAndExpert.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                            @Override
                            public void onNegativeBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }

                            @Override
                            public void onPositiveBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }
                        });
                    }
                }
            }, true);


        } else {
            AppClass.snackBarView.snackBarShow(SelectCategoryAndExpert.this, getString(R.string.nonetwork));
        }
    }

    private void setCategoryList() {
        categoryAdapter = new VoiceVideoCategoryAdapter(SelectCategoryAndExpert.this, categoryList, new VoiceVideoCategoryAdapter.CategorySelection() {
            @Override
            public void onCategorySelection(String categoryName, String categoryId) {

                Logger.e("categoryName " + categoryName + " categoryId " + categoryId);

                SelectCategoryAndExpert.this.categoryId = categoryId;

            }
        });
        rv_select_category.setAdapter(categoryAdapter);
    }

    private void getExpertList() {

        ApiCall.getInstance().getExpertList(SelectCategoryAndExpert.this, AppClass.preferences.getUserId(), new IApiCallback() {
            @Override
            public void onSuccess(Object data) {
                JsonArray response = (JsonArray) data;

                ArrayList<ExpertModel> tempExpertList = new Gson().fromJson(response.toString(),
                        new TypeToken<List<ExpertModel>>() {
                        }.getType());


                expertList.clear();
                expertList.addAll(tempExpertList);
                rv_select_expert.getAdapter().notifyDataSetChanged();


            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {

                if (StringUtils.isNotEmpty(errorMessage)) {
                    DialogUtils.openDialog(SelectCategoryAndExpert.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                        @Override
                        public void onNegativeBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }

                        @Override
                        public void onPositiveBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }
                    });
                }

            }
        }, true);


    }
}