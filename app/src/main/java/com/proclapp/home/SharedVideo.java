package com.proclapp.home;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.expert.ExpertModel;
import com.proclapp.home.adapter.SelectExpertAdapter;
import com.proclapp.home.adapter.VoiceVideoCategoryAdapter;
import com.proclapp.home.voice_video.SelectCategoryActivity;
import com.proclapp.home.voice_video.SelectExpertActivity;
import com.proclapp.home.voice_video.VideoRecorderActivity;
import com.proclapp.model.CategoryModel;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.AlertDialogInterface;
import com.proclapp.utils.DialogUtils;
import com.proclapp.utils.Logger;
import com.proclapp.utils.StringUtils;
import com.zyf.vc.VideoPreferences;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.RequestBody;

public class SharedVideo extends AppCompatActivity {

    // private RecyclerView rv_select_expert, rv_select_category;
    private EditText et_description;
    private TextView btn_add_video;
    private Uri mVideoIntentUri;
    private VoiceVideoCategoryAdapter categoryAdapter;
    private ArrayList<CategoryModel> categoryList = new ArrayList<>();
    private ArrayList<ExpertModel> expertList = new ArrayList<>();
    private ArrayList<String> expertIdList = new ArrayList<>();
    private String expertId;
    private String categoryId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shared_video);

        initView();

        // setUpRecyclerViews();
        handleVideoIntent();

        //callAPICategoryList();

        btn_add_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addVideo();
//                if (expertId == null) {
//                    Toast.makeText(SharedVideo.this, getResources().getString(R.string.select_a_mentor), Toast.LENGTH_SHORT).show();
//                } else if (categoryId == null) {
//                    Toast.makeText(SharedVideo.this, getResources().getString(R.string.select_category), Toast.LENGTH_SHORT).show();
//                } else {
//                    addVideo();
//                }
            }
        });
    }

    private void initView() {
//        rv_select_expert = findViewById(R.id.rv_select_expert);
//        rv_select_category = findViewById(R.id.rv_select_category);
        et_description = findViewById(R.id.edit_txt_description);
        btn_add_video = findViewById(R.id.btn_add_video);
    }

    private void setUpRecyclerViews() {
        GridLayoutManager layoutManager = new GridLayoutManager(SharedVideo.this, 3);
        GridLayoutManager layoutManager2 = new GridLayoutManager(SharedVideo.this, 3);

//        rv_select_expert.setLayoutManager(layoutManager);
//        rv_select_category.setLayoutManager(layoutManager2);

        SelectExpertAdapter selectExpertAdapter = new SelectExpertAdapter(SharedVideo.this, expertList, expertId -> {
            SharedVideo.this.expertId = expertId;
            expertIdList.clear();
            expertIdList.add(expertId);
        });

        // rv_select_expert.setAdapter(selectExpertAdapter);

        //getExpertList();
    }

    private void handleVideoIntent() {
        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();

        handleSendVideo(intent); // Handle single image being sent

//        if ((Intent.ACTION_SEND.equals(action) || Intent.ACTION_GET_CONTENT.equals(action)) && type != null) {
//            if (type.startsWith("video/")) {
//                handleSendVideo(intent); // Handle single image being sent
//            }
//        }
    }

    private void handleSendVideo(Intent intent) {
        if (intent.getStringExtra("source") != null && intent.getStringExtra("source").equals("home"))
            mVideoIntentUri = intent.getData();
        else
            mVideoIntentUri = intent.getParcelableExtra(Intent.EXTRA_STREAM);

        if (mVideoIntentUri != null) {
            // Update UI to reflect image being shared
            btn_add_video.setVisibility(View.VISIBLE);
        } else {
            btn_add_video.setVisibility(View.GONE);
        }
    }

    private void addVideo() {
        if (mVideoIntentUri == null) {
            Toast.makeText(this, "No video selected", Toast.LENGTH_SHORT).show();
            return;
        }
        Log.e("VIDEO TEST", "adding Video");
        MediaPlayer mp = MediaPlayer.create(this, mVideoIntentUri);
        int duration = mp.getDuration();
        mp.release();
        Log.e("DURATION", String.valueOf(duration));


        String hour, min, sec;

        if (((int) TimeUnit.MILLISECONDS.toHours(duration)) < 10) {
            hour = "0" + TimeUnit.MILLISECONDS.toHours(duration);
        } else {
            hour = "" + TimeUnit.MILLISECONDS.toHours(duration);
        }

        if (((int) TimeUnit.MILLISECONDS.toMinutes(duration)) < 10) {
            min = "0" + TimeUnit.MILLISECONDS.toMinutes(duration);
        } else {
            min = "" + TimeUnit.MILLISECONDS.toMinutes(duration);
        }

        if (((int) TimeUnit.MILLISECONDS.toMinutes(duration)) < 10) {
            sec = "0" + (TimeUnit.MILLISECONDS.toSeconds(duration) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(duration)));
        } else {
            sec = "" + (TimeUnit.MILLISECONDS.toSeconds(duration) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(duration)));
        }

        String time;
        if (((int) TimeUnit.MILLISECONDS.toHours(duration)) > 0) {
            time = hour + ":" + min + ":" + sec;
        } else {
            time = min + ":" + sec;
        }


        Log.e("Duration ", time);

        String userId = new VideoPreferences(this).getUserId();

        HashMap<String, RequestBody> request = new HashMap<>();
        request.put("user_id", RequestBody.create(MediaType.parse("multipart/form-data"), userId));
        request.put("vv_type", RequestBody.create(MediaType.parse("multipart/form-data"), "2"));
//        request.put("post_category", RequestBody.create(MediaType.parse("multipart/form-data"), categoryId));
//        request.put("expert", RequestBody.create(MediaType.parse("multipart/form-data"), expertId));
        request.put("post_description", RequestBody.create(MediaType.parse("multipart/form-data"), et_description.getText().toString()));
        request.put("post_type", RequestBody.create(MediaType.parse("multipart/form-data"), "3"));

        com.zyf.vc.retrofit.ApiCall.getInstance(this).addVVPost(this, request, getPath(mVideoIntentUri), new com.zyf.vc.retrofit.IApiCallback() {
            @Override
            public void onSuccess(Object data) {

                Toast.makeText(SharedVideo.this, "Video uploded successfully.", Toast.LENGTH_LONG).show();

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        onBackPressed();
                    }
                }, 1000);


            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {

                Toast.makeText(SharedVideo.this, errorMessage, Toast.LENGTH_LONG).show();

            }
        }, true);

    }

    private void callAPICategoryList() {

        if (AppClass.isInternetConnectionAvailable()) {

            HashMap<String, String> request = new HashMap<>();
            request.put("user_id", AppClass.preferences.getUserId());

            ApiCall.getInstance().getCategories(SharedVideo.this, request, new IApiCallback() {
                @Override
                public void onSuccess(Object data) {

                    categoryList.clear();
                    categoryList.addAll((ArrayList<CategoryModel>) data);

                    setCategoryList();

                }

                @Override
                public void onFailure(boolean isFalseFromService, String errorMessage) {
                    if (StringUtils.isNotEmpty(errorMessage)) {
                        DialogUtils.openDialog(SharedVideo.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                            @Override
                            public void onNegativeBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }

                            @Override
                            public void onPositiveBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }
                        });
                    }
                }
            }, true);


        } else {
            AppClass.snackBarView.snackBarShow(SharedVideo.this, getString(R.string.nonetwork));
        }


    }

    public String getPath(Uri uri) {

        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();

        return cursor.getString(column_index);
    }

    private void getExpertList() {

        ApiCall.getInstance().getExpertList(SharedVideo.this, AppClass.preferences.getUserId(), new IApiCallback() {
            @Override
            public void onSuccess(Object data) {
                JsonArray response = (JsonArray) data;

                ArrayList<ExpertModel> tempExpertList = new Gson().fromJson(response.toString(),
                        new TypeToken<List<ExpertModel>>() {
                        }.getType());


                expertList.clear();
                expertList.addAll(tempExpertList);
                //rv_select_expert.getAdapter().notifyDataSetChanged();


            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {

                if (StringUtils.isNotEmpty(errorMessage)) {
                    DialogUtils.openDialog(SharedVideo.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                        @Override
                        public void onNegativeBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }

                        @Override
                        public void onPositiveBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }
                    });
                }

            }
        }, true);


    }

    private void setCategoryList() {
        categoryAdapter = new VoiceVideoCategoryAdapter(SharedVideo.this, categoryList, new VoiceVideoCategoryAdapter.CategorySelection() {
            @Override
            public void onCategorySelection(String categoryName, String categoryId) {

                Logger.e("categoryName " + categoryName + " categoryId " + categoryId);

                SharedVideo.this.categoryId = categoryId;

            }
        });
        //rv_select_category.setAdapter(categoryAdapter);
    }
}