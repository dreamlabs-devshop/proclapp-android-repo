package com.proclapp.home;

import android.content.DialogInterface;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.model.AllPostsModel;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.AlertDialogInterface;
import com.proclapp.utils.DateTimeUtils;
import com.proclapp.utils.DialogUtils;
import com.proclapp.utils.ImageLoadUtils;
import com.proclapp.utils.KeyBoardUtils;
import com.proclapp.utils.StringUtils;

import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class SuggestEditActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView img_back;

    private TextView txt_suggest_edit,
            txt_post,
            txt_user_name,
            txt_user_place,
            txt_date,
            txt_question_value;

    private EditText edt_suggest_edit;

    private CircleImageView img_user_profile;
    private AllPostsModel postData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suggest_edit);

        postData = (AllPostsModel) getIntent().getSerializableExtra("postData");

        initView();
        initStyle();
        initListener();

        setData();
    }

    private void initView() {

        img_back = findViewById(R.id.img_back);
        txt_suggest_edit = findViewById(R.id.txt_suggest_edit);
        txt_post = findViewById(R.id.txt_post);
        txt_user_name = findViewById(R.id.txt_user_name);
        txt_user_place = findViewById(R.id.txt_user_place);
        txt_date = findViewById(R.id.txt_date);
        txt_question_value = findViewById(R.id.txt_question_value);
        edt_suggest_edit = findViewById(R.id.edt_suggest_edit);
        img_user_profile = findViewById(R.id.img_user_profile);

    }

    private void initStyle() {

        txt_suggest_edit.setTypeface(AppClass.lato_regular);
        txt_post.setTypeface(AppClass.lato_regular);
        txt_user_name.setTypeface(AppClass.lato_regular);
        txt_user_place.setTypeface(AppClass.lato_regular);
        txt_date.setTypeface(AppClass.lato_regular);
        txt_question_value.setTypeface(AppClass.lato_regular);
        edt_suggest_edit.setTypeface(AppClass.lato_regular);

    }

    private void initListener() {

        img_back.setOnClickListener(this);
        txt_post.setOnClickListener(this);
    }

    private void setData() {

        txt_user_name.setText(postData.getAuthor());
        txt_date.setText(DateTimeUtils.changeDateTimeFormat("yyyy-MM-dd HH:mm:ss",
                "dd MMM",
                postData.getPostDate()));

        txt_user_place.setText(postData.getProfessionalQualification());
        txt_question_value.setText(postData.getPostTitle());

        ImageLoadUtils.imageLoad(SuggestEditActivity.this,
                img_user_profile,
                postData.getProfileImage(),
                R.drawable.menu_user_ph);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.img_back:

                onBackPressed();

                break;

            case R.id.txt_post:

                KeyBoardUtils.closeSoftKeyboard(SuggestEditActivity.this);

                if (edt_suggest_edit.getText().toString().trim().isEmpty()) {

                    AppClass.snackBarView.snackBarShow(SuggestEditActivity.this, getString(R.string.please_enter_suggest));

                } else {
                    callAPISuggestEdit();
                }

                break;

        }

    }

    private void callAPISuggestEdit() {

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("post_id", postData.getPostId());
        request.put("post_title", edt_suggest_edit.getText().toString());

        ApiCall.getInstance().suggestEdit(SuggestEditActivity.this, request, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {

                String response = (String) data;

                //AppClass.snackBarView.snackBarShow(PostArticleActivity.this, response);
                DialogUtils.openDialog(SuggestEditActivity.this, response, getString(R.string.ok), "", new AlertDialogInterface() {
                    @Override
                    public void onNegativeBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }

                    @Override
                    public void onPositiveBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                        finish();
                    }
                });
            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {
                if (StringUtils.isNotEmpty(errorMessage)) {
                    DialogUtils.openDialog(SuggestEditActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                        @Override
                        public void onNegativeBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }

                        @Override
                        public void onPositiveBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }
                    });
                }

            }
        }, true);

    }
}
