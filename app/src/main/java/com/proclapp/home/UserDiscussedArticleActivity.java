package com.proclapp.home;

import android.content.DialogInterface;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.home.adapter.AllPostsAdapter;
import com.proclapp.interfaces.RecyclerClickListener;
import com.proclapp.model.AllPostsModel;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.AlertDialogInterface;
import com.proclapp.utils.DialogUtils;
import com.proclapp.utils.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;

public class UserDiscussedArticleActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView img_back;
    private RecyclerView rv_article_list;
    private TextView tv_no_data;

    private ArrayList<AllPostsModel> articalList = new ArrayList<>();

    private String type = "";
    private AllPostsAdapter articleAndDiscussionAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_discussed_article);

        type = getIntent().getStringExtra("type");

        initView();
        initListener();
    }

    private void initView() {

        img_back = findViewById(R.id.img_back);
        rv_article_list = findViewById(R.id.rv_article_list);
        tv_no_data = findViewById(R.id.tv_no_data);

        rv_article_list.setLayoutManager(new LinearLayoutManager(UserDiscussedArticleActivity.this));

        articleAndDiscussionAdapter = new AllPostsAdapter(UserDiscussedArticleActivity.this, articalList, new RecyclerClickListener() {
            @Override
            public void onItemClick(int pos, String tag) {

            }
        });
        rv_article_list.setAdapter(articleAndDiscussionAdapter);

        if (type.equalsIgnoreCase("discussed")) {
            callAPIGetDiscussedADList();
        } else {
            callAPIGetNonDiscussedADList();
        }
    }

    private void initListener() {
        img_back.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.img_back:

                onBackPressed();

                break;
        }
    }

    private void callAPIGetDiscussedADList() {


        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());

        ApiCall.getInstance().getDiscussedArticleList(UserDiscussedArticleActivity.this, request, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {

                tv_no_data.setVisibility(View.GONE);
                rv_article_list.setVisibility(View.VISIBLE);
                articalList.clear();
                articalList.addAll((ArrayList<AllPostsModel>) data);
                articleAndDiscussionAdapter.notifyDataSetChanged();

            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {
                if (StringUtils.isNotEmpty(errorMessage)) {

                    if (errorMessage.equalsIgnoreCase("No data found")) {

                        tv_no_data.setVisibility(View.VISIBLE);
                        rv_article_list.setVisibility(View.GONE);
                        tv_no_data.setText(getString(R.string.no_artical_found));

                    } else {

                        DialogUtils.openDialog(UserDiscussedArticleActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                            @Override
                            public void onNegativeBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }

                            @Override
                            public void onPositiveBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }
                        });
                    }
                }
            }
        }, true);


    }

    private void callAPIGetNonDiscussedADList() {


        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());

        ApiCall.getInstance().getNonDiscussedArticleList(UserDiscussedArticleActivity.this, request, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {

                tv_no_data.setVisibility(View.GONE);
                rv_article_list.setVisibility(View.VISIBLE);
                articalList.clear();
                articalList.addAll((ArrayList<AllPostsModel>) data);
                articleAndDiscussionAdapter.notifyDataSetChanged();

            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {
                if (StringUtils.isNotEmpty(errorMessage)) {

                    if (errorMessage.equalsIgnoreCase("No data found")) {

                        tv_no_data.setVisibility(View.VISIBLE);
                        rv_article_list.setVisibility(View.GONE);
                        tv_no_data.setText(getString(R.string.no_artical_found));

                    } else {

                        DialogUtils.openDialog(UserDiscussedArticleActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                            @Override
                            public void onNegativeBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }

                            @Override
                            public void onPositiveBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }
                        });
                    }
                }
            }
        }, true);


    }

}
