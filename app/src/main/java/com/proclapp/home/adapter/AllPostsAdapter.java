package com.proclapp.home.adapter;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.text.Html;
import android.text.util.Linkify;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.home.CarryOverDialog;
import com.proclapp.home.PostDiscussionActivity;
import com.proclapp.home.adapter.viewholder.AdvertViewHolder;
import com.proclapp.home.adapter.viewholder.ArticalViewHolder;
import com.proclapp.home.adapter.viewholder.QuestionAnswerViewHolder;
import com.proclapp.home.adapter.viewholder.ResearchPaperViewHolder;
import com.proclapp.home.adapter.viewholder.SuggetionTrendingViewHolder;
import com.proclapp.home.adapter.viewholder.VoiceVideoViewHolder;
import com.proclapp.home.voice_video.VideoPlayActivity;
import com.proclapp.home.voice_video.VoiceVideoDetails;
import com.proclapp.interfaces.RecyclerClickListener;
import com.proclapp.model.AllPostsModel;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.DateTimeUtils;
import com.proclapp.utils.ImageLoadUtils;
import com.proclapp.utils.Logger;
import com.proclapp.utils.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;

public class AllPostsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private AppCompatActivity context;
    private ArrayList<AllPostsModel> allPostsLists;
    private RecyclerClickListener listener;
    private String tag = "";
    private MediaPlayer mPlayer;


    public AllPostsAdapter(AppCompatActivity context, ArrayList<AllPostsModel> allPostsLists, RecyclerClickListener listener) {
        this.context = context;
        this.allPostsLists = allPostsLists;
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = null;

        if (viewType == 0) {  //  for research paper
            view = LayoutInflater.from(context).inflate(R.layout.row_research_paper_adapter, parent, false);
            return new ResearchPaperViewHolder(view, context, listener, allPostsLists, this);
        } else if (viewType == 1) {  // for artical
            view = LayoutInflater.from(context).inflate(R.layout.row_article, parent, false);
            return new ArticalViewHolder(view, context, allPostsLists, this);
        } else if (viewType == 3) {  // for advert
            view = LayoutInflater.from(context).inflate(R.layout.row_advert, parent, false);
            return new AdvertViewHolder(view, context, allPostsLists, this);
        } else if (viewType == 2) {  // for quetion
            view = LayoutInflater.from(context).inflate(R.layout.row_question_answer_adapter, parent, false);
            return new QuestionAnswerViewHolder(view, context, allPostsLists, this);
        } else if (viewType == 7) {  // for quetion
            view = LayoutInflater.from(context).inflate(R.layout.row_voice_and_video_adapter, parent, false);
            return new VoiceVideoViewHolder(view, context, this, allPostsLists);
        } else {
            view = LayoutInflater.from(context).inflate(R.layout.row_suggetion_trending, parent, false);
            return new SuggetionTrendingViewHolder(view, context, allPostsLists, this);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder viewHolder, int pos) {

        AllPostsModel allPostsData = allPostsLists.get(pos);

        LinearLayout llCarryOver = null;


        if (viewHolder instanceof ResearchPaperViewHolder) {
            ResearchPaperViewHolder holder = (ResearchPaperViewHolder) viewHolder;

            holder.txt_type_category.setText("Research Paper | " + allPostsData.getCategoryName());
            holder.txt_user_name.setText(allPostsData.getAuthor());
            holder.txt_description.setText(allPostsData.getPostTitle());
//            holder.txt_description.setText(Html.fromHtml(allPostsData.getPostDescription()));
            //holder.txt_date.setText(DateTimeUtils.changeDateTimeFormat("yyyy-MM-dd HH:mm:ss", "dd MMM", allPostsData.getPostDate()));
            holder.txt_date.setText(DateTimeUtils.calculateTimeBetweenTwoDates(context, allPostsData.getPostDate(), false));
            holder.txt_user_place.setText(allPostsData.getProfessionalQualification());
            holder.txt_doc_name.setText(allPostsData.getFiles().getOriFileName());
            holder.txt_doc_size.setText(allPostsData.getFiles().getFileSize());

            holder.txt_like_count.setText(allPostsData.getPostLikeCount());

            holder.txt_share_count.setText(allPostsData.getPostShareCount());
            holder.txt_comments_count.setText(allPostsData.getTotal_answers());

            if (allPostsData.getIsPostBookmark().equalsIgnoreCase("true")) {
                //holder.txt_bookmark.setText(context.getString(R.string.bookmarked));
                holder.txt_bookmark.setSelected(true);
            } else {
                //holder.txt_bookmark.setText(context.getString(R.string.bookmark));
                holder.txt_bookmark.setSelected(false);
            }

            if (allPostsData.getIsPostLiked().equalsIgnoreCase("true")) {
                holder.txt_like_count.setSelected(true);
            } else {
                holder.txt_like_count.setSelected(false);
            }

            Logger.d("follow===" + allPostsData.getIs_following());

            if (allPostsData.getIs_following() != null &&
                    allPostsData.getIs_following().equalsIgnoreCase("true")) {
                holder.img_follow.setSelected(true);
            } else {
                holder.img_follow.setSelected(false);
            }

            if (StringUtils.isNotEmpty(allPostsData.getAuthorId()) &&
                    allPostsData.getAuthorId().equalsIgnoreCase(AppClass.preferences.getUserId())) {
                holder.img_follow.setVisibility(View.GONE);
            } else {
                holder.img_follow.setVisibility(View.VISIBLE);
            }

            ImageLoadUtils.imageLoad(context,
                    holder.img_user_profile,
                    allPostsData.getProfileImage(),
                    R.drawable.menu_user_ph);

        } else if (viewHolder instanceof QuestionAnswerViewHolder) {
            QuestionAnswerViewHolder holder = (QuestionAnswerViewHolder) viewHolder;
            holder.txt_question_category.setText("Question | " + allPostsData.getCategoryName());
            llCarryOver = holder.ll_carryover;

            if (allPostsData.getIs_carryover_expire().equalsIgnoreCase("true")) {
                llCarryOver.setVisibility(View.GONE);
            } else {
                llCarryOver.setVisibility(View.VISIBLE);
                if (allPostsData.getIs_carryover().equalsIgnoreCase("true")) {
                    llCarryOver.setSelected(true);
                } else {
                    llCarryOver.setSelected(false);
                }

            }

            holder.txt_user_place.setText(allPostsData.getProfessionalQualification());

            holder.txt_question_value.setText(allPostsData.getPostTitle());

            holder.txt_user_name.setText(allPostsData.getAuthor());
            //holder.txt_date.setText(DateTimeUtils.changeDateTimeFormat("yyyy-MM-dd HH:mm:ss", "dd MMM", allPostsData.getPostDate()));

            holder.txt_date.setText(DateTimeUtils.calculateTimeBetweenTwoDates(context, allPostsData.getPostDate(), false));

            holder.txt_like_count.setText(allPostsData.getPostLikeCount());

            holder.txt_share_count.setText(allPostsData.getPostShareCount());
            holder.txt_comments_count.setText(allPostsData.getTotal_answers());
            holder.txt_flash_count.setText(allPostsData.getPostFlashedCount());


            if (allPostsData.getIsPostLiked().equalsIgnoreCase("true")) {
                holder.txt_like_count.setSelected(true);
            } else {
                holder.txt_like_count.setSelected(false);
            }
            if (allPostsData.getIs_following().equalsIgnoreCase("true")) {
                holder.img_follow.setSelected(true);
            } else {
                holder.img_follow.setSelected(false);
            }

            if (allPostsData.getAuthorId().equalsIgnoreCase(AppClass.preferences.getUserId())) {
                holder.img_follow.setVisibility(View.GONE);
            } else {
                holder.img_follow.setVisibility(View.VISIBLE);
            }


            ImageLoadUtils.imageLoad(context,
                    holder.img_user_profile,
                    allPostsData.getProfileImage(),
                    R.drawable.menu_user_ph);

        } else if (viewHolder instanceof ArticalViewHolder) {
            ArticalViewHolder holder = (ArticalViewHolder) viewHolder;

            llCarryOver = holder.ll_carryover;
            if (allPostsData.getIs_carryover_expire().equalsIgnoreCase("true")) {
                llCarryOver.setVisibility(View.GONE);
            } else {
                llCarryOver.setVisibility(View.VISIBLE);
                if (allPostsData.getIs_carryover().equalsIgnoreCase("true")) {
                    llCarryOver.setSelected(true);
                } else {
                    llCarryOver.setSelected(false);
                }

            }


            holder.txt_artical_category.setText(String.format("%s | %s", context.getString(R.string.article), allPostsData.getCategoryName()));

            holder.txt_user_name.setText(allPostsData.getAuthor());
            holder.txt_title.setText(allPostsData.getPostTitle());
            holder.txt_description.setText(Html.fromHtml(allPostsData.getPostDescription()));
            Linkify.addLinks(holder.txt_description, Linkify.ALL);
            holder.txt_date.setText(DateTimeUtils.calculateTimeBetweenTwoDates(context, allPostsData.getPostDate(), false));

            //holder.txt_date.setText(DateTimeUtils.changeDateTimeFormat("yyyy-MM-dd HH:mm:ss", "dd MMM", allPostsData.getPostDate()));
            holder.txt_user_place.setText(allPostsData.getProfessionalQualification());
            holder.txt_like_count.setText(allPostsData.getPostLikeCount());

            if (StringUtils.isNotEmpty(allPostsData.getTotal_answers()))
                holder.txt_comments_count.setText(allPostsData.getTotal_answers());
            else
                holder.txt_comments_count.setText("0");

//            holder.img_article.setVisibility(View.GONE);

            if (StringUtils.isNotEmpty(allPostsData.getIsPostLiked()) &&
                    allPostsData.getIsPostLiked().equalsIgnoreCase("true")) {
                holder.txt_like_count.setSelected(true);
            } else {
                holder.txt_like_count.setSelected(false);
            }

            if (StringUtils.isNotEmpty(allPostsData.getIs_following()) &&
                    allPostsData.getIs_following().equalsIgnoreCase("true")) {
                holder.img_follow.setSelected(true);
            } else {
                holder.img_follow.setSelected(false);
            }

            if (StringUtils.isNotEmpty(allPostsData.getAuthorId()) &&
                    allPostsData.getAuthorId().equalsIgnoreCase(AppClass.preferences.getUserId())) {
                holder.img_follow.setVisibility(View.GONE);
            } else {
                holder.img_follow.setVisibility(View.VISIBLE);
            }

//            try {
//
//                if (allPostsData.getImages().size() > 0) {
//                    if (allPostsData.getImages().get(0).getImage_name().isEmpty()) {
//                        holder.fl_image.setVisibility(View.GONE);
//
//                    } else {
//                        holder.fl_image.setVisibility(View.VISIBLE);
//                        ImageLoadUtils.imageLoad(context,
//                                holder.img_article,
//                                allPostsData.getImages().get(0).getImage_name());
//
//
//                        holder.img_article.setVisibility(View.VISIBLE);
//
//                        if (allPostsData.getImages().size() > 1) {
//                            holder.img_article_shadow.setVisibility(View.VISIBLE);
//                            holder.txt_image_count.setVisibility(View.VISIBLE);
//                            holder.txt_image_count.setText("+" + (allPostsData.getImages().size() - 1));
//                        } else {
//                            holder.img_article_shadow.setVisibility(View.GONE);
//                            holder.txt_image_count.setVisibility(View.GONE);
//                        }
//                    }
//                } else {
//                    holder.fl_image.setVisibility(View.GONE);
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }

            ImageLoadUtils.imageLoad(context,
                    holder.img_user_profile,
                    allPostsData.getProfileImage(),
                    R.drawable.menu_user_ph);

            if (allPostsData.getImages().size() == 1) {

                holder.iv_single_image.setVisibility(View.VISIBLE);
                holder.rv_all_images.setVisibility(View.GONE);

                ImageLoadUtils.imageLoad(context,
                        holder.iv_single_image,
                        allPostsData.getImages().get(0).getImage_name(),
                        R.drawable.upload_img_ph);

            } else {
                holder.iv_single_image.setVisibility(View.GONE);
                holder.rv_all_images.setVisibility(View.VISIBLE);
                ArticleImagesAdapter articleImagesAdapter = new ArticleImagesAdapter(context, allPostsData.getImages());
                holder.rv_all_images.setAdapter(articleImagesAdapter);
            }

        } else if (viewHolder instanceof AdvertViewHolder) {

            AdvertViewHolder holder = (AdvertViewHolder) viewHolder;
            AllPostsModel advertData = allPostsLists.get(pos);

            if (AppClass.preferences.getUserId().equalsIgnoreCase(allPostsLists.get(holder.getAdapterPosition()).getAuthorId())) {
                holder.img_option.setVisibility(View.VISIBLE);
            } else {
                holder.img_option.setVisibility(View.GONE);
            }

            holder.txt_artical_category.setText(context.getString(R.string.sponsored));

            holder.txt_user_name.setText(advertData.getAuthor());
            holder.txt_title.setText(advertData.getPostTitle());
            holder.txt_description.setText(advertData.getPostDescription());
            holder.txt_date.setText(DateTimeUtils.changeDateTimeFormat("yyyy-MM-dd HH:mm:ss", "dd MMM", advertData.getPostDate()));
            holder.txt_user_place.setText(advertData.getProfessionalQualification());
            holder.txt_like_count.setText(advertData.getPostLikeCount());

            if (StringUtils.isNotEmpty(advertData.getPostCommentCount()))
                holder.txt_comments_count.setText(advertData.getPostCommentCount());
            else
                holder.txt_comments_count.setText("0");

            if (StringUtils.isNotEmpty(advertData.getIsPostLiked()) &&
                    advertData.getIsPostLiked().equalsIgnoreCase("true")) {
                holder.txt_like_count.setSelected(true);
            } else {
                holder.txt_like_count.setSelected(false);
            }

            ImageLoadUtils.imageLoad(context,
                    holder.img_user_profile,
                    advertData.getProfileImage(),
                    R.drawable.menu_user_ph);

            if (advertData.getImages().size() == 1) {

                ImageLoadUtils.imageLoad(context,
                        holder.iv_advert_single_image,
                        advertData.getImages().get(0).getImage_name(),
                        R.drawable.post_img_ph);

                holder.ll_multiple_image.setVisibility(View.GONE);
                holder.iv_advert_single_image.setVisibility(View.VISIBLE);

            } else if (advertData.getImages().size() == 2) {

                ImageLoadUtils.imageLoad(context,
                        holder.iv_advert_one,
                        advertData.getImages().get(0).getImage_name(),
                        R.drawable.post_img_ph);

                ImageLoadUtils.imageLoad(context,
                        holder.iv_advert_two,
                        advertData.getImages().get(1).getImage_name(),
                        R.drawable.post_img_ph);

                holder.ll_multiple_image.setVisibility(View.VISIBLE);
                holder.iv_advert_single_image.setVisibility(View.GONE);
                holder.fl_image.setVisibility(View.GONE);
                holder.iv_advert_three.setVisibility(View.GONE);

            } else if (advertData.getImages().size() == 3) {

                ImageLoadUtils.imageLoad(context,
                        holder.iv_advert_one,
                        advertData.getImages().get(0).getImage_name(),
                        R.drawable.post_img_ph);

                ImageLoadUtils.imageLoad(context,
                        holder.iv_advert_two,
                        advertData.getImages().get(1).getImage_name(),
                        R.drawable.post_img_ph);

                ImageLoadUtils.imageLoad(context,
                        holder.iv_advert_three,
                        advertData.getImages().get(2).getImage_name(),
                        R.drawable.post_img_ph);

                holder.ll_multiple_image.setVisibility(View.VISIBLE);
                holder.iv_advert_single_image.setVisibility(View.GONE);
                holder.img_article_shadow.setVisibility(View.GONE);
                holder.txt_image_count.setVisibility(View.GONE);

            } else if (advertData.getImages().size() > 3) {

                ImageLoadUtils.imageLoad(context,
                        holder.iv_advert_one,
                        advertData.getImages().get(0).getImage_name(),
                        R.drawable.post_img_ph);

                ImageLoadUtils.imageLoad(context,
                        holder.iv_advert_two,
                        advertData.getImages().get(1).getImage_name(),
                        R.drawable.post_img_ph);

                ImageLoadUtils.imageLoad(context,
                        holder.iv_advert_three,
                        advertData.getImages().get(2).getImage_name(),
                        R.drawable.post_img_ph);

                holder.ll_multiple_image.setVisibility(View.VISIBLE);
                holder.iv_advert_single_image.setVisibility(View.GONE);
                holder.img_article_shadow.setVisibility(View.VISIBLE);
                holder.txt_image_count.setVisibility(View.VISIBLE);
                holder.txt_image_count.setText("+" + (advertData.getImages().size() - 2));


            }
        }
        if (viewHolder instanceof VoiceVideoViewHolder) {
            final VoiceVideoViewHolder holder = (VoiceVideoViewHolder) viewHolder;
            AllPostsModel audioVideoData = allPostsLists.get(pos);

            holder.txt_like.setCompoundDrawables(getDrawableAndResize(R.drawable.selector_like_post), null, null, null);
            holder.txt_comment.setCompoundDrawables(getDrawableAndResize(R.drawable.discuss), null, null, null);
            holder.txt_share.setCompoundDrawables(getDrawableAndResize(R.drawable.share_blue), null, null, null);
            holder.txt_comments_count.setCompoundDrawables(getDrawableAndResize(R.drawable.comments), null, null, null);
            holder.txt_user_name.setTypeface(holder.txt_user_name.getTypeface(), Typeface.BOLD);


            if (audioVideoData.getVvType().equalsIgnoreCase("1")) {
                holder.rl_video.setVisibility(View.GONE);
                holder.ll_voice.setVisibility(View.VISIBLE);


            } else {
                holder.rl_video.setVisibility(View.VISIBLE);
                holder.ll_voice.setVisibility(View.GONE);

                ImageLoadUtils.imageLoad(context,
                        holder.img_video,
                        audioVideoData.getVideosPost().getVideoUrl());
            }

            if (StringUtils.isNotEmpty(audioVideoData.getIsPostLiked()) &&
                    audioVideoData.getIsPostLiked().equalsIgnoreCase("true")) {
                holder.txt_like.setSelected(true);
            } else {
                holder.txt_like.setSelected(false);
            }
            holder.txt_like.setText(audioVideoData.getPostLikeCount());
            if (StringUtils.isNotEmpty(audioVideoData.getTotal_answers()))
                holder.txt_comments_count.setText(audioVideoData.getTotal_answers());
            else
                holder.txt_comments_count.setText("0");

            ImageLoadUtils.imageLoad(context,
                    holder.img_user_profile,
                    audioVideoData.getProfileImage(),
                    R.drawable.menu_user_ph);

            if (StringUtils.isNotEmpty(audioVideoData.getPostDescription()))
                holder.txt_description.setText(audioVideoData.getPostDescription());

            if (StringUtils.isNotEmpty(audioVideoData.getAuthor()))
                holder.txt_user_name.setText(audioVideoData.getAuthor());

            if (StringUtils.isNotEmpty(audioVideoData.getProfessionalQualification()))
                holder.txt_user_place.setText(audioVideoData.getProfessionalQualification());

            if (StringUtils.isNotEmpty(audioVideoData.getCategoryName()))
                holder.txt_title_value.setText(audioVideoData.getCategoryName());

            if (StringUtils.isNotEmpty(audioVideoData.getPostDate()))
                holder.txt_date.setText(DateTimeUtils.calculateTimeBetweenTwoDates(context, audioVideoData.getPostDate(), false));

            if (audioVideoData.getRequest_to_reply().equalsIgnoreCase("2") ||
                    audioVideoData.getRequest_to_reply().equalsIgnoreCase("3")) {
                holder.tv_see_answer.setVisibility(View.VISIBLE);
                holder.tv_give_answer.setVisibility(View.VISIBLE);
            } else if (audioVideoData.getRequest_to_reply().equalsIgnoreCase("1") ||
                    audioVideoData.getRequest_to_reply().equalsIgnoreCase("4")) {
                holder.tv_see_answer.setVisibility(View.VISIBLE);
                holder.tv_give_answer.setVisibility(View.GONE);
            } else {
                holder.tv_see_answer.setVisibility(View.GONE);
                holder.tv_give_answer.setVisibility(View.GONE);
            }

            if (audioVideoData.getVvType().equalsIgnoreCase("1")) {
                holder.rl_video.setVisibility(View.GONE);
                holder.ll_voice.setVisibility(View.VISIBLE);


            } else {
                holder.rl_video.setVisibility(View.VISIBLE);
                holder.ll_voice.setVisibility(View.GONE);

                ImageLoadUtils.imageLoad(context,
                        holder.img_video,
                        audioVideoData.getVideosPost().getVideoUrl());
            }

            ImageLoadUtils.imageLoad(context,
                    holder.img_user_profile,
                    audioVideoData.getProfileImage(),
                    R.drawable.menu_user_ph);

            if (StringUtils.isNotEmpty(audioVideoData.getAuthor()))
                holder.txt_user_name.setText(audioVideoData.getAuthor());

            if (StringUtils.isNotEmpty(audioVideoData.getProfessionalQualification()))
                holder.txt_user_place.setText(audioVideoData.getProfessionalQualification());

            if (StringUtils.isNotEmpty(audioVideoData.getCategoryName()))
                holder.txt_title_value.setText(audioVideoData.getCategoryName());

            if (audioVideoData.getRequest_to_reply().equalsIgnoreCase("2") ||
                    audioVideoData.getRequest_to_reply().equalsIgnoreCase("3")) {
                holder.tv_see_answer.setVisibility(View.VISIBLE);
                holder.tv_give_answer.setVisibility(View.VISIBLE);
            } else if (audioVideoData.getRequest_to_reply().equalsIgnoreCase("1") ||
                    audioVideoData.getRequest_to_reply().equalsIgnoreCase("4")) {
                holder.tv_see_answer.setVisibility(View.VISIBLE);
                holder.tv_give_answer.setVisibility(View.GONE);
            } else {
                holder.tv_see_answer.setVisibility(View.GONE);
                holder.tv_give_answer.setVisibility(View.GONE);
            }

            holder.ll_discuss.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (holder.getAdapterPosition() >= 0)
                        context.startActivity(new Intent(context, PostDiscussionActivity.class)
                                .putExtra("postId", audioVideoData.getPostId()).putExtra("isPostVV", "true"));
                }
            });

            holder.txt_share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    String path = allPostsLists.get(pos).getVvType().equals("1") ? allPostsLists.get(pos).getAudioPost().getAudioUrl() :
                            allPostsLists.get(pos).getVideosPost().getVideoUrl();

                    Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                    sharingIntent.setType("*/*");
                    sharingIntent.putExtra(Intent.EXTRA_TEXT, path);
                    context.startActivity(Intent.createChooser(sharingIntent, context.getString(R.string.share_using)));

                }
            });

            holder.txt_like.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (holder.getAdapterPosition() >= 0)
                        callAPILikeUnlike(holder.txt_like.isSelected() ? "0" : "1", holder);
                }
            });


            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    context.startActivity(new Intent(context, VoiceVideoDetails.class)
                            .putExtra("audioVideoDetails", audioVideoData));
//                    if (!allPostsLists.get(pos).getVvType().equalsIgnoreCase("1")) {
//
//                        context.startActivity(new Intent(context, VideoPlayActivity.class)
//                                .putExtra("audiovideourl", allPostsLists.get(holder.getAdapterPosition()).getVideosPost().getVideoUrl()));
//
//                    }


                }
            });

            AudioManager am = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
            am.setMode(AudioManager.MODE_NORMAL);
            mPlayer = new MediaPlayer();


            holder.iv_play.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (mPlayer != null)
                        mPlayer.release();

                    onStreamAudio(allPostsLists.get(pos).getAudioPost().getAudioUrl(), holder);
                    holder.iv_pause.setVisibility(View.VISIBLE);
                    holder.iv_play.setVisibility(View.GONE);


                }
            });

            holder.iv_pause.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    try {
                        holder.iv_pause.setVisibility(View.GONE);
                        holder.iv_play.setVisibility(View.VISIBLE);
                        mPlayer.stop();
                        mPlayer.release();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }
            });
        } else if (viewHolder instanceof SuggetionTrendingViewHolder) {

            if (allPostsLists != null) {

                SuggetionTrendingViewHolder holder = (SuggetionTrendingViewHolder) viewHolder;

                if (tag.equalsIgnoreCase(context.getString(R.string.suggested_friend)) &&
                        (allPostsLists.get(pos).getSuggestedFriends() == null ||
                                allPostsLists.get(pos).getSuggestedFriends().isEmpty())) {
                    holder.tv_tag.setVisibility(View.GONE);
                } else if (tag.equalsIgnoreCase(context.getString(R.string.suggested_mentor)) &&
                        (allPostsLists.get(pos).getSuggestedExperts() == null ||
                                allPostsLists.get(pos).getSuggestedExperts().isEmpty())) {
                    holder.tv_tag.setVisibility(View.GONE);
                } else if (tag.equalsIgnoreCase(context.getString(R.string.trending_topics)) &&
                        (allPostsLists.get(pos).getTrendingTopics() == null ||
                                allPostsLists.get(pos).getTrendingTopics().isEmpty())) {
                    holder.tv_tag.setVisibility(View.GONE);
                } else {
                    holder.tv_tag.setVisibility(View.VISIBLE);
                }
                holder.tv_tag.setText(tag);
                holder.rv_suggetions_trending.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                holder.rv_suggetions_trending.setAdapter(new SuggetionTrendigViewAdapter(context, allPostsLists, holder.getAdapterPosition()));

            }
        }

        if (llCarryOver != null) {
            llCarryOver.setOnClickListener(view -> {

                if (view.isSelected()) {
                    AppClass.snackBarView.snackBarShow(context, "Already carryover");
                } else {

                    new CarryOverDialog(context,
                            allPostsLists.get(viewHolder.getAdapterPosition()).getPostId(),
                            viewHolder.getAdapterPosition(), (carryOverPos) -> {
                        allPostsLists.get(carryOverPos).setIsCarryover("true");
                        notifyDataSetChanged();

                    }).show();
                }
            });
        }
    }

    private void onStreamAudio(String url, final VoiceVideoViewHolder holder) {
        mPlayer = new MediaPlayer();
        mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        // Listen for if the audio file can't be prepared
        mPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                // ... react appropriately ...
                // The MediaPlayer has moved to the Error state, must be reset!
                return false;
            }
        });
        mPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mPlayer.start();

            }
        });

        try {
            mPlayer.setDataSource(url);
            mPlayer.prepareAsync();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public int getItemCount() {
        return allPostsLists.size();
    }

    @Override
    public int getItemViewType(int position) {

        if (StringUtils.isNotEmpty(allPostsLists.get(position).getsFriend()) &&
                allPostsLists.get(position).getsFriend().equalsIgnoreCase("1")) {
            tag = context.getString(R.string.suggested_friend);
            return 4;
        } else if (StringUtils.isNotEmpty(allPostsLists.get(position).getsExpert()) &&
                allPostsLists.get(position).getsExpert().equalsIgnoreCase("1")) {
            tag = context.getString(R.string.suggested_mentor);
            return 5;
        } else if (StringUtils.isNotEmpty(allPostsLists.get(position).getTrTopics()) &&
                allPostsLists.get(position).getTrTopics().equalsIgnoreCase("1")) {
            tag = context.getString(R.string.trending_topics);
            return 6;
        } else if (allPostsLists.get(position).getPostType().equalsIgnoreCase("4")) {
            return 0; // Researchpaper
        } else if (allPostsLists.get(position).getPostType().equalsIgnoreCase("3")) {
            return 7; //voice and video
        } else if (allPostsLists.get(position).getPostType().equalsIgnoreCase("2")) {
            return 2; // questions
        } else if (allPostsLists.get(position).getPostType().equalsIgnoreCase("5")) {
            return 3; // advert
        } else {
            return 1; // article
        }

    }

    private void callAPILikeUnlike(final String status, VoiceVideoViewHolder holder) {
        Log.d("Call like unlike", "clicked like button");

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("post_id", allPostsLists.get(holder.getAdapterPosition()).getPostId());
        request.put("like_unlike", status);

        ApiCall.getInstance().likeUnlike(context, request, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {
                String response = (String) data;


            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {
                /*DialogUtils.openDialog(activity, errorMessage, activity.getString(R.string.ok), "", new AlertDialogInterface() {
                    @Override
                    public void onNegativeBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }

                    @Override
                    public void onPositiveBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }
                });*/
            }
        }, false);


        if (status.equalsIgnoreCase("0")) {
            allPostsLists.get(holder.getAdapterPosition()).setIsPostLiked("false");
            int count = Integer.parseInt(allPostsLists.get(holder.getAdapterPosition()).getPostLikeCount()) - 1;
            allPostsLists.get(holder.getAdapterPosition()).setPostLikeCount("" + count);
        } else {
            allPostsLists.get(holder.getAdapterPosition()).setIsPostLiked("true");
            int count = Integer.parseInt(allPostsLists.get(holder.getAdapterPosition()).getPostLikeCount()) + 1;
            allPostsLists.get(holder.getAdapterPosition()).setPostLikeCount("" + count);
        }
        this.notifyDataSetChanged();
    }

    private Drawable getDrawableAndResize(int drawableId) {
        Drawable drawable = ContextCompat.getDrawable(context, drawableId);
        drawable.setBounds(0,
                0,
                (int) (drawable.getIntrinsicWidth() * 1.4),
                (int) (drawable.getIntrinsicHeight() * 1.4));

        return drawable;
    }
}
