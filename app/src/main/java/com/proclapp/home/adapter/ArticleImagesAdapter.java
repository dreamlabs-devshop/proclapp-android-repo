package com.proclapp.home.adapter;

import android.app.Activity;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.proclapp.R;
import com.proclapp.home.ImagePagerActivity;
import com.proclapp.model.AllPostsModel;
import com.proclapp.utils.ImageLoadUtils;

import java.util.ArrayList;

public class ArticleImagesAdapter extends RecyclerView.Adapter<ArticleImagesAdapter.NotificationHolder> {

    Activity activity;
    ArrayList<AllPostsModel.Images> images;
    DisplayMetrics metrics;

    public ArticleImagesAdapter(Activity activity, ArrayList<AllPostsModel.Images> images) {
        this.activity = activity;
        this.images = images;

        metrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
    }

    @NonNull
    @Override
    public NotificationHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(activity).inflate(R.layout.row_article_images, viewGroup, false);
        return new NotificationHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final NotificationHolder holder, int i) {

        ImageLoadUtils.imageLoad(activity,
                holder.img_article,
                images.get(holder.getAdapterPosition()).getImage_name(),
                R.drawable.upload_img_ph);


        holder.img_article.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.startActivity(new Intent(activity, ImagePagerActivity.class)
                        .putExtra("imagesUrl", images)
                        .putExtra("clickedpos", holder.getAdapterPosition()));

            }
        });

    }

    @Override
    public int getItemCount() {
        return images.size();
    }

    public class NotificationHolder extends RecyclerView.ViewHolder {

        private ImageView img_article;

        public NotificationHolder(@NonNull View itemView) {
            super(itemView);

            img_article = itemView.findViewById(R.id.img_article);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams((metrics.widthPixels * 150) / 480, (metrics.heightPixels * 150) / 800);
            img_article.setLayoutParams(params);

        }
    }
}
