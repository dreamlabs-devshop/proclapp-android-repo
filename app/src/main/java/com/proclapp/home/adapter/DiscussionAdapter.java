package com.proclapp.home.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chinalwb.are.render.AreTextView;
import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.home.AnswerEditDeleteDialog;
import com.proclapp.home.SeeDiscussionActivity;
import com.proclapp.home.question.AnswerListAdapter;
import com.proclapp.interfaces.RecyclerClickListener;
import com.proclapp.model.AnswerModel;
import com.proclapp.profile.OtherUserProfileActivity;
import com.proclapp.profile.ProfileActivity;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.DateTimeUtils;
import com.proclapp.utils.ImageLoadUtils;
import com.proclapp.utils.KeyBoardUtils;
import com.proclapp.utils.Logger;
import com.proclapp.utils.SquareImageView;

import java.util.ArrayList;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class DiscussionAdapter extends RecyclerView.Adapter<DiscussionAdapter.DiscussionViewHolder> {

    private Activity context;
    private ArrayList<AnswerModel> discussionList;

    private RecyclerClickListener clickListener;

    public DiscussionAdapter(Activity context, ArrayList<AnswerModel> discussionList, RecyclerClickListener clickListener) {
        this.context = context;
        this.discussionList = discussionList;
        this.clickListener = clickListener;
    }

    @NonNull
    @Override
    public DiscussionViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_discussion_new, viewGroup, false);
        return new DiscussionViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final DiscussionViewHolder holder, int i) {
        try {

            if (AppClass.preferences.getUserId().equalsIgnoreCase(discussionList.get(holder.getAdapterPosition()).getUser().getUserId())) {
                holder.optionImage.setVisibility(View.VISIBLE);
            } else {
                holder.optionImage.setVisibility(View.INVISIBLE);
            }

            if (discussionList.get(holder.getAdapterPosition()).getAnswerImg().size() == 1) {
                holder.singleImageView.setVisibility(View.VISIBLE);
                ImageLoadUtils.imageLoad(context,
                        holder.singleImageView,
                        discussionList.get(holder.getAdapterPosition()).getAnswerImg().get(0).getImage_name(),
                        R.drawable.upload_img_ph);
            } else {
                holder.singleImageView.setVisibility(View.GONE);
            }
            if (discussionList.get(holder.getAdapterPosition()).getAnswer_upvote_comment_count() > 0) {
                holder.upVoteCountTextView.setText(String.valueOf(discussionList.get(holder.getAdapterPosition()).getAnswer_upvote_comment_count()));
                holder.upVoteTextView.setText(context.getResources().getQuantityString(R.plurals.upvote, discussionList.get(holder.getAdapterPosition()).getAnswer_upvote_comment_count(), discussionList.get(holder.getAdapterPosition()).getAnswer_upvote_comment_count()));

            }
            if (discussionList.get(holder.getAdapterPosition()).getAnswer_downvote_comment_count() > 0) {
                holder.downVoteCountTextView.setText(String.valueOf(discussionList.get(holder.getAdapterPosition()).getAnswer_downvote_comment_count()));
                holder.downVoteTextView.setText(context.getResources().getQuantityString(R.plurals.downvote, discussionList.get(holder.getAdapterPosition()).getAnswer_downvote_comment_count(), discussionList.get(holder.getAdapterPosition()).getAnswer_downvote_comment_count()));

            }
            holder.answerTextView.fromHtml(discussionList.get(holder.getAdapterPosition()).answer);
            holder.nameTextView.setText(discussionList.get(holder.getAdapterPosition()).getUser().name);
            holder.organisationTextView.setText(discussionList.get(holder.getAdapterPosition()).getUser().professionalQualification);
            holder.commentCountTextView.setText(discussionList.get(holder.getAdapterPosition()).getAnswer_comment_count());
            holder.dateTextView.setText(DateTimeUtils.changeDateTimeFormat("yyyy-MM-dd HH:mm:ss",
                    "dd MMM, HH:mm a",
                    discussionList.get(holder.getAdapterPosition()).date));
            ImageLoadUtils.imageLoad(context,
                    holder.userImageView,
                    discussionList.get(holder.getAdapterPosition()).getUser().profileImage,
                    R.drawable.menu_user_ph);

            ImageLoadUtils.imageLoad(context,
                    holder.voterImagePic,
                    AppClass.preferences.getProfileImage(),
                    R.drawable.menu_user_ph);


            if (discussionList.get(holder.getAdapterPosition()).getIs_post_answer_upvote().equalsIgnoreCase("true")) {
                holder.yourVoteTextView.setText(context.getString(R.string.you_upvoted_this));
                holder.downVoteLayout.setSelected(false);
                holder.upVoteLayout.setSelected(true);
            }

            if (discussionList.get(holder.getAdapterPosition()).getIs_post_answer_downvote().equalsIgnoreCase("true")) {
                holder.yourVoteTextView.setText(context.getString(R.string.You_downvoted_this));
                holder.downVoteLayout.setSelected(true);
                holder.upVoteLayout.setSelected(false);
            }

            if (discussionList.get(holder.getAdapterPosition()).getIs_post_answer_upvote().equalsIgnoreCase("false") &&
                    discussionList.get(holder.getAdapterPosition()).getIs_post_answer_downvote().equalsIgnoreCase("false")) {

                holder.voterImagePic.setVisibility(View.INVISIBLE);
                holder.yourVoteTextView.setVisibility(View.INVISIBLE);
            }


            holder.upVoteLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    holder.voterImagePic.setVisibility(View.VISIBLE);
                    holder.yourVoteTextView.setVisibility(View.VISIBLE);

                    callAPIPostUpVoteDownVote("1", holder);
                }
            });

            holder.downVoteLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    holder.voterImagePic.setVisibility(View.VISIBLE);
                    holder.voterImagePic.setVisibility(View.VISIBLE);

                    callAPIPostUpVoteDownVote("2", holder);
                }
            });

            holder.shareLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String shareBody = discussionList.get(holder.getAdapterPosition()).getAnswer() + context.getString(R.string.play_store_app_url);
                    Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                    sharingIntent.setType("text/plain");
                    sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
                    context.startActivity(Intent.createChooser(sharingIntent, context.getString(R.string.share_using)));
                }
            });

            String answerData = holder.answerTextView.getText().toString();
            int lineCount = answerData.length();
            String message = String.valueOf(lineCount);
            Log.d("line count: ", message);

            holder.seeMoreTextView.setVisibility(lineCount > 300 ? View.VISIBLE : View.GONE);
            holder.seeMoreTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (holder.answerTextView.getMaxLines() == 6) {
                        holder.seeMoreTextView.setText(context.getString(R.string.read_less));
                        holder.answerTextView.setMaxLines(Integer.MAX_VALUE);
                    } else {
                        holder.seeMoreTextView.setText(R.string.read_more);
                        holder.answerTextView.setMaxLines(6);
                    }
                }
            });

            holder.commentCountTextView.setCompoundDrawables(getDrawableAndResize(R.drawable.comments), null, null, null);
            holder.upVoteTextView.setCompoundDrawables(getDrawableAndResize(R.drawable.selector_upvote), null, null, null);
            holder.downVoteTextView.setCompoundDrawables(getDrawableAndResize(R.drawable.selector_downvote), null, null, null);
            holder.shareTextView.setCompoundDrawables(getDrawableAndResize(R.drawable.share_blue), null, null, null);


            //holder.swipe_to_delete.setRightSwipeEnabled(false);

//            if (openFor.equalsIgnoreCase("qa") ||
//                    openFor.equalsIgnoreCase("Question")) {
//                holder.txt_see_answer.setText(activity.getString(R.string.see_answer));
//            } else {
//                holder.txt_see_answer.setText(activity.getString(R.string.see_comment));
//            }

            holder.optionImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    KeyBoardUtils.closeSoftKeyboard(context);
                    if (AppClass.preferences.getUserId().equalsIgnoreCase(discussionList.get(holder.getAdapterPosition()).getUser().getUserId())) {

                        new AnswerEditDeleteDialog(context, new AnswerEditDeleteDialog.OnItemClick() {
                            @Override
                            public void onClick(String tag) {

                                if (tag.equalsIgnoreCase("delete")) {
                                    deleteAnswer(holder);
                                } else {
                                    clickListener.onItemClick(holder.getAdapterPosition(), "edit");
                                }

                            }
                        }).show();

                    }
                }
            });

//            holder.txt_person_name.setText(answerLists.get(holder.getAdapterPosition()).getUser().name);
//            holder.txt_person_organisation.setText(answerLists.get(holder.getAdapterPosition()).getUser().professionalQualification);
//
//            ImageLoadUtils.imageLoad(activity,
//                    holder.img_user_pic,
//                    answerLists.get(holder.getAdapterPosition()).getUser().profileImage,
//                    R.drawable.menu_user_ph);
//
//            holder.txt_see_answer.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    clickListener.onItemClick(holder.getAdapterPosition(), "see_answer");
//                }
//            });
//
//
        } catch (Exception e) {
            e.printStackTrace();
        }
//        holder.txt_person_name.setText(discussionList.get(holder.getAdapterPosition()).getUser().name);
//        holder.txt_person_organisation.setText(discussionList.get(holder.getAdapterPosition()).getUser().professionalQualification);
//        ImageLoadUtils.imageLoad(context,
//                holder.img_user_pic,
//                discussionList.get(holder.getAdapterPosition()).getUser().profileImage,
//                R.drawable.menu_user_ph);
//
//        if (AppClass.preferences.getUserId().equalsIgnoreCase(discussionList.get(holder.getAdapterPosition()).getUser().getUserId()))
//            holder.img_option.setVisibility(View.VISIBLE);
//        else
//            holder.img_option.setVisibility(View.INVISIBLE);
    }

    private Drawable getDrawableAndResize(int drawableId) {
        Drawable drawable = ContextCompat.getDrawable(context, drawableId);
        drawable.setBounds(0,
                0,
                (int) (drawable.getIntrinsicWidth() * 1.4),
                (int) (drawable.getIntrinsicHeight() * 1.4));

        return drawable;
    }

    @Override
    public int getItemCount() {
        return discussionList.size();
    }

    private void deleteAnswer(final DiscussionViewHolder holder) {

        ApiCall.getInstance().deleteAnswer(context, AppClass.preferences.getUserId(), discussionList.get(holder.getAdapterPosition()).postAnswersId, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {

                discussionList.remove(holder.getAdapterPosition());
                notifyDataSetChanged();

            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {

            }
        }, true);

    }

    private void callAPIPostUpVoteDownVote(String status, DiscussionAdapter.DiscussionViewHolder holder) {

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("post_id", discussionList.get(holder.getAdapterPosition()).postId);
        request.put("answer_id", discussionList.get(holder.getAdapterPosition()).postAnswersId);
        request.put("status", status);

        Logger.d("request:" + request);

        ApiCall.getInstance().postUpVoteDownVote(context,
                request, new IApiCallback() {
                    @Override
                    public void onSuccess(Object data) {
                        if (status.equalsIgnoreCase("1")) {
                            holder.downVoteLayout.setSelected(false);
                            holder.upVoteLayout.setSelected(true);
                            holder.yourVoteTextView.setText(context.getString(R.string.you_upvoted_this));
                            holder.upVoteCountTextView.setText(String.valueOf((discussionList.get(holder.getAdapterPosition()).getAnswer_upvote_comment_count() + 1)));
                            discussionList.get(holder.getAdapterPosition()).setAnswer_upvote_comment_count(discussionList.get(holder.getAdapterPosition()).getAnswer_upvote_comment_count() + 1);
                            holder.upVoteTextView.setText(context.getResources().getQuantityString(R.plurals.upvote, discussionList.get(holder.getAdapterPosition()).getAnswer_upvote_comment_count(), discussionList.get(holder.getAdapterPosition()).getAnswer_upvote_comment_count()));

                            if (discussionList.get(holder.getAdapterPosition()).getAnswer_downvote_comment_count() > 0) {

                                String downVoteCount = String.valueOf((discussionList.get(holder.getAdapterPosition()).getAnswer_downvote_comment_count() - 1));

                                if (downVoteCount.equalsIgnoreCase("0")) {
                                    holder.downVoteCountTextView.setText("");
                                } else {
                                    holder.downVoteCountTextView.setText(downVoteCount);
                                }
                                discussionList.get(holder.getAdapterPosition()).setAnswer_downvote_comment_count(discussionList.get(holder.getAdapterPosition()).getAnswer_downvote_comment_count() - 1);
                                holder.downVoteTextView.setText(context.getResources().getQuantityString(R.plurals.downvote, discussionList.get(holder.getAdapterPosition()).getAnswer_downvote_comment_count(), discussionList.get(holder.getAdapterPosition()).getAnswer_downvote_comment_count()));

                            }

                        } else {
                            holder.downVoteLayout.setSelected(true);
                            holder.upVoteLayout.setSelected(false);
                            holder.yourVoteTextView.setText(context.getString(R.string.You_downvoted_this));
                            if (discussionList.get(holder.getAdapterPosition()).getAnswer_upvote_comment_count() > 0) {

                                String upVoteCount = String.valueOf((discussionList.get(holder.getAdapterPosition()).getAnswer_upvote_comment_count() - 1));
                                if (upVoteCount.equalsIgnoreCase("0")) {
                                    holder.upVoteCountTextView.setText("");
                                } else {
                                    holder.upVoteCountTextView.setText(upVoteCount);
                                }

                                discussionList.get(holder.getAdapterPosition()).setAnswer_upvote_comment_count(discussionList.get(holder.getAdapterPosition()).getAnswer_upvote_comment_count() - 1);
                                holder.upVoteTextView.setText(context.getResources().getQuantityString(R.plurals.upvote, discussionList.get(holder.getAdapterPosition()).getAnswer_upvote_comment_count(), discussionList.get(holder.getAdapterPosition()).getAnswer_upvote_comment_count()));

                            }

                            holder.downVoteCountTextView.setText(String.valueOf((discussionList.get(holder.getAdapterPosition()).getAnswer_downvote_comment_count() + 1)));
                            discussionList.get(holder.getAdapterPosition()).setAnswer_downvote_comment_count((discussionList.get(holder.getAdapterPosition()).getAnswer_downvote_comment_count() + 1));
                            holder.downVoteTextView.setText(context.getResources().getQuantityString(R.plurals.downvote, discussionList.get(holder.getAdapterPosition()).getAnswer_downvote_comment_count(), discussionList.get(holder.getAdapterPosition()).getAnswer_downvote_comment_count()));

                        }
                    }

                    @Override
                    public void onFailure(boolean isFalseFromService, String errorMessage) {

                    }


                }, true);

    }

    public class DiscussionViewHolder extends RecyclerView.ViewHolder {

        TextView
                nameTextView,
                organisationTextView,
                dateTextView,
                commentCountTextView,
                seeMoreTextView,
                upVoteTextView,
                upVoteCountTextView,
                downVoteTextView,
                downVoteCountTextView,
                yourVoteTextView,
                shareTextView;
        AreTextView answerTextView;
        CircleImageView userImageView, voterImagePic;
        ImageView optionImage;
        SquareImageView singleImageView;
        LinearLayout upVoteLayout, downVoteLayout, shareLayout;

        public DiscussionViewHolder(@NonNull View itemView) {
            super(itemView);
            nameTextView = itemView.findViewById(R.id.txt_person_name_1);
            organisationTextView = itemView.findViewById(R.id.txt_person_organisation_1);
            dateTextView = itemView.findViewById(R.id.txt_time_1);
            answerTextView = itemView.findViewById(R.id.txt_answer_data_2);
            userImageView = itemView.findViewById(R.id.img_user_pic_1);
            optionImage = itemView.findViewById(R.id.img_option_1);
            commentCountTextView = itemView.findViewById(R.id.txt_comment_count_1);
            singleImageView = itemView.findViewById(R.id.iv_single_image_1);
            seeMoreTextView = itemView.findViewById(R.id.tv_see_more_1);
            shareTextView = itemView.findViewById(R.id.txt_share);
            upVoteTextView = itemView.findViewById(R.id.txt_upvote_1);
            upVoteCountTextView = itemView.findViewById(R.id.txt_upvote_count_1);
            downVoteTextView = itemView.findViewById(R.id.txt_downvote_1);
            downVoteCountTextView = itemView.findViewById(R.id.txt_downvote_count_1);
            yourVoteTextView = itemView.findViewById(R.id.txt_your_vote_1);
            voterImagePic = itemView.findViewById(R.id.img_voter_pic_1);
            upVoteLayout = itemView.findViewById(R.id.ll_upvote_1);
            downVoteLayout = itemView.findViewById(R.id.ll_downvote_1);
            shareLayout = itemView.findViewById(R.id.ll_share_1);

            itemView.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            context.startActivity(new Intent(context, SeeDiscussionActivity.class)
                                    .putExtra("postId", discussionList.get(getAdapterPosition()).getPostId())
                                    .putExtra("answerLists", discussionList.get(getAdapterPosition()).getPostAnswersId()));
                        }
                    });
            answerTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    context.startActivity(new Intent(context, SeeDiscussionActivity.class)
                            .putExtra("postId", discussionList.get(getAdapterPosition()).getPostId())
                            .putExtra("answerLists", discussionList.get(getAdapterPosition()).getPostAnswersId()));
                }
            });

        }
    }

//    public class DiscussionHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
//
//        ImageView
//                img_user_pic,
//                img_option;
//
//        TextView
//                txt_person_name,
//                txt_person_organisation,
//                txt_see_answer;
//
//
//        public DiscussionHolder(@NonNull View itemView) {
//            super(itemView);
//            initViews();
//            setTypeFace();
//            setClickListener();
//        }
//
//        @Override
//        public void onClick(View view) {
//
//            switch (view.getId()) {
//                case R.id.img_user_pic:
//                case R.id.txt_person_name:
//                case R.id.txt_person_organisation:
//
//                    if (AppClass.preferences.getUserId().equalsIgnoreCase(discussionList.get(getAdapterPosition()).getUser().getUserId())) {
//                        context.startActivity(new Intent(context, ProfileActivity.class));
//
//                    } else {
//
//                        context.startActivity(new Intent(context, OtherUserProfileActivity.class)
//                                .putExtra("user_id", discussionList.get(getAdapterPosition()).getUser().getUserId()));
//                    }
//                    break;
//
//                case R.id.txt_see_answer:
//
//                    context.startActivity(new Intent(context, SeeDiscussionActivity.class)
//                            .putExtra("postId", discussionList.get(getAdapterPosition()).getPostId())
//                            .putExtra("answerLists", discussionList.get(getAdapterPosition()).getPostAnswersId()));
//
//                    break;
//
//                case R.id.img_option:
//                    KeyBoardUtils.closeSoftKeyboard(context);
//                    if (AppClass.preferences.getUserId().equalsIgnoreCase(discussionList.get(getAdapterPosition()).getUser().getUserId())) {
//
//                        new AnswerEditDeleteDialog(context, new AnswerEditDeleteDialog.OnItemClick() {
//                            @Override
//                            public void onClick(String tag) {
//
//                                if (tag.equalsIgnoreCase("delete")) {
//                                    deleteAnswer(DiscussionHolder.this);
//                                } else {
//                                    clickListener.onItemClick(getAdapterPosition(), "edit");
//                                }
//
//                            }
//                        }).show();
//
//                    }
//                    break;
//
//
//            }
//        }
//
//        private void initViews() {
//            img_user_pic = itemView.findViewById(R.id.img_user_pic);
//            img_option = itemView.findViewById(R.id.img_option);
//            txt_person_name = itemView.findViewById(R.id.txt_person_name);
//            txt_person_organisation = itemView.findViewById(R.id.txt_person_organisation);
//            txt_see_answer = itemView.findViewById(R.id.txt_see_answer);
//        }
//
//        private void setClickListener() {
//            img_user_pic.setOnClickListener(this);
//            img_option.setOnClickListener(this);
//            txt_person_name.setOnClickListener(this);
//            txt_person_organisation.setOnClickListener(this);
//            txt_see_answer.setOnClickListener(this);
//
//        }
//
//        private void setTypeFace() {
//            txt_person_name.setTypeface(AppClass.lato_semibold);
//            txt_person_organisation.setTypeface(AppClass.lato_regular);
//
//        }
//
//    }

}
