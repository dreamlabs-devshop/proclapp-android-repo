package com.proclapp.home.adapter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.proclapp.R;
import com.proclapp.model.AllPostsModel;
import com.proclapp.utils.ImageLoadUtils;
import com.proclapp.utils.Logger;

import java.util.ArrayList;

public class EditPostArticleImagesAdapter extends RecyclerView.Adapter<EditPostArticleImagesAdapter.MyHolder> {

    private DisplayMetrics metrics;
    private Activity activity;
    private ArrayList<AllPostsModel.Images> imagesList;
    public static StringBuilder builder = new StringBuilder();
    private String prefix = "";
    private UpdateListOnDelete updateListOnDelete;

    public EditPostArticleImagesAdapter(Activity activity, ArrayList<AllPostsModel.Images> imagesList, UpdateListOnDelete updateListOnDelete) {
        this.activity = activity;
        this.imagesList = imagesList;
        this.updateListOnDelete = updateListOnDelete;

        metrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
    }

    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(activity).inflate(R.layout.row_post_article_images, viewGroup, false);
        return new MyHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyHolder holder, int i) {

        if (imagesList.get(i).getImage_name().isEmpty()) {

            Bitmap myBitmap = BitmapFactory.decodeFile(imagesList.get(holder.getAdapterPosition()).getImage().getAbsolutePath());
            holder.img_upload_post.setImageBitmap(myBitmap);

        } else {

            ImageLoadUtils.imageLoad(activity,
                    holder.img_upload_post,
                    imagesList.get(i).getImage_name());
        }

        holder.img_upload_post_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Logger.d("image id1===" + imagesList.get(holder.getAdapterPosition()).getPost_img_id());

                if (!imagesList.get(holder.getAdapterPosition()).getPost_img_id().isEmpty()) {
                    builder.append(prefix + imagesList.get(holder.getAdapterPosition()).getPost_img_id());
                    prefix = ",";
                    Logger.d("image id2===" + builder.toString());
                }
                imagesList.remove(holder.getAdapterPosition());
                Logger.d("image id3===" + imagesList.size());
                notifyDataSetChanged();

                updateListOnDelete.updateList(imagesList);
            }
        });
    }

    @Override
    public int getItemCount() {
        return imagesList.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {

        private ImageView img_upload_post,
                img_upload_post_delete;

        public MyHolder(@NonNull View itemView) {
            super(itemView);

            img_upload_post_delete = itemView.findViewById(R.id.img_upload_post_delete);
            img_upload_post = itemView.findViewById(R.id.img_upload_post);
            img_upload_post_delete.setVisibility(View.VISIBLE);
            /*FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams((metrics.widthPixels * 132) / 480, (metrics.heightPixels * 132) / 800);
            img_upload_post.setLayoutParams(layoutParams);
            layoutParams.setMargins(15, 15, 15, 15);*/
            img_upload_post.setScaleType(ImageView.ScaleType.CENTER_CROP);

        }
    }

    public void updateList(ArrayList<AllPostsModel.Images> imagesList) {
        this.imagesList = imagesList;
    }

    public ArrayList<AllPostsModel.Images> getUpdatedList() {
        return imagesList;
    }

    public interface UpdateListOnDelete {
        void updateList(ArrayList<AllPostsModel.Images> imagesList);
    }
}
