package com.proclapp.home.adapter;

import android.app.Activity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.model.FollowerFollowingModel;
import com.proclapp.utils.ImageLoadUtils;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 *
 */

public class FlashToUserAdapter extends RecyclerView.Adapter<FlashToUserAdapter.MyViewHolder> implements Filterable {

    Activity activity;
    ArrayList<FollowerFollowingModel> userLists = new ArrayList<>();
    ArrayList<FollowerFollowingModel> categoryListFiltered = new ArrayList<>();

    public FlashToUserAdapter(Activity activity, ArrayList<FollowerFollowingModel> userLists) {
        this.activity = activity;
        this.userLists = userLists;
        categoryListFiltered = userLists;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_flash_to, parent, false);
        return new MyViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        holder.txt_user_name.setText(categoryListFiltered.get(holder.getAdapterPosition()).getName());

        holder.txt_flash_count.setText(categoryListFiltered.get(holder.getAdapterPosition()).getFlashed() +
                " " +
                activity.getString(R.string.flashed));

        ImageLoadUtils.imageLoad(activity,
                holder.img_user_profile,
                categoryListFiltered.get(holder.getAdapterPosition()).getProfileImage(),
                R.drawable.menu_user_ph);

        if (categoryListFiltered.get(holder.getAdapterPosition()).isSelected()) {
            holder.cb_flash.setSelected(true);
        } else {
            holder.cb_flash.setSelected(false);
        }

        holder.ll_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (categoryListFiltered.get(holder.getAdapterPosition()).isSelected()) {
                    categoryListFiltered.get(holder.getAdapterPosition()).setSelected(false);
                } else {
                    categoryListFiltered.get(holder.getAdapterPosition()).setSelected(true);
                }

                notifyDataSetChanged();

            }
        });


    }

    @Override
    public int getItemCount() {
        return categoryListFiltered.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txt_user_name,
                txt_flash_count;
        CircleImageView img_user_profile;
        AppCompatImageView cb_flash;
        LinearLayout ll_main;

        public MyViewHolder(View itemView) {
            super(itemView);
            txt_user_name = itemView.findViewById(R.id.txt_user_name);
            txt_flash_count = itemView.findViewById(R.id.txt_flash_count);
            img_user_profile = itemView.findViewById(R.id.img_user_profile);
            cb_flash = itemView.findViewById(R.id.cb_flash);
            ll_main = itemView.findViewById(R.id.ll_main);

            txt_user_name.setTypeface(AppClass.lato_medium);
            txt_flash_count.setTypeface(AppClass.lato_medium);

        }
    }


    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                Log.d("getFilter", "adapter" + charSequence);

                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    categoryListFiltered = userLists;
                } else {
                    ArrayList<FollowerFollowingModel> filteredList = new ArrayList<>();
                    for (FollowerFollowingModel row : userLists) {

                        if (row.getName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    categoryListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = categoryListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                categoryListFiltered = (ArrayList<FollowerFollowingModel>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
