package com.proclapp.home.adapter;

import android.app.Activity;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.proclapp.AppClass;
import com.proclapp.R;

import de.hdodenhof.circleimageview.CircleImageView;

public class HomeMainAdapter extends RecyclerView.Adapter<HomeMainAdapter.MyViewHolder> {

    Activity activity;

    public HomeMainAdapter(Activity activity) {
        this.activity = activity;


    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_home_page_adapter, parent, false);
        return new MyViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        if (position == 0) {
            holder.txt_question_technology_innovation.setText("Question | Technology Innovation");
            holder.ll_comment.setVisibility(View.GONE);
            holder.ll_question_answer.setVisibility(View.VISIBLE);
            holder.txt_question_value.setVisibility(View.VISIBLE);
            holder.txt_answer_value.setVisibility(View.GONE);
            holder.img_article.setVisibility(View.GONE);
            holder.ll_document.setVisibility(View.GONE);
            holder.ll_question_answer_shared.setVisibility(View.GONE);
            holder.ll_shared.setVisibility(View.GONE);
        } else if (position == 1) {

            holder.txt_question_technology_innovation.setText("Article | Insecurity in africa");

            holder.ll_comment.setVisibility(View.GONE);
            holder.ll_question_answer.setVisibility(View.VISIBLE);
            holder.ll_document.setVisibility(View.GONE);
            holder.ll_question_answer_shared.setVisibility(View.GONE);
            holder.ll_shared.setVisibility(View.GONE);
            holder.txt_share_count.setVisibility(View.GONE);
            holder.txt_flash_count.setVisibility(View.GONE);
            holder.txt_answer.setText(activity.getResources().getString(R.string.discuss));
            holder.txt_answer.setCompoundDrawablesWithIntrinsicBounds(R.drawable.discuss, 0, 0, 0);
        } else if (position == 2) {

            holder.txt_question_technology_innovation.setText("Research Paper | Health institute");

            holder.ll_comment.setVisibility(View.GONE);
            holder.ll_question_answer.setVisibility(View.GONE);
            holder.ll_document.setVisibility(View.VISIBLE);
            holder.ll_question_answer_shared.setVisibility(View.GONE);
            holder.ll_shared.setVisibility(View.GONE);
            holder.txt_flash_count.setVisibility(View.GONE);
            holder.txt_answer.setText(activity.getResources().getString(R.string.download));
            holder.txt_answer.setCompoundDrawablesWithIntrinsicBounds(R.drawable.download, 0, 0, 0);

            holder.txt_decline.setText(activity.getResources().getString(R.string.recommand));
            holder.txt_decline.setCompoundDrawablesWithIntrinsicBounds(R.drawable.recommand, 0, 0, 0);

            holder.txt_carryover.setText(activity.getResources().getString(R.string.bookmark));
            holder.txt_carryover.setCompoundDrawablesWithIntrinsicBounds(R.drawable.bookmark, 0, 0, 0);

            holder.ll_flash.setVisibility(View.GONE);
        } else if (position == 3) {

            holder.txt_question_technology_innovation.setText("Shared | Technology Innovation");

            holder.ll_comment.setVisibility(View.VISIBLE);
            holder.ll_question_answer.setVisibility(View.GONE);
            holder.ll_document.setVisibility(View.GONE);
            holder.ll_question_answer_shared.setVisibility(View.VISIBLE);
            holder.ll_shared.setVisibility(View.VISIBLE);
            holder.txt_comments_count.setVisibility(View.GONE);
            holder.txt_like_count.setVisibility(View.GONE);
            holder.txt_share_count.setVisibility(View.GONE);
            holder.txt_flash_count.setVisibility(View.GONE);
            holder.txt_answer.setText(activity.getResources().getString(R.string.upvote));
            holder.txt_answer.setCompoundDrawablesWithIntrinsicBounds(R.drawable.upvote_blue, 0, 0, 0);

            holder.txt_decline.setText(activity.getResources().getString(R.string.downvote));
            holder.txt_decline.setCompoundDrawablesWithIntrinsicBounds(R.drawable.downvote, 0, 0, 0);

            holder.ll_carryover.setVisibility(View.GONE);
            holder.ll_flash.setVisibility(View.INVISIBLE);
        }

        holder.ll_option.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*new HomePageOptionsDialog(activity,
                        "",
                        "", new HomePageOptionsDialog.UpdateListData() {
                    @Override
                    public void updateData(String postId) {

                    }
                }).show();*/
            }
        });


        holder.ll_question.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (position == 0 || position == 1) {
                   // activity.startActivity(new Intent(activity, QuestionDetailsActivity.class));
                }
            }
        });

        holder.ll_flash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //activity.startActivity(new Intent(activity, FlashActivity.class));
            }
        });

    }

    @Override
    public int getItemCount() {
        return 4;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView
                txt_question_technology_innovation,
                txt_user_name,
                txt_user_place,
                txt_flash_count,
                txt_date,
                txt_question_value,
                txt_answer_value,
                txt_description,
                txt_doc_name,
                txt_doc_size,
                txt_shareed_title,
                txt_user_name_shared,
                txt_user_place_shared,
                txt_date_shared,
                txt_question_value_shared,
                txt_answer_value_shared,
                txt_comments_count,
                txt_share_count,
                txt_like_count,
                txt_answer,
                txt_decline,
                txt_carryover,
                txt_flash;
        private CircleImageView img_user_profile,
                img_user_profile_shared;
        private ImageView img_follow,
                img_option,
                img_article,
                img_doc,
                img_article_shared,
                img_emoji;
        private LinearLayout ll_question_answer,
                ll_document,
                ll_question_answer_shared,
                ll_comment,
                ll_shared,
                ll_answer,
                ll_decline,
                ll_carryover,
                ll_flash,
                ll_option,
                ll_question;
        private EditText edt_comment;

        public MyViewHolder(View itemView) {
            super(itemView);

            txt_question_technology_innovation = itemView.findViewById(R.id.txt_question_technology_innovation);
            txt_user_name = itemView.findViewById(R.id.txt_user_name);
            txt_user_place = itemView.findViewById(R.id.txt_user_place);
            txt_flash_count = itemView.findViewById(R.id.txt_flash_count);
            txt_date = itemView.findViewById(R.id.txt_date);
            txt_question_value = itemView.findViewById(R.id.txt_question_value);
            txt_answer_value = itemView.findViewById(R.id.txt_answer_value);
            txt_description = itemView.findViewById(R.id.txt_description);
            txt_doc_name = itemView.findViewById(R.id.txt_doc_name);
            txt_doc_size = itemView.findViewById(R.id.txt_doc_size);
            txt_shareed_title = itemView.findViewById(R.id.txt_shareed_title);
            txt_user_name_shared = itemView.findViewById(R.id.txt_user_name_shared);
            txt_user_place_shared = itemView.findViewById(R.id.txt_user_place_shared);
            txt_date_shared = itemView.findViewById(R.id.txt_date_shared);
            txt_question_value_shared = itemView.findViewById(R.id.txt_question_value_shared);
            txt_answer_value_shared = itemView.findViewById(R.id.txt_answer_value_shared);
            txt_comments_count = itemView.findViewById(R.id.txt_comments_count);
            txt_share_count = itemView.findViewById(R.id.txt_share_count);
            txt_like_count = itemView.findViewById(R.id.txt_like_count);
            txt_answer = itemView.findViewById(R.id.txt_answer);
            txt_decline = itemView.findViewById(R.id.txt_decline);
            txt_carryover = itemView.findViewById(R.id.txt_carryover);
            txt_flash = itemView.findViewById(R.id.txt_flash);

            img_user_profile = itemView.findViewById(R.id.img_user_profile);
            img_user_profile_shared = itemView.findViewById(R.id.img_user_profile_shared);

            edt_comment = itemView.findViewById(R.id.edt_comment);

            img_follow = itemView.findViewById(R.id.img_follow);
            img_option = itemView.findViewById(R.id.img_option);
            img_article = itemView.findViewById(R.id.img_article);
            img_doc = itemView.findViewById(R.id.img_doc);
            img_article_shared = itemView.findViewById(R.id.img_article_shared);
            img_emoji = itemView.findViewById(R.id.img_emoji);

            ll_question_answer = itemView.findViewById(R.id.ll_question_answer);
            ll_document = itemView.findViewById(R.id.ll_document);
            ll_question_answer_shared = itemView.findViewById(R.id.ll_question_answer_shared);
            ll_comment = itemView.findViewById(R.id.ll_comment);
            ll_shared = itemView.findViewById(R.id.ll_shared);
            ll_answer = itemView.findViewById(R.id.ll_answer);
            ll_decline = itemView.findViewById(R.id.ll_decline);
            ll_carryover = itemView.findViewById(R.id.ll_carryover);
            ll_flash = itemView.findViewById(R.id.ll_flash);
            ll_option = itemView.findViewById(R.id.ll_option);
            ll_question = itemView.findViewById(R.id.ll_question);

            // set typeface

            txt_question_technology_innovation.setTypeface(AppClass.lato_regular);
            txt_user_name.setTypeface(AppClass.lato_medium);
            txt_user_place.setTypeface(AppClass.lato_regular);
            txt_flash_count.setTypeface(AppClass.lato_regular);
            txt_date.setTypeface(AppClass.lato_regular);
            txt_question_value.setTypeface(AppClass.lato_bold);
            txt_answer_value.setTypeface(AppClass.lato_regular);
            txt_description.setTypeface(AppClass.lato_regular);
            txt_doc_name.setTypeface(AppClass.lato_heavy);
            txt_doc_size.setTypeface(AppClass.lato_regular);
            txt_shareed_title.setTypeface(AppClass.lato_bold);
            txt_user_name_shared.setTypeface(AppClass.lato_medium);
            txt_user_place_shared.setTypeface(AppClass.lato_regular);
            txt_date_shared.setTypeface(AppClass.lato_regular);
            txt_question_value_shared.setTypeface(AppClass.lato_bold);
            txt_answer_value_shared.setTypeface(AppClass.lato_regular);
            txt_comments_count.setTypeface(AppClass.lato_regular);
            txt_share_count.setTypeface(AppClass.lato_regular);
            txt_like_count.setTypeface(AppClass.lato_regular);
            txt_answer.setTypeface(AppClass.lato_regular);
            txt_decline.setTypeface(AppClass.lato_regular);
            txt_carryover.setTypeface(AppClass.lato_regular);
            txt_flash.setTypeface(AppClass.lato_regular);

            edt_comment.setTypeface(AppClass.lato_medium);
        }
    }


}
