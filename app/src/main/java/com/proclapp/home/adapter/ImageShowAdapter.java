package com.proclapp.home.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.proclapp.R;
import com.proclapp.model.AllPostsModel;
import com.proclapp.utils.ImageLoadUtils;

import java.util.ArrayList;

public class ImageShowAdapter extends RecyclerView.Adapter<ImageShowAdapter.ImageShowHolder> {

    ArrayList<AllPostsModel.Images> images;
    private Context context;

    public ImageShowAdapter(Context context, ArrayList<AllPostsModel.Images> images) {
        this.context = context;
        this.images = images;
    }

    @NonNull
    @Override
    public ImageShowHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_article_image, viewGroup, false);
        return new ImageShowHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ImageShowHolder holder, int i) {

        ImageLoadUtils.imageLoad(context, holder.img_article, images.get(i).getImage_name());

    }

    @Override
    public int getItemCount() {
        return images.size();
    }

    public class ImageShowHolder extends RecyclerView.ViewHolder {

        ImageView img_article;

        public ImageShowHolder(@NonNull View itemView) {
            super(itemView);
            img_article = itemView.findViewById(R.id.img_article);
        }
    }
}
