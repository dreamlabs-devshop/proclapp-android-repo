package com.proclapp.home.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.proclapp.R;
import com.proclapp.home.friends.FriendsActivity;
import com.proclapp.interfaces.RecyclerClickListener;
import com.proclapp.model.NotificationModel;
import com.proclapp.utils.DateTimeUtils;
import com.proclapp.utils.ImageLoadUtils;
import com.proclapp.utils.StringUtils;

import java.util.ArrayList;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.NotificationHolder> {

    private Context context;
    private ArrayList<NotificationModel> notificationList;
    private RecyclerClickListener clickListener;


    public NotificationAdapter(Context context, ArrayList<NotificationModel> notificationList, RecyclerClickListener clickListener) {
        this.context = context;
        this.notificationList = notificationList;
        this.clickListener = clickListener;

    }

    @NonNull
    @Override
    public NotificationHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_notification, viewGroup, false);
        return new NotificationHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final NotificationHolder holder, int i) {
        NotificationModel notificationData = notificationList.get(holder.getAdapterPosition());

        holder.tv_noti_message.setText(notificationData.getMessage());
        holder.tv_edit_suggestion_by.setText(notificationData.getMessage());
        if (StringUtils.isNotEmpty(notificationData.getPost_title()))
            holder.txt_question_value.setText(notificationData.getPost_title());
        holder.tv_noti_time.setText(DateTimeUtils.changeDateTimeFormat("yyyy-MM-dd HH:mm:ss", "hh:mm a, MMM dd", notificationData.getDate()));

        ImageLoadUtils.imageLoad(context, holder.iv_profile, notificationData.getImage_thumb(), R.drawable.profile_pic_ph);

        holder.itemView.setOnClickListener(v -> {
            if (notificationList.get(holder.getAdapterPosition()).getNotificationType().equalsIgnoreCase("1")||
                    notificationList.get(holder.getAdapterPosition()).getNotificationType().equalsIgnoreCase("2")) {
                context.startActivity(new Intent(context, FriendsActivity.class)
                        .putExtra("openfrom", "notification"));
            } else /*if (notificationList.get(holder.getAdapterPosition()).getNotificationType().equalsIgnoreCase("5") ||
                    notificationList.get(holder.getAdapterPosition()).getNotificationType().equalsIgnoreCase("4") ||
                    notificationList.get(holder.getAdapterPosition()).getNotificationType().equalsIgnoreCase("9") ||
                    notificationList.get(holder.getAdapterPosition()).getNotificationType().equalsIgnoreCase("18") ||
                    notificationList.get(holder.getAdapterPosition()).getNotificationType().equalsIgnoreCase("13"))*/ {

                clickListener.onItemClick(holder.getAdapterPosition(), "post");

            }
        });

        if (notificationData.getNotificationType().equalsIgnoreCase("13")) {
            holder.ll_accept_decline.setVisibility(View.VISIBLE);
            holder.ll_data.setVisibility(View.GONE);
        } else {
            holder.ll_accept_decline.setVisibility(View.GONE);
            holder.ll_data.setVisibility(View.VISIBLE);
        }

        holder.txt_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickListener.onItemClick(holder.getAdapterPosition(), "1");
            }
        });
        holder.txt_decline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickListener.onItemClick(holder.getAdapterPosition(), "0");
            }
        });

        if (notificationData.getNotificationType().equalsIgnoreCase("23")){
            holder.tv_noti_message.setMaxLines(2);
        }else {
            holder.tv_noti_message.setMaxLines(5);
        }


    }

    @Override
    public int getItemCount() {
        return notificationList.size();
    }


    public class NotificationHolder extends RecyclerView.ViewHolder {

        private TextView
                tv_noti_message,
                tv_edit_suggestion_by,
                txt_question_value,
                txt_accept,
                txt_decline,
                tv_noti_time;
        private LinearLayout
                ll_accept_decline,
                ll_data;
        private ImageView iv_profile;

        public NotificationHolder(@NonNull View itemView) {
            super(itemView);

            tv_noti_message = itemView.findViewById(R.id.tv_noti_message);
            tv_noti_time = itemView.findViewById(R.id.tv_noti_time);
            ll_accept_decline = itemView.findViewById(R.id.ll_accept_decline);
            ll_data = itemView.findViewById(R.id.ll_data);
            tv_edit_suggestion_by = itemView.findViewById(R.id.tv_edit_suggestion_by);
            txt_accept = itemView.findViewById(R.id.txt_accept);
            txt_decline = itemView.findViewById(R.id.txt_decline);
            txt_question_value = itemView.findViewById(R.id.txt_question_value);
            iv_profile = itemView.findViewById(R.id.iv_profile);

        }
    }

}
