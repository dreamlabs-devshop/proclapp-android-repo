package com.proclapp.home.adapter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.proclapp.R;
import com.proclapp.model.PostAdvUploadModel;

import java.util.ArrayList;

public class PostArticleImagesAdapter extends RecyclerView.Adapter<PostArticleImagesAdapter.MyHolder> {

    private DisplayMetrics metrics;
    private Activity activity;
    private ArrayList<PostAdvUploadModel> imagesList;

    public PostArticleImagesAdapter(Activity activity, ArrayList<PostAdvUploadModel> imagesList) {
        this.activity = activity;
        this.imagesList = imagesList;

        metrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
    }

    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(activity).inflate(R.layout.row_post_article_images, viewGroup, false);
        return new MyHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyHolder holder, int i) {

        Bitmap myBitmap = BitmapFactory.decodeFile(imagesList.get(holder.getAdapterPosition()).getImage().getAbsolutePath());
        holder.img_upload_post.setImageBitmap(myBitmap);

        holder.img_upload_post_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imagesList.remove(holder.getAdapterPosition());
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return imagesList.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {

        private ImageView
                img_upload_post,
                img_upload_post_delete;

        public MyHolder(@NonNull View itemView) {
            super(itemView);

            img_upload_post_delete = itemView.findViewById(R.id.img_upload_post_delete);
            img_upload_post = itemView.findViewById(R.id.img_upload_post);

            /*FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams((metrics.widthPixels * 132) / 480, (metrics.heightPixels * 132) / 800);
            img_upload_post.setLayoutParams(layoutParams);
            layoutParams.setMargins(15, 15, 15, 15);*/
            img_upload_post.setScaleType(ImageView.ScaleType.CENTER_CROP);

        }
    }
}
