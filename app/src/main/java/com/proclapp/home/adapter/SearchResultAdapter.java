package com.proclapp.home.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.proclapp.R;
import com.proclapp.home.DiscussActivity;
import com.proclapp.home.question.QuestionDetailsActivity;
import com.proclapp.model.AllPostsModel;
import com.proclapp.utils.ImageLoadUtils;
import com.proclapp.utils.StringUtils;

import java.util.ArrayList;

public class SearchResultAdapter extends RecyclerView.Adapter<SearchResultAdapter.SearchResultHolder> {


    private Context context;
    private ArrayList<AllPostsModel> allPostsLists;

    public SearchResultAdapter(Context context, ArrayList<AllPostsModel> allPostsLists) {
        this.context = context;
        this.allPostsLists = allPostsLists;
    }

    @NonNull
    @Override
    public SearchResultHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_search_post, viewGroup, false);
        return new SearchResultHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final SearchResultHolder holder, int i) {

        if (StringUtils.isNotEmpty(allPostsLists.get(holder.getAdapterPosition()).getPostTitle()))
            holder.tv_title.setText(allPostsLists.get(holder.getAdapterPosition()).getPostTitle());

        ImageLoadUtils.imageLoad(context,
                holder.iv_profile,
                allPostsLists.get(holder.getAdapterPosition()).getProfileImage(),
                R.drawable.menu_user_ph);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (allPostsLists.get(holder.getAdapterPosition()).getPostType().equalsIgnoreCase("0")) {
                    context.startActivity(new Intent(context, QuestionDetailsActivity.class)
                            .putExtra("openfrom", "rp")
                            .putExtra("questionDetails", allPostsLists.get(holder.getAdapterPosition())));

                } else if (allPostsLists.get(holder.getAdapterPosition()).getPostType().equalsIgnoreCase("1")) {
                    context.startActivity(new Intent(context, DiscussActivity.class)
                            .putExtra("articleDetails", allPostsLists.get(holder.getAdapterPosition())));

                } else {
                    context.startActivity(new Intent(context, QuestionDetailsActivity.class)
                            .putExtra("openfrom", "qa")
                            .putExtra("questionDetails", allPostsLists.get(holder.getAdapterPosition())));
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return allPostsLists.size();
    }

    public class SearchResultHolder extends RecyclerView.ViewHolder {

        ImageView iv_profile;
        TextView tv_title;

        public SearchResultHolder(@NonNull View itemView) {
            super(itemView);
            iv_profile = itemView.findViewById(R.id.iv_profile);
            tv_title = itemView.findViewById(R.id.tv_title);

        }
    }
}

