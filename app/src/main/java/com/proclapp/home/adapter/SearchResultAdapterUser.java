package com.proclapp.home.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.home.DiscussActivity;
import com.proclapp.home.question.QuestionDetailsActivity;
import com.proclapp.model.AnswerModel;
import com.proclapp.profile.OtherUserProfileActivity;
import com.proclapp.profile.ProfileActivity;
import com.proclapp.utils.ImageLoadUtils;
import com.proclapp.utils.StringUtils;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class SearchResultAdapterUser extends RecyclerView.Adapter<SearchResultAdapterUser.SearchResultHolder> {


    private Context context;
    private ArrayList<AnswerModel.SearchedUser> allUsersLists;

    public SearchResultAdapterUser(Context context, ArrayList<AnswerModel.SearchedUser> allUsersLists) {
        this.context = context;
        this.allUsersLists = allUsersLists;
    }

    @NonNull
    public SearchResultHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_search_post_user, viewGroup, false);
        return new SearchResultAdapterUser.SearchResultHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final SearchResultAdapterUser.SearchResultHolder holder, int i) {

        if (StringUtils.isNotEmpty(allUsersLists.get(holder.getAdapterPosition()).getName()))
            holder.tv_name.setText(allUsersLists.get(holder.getAdapterPosition()).getName());

        if (StringUtils.isNotEmpty(allUsersLists.get(holder.getAdapterPosition()).getEmail()))
            holder.tv_email.setText(allUsersLists.get(holder.getAdapterPosition()).getEmail());

        ImageLoadUtils.imageLoad(context,
                holder.iv_profile_pic,
                allUsersLists.get(holder.getAdapterPosition()).getProfileImage(),
                R.drawable.menu_user_ph);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.getAdapterPosition() >= 0)
                    if (AppClass.preferences.getUserId().equalsIgnoreCase(allUsersLists.get(holder.getAdapterPosition()).getUserId())) {
                        context.startActivity(new Intent(context, ProfileActivity.class));

                    } else {
                        context.startActivity(new Intent(context, OtherUserProfileActivity.class)
                                .putExtra("user_id", allUsersLists.get(holder.getAdapterPosition()).getUserId()));
                    }
            }
        });

    }

    @Override
    public int getItemCount() {
        return allUsersLists.size();
    }

    public class SearchResultHolder extends RecyclerView.ViewHolder {

        CircleImageView iv_profile_pic;
        TextView tv_name, tv_email;

        public SearchResultHolder(@NonNull View itemView) {
            super(itemView);
            iv_profile_pic = itemView.findViewById(R.id.iv_profile_pic);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_email = itemView.findViewById(R.id.tv_email);

        }
    }
}
