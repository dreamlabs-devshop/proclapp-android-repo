package com.proclapp.home.adapter;

import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.expert.ExpertModel;
import com.proclapp.utils.ImageLoadUtils;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 *
 */

public class SelectExpertAdapter extends RecyclerView.Adapter<SelectExpertAdapter.MyViewHolder> implements Filterable {

    private ExpertSelection expertSelectionListener;
    private Activity activity;
    private ArrayList<ExpertModel> list;
    private ArrayList<ExpertModel> categoryListFiltered;
    private int oldPos = -1;

    public SelectExpertAdapter(Activity activity, ArrayList<ExpertModel> list, ExpertSelection expertSelectionListener) {
        this.activity = activity;
        this.list = list;
        categoryListFiltered = list;
        this.expertSelectionListener = expertSelectionListener;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_select_expert, parent, false);
        return new MyViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.cb_flash.setEnabled(false);

        if (categoryListFiltered.get(holder.getAdapterPosition()).isExpertSelected()) {
            holder.cb_flash.setChecked(true);
        } else {
            holder.cb_flash.setChecked(false);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                expertSelectionListener.onExpertSelect(categoryListFiltered.get(holder.getAdapterPosition()).getUserId());

                if (oldPos >= 0) {
                    categoryListFiltered.get(oldPos).setExpertSelected(false);
                }

                categoryListFiltered.get(holder.getAdapterPosition()).setExpertSelected(true);

                oldPos = holder.getAdapterPosition();

                notifyDataSetChanged();
            }
        });

        holder.txt_user_name.setText(String.format("%s %s", categoryListFiltered.get(holder.getAdapterPosition()).getFirstname(), categoryListFiltered.get(holder.getAdapterPosition()).getLastname()));
        holder.txt_flash_count.setVisibility(View.GONE);

        ImageLoadUtils.imageLoad(activity,
                holder.img_user_profile,
                categoryListFiltered.get(holder.getAdapterPosition()).getProfileImage(),
                R.drawable.flash_user_ph);


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                Log.d("getFilter", "adapter" + charSequence);

                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    categoryListFiltered = list;
                } else {
                    ArrayList<ExpertModel> filteredList = new ArrayList<>();
                    for (ExpertModel row : list) {

                        if (row.getFirstname().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    categoryListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = categoryListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                categoryListFiltered = (ArrayList<ExpertModel>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public interface ExpertSelection {
        void onExpertSelect(String expertId);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txt_user_name,
                txt_flash_count;
        CircleImageView img_user_profile;
        CheckBox cb_flash;
        LinearLayout ll_main;
        ImageView img_online;

        public MyViewHolder(View itemView) {
            super(itemView);
            txt_user_name = itemView.findViewById(R.id.txt_user_name);
            txt_flash_count = itemView.findViewById(R.id.txt_flash_count);
            img_user_profile = itemView.findViewById(R.id.img_user_profile);
            cb_flash = itemView.findViewById(R.id.cb_flash);
            ll_main = itemView.findViewById(R.id.ll_main);
            img_online = itemView.findViewById(R.id.img_online);

            txt_user_name.setTypeface(AppClass.lato_medium);
            txt_flash_count.setTypeface(AppClass.lato_medium);

        }
    }

}
