package com.proclapp.home.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.model.AllPostsModel;
import com.proclapp.profile.OtherUserProfileActivity;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.AlertDialogInterface;
import com.proclapp.utils.DialogUtils;
import com.proclapp.utils.ImageLoadUtils;
import com.proclapp.utils.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;

public class SuggetionTrendigViewAdapter extends RecyclerView.Adapter<SuggetionTrendigViewAdapter.SuggetionTrendigViewHolder> {

    private Context context;
    private ArrayList<AllPostsModel> allPostsList;
    private int pos;
    private String openFor = "";

    public SuggetionTrendigViewAdapter(Context context, ArrayList<AllPostsModel> allPostsList, int pos) {
        this.context = context;
        this.allPostsList = allPostsList;
        this.pos = pos;
    }

    @NonNull
    @Override
    public SuggetionTrendigViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(context).inflate(R.layout.row_suggetion_trending_view, viewGroup, false);
        return new SuggetionTrendigViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SuggetionTrendigViewHolder holder, int i) {


        if (openFor.equalsIgnoreCase("sFriend")) {

            ImageLoadUtils.imageLoad(context, holder.iv_pro_pic, allPostsList.get(pos).getSuggestedFriends().get(holder.getAdapterPosition()).getProfileImage(), R.drawable.profile_pic_ph);
            holder.tv_name.setText(allPostsList.get(pos).getSuggestedFriends().get(holder.getAdapterPosition()).getName());
            holder.tv_pro.setText(allPostsList.get(pos).getSuggestedFriends().get(holder.getAdapterPosition()).getProfession());
            holder.tv_follower_count.setText(context.getString(R.string.total_follower, allPostsList.get(pos).getSuggestedFriends().get(holder.getAdapterPosition()).getTotalFollowers()));
            holder.tv_pro.setVisibility(View.VISIBLE);
            holder.tv_follow.setText(context.getString(R.string.add_friend));
        } else if (openFor.equalsIgnoreCase("sExpert")) {

            ImageLoadUtils.imageLoad(context, holder.iv_pro_pic, allPostsList.get(pos).getSuggestedExperts().get(holder.getAdapterPosition()).getProfileImage(), R.drawable.profile_pic_ph);
            holder.tv_name.setText(allPostsList.get(pos).getSuggestedExperts().get(holder.getAdapterPosition()).getName());
            holder.tv_follower_count.setText(context.getString(R.string.total_follower, allPostsList.get(pos).getSuggestedExperts().get(holder.getAdapterPosition()).getTotalFollowers()));
            holder.tv_follow.setText(context.getString(R.string.follow));
//            holder.tv_pro.setText(allPostsList.get(pos).getSuggestedExperts().get(i).getProfession());
            holder.tv_pro.setVisibility(View.VISIBLE);
        } else {
            ImageLoadUtils.imageLoad(context, holder.iv_pro_pic, allPostsList.get(pos).getTrendingTopics().get(holder.getAdapterPosition()).getCategory_img(), R.drawable.profile_pic_ph);
            holder.tv_name.setText(allPostsList.get(pos).getTrendingTopics().get(holder.getAdapterPosition()).getCategory_name());
            holder.tv_pro.setVisibility(View.GONE);
            holder.tv_follow.setText(context.getString(R.string.follow));
            holder.tv_follower_count.setText(context.getString(R.string.total_follower, allPostsList.get(pos).getTrendingTopics().get(holder.getAdapterPosition()).getTotalFollowers()));
        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (openFor.equalsIgnoreCase("sFriend")) {

                    context.startActivity(new Intent(context, OtherUserProfileActivity.class)
                            .putExtra("user_id", allPostsList.get(pos).getSuggestedFriends().get(holder.getAdapterPosition()).getUserId()));

                } else if (openFor.equalsIgnoreCase("sExpert")) {
                    context.startActivity(new Intent(context, OtherUserProfileActivity.class)
                            .putExtra("user_id", allPostsList.get(pos).getSuggestedExperts().get(holder.getAdapterPosition()).getUserId()));
                }
            }
        });

        holder.tv_follow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (openFor.equalsIgnoreCase("sFriend")) {
                    if (holder.tv_follow.getText().toString().equalsIgnoreCase(context.getString(R.string.add_friend))) {
                        callAPIfriendUnfriend(allPostsList.get(pos).getSuggestedFriends().get(holder.getAdapterPosition()).getUserId());
                        holder.tv_follow.setText(context.getString(R.string.friends));
                    }

                } else if (openFor.equalsIgnoreCase("sExpert")) {
                    if (holder.tv_follow.getText().toString().equalsIgnoreCase(context.getString(R.string.follow))) {
                        callAPIChangeFollowUnfollowStatus(allPostsList.get(pos).getSuggestedExperts().get(holder.getAdapterPosition()).getUserId());
                        holder.tv_follow.setText(context.getString(R.string.following));
                    }
                } else {

                    if (holder.tv_follow.getText().toString().equalsIgnoreCase(context.getString(R.string.follow))) {
                        holder.tv_follow.setText(context.getString(R.string.following));
                        updateCategoryToUserProfile(allPostsList.get(pos).getTrendingTopics().get(holder.getAdapterPosition()).getCategory_id());
                    }

                }


            }
        });


    }

    @Override
    public int getItemCount() {

        if (StringUtils.isNotEmpty(allPostsList.get(pos).getsFriend()) &&
                allPostsList.get(pos).getsFriend().equalsIgnoreCase("1")) {
            openFor = "sFriend";
            return allPostsList.get(pos).getSuggestedFriends().size();
        } else if (StringUtils.isNotEmpty(allPostsList.get(pos).getsExpert()) &&
                allPostsList.get(pos).getsExpert().equalsIgnoreCase("1")) {
            openFor = "sExpert";
            return allPostsList.get(pos).getSuggestedExperts().size();
        } else {
            openFor = "trTopics";
            return allPostsList.get(pos).getTrendingTopics().size();
        }

    }


    private void callAPIChangeFollowUnfollowStatus(String otherUserId) {

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("follower_id", otherUserId);
        request.put("type", "1");

        ApiCall.getInstance().changeFollowUnfollowStatus((AppCompatActivity) context, request, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {
                String response = (String) data;


            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {
                if (StringUtils.isNotEmpty(errorMessage)) {
                    DialogUtils.openDialog((AppCompatActivity) context, errorMessage, context.getString(R.string.ok), "", new AlertDialogInterface() {
                        @Override
                        public void onNegativeBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }

                        @Override
                        public void onPositiveBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }
                    });
                }
            }
        }, true);


    }

    private void callAPIfriendUnfriend(String otherUserId) {


        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("friend_id", otherUserId);
        request.put("type", "1");

        ApiCall.getInstance().friendUnfriend((AppCompatActivity) context, request, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {


            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {
                if (StringUtils.isNotEmpty(errorMessage)) {
                    DialogUtils.openDialog((AppCompatActivity) context, errorMessage, context.getString(R.string.ok), "", new AlertDialogInterface() {
                        @Override
                        public void onNegativeBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }

                        @Override
                        public void onPositiveBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }
                    });
                }

            }
        }, true);

    }

    private void updateCategoryToUserProfile(String category) {

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("category", category);

        ApiCall.getInstance().updateCategoryToUserProfile((AppCompatActivity) context, request, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {
                String responce = (String) data;
            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {
                if (StringUtils.isNotEmpty(errorMessage)) {
                    DialogUtils.openDialog((AppCompatActivity) context, errorMessage, context.getString(R.string.ok), "", new AlertDialogInterface() {
                        @Override
                        public void onNegativeBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }

                        @Override
                        public void onPositiveBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }
                    });
                }
            }
        }, false);


    }


    public class SuggetionTrendigViewHolder extends RecyclerView.ViewHolder {

        private ImageView iv_pro_pic;
        private TextView
                tv_name,
                tv_pro,
                tv_follow,
                tv_follower_count;

        public SuggetionTrendigViewHolder(@NonNull View itemView) {
            super(itemView);

            iv_pro_pic = itemView.findViewById(R.id.iv_pro_pic);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_pro = itemView.findViewById(R.id.tv_pro);
            tv_follower_count = itemView.findViewById(R.id.tv_follower_count);
            tv_follow = itemView.findViewById(R.id.tv_follow);

        }
    }

}
