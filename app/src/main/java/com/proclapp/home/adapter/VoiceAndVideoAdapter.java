package com.proclapp.home.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.MediaPlayer;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.net.Uri;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.home.DiscussActivity;
import com.proclapp.home.PostDiscussionActivity;
import com.proclapp.home.adapter.viewholder.AdvertViewHolder;
import com.proclapp.home.adapter.viewholder.ArticalViewHolder;
import com.proclapp.home.adapter.viewholder.VoiceVideoViewHolder;
import com.proclapp.home.voice_video.VideoPlayActivity;
import com.proclapp.home.voice_video.VoiceVideoDetails;
import com.proclapp.interfaces.RecyclerClickListener;
import com.proclapp.model.AllPostsModel;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.DateTimeUtils;
import com.proclapp.utils.ImageLoadUtils;
import com.proclapp.utils.StringUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;


/**
 *
 */

public class VoiceAndVideoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    AppCompatActivity activity;
    DisplayMetrics metrics;
    private ArrayList<AllPostsModel> voiceVideoList;
    private MediaPlayer mPlayer;

    public VoiceAndVideoAdapter(AppCompatActivity activity, ArrayList<AllPostsModel> voiceVideoList) {
        this.activity = activity;
        this.voiceVideoList = voiceVideoList;

        metrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == 0) {
            View rootView = LayoutInflater.from(activity).inflate(R.layout.row_voice_and_video_adapter, parent, false);
            return new VoiceVideoViewHolder(rootView, activity, new AllPostsAdapter(activity, voiceVideoList, new RecyclerClickListener() {
                @Override
                public void onItemClick(int pos, String tag) {

                }
            }), voiceVideoList);
        }
        View view = LayoutInflater.from(activity).inflate(R.layout.row_advert, parent, false);
        return new AdvertViewHolder(view, activity, voiceVideoList, this);

    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder viewHolder, int position) {


        if (viewHolder instanceof VoiceVideoViewHolder) {
            final VoiceVideoViewHolder holder = (VoiceVideoViewHolder) viewHolder;
            AllPostsModel audioVideoData = voiceVideoList.get(viewHolder.getAdapterPosition());

            increaseDrawableSizes(holder);
            holder.txt_user_name.setTypeface(holder.txt_user_name.getTypeface(), Typeface.BOLD);

            //holder.txt_comments_count.setText(voiceVideoList.get(holder.getAdapterPosition()).getAnswer_comment_count());
            if (StringUtils.isNotEmpty(audioVideoData.getIsPostLiked()) &&
                    audioVideoData.getIsPostLiked().equalsIgnoreCase("true")) {
                holder.txt_like.setSelected(true);
            } else {
                holder.txt_like.setSelected(false);
            }
            holder.txt_like.setText(audioVideoData.getPostLikeCount());

            if (StringUtils.isNotEmpty(audioVideoData.getTotal_answers()))
                holder.txt_comments_count.setText(audioVideoData.getTotal_answers());
            else
                holder.txt_comments_count.setText("0");


            if (audioVideoData.getVvType().equalsIgnoreCase("1")) {
                holder.rl_video.setVisibility(View.GONE);
                holder.ll_voice.setVisibility(View.VISIBLE);


            } else {
                holder.rl_video.setVisibility(View.VISIBLE);
                holder.ll_voice.setVisibility(View.GONE);

                ImageLoadUtils.imageLoad(activity,
                        holder.img_video,
                        audioVideoData.getVideosPost().getVideoUrl());
            }

            ImageLoadUtils.imageLoad(activity,
                    holder.img_user_profile,
                    audioVideoData.getProfileImage(),
                    R.drawable.menu_user_ph);

            if (StringUtils.isNotEmpty(audioVideoData.getPostDescription()))
                holder.txt_description.setText(audioVideoData.getPostDescription());

            if (StringUtils.isNotEmpty(audioVideoData.getAuthor()))
                holder.txt_user_name.setText(audioVideoData.getAuthor());

            if (StringUtils.isNotEmpty(audioVideoData.getProfessionalQualification()))
                holder.txt_user_place.setText(audioVideoData.getProfessionalQualification());

            if (StringUtils.isNotEmpty(audioVideoData.getCategoryName()))
                holder.txt_title_value.setText(audioVideoData.getCategoryName());

            if (StringUtils.isNotEmpty(audioVideoData.getPostDate()))
                holder.txt_date.setText(DateTimeUtils.calculateTimeBetweenTwoDates(activity, audioVideoData.getPostDate(), false));

            if (audioVideoData.getRequest_to_reply().equalsIgnoreCase("2") ||
                    audioVideoData.getRequest_to_reply().equalsIgnoreCase("3")) {
                holder.tv_see_answer.setVisibility(View.VISIBLE);
                holder.tv_give_answer.setVisibility(View.VISIBLE);
            } else if (audioVideoData.getRequest_to_reply().equalsIgnoreCase("1") ||
                    audioVideoData.getRequest_to_reply().equalsIgnoreCase("4")) {
                holder.tv_see_answer.setVisibility(View.VISIBLE);
                holder.tv_give_answer.setVisibility(View.GONE);
            } else {
                holder.tv_see_answer.setVisibility(View.GONE);
                holder.tv_give_answer.setVisibility(View.GONE);
            }


            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    activity.startActivity(new Intent(activity, VoiceVideoDetails.class)
                            .putExtra("audioVideoDetails", audioVideoData));
//                    if (!voiceVideoList.get(holder.getAdapterPosition()).getVvType().equalsIgnoreCase("1")) {
//
//                        activity.startActivity(new Intent(activity, VideoPlayActivity.class)
//                                .putExtra("audiovideourl", voiceVideoList.get(holder.getAdapterPosition()).getVideosPost().getVideoUrl()));
//
//                    }


                }
            });

            AudioManager am = (AudioManager) activity.getSystemService(Context.AUDIO_SERVICE);
            am.setMode(AudioManager.MODE_NORMAL);
            mPlayer = new MediaPlayer();

            holder.txt_like.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (holder.getAdapterPosition() >= 0)
                        callAPILikeUnlike(holder.txt_like.isSelected() ? "0" : "1", holder);
                }
            });

            holder.txt_share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    String path = voiceVideoList.get(holder.getAdapterPosition()).getVvType().equals("1") ? voiceVideoList.get(holder.getAdapterPosition()).getAudioPost().getAudioUrl() :
                            voiceVideoList.get(holder.getAdapterPosition()).getVideosPost().getVideoUrl();

                    Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                    sharingIntent.setType("*/*");
                    sharingIntent.putExtra(Intent.EXTRA_TEXT, path);
                    activity.startActivity(Intent.createChooser(sharingIntent, activity.getString(R.string.share_using)));

                }
            });


            holder.iv_play.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (mPlayer != null)
                        mPlayer.release();

                    onStreamAudio(voiceVideoList.get(holder.getAdapterPosition()).getAudioPost().getAudioUrl(), holder);
                    holder.iv_pause.setVisibility(View.VISIBLE);
                    holder.iv_play.setVisibility(View.GONE);


                }
            });

            holder.iv_pause.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    try {
                        holder.iv_pause.setVisibility(View.GONE);
                        holder.iv_play.setVisibility(View.VISIBLE);
                        mPlayer.stop();
                        mPlayer.release();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }
            });

            holder.ll_discuss.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (holder.getAdapterPosition() >= 0)
                        activity.startActivity(new Intent(activity, PostDiscussionActivity.class)
                                .putExtra("postId", voiceVideoList.get(holder.getAdapterPosition()).getPostId()).putExtra("isPostVV", "true"));
                }
            });
        } else if (viewHolder instanceof AdvertViewHolder) {

            AdvertViewHolder holder = (AdvertViewHolder) viewHolder;
            AllPostsModel advertData = voiceVideoList.get(holder.getAdapterPosition());

            if (AppClass.preferences.getUserId().equalsIgnoreCase(voiceVideoList.get(holder.getAdapterPosition()).getAuthorId())) {
                holder.img_option.setVisibility(View.VISIBLE);
            } else {
                holder.img_option.setVisibility(View.GONE);
            }

            holder.txt_artical_category.setText(activity.getString(R.string.sponsored));

            holder.txt_user_name.setText(advertData.getAuthor());
            holder.txt_title.setText(advertData.getPostTitle());
            holder.txt_description.setText(advertData.getPostDescription());
            holder.txt_date.setText(DateTimeUtils.changeDateTimeFormat("yyyy-MM-dd HH:mm:ss", "dd MMM", advertData.getPostDate()));
            holder.txt_user_place.setText(advertData.getProfessionalQualification());
            holder.txt_like_count.setText(advertData.getPostLikeCount());

            if (StringUtils.isNotEmpty(advertData.getPostCommentCount()))
                holder.txt_comments_count.setText(advertData.getPostCommentCount());
            else
                holder.txt_comments_count.setText("0");

            if (StringUtils.isNotEmpty(advertData.getIsPostLiked()) &&
                    advertData.getIsPostLiked().equalsIgnoreCase("true")) {
                holder.txt_like_count.setSelected(true);
            } else {
                holder.txt_like_count.setSelected(false);
            }

            ImageLoadUtils.imageLoad(activity,
                    holder.img_user_profile,
                    advertData.getProfileImage(),
                    R.drawable.menu_user_ph);

            if (advertData.getImages().size() == 1) {

                ImageLoadUtils.imageLoad(activity,
                        holder.iv_advert_single_image,
                        advertData.getImages().get(0).getImage_name(),
                        R.drawable.post_img_ph);

                holder.ll_multiple_image.setVisibility(View.GONE);
                holder.iv_advert_single_image.setVisibility(View.VISIBLE);

            } else if (advertData.getImages().size() == 2) {

                ImageLoadUtils.imageLoad(activity,
                        holder.iv_advert_one,
                        advertData.getImages().get(0).getImage_name(),
                        R.drawable.post_img_ph);

                ImageLoadUtils.imageLoad(activity,
                        holder.iv_advert_two,
                        advertData.getImages().get(1).getImage_name(),
                        R.drawable.post_img_ph);

                holder.ll_multiple_image.setVisibility(View.VISIBLE);
                holder.iv_advert_single_image.setVisibility(View.GONE);
                holder.fl_image.setVisibility(View.GONE);
                holder.iv_advert_three.setVisibility(View.GONE);

            } else if (advertData.getImages().size() == 3) {

                ImageLoadUtils.imageLoad(activity,
                        holder.iv_advert_one,
                        advertData.getImages().get(0).getImage_name(),
                        R.drawable.post_img_ph);

                ImageLoadUtils.imageLoad(activity,
                        holder.iv_advert_two,
                        advertData.getImages().get(1).getImage_name(),
                        R.drawable.post_img_ph);

                ImageLoadUtils.imageLoad(activity,
                        holder.iv_advert_three,
                        advertData.getImages().get(2).getImage_name(),
                        R.drawable.post_img_ph);

                holder.ll_multiple_image.setVisibility(View.VISIBLE);
                holder.iv_advert_single_image.setVisibility(View.GONE);
                holder.img_article_shadow.setVisibility(View.GONE);
                holder.txt_image_count.setVisibility(View.GONE);

            } else if (advertData.getImages().size() > 3) {

                ImageLoadUtils.imageLoad(activity,
                        holder.iv_advert_one,
                        advertData.getImages().get(0).getImage_name(),
                        R.drawable.post_img_ph);

                ImageLoadUtils.imageLoad(activity,
                        holder.iv_advert_two,
                        advertData.getImages().get(1).getImage_name(),
                        R.drawable.post_img_ph);

                ImageLoadUtils.imageLoad(activity,
                        holder.iv_advert_three,
                        advertData.getImages().get(2).getImage_name(),
                        R.drawable.post_img_ph);

                holder.ll_multiple_image.setVisibility(View.VISIBLE);
                holder.iv_advert_single_image.setVisibility(View.GONE);
                holder.img_article_shadow.setVisibility(View.VISIBLE);
                holder.txt_image_count.setVisibility(View.VISIBLE);
                holder.txt_image_count.setText(String.format(Locale.getDefault(), "+%d", advertData.getImages().size() - 2));


            }
        }

    }

    private void callAPILikeUnlike(final String status, VoiceVideoViewHolder holder) {
        Log.d("Call like unlike", "clicked like button");

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("post_id", voiceVideoList.get(holder.getAdapterPosition()).getPostId());
        request.put("like_unlike", status);

        ApiCall.getInstance().likeUnlike(activity, request, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {
                String response = (String) data;


            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {
                /*DialogUtils.openDialog(activity, errorMessage, activity.getString(R.string.ok), "", new AlertDialogInterface() {
                    @Override
                    public void onNegativeBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }

                    @Override
                    public void onPositiveBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }
                });*/
            }
        }, false);


        if (status.equalsIgnoreCase("0")) {
            voiceVideoList.get(holder.getAdapterPosition()).setIsPostLiked("false");
            int count = Integer.parseInt(voiceVideoList.get(holder.getAdapterPosition()).getPostLikeCount()) - 1;
            voiceVideoList.get(holder.getAdapterPosition()).setPostLikeCount("" + count);
        } else {
            voiceVideoList.get(holder.getAdapterPosition()).setIsPostLiked("true");
            int count = Integer.parseInt(voiceVideoList.get(holder.getAdapterPosition()).getPostLikeCount()) + 1;
            voiceVideoList.get(holder.getAdapterPosition()).setPostLikeCount("" + count);
        }
        this.notifyDataSetChanged();
    }

    private void increaseDrawableSizes(VoiceVideoViewHolder holder) {
        holder.txt_like.setCompoundDrawables(getDrawableAndResize(R.drawable.selector_like_post), null, null, null);
        holder.txt_comment.setCompoundDrawables(getDrawableAndResize(R.drawable.discuss), null, null, null);
        holder.txt_share.setCompoundDrawables(getDrawableAndResize(R.drawable.share_blue), null, null, null);
        holder.txt_comments_count.setCompoundDrawables(getDrawableAndResize(R.drawable.comments), null, null, null);
    }

    private Drawable getDrawableAndResize(int drawableId) {
        Drawable drawable = ContextCompat.getDrawable(activity, drawableId);
        drawable.setBounds(0,
                0,
                (int) (drawable.getIntrinsicWidth() * 1.4),
                (int) (drawable.getIntrinsicHeight() * 1.4));

        return drawable;
    }

    private void onStreamAudio(String url, final VoiceVideoViewHolder holder) {
        mPlayer = new MediaPlayer();
        mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        // Listen for if the audio file can't be prepared
        mPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                // ... react appropriately ...
                // The MediaPlayer has moved to the Error state, must be reset!
                return false;
            }
        });
        mPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mPlayer.start();

            }
        });

        try {
            mPlayer.setDataSource(url);
            mPlayer.prepareAsync();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return voiceVideoList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (voiceVideoList.get(position).getPostType().equalsIgnoreCase("3")) {
            return 0;
        } else {
            return 1;
        }
    }
}

