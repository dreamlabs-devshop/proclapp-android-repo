package com.proclapp.home.adapter;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.home.voice_video.VideoPlayActivity;
import com.proclapp.interfaces.RecyclerClickListener;
import com.proclapp.model.AllPostsModel;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.AlertDialogInterface;
import com.proclapp.utils.DateTimeUtils;
import com.proclapp.utils.DialogUtils;
import com.proclapp.utils.Logger;
import com.proclapp.utils.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 */

public class VoiceRecordingListAdapter extends RecyclerView.Adapter<VoiceRecordingListAdapter.MyViewHolder> /*implements Filterable*/ {

    private Activity activity;
    private ArrayList<AllPostsModel> list;
    private RecyclerClickListener clickListener;
    private boolean isVoice;
    private boolean isSelected = false;
    private int oldPos = -1;

    public VoiceRecordingListAdapter(Activity activity, ArrayList<AllPostsModel> list, boolean isVoice, RecyclerClickListener clickListener) {
        this.activity = activity;
        this.list = list;
        this.isVoice = isVoice;
        this.clickListener = clickListener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_voice_recording, parent, false);
        return new MyViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        final AllPostsModel audioVideoData = list.get(position);

        if (isVoice) {

            if (StringUtils.isNotEmpty(audioVideoData.getAudioPost().getAudioName()))
                holder.txt_recording.setText(audioVideoData.getAudioPost().getAudioName());

            if (StringUtils.isNotEmpty(audioVideoData.getAudioPost().getDate()))
                holder.txt_recording_time.setText(DateTimeUtils.changeDateTimeFormat("yyyy-MM-dd HH:mm:ss", "HH:mm',' MMM d yyyy", audioVideoData.getAudioPost().getDate()));


        } else {

            if (StringUtils.isNotEmpty(audioVideoData.getVideosPost().getVideoName()))
                holder.txt_recording.setText(audioVideoData.getVideosPost().getVideoName());

            if (StringUtils.isNotEmpty(audioVideoData.getVideosPost().getDate()))
                holder.txt_recording_time.setText(DateTimeUtils.changeDateTimeFormat("yyyy-MM-dd HH:mm:ss", "HH:mm',' MMM d yyyy", audioVideoData.getVideosPost().getDate()));

        }


        if (audioVideoData.isRequestedToExpert()) {
            uiUpdateForSelcted(holder);
        } else {
            uiUpdateForNotSelcted(holder);
        }


        holder.txt_send_request.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (holder.txt_send_request.getText().toString().equalsIgnoreCase(activity.getResources().getString(R.string.delete))) {

                    DialogUtils.openDialog(activity, isVoice ? activity.getString(R.string.are_you_sure_you_want_to_delete_this_audio) : activity.getString(R.string.are_you_sure_you_want_to_delete_this_video), activity.getString(R.string.delete), activity.getString(R.string.cancel), new AlertDialogInterface() {
                        @Override
                        public void onNegativeBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }

                        @Override
                        public void onPositiveBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                            callAPIDeletePost(holder, audioVideoData.getPostId());
                        }
                    });
                } else {
                    clickListener.onItemClick(holder.getAdapterPosition(), "send_request");
                }

            }
        });


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (isVoice) {
                    activity.startActivity(new Intent(activity, VideoPlayActivity.class)
                            .putExtra("audiovideourl", list.get(holder.getAdapterPosition()).getAudioPost().getAudioUrl()));
                } else {
                    activity.startActivity(new Intent(activity, VideoPlayActivity.class)
                            .putExtra("audiovideourl", list.get(holder.getAdapterPosition()).getVideosPost().getVideoUrl()));
                }


            }
        });

    }

    private void uiUpdateForSelcted(MyViewHolder holder) {

        holder.ll_main.setBackgroundColor(ContextCompat.getColor(activity, R.color.color_e4e4e4));
        holder.img_music.setImageResource(R.drawable.delete_recording);
        holder.txt_send_request.setText(activity.getResources().getString(R.string.delete));
        holder.txt_send_request.setBackgroundResource(R.drawable.delete_btn_btg);
        holder.txt_send_request.setTextColor(ContextCompat.getColor(activity, R.color.color_d90707));
        isSelected = true;
    }

    private void uiUpdateForNotSelcted(MyViewHolder holder) {
        holder.ll_main.setBackgroundColor(ContextCompat.getColor(activity, R.color.color_f3f3f3));
        holder.img_music.setImageResource(R.drawable.recording_file);
        holder.txt_send_request.setText(activity.getResources().getString(R.string.send_request));
        holder.txt_send_request.setBackgroundResource(R.drawable.send_btn_bg);
        holder.txt_send_request.setTextColor(ContextCompat.getColor(activity, R.color.white));
        isSelected = false;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    private void callAPIDeletePost(final MyViewHolder holder, String postId) {

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("post_id", postId);

        Logger.d("request:" + request);

        ApiCall.getInstance().deletePost(activity, request, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {

                list.remove(holder.getAdapterPosition());

                AppClass.snackBarView.snackBarShow(activity, isVoice ? activity.getString(R.string.audio_deleted_successfully) : activity.getString(R.string.video_deleted_successfully));

                notifyDataSetChanged();


            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {


                DialogUtils.openDialog(activity, errorMessage, activity.getString(R.string.ok), "", new AlertDialogInterface() {
                    @Override
                    public void onNegativeBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }

                    @Override
                    public void onPositiveBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }
                });
            }


        }, true);

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView
                txt_recording,
                txt_recording_time,
                txt_send_request;

        ImageView img_music;

        LinearLayout ll_main;

        public MyViewHolder(View itemView) {
            super(itemView);
            txt_recording = itemView.findViewById(R.id.txt_recording);
            txt_recording_time = itemView.findViewById(R.id.txt_recording_time);
            txt_send_request = itemView.findViewById(R.id.txt_send_request);
            ll_main = itemView.findViewById(R.id.ll_main);
            img_music = itemView.findViewById(R.id.img_music);

            txt_recording.setTypeface(AppClass.lato_medium);
            txt_recording_time.setTypeface(AppClass.lato_medium);
            txt_send_request.setTypeface(AppClass.lato_medium);


        }

    }

}
