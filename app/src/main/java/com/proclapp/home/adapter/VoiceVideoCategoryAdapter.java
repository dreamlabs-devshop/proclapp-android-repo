package com.proclapp.home.adapter;

import android.app.Activity;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.model.CategoryModel;
import com.proclapp.utils.ImageLoadUtils;

import java.util.ArrayList;

public class VoiceVideoCategoryAdapter extends RecyclerView.Adapter<VoiceVideoCategoryAdapter.VoiceVideoCategoryHolder> implements Filterable {

    Activity activity;
    ArrayList<CategoryModel> list;
    ArrayList<CategoryModel> categoryListFiltered;
    CategorySelection categorySelection;
    DisplayMetrics metrics;
    ArrayList<String> selectedCategories;

    private int oldPos = -1;

    public VoiceVideoCategoryAdapter(Activity activity, ArrayList<CategoryModel> list, CategorySelection selection) {
        this.activity = activity;
        this.list = list;
        categoryListFiltered = list;
        this.categorySelection = selection;

        metrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);

    }


    @NonNull
    @Override
    public VoiceVideoCategoryHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View rootView = LayoutInflater.from(activity).inflate(R.layout.row_signup_step3_category, viewGroup, false);
        return new VoiceVideoCategoryHolder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull final VoiceVideoCategoryHolder holder, int i) {

        holder.txt_category_name.setText(categoryListFiltered.get(holder.getAdapterPosition()).getCategory_name());


        if (categoryListFiltered.get(holder.getAdapterPosition()).isCategorySelected()) {

            ImageLoadUtils.imageLoad(activity,
                    holder.img_category,
                    categoryListFiltered.get(holder.getAdapterPosition()).getCategory_selected_img());

        } else {

            ImageLoadUtils.imageLoad(activity,
                    holder.img_category,
                    categoryListFiltered.get(holder.getAdapterPosition()).getCategory_img());

        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (oldPos >= 0) {
                    categoryListFiltered.get(oldPos).setCategorySelected(false);
                }

                categorySelection.onCategorySelection(categoryListFiltered.get(holder.getAdapterPosition()).getCategory_name(),
                        categoryListFiltered.get(holder.getAdapterPosition()).getCategory_id());


                categoryListFiltered.get(holder.getAdapterPosition()).setCategorySelected(true);


                oldPos = holder.getAdapterPosition();

                notifyDataSetChanged();


            }
        });


    }

    @Override
    public int getItemCount() {
        return categoryListFiltered.size();
    }

    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                Log.d("getFilter", "adapter" + charSequence);

                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    categoryListFiltered = list;
                } else {
                    ArrayList<CategoryModel> filteredList = new ArrayList<>();
                    for (CategoryModel row : list) {

                        if (row.getCategory_name().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    categoryListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = categoryListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                categoryListFiltered = (ArrayList<CategoryModel>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public interface CategorySelection {
        void onCategorySelection(String categoryName, String categoryId);
    }


    public class VoiceVideoCategoryHolder extends RecyclerView.ViewHolder {

        TextView txt_category_name;
        ImageView img_category;

        public VoiceVideoCategoryHolder(@NonNull View itemView) {
            super(itemView);

            txt_category_name = itemView.findViewById(R.id.txt_category_name);
            img_category = itemView.findViewById(R.id.img_category);

            txt_category_name.setTypeface(AppClass.lato_regular);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams((metrics.widthPixels * 83) / 480, (metrics.heightPixels * 83) / 800, Gravity.CENTER);
            img_category.setLayoutParams(params);

        }
    }
}
