package com.proclapp.home.adapter.viewholder;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.advert.AdvertDetailActivity;
import com.proclapp.advert.PostAdvSummaryActivity;
import com.proclapp.home.AnswerEditDeleteDialog;
import com.proclapp.model.AllPostsModel;
import com.proclapp.profile.OtherUserProfileActivity;
import com.proclapp.profile.ProfileActivity;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.AlertDialogInterface;
import com.proclapp.utils.DialogUtils;
import com.proclapp.utils.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;

public class AdvertViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public ImageView
            img_user_profile,
            img_option,
            img_article,
            img_article_shadow;
    public TextView
            txt_artical_category,
            txt_user_name,
            txt_user_place,
            txt_date,
            txt_comments_count,
            txt_like_count,
            txt_title,
            txt_description,
            txt_image_count;
    public LinearLayout
            ll_option,
            ll_name,
            ll_multiple_image;
    public FrameLayout fl_image;
    public AppCompatImageView
            iv_advert_single_image,
            iv_advert_one,
            iv_advert_two,
            iv_advert_three;
    private Activity activity;
    private ArrayList<AllPostsModel> allPostsLists;
    private RecyclerView.Adapter allPostsAdapter;

    public AdvertViewHolder(@NonNull View itemView,
                            Activity activity,
                            ArrayList<AllPostsModel> allPostsLists,
                            RecyclerView.Adapter allPostsAdapter) {
        super(itemView);
        this.activity = activity;
        this.allPostsAdapter = allPostsAdapter;
        this.allPostsLists = allPostsLists;

        initViews();
        setTypeFace();
        setClickListener();

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.img_user_profile:
            case R.id.ll_name:

                if (AppClass.preferences.getUserId().equalsIgnoreCase(allPostsLists.get(getAdapterPosition()).getAuthorId())) {
                    activity.startActivity(new Intent(activity, ProfileActivity.class));

                } else {
                    if (allPostsLists.get(getAdapterPosition()).getIs_user().equalsIgnoreCase("1")) {

                        activity.startActivity(new Intent(activity, OtherUserProfileActivity.class)
                                .putExtra("user_id", allPostsLists.get(getAdapterPosition()).getAuthorId()));
                    }
                }

                break;

            case R.id.ll_option:
                if (AppClass.preferences.getUserId().equalsIgnoreCase(allPostsLists.get(getAdapterPosition()).getAuthorId())) {

                    new AnswerEditDeleteDialog(activity, allPostsLists.get(getAdapterPosition()), new AnswerEditDeleteDialog.OnItemClick() {
                        @Override
                        public void onClick(String tag) {

                            if (tag.equalsIgnoreCase("delete")) {
                                DialogUtils.openDialog(activity, activity.getString(R.string.are_you_sure_you_want_to_delete_this_advert), activity.getString(R.string.delete), activity.getString(R.string.cancel), new AlertDialogInterface() {
                                    @Override
                                    public void onNegativeBtnClicked(DialogInterface alrt) {
                                        alrt.dismiss();
                                    }

                                    @Override
                                    public void onPositiveBtnClicked(DialogInterface alrt) {
                                        alrt.dismiss();
                                        callAPIDeletePost();
                                    }
                                });
                            } else {
                                activity.startActivity(new Intent(activity, PostAdvSummaryActivity.class)
                                        .putExtra("advertdata", allPostsLists.get(getAdapterPosition())));
                            }
                        }
                    }).show();

                }
                break;

            case R.id.txt_like_count:
                callAPILikeUnlike(txt_like_count.isSelected() ? "0" : "1");
                break;

        }

    }

    private void initViews() {

        txt_artical_category = itemView.findViewById(R.id.txt_artical_category);
        txt_user_name = itemView.findViewById(R.id.txt_user_name);
        txt_user_place = itemView.findViewById(R.id.txt_user_place);
        txt_date = itemView.findViewById(R.id.txt_date);
        txt_comments_count = itemView.findViewById(R.id.txt_comments_count);
        txt_like_count = itemView.findViewById(R.id.txt_like_count);

        txt_image_count = itemView.findViewById(R.id.txt_image_count);
        txt_title = itemView.findViewById(R.id.txt_title);
        txt_description = itemView.findViewById(R.id.txt_description);
        ll_option = itemView.findViewById(R.id.ll_option);

        ll_name = itemView.findViewById(R.id.ll_name);
        ll_multiple_image = itemView.findViewById(R.id.ll_multiple_image);

        img_user_profile = itemView.findViewById(R.id.img_user_profile);
        img_option = itemView.findViewById(R.id.img_option);
        img_article = itemView.findViewById(R.id.img_article);
        img_article_shadow = itemView.findViewById(R.id.img_article_shadow);
        iv_advert_single_image = itemView.findViewById(R.id.iv_advert_single_image);
        iv_advert_one = itemView.findViewById(R.id.iv_advert_one);
        iv_advert_two = itemView.findViewById(R.id.iv_advert_two);
        iv_advert_three = itemView.findViewById(R.id.iv_advert_three);

        fl_image = itemView.findViewById(R.id.fl_image);


    }

    private void setTypeFace() {

        txt_artical_category.setTypeface(AppClass.lato_regular);
        txt_user_name.setTypeface(AppClass.lato_bold);
        txt_user_place.setTypeface(AppClass.lato_regular);
        txt_date.setTypeface(AppClass.lato_regular);
        txt_title.setTypeface(AppClass.lato_bold);
        txt_description.setTypeface(AppClass.lato_regular);
        txt_comments_count.setTypeface(AppClass.lato_regular);
        txt_like_count.setTypeface(AppClass.lato_regular);


    }

    private void setClickListener() {
        ll_option.setOnClickListener(this);
        txt_like_count.setOnClickListener(this);
        img_user_profile.setOnClickListener(this);
        ll_name.setOnClickListener(this);

        itemView.setOnClickListener(view -> {
            if (StringUtils.isNotEmpty(allPostsLists.get(getAdapterPosition()).getAdvert_url())) {
                String url = allPostsLists.get(getAdapterPosition()).getAdvert_url().contains("http") ?
                        allPostsLists.get(getAdapterPosition()).getAdvert_url() : "https://" + allPostsLists.get(getAdapterPosition()).getAdvert_url();
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                activity.startActivity(browserIntent);

            } else {

                activity.startActivity(new Intent(activity, AdvertDetailActivity.class)
                        .putExtra("advertDetails", allPostsLists.get(getAdapterPosition())));
            }
        });

    }

    private void callAPILikeUnlike(String status) {


        if (status.equalsIgnoreCase("0")) {
            allPostsLists.get(getAdapterPosition()).setIsPostLiked("false");
            int count = Integer.parseInt(allPostsLists.get(getAdapterPosition()).getPostLikeCount()) - 1;
            allPostsLists.get(getAdapterPosition()).setPostLikeCount("" + count);
            allPostsAdapter.notifyDataSetChanged();
        } else {
            allPostsLists.get(getAdapterPosition()).setIsPostLiked("true");
            int count = Integer.parseInt(allPostsLists.get(getAdapterPosition()).getPostLikeCount()) + 1;
            allPostsLists.get(getAdapterPosition()).setPostLikeCount("" + count);
            allPostsAdapter.notifyDataSetChanged();
        }

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("post_id", allPostsLists.get(getAdapterPosition()).getPostId());
        request.put("like_unlike", status);

        ApiCall.getInstance().likeUnlike(activity, request, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {

            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {
                /*DialogUtils.openDialog(activity, errorMessage, activity.getString(R.string.ok), "", new AlertDialogInterface() {
                    @Override
                    public void onNegativeBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }

                    @Override
                    public void onPositiveBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }
                });*/
            }
        }, false);

    }

    private void callAPIDeletePost() {

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("post_id", allPostsLists.get(getAdapterPosition()).getPostId());

        ApiCall.getInstance().deletePost(activity,
                request, new IApiCallback() {
                    @Override
                    public void onSuccess(Object data) {
                        allPostsLists.remove(getAdapterPosition());
                        allPostsAdapter.notifyDataSetChanged();

                    }

                    @Override
                    public void onFailure(boolean isFalseFromService, String errorMessage) {


                        DialogUtils.openDialog(activity, errorMessage, activity.getString(R.string.ok), "", new AlertDialogInterface() {
                            @Override
                            public void onNegativeBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }

                            @Override
                            public void onPositiveBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }
                        });
                    }


                }, true);

    }

}
