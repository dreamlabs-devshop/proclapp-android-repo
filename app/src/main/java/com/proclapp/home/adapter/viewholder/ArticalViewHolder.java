package com.proclapp.home.adapter.viewholder;

import android.content.DialogInterface;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.home.DiscussActivity;
import com.proclapp.home.FlashActivity;
import com.proclapp.home.HomePageOptionsDialog;
import com.proclapp.home.HomePageOptionsForReaderDialog;
import com.proclapp.home.PostDiscussionActivity;
import com.proclapp.home.adapter.AllPostsAdapter;
import com.proclapp.home.adapter.ArticleImagesAdapter;
import com.proclapp.interfaces.RecyclerClickListener;
import com.proclapp.model.AllPostsModel;
import com.proclapp.profile.OtherUserProfileActivity;
import com.proclapp.profile.ProfileActivity;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.signup.StartupActivity;
import com.proclapp.utils.AlertDialogInterface;
import com.proclapp.utils.DialogUtils;
import com.proclapp.utils.ImageLoadUtils;
import com.proclapp.utils.Logger;
import com.proclapp.utils.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;

public class ArticalViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView
            txt_artical_category,
            txt_user_name,
            txt_user_place,
            txt_date,
            txt_comments_count,
            txt_like_count,
            txt_discuss,
            txt_decline,
            txt_carryover,
            txt_title,
            txt_description,
            txt_flash;
    //txt_image_count;
    public ImageView
            img_user_profile,
            img_follow,
            img_option,
    //img_article,
    iv_single_image;
    //img_article_shadow;
    public LinearLayout
            ll_option,
            ll_name,
            ll_discuss,
            ll_decline,
            ll_carryover,
            ll_flash;
    //public FrameLayout fl_image;
    private AppCompatActivity activity;
    private RecyclerClickListener clickListener;
    private AllPostsAdapter allPostsAdapter;
    private ArrayList<AllPostsModel> allPostsLists;
    private String type;
    private DisplayMetrics metrics;
    public RecyclerView
            rv_images,
            rv_all_images;


    public ArticalViewHolder(@NonNull View itemView,
                             AppCompatActivity activity,
                             ArrayList<AllPostsModel> allPostsLists,
                             AllPostsAdapter allPostsAdapter) {
        super(itemView);
        this.activity = activity;
        this.allPostsAdapter = allPostsAdapter;
        this.allPostsLists = allPostsLists;

        metrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);

        initViews(itemView);
        increaseDrawableSizes();
        setTypeFace();
        setClickListener();

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArticalViewHolder.this.activity.startActivity(new Intent(ArticalViewHolder.this.activity, DiscussActivity.class)
                        .putExtra("articleDetails", ArticalViewHolder.this.allPostsLists.get(getAdapterPosition())));
            }
        });

    }

    @Override
    public void onClick(View view) {

        if (!AppClass.preferences.getUserId().isEmpty()) {
            switch (view.getId()) {

                case R.id.ll_name:
                case R.id.img_user_profile:
                    if (getAdapterPosition() >= 0)
                        if (AppClass.preferences.getUserId().equalsIgnoreCase(allPostsLists.get(getAdapterPosition()).getAuthorId())) {
                            activity.startActivity(new Intent(activity, ProfileActivity.class));

                        } else {
                            if (allPostsLists.get(getAdapterPosition()).getIs_user().equalsIgnoreCase("1")) {

                                activity.startActivity(new Intent(activity, OtherUserProfileActivity.class)
                                        .putExtra("user_id", allPostsLists.get(getAdapterPosition()).getAuthorId()));
                            }
                        }
                    break;

                case R.id.ll_option:
                    if (getAdapterPosition() >= 0)
                        if (AppClass.preferences.getUserId().equalsIgnoreCase(allPostsLists.get(getAdapterPosition()).getAuthorId())) {

                            new HomePageOptionsDialog(activity
                                    , allPostsLists.get(getAdapterPosition())
                                    , allPostsLists.get(getAdapterPosition()).getPostId()
                                    , new HomePageOptionsDialog.UpdateListData() {
                                @Override
                                public void updateData(String postId) {
                                    allPostsLists.remove(getAdapterPosition());
                                    allPostsAdapter.notifyDataSetChanged();
                                }
                            }).show();

                        } else {
                            new HomePageOptionsForReaderDialog(activity,
                                    allPostsLists.get(getAdapterPosition()),
                                    allPostsLists.get(getAdapterPosition()).getPostId()).show();
                        }
                    break;

                case R.id.ll_decline:
                    if (getAdapterPosition() >= 0)
                        callAPIDeclinePost("1");

                    break;

                case R.id.txt_like_count:
                    if (getAdapterPosition() >= 0)
                        callAPILikeUnlike(txt_like_count.isSelected() ? "0" : "1");
                    break;

                case R.id.img_follow:
                    if (getAdapterPosition() >= 0)
                        callAPIChangeFollowUnfollowStatus(img_follow.isSelected() ? "0" : "1", allPostsLists.get(getAdapterPosition()).getAuthorId(), getAdapterPosition());
                    break;


                case R.id.ll_flash:
                    if (getAdapterPosition() >= 0)
                        activity.startActivity(new Intent(activity, FlashActivity.class)
                                .putExtra("post_id", allPostsLists.get(getAdapterPosition()).getPostId()));

                    break;

                /*case R.id.fl_image:

                    activity.startActivity(new Intent(activity, ArticleImagesActivity.class)
                            .putExtra("images", allPostsLists.get(getAdapterPosition()).getImages()));

                    break;*/

                case R.id.ll_discuss:
                    if (getAdapterPosition() >= 0)
                        activity.startActivity(new Intent(activity, PostDiscussionActivity.class)
                                .putExtra("postId", allPostsLists.get(getAdapterPosition()).getPostId()));
                    break;

                case R.id.txt_description:
                    if (getAdapterPosition() >= 0)
                        activity.startActivity(new Intent(activity, DiscussActivity.class)
                                .putExtra("articleDetails", allPostsLists.get(getAdapterPosition())));
                    break;

            }
        } else {
            activity.finishAffinity();
            activity.startActivity(new Intent(activity, StartupActivity.class));
        }
    }

    private void increaseDrawableSizes() {
        txt_like_count.setCompoundDrawables(getDrawableAndResize(R.drawable.selector_like_post), null, null, null);
        txt_comments_count.setCompoundDrawables(getDrawableAndResize(R.drawable.comments), null, null, null);
        txt_discuss.setCompoundDrawables(getDrawableAndResize(R.drawable.discuss), null, null, null);
        txt_decline.setCompoundDrawables(getDrawableAndResize(R.drawable.decline), null, null, null);
        txt_carryover.setCompoundDrawables(getDrawableAndResize(R.drawable.carryover), null, null, null);
        txt_flash.setCompoundDrawables(getDrawableAndResize(R.drawable.flash), null, null, null);
    }

    private Drawable getDrawableAndResize(int drawableId) {
        Drawable drawable = ContextCompat.getDrawable(activity, drawableId);
        drawable.setBounds(0,
                0,
                (int) (drawable.getIntrinsicWidth() * 1.4),
                (int) (drawable.getIntrinsicHeight() * 1.4));

        return drawable;
    }


    private void initViews(View itemView) {

        txt_artical_category = itemView.findViewById(R.id.txt_artical_category);
        txt_user_name = itemView.findViewById(R.id.txt_user_name);
        txt_user_place = itemView.findViewById(R.id.txt_user_place);
        txt_date = itemView.findViewById(R.id.txt_date);
        txt_comments_count = itemView.findViewById(R.id.txt_comments_count);
        txt_like_count = itemView.findViewById(R.id.txt_like_count);
        txt_discuss = itemView.findViewById(R.id.txt_discuss);
        txt_decline = itemView.findViewById(R.id.txt_decline);
        txt_carryover = itemView.findViewById(R.id.txt_carryover);
        txt_flash = itemView.findViewById(R.id.txt_flash);
//        txt_image_count = itemView.findViewById(R.id.txt_image_count);
        txt_title = itemView.findViewById(R.id.txt_title);
        txt_description = itemView.findViewById(R.id.txt_description);
        ll_option = itemView.findViewById(R.id.ll_option);
        ll_flash = itemView.findViewById(R.id.ll_flash);
        ll_name = itemView.findViewById(R.id.ll_name);
        ll_decline = itemView.findViewById(R.id.ll_decline);
//        fl_image = itemView.findViewById(R.id.fl_image);
        ll_discuss = itemView.findViewById(R.id.ll_discuss);
        img_user_profile = itemView.findViewById(R.id.img_user_profile);
        img_follow = itemView.findViewById(R.id.img_follow);
        img_option = itemView.findViewById(R.id.img_option);
//        img_article = itemView.findViewById(R.id.img_article);
//        img_article_shadow = itemView.findViewById(R.id.img_article_shadow);
        ll_carryover = itemView.findViewById(R.id.ll_carryover);
        iv_single_image = itemView.findViewById(R.id.iv_single_image);
        rv_all_images = itemView.findViewById(R.id.rv_all_images);
        rv_images = itemView.findViewById(R.id.rv_images);

        GridLayoutManager layoutManager = new GridLayoutManager(activity, 3);
        rv_all_images.setLayoutManager(layoutManager);

        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams((metrics.widthPixels * 124) / 480
                , (metrics.heightPixels * 124) / 800);
//        img_article.setLayoutParams(params);
//        img_article.setScaleType(ImageView.ScaleType.CENTER_CROP);
//        img_article_shadow.setLayoutParams(params);
    }

    private void setTypeFace() {

        txt_artical_category.setTypeface(AppClass.lato_regular);
        txt_user_name.setTypeface(AppClass.lato_bold);
        txt_user_place.setTypeface(AppClass.lato_regular);
        txt_date.setTypeface(AppClass.lato_regular);
        txt_title.setTypeface(AppClass.lato_bold);
        txt_description.setTypeface(AppClass.lato_regular);
        txt_comments_count.setTypeface(AppClass.lato_regular);
        txt_like_count.setTypeface(AppClass.lato_regular);
        txt_discuss.setTypeface(AppClass.lato_regular);
        txt_decline.setTypeface(AppClass.lato_regular);
        txt_carryover.setTypeface(AppClass.lato_regular);
        txt_flash.setTypeface(AppClass.lato_regular);

    }

    private void setClickListener() {
        ll_option.setOnClickListener(this);
        ll_flash.setOnClickListener(this);
        txt_like_count.setOnClickListener(this);
        img_user_profile.setOnClickListener(this);
        img_follow.setOnClickListener(this);
        ll_name.setOnClickListener(this);
        ll_decline.setOnClickListener(this);
        ll_discuss.setOnClickListener(this);
        txt_description.setOnClickListener(this);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.startActivity(new Intent(activity, DiscussActivity.class)
                        .putExtra("articleDetails", allPostsLists.get(getAdapterPosition())));
            }
        });

    }

    private void callAPILikeUnlike(final String status) {

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("post_id", allPostsLists.get(getAdapterPosition()).getPostId());
        request.put("like_unlike", status);

        ApiCall.getInstance().likeUnlike(activity, request, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {
                String response = (String) data;


            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {
                /*DialogUtils.openDialog(activity, errorMessage, activity.getString(R.string.ok), "", new AlertDialogInterface() {
                    @Override
                    public void onNegativeBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }

                    @Override
                    public void onPositiveBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }
                });*/
            }
        }, false);


        if (status.equalsIgnoreCase("0")) {
            allPostsLists.get(getAdapterPosition()).setIsPostLiked("false");
            int count = Integer.parseInt(allPostsLists.get(getAdapterPosition()).getPostLikeCount()) - 1;
            allPostsLists.get(getAdapterPosition()).setPostLikeCount("" + count);
            allPostsAdapter.notifyDataSetChanged();
        } else {
            allPostsLists.get(getAdapterPosition()).setIsPostLiked("true");
            int count = Integer.parseInt(allPostsLists.get(getAdapterPosition()).getPostLikeCount()) + 1;
            allPostsLists.get(getAdapterPosition()).setPostLikeCount("" + count);
            allPostsAdapter.notifyDataSetChanged();
        }
    }

    private void callAPIfriendUnfriend(final String type) {


        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("friend_id", allPostsLists.get(getAdapterPosition()).getAuthorId());
        request.put("type", type);

        ApiCall.getInstance().friendUnfriend(activity, request, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {

                String response = (String) data;

                if (type.equalsIgnoreCase("1")) {
                    img_follow.setSelected(true);
                } else {
                    img_follow.setSelected(false);
                }

                AppClass.snackBarView.snackBarShow(activity, response);

            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {
                if (StringUtils.isNotEmpty(errorMessage)) {
                    DialogUtils.openDialog(activity, errorMessage, activity.getString(R.string.ok), "", new AlertDialogInterface() {
                        @Override
                        public void onNegativeBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }

                        @Override
                        public void onPositiveBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }
                    });
                }

            }
        }, true);

    }

    private void callAPIDeclinePost(String status) {

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("post_id", allPostsLists.get(getAdapterPosition()).getPostId());
        request.put("status", status);

        Logger.d("request:" + request);

        ApiCall.getInstance().declinePost(activity,
                request, new IApiCallback() {
                    @Override
                    public void onSuccess(Object data) {

                        allPostsLists.remove(getAdapterPosition());
                        allPostsAdapter.notifyDataSetChanged();

                        /*DialogUtils.openDialog(activity, data.toString(), activity.getString(R.string.ok), "", new AlertDialogInterface() {
                            @Override
                            public void onNegativeBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }

                            @Override
                            public void onPositiveBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();

                            }
 getNo_of_sessions                       });*/
                    }

                    @Override
                    public void onFailure(boolean isFalseFromService, String errorMessage) {


                        DialogUtils.openDialog(activity, errorMessage, activity.getString(R.string.ok), "", new AlertDialogInterface() {
                            @Override
                            public void onNegativeBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }

                            @Override
                            public void onPositiveBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }
                        });
                    }


                }, true);

    }

    private void callAPIChangeFollowUnfollowStatus(final String type, String otherUserId, final int position) {

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("follower_id", otherUserId);
        request.put("type", type);

        ApiCall.getInstance().changeFollowUnfollowStatus(activity, request, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {
                String response = (String) data;

                if (type.equalsIgnoreCase("0")) {
                    // txt_follow.setText(R.string.follow);
                    allPostsLists.get(position).setIs_following("false");
                } else {
                    //txt_follow.setText(R.string.unfollow);
                    allPostsLists.get(position).setIs_following("true");
                }

                allPostsAdapter.notifyDataSetChanged();

            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {

            }
        }, true);


    }


}
