package com.proclapp.home.adapter.viewholder;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.DownloadListener;
import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.home.HomePageOptionsDialog;
import com.proclapp.home.HomePageOptionsForReaderDialog;
import com.proclapp.home.PdfReader;
import com.proclapp.home.adapter.AllPostsAdapter;
import com.proclapp.home.question.QuestionDetailsActivity;
import com.proclapp.interfaces.RecyclerClickListener;
import com.proclapp.model.AllPostsModel;
import com.proclapp.profile.OtherUserProfileActivity;
import com.proclapp.profile.ProfileActivity;
import com.proclapp.recommend.RecommendActivity;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.signup.StartupActivity;
import com.proclapp.utils.AlertDialogInterface;
import com.proclapp.utils.DialogUtils;
import com.proclapp.utils.StringUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

public class ResearchPaperViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView
            txt_type_category,
            txt_user_name,
            txt_user_place,
            txt_date,
            txt_description,
            txt_doc_name,
            txt_doc_size,
            txt_comments_count,
            txt_share_count,
            txt_like_count,
            txt_download,
            txt_recommend,
            txt_bookmark;
    public ImageView
            img_user_profile,
            img_follow,
            img_option,
            img_doc;
    private LinearLayout
            ll_document,
            ll_name,
            ll_doc,
            ll_decline,
            ll_option;
    private AppCompatActivity activity;
    private RecyclerClickListener clickListener;
    private ArrayList<AllPostsModel> allPostsLists;
    private AllPostsAdapter allPostsAdapter;
    private String type;

    public ResearchPaperViewHolder(@NonNull View itemView, AppCompatActivity activity,
                                   RecyclerClickListener clickListener,
                                   ArrayList<AllPostsModel> allPostsLists,
                                   AllPostsAdapter allPostsAdapter) {
        super(itemView);

        this.activity = activity;
        this.clickListener = clickListener;
        this.allPostsLists = allPostsLists;
        this.allPostsAdapter = allPostsAdapter;


        initViews();
        increaseDrawableSizes();
        setTypeFace();
        setClickListener();


        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ResearchPaperViewHolder.this.activity.startActivity(new Intent(ResearchPaperViewHolder.this.activity, QuestionDetailsActivity.class)
                        .putExtra("openfrom", "rp")
                        .putExtra("questionDetails", ResearchPaperViewHolder.this.allPostsLists.get(getAdapterPosition())));
            }
        });

    }

    private void increaseDrawableSizes() {
        txt_like_count.setCompoundDrawables(getDrawableAndResize(R.drawable.selector_like_post), null, null, null);
        txt_comments_count.setCompoundDrawables(getDrawableAndResize(R.drawable.comments), null, null, null);
        txt_download.setCompoundDrawables(getDrawableAndResize(R.drawable.download), null, null, null);
        txt_recommend.setCompoundDrawables(getDrawableAndResize(R.drawable.recommand), null, null, null);
        txt_bookmark.setCompoundDrawables(getDrawableAndResize(R.drawable.selector_bookmark_post), null, null, null);
    }

    private Drawable getDrawableAndResize(int drawableId) {
        Drawable drawable = ContextCompat.getDrawable(activity, drawableId);
        drawable.setBounds(0,
                0,
                (int) (drawable.getIntrinsicWidth() * 1.4),
                (int) (drawable.getIntrinsicHeight() * 1.4));

        return drawable;
    }


    private void initViews() {

        txt_type_category = itemView.findViewById(R.id.txt_type_category);
        txt_user_name = itemView.findViewById(R.id.txt_user_name);
        txt_user_place = itemView.findViewById(R.id.txt_user_place);
        txt_date = itemView.findViewById(R.id.txt_date);

        txt_description = itemView.findViewById(R.id.txt_description);
        txt_doc_name = itemView.findViewById(R.id.txt_doc_name);
        txt_doc_size = itemView.findViewById(R.id.txt_doc_size);
        txt_comments_count = itemView.findViewById(R.id.txt_comments_count);
        txt_share_count = itemView.findViewById(R.id.txt_share_count);
        txt_like_count = itemView.findViewById(R.id.txt_like_count);
        txt_download = itemView.findViewById(R.id.txt_download);
        txt_recommend = itemView.findViewById(R.id.txt_recommend);
        txt_bookmark = itemView.findViewById(R.id.txt_bookmark);

        ll_document = itemView.findViewById(R.id.ll_document);
        ll_option = itemView.findViewById(R.id.ll_option);
        ll_name = itemView.findViewById(R.id.ll_name);
        ll_doc = itemView.findViewById(R.id.ll_doc);
        ll_decline = itemView.findViewById(R.id.ll_decline);

        img_user_profile = itemView.findViewById(R.id.img_user_profile);
        img_follow = itemView.findViewById(R.id.img_follow);
        img_option = itemView.findViewById(R.id.img_option);
        img_doc = itemView.findViewById(R.id.img_doc);

    }


    private void setClickListener() {
        txt_bookmark.setOnClickListener(this);
        txt_like_count.setOnClickListener(this);
        txt_download.setOnClickListener(this);
        ll_option.setOnClickListener(this);
        ll_name.setOnClickListener(this);
        ll_doc.setOnClickListener(this);
        img_user_profile.setOnClickListener(this);
        img_follow.setOnClickListener(this);
        ll_decline.setOnClickListener(this);
    }

    private void setTypeFace() {

        txt_type_category.setTypeface(AppClass.lato_regular);
        txt_user_name.setTypeface(AppClass.lato_bold);
        txt_user_place.setTypeface(AppClass.lato_regular);
        txt_date.setTypeface(AppClass.lato_regular);
        txt_description.setTypeface(AppClass.lato_regular);
        txt_doc_name.setTypeface(AppClass.lato_heavy);
        txt_doc_size.setTypeface(AppClass.lato_regular);
        txt_comments_count.setTypeface(AppClass.lato_regular);
        txt_share_count.setTypeface(AppClass.lato_regular);
        txt_like_count.setTypeface(AppClass.lato_regular);
        txt_download.setTypeface(AppClass.lato_regular);
        txt_recommend.setTypeface(AppClass.lato_regular);
        txt_bookmark.setTypeface(AppClass.lato_regular);

    }

    @Override
    public void onClick(View view) {


        if (!AppClass.preferences.getUserId().isEmpty()) {

            switch (view.getId()) {

                case R.id.txt_bookmark:
                    callAPIBookmarkPost(txt_bookmark.isSelected() ? "0" : "1");
                    break;

                case R.id.txt_like_count:
                    callAPILikeUnlike(txt_like_count.isSelected() ? "0" : "1");
                    break;


                case R.id.txt_download:
                    downloadFile(allPostsLists.get(getAdapterPosition()).getFiles().getFileName(), true);
                    break;

                case R.id.ll_doc:
                    /*if (!AppClass.preferences.getUserId().equalsIgnoreCase(allPostsLists.get(getAdapterPosition()).getAuthorId()) &&
                            !allPostsLists.get(getAdapterPosition()).isSubscribe()) {

                        new AlertDialog(activity, activity.getString(R.string.app_name), activity.getString(R.string.not_subscribed), activity.getString(R.string.subscribe), activity.getString(R.string.cancel), new AlertDialog.AlertInterface() {
                            @Override
                            public void onNegativeBtnClicked(Dialog dialog) {
                                dialog.dismiss();
                            }

                            @Override
                            public void onPositiveBtnClicked(Dialog dialog) {
                                dialog.dismiss();
                                activity.startActivity(new Intent(activity, SubscribeUserWebViewActivity.class)
                                        .putExtra("post_id", allPostsLists.get(getAdapterPosition()).getPostId()));


                            }
                        }).show();

                    } else {*/
                    downloadFile(allPostsLists.get(getAdapterPosition()).getFiles().getFileName(), false);

//                    }
                    break;

                case R.id.ll_name:
                case R.id.img_user_profile:
                    if (AppClass.preferences.getUserId().equalsIgnoreCase(allPostsLists.get(getAdapterPosition()).getAuthorId())) {
                        activity.startActivity(new Intent(activity, ProfileActivity.class));

                    } else {

                        if (allPostsLists.get(getAdapterPosition()).getIs_user().equalsIgnoreCase("1")) {

                            activity.startActivity(new Intent(activity, OtherUserProfileActivity.class)
                                    .putExtra("user_id", allPostsLists.get(getAdapterPosition()).getAuthorId()));
                        }
                    }
                    break;

                case R.id.ll_option:

                    if (AppClass.preferences.getUserId().equalsIgnoreCase(allPostsLists.get(getAdapterPosition()).getAuthorId())) {

                        new HomePageOptionsDialog(activity
                                , allPostsLists.get(getAdapterPosition())
                                , allPostsLists.get(getAdapterPosition()).getPostId()
                                , new HomePageOptionsDialog.UpdateListData() {
                            @Override
                            public void updateData(String postId) {
                                allPostsLists.remove(getAdapterPosition());
                                allPostsAdapter.notifyDataSetChanged();
                            }
                        }).show();

                    } else {
                        new HomePageOptionsForReaderDialog(activity,
                                allPostsLists.get(getAdapterPosition()),
                                allPostsLists.get(getAdapterPosition()).getPostId()).show();
                    }
                    break;

                case R.id.img_follow:
                    callAPIChangeFollowUnfollowStatus(img_follow.isSelected() ? "0" : "1", allPostsLists.get(getAdapterPosition()).getAuthorId(), getAdapterPosition());
                    break;

                case R.id.ll_decline:
                    activity.startActivity(new Intent(activity, RecommendActivity.class)
                            .putExtra("post_id", allPostsLists.get(getAdapterPosition()).getPostId()));
                    break;

            }
        } else {
            activity.finishAffinity();
            activity.startActivity(new Intent(activity, StartupActivity.class));
        }
    }


    private void callAPIBookmarkPost(final String status) {

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("post_id", allPostsLists.get(getAdapterPosition()).getPostId());
        request.put("bookmark_unbookmark", status);

        ApiCall.getInstance().bookmarkUnbookmark(activity, request, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {
                String response = (String) data;

                if (status.equalsIgnoreCase("0")) {
                    //txt_bookmark.setText("Bookmark");
                    txt_bookmark.setSelected(false);
                } else {
                    //txt_bookmark.setText("Bookmarked");
                    txt_bookmark.setSelected(true);
                }


            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {
                DialogUtils.openDialog(activity, errorMessage, activity.getString(R.string.ok), "", new AlertDialogInterface() {
                    @Override
                    public void onNegativeBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }

                    @Override
                    public void onPositiveBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }
                });
            }
        }, true);


    }

    private void callAPILikeUnlike(String status) {


        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("post_id", allPostsLists.get(getAdapterPosition()).getPostId());
        request.put("like_unlike", status);

        ApiCall.getInstance().likeUnlike(activity, request, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {
                String response = (String) data;


            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {
                DialogUtils.openDialog(activity, errorMessage, activity.getString(R.string.ok), "", new AlertDialogInterface() {
                    @Override
                    public void onNegativeBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }

                    @Override
                    public void onPositiveBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }
                });
            }
        }, false);

        if (status.equalsIgnoreCase("0")) {
            allPostsLists.get(getAdapterPosition()).setIsPostLiked("false");
            int count = Integer.parseInt(allPostsLists.get(getAdapterPosition()).getPostLikeCount()) - 1;
            allPostsLists.get(getAdapterPosition()).setPostLikeCount("" + count);
            allPostsAdapter.notifyDataSetChanged();
        } else {
            allPostsLists.get(getAdapterPosition()).setIsPostLiked("true");
            int count = Integer.parseInt(allPostsLists.get(getAdapterPosition()).getPostLikeCount()) + 1;
            allPostsLists.get(getAdapterPosition()).setPostLikeCount("" + count);
            allPostsAdapter.notifyDataSetChanged();
        }


    }

    private void downloadFile(String fileUrl, boolean downloadOnly) {

        final String dirPath = Environment.getExternalStorageDirectory().toString() + "/" + activity.getString(R.string.app_name) + "/";
        final String fileName = "" + allPostsLists.get(getAdapterPosition()).getFiles().getOriFileName();

        final ProgressDialog progressDialog = new ProgressDialog(activity);
        progressDialog.setMessage(downloadOnly ? "Downloading" : "Opening");
        progressDialog.setCancelable(true);
        progressDialog.setCanceledOnTouchOutside(false);

        progressDialog.show();

        AndroidNetworking
                .download(fileUrl, dirPath, fileName)
                .build()
                .startDownload(new DownloadListener() {
                    @Override
                    public void onDownloadComplete() {
                        AppClass.snackBarView.snackBarShow(activity, "Download Completed");
                        progressDialog.dismiss();

                        try {
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            File file = new File(dirPath + fileName);
                            MimeTypeMap map = MimeTypeMap.getSingleton();
                            String ext = MimeTypeMap.getFileExtensionFromUrl(file.getName());
                            String type = map.getMimeTypeFromExtension(ext);

                            if (type == null)
                                type = "*/*";
                            Uri data = Uri.fromFile(file);
                            intent.setDataAndType(data, type);

                            if (downloadOnly) {
                                activity.startActivity(intent);

                            } else
                                activity.startActivity(new Intent(activity, PdfReader.class).putExtra("file", data.toString()));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        AppClass.snackBarView.snackBarShow(activity, "Error while downloading");
                        progressDialog.dismiss();
                    }
                });

    }

    private void callAPIfriendUnfriend(final String type) {


        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("friend_id", allPostsLists.get(getAdapterPosition()).getAuthorId());
        request.put("type", type);

        ApiCall.getInstance().friendUnfriend(activity, request, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {

                String response = (String) data;

                if (type.equalsIgnoreCase("1")) {
                    img_follow.setSelected(true);
                } else {
                    img_follow.setSelected(false);
                }

                AppClass.snackBarView.snackBarShow(activity, response);

            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {
                if (StringUtils.isNotEmpty(errorMessage)) {
                    DialogUtils.openDialog(activity, errorMessage, activity.getString(R.string.ok), "", new AlertDialogInterface() {
                        @Override
                        public void onNegativeBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }

                        @Override
                        public void onPositiveBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }
                    });
                }

            }
        }, true);

    }


    private void callAPIDownloadPost() {

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("post_id", allPostsLists.get(getAdapterPosition()).getPostId());

        ApiCall.getInstance().downloadPost(activity, request, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {
                String response = (String) data;

            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {
                DialogUtils.openDialog(activity, errorMessage, activity.getString(R.string.ok), "", new AlertDialogInterface() {
                    @Override
                    public void onNegativeBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }

                    @Override
                    public void onPositiveBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }
                });
            }
        }, true);


    }

    private void callAPIChangeFollowUnfollowStatus(final String type, String otherUserId, final int position) {

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("follower_id", otherUserId);
        request.put("type", type);

        ApiCall.getInstance().changeFollowUnfollowStatus(activity, request, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {
                String response = (String) data;

                if (type.equalsIgnoreCase("0")) {
                    // txt_follow.setText(R.string.follow);
                    allPostsLists.get(position).setIs_following("false");
                } else {
                    //txt_follow.setText(R.string.unfollow);
                    allPostsLists.get(position).setIs_following("true");
                }

                allPostsAdapter.notifyDataSetChanged();

            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {

            }
        }, true);


    }

}
