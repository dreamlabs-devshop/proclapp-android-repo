package com.proclapp.home.adapter.viewholder;

import android.app.Activity;
import android.content.DialogInterface;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.model.AllPostsModel;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.AlertDialogInterface;
import com.proclapp.utils.DialogUtils;
import com.proclapp.utils.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;

public class SuggetionTrendingViewHolder extends RecyclerView.ViewHolder {


    public RecyclerView rv_suggetions_trending;
    public TextView tv_tag;
    private Activity activity;
    private ArrayList<AllPostsModel> allPostsLists;
    private RecyclerView.Adapter allPostsAdapter;

    public SuggetionTrendingViewHolder(@NonNull View itemView, Activity activity,
                                       ArrayList<AllPostsModel> allPostsLists,
                                       RecyclerView.Adapter allPostsAdapter) {
        super(itemView);

        this.activity = activity;
        this.allPostsLists = allPostsLists;
        this.allPostsAdapter = allPostsAdapter;

        rv_suggetions_trending = itemView.findViewById(R.id.rv_suggetions_trending);
        tv_tag = itemView.findViewById(R.id.tv_tag);



    }


    private void updateCategoryToUserProfile(String category) {

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("category", category);

        ApiCall.getInstance().updateCategoryToUserProfile(activity, request, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {
                String responce = (String) data;
                AppClass.snackBarView.snackBarShow(activity, responce);


            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {
                if (StringUtils.isNotEmpty(errorMessage)) {
                    DialogUtils.openDialog(activity, errorMessage, activity.getString(R.string.ok), "", new AlertDialogInterface() {
                        @Override
                        public void onNegativeBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }

                        @Override
                        public void onPositiveBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }
                    });
                }
            }
        }, false);


    }


}
