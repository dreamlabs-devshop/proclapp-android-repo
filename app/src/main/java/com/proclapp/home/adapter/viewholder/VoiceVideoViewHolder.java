package com.proclapp.home.adapter.viewholder;

import android.app.Activity;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.dialog.ExpertAnswerDialog;
import com.proclapp.home.HomePageOptionsDialog;
import com.proclapp.home.HomePageOptionsForReaderDialog;
import com.proclapp.home.adapter.AllPostsAdapter;
import com.proclapp.home.adapter.VoiceAndVideoAdapter;
import com.proclapp.home.voice_video.VVExpertReplyActivity;
import com.proclapp.model.AllPostsModel;
import com.proclapp.profile.OtherUserProfileActivity;
import com.proclapp.profile.ProfileActivity;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;

import java.util.ArrayList;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class VoiceVideoViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public RelativeLayout rl_video;
    public LinearLayout ll_voice,
            ll_name,
            ll_option, ll_discuss;
    public ImageView video_view,
            img_video,
            img_option,
            iv_play,
            iv_pause;
    public CircleImageView img_user_profile;
    public TextView txt_user_name,
            txt_user_place,
            txt_date,
            txt_title_value,
            txt_time,
            txt_record_time,
            tv_give_answer,
            txt_like,
            txt_comment,
            txt_share,
            txt_description,
            txt_comments_count,
            tv_see_answer;


    //VoiceAndVideoAdapter voiceAndVideoAdapter;
    AllPostsAdapter allPostsAdapter;
    Activity activity;
    private ArrayList<AllPostsModel> voiceVideoList;


    public VoiceVideoViewHolder(@NonNull View itemView, Activity activity, AllPostsAdapter allPostsAdapter, ArrayList<AllPostsModel> voiceVideoList) {
        super(itemView);

        this.activity = activity;
        //this.voiceAndVideoAdapter = voiceAndVideoAdapter;
        this.allPostsAdapter = allPostsAdapter;
        this.voiceVideoList = voiceVideoList;

        ll_voice = itemView.findViewById(R.id.ll_voice);
        ll_name = itemView.findViewById(R.id.ll_name);
        ll_option = itemView.findViewById(R.id.ll_option);
        rl_video = itemView.findViewById(R.id.rl_video);
        ll_discuss = itemView.findViewById(R.id.ll_discuss);


        video_view = itemView.findViewById(R.id.video_view);
        img_video = itemView.findViewById(R.id.img_video);
        img_user_profile = itemView.findViewById(R.id.img_user_profile);
        img_option = itemView.findViewById(R.id.img_option);
        iv_play = itemView.findViewById(R.id.iv_play);
        iv_pause = itemView.findViewById(R.id.iv_pause);
        txt_user_name = itemView.findViewById(R.id.txt_user_name);
        txt_user_place = itemView.findViewById(R.id.txt_user_place);
        txt_date = itemView.findViewById(R.id.txt_date);
        txt_title_value = itemView.findViewById(R.id.txt_title_value);
        txt_time = itemView.findViewById(R.id.txt_time);
        txt_record_time = itemView.findViewById(R.id.txt_record_time);
        tv_see_answer = itemView.findViewById(R.id.tv_see_answer);
        tv_give_answer = itemView.findViewById(R.id.tv_give_answer);
        txt_description = itemView.findViewById(R.id.txt_description);

        txt_like = itemView.findViewById(R.id.txt_like);
        txt_comment = itemView.findViewById(R.id.txt_comment);
        txt_share = itemView.findViewById(R.id.txt_share);
        txt_comments_count = itemView.findViewById(R.id.txt_comments_count);

            /*RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams((metrics.widthPixels * 448) / 480, (metrics.heightPixels * 140) / 800);
            video_view.setLayoutParams(layoutParams);
            video_view.setLayoutParams(layoutParams);*/
        txt_comment.setTypeface(AppClass.lato_regular);
        txt_like.setTypeface(AppClass.lato_regular);
        txt_comments_count.setTypeface(AppClass.lato_regular);

        txt_user_name.setTypeface(AppClass.lato_regular);
        txt_user_place.setTypeface(AppClass.lato_regular);
        txt_date.setTypeface(AppClass.lato_regular);
        txt_title_value.setTypeface(AppClass.lato_regular);
        txt_time.setTypeface(AppClass.lato_medium);
        txt_record_time.setTypeface(AppClass.lato_medium);
        tv_give_answer.setTypeface(AppClass.lato_medium);
        tv_see_answer.setTypeface(AppClass.lato_regular);
        ll_option.setOnClickListener(this);
        img_user_profile.setOnClickListener(this);
        txt_user_name.setOnClickListener(this);
        tv_see_answer.setOnClickListener(this);
        tv_give_answer.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.ll_option:

                if (AppClass.preferences.getUserId().equalsIgnoreCase(voiceVideoList.get(getAdapterPosition()).getAuthorId())) {

                    new HomePageOptionsDialog(activity
                            , voiceVideoList.get(getAdapterPosition())
                            , voiceVideoList.get(getAdapterPosition()).getPostId()
                            , new HomePageOptionsDialog.UpdateListData() {
                        @Override
                        public void updateData(String postId) {
                            voiceVideoList.remove(getAdapterPosition());
                            allPostsAdapter.notifyDataSetChanged();
                        }
                    }).show();

                } else {
                    new HomePageOptionsForReaderDialog(activity,
                            voiceVideoList.get(getAdapterPosition()),
                            voiceVideoList.get(getAdapterPosition()).getPostId()).show();
                }
                break;

            case R.id.txt_user_name:
            case R.id.img_user_profile:
                if (AppClass.preferences.getUserId().equalsIgnoreCase(voiceVideoList.get(getAdapterPosition()).getAuthorId())) {
                    activity.startActivity(new Intent(activity, ProfileActivity.class));

                } else {

                    if (voiceVideoList.get(getAdapterPosition()).getIs_user().equalsIgnoreCase("1")) {
                        activity.startActivity(new Intent(activity, OtherUserProfileActivity.class)
                                .putExtra("user_id", voiceVideoList.get(getAdapterPosition()).getAuthorId()));
                    }
                }
                break;

            case R.id.tv_see_answer:
                activity.startActivity(new Intent(activity, VVExpertReplyActivity.class)
                        .putExtra("vvdata", voiceVideoList.get(getAdapterPosition())));
                break;

            case R.id.tv_give_answer:
                new ExpertAnswerDialog(activity, voiceVideoList.get(getAdapterPosition())).show();
                break;
        }
    }

}
