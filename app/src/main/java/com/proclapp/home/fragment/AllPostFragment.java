package com.proclapp.home.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.expert.ExpertModel;
import com.proclapp.home.HomeActivity;
import com.proclapp.home.adapter.AllPostsAdapter;
import com.proclapp.model.AllPostsModel;
import com.proclapp.model.CategoryModel;
import com.proclapp.model.FollowerFollowingModel;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.AlertDialogInterface;
import com.proclapp.utils.DialogUtils;
import com.proclapp.utils.StaticData;
import com.proclapp.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class AllPostFragment extends Fragment implements SwipyRefreshLayout.OnRefreshListener {


    LinearLayoutManager layoutManager;
    private SwipyRefreshLayout sr_all_post;
    private RecyclerView rv_all_post;
    private TextView tv_no_data;
    private HomeActivity mHomeActivity;
    private ArrayList<AllPostsModel> allPostsLists = new ArrayList<>();
    private AllPostsAdapter allPostsAdapter;
    private int offset = 0;
    private boolean isServiceCalling = false, isLastPage = false;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_all_post, container, false);
        initViews(view);

        setUpRecyclerView();

        return view;
    }

    private void initViews(View view) {

        mHomeActivity = (HomeActivity) getActivity();

        sr_all_post = view.findViewById(R.id.sr_all_post);
        rv_all_post = view.findViewById(R.id.rv_all_post);
        tv_no_data = view.findViewById(R.id.tv_no_data);

        sr_all_post.setOnRefreshListener(this);
        sr_all_post.setColorSchemeColors(getResources().getColor(R.color.color_0c6984), getResources().getColor(R.color.black));

    }

    @Override
    public void onRefresh(final SwipyRefreshLayoutDirection direction) {
        offset = direction == SwipyRefreshLayoutDirection.TOP ? 0 : offset + 20;
        callAPIGetPosts();
    }


    private void setUpRecyclerView() {

        layoutManager = new LinearLayoutManager(mHomeActivity);
        rv_all_post.setLayoutManager(layoutManager);
        allPostsAdapter = new AllPostsAdapter(mHomeActivity, allPostsLists, (pos, tag) -> {

        });
        rv_all_post.setAdapter(allPostsAdapter);


        sr_all_post.post(new Runnable() {
            @Override
            public void run() {
                sr_all_post.setRefreshing(true);
                onRefresh(SwipyRefreshLayoutDirection.TOP);
            }
        });


        rv_all_post.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                int visibleItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int firstVisibleItemPosition = layoutManager.findLastVisibleItemPosition();

                if (!isServiceCalling && !isLastPage) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0) {
                        sr_all_post.post(new Runnable() {
                            @Override
                            public void run() {
                                sr_all_post.setRefreshing(true);
                                onRefresh(SwipyRefreshLayoutDirection.BOTTOM);
                            }
                        });

                    }
                }
            }
        });


    }


    @Override
    public void onResume() {
        super.onResume();
        if (StaticData.isEdited) {
            sr_all_post.post(new Runnable() {
                @Override
                public void run() {
                    sr_all_post.setRefreshing(true);
                    onRefresh(SwipyRefreshLayoutDirection.TOP);
                }
            });
        }
    }

    private void callAPIGetPosts() {

        isServiceCalling = true;
        StaticData.isEdited = false;
        ApiCall.getInstance().getPosts(mHomeActivity, AppClass.preferences.getUserId(), "", String.valueOf(offset), new IApiCallback() {
            @Override
            public void onSuccess(Object data) {
                JsonObject jsonObject = (JsonObject) data;

                ArrayList<AllPostsModel> tempFilteredList = new Gson().fromJson(jsonObject.getAsJsonArray("posts_list").toString(),
                        new TypeToken<List<AllPostsModel>>() {
                        }.getType());

                if (offset == 0)
                    allPostsLists.clear();

                allPostsLists.addAll(tempFilteredList);

                if (offset == 0) {

                    for (int i = 0; i < allPostsLists.size(); i++) {


                        if (StringUtils.isNotEmpty(allPostsLists.get(i).getsFriend()) &&
                                allPostsLists.get(i).getsFriend().equalsIgnoreCase("1")) {
                            ArrayList<FollowerFollowingModel> tempSFriendList = new Gson().fromJson(jsonObject.getAsJsonArray("suggested_friends").toString(),
                                    new TypeToken<List<FollowerFollowingModel>>() {
                                    }.getType());
                            allPostsLists.get(i).setSuggestedFriends(tempSFriendList);

                        } else if (StringUtils.isNotEmpty(allPostsLists.get(i).getsExpert()) &&
                                allPostsLists.get(i).getsExpert().equalsIgnoreCase("1")) {
                            ArrayList<ExpertModel> tempSExpertList = new Gson().fromJson(jsonObject.getAsJsonArray("suggested_experts").toString(),
                                    new TypeToken<List<ExpertModel>>() {
                                    }.getType());

                            allPostsLists.get(i).setSuggestedExperts(tempSExpertList);

                        } else if (StringUtils.isNotEmpty(allPostsLists.get(i).getTrTopics()) &&
                                allPostsLists.get(i).getTrTopics().equalsIgnoreCase("1")) {
                            ArrayList<CategoryModel> tempTrTopicsList = new Gson().fromJson(jsonObject.getAsJsonArray("trending_topics").toString(),
                                    new TypeToken<List<CategoryModel>>() {
                                    }.getType());
                            allPostsLists.get(i).setTrendingTopics(tempTrTopicsList);
                        }
                    }
                }

                sr_all_post.setRefreshing(false);
                isLastPage = false;
                isServiceCalling = false;
                rv_all_post.setVisibility(View.VISIBLE);
                tv_no_data.setVisibility(View.GONE);
                allPostsAdapter.notifyDataSetChanged();


            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {


                sr_all_post.setRefreshing(false);
                isLastPage = true;
                isServiceCalling = false;


                if (errorMessage.equalsIgnoreCase("No posts")) {

                    if (offset == 0) {
                        tv_no_data.setVisibility(View.VISIBLE);
                        rv_all_post.setVisibility(View.GONE);
                    }

                } else {
                    if (mHomeActivity != null) {

                        DialogUtils.openDialog(mHomeActivity, errorMessage, mHomeActivity.getString(R.string.ok), "", new AlertDialogInterface() {
                            @Override
                            public void onNegativeBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }

                            @Override
                            public void onPositiveBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }
                        });
                    }
                }
            }
        }, false);

    }


}
