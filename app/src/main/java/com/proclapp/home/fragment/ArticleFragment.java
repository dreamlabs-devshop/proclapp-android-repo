package com.proclapp.home.fragment;

import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.expert.ExpertModel;
import com.proclapp.home.HomeActivity;
import com.proclapp.home.adapter.AllPostsAdapter;
import com.proclapp.interfaces.RecyclerClickListener;
import com.proclapp.model.AllPostsModel;
import com.proclapp.model.CategoryModel;
import com.proclapp.model.FollowerFollowingModel;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.AlertDialogInterface;
import com.proclapp.utils.DialogUtils;
import com.proclapp.utils.StaticData;
import com.proclapp.utils.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ArticleFragment extends Fragment implements SwipyRefreshLayout.OnRefreshListener {

    private SwipyRefreshLayout sr_artical;
    private RecyclerView rv_artical;
    private TextView tv_no_data;
    private ArrayList<AllPostsModel> articalList = new ArrayList<>();
    private AllPostsAdapter articleAndDiscussionAdapter;
    private HomeActivity mHomeActivity;
    private int offset = 0;
    private boolean isServiceCalling = false, isLastPage = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_article, container, false);
        initViews(view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (StaticData.isEdited) {
            sr_artical.post(new Runnable() {
                @Override
                public void run() {
                    sr_artical.setRefreshing(true);
                    onRefresh(SwipyRefreshLayoutDirection.TOP);
                }
            });
        }
    }

    @Override
    public void onRefresh(SwipyRefreshLayoutDirection direction) {
        offset = direction == SwipyRefreshLayoutDirection.TOP ? 0 : offset + 20;
        callAPIGetArticalList();
    }

    private void initViews(View view) {

        mHomeActivity = (HomeActivity) getActivity();

        sr_artical = view.findViewById(R.id.sr_artical);
        rv_artical = view.findViewById(R.id.rv_artical);
        tv_no_data = view.findViewById(R.id.tv_no_data);
        sr_artical.setColorSchemeColors(getResources().getColor(R.color.color_0c6984), getResources().getColor(R.color.black));

        sr_artical.setOnRefreshListener(this);

        setUpRecyclerView();

    }

    private void setUpRecyclerView() {

        final LinearLayoutManager layoutManager = new LinearLayoutManager(mHomeActivity);
        rv_artical.setLayoutManager(layoutManager);
        articleAndDiscussionAdapter = new AllPostsAdapter(mHomeActivity, articalList, new RecyclerClickListener() {
            @Override
            public void onItemClick(int pos, String tag) {

            }
        });
        rv_artical.setAdapter(articleAndDiscussionAdapter);


        sr_artical.post(new Runnable() {
            @Override
            public void run() {
                sr_artical.setRefreshing(true);
                onRefresh(SwipyRefreshLayoutDirection.TOP);
            }
        });

        rv_artical.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int firstVisibleItemPosition = layoutManager.findLastVisibleItemPosition();

                if (!isServiceCalling && !isLastPage) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0) {
                        sr_artical.post(new Runnable() {
                            @Override
                            public void run() {
                                sr_artical.setRefreshing(true);
                                onRefresh(SwipyRefreshLayoutDirection.BOTTOM);
                            }
                        });

                    }
                }
            }
        });

    }


    private void callAPIGetArticalList() {

        isServiceCalling = true;
        StaticData.isEdited = false;

        mHomeActivity = (HomeActivity) getActivity();

        if (mHomeActivity == null)
            return;

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("type", "1");
        request.put("offset", String.valueOf(offset));

        ApiCall.getInstance().getFilteredList(mHomeActivity, request, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {


                JsonObject jsonObject = (JsonObject) data;

                ArrayList<AllPostsModel> tempFilteredList = new Gson().fromJson(jsonObject.getAsJsonArray("posts").toString(),
                        new TypeToken<List<AllPostsModel>>() {
                        }.getType());


                if (offset == 0)
                    articalList.clear();

                articalList.addAll(tempFilteredList);
                if (offset == 0) {
                    for (int i = 0; i < articalList.size(); i++) {

                        if (StringUtils.isNotEmpty(articalList.get(i).getsFriend()) &&
                                articalList.get(i).getsFriend().equalsIgnoreCase("1")) {
                            ArrayList<FollowerFollowingModel> tempSFriendList = new Gson().fromJson(jsonObject.getAsJsonArray("suggested_friends").toString(),
                                    new TypeToken<List<FollowerFollowingModel>>() {
                                    }.getType());
                            articalList.get(i).setSuggestedFriends(tempSFriendList);

                        } else if (StringUtils.isNotEmpty(articalList.get(i).getsExpert()) &&
                                articalList.get(i).getsExpert().equalsIgnoreCase("1")) {
                            ArrayList<ExpertModel> tempSExpertList = new Gson().fromJson(jsonObject.getAsJsonArray("suggested_experts").toString(),
                                    new TypeToken<List<ExpertModel>>() {
                                    }.getType());

                            articalList.get(i).setSuggestedExperts(tempSExpertList);

                        } else if (StringUtils.isNotEmpty(articalList.get(i).getTrTopics()) &&
                                articalList.get(i).getTrTopics().equalsIgnoreCase("1")) {
                            ArrayList<CategoryModel> tempTrTopicsList = new Gson().fromJson(jsonObject.getAsJsonArray("trending_topics").toString(),
                                    new TypeToken<List<CategoryModel>>() {
                                    }.getType());
                            articalList.get(i).setTrendingTopics(tempTrTopicsList);
                        }

                    }
                }

                sr_artical.setRefreshing(false);
                isLastPage = false;
                isServiceCalling = false;
                tv_no_data.setVisibility(View.GONE);
                rv_artical.setVisibility(View.VISIBLE);
                articleAndDiscussionAdapter.notifyDataSetChanged();
            }


            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {


                sr_artical.setRefreshing(false);
                isLastPage = true;
                isServiceCalling = false;


                if (StringUtils.isNotEmpty(errorMessage)) {

                    if (errorMessage.equalsIgnoreCase("No data found")) {

                        if (offset == 0) {
                            tv_no_data.setVisibility(View.VISIBLE);
                            rv_artical.setVisibility(View.GONE);
                        }


                    } else {

                        if (getActivity() == null)
                            return;

                        DialogUtils.openDialog(getActivity(), errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                            @Override
                            public void onNegativeBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }

                            @Override
                            public void onPositiveBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }
                        });
                    }
                }
            }
        }, false);


    }

}
