package com.proclapp.home.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;
import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.home.HomeActivity;
import com.proclapp.home.NewPostDialog;
import com.proclapp.home.adapter.SectionsPagerAdapter;
import com.proclapp.signup.StartupActivity;
import com.proclapp.utils.StringUtils;
import com.proclapp.utils.Utils;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class HomeFragment extends Fragment implements View.OnClickListener {


    public TextView
            txt_q_a,
            txt_a_d,
            txt_v_v,
            txt_rp;
    private ImageView img_new_post;
    private AppCompatActivity activity;
    private View tab_separator;
//    private ImageView
//            img_menu,
//            img_new_post;
//    private View
//            v_qa,
//            v_ad,
//            v_vv,
//            v_rp,
//            v_menu;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.home_fragment, container, false);
        activity = (HomeActivity) getActivity();
        ///initListener();

        Utils.addFragment(new AllPostFragment(), getChildFragmentManager(), false);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tab_separator = view.findViewById(R.id.tab_separator);
        img_new_post = view.findViewById(R.id.img_new_post);
        img_new_post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (StringUtils.isNotEmpty(AppClass.preferences.getUserId())) {
                    new NewPostDialog(activity).show();
                } else {
                    activity.finishAffinity();
                    startActivity(new Intent(activity, StartupActivity.class));
                }
            }
        });
        initViewPager();
    }

    private void initViewPager() {
        SectionsPagerAdapter adapter = new SectionsPagerAdapter(getChildFragmentManager());

        adapter.addFragment(new MenuFragment());
        adapter.addFragment(new QuestionFragment());
        adapter.addFragment(new ArticleFragment());
        adapter.addFragment(new VideoVoiceFragment());
        adapter.addFragment(new ResearchPaperFragment());
        //ArticleFragment()
        //AllPostFragment()

        //ViewPager viewPager = activity.findViewById(R.id.view_pager);
        //viewPager.setAdapter(adapter);


        TabLayout tabLayout = activity.findViewById(R.id.tabs);
        //tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabsFromPagerAdapter(adapter);
        tabLayout.setTabTextColors(getResources().getColor(R.color.color_2fc0d1), getResources().getColor(android.R.color.white));

        //tabLayout.getTabAt(1).select();


        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TextView tv = (TextView) LayoutInflater.from(activity).inflate(R.layout.tab_text, null);
            ImageView iv = (ImageView) LayoutInflater.from(activity).inflate(R.layout.tab_menu, null);

            if (i == 0) {
                tabLayout.getTabAt(i).setCustomView(iv);
            } else {
                tabLayout.getTabAt(i).setCustomView(tv);
            }

            switch (i) {
                case 1:
                    tv.setText(R.string.q_a);
                    break;
                case 2:
                    tv.setText(R.string.a_d);
                    break;
                case 3:
                    tv.setText(R.string.v_v);
                    break;
                case 4:
                    tv.setText(R.string.rp);
                    break;
            }
        }

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        img_new_post.setVisibility(View.GONE);
                        Utils.addFragment(new MenuFragment(HomeFragment.this), getChildFragmentManager(), false);
                        break;
                    case 1:
                        img_new_post.setVisibility(View.VISIBLE);
                        Utils.addFragment(new QuestionFragment(), getChildFragmentManager(), false);
                        break;
                    case 2:
                        img_new_post.setVisibility(View.VISIBLE);
                        Utils.addFragment(new ArticleFragment(), getChildFragmentManager(), false);
                        break;
                    case 3:
                        img_new_post.setVisibility(View.VISIBLE);
                        Utils.addFragment(new VideoVoiceFragment(), getChildFragmentManager(), false);
                        break;
                    case 4:
                        img_new_post.setVisibility(View.VISIBLE);
                        Utils.addFragment(new ResearchPaperFragment(), getChildFragmentManager(), false);
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

//    private void initView(View view) {
//
//        img_menu = view.findViewById(R.id.img_menu);
//        img_new_post = view.findViewById(R.id.img_new_post);
//
//        txt_q_a = view.findViewById(R.id.txt_q_a);
//        txt_a_d = view.findViewById(R.id.txt_a_d);
//        txt_v_v = view.findViewById(R.id.txt_v_v);
//        txt_rp = view.findViewById(R.id.txt_rp);
//
//        //v_qa = view.findViewById(R.id.v_qa);
//        //v_ad = view.findViewById(R.id.v_ad);
//        //v_vv = view.findViewById(R.id.v_vv);
//        //v_rp = view.findViewById(R.id.v_rp);
//        v_menu = view.findViewById(R.id.v_menu);
//
//
//    }

//    private void initListener() {
//        img_menu.setOnClickListener(this);
//        img_new_post.setOnClickListener(this);
//        txt_q_a.setOnClickListener(this);
//        txt_a_d.setOnClickListener(this);
//        txt_v_v.setOnClickListener(this);
//        txt_rp.setOnClickListener(this);
//
//    }

    @Override
    public void onClick(View view) {

//        switch (view.getId()) {
//
//            case R.id.img_menu:
//
//
//                Utils.addFragment(new MenuFragment(this), getChildFragmentManager(), false);
//
//
//                img_new_post.setVisibility(View.GONE);
//
//                v_qa.setVisibility(View.INVISIBLE);
//                v_ad.setVisibility(View.INVISIBLE);
//                v_vv.setVisibility(View.INVISIBLE);
//                v_rp.setVisibility(View.INVISIBLE);
//                v_menu.setVisibility(View.VISIBLE);
//
//                txt_q_a.setTextColor(ContextCompat.getColor(activity, R.color.color_666666));
//                txt_a_d.setTextColor(ContextCompat.getColor(activity, R.color.color_666666));
//                txt_v_v.setTextColor(ContextCompat.getColor(activity, R.color.color_666666));
//                txt_rp.setTextColor(ContextCompat.getColor(activity, R.color.color_666666));
//
//                img_menu.setImageResource(R.drawable.menu_selected);
//
//                break;
//
//            case R.id.img_new_post:
//                if (StringUtils.isNotEmpty(AppClass.preferences.getUserId())) {
//                    new NewPostDialog(activity).show();
//                } else {
//                    activity.finishAffinity();
//                    startActivity(new Intent(activity, StartupActivity.class));
//                }
//                break;
//
//            case R.id.txt_q_a:
//
//                v_qa.setVisibility(View.VISIBLE);
//                v_ad.setVisibility(View.INVISIBLE);
//                v_vv.setVisibility(View.INVISIBLE);
//                v_rp.setVisibility(View.INVISIBLE);
//                v_menu.setVisibility(View.INVISIBLE);
//
//                img_new_post.setVisibility(View.VISIBLE);
//
//                txt_q_a.setTextColor(ContextCompat.getColor(activity, R.color.color_2fc0d1));
//                txt_a_d.setTextColor(ContextCompat.getColor(activity, R.color.color_666666));
//                txt_v_v.setTextColor(ContextCompat.getColor(activity, R.color.color_666666));
//                txt_rp.setTextColor(ContextCompat.getColor(activity, R.color.color_666666));
//
//                img_menu.setImageResource(R.drawable.menu);
//
//                Utils.addFragment(new QuestionFragment(), getChildFragmentManager(), false);
//
//
//                break;
//
//            case R.id.txt_a_d:
//
//                v_qa.setVisibility(View.INVISIBLE);
//                v_ad.setVisibility(View.VISIBLE);
//                v_vv.setVisibility(View.INVISIBLE);
//                v_rp.setVisibility(View.INVISIBLE);
//                v_menu.setVisibility(View.INVISIBLE);
//                img_new_post.setVisibility(View.VISIBLE);
//
//                txt_q_a.setTextColor(ContextCompat.getColor(activity, R.color.color_666666));
//                txt_a_d.setTextColor(ContextCompat.getColor(activity, R.color.color_2fc0d1));
//                txt_v_v.setTextColor(ContextCompat.getColor(activity, R.color.color_666666));
//                txt_rp.setTextColor(ContextCompat.getColor(activity, R.color.color_666666));
//
//                img_menu.setImageResource(R.drawable.menu);
//
//                Utils.addFragment(new ArticleFragment(), getChildFragmentManager(), false);
//
//                break;
//
//            case R.id.txt_v_v:
//
//                v_qa.setVisibility(View.INVISIBLE);
//                v_ad.setVisibility(View.INVISIBLE);
//                v_vv.setVisibility(View.VISIBLE);
//                v_rp.setVisibility(View.INVISIBLE);
//                v_menu.setVisibility(View.INVISIBLE);
//                img_new_post.setVisibility(View.VISIBLE);
//
//                txt_q_a.setTextColor(ContextCompat.getColor(activity, R.color.color_666666));
//                txt_a_d.setTextColor(ContextCompat.getColor(activity, R.color.color_666666));
//                txt_v_v.setTextColor(ContextCompat.getColor(activity, R.color.color_2fc0d1));
//                txt_rp.setTextColor(ContextCompat.getColor(activity, R.color.color_666666));
//
//                img_menu.setImageResource(R.drawable.menu);
//
//                Utils.addFragment(new VideoVoiceFragment(), getChildFragmentManager(), false);
//
//                break;
//
//            case R.id.txt_rp:
//
//                v_qa.setVisibility(View.INVISIBLE);
//                v_ad.setVisibility(View.INVISIBLE);
//                v_vv.setVisibility(View.INVISIBLE);
//                v_rp.setVisibility(View.VISIBLE);
//                v_menu.setVisibility(View.INVISIBLE);
//                img_new_post.setVisibility(View.VISIBLE);
//
//                txt_q_a.setTextColor(ContextCompat.getColor(activity, R.color.color_666666));
//                txt_a_d.setTextColor(ContextCompat.getColor(activity, R.color.color_666666));
//                txt_v_v.setTextColor(ContextCompat.getColor(activity, R.color.color_666666));
//                txt_rp.setTextColor(ContextCompat.getColor(activity, R.color.color_2fc0d1));
//
//                img_menu.setImageResource(R.drawable.menu);
//
//                Utils.addFragment(new ResearchPaperFragment(), getChildFragmentManager(), false);
//
//                break;
//
//
//        }
    }

}
