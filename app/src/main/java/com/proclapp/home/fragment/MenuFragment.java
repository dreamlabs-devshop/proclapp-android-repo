package com.proclapp.home.fragment;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;
import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.home.HomeActivity;
import com.proclapp.home.UserDiscussedArticleActivity;
import com.proclapp.home.UserQuestionListActivity;
import com.proclapp.home.menu.ResearchPaperActivity;
import com.proclapp.home.menu.TrendingTopicsActivity;
import com.proclapp.profile.ProfileActivity;
import com.proclapp.signup.StartupActivity;
import com.proclapp.utils.AlertDialogInterface;
import com.proclapp.utils.DialogUtils;
import com.proclapp.utils.ImageLoadUtils;
import com.proclapp.utils.LogOutUtils;
import com.proclapp.utils.ServiceLogOut;
import com.proclapp.utils.StringUtils;
import com.proclapp.utils.Utils;

import de.hdodenhof.circleimageview.CircleImageView;

@SuppressLint("ValidFragment")
public class MenuFragment extends Fragment implements View.OnClickListener {


    private TextView
            txt_user_name,
            txt_user_profession,
            txt_question_answer,
            txt_answered,
            txt_unanswered,
            txt_articles_and_discussions,
            txt_discussed,
            txt_non_discussed,
            txt_voice_video_content,
            txt_research_paper,
            txt_experts,
            txt_trending_topic,
    //txt_Adverts,
    tv_no_data;
    private LinearLayout
            ll_question_answer_drop_menu,
            ll_article_discussion_drop_menu,
            ll_voice_video_content,
            ll_research_paper,
            ll_experts,
            ll_trending_topics,
    //ll_adverts,
    ll_profile;
    private RelativeLayout
            rl_menu,
            rl_home;
    private HomeActivity mHomeActivity;
    private CircleImageView img_user_profile;

    private ImageView
            img_online,
            img_logout,
            img_question_answer_dropdown,
            img_article_discussion_dropdown;

    private boolean is_question_answer_expanded = false;
    private boolean is_article_discussion_expanded = false;

    private HomeFragment mHomeFragment;

    public MenuFragment() {
    }

    @SuppressLint("ValidFragment")
    public MenuFragment(HomeFragment mHomeFragment) {
        this.mHomeFragment = mHomeFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_menu, container, false);
        initViews(view);
        initListener();
        initStyle();
        setProfileData();


        return view;
    }


    @Override
    public void onClick(View view) {
        TabLayout tabs = getActivity().findViewById(R.id.tabs);

        switch (view.getId()) {

            case R.id.ll_voice_video_content:

                tabs.getTabAt(3).select();
//                mHomeFragment.onClick(mHomeFragment.txt_v_v);
                break;

            case R.id.ll_research_paper:
                tabs.getTabAt(4).select();
                // startActivity(new Intent(mHomeActivity, ResearchPaperActivity.class));
                //mHomeFragment.onClick(mHomeFragment.txt_rp);
                break;

            case R.id.ll_experts:
                mHomeActivity.tabsWidget.setCurrentTab(2);
                mHomeActivity.mTabHost.setCurrentTab(2);
                break;

            case R.id.ll_trending_topics:
                startActivity(new Intent(mHomeActivity, TrendingTopicsActivity.class));
                break;

//            case R.id.ll_adverts:
//                mHomeActivity.tabsWidget.setCurrentTab(3);
//                mHomeActivity.mTabHost.setCurrentTab(3);
//                break;

            case R.id.ll_profile:
                startActivity(new Intent(mHomeActivity, ProfileActivity.class));
                break;

            case R.id.img_question_answer_dropdown:

                if (is_question_answer_expanded) {
                    is_question_answer_expanded = false;
                    img_question_answer_dropdown.setImageResource(R.drawable.arw_expand);
                    ll_question_answer_drop_menu.setVisibility(View.GONE);
                } else {
                    is_question_answer_expanded = true;
                    img_question_answer_dropdown.setImageResource(R.drawable.arw_collapse);
                    ll_question_answer_drop_menu.setVisibility(View.VISIBLE);
                }

                break;

            case R.id.img_article_discussion_dropdown:
                if (is_article_discussion_expanded) {
                    is_article_discussion_expanded = false;
                    img_article_discussion_dropdown.setImageResource(R.drawable.arw_expand);
                    ll_article_discussion_drop_menu.setVisibility(View.GONE);
                } else {
                    is_article_discussion_expanded = true;
                    img_article_discussion_dropdown.setImageResource(R.drawable.arw_collapse);
                    ll_article_discussion_drop_menu.setVisibility(View.VISIBLE);
                }
                break;

            case R.id.img_logout:
                DialogUtils.openDialog(getActivity(), "Are you sure you want to logout?", "Logout", "Cancel", new AlertDialogInterface() {
                    @Override
                    public void onNegativeBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }

                    @Override
                    public void onPositiveBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                        new ServiceLogOut(mHomeActivity, new ServiceLogOut.ServiceDoneListener() {
                            @Override
                            public void onSuccess(String message) {
                                LogOutUtils.LogoutUser(mHomeActivity);
                                Intent intent = new Intent(mHomeActivity, StartupActivity.class);
                                mHomeActivity.startActivity(intent);

                            }

                            @Override
                            public void onFail(boolean isResponseFalseFromService, String message) {
                                Intent intent = new Intent(mHomeActivity, StartupActivity.class);
                                mHomeActivity.startActivity(intent);
                                LogOutUtils.LogoutUser(mHomeActivity);
                            }
                        });
                    }
                });
                break;

            case R.id.txt_answered:

                startActivity(new Intent(mHomeActivity, UserQuestionListActivity.class)
                        .putExtra("type", "answered"));

                break;

            case R.id.txt_unanswered:

                startActivity(new Intent(mHomeActivity, UserQuestionListActivity.class)
                        .putExtra("type", "unanswered"));

                break;

            case R.id.txt_discussed:

                startActivity(new Intent(mHomeActivity, UserDiscussedArticleActivity.class)
                        .putExtra("type", "discussed"));

                break;

            case R.id.txt_non_discussed:

                startActivity(new Intent(mHomeActivity, UserDiscussedArticleActivity.class)
                        .putExtra("type", "non_discussed"));

                break;

        }
    }

    private void initViews(View view) {

        mHomeActivity = (HomeActivity) getActivity();
        txt_user_name = view.findViewById(R.id.txt_user_name);
        txt_user_profession = view.findViewById(R.id.txt_user_profession);
        txt_question_answer = view.findViewById(R.id.txt_question_answer);
        txt_answered = view.findViewById(R.id.txt_answered);
        txt_unanswered = view.findViewById(R.id.txt_unanswered);
        txt_articles_and_discussions = view.findViewById(R.id.txt_articles_and_discussions);
        txt_discussed = view.findViewById(R.id.txt_discussed);
        txt_non_discussed = view.findViewById(R.id.txt_non_discussed);
        txt_voice_video_content = view.findViewById(R.id.txt_voice_video_content);
        txt_research_paper = view.findViewById(R.id.txt_research_paper);
        txt_experts = view.findViewById(R.id.txt_experts);
        txt_trending_topic = view.findViewById(R.id.txt_trending_topic);
        //txt_Adverts = view.findViewById(R.id.txt_Adverts);
        tv_no_data = view.findViewById(R.id.tv_no_data);

        rl_menu = view.findViewById(R.id.rl_menu);
        rl_home = view.findViewById(R.id.rl_home);

        ll_question_answer_drop_menu = view.findViewById(R.id.ll_question_answer_drop_menu);
        ll_article_discussion_drop_menu = view.findViewById(R.id.ll_article_discussion_drop_menu);
        ll_voice_video_content = view.findViewById(R.id.ll_voice_video_content);
        ll_research_paper = view.findViewById(R.id.ll_research_paper);
        ll_experts = view.findViewById(R.id.ll_experts);
        ll_trending_topics = view.findViewById(R.id.ll_trending_topics);
        //ll_adverts = view.findViewById(R.id.ll_adverts);
        ll_profile = view.findViewById(R.id.ll_profile);

        img_user_profile = view.findViewById(R.id.img_user_profile);

        img_logout = view.findViewById(R.id.img_logout);
        img_user_profile = view.findViewById(R.id.img_user_profile);
        img_online = view.findViewById(R.id.img_online);
        img_question_answer_dropdown = view.findViewById(R.id.img_question_answer_dropdown);
        img_article_discussion_dropdown = view.findViewById(R.id.img_article_discussion_dropdown);

        if (!StringUtils.isNotEmpty(AppClass.preferences.getUserId())) {

            img_logout.setVisibility(View.GONE);

        } else {
            img_logout.setVisibility(View.VISIBLE);
        }

        if (!StringUtils.isNotEmpty(AppClass.preferences.getUserId())) {
            ll_profile.setVisibility(View.GONE);
        } else {
            ll_profile.setVisibility(View.VISIBLE);
        }

    }

    private void initListener() {

        img_logout.setOnClickListener(this);
        img_question_answer_dropdown.setOnClickListener(this);
        img_article_discussion_dropdown.setOnClickListener(this);
        ll_voice_video_content.setOnClickListener(this);
        ll_research_paper.setOnClickListener(this);
        ll_experts.setOnClickListener(this);
        ll_trending_topics.setOnClickListener(this);
        //ll_adverts.setOnClickListener(this);
        ll_profile.setOnClickListener(this);
        txt_answered.setOnClickListener(this);
        txt_unanswered.setOnClickListener(this);
        txt_discussed.setOnClickListener(this);
        txt_non_discussed.setOnClickListener(this);

    }

    private void initStyle() {


        txt_user_name.setTypeface(AppClass.lato_heavy);
        txt_user_profession.setTypeface(AppClass.lato_regular);
        txt_question_answer.setTypeface(AppClass.lato_regular);
        txt_answered.setTypeface(AppClass.lato_light);
        txt_unanswered.setTypeface(AppClass.lato_light);
        txt_articles_and_discussions.setTypeface(AppClass.lato_regular);
        txt_discussed.setTypeface(AppClass.lato_light);
        txt_non_discussed.setTypeface(AppClass.lato_light);
        txt_voice_video_content.setTypeface(AppClass.lato_regular);
        txt_research_paper.setTypeface(AppClass.lato_regular);
        txt_experts.setTypeface(AppClass.lato_regular);
        txt_trending_topic.setTypeface(AppClass.lato_regular);
        //txt_Adverts.setTypeface(AppClass.lato_regular);
    }

    private void setProfileData() {

        if (!AppClass.preferences.getToken().isEmpty()) {

            ImageLoadUtils.imageLoad(mHomeActivity,
                    img_user_profile,
                    AppClass.preferences.getProfileImage(),
                    R.drawable.user_img_ph);

            txt_user_name.setText(AppClass.preferences.getUserFirstName() + " " + AppClass.preferences.getUserLastName());
            txt_user_profession.setText(AppClass.preferences.getCurrentJob());
        }
    }

}
