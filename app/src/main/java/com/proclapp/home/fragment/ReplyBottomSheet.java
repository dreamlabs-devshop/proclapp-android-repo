package com.proclapp.home.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.home.question.AnswerDetailsActivity;
import com.proclapp.model.AnswerModel;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.AlertDialogInterface;
import com.proclapp.utils.DialogUtils;
import com.proclapp.utils.KeyBoardUtils;
import com.proclapp.utils.Logger;
import com.proclapp.utils.StringUtils;

import org.jetbrains.annotations.Nullable;

import java.util.HashMap;

public class ReplyBottomSheet extends BottomSheetDialogFragment {

    private BottomSheetListener mListener;
    private AnswerModel answerLists;
    private EditText textField;


    @SuppressLint("ClickableViewAccessibility")
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.reply_bottom_sheet, container, false);
        textField = v.findViewById(R.id.edt_comment);
        ImageView sendImage = v.findViewById(R.id.img_send);
        String comment_id = getArguments().getString("comment_id");


        sendImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onButtonClicked(textField.getText().toString().trim(),comment_id);
                ReplyBottomSheet.this.dismiss();

            }
        });

        return v;
    }

    public interface BottomSheetListener {
        void onButtonClicked(String text,String id);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mListener = (BottomSheetListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement BottomSheetListener");
        }
    }

}
