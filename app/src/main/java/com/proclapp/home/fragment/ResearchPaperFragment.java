package com.proclapp.home.fragment;

import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.home.HomeActivity;
import com.proclapp.home.adapter.AllPostsAdapter;
import com.proclapp.model.AllPostsModel;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.AlertDialogInterface;
import com.proclapp.utils.DialogUtils;
import com.proclapp.utils.StaticData;
import com.proclapp.utils.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ResearchPaperFragment extends Fragment implements SwipyRefreshLayout.OnRefreshListener {

    private SwipyRefreshLayout sr_research_paper;
    private RecyclerView rv_research_paper;
    private ArrayList<AllPostsModel> researchPaperList = new ArrayList<>();
    private AllPostsAdapter researchPaperAdapter;
    private TextView tv_no_data;
    private HomeActivity mHomeActivity;
    private int offset = 0;
    private boolean isServiceCalling = false, isLastPage = false;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_research_paper, container, false);
        initViews(view);

        setUpRecyclerView();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (StaticData.isEdited) {
            sr_research_paper.post(new Runnable() {
                @Override
                public void run() {
                    sr_research_paper.setRefreshing(true);
                    onRefresh(SwipyRefreshLayoutDirection.TOP);
                }
            });
        }
    }

    @Override
    public void onRefresh(SwipyRefreshLayoutDirection direction) {
        if (direction == SwipyRefreshLayoutDirection.TOP) {
            offset = 0;
            callAPIGetRPList();
        } else {
            offset += 20;
            callAPIGetRPList();
        }
    }

    private void initViews(View view) {

        mHomeActivity = (HomeActivity) getActivity();


        sr_research_paper = view.findViewById(R.id.sr_research_paper);
        rv_research_paper = view.findViewById(R.id.rv_research_paper);
        tv_no_data = view.findViewById(R.id.tv_no_data);
        sr_research_paper.setOnRefreshListener(this);
        sr_research_paper.setColorSchemeColors(getResources().getColor(R.color.color_0c6984), getResources().getColor(R.color.black));


    }

    private void setUpRecyclerView() {

        final LinearLayoutManager layoutManager = new LinearLayoutManager(mHomeActivity);
        rv_research_paper.setLayoutManager(layoutManager);
        researchPaperAdapter = new AllPostsAdapter(mHomeActivity, researchPaperList, (pos, tag) -> {

        });
        rv_research_paper.setAdapter(researchPaperAdapter);

        sr_research_paper.post(new Runnable() {
            @Override
            public void run() {
                sr_research_paper.setRefreshing(true);
                onRefresh(SwipyRefreshLayoutDirection.TOP);
            }
        });

        rv_research_paper.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                int visibleItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int firstVisibleItemPosition = layoutManager.findLastVisibleItemPosition();

                if (!isServiceCalling && !isLastPage) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0) {
                        sr_research_paper.post(new Runnable() {
                            @Override
                            public void run() {
                                sr_research_paper.setRefreshing(true);
                                onRefresh(SwipyRefreshLayoutDirection.BOTTOM);
                            }
                        });

                    }
                }
            }
        });

    }


    private void callAPIGetRPList() {

        if (getActivity() == null)
            return;

        isServiceCalling = true;
        StaticData.isEdited = false;

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("type", "4");
        request.put("offset", String.valueOf(offset));

        ApiCall.getInstance().getFilteredList(getActivity(), request, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {

                JsonObject jsonObject = (JsonObject) data;

                ArrayList<AllPostsModel> tempFilteredList = new Gson().fromJson(jsonObject.getAsJsonArray("posts").toString(),
                        new TypeToken<List<AllPostsModel>>() {
                        }.getType());

                if (offset == 0)
                    researchPaperList.clear();

                researchPaperList.addAll(tempFilteredList);


                tv_no_data.setVisibility(View.GONE);
                rv_research_paper.setVisibility(View.VISIBLE);
                researchPaperAdapter.notifyDataSetChanged();
                sr_research_paper.setRefreshing(false);
                isLastPage = false;
                isServiceCalling = false;


            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {


                sr_research_paper.setRefreshing(false);
                isLastPage = true;
                isServiceCalling = false;


                if (StringUtils.isNotEmpty(errorMessage)) {

                    if (errorMessage.equalsIgnoreCase("No data found")) {
                        if (offset == 0) {
                            tv_no_data.setVisibility(View.VISIBLE);
                            rv_research_paper.setVisibility(View.GONE);
                        }

                    } else {

                        if (getActivity() == null)
                            return;

                        DialogUtils.openDialog(getActivity(), errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                            @Override
                            public void onNegativeBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }

                            @Override
                            public void onPositiveBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }
                        });
                    }
                }
            }
        }, false);


    }

}
