package com.proclapp.home.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.home.HomeActivity;
import com.proclapp.home.SearchActivity;
import com.proclapp.home.adapter.SearchResultAdapter;
import com.proclapp.model.AllPostsModel;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;

import java.util.ArrayList;
import java.util.List;

public class SearchPostFragment extends Fragment {

    private TextView txt_nothing_search, txt_no_data;
    private RecyclerView rv_search_list;
    private ArrayList<AllPostsModel> allPostsLists = new ArrayList<>();
    private AppCompatActivity activity;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        activity = (SearchActivity) getActivity();
        return inflater.inflate(R.layout.search_post_fragment, container, false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        setRecyclerView();
    }

    private void initViews(View view) {
        rv_search_list = view.findViewById(R.id.rv_search_list_post);
        txt_nothing_search = view.findViewById(R.id.txt_nothing_search_post);
        txt_no_data = view.findViewById(R.id.txt_no_data_post);
    }

    private void setRecyclerView() {
        rv_search_list.setLayoutManager(new LinearLayoutManager(getActivity()));
        rv_search_list.setAdapter(new SearchResultAdapter(activity, allPostsLists));
    }

}
