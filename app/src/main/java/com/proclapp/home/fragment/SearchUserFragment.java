package com.proclapp.home.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.proclapp.R;
import com.proclapp.home.SearchActivity;
import com.proclapp.home.adapter.SearchResultAdapter;
import com.proclapp.home.adapter.SearchResultAdapterUser;
import com.proclapp.model.AllPostsModel;
import com.proclapp.model.AnswerModel;

import java.util.ArrayList;

public class SearchUserFragment extends Fragment {

    private TextView txt_nothing_search, txt_no_data;
    private RecyclerView rv_search_list;
    private ArrayList<AnswerModel.SearchedUser> allUsersLists = new ArrayList<>();
    private AppCompatActivity activity;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        activity = (SearchActivity) getActivity();
        return inflater.inflate(R.layout.search_user_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        setRecyclerView();
    }

    private void initViews(View view) {
        rv_search_list = view.findViewById(R.id.rv_search_list_user);
        txt_nothing_search = view.findViewById(R.id.txt_nothing_search_user);
        txt_no_data = view.findViewById(R.id.txt_no_data_user);
    }

    private void setRecyclerView() {
        rv_search_list.setLayoutManager(new LinearLayoutManager(getActivity()));
        rv_search_list.setAdapter(new SearchResultAdapterUser(activity, allUsersLists));
    }
}
