package com.proclapp.home.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.home.HomeActivity;
import com.proclapp.home.adapter.VoiceAndVideoAdapter;
import com.proclapp.model.AllPostsModel;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.AlertDialogInterface;
import com.proclapp.utils.DialogUtils;
import com.proclapp.utils.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class VideoVoiceFragment extends Fragment implements SwipyRefreshLayout.OnRefreshListener {


    private SwipyRefreshLayout sr_video_voice;
    private RecyclerView rv_video_voice;
    private TextView tv_no_data;
    private HomeActivity mHomeActivity;
    private ArrayList<AllPostsModel> voiceVideoList = new ArrayList<>();
    private VoiceAndVideoAdapter voiceAndVideoAdapter;
    private int offset = 0;
    private boolean isServiceCalling = false, isLastPage = false;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_video_voice, container, false);
        initViews(view);

        setUpRecyclerView();

        return view;
    }

    @Override
    public void onRefresh(SwipyRefreshLayoutDirection direction) {
        if (direction == SwipyRefreshLayoutDirection.TOP) {
            offset = 0;
            callAPIGetVoiceVideoList();
        } else {
            offset += 20;
            callAPIGetVoiceVideoList();
        }
    }

    private void initViews(View view) {

        mHomeActivity = (HomeActivity) getActivity();

        sr_video_voice = view.findViewById(R.id.sr_video_voice);
        rv_video_voice = view.findViewById(R.id.rv_video_voice);
        tv_no_data = view.findViewById(R.id.tv_no_data);
        sr_video_voice.setColorSchemeColors(getResources().getColor(R.color.color_0c6984), getResources().getColor(R.color.black));

        sr_video_voice.setOnRefreshListener(this);
    }

    private void setUpRecyclerView() {

        final LinearLayoutManager layoutManager = new LinearLayoutManager(mHomeActivity);
        rv_video_voice.setLayoutManager(layoutManager);
        voiceAndVideoAdapter = new VoiceAndVideoAdapter(mHomeActivity, voiceVideoList);
        rv_video_voice.setAdapter(voiceAndVideoAdapter);

        sr_video_voice.post(new Runnable() {
            @Override
            public void run() {
                sr_video_voice.setRefreshing(true);
                onRefresh(SwipyRefreshLayoutDirection.TOP);
            }
        });

        rv_video_voice.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                int visibleItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int firstVisibleItemPosition = layoutManager.findLastVisibleItemPosition();

                if (!isServiceCalling && !isLastPage) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0) {
                        sr_video_voice.post(new Runnable() {
                            @Override
                            public void run() {
                                sr_video_voice.setRefreshing(true);
                                onRefresh(SwipyRefreshLayoutDirection.BOTTOM);
                            }
                        });

                    }
                }
            }
        });

    }

    private void callAPIGetVoiceVideoList() {

        if (getActivity() == null)
            return;

        isServiceCalling = true;

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("type", "3");
        request.put("offset", String.valueOf(offset));

        ApiCall.getInstance().getFilteredList(getActivity(), request, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {

                JsonObject jsonObject = (JsonObject) data;

                ArrayList<AllPostsModel> tempFilteredList = new Gson().fromJson(jsonObject.getAsJsonArray("posts").toString(),
                        new TypeToken<List<AllPostsModel>>() {
                        }.getType());


                if (offset == 0)
                    voiceVideoList.clear();
                voiceVideoList.addAll(tempFilteredList);


                sr_video_voice.setRefreshing(false);
                isLastPage = false;
                isServiceCalling = false;
                tv_no_data.setVisibility(View.GONE);
                rv_video_voice.setVisibility(View.VISIBLE);
                voiceAndVideoAdapter.notifyDataSetChanged();

            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {


                sr_video_voice.setRefreshing(false);
                isLastPage = true;
                isServiceCalling = false;


                if (StringUtils.isNotEmpty(errorMessage)) {

                    if (errorMessage.equalsIgnoreCase("No data found")) {

                        if (offset == 0) {
                            tv_no_data.setVisibility(View.VISIBLE);
                            rv_video_voice.setVisibility(View.GONE);
                        }
                    } else {

                        if (getActivity() == null)
                            return;

                        DialogUtils.openDialog(getActivity(), errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                            @Override
                            public void onNegativeBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }

                            @Override
                            public void onPositiveBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }
                        });
                    }
                }
            }
        }, false);


    }
}
