package com.proclapp.home.friends;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.proclapp.R;
import com.proclapp.interfaces.RecyclerClickListener;
import com.proclapp.model.FollowerFollowingModel;
import com.proclapp.utils.ImageLoadUtils;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class FriendSuggetionAdapter extends RecyclerView.Adapter<FriendSuggetionAdapter.FriendSuggetionHolder> {

    AppCompatActivity context;
    String fromWhich;
    RecyclerClickListener listener;
    ArrayList<FollowerFollowingModel> friendsList;

    public FriendSuggetionAdapter(AppCompatActivity context, String fromWhich, ArrayList<FollowerFollowingModel> friendsList, RecyclerClickListener listener) {
        this.context = context;
        this.fromWhich = fromWhich;
        this.listener = listener;
        this.friendsList = friendsList;
    }


    @NonNull
    @Override
    public FriendSuggetionHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_friends_suggetions, viewGroup, false);
        return new FriendSuggetionHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final FriendSuggetionHolder holder, int i) {
        FollowerFollowingModel suggetionData = friendsList.get(i);

        holder.tv_friend_name.setText(suggetionData.getName());

        ImageLoadUtils.imageLoad(context,
                holder.iv_friend_proflie,
                suggetionData.getProfileImage(),
                R.drawable.profile_pic_ph);

        holder.iv_friend_proflie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.iv_send_request.isSelected())
                    listener.onItemClick(holder.getAdapterPosition(), "request_cancel");
                else
                    listener.onItemClick(holder.getAdapterPosition(), "request_send");
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(holder.getAdapterPosition(), "itemView");
            }
        });

    }

    @Override
    public int getItemCount() {
        return friendsList.size();
    }

    public class FriendSuggetionHolder extends RecyclerView.ViewHolder {

        private TextView tv_friend_name;
        private CircleImageView iv_friend_proflie;
        private ImageView iv_send_request;

        public FriendSuggetionHolder(@NonNull View itemView) {
            super(itemView);

            tv_friend_name = itemView.findViewById(R.id.tv_friend_name);
            iv_friend_proflie = itemView.findViewById(R.id.iv_friend_proflie);
            iv_send_request = itemView.findViewById(R.id.iv_send_request);

        }
    }
}
