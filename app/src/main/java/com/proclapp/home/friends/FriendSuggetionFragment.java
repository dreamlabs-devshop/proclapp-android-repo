package com.proclapp.home.friends;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.chat.ChatActivity;
import com.proclapp.interfaces.RecyclerClickListener;
import com.proclapp.model.FollowerFollowingModel;
import com.proclapp.profile.OtherUserProfileActivity;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.AlertDialogInterface;
import com.proclapp.utils.DialogUtils;
import com.proclapp.utils.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FriendSuggetionFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    int offset = 0;
    private RecyclerView rv_friend_suggestons_list;
    private FriendsActivity mFriendsActivity;
    private ArrayList<FollowerFollowingModel> friendsList = new ArrayList<>();
    private TextView tv_no_suggetions;

    private SwipeRefreshLayout sr_friend_suggetion_list;
    private String openFrom = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_friend_suggestion, container, false);

        initViews(view);
        setUpRecyclerView();
        rv_friend_suggestons_list = view.findViewById(R.id.rv_friend_suggestons_list);

        sr_friend_suggetion_list.setOnRefreshListener(this);

        return view;
    }

    @Override
    public void onRefresh() {
        offset = 0;
        friendSuggestion();
    }

    private void initViews(View view) {

        mFriendsActivity = (FriendsActivity) getActivity();
        rv_friend_suggestons_list = view.findViewById(R.id.rv_friend_suggestons_list);
        sr_friend_suggetion_list = view.findViewById(R.id.sr_friend_suggetion_list);
        tv_no_suggetions = view.findViewById(R.id.tv_no_suggetions);

        if (getArguments() != null &&
                StringUtils.isNotEmpty(getArguments().getString("openfrom")) && (
                getArguments().getString("openfrom").equalsIgnoreCase("notification")) ||
                getArguments().getString("openfrom").equalsIgnoreCase("chat")) {
            openFrom = getArguments().getString("openfrom");

        }

    }

    private void setUpRecyclerView() {

        LinearLayoutManager layoutManager = new LinearLayoutManager(mFriendsActivity);
        rv_friend_suggestons_list.setLayoutManager(layoutManager);
        rv_friend_suggestons_list.setAdapter(new FriendSuggetionAdapter(mFriendsActivity, "suggestions", friendsList, new RecyclerClickListener() {
            @Override
            public void onItemClick(int pos, String tag) {

                if (tag.equalsIgnoreCase("friends")) {
                    startActivity(new Intent(mFriendsActivity, ChatActivity.class)
                            .putExtra("friend_id", friendsList.get(pos).getUserId())
                            .putExtra("profileimage", friendsList.get(pos).getProfileImage())
                            .putExtra("name", friendsList.get(pos).getName()));

                } else if (tag.equalsIgnoreCase("itemView")) {


                            startActivity(new Intent(mFriendsActivity, OtherUserProfileActivity.class)
                                    .putExtra("user_id", friendsList.get(pos).getUserId()));



                    /*if (openFrom.equalsIgnoreCase("chat")) {
                        startActivity(new Intent(mFriendsActivity, ChatActivity.class)
                                .putExtra("friend_id", friendsList.get(pos).getUserId())
                                .putExtra("profileimage", friendsList.get(pos).getProfileImage())
                                .putExtra("name", friendsList.get(pos).getName()));
                    }*/
                } else {
                    callAPIfriendUnfriend(tag.equalsIgnoreCase("request_send") ? "1" : "0", friendsList.get(pos).getUserId());
                }

            }
        }));

        sr_friend_suggetion_list.setRefreshing(true);

        friendSuggestion();


    }

    private void friendSuggestion() {

        if (!sr_friend_suggetion_list.isRefreshing())
            sr_friend_suggetion_list.setRefreshing(true);

        ApiCall.getInstance().friendSuggestion(mFriendsActivity, AppClass.preferences.getUserId(), String.valueOf(offset), new IApiCallback() {
            @Override
            public void onSuccess(Object data) {

                ArrayList<FollowerFollowingModel> tempSuggetionList = new Gson().fromJson(((JsonObject) data).getAsJsonArray("suggestion_list").toString()
                        , new TypeToken<List<FollowerFollowingModel>>() {
                        }.getType());

                friendsList.clear();
                friendsList.addAll(tempSuggetionList);
                rv_friend_suggestons_list.getAdapter().notifyDataSetChanged();

                rv_friend_suggestons_list.setVisibility(View.VISIBLE);
                tv_no_suggetions.setVisibility(View.GONE);

                if (sr_friend_suggetion_list.isRefreshing())
                    sr_friend_suggetion_list.setRefreshing(false);

            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {
                if (sr_friend_suggetion_list.isRefreshing())
                    sr_friend_suggetion_list.setRefreshing(false);

                if (StringUtils.isNotEmpty(errorMessage)) {

                    if (errorMessage.equalsIgnoreCase("No data found")) {
                        tv_no_suggetions.setVisibility(View.VISIBLE);
                        rv_friend_suggestons_list.setVisibility(View.GONE);
                    } else {
                        DialogUtils.openDialog(mFriendsActivity, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                            @Override
                            public void onNegativeBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }

                            @Override
                            public void onPositiveBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }
                        });
                    }


                }


            }
        }, false);

    }

    private void callAPIfriendUnfriend(final String type, String otherUserId) {


        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("friend_id", otherUserId);
        request.put("type", type);

        ApiCall.getInstance().friendUnfriend(mFriendsActivity, request, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {

                String response = (String) data;


            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {
                if (StringUtils.isNotEmpty(errorMessage)) {
                    DialogUtils.openDialog(mFriendsActivity, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                        @Override
                        public void onNegativeBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }

                        @Override
                        public void onPositiveBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }
                    });
                }

            }
        }, true);

    }


}
