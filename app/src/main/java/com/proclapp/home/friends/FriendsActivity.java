package com.proclapp.home.friends;

import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.proclapp.R;
import com.proclapp.utils.StringUtils;
import com.proclapp.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class FriendsActivity extends AppCompatActivity implements View.OnClickListener {

    private TabLayout tabs_friends;
    private ImageView img_back,
            img_add_friends;
    private TextView txt_friend;
    private ViewPager view_pager_friends;
//    private EditText edt_search;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friends);

        Utils.setStatusBarColor(getWindow(), this, R.color.white);

        initView();
        initListener();
        setupViewPager(view_pager_friends);

        getIntentData();

    }

    private void initView() {
        img_back = findViewById(R.id.img_back);
        img_add_friends = findViewById(R.id.img_add_friends);
        view_pager_friends = findViewById(R.id.view_pager_friends);
        tabs_friends = findViewById(R.id.tabs_friends);

        txt_friend = findViewById(R.id.txt_friend);

    }


    private void initListener() {
        img_back.setOnClickListener(this);
        img_add_friends.setOnClickListener(this);
    }

    private void getIntentData() {

        if (getIntent() != null &&
                StringUtils.isNotEmpty(getIntent().getStringExtra("openfrom")) && (
                getIntent().getStringExtra("openfrom").equalsIgnoreCase("notification")) ||
                getIntent().getStringExtra("openfrom").equalsIgnoreCase("chat")) {


            tabs_friends.getTabAt(1).select();

        }

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.img_back:
                onBackPressed();
                break;

            case R.id.img_add_friends:
                onBackPressed();
                break;
        }
    }


    private void setupViewPager(ViewPager viewPager) {

        viewPager.setOffscreenPageLimit(2);

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        Bundle bundle = new Bundle();
        bundle.putString("openfrom", getIntent().getStringExtra("openfrom"));

        FriendSuggetionFragment friendSuggetionFragment = new FriendSuggetionFragment();
        FriendsFragment friendsFragment = new FriendsFragment();

        friendSuggetionFragment.setArguments(bundle);
        friendsFragment.setArguments(bundle);

        adapter.addFrag(friendSuggetionFragment, "Suggestions");
        adapter.addFrag(friendsFragment, "Friends");
        viewPager.setAdapter(adapter);
        tabs_friends.setupWithViewPager(viewPager);
    }


    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}
