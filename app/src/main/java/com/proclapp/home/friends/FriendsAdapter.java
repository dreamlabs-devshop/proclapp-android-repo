package com.proclapp.home.friends;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.interfaces.RecyclerClickListener;
import com.proclapp.model.FollowerFollowingModel;
import com.proclapp.utils.ImageLoadUtils;

import java.util.ArrayList;

public class FriendsAdapter extends RecyclerView.Adapter<FriendsAdapter.FriendsViewHolder> {

    AppCompatActivity context;
    String fromWhich;
    RecyclerClickListener listener;
    ArrayList<FollowerFollowingModel> friendsList;

    public FriendsAdapter(AppCompatActivity context, String fromWhich, ArrayList<FollowerFollowingModel> friendsList, RecyclerClickListener listener) {
        this.context = context;
        this.fromWhich = fromWhich;
        this.listener = listener;
        this.friendsList = friendsList;
    }

    @NonNull
    @Override
    public FriendsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_friends, viewGroup, false);
        return new FriendsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final FriendsViewHolder holder, int i) {


        FollowerFollowingModel friendData = friendsList.get(i);

        holder.tv_friend_name.setText(friendData.getName());

        if (fromWhich.equalsIgnoreCase("friends")) {

            holder.iv_chat.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.msg_blue_icon));
        } else {
            holder.iv_chat.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.add_friend));
            holder.iv_chat.setColorFilter(ContextCompat.getColor(context, R.color.color_0c6984), android.graphics.PorterDuff.Mode.SRC_IN);

        }

        ImageLoadUtils.imageLoad(context,
                holder.iv_friend_proflie,
                friendData.getProfileImage(),
                R.drawable.profile_pic_ph);


        holder.itemView.setOnClickListener(view -> listener.onItemClick(holder.getAdapterPosition(), "itemView"));
        holder.iv_chat.setOnClickListener(view -> listener.onItemClick(holder.getAdapterPosition(), "friends"));


        if (friendData.getRequest_status().equalsIgnoreCase("0")) {

            if (friendData.getSender_id().equalsIgnoreCase(AppClass.preferences.getUserId())) {
                holder.ll_friend_request.setVisibility(View.GONE);
                holder.txt_requested.setVisibility(View.VISIBLE);
            } else {
                holder.ll_friend_request.setVisibility(View.VISIBLE);
                holder.txt_requested.setVisibility(View.GONE);
            }
            holder.iv_chat.setVisibility(View.GONE);
        } else {
            holder.txt_requested.setVisibility(View.GONE);
            holder.ll_friend_request.setVisibility(View.GONE);
            holder.iv_chat.setVisibility(View.VISIBLE);
        }

        holder.iv_request_accept.setOnClickListener(view -> listener.onItemClick(holder.getAdapterPosition(), "accept"));
        holder.iv_request_reject.setOnClickListener(view -> listener.onItemClick(holder.getAdapterPosition(), "reject"));

    }

    @Override
    public int getItemCount() {
        return friendsList.size();
    }

    public class FriendsViewHolder extends RecyclerView.ViewHolder {

        private ImageView
                iv_chat,
                iv_request_accept,
                iv_request_reject,
                iv_friend_proflie;
        private TextView
                tv_friend_name,
                txt_requested;
        private LinearLayout ll_friend_request;

        public FriendsViewHolder(@NonNull View itemView) {
            super(itemView);

            iv_chat = itemView.findViewById(R.id.iv_chat);
            iv_friend_proflie = itemView.findViewById(R.id.iv_friend_proflie);
            tv_friend_name = itemView.findViewById(R.id.tv_friend_name);
            txt_requested = itemView.findViewById(R.id.txt_requested);
            iv_request_accept = itemView.findViewById(R.id.iv_request_accept);
            iv_request_reject = itemView.findViewById(R.id.iv_request_reject);
            ll_friend_request = itemView.findViewById(R.id.ll_friend_request);

        }
    }

}
