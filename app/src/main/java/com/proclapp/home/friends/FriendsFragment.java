package com.proclapp.home.friends;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.chat.ChatActivity;
import com.proclapp.interfaces.RecyclerClickListener;
import com.proclapp.model.FollowerFollowingModel;
import com.proclapp.profile.OtherUserProfileActivity;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.AlertDialogInterface;
import com.proclapp.utils.DialogUtils;
import com.proclapp.utils.Logger;
import com.proclapp.utils.StringUtils;

import java.util.ArrayList;

public class FriendsFragment extends Fragment implements SwipyRefreshLayout.OnRefreshListener {

    ArrayList<FollowerFollowingModel> friendsList = new ArrayList<>();
    private RecyclerView rv_friends_list;
    private FriendsActivity mFriendsActivity;
    private SwipyRefreshLayout sr_friend_list;
    private TextView tv_no_friends;
    private int offset = 0;
    private String openFrom = "";
    private boolean isServiceCalling = false, isLastPage = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_friends, container, false);
        initViews(view);
        setUpRecyclerView();
        sr_friend_list.setOnRefreshListener(this);

        if (getArguments() != null &&
                StringUtils.isNotEmpty(getArguments().getString("openfrom")) && (
                getArguments().getString("openfrom").equalsIgnoreCase("notification")) ||
                getArguments().getString("openfrom").equalsIgnoreCase("chat")) {
            openFrom = getArguments().getString("openfrom");

        }

        return view;
    }

    @Override
    public void onRefresh(SwipyRefreshLayoutDirection direction) {
        offset = direction == SwipyRefreshLayoutDirection.TOP ? 0 : offset + 20;
        callAPIGetFriendsList();
    }


    private void initViews(View view) {

        mFriendsActivity = (FriendsActivity) getActivity();
        rv_friends_list = view.findViewById(R.id.rv_friends_list);
        tv_no_friends = view.findViewById(R.id.tv_no_friends);
        sr_friend_list = view.findViewById(R.id.sr_friend_list);

    }

    private void setUpRecyclerView() {

        LinearLayoutManager layoutManager = new LinearLayoutManager(mFriendsActivity);
        rv_friends_list.setLayoutManager(layoutManager);
        rv_friends_list.setAdapter(new FriendsAdapter(mFriendsActivity, "friends", friendsList, new RecyclerClickListener() {
            @Override
            public void onItemClick(int pos, String tag) {
                if (tag.equalsIgnoreCase("friends")) {
                    startActivity(new Intent(mFriendsActivity, ChatActivity.class)
                            .putExtra("friend_id", friendsList.get(pos).getUserId())
                            .putExtra("profileimage", friendsList.get(pos).getProfileImage())
                            .putExtra("name", friendsList.get(pos).getName()));

                } else if (tag.equalsIgnoreCase("itemView") && openFrom.equalsIgnoreCase("chat")) {
                    startActivity(new Intent(mFriendsActivity, ChatActivity.class)
                            .putExtra("friend_id", friendsList.get(pos).getUserId())
                            .putExtra("profileimage", friendsList.get(pos).getProfileImage())
                            .putExtra("name", friendsList.get(pos).getName()));
                } else if (tag.equalsIgnoreCase("itemView")) {
                    startActivity(new Intent(mFriendsActivity, OtherUserProfileActivity.class)
                            .putExtra("user_id", friendsList.get(pos).getUserId()));
                } else if (tag.equalsIgnoreCase("accept")) {
                    acceptRejectFriendRequest(friendsList.get(pos).getUserId(), "1", pos);

                } else if (tag.equalsIgnoreCase("reject")) {
                    acceptRejectFriendRequest(friendsList.get(pos).getUserId(), "2", pos);
                }
            }
        }));

        sr_friend_list.post(() -> {
            sr_friend_list.setRefreshing(true);
            onRefresh(SwipyRefreshLayoutDirection.TOP);
        });

        rv_friends_list.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                int visibleItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int firstVisibleItemPosition = layoutManager.findLastVisibleItemPosition();

                if (!isServiceCalling && !isLastPage) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0) {
                        sr_friend_list.post(new Runnable() {
                            @Override
                            public void run() {
                                sr_friend_list.setRefreshing(true);
                                onRefresh(SwipyRefreshLayoutDirection.BOTTOM);
                            }
                        });

                    }
                }
            }
        });


    }


    private void callAPIGetFriendsList() {


        ApiCall.getInstance().getFriendsList(mFriendsActivity, AppClass.preferences.getUserId(), String.valueOf(offset), new IApiCallback() {
            @Override
            public void onSuccess(Object data) {


                if (offset == 0) {
                    friendsList.clear();
                }

                friendsList.addAll((ArrayList<FollowerFollowingModel>) data);
                sr_friend_list.setRefreshing(false);
                isLastPage = false;
                isServiceCalling = false;
                tv_no_friends.setVisibility(View.GONE);
                rv_friends_list.setVisibility(View.VISIBLE);
                rv_friends_list.getAdapter().notifyDataSetChanged();


            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {


                sr_friend_list.setRefreshing(false);
                isLastPage = true;
                isServiceCalling = false;

                if (StringUtils.isNotEmpty(errorMessage)) {
                    if (errorMessage.equalsIgnoreCase("No friends")) {

                        if (offset == 0) {
                            tv_no_friends.setText(errorMessage);
                            tv_no_friends.setVisibility(View.VISIBLE);
                            rv_friends_list.setVisibility(View.GONE);
                        }

                    } else {


                        DialogUtils.openDialog(mFriendsActivity, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                            @Override
                            public void onNegativeBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }

                            @Override
                            public void onPositiveBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }
                        });
                    }
                }
            }
        }, false);


    }


    private void acceptRejectFriendRequest(String senderId, final String status, final int pos) {

        String userId = AppClass.preferences.getUserId();

        ApiCall.getInstance().acceptRejectRequest(mFriendsActivity, userId, senderId, status, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {

                if (status.equalsIgnoreCase("1")) {
                    friendsList.get(pos).setRequest_status("1");
                } else {
                    friendsList.remove(pos);
                }

                rv_friends_list.getAdapter().notifyDataSetChanged();

            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {

                Logger.e("Fail " + errorMessage);
                if (StringUtils.isNotEmpty(errorMessage)) {
                    DialogUtils.openDialog(mFriendsActivity, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                        @Override
                        public void onNegativeBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }

                        @Override
                        public void onPositiveBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }
                    });
                }

            }
        }, true);

    }

}

