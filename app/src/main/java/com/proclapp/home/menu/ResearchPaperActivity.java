package com.proclapp.home.menu;

import android.content.DialogInterface;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.home.adapter.AllPostsAdapter;
import com.proclapp.interfaces.RecyclerClickListener;
import com.proclapp.model.AllPostsModel;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.AlertDialogInterface;
import com.proclapp.utils.DialogUtils;
import com.proclapp.utils.Logger;

import java.util.ArrayList;
import java.util.HashMap;

public class ResearchPaperActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView img_back;
    private RecyclerView rv_research_paper;
    private ArrayList<AllPostsModel> userResearchPaperList = new ArrayList<>();
    private AllPostsAdapter researchPaperAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_research_paper);

        initView();
        callAPIUserResearchPaper();
    }

    private void initView() {
        img_back = findViewById(R.id.img_back);
        rv_research_paper = findViewById(R.id.rv_research_paper);

        img_back.setOnClickListener(this);

        rv_research_paper.setLayoutManager(new LinearLayoutManager(ResearchPaperActivity.this));
        researchPaperAdapter = new AllPostsAdapter(ResearchPaperActivity.this, userResearchPaperList, new RecyclerClickListener() {
            @Override
            public void onItemClick(int pos, String tag) {

            }
        });
        rv_research_paper.setAdapter(researchPaperAdapter);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.img_back:
                onBackPressed();
                break;
        }
    }

    private void callAPIUserResearchPaper() {

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());

        Logger.d("request:" + request);

        ApiCall.getInstance().userResearchPaper(ResearchPaperActivity.this,
                request, new IApiCallback() {
                    @Override
                    public void onSuccess(Object data) {

                        userResearchPaperList.clear();
                        userResearchPaperList.addAll((ArrayList<AllPostsModel>) data);

                        researchPaperAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onFailure(boolean isFalseFromService, String errorMessage) {

                        DialogUtils.openDialog(ResearchPaperActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                            @Override
                            public void onNegativeBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }

                            @Override
                            public void onPositiveBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }
                        });
                    }


                }, true);

    }
}
