package com.proclapp.home.menu;

import android.app.Activity;
import android.content.DialogInterface;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.model.CategoryModel;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.AlertDialogInterface;
import com.proclapp.utils.DialogUtils;
import com.proclapp.utils.ImageLoadUtils;
import com.proclapp.utils.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 *
 */

public class TrendingTopicAdapter extends RecyclerView.Adapter<TrendingTopicAdapter.MyViewHolder> {

    private Activity activity;
    private ArrayList<CategoryModel> list;

    private int lastPosition = -1;

    public TrendingTopicAdapter(Activity activity, ArrayList<CategoryModel> list) {
        this.activity = activity;
        this.list = list;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_trending_topic, parent, false);
        return new MyViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        holder.txt_topic_name.setText(list.get(holder.getAdapterPosition()).getCategory_name());

        if (list.get(holder.getAdapterPosition()).getIs_selected() == 1) {

            holder.txt_follow.setText(activity.getString(R.string.following));
            holder.txt_follow.setCompoundDrawablesWithIntrinsicBounds(R.drawable.tick, 0, 0, 0);
        } else {
            holder.txt_follow.setText(activity.getString(R.string.follow));
            holder.txt_follow.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }

        ImageLoadUtils.imageLoad(activity,
                holder.img_topic,
                list.get(position).getCategory_img());

        if (StringUtils.isNotEmpty(list.get(holder.getAdapterPosition()).getTotalFollowers()))
            holder.txt_topic_follower_count.setText(activity.getString(R.string.trending_follower, list.get(holder.getAdapterPosition()).getTotalFollowers()));


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    private void updateCategoryToUserProfile(String category) {

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("category", category);

        ApiCall.getInstance().updateCategoryToUserProfile(activity, request, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {
                String responce = (String) data;

            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {
                if (StringUtils.isNotEmpty(errorMessage)) {
                    DialogUtils.openDialog(activity, errorMessage, activity.getString(R.string.ok), "", new AlertDialogInterface() {
                        @Override
                        public void onNegativeBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }

                        @Override
                        public void onPositiveBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }
                    });
                }
            }
        }, false);


    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txt_topic_name,
                txt_topic_follower_count,
                txt_follow;
        CircleImageView img_topic;

        public MyViewHolder(View itemView) {
            super(itemView);
            txt_topic_name = itemView.findViewById(R.id.txt_topic_name);
            txt_topic_follower_count = itemView.findViewById(R.id.txt_topic_follower_count);
            img_topic = itemView.findViewById(R.id.img_topic);
            txt_follow = itemView.findViewById(R.id.txt_follow);
        }
    }


}
