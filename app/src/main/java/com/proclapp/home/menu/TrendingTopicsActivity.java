package com.proclapp.home.menu;

import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.model.CategoryModel;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.AlertDialogInterface;
import com.proclapp.utils.DialogUtils;
import com.proclapp.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;

public class TrendingTopicsActivity extends AppCompatActivity implements View.OnClickListener, SwipyRefreshLayout.OnRefreshListener {

    private RecyclerView rv_trending_topics;
    private SwipyRefreshLayout sr_trending;
    private ImageView img_back;
    private TextView txt_title;
    private TrendingTopicAdapter trendingTopicAdapter;
    private ArrayList<CategoryModel> trendingTopicsList = new ArrayList<>();
    private int offset = 0;
    private boolean isServiceCalling = false, isLastPage = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trending_topics);

        Utils.setStatusBarColor(getWindow(), this, R.color.white);

        initView();
        initListener();
        setRecyclerview();
    }

    @Override
    public void onRefresh(SwipyRefreshLayoutDirection direction) {
        offset = direction == SwipyRefreshLayoutDirection.TOP ? 0 : offset + 20;
        callAPITrendingTopics();
    }

    private void initView() {

        txt_title = findViewById(R.id.txt_title);
        img_back = findViewById(R.id.img_back);

        rv_trending_topics = findViewById(R.id.rv_trending_topics);
        sr_trending = findViewById(R.id.sr_trending);

        sr_trending.setOnRefreshListener(this);

    }

    private void setRecyclerview() {

        GridLayoutManager layoutManager = new GridLayoutManager(TrendingTopicsActivity.this, 2);
        rv_trending_topics.setLayoutManager(layoutManager);

        trendingTopicAdapter = new TrendingTopicAdapter(TrendingTopicsActivity.this, trendingTopicsList);
        rv_trending_topics.setAdapter(trendingTopicAdapter);

        sr_trending.post(() -> {
            sr_trending.setRefreshing(true);
            onRefresh(SwipyRefreshLayoutDirection.TOP);
        });


        rv_trending_topics.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                int visibleItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int firstVisibleItemPosition = layoutManager.findLastVisibleItemPosition();

                if (!isServiceCalling && !isLastPage) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0) {
                        sr_trending.post(() -> {
                            sr_trending.setRefreshing(true);
                            onRefresh(SwipyRefreshLayoutDirection.BOTTOM);
                        });

                    }
                }

            }
        });

        rv_trending_topics.setItemAnimator(new DefaultItemAnimator());

    }


    private void initListener() {
        img_back.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.img_back:
                onBackPressed();
                break;
        }
    }

    private void callAPITrendingTopics() {

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("offset", String.valueOf(offset));


        ApiCall.getInstance().trendingTopics(TrendingTopicsActivity.this,
                request, new IApiCallback() {
                    @Override
                    public void onSuccess(Object data) {
                        if (offset == 0)
                            trendingTopicsList.clear();


                        trendingTopicsList.addAll((ArrayList<CategoryModel>) data);

                            sr_trending.setRefreshing(false);
                            isLastPage = false;
                            isServiceCalling = false;
                            rv_trending_topics.setVisibility(View.VISIBLE);
                            trendingTopicAdapter.notifyDataSetChanged();


                    }

                    @Override
                    public void onFailure(boolean isFalseFromService, String errorMessage) {


                            sr_trending.setRefreshing(false);
                            isLastPage = true;
                            isServiceCalling = false;



                        if (errorMessage.equalsIgnoreCase("No data found")) {

                            if (offset == 0) {
                                rv_trending_topics.setVisibility(View.GONE);
                            }

                        } else {

                            DialogUtils.openDialog(TrendingTopicsActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                                @Override
                                public void onNegativeBtnClicked(DialogInterface alrt) {
                                    alrt.dismiss();
                                }

                                @Override
                                public void onPositiveBtnClicked(DialogInterface alrt) {
                                    alrt.dismiss();
                                }
                            });
                        }
                    }


                }, false);

    }



}
