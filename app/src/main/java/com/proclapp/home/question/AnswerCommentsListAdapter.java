package com.proclapp.home.question;

import android.app.Activity;
import android.content.DialogInterface;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.home.AnswerEditDeleteDialog;
import com.proclapp.home.SeeDiscussionActivity;
import com.proclapp.home.fragment.ReplyBottomSheet;
import com.proclapp.interfaces.RecyclerClickListener;
import com.proclapp.model.AnswerModel;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.AlertDialogInterface;
import com.proclapp.utils.DialogUtils;
import com.proclapp.utils.ImageLoadUtils;
import com.proclapp.utils.KeyBoardUtils;
import com.proclapp.utils.Logger;

import java.util.ArrayList;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 *
 */

public class AnswerCommentsListAdapter extends RecyclerView.Adapter<AnswerCommentsListAdapter.MyViewHolder> {

    private AppCompatActivity activity;
    private ArrayList<AnswerModel.Comments> commentsList;
    private RecyclerClickListener listener;
    private String postId;

    public AnswerCommentsListAdapter(AppCompatActivity activity, ArrayList<AnswerModel.Comments> commentsList, String postId, RecyclerClickListener listener) {
        this.activity = activity;
        this.commentsList = commentsList;
        this.listener = listener;
        this.postId = postId;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_answer_comment_list, parent, false);
        return new MyViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        increaseDrawableSizes(holder);

        final AnswerModel.Comments comment = commentsList.get(holder.getAdapterPosition());
        final ArrayList<AnswerModel.Comments> replies = commentsList.get(holder.getAdapterPosition()).getReplies();

        LinearLayoutManager layoutManager = new LinearLayoutManager(activity);
        holder.repliesRv.setLayoutManager(layoutManager);
        holder.repliesRv.setNestedScrollingEnabled(false);
        holder.repliesRv.setAdapter(new ReplyListAdapter(activity, replies));


//        if (reply != null) {
//            float scale = activity.getResources().getDisplayMetrics().density;
//            int dpAsPixels = (int) (30 * scale + 0.5f);
//
//            //buildQuotedComment(holder, reply);
//            holder.ll_response_buttons.setVisibility(View.GONE);
//            holder.grey_border.setVisibility(View.VISIBLE);
//            holder.ll_main.setPadding(dpAsPixels, 0, 0, 0);
//        }

        holder.txt_person_name.setText(comment.getUser().getName());
        holder.txt_ans_comment.setText(comment.getComment());
        holder.txt_person_organisation.setText(comment.getUser().getProfessionalQualification());

        ImageLoadUtils.imageLoad(activity,
                holder.img_voter_pic,
                AppClass.preferences.getProfileImage(),
                R.drawable.menu_user_ph);

        if (comment.getComment_like_count() > 0) {
            holder.txt_like_count.setText(String.valueOf(comment.getComment_like_count()));
            holder.txt_like.setText(activity.getResources().getQuantityString(R.plurals.like, comment.getAnswer_upvote_comment_count(), comment.getAnswer_upvote_comment_count()));

        }
//        if (comments.getAnswer_downvote_comment_count() > 0) {
//            holder.txt_downvote_count.setText(String.valueOf(comments.getAnswer_downvote_comment_count()));
//            holder.txt_downvote.setText(activity.getResources().getQuantityString(R.plurals.downvote, comments.getAnswer_downvote_comment_count(), comments.getAnswer_downvote_comment_count()));
//
//        }

        if (comment.getIs_comment_liked().equalsIgnoreCase("true")) {
            holder.txt_your_vote.setText(activity.getString(R.string.you_liked_this));
            //holder.ll_down_vote.setSelected(false);
            holder.ll_like.setSelected(true);
        }

//        if (comments.getIs_post_answer_downvote().equalsIgnoreCase("true")) {
//            holder.txt_your_vote.setText(activity.getString(R.string.You_downvoted_this));
//            holder.ll_down_vote.setSelected(true);
//            holder.ll_up_vote.setSelected(false);
//        }

        if (comment.getIs_comment_liked().equalsIgnoreCase("false")) {

            holder.img_voter_pic.setVisibility(View.INVISIBLE);
            holder.txt_your_vote.setVisibility(View.INVISIBLE);
            holder.ll_voter_details.setVisibility(View.GONE);
        }

        ImageLoadUtils.imageLoad(activity,
                holder.img_user_pic,
                comment.getUser().getProfileImage(),
                R.drawable.menu_user_ph);

        if (AppClass.preferences.getUserId().equalsIgnoreCase(commentsList.get(holder.getAdapterPosition()).getUser().getUserId()))
            holder.img_option.setVisibility(View.VISIBLE);
        else
            holder.img_option.setVisibility(View.GONE);

        holder.img_option.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                KeyBoardUtils.closeSoftKeyboard(activity);
                if (AppClass.preferences.getUserId().equalsIgnoreCase(commentsList.get(holder.getAdapterPosition()).getUser().getUserId())) {

                    new AnswerEditDeleteDialog(activity, "comment", new AnswerEditDeleteDialog.OnItemClick() {
                        @Override
                        public void onClick(String tag) {

                            if (tag.equalsIgnoreCase("delete")) {
                                callAPIDeletePostAnswerComment(comment.getComment_id(), holder.getAdapterPosition());
                            } else {
                                listener.onItemClick(holder.getAdapterPosition(), "edit");
                            }

                        }
                    }).show();

                }
            }
        });

        holder.ll_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callAPILikeUnlike(holder.ll_like.isSelected() ? "0" : "1", holder);
            }
        });


        holder.ll_reply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ReplyBottomSheet bottomSheet = new ReplyBottomSheet();
                Bundle bundle = new Bundle();
                bundle.putString("comment_id", commentsList.get(holder.getAdapterPosition()).comment_id);
                bottomSheet.setArguments(bundle);
                bottomSheet.show(activity.getSupportFragmentManager(), "");
            }
        });

//        holder.ll_down_vote.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (!holder.ll_down_vote.isSelected())
//                    callAPIPostUpVoteDownVote("2", holder);
//            }
//        });

        holder.ll_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sharePost(holder.getAdapterPosition());
            }
        });
    }

    private void buildQuotedComment(MyViewHolder holder, AnswerModel.Comments reply) {
        holder.ll_quote.setVisibility(View.VISIBLE);

        holder.txt_person_name_quote.setText(reply.getUser().getName());
        holder.txt_ans_comment_quote.setText(reply.getComment());
        holder.txt_person_organisation_quote.setText(reply.getUser().getProfessionalQualification());

//        ImageLoadUtils.imageLoad(activity,
//                holder.img_user_pic_quote,
//                reply.getUser().getProfileImage(),
//                R.drawable.menu_user_ph);
    }

    private void increaseDrawableSizes(MyViewHolder holder) {
        holder.txt_like.setCompoundDrawables(getDrawableAndResize(R.drawable.selector_like_post), null, null, null);
        holder.txt_reply.setCompoundDrawables(getDrawableAndResize(R.drawable.comments), null, null, null);
        holder.txt_share.setCompoundDrawables(getDrawableAndResize(R.drawable.share_blue), null, null, null);
    }

    private Drawable getDrawableAndResize(int drawableId) {
        Drawable drawable = ContextCompat.getDrawable(activity, drawableId);
        drawable.setBounds(0,
                0,
                (int) (drawable.getIntrinsicWidth() * 1.4),
                (int) (drawable.getIntrinsicHeight() * 1.4));

        return drawable;
    }

    private void sharePost(int index) {

        String shareBody = commentsList.get(index).comment;
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
        activity.startActivity(Intent.createChooser(sharingIntent, activity.getString(R.string.share_using)));

    }

    private void callAPIPostUpVoteDownVote(String status, MyViewHolder holder) {
        //answerLists.postId
        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("post_id", postId);
        request.put("answer_id", commentsList.get(holder.getAdapterPosition()).answer_id);
        request.put("comment_id", commentsList.get(holder.getAdapterPosition()).comment_id);
        request.put("status", status);

        String upDownVoteStatus = status;

        Logger.d("request:" + request);

        ApiCall.getInstance().postCommentUpVoteDownVote(activity, request, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {

                if (upDownVoteStatus.equalsIgnoreCase("1")) {
                    //holder.ll_down_vote.setSelected(false);
                    holder.ll_like.setSelected(true);
                    holder.ll_voter_details.setVisibility(View.VISIBLE);
                    holder.img_voter_pic.setVisibility(View.VISIBLE);
                    holder.txt_your_vote.setVisibility(View.VISIBLE);
                    holder.txt_your_vote.setText(activity.getString(R.string.you_upvoted_this));
                    holder.txt_like_count.setText(String.valueOf((commentsList.get(holder.getAdapterPosition()).getAnswer_upvote_comment_count() + 1)));
                    commentsList.get(holder.getAdapterPosition()).setAnswer_upvote_comment_count(commentsList.get(holder.getAdapterPosition()).getAnswer_upvote_comment_count() + 1);
                    holder.txt_like.setText(activity.getResources().getQuantityString(R.plurals.like, commentsList.get(holder.getAdapterPosition()).getAnswer_upvote_comment_count(), commentsList.get(holder.getAdapterPosition()).getAnswer_upvote_comment_count()));
                    if (commentsList.get(holder.getAdapterPosition()).getAnswer_downvote_comment_count() > 0) {

                        String downVoteCount = String.valueOf((commentsList.get(holder.getAdapterPosition()).getAnswer_downvote_comment_count() - 1));

                        if (downVoteCount.equalsIgnoreCase("0")) {
                            holder.txt_downvote_count.setText("");
                        } else {
                            holder.txt_downvote_count.setText(downVoteCount);
                        }
                        commentsList.get(holder.getAdapterPosition()).setAnswer_downvote_comment_count(commentsList.get(holder.getAdapterPosition()).getAnswer_downvote_comment_count() - 1);
                        // holder.txt_downvote.setText(activity.getResources().getQuantityString(R.plurals.downvote, commentsList.get(holder.getAdapterPosition()).getAnswer_downvote_comment_count(), commentsList.get(holder.getAdapterPosition()).getAnswer_downvote_comment_count()));
                    }

                } else {
                    //holder.ll_down_vote.setSelected(true);
                    holder.ll_like.setSelected(false);
                    holder.txt_your_vote.setText(activity.getString(R.string.You_downvoted_this));
                    if (commentsList.get(holder.getAdapterPosition()).getAnswer_upvote_comment_count() > 0) {

                        String upVoteCount = String.valueOf((commentsList.get(holder.getAdapterPosition()).getAnswer_upvote_comment_count() - 1));
                        if (upVoteCount.equalsIgnoreCase("0")) {
                            holder.txt_like_count.setText("");
                        } else {
                            holder.txt_like_count.setText(upVoteCount);
                        }

                        commentsList.get(holder.getAdapterPosition()).setAnswer_upvote_comment_count(commentsList.get(holder.getAdapterPosition()).getAnswer_upvote_comment_count() - 1);
                        holder.txt_like.setText(activity.getResources().getQuantityString(R.plurals.upvote, commentsList.get(holder.getAdapterPosition()).getAnswer_upvote_comment_count(), commentsList.get(holder.getAdapterPosition()).getAnswer_upvote_comment_count()));

                    }
                    holder.txt_downvote_count.setText(String.valueOf((commentsList.get(holder.getAdapterPosition()).getAnswer_downvote_comment_count() + 1)));
                    commentsList.get(holder.getAdapterPosition()).setAnswer_downvote_comment_count((commentsList.get(holder.getAdapterPosition()).getAnswer_downvote_comment_count() + 1));
                    //holder.txt_downvote.setText(activity.getResources().getQuantityString(R.plurals.downvote, commentsList.get(holder.getAdapterPosition()).getAnswer_downvote_comment_count(), commentsList.get(holder.getAdapterPosition()).getAnswer_downvote_comment_count()));

                }

            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {

            }


        }, true);

    }

    private void callAPILikeUnlike(String status, MyViewHolder holder) {
        //answerLists.postId
        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("comment_id", commentsList.get(holder.getAdapterPosition()).comment_id);
        request.put("like_unlike", status);

        Logger.d("request:" + request);

        ApiCall.getInstance().postCommentLikeUnlike(activity, request, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {
                Log.d("the on success response", status);

                if (status.equalsIgnoreCase("1")) {
                    //holder.ll_down_vote.setSelected(false);
                    holder.ll_like.setSelected(true);
                    holder.ll_voter_details.setVisibility(View.VISIBLE);
                    holder.img_voter_pic.setVisibility(View.VISIBLE);
                    holder.txt_your_vote.setVisibility(View.VISIBLE);
                    holder.txt_your_vote.setText(activity.getString(R.string.you_liked_this));
                    holder.txt_like_count.setText(String.valueOf((commentsList.get(holder.getAdapterPosition()).getComment_like_count() + 1)));
                    commentsList.get(holder.getAdapterPosition()).setComment_like_count(commentsList.get(holder.getAdapterPosition()).getComment_like_count() + 1);
                    //holder.txt_like.setText(activity.getResources().getQuantityString(R.plurals.like, commentsList.get(holder.getAdapterPosition()).getComment_like_count(), commentsList.get(holder.getAdapterPosition()).getComment_like_count()));
//                    if (commentsList.get(holder.getAdapterPosition()).getAnswer_downvote_comment_count() > 0) {
//
//                        String downVoteCount = String.valueOf((commentsList.get(holder.getAdapterPosition()).getAnswer_downvote_comment_count() - 1));
//
//                        if (downVoteCount.equalsIgnoreCase("0")) {
//                            holder.txt_downvote_count.setText("");
//                        } else {
//                            holder.txt_downvote_count.setText(downVoteCount);
//                        }
//                        commentsList.get(holder.getAdapterPosition()).setAnswer_downvote_comment_count(commentsList.get(holder.getAdapterPosition()).getAnswer_downvote_comment_count() - 1);
//                        // holder.txt_downvote.setText(activity.getResources().getQuantityString(R.plurals.downvote, commentsList.get(holder.getAdapterPosition()).getAnswer_downvote_comment_count(), commentsList.get(holder.getAdapterPosition()).getAnswer_downvote_comment_count()));
//                    }

                } else {
                    //holder.ll_down_vote.setSelected(true);
                    holder.ll_like.setSelected(false);
                    holder.img_voter_pic.setVisibility(View.INVISIBLE);
                    holder.txt_your_vote.setVisibility(View.INVISIBLE);
                    holder.ll_voter_details.setVisibility(View.GONE);

                    if (commentsList.get(holder.getAdapterPosition()).getComment_like_count() > 0) {

                        String likeCount = String.valueOf((commentsList.get(holder.getAdapterPosition()).getComment_like_count() - 1));
                        if (likeCount.equalsIgnoreCase("0")) {
                            holder.txt_like_count.setText("");
                        } else {
                            holder.txt_like_count.setText(likeCount);
                        }

                        commentsList.get(holder.getAdapterPosition()).setComment_like_count(commentsList.get(holder.getAdapterPosition()).getComment_like_count() - 1);
                        //holder.txt_like.setText(activity.getResources().getQuantityString(R.plurals.like, commentsList.get(holder.getAdapterPosition()).getComment_like_count(), commentsList.get(holder.getAdapterPosition()).getComment_like_count()));

                    }
                    //holder.txt_downvote_count.setText(String.valueOf((commentsList.get(holder.getAdapterPosition()).getAnswer_downvote_comment_count() + 1)));
                    //commentsList.get(holder.getAdapterPosition()).setAnswer_downvote_comment_count((commentsList.get(holder.getAdapterPosition()).getAnswer_downvote_comment_count() + 1));
                    //holder.txt_downvote.setText(activity.getResources().getQuantityString(R.plurals.downvote, commentsList.get(holder.getAdapterPosition()).getAnswer_downvote_comment_count(), commentsList.get(holder.getAdapterPosition()).getAnswer_downvote_comment_count()));

                }

            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {

            }


        }, true);

    }

    @Override
    public int getItemCount() {
        return commentsList.size();
    }

    private void callAPIDeletePostAnswerComment(String comment_id, final int position) {

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("comment_id", comment_id);

        Logger.d("request:" + request);

        ApiCall.getInstance().deletePostAnswerComment(activity,
                request, new IApiCallback() {
                    @Override
                    public void onSuccess(Object data) {

                        commentsList.remove(position);
                        notifyDataSetChanged();
                    }

                    @Override
                    public void onFailure(boolean isFalseFromService, String errorMessage) {


                        DialogUtils.openDialog(activity, errorMessage, activity.getString(R.string.ok), "", new AlertDialogInterface() {
                            @Override
                            public void onNegativeBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }

                            @Override
                            public void onPositiveBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }
                        });
                    }


                }, true);

    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txt_person_name,
                txt_person_organisation,
                txt_your_vote,
                txt_like_count,
                txt_like,
                txt_downvote_count,
                txt_reply,
                txt_share,
                txt_person_name_quote,
                txt_person_organisation_quote,
                txt_ans_comment_quote,
                txt_ans_comment;
        View grey_border;
        private CircleImageView img_user_pic, img_voter_pic, img_user_pic_quote;
        private LinearLayout ll_details, ll_like, ll_reply, ll_share, ll_voter_details, ll_quote, ll_comment, ll_main, ll_response_buttons;
        private ImageView img_option;
        private RecyclerView repliesRv;

        public MyViewHolder(View itemView) {
            super(itemView);
            txt_your_vote = itemView.findViewById(R.id.txt_your_vote);
            txt_like_count = itemView.findViewById(R.id.txt_like_count);
            txt_like = itemView.findViewById(R.id.txt_like);
            txt_downvote_count = itemView.findViewById(R.id.txt_downvote_count);
            txt_reply = itemView.findViewById(R.id.txt_reply);
            txt_person_name = itemView.findViewById(R.id.txt_person_name);
            txt_person_organisation = itemView.findViewById(R.id.txt_person_organisation);
            txt_ans_comment = itemView.findViewById(R.id.txt_ans_comment);
            txt_share = itemView.findViewById(R.id.txt_share);
            img_option = itemView.findViewById(R.id.img_option);
            ll_details = itemView.findViewById(R.id.ll_details);
            ll_like = itemView.findViewById(R.id.ll_like);
            grey_border = itemView.findViewById(R.id.grey_border);
            ll_reply = itemView.findViewById(R.id.ll_reply);
            ll_share = itemView.findViewById(R.id.ll_share);
            ll_comment = itemView.findViewById(R.id.swipe_to_delete);
            ll_voter_details = itemView.findViewById(R.id.ll_voter_details);
            ll_quote = itemView.findViewById(R.id.ll_quote);
            ll_main = itemView.findViewById(R.id.ll_main);
            ll_response_buttons = itemView.findViewById(R.id.ll_response_buttons);
            txt_person_name_quote = itemView.findViewById(R.id.txt_person_name_quote);
            txt_person_organisation_quote = itemView.findViewById(R.id.txt_person_organisation_quote);
            txt_ans_comment_quote = itemView.findViewById(R.id.txt_ans_comment_quote);
            repliesRv = itemView.findViewById(R.id.replies_rv);

            img_voter_pic = itemView.findViewById(R.id.img_voter_pic);
            img_user_pic = itemView.findViewById(R.id.img_user_pic);
            //img_user_pic_quote = itemView.findViewById(R.id.img_user_pic_quote);

            // set typeface

            txt_person_name.setTypeface(AppClass.lato_medium);
            txt_person_organisation.setTypeface(AppClass.lato_regular);
            txt_ans_comment.setTypeface(AppClass.lato_regular);

            ll_details.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(getAdapterPosition(), "profile");
                }
            });

            img_user_pic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(getAdapterPosition(), "profile");
                }
            });

        }
    }
}
