package com.proclapp.home.question;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.home.AnswerEditDeleteDialog;
import com.proclapp.home.adapter.ArticleImagesAdapter;
import com.proclapp.home.fragment.ReplyBottomSheet;
import com.proclapp.interfaces.RecyclerClickListener;
import com.proclapp.linkpreview.LinkPreviewCallback;
import com.proclapp.linkpreview.SourceContent;
import com.proclapp.linkpreview.TextCrawler;
import com.proclapp.model.AnswerModel;
import com.proclapp.profile.OtherUserProfileActivity;
import com.proclapp.profile.ProfileActivity;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.AlertDialogInterface;
import com.proclapp.utils.DateTimeUtils;
import com.proclapp.utils.DialogUtils;
import com.proclapp.utils.ImageLoadUtils;
import com.proclapp.utils.KeyBoardUtils;
import com.proclapp.utils.Logger;
import com.proclapp.utils.StringUtils;
import com.proclapp.utils.Utils;

import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class AnswerDetailsActivity extends AppCompatActivity implements View.OnClickListener, LinkPreviewCallback, ReplyBottomSheet.BottomSheetListener {

    public static boolean isAnswerEdited = false;
    private ImageView img_close,
            img_option,
            img_follow,
            img_send,
            iv_single_image,
            img_emoji;
    private CircleImageView img_user_pic,
            img_voter_pic;
    private EditText edt_comment;
    private RecyclerView rv_comments_list, rv_all_images;
    private TextView txt_person_name,
            txt_person_organisation,
            txt_time,
            txt_answer_data,
            txt_your_vote,
            txt_comment_count,
            txt_view_count,
            txt_upvote,
            txt_upvote_count,
            txt_downvote,
            txt_downvote_count,
            txt_bookmark,
            txt_share,
            tv_see_more;
    private LinearLayout ll_upvote,
            ll_downvote,
            ll_bookmark,
            ll_share;
    private AnswerCommentsListAdapter answerCommentsListAdapter;
    private String commentId = "";
    private AnswerModel answerLists;
    private String upDownVoteStatus = "";
    private TextCrawler textCrawler;
    private ViewGroup dropPreview;
    private Bitmap[] currentImageSet;
    private int countBigImages = 0;
    private String currentTitle = "", currentUrl = "", currentCannonicalUrl = "", currentDescription = "";
    private LinearLayout linearLayout;
    private String answerId = "", postId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_answer_details);
        Utils.setStatusBarColor(getWindow(), this, R.color.white);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        textCrawler = new TextCrawler();
        answerId = getIntent().getStringExtra("answerLists");
        postId = getIntent().getStringExtra("postId");


        initView();
        increaseDrawableSizes();
        initStyle();
        initListener();

        callAPIGetAnswerDetails();
        callAPIPostRead();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isAnswerEdited)
            callAPIGetAnswerDetails();
    }

    @Override
    public void onButtonClicked(String text, String comment_id) {
        KeyBoardUtils.closeSoftKeyboard(AnswerDetailsActivity.this);
        if (text.trim().length() > 0) {
            callAPIAddPostAnswerReply(answerLists.getPostAnswersId(), text, comment_id);
        }
    }

    private void setData() {

        txt_person_name.setText(answerLists.getUser().getName());
        if (StringUtils.isNotEmpty(answerLists.getUser().getProfessionalQualification()))
            txt_person_organisation.setText(String.format(" (%s)", answerLists.getUser().getProfessionalQualification()));
        txt_answer_data.setText(StringUtils.isHtml(answerLists.getAnswer()) ? Utils.fromHtml(answerLists.getAnswer()) : answerLists.getAnswer());

        if (answerLists.getUserId().equalsIgnoreCase(AppClass.preferences.getUserId())) {
            img_option.setVisibility(View.VISIBLE);
        } else {
            img_option.setVisibility(View.GONE);
        }

        txt_time.setText(DateTimeUtils.changeDateTimeFormat("yyyy-MM-dd HH:mm:ss",
                "dd MMM, yyyy, HH:mm a",
                answerLists.getDate()));

        ImageLoadUtils.imageLoad(this,
                img_user_pic,
                answerLists.getUser().getProfileImage(),
                R.drawable.menu_user_ph);

        if (answerLists.getUser().getIsFollowing().equalsIgnoreCase("true")) {
            img_follow.setSelected(true);
        } else {
            img_follow.setSelected(false);
        }

        if (answerLists.getUser().getUserId().equalsIgnoreCase(AppClass.preferences.getUserId())) {
            img_follow.setVisibility(View.GONE);
        } else {
            img_follow.setVisibility(View.GONE);
        }


        txt_comment_count.setText(answerLists.getAnswer_comment_count());
        if (answerLists.getAnswer_upvote_comment_count() > 0) {
            txt_upvote_count.setText(String.valueOf(answerLists.getAnswer_upvote_comment_count()));
            txt_upvote.setText(getResources().getQuantityString(R.plurals.upvote, answerLists.getAnswer_upvote_comment_count(), answerLists.getAnswer_upvote_comment_count()));

        }
        if (answerLists.getAnswer_downvote_comment_count() > 0) {
            txt_downvote_count.setText(String.valueOf(answerLists.getAnswer_downvote_comment_count()));
            txt_downvote.setText(getResources().getQuantityString(R.plurals.downvote, answerLists.getAnswer_downvote_comment_count(), answerLists.getAnswer_downvote_comment_count()));

        }


        txt_view_count.setText(String.valueOf(answerLists.getPost_answer_read_count()));


        ImageLoadUtils.imageLoad(this,
                img_voter_pic,
                AppClass.preferences.getProfileImage(),
                R.drawable.upvoted_user_ph);


        if (answerLists.getIs_post_answer_upvote().equalsIgnoreCase("true")) {
            txt_your_vote.setText(getString(R.string.you_upvoted_this));
            ll_downvote.setSelected(false);
            ll_upvote.setSelected(true);
        }

        if (answerLists.getIs_post_answer_downvote().equalsIgnoreCase("true")) {
            txt_your_vote.setText(getString(R.string.You_downvoted_this));
            ll_downvote.setSelected(true);
            ll_upvote.setSelected(false);
        }

        if (answerLists.getIs_post_answer_upvote().equalsIgnoreCase("false") &&
                answerLists.getIs_post_answer_downvote().equalsIgnoreCase("false")) {

            img_voter_pic.setVisibility(View.INVISIBLE);
            txt_your_vote.setVisibility(View.INVISIBLE);
        }

        if (answerLists.getIs_post_answer_bookmark().equalsIgnoreCase("false")) {
            txt_bookmark.setText(getString(R.string.bookmark));
            txt_bookmark.setSelected(false);
        } else {
            txt_bookmark.setText(getString(R.string.bookmarked));
            txt_bookmark.setSelected(true);
        }

        rv_all_images.setAdapter(new ArticleImagesAdapter(this, answerLists.getAnswerImg()));

        if (answerLists.getAnswerImg().size() == 1) {
            iv_single_image.setVisibility(View.VISIBLE);
            rv_all_images.setVisibility(View.GONE);
            ImageLoadUtils.imageLoad(this,
                    iv_single_image,
                    answerLists.getAnswerImg().get(0).getImage_name(),
                    R.drawable.upload_img_ph);
        } else {
            iv_single_image.setVisibility(View.GONE);
        }

        answerCommentsListAdapter = new AnswerCommentsListAdapter(AnswerDetailsActivity.this, answerLists.getComments(), postId, new RecyclerClickListener() {
            @Override
            public void onItemClick(int pos, String tag) {
                if (tag.equalsIgnoreCase("edit")) {
                    commentId = answerLists.getComments().get(pos).comment_id;
                    edt_comment.setText(answerLists.getComments().get(pos).comment);
                    edt_comment.requestFocus();

                } else if (tag.equalsIgnoreCase("profile")) {
                    if (AppClass.preferences.getUserId().equalsIgnoreCase(answerLists.getComments().get(pos).getUser_id())) {
                        startActivity(new Intent(AnswerDetailsActivity.this, ProfileActivity.class));
                    } else {
                        startActivity(new Intent(AnswerDetailsActivity.this, OtherUserProfileActivity.class)
                                .putExtra("user_id", answerLists.getComments().get(pos).getUser_id()));
                    }

                }
            }
        });


        rv_comments_list.setAdapter(answerCommentsListAdapter);

        textCrawler = new TextCrawler();

        if (answerLists.getAnswerLink() != null &&
                !StringUtils.isNotEmpty(answerLists.getAnswerLink().getLink_title())) {
            addLink(answerLists.getAnswerLink().getLink());
        } else {

            View linkPreview = getLayoutInflater().inflate(R.layout.layout_link_preview, null);
            dropPreview.removeAllViews();
            dropPreview.addView(linkPreview);
            AppCompatTextView tvLinkPreviewTitle = linkPreview.findViewById(R.id.tv_linkpreview_title);
            AppCompatTextView tvLinkPreviewDescription = linkPreview.findViewById(R.id.tv_linkpreview_description);
            AppCompatTextView tvLinkPreviewLink = linkPreview.findViewById(R.id.tv_link);
            AppCompatImageView ivLinkPreviewImage = linkPreview.findViewById(R.id.iv_url_image);
            AppCompatImageView ivLinkPreviewClose = linkPreview.findViewById(R.id.iv_close);

            tvLinkPreviewTitle.setText(answerLists.getAnswerLink().getLink_title());
            tvLinkPreviewDescription.setText(answerLists.getAnswerLink().getLink_description());
            tvLinkPreviewLink.setText(answerLists.getAnswerLink().getLink());

            ivLinkPreviewClose.setVisibility(View.GONE);

            if (StringUtils.isNotEmpty(answerLists.getAnswerLink().getLink_image())) {

                ImageLoadUtils.imageLoad(this,
                        ivLinkPreviewImage,
                        answerLists.getAnswerLink().getLink_image());

            }
        }
        isAnswerEdited = false;

        if (txt_answer_data.getLineCount() < 6) {
            tv_see_more.setVisibility(View.GONE);
        }

        /*if (answerLists.getIs_post_answer_liked().equalsIgnoreCase("true")) {
            ll_upvote.setSelected(true);
        } else {
            ll_upvote.setSelected(false);
        }*/

    }

    private void increaseDrawableSizes() {
        txt_view_count.setCompoundDrawables(getDrawableAndResize(R.drawable.viewed_icon), null, null, null);
        txt_comment_count.setCompoundDrawables(getDrawableAndResize(R.drawable.comments), null, null, null);
        txt_upvote.setCompoundDrawables(getDrawableAndResize(R.drawable.selector_upvote), null, null, null);
        txt_downvote.setCompoundDrawables(getDrawableAndResize(R.drawable.selector_downvote), null, null, null);
        txt_share.setCompoundDrawables(getDrawableAndResize(R.drawable.share_blue), null, null, null);
    }

    private Drawable getDrawableAndResize(int drawableId) {
        Drawable drawable = ContextCompat.getDrawable(this, drawableId);
        drawable.setBounds(0,
                0,
                (int) (drawable.getIntrinsicWidth() * 1.4),
                (int) (drawable.getIntrinsicHeight() * 1.4));

        return drawable;
    }

    private void initView() {
        img_close = findViewById(R.id.img_close);
        img_option = findViewById(R.id.img_option);
        img_follow = findViewById(R.id.img_follow);
        img_send = findViewById(R.id.img_send);
        img_emoji = findViewById(R.id.img_emoji);

        img_user_pic = findViewById(R.id.img_user_pic);
        img_voter_pic = findViewById(R.id.img_voter_pic);
        iv_single_image = findViewById(R.id.iv_single_image);

        edt_comment = findViewById(R.id.edt_comment);

        txt_person_name = findViewById(R.id.txt_person_name);
        txt_person_organisation = findViewById(R.id.txt_person_organisation);
        txt_time = findViewById(R.id.txt_time);
        txt_answer_data = findViewById(R.id.txt_answer_data);
        txt_your_vote = findViewById(R.id.txt_your_vote);
        txt_comment_count = findViewById(R.id.txt_comment_count);
        txt_view_count = findViewById(R.id.txt_view_count);
        txt_upvote = findViewById(R.id.txt_upvote);
        txt_upvote_count = findViewById(R.id.txt_upvote_count);
        txt_downvote = findViewById(R.id.txt_downvote);
        txt_downvote_count = findViewById(R.id.txt_downvote_count);
        txt_bookmark = findViewById(R.id.txt_bookmark);
        txt_share = findViewById(R.id.txt_share);
        tv_see_more = findViewById(R.id.tv_see_more);

        ll_upvote = findViewById(R.id.ll_upvote);
        ll_downvote = findViewById(R.id.ll_downvote);
        ll_bookmark = findViewById(R.id.ll_bookmark);
        ll_share = findViewById(R.id.ll_share);
        dropPreview = findViewById(R.id.drop_preview);

        rv_all_images = findViewById(R.id.rv_all_images);
        rv_comments_list = findViewById(R.id.rv_comments_list);
        LinearLayoutManager layoutManager = new LinearLayoutManager(AnswerDetailsActivity.this);
        rv_comments_list.setLayoutManager(layoutManager);
        rv_comments_list.setNestedScrollingEnabled(false);


        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 3);
        rv_all_images.setLayoutManager(gridLayoutManager);


    }

    private void initStyle() {
        txt_person_name.setTypeface(AppClass.lato_bold);
        txt_person_organisation.setTypeface(AppClass.lato_regular);
        txt_time.setTypeface(AppClass.lato_regular);
        txt_answer_data.setTypeface(AppClass.lato_medium);
        txt_your_vote.setTypeface(AppClass.lato_regular);
        txt_comment_count.setTypeface(AppClass.lato_regular);
        txt_view_count.setTypeface(AppClass.lato_regular);
        txt_upvote.setTypeface(AppClass.lato_regular);
        txt_downvote.setTypeface(AppClass.lato_regular);
        txt_bookmark.setTypeface(AppClass.lato_regular);
        txt_share.setTypeface(AppClass.lato_regular);
        edt_comment.setTypeface(AppClass.lato_medium);
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initListener() {
        ll_upvote.setOnClickListener(this);
        ll_downvote.setOnClickListener(this);
        ll_bookmark.setOnClickListener(this);
        ll_share.setOnClickListener(this);
        img_close.setOnClickListener(this);
        img_send.setOnClickListener(this);
        img_follow.setOnClickListener(this);
        img_option.setOnClickListener(this);
        dropPreview.setOnClickListener(this);
        tv_see_more.setOnClickListener(this);

        edt_comment.setOnTouchListener((v, event) -> {
            if (edt_comment.hasFocus()) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                if ((event.getAction() & MotionEvent.ACTION_MASK) == MotionEvent.ACTION_SCROLL) {
                    v.getParent().requestDisallowInterceptTouchEvent(false);
                    return true;
                }
            }
            return false;
        });

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.ll_upvote:

                img_voter_pic.setVisibility(View.VISIBLE);
                txt_your_vote.setVisibility(View.VISIBLE);

//                likeUnlikePostAnswer(ll_upvote.isSelected() ? "0" : "1");

                callAPIPostUpVoteDownVote("1");

                break;

            case R.id.ll_downvote:

                img_voter_pic.setVisibility(View.VISIBLE);
                txt_your_vote.setVisibility(View.VISIBLE);

                callAPIPostUpVoteDownVote("2");

                break;

            case R.id.ll_bookmark:

                if (answerLists.getIs_post_answer_bookmark().equalsIgnoreCase("true")) {
                    callAPIBookmarkPostAnswer("0");
                } else {
                    callAPIBookmarkPostAnswer("1");
                }

                break;

            case R.id.ll_share:
                sharePost();
                break;

            case R.id.img_close:
                onBackPressed();
                break;

            case R.id.img_send:

                KeyBoardUtils.closeSoftKeyboard(AnswerDetailsActivity.this);

                if (edt_comment.getText().toString().trim().length() > 0) {
                    if (!StringUtils.isNotEmpty(commentId))
                        callAPIAddPostAnswerComment(answerLists.getPostAnswersId());
                    else
                        editComment();
                }

                break;

            case R.id.img_follow:

                KeyBoardUtils.closeSoftKeyboard(AnswerDetailsActivity.this);
                callAPIChangeFollowUnfollowStatus(img_follow.isSelected() ? "0" : "1", answerLists.getUser().getUserId());
                break;

            case R.id.img_option:
                KeyBoardUtils.closeSoftKeyboard(AnswerDetailsActivity.this);
                if (AppClass.preferences.getUserId().equalsIgnoreCase(answerLists.getUser().getUserId())) {

                    new AnswerEditDeleteDialog(this, new AnswerEditDeleteDialog.OnItemClick() {
                        @Override
                        public void onClick(String tag) {
                            if (tag.equalsIgnoreCase("delete")) {
                                deleteAnswer();
                            } else {
                                startActivity(new Intent(AnswerDetailsActivity.this, PostAnswerActivity.class)
                                        .putExtra("questionDetails", getIntent().getSerializableExtra("questionDetails"))
                                        .putExtra("openfor", "edit")
                                        .putExtra("postAnswer", answerLists));
                            }

                        }
                    }).show();

                }
                break;


            case R.id.drop_preview:
                if (answerLists.getAnswerLink() != null && StringUtils.isNotEmpty(answerLists.getAnswerLink().getLink())) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(answerLists.getAnswerLink().getLink()));
                    startActivity(browserIntent);
                }
                break;


            case R.id.tv_see_more:
                if (txt_answer_data.getMaxLines() == 6) {
                    tv_see_more.setText(getString(R.string.read_less));
                    txt_answer_data.setMaxLines(Integer.MAX_VALUE);
                } else {
                    tv_see_more.setText(R.string.read_more);
                    txt_answer_data.setMaxLines(6);
                }
                break;
        }
    }

    private void callAPIChangeFollowUnfollowStatus(final String type, String otherUserId) {

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("follower_id", otherUserId);
        request.put("type", type);

        ApiCall.getInstance().changeFollowUnfollowStatus(this, request, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {

                if (type.equalsIgnoreCase("1")) {
                    img_follow.setSelected(true);
                } else {
                    img_follow.setSelected(false);
                }

            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {

            }
        }, true);

    }

    private void callAPIGetAnswerDetails() {

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("post_answers_id", answerId);
        Logger.d("request:" + request);

        ApiCall.getInstance().getAnswerDetails(AnswerDetailsActivity.this,
                request, new IApiCallback() {
                    @Override
                    public void onSuccess(Object data) {

                        JsonObject jsonObject = (JsonObject) data;
                        JsonObject obj = jsonObject.getAsJsonObject("answer_details");
                        answerLists = new Gson().fromJson(obj.toString(), AnswerModel.class);
                        setData();
                    }

                    @Override
                    public void onFailure(boolean isFalseFromService, String errorMessage) {


                        DialogUtils.openDialog(AnswerDetailsActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                            @Override
                            public void onNegativeBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }

                            @Override
                            public void onPositiveBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }
                        });
                    }


                }, true);

    }

    private void callAPIPostRead() {

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("post_id", postId);
        request.put("answer_id", answerId);

        Logger.d("request:" + request);

        ApiCall.getInstance().postRead(AnswerDetailsActivity.this, request, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {

            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {

            }


        }, true);

    }

    private void callAPIPostUpVoteDownVote(String status) {

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("post_id", answerLists.postId);
        request.put("answer_id", answerLists.postAnswersId);
        request.put("status", status);

        upDownVoteStatus = status;

        Logger.d("request:" + request);

        ApiCall.getInstance().postUpVoteDownVote(AnswerDetailsActivity.this,
                request, new IApiCallback() {
                    @Override
                    public void onSuccess(Object data) {
                        if (upDownVoteStatus.equalsIgnoreCase("1")) {
                            ll_downvote.setSelected(false);
                            ll_upvote.setSelected(true);
                            txt_your_vote.setText(getString(R.string.you_upvoted_this));
                            txt_upvote_count.setText(String.valueOf((answerLists.getAnswer_upvote_comment_count() + 1)));
                            answerLists.setAnswer_upvote_comment_count(answerLists.getAnswer_upvote_comment_count() + 1);
                            txt_upvote.setText(getResources().getQuantityString(R.plurals.upvote, answerLists.getAnswer_upvote_comment_count(), answerLists.getAnswer_upvote_comment_count()));

                            if (answerLists.getAnswer_downvote_comment_count() > 0) {

                                String downVoteCount = String.valueOf((answerLists.getAnswer_downvote_comment_count() - 1));

                                if (downVoteCount.equalsIgnoreCase("0")) {
                                    txt_downvote_count.setText("");
                                } else {
                                    txt_downvote_count.setText(downVoteCount);
                                }
                                answerLists.setAnswer_downvote_comment_count(answerLists.getAnswer_downvote_comment_count() - 1);
                                txt_downvote.setText(getResources().getQuantityString(R.plurals.downvote, answerLists.getAnswer_downvote_comment_count(), answerLists.getAnswer_downvote_comment_count()));

                            }

                        } else {
                            ll_downvote.setSelected(true);
                            ll_upvote.setSelected(false);
                            txt_your_vote.setText(getString(R.string.You_downvoted_this));
                            if (answerLists.getAnswer_upvote_comment_count() > 0) {

                                String upVoteCount = String.valueOf((answerLists.getAnswer_upvote_comment_count() - 1));
                                if (upVoteCount.equalsIgnoreCase("0")) {
                                    txt_upvote_count.setText("");
                                } else {
                                    txt_upvote_count.setText(upVoteCount);
                                }

                                answerLists.setAnswer_upvote_comment_count(answerLists.getAnswer_upvote_comment_count() - 1);
                                txt_upvote.setText(getResources().getQuantityString(R.plurals.upvote, answerLists.getAnswer_upvote_comment_count(), answerLists.getAnswer_upvote_comment_count()));

                            }

                            txt_downvote_count.setText(String.valueOf((answerLists.getAnswer_downvote_comment_count() + 1)));
                            answerLists.setAnswer_downvote_comment_count((answerLists.getAnswer_downvote_comment_count() + 1));
                            txt_downvote.setText(getResources().getQuantityString(R.plurals.downvote, answerLists.getAnswer_downvote_comment_count(), answerLists.getAnswer_downvote_comment_count()));

                        }
                    }

                    @Override
                    public void onFailure(boolean isFalseFromService, String errorMessage) {

                    }


                }, true);

    }

    private void callAPIBookmarkPostAnswer(final String status) {

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("post_id", answerLists.getPostId());
        request.put("answer_id", answerLists.getPostAnswersId());
        request.put("bookmark_unbookmark", status);

        ApiCall.getInstance().bookmarkUnbookmarkPostAnswer(AnswerDetailsActivity.this, request, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {
                String response = (String) data;

                if (status.equalsIgnoreCase("0")) {
                    txt_bookmark.setText("Bookmark");
                    txt_bookmark.setSelected(false);
                    answerLists.setIs_post_answer_bookmark("false");

                } else {
                    txt_bookmark.setText("Bookmarked");
                    txt_bookmark.setSelected(true);
                    answerLists.setIs_post_answer_bookmark("true");
                }


            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {
                DialogUtils.openDialog(AnswerDetailsActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                    @Override
                    public void onNegativeBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }

                    @Override
                    public void onPositiveBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }
                });
            }
        }, true);


    }

    private void callAPIAddPostAnswerReply(String answer_id, String comment, String comment_id) {
        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("answer_id", answer_id);
        request.put("comment", comment);
        request.put("reply_id", comment_id);

        Logger.d("request:" + request);

        ApiCall.getInstance().addPostAnswerComment(AnswerDetailsActivity.this,
                request, new IApiCallback() {
                    @Override
                    public void onSuccess(Object data) {

                        //edt_comment.setText("");
                        callAPIGetAnswerDetails();


                    }

                    @Override
                    public void onFailure(boolean isFalseFromService, String errorMessage) {


                        DialogUtils.openDialog(AnswerDetailsActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                            @Override
                            public void onNegativeBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }

                            @Override
                            public void onPositiveBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }
                        });
                    }


                }, true);
    }

    private void callAPIAddPostAnswerComment(String answer_id) {

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("answer_id", answer_id);
        request.put("comment", edt_comment.getText().toString().trim());

        Logger.d("request:" + request);

        ApiCall.getInstance().addPostAnswerComment(AnswerDetailsActivity.this,
                request, new IApiCallback() {
                    @Override
                    public void onSuccess(Object data) {

                        edt_comment.setText("");
                        callAPIGetAnswerDetails();


                    }

                    @Override
                    public void onFailure(boolean isFalseFromService, String errorMessage) {


                        DialogUtils.openDialog(AnswerDetailsActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                            @Override
                            public void onNegativeBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }

                            @Override
                            public void onPositiveBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }
                        });
                    }


                }, true);

    }

    private void editComment() {
        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("comment_id", commentId);
        request.put("answer_id", answerLists.postAnswersId);
        request.put("comment", edt_comment.getText().toString().trim());

        ApiCall.getInstance().editPostAnswerComment(AnswerDetailsActivity.this, request, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {
                commentId = "";
                edt_comment.setText("");
                callAPIGetAnswerDetails();

            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {

                AppClass.snackBarView.snackBarShow(AnswerDetailsActivity.this, errorMessage);

            }
        }, true);

    }

    private void deleteAnswer() {

        ApiCall.getInstance().deleteAnswer(AnswerDetailsActivity.this, AppClass.preferences.getUserId(), answerLists.postAnswersId, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {


                finish();

            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {

            }
        }, true);

    }


    private void addLink(String link) {
        if (StringUtils.isNotEmpty(link))
            textCrawler.makePreview(this, link.startsWith("http") ? link : "https://" + link);

    }

    private void likeUnlikePostAnswer(String likeUnLike) {
        if (likeUnLike.equalsIgnoreCase("1")) {
            ll_upvote.setSelected(true);
            answerLists.setPost_answer_like_count("" + (Integer.parseInt(answerLists.getPost_answer_like_count()) + 1));
        } else {
            ll_upvote.setSelected(false);
            answerLists.setPost_answer_like_count("" + (Integer.parseInt(answerLists.getPost_answer_like_count()) - 1));
        }
        txt_upvote_count.setText(answerLists.getPost_answer_like_count());
        ApiCall.getInstance().likeUnlikePostAnswer(AnswerDetailsActivity.this,
                AppClass.preferences.getUserId(),
                answerLists.getPostId(),
                answerLists.postAnswersId, likeUnLike, new IApiCallback() {
                    @Override
                    public void onSuccess(Object data) {

                    }

                    @Override
                    public void onFailure(boolean isFalseFromService, String errorMessage) {

                    }
                }, false);


    }

    @Override
    public void onPre() {
        KeyBoardUtils.closeSoftKeyboard(AnswerDetailsActivity.this);

        currentImageSet = null;
        currentTitle = currentDescription = currentUrl = currentCannonicalUrl = "";


        View mainView = getLayoutInflater().inflate(R.layout.link_preview_main_view, null);

        linearLayout = mainView.findViewById(R.id.external);

        getLayoutInflater().inflate(R.layout.loading, linearLayout);

        dropPreview.addView(mainView);
    }

    @Override
    public void onPos(SourceContent sourceContent, boolean b) {
        linearLayout.removeAllViews();

        if (b || sourceContent.getFinalUrl().equals("")) {

            View failed = getLayoutInflater().inflate(R.layout.failed, linearLayout);

            TextView titleTextView = failed.findViewById(R.id.text);
            titleTextView.setText(String.format("%s\n%s", getString(R.string.failed_preview), sourceContent.getFinalUrl()));

            failed.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
//                    dropPreview.removeAllViews();
                    currentTitle = currentDescription = currentUrl = currentCannonicalUrl = "";
                }
            });

        } else {

            currentImageSet = new Bitmap[sourceContent.getImages().size()];

            View linkPreview = getLayoutInflater().inflate(R.layout.layout_link_preview, linearLayout);

            AppCompatTextView tvLinkPreviewTitle = linkPreview.findViewById(R.id.tv_linkpreview_title);
            AppCompatTextView tvLinkPreviewDescription = linkPreview.findViewById(R.id.tv_linkpreview_description);
            AppCompatTextView tvLinkPreviewLink = linkPreview.findViewById(R.id.tv_link);
            AppCompatImageView ivLinkPreviewImage = linkPreview.findViewById(R.id.iv_url_image);
            AppCompatImageView ivLinkPreviewClose = linkPreview.findViewById(R.id.iv_close);

            tvLinkPreviewTitle.setText(sourceContent.getTitle());
            tvLinkPreviewDescription.setText(sourceContent.getDescription());
            tvLinkPreviewLink.setText(sourceContent.getCannonicalUrl());

            ivLinkPreviewClose.setVisibility(View.GONE);

            if (sourceContent.getImages().size() > 0) {

                ImageLoadUtils.imageLoad(AnswerDetailsActivity.this,
                        ivLinkPreviewImage,
                        sourceContent.getImages().get(0));

            }

        }


        currentTitle = sourceContent.getTitle();
        currentDescription = sourceContent.getDescription();
        currentUrl = sourceContent.getUrl();
        currentCannonicalUrl = sourceContent.getCannonicalUrl();
    }

    private void sharePost() {

        String shareBody = answerLists.getAnswer() + getString(R.string.play_store_app_url);
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(sharingIntent, getString(R.string.share_using)));

    }

}
