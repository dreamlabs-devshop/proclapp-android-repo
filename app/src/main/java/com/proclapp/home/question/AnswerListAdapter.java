package com.proclapp.home.question;

import android.app.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.home.AnswerEditDeleteDialog;
import com.proclapp.interfaces.RecyclerClickListener;
import com.proclapp.model.AnswerModel;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.DateTimeUtils;
import com.proclapp.utils.ImageLoadUtils;
import com.proclapp.utils.KeyBoardUtils;
import com.proclapp.utils.Logger;
import com.proclapp.utils.SquareImageView;

import java.util.ArrayList;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class AnswerListAdapter extends RecyclerView.Adapter<AnswerListAdapter.AnswerViewHolder> {

    private Activity activity;
    private String openFor;
    private ArrayList<AnswerModel> answerLists;
    private RecyclerClickListener clickListener;

    public AnswerListAdapter(Activity activity, String openFor, ArrayList<AnswerModel> answerLists, RecyclerClickListener clickListener) {
        this.activity = activity;
        this.openFor = openFor;
        this.answerLists = answerLists;
        this.clickListener = clickListener;

    }

    @NonNull
    @Override
    public AnswerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_answer_list_new, parent, false);
        return new AnswerViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull final AnswerViewHolder holder, final int position) {

        try {

            if (AppClass.preferences.getUserId().equalsIgnoreCase(answerLists.get(holder.getAdapterPosition()).getUser().getUserId())) {
                holder.optionImage.setVisibility(View.VISIBLE);
            } else {
                holder.optionImage.setVisibility(View.INVISIBLE);
            }

            if (answerLists.get(holder.getAdapterPosition()).getAnswerImg().size() == 1) {
                holder.singleImageView.setVisibility(View.VISIBLE);
                ImageLoadUtils.imageLoad(activity,
                        holder.singleImageView,
                        answerLists.get(holder.getAdapterPosition()).getAnswerImg().get(0).getImage_name(),
                        R.drawable.upload_img_ph);
            } else {
                holder.singleImageView.setVisibility(View.GONE);
            }
            if (answerLists.get(holder.getAdapterPosition()).getAnswer_upvote_comment_count() > 0) {
                holder.upVoteCountTextView.setText(String.valueOf(answerLists.get(holder.getAdapterPosition()).getAnswer_upvote_comment_count()));
                holder.upVoteTextView.setText(activity.getResources().getQuantityString(R.plurals.upvote, answerLists.get(holder.getAdapterPosition()).getAnswer_upvote_comment_count(), answerLists.get(holder.getAdapterPosition()).getAnswer_upvote_comment_count()));

            }
            if (answerLists.get(holder.getAdapterPosition()).getAnswer_downvote_comment_count() > 0) {
                holder.downVoteCountTextView.setText(String.valueOf(answerLists.get(holder.getAdapterPosition()).getAnswer_downvote_comment_count()));
                holder.downVoteTextView.setText(activity.getResources().getQuantityString(R.plurals.downvote, answerLists.get(holder.getAdapterPosition()).getAnswer_downvote_comment_count(), answerLists.get(holder.getAdapterPosition()).getAnswer_downvote_comment_count()));

            }
            holder.answerTextView.setText(answerLists.get(holder.getAdapterPosition()).answer);
            holder.nameTextView.setText(answerLists.get(holder.getAdapterPosition()).getUser().name);
            holder.organisationTextView.setText(answerLists.get(holder.getAdapterPosition()).getUser().professionalQualification);
            holder.commentCountTextView.setText(answerLists.get(holder.getAdapterPosition()).getAnswer_comment_count());
            holder.dateTextView.setText(DateTimeUtils.changeDateTimeFormat("yyyy-MM-dd HH:mm:ss",
                    "dd MMM, HH:mm a",
                    answerLists.get(holder.getAdapterPosition()).date));
            ImageLoadUtils.imageLoad(activity,
                    holder.userImageView,
                    answerLists.get(holder.getAdapterPosition()).getUser().profileImage,
                    R.drawable.menu_user_ph);

            ImageLoadUtils.imageLoad(activity,
                    holder.voterImagePic,
                    AppClass.preferences.getProfileImage(),
                    R.drawable.menu_user_ph);


            if (answerLists.get(holder.getAdapterPosition()).getIs_post_answer_upvote().equalsIgnoreCase("true")) {
                holder.yourVoteTextView.setText(activity.getString(R.string.you_upvoted_this));
                holder.downVoteLayout.setSelected(false);
                holder.upVoteLayout.setSelected(true);
            }

            if (answerLists.get(holder.getAdapterPosition()).getIs_post_answer_downvote().equalsIgnoreCase("true")) {
                holder.yourVoteTextView.setText(activity.getString(R.string.You_downvoted_this));
                holder.downVoteLayout.setSelected(true);
                holder.upVoteLayout.setSelected(false);
            }

            if (answerLists.get(holder.getAdapterPosition()).getIs_post_answer_upvote().equalsIgnoreCase("false") &&
                    answerLists.get(holder.getAdapterPosition()).getIs_post_answer_downvote().equalsIgnoreCase("false")) {

                holder.voterImagePic.setVisibility(View.INVISIBLE);
                holder.yourVoteTextView.setVisibility(View.INVISIBLE);
            }


            holder.itemView.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            clickListener.onItemClick(holder.getAdapterPosition(), "see_answer");
                        }
                    });

            holder.answerTextView.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            clickListener.onItemClick(holder.getAdapterPosition(), "see_answer");

                        }
                    }
            );

            holder.upVoteLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    holder.voterImagePic.setVisibility(View.VISIBLE);
                    holder.yourVoteTextView.setVisibility(View.VISIBLE);

                    callAPIPostUpVoteDownVote("1", holder);
                }
            });

            holder.downVoteLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    holder.voterImagePic.setVisibility(View.VISIBLE);
                    holder.voterImagePic.setVisibility(View.VISIBLE);

                    callAPIPostUpVoteDownVote("2", holder);
                }
            });

            holder.shareLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    String shareBody = answerLists.get(holder.getAdapterPosition()).getAnswer() + activity.getString(R.string.play_store_app_url);
                    Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                    sharingIntent.setType("text/plain");
                    sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
                    activity.startActivity(Intent.createChooser(sharingIntent, activity.getString(R.string.share_using)));

                }
            });

            String answerData = holder.answerTextView.getText().toString();
            int lineCount = answerData.length();
            String message = String.valueOf(lineCount);
            Log.d("line count: ", message);

            holder.seeMoreTextView.setVisibility(lineCount > 300 ? View.VISIBLE : View.GONE);
            holder.seeMoreTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (holder.answerTextView.getMaxLines() == 6) {
                        holder.seeMoreTextView.setText(activity.getString(R.string.read_less));
                        holder.answerTextView.setMaxLines(Integer.MAX_VALUE);
                    } else {
                        holder.seeMoreTextView.setText(R.string.read_more);
                        holder.answerTextView.setMaxLines(6);
                    }
                }
            });

            holder.commentCountTextView.setCompoundDrawables(getDrawableAndResize(R.drawable.comments), null, null, null);
            holder.upVoteTextView.setCompoundDrawables(getDrawableAndResize(R.drawable.selector_upvote), null, null, null);
            holder.downVoteTextView.setCompoundDrawables(getDrawableAndResize(R.drawable.selector_downvote), null, null, null);
            holder.shareTextView.setCompoundDrawables(getDrawableAndResize(R.drawable.share_blue), null, null, null);


            //holder.swipe_to_delete.setRightSwipeEnabled(false);

//            if (openFor.equalsIgnoreCase("qa") ||
//                    openFor.equalsIgnoreCase("Question")) {
//                holder.txt_see_answer.setText(activity.getString(R.string.see_answer));
//            } else {
//                holder.txt_see_answer.setText(activity.getString(R.string.see_comment));
//            }

            holder.optionImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    KeyBoardUtils.closeSoftKeyboard(activity);
                    if (AppClass.preferences.getUserId().equalsIgnoreCase(answerLists.get(holder.getAdapterPosition()).getUser().getUserId())) {

                        new AnswerEditDeleteDialog(activity, new AnswerEditDeleteDialog.OnItemClick() {
                            @Override
                            public void onClick(String tag) {

                                if (tag.equalsIgnoreCase("delete")) {
                                    deleteAnswer(holder);
                                } else {
                                    clickListener.onItemClick(holder.getAdapterPosition(), "edit");
                                }

                            }
                        }).show();

                    }
                }
            });

//            holder.txt_person_name.setText(answerLists.get(holder.getAdapterPosition()).getUser().name);
//            holder.txt_person_organisation.setText(answerLists.get(holder.getAdapterPosition()).getUser().professionalQualification);
//
//            ImageLoadUtils.imageLoad(activity,
//                    holder.img_user_pic,
//                    answerLists.get(holder.getAdapterPosition()).getUser().profileImage,
//                    R.drawable.menu_user_ph);
//
//            holder.txt_see_answer.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    clickListener.onItemClick(holder.getAdapterPosition(), "see_answer");
//                }
//            });
//
//
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Drawable getDrawableAndResize(int drawableId) {
        Drawable drawable = ContextCompat.getDrawable(activity, drawableId);
        drawable.setBounds(0,
                0,
                (int) (drawable.getIntrinsicWidth() * 1.4),
                (int) (drawable.getIntrinsicHeight() * 1.4));

        return drawable;
    }

    @Override
    public int getItemCount() {
        return answerLists.size();
    }

    private void deleteAnswer(final AnswerViewHolder holder) {

        ApiCall.getInstance().deleteAnswer(activity, AppClass.preferences.getUserId(), answerLists.get(holder.getAdapterPosition()).postAnswersId, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {

                answerLists.remove(holder.getAdapterPosition());
                notifyDataSetChanged();

                clickListener.onItemClick(holder.getAdapterPosition(), "delete");

            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {

            }
        }, true);

    }

    private void callAPIPostUpVoteDownVote(String status, AnswerViewHolder holder) {

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("post_id", answerLists.get(holder.getAdapterPosition()).postId);
        request.put("answer_id", answerLists.get(holder.getAdapterPosition()).postAnswersId);
        request.put("status", status);

        Logger.d("request:" + request);

        ApiCall.getInstance().postUpVoteDownVote(activity,
                request, new IApiCallback() {
                    @Override
                    public void onSuccess(Object data) {
                        if (status.equalsIgnoreCase("1")) {
                            holder.downVoteLayout.setSelected(false);
                            holder.upVoteLayout.setSelected(true);
                            holder.yourVoteTextView.setText(activity.getString(R.string.you_upvoted_this));
                            holder.upVoteCountTextView.setText(String.valueOf((answerLists.get(holder.getAdapterPosition()).getAnswer_upvote_comment_count() + 1)));
                            answerLists.get(holder.getAdapterPosition()).setAnswer_upvote_comment_count(answerLists.get(holder.getAdapterPosition()).getAnswer_upvote_comment_count() + 1);
                            holder.upVoteTextView.setText(activity.getResources().getQuantityString(R.plurals.upvote, answerLists.get(holder.getAdapterPosition()).getAnswer_upvote_comment_count(), answerLists.get(holder.getAdapterPosition()).getAnswer_upvote_comment_count()));

                            if (answerLists.get(holder.getAdapterPosition()).getAnswer_downvote_comment_count() > 0) {

                                String downVoteCount = String.valueOf((answerLists.get(holder.getAdapterPosition()).getAnswer_downvote_comment_count() - 1));

                                if (downVoteCount.equalsIgnoreCase("0")) {
                                    holder.downVoteCountTextView.setText("");
                                } else {
                                    holder.downVoteCountTextView.setText(downVoteCount);
                                }
                                answerLists.get(holder.getAdapterPosition()).setAnswer_downvote_comment_count(answerLists.get(holder.getAdapterPosition()).getAnswer_downvote_comment_count() - 1);
                                holder.downVoteTextView.setText(activity.getResources().getQuantityString(R.plurals.downvote, answerLists.get(holder.getAdapterPosition()).getAnswer_downvote_comment_count(), answerLists.get(holder.getAdapterPosition()).getAnswer_downvote_comment_count()));

                            }

                        } else {
                            holder.downVoteLayout.setSelected(true);
                            holder.upVoteLayout.setSelected(false);
                            holder.yourVoteTextView.setText(activity.getString(R.string.You_downvoted_this));
                            if (answerLists.get(holder.getAdapterPosition()).getAnswer_upvote_comment_count() > 0) {

                                String upVoteCount = String.valueOf((answerLists.get(holder.getAdapterPosition()).getAnswer_upvote_comment_count() - 1));
                                if (upVoteCount.equalsIgnoreCase("0")) {
                                    holder.upVoteCountTextView.setText("");
                                } else {
                                    holder.upVoteCountTextView.setText(upVoteCount);
                                }

                                answerLists.get(holder.getAdapterPosition()).setAnswer_upvote_comment_count(answerLists.get(holder.getAdapterPosition()).getAnswer_upvote_comment_count() - 1);
                                holder.upVoteTextView.setText(activity.getResources().getQuantityString(R.plurals.upvote, answerLists.get(holder.getAdapterPosition()).getAnswer_upvote_comment_count(), answerLists.get(holder.getAdapterPosition()).getAnswer_upvote_comment_count()));

                            }

                            holder.downVoteCountTextView.setText(String.valueOf((answerLists.get(holder.getAdapterPosition()).getAnswer_downvote_comment_count() + 1)));
                            answerLists.get(holder.getAdapterPosition()).setAnswer_downvote_comment_count((answerLists.get(holder.getAdapterPosition()).getAnswer_downvote_comment_count() + 1));
                            holder.downVoteTextView.setText(activity.getResources().getQuantityString(R.plurals.downvote, answerLists.get(holder.getAdapterPosition()).getAnswer_downvote_comment_count(), answerLists.get(holder.getAdapterPosition()).getAnswer_downvote_comment_count()));

                        }
                    }

                    @Override
                    public void onFailure(boolean isFalseFromService, String errorMessage) {

                    }


                }, true);

    }

    public static class AnswerViewHolder extends RecyclerView.ViewHolder {

        TextView
                nameTextView,
                organisationTextView,
                dateTextView,
                commentCountTextView,
                seeMoreTextView,
                upVoteTextView,
                upVoteCountTextView,
                downVoteTextView,
                downVoteCountTextView,
                answerTextView,
                yourVoteTextView,
                shareTextView;
        CircleImageView userImageView, voterImagePic;
        ImageView optionImage;
        SquareImageView singleImageView;
        LinearLayout upVoteLayout, downVoteLayout, shareLayout;

        public AnswerViewHolder(@NonNull View itemView) {
            super(itemView);
            nameTextView = itemView.findViewById(R.id.txt_person_name_1);
            organisationTextView = itemView.findViewById(R.id.txt_person_organisation_1);
            dateTextView = itemView.findViewById(R.id.txt_time_1);
            answerTextView = itemView.findViewById(R.id.txt_answer_data_1);
            userImageView = itemView.findViewById(R.id.img_user_pic_1);
            optionImage = itemView.findViewById(R.id.img_option_1);
            commentCountTextView = itemView.findViewById(R.id.txt_comment_count_1);
            singleImageView = itemView.findViewById(R.id.iv_single_image_1);
            seeMoreTextView = itemView.findViewById(R.id.tv_see_more_1);
            upVoteTextView = itemView.findViewById(R.id.txt_upvote_1);
            upVoteCountTextView = itemView.findViewById(R.id.txt_upvote_count_1);
            downVoteTextView = itemView.findViewById(R.id.txt_downvote_1);
            downVoteCountTextView = itemView.findViewById(R.id.txt_downvote_count_1);
            yourVoteTextView = itemView.findViewById(R.id.txt_your_vote_1);
            voterImagePic = itemView.findViewById(R.id.img_voter_pic_1);
            upVoteLayout = itemView.findViewById(R.id.ll_upvote_1);
            downVoteLayout = itemView.findViewById(R.id.ll_downvote_1);
            shareLayout = itemView.findViewById(R.id.ll_share_1);
            shareTextView = itemView.findViewById(R.id.txt_share);

        }
    }


//    public class MyViewHolder extends RecyclerView.ViewHolder {
//
//        RelativeLayout view_background;
//        TextView
//                txt_person_name,
//                txt_person_organisation,
//                txt_see_answer;
//        CircleImageView img_user_pic;
//        SwipeLayout swipe_to_delete;
//
//        LinearLayout
//                ll_delete,
//                ll_edit;
//
//        AppCompatImageView img_option;
//
//        public MyViewHolder(View itemView) {
//            super(itemView);
//            txt_person_name = itemView.findViewById(R.id.txt_person_name);
//            txt_person_organisation = itemView.findViewById(R.id.txt_person_organisation);
//            txt_see_answer = itemView.findViewById(R.id.txt_see_answer);
//
//            img_user_pic = itemView.findViewById(R.id.img_user_pic);
//            swipe_to_delete = itemView.findViewById(R.id.swipe_to_delete);
//            view_background = itemView.findViewById(R.id.view_background);
//            ll_delete = itemView.findViewById(R.id.ll_delete);
//            ll_edit = itemView.findViewById(R.id.ll_edit);
//            img_option = itemView.findViewById(R.id.img_option);
//
//            // set typeface
//            txt_person_name.setTypeface(AppClass.lato_semibold);
//            txt_person_organisation.setTypeface(AppClass.lato_regular);
//            txt_see_answer.setTypeface(AppClass.lato_regular);
//
//        }
//    }

}
