package com.proclapp.home.question;

import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.util.Linkify;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.advert.AdvertDetailActivity;
import com.proclapp.home.AddLinkDialog;
import com.proclapp.home.adapter.PostArticleImagesAdapter;
import com.proclapp.home.voice_video.VVExpertReplyActivity;
import com.proclapp.linkpreview.LinkPreviewCallback;
import com.proclapp.linkpreview.SourceContent;
import com.proclapp.linkpreview.TextCrawler;
import com.proclapp.model.AllPostsModel;
import com.proclapp.model.AnswerModel;
import com.proclapp.model.PostAdvUploadModel;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.AlertDialogInterface;
import com.proclapp.utils.DateTimeUtils;
import com.proclapp.utils.DialogUtils;
import com.proclapp.utils.ImageLoadUtils;
import com.proclapp.utils.ImagePickerUtils.ImagePickerBottomSheet;
import com.proclapp.utils.KeyBoardUtils;
import com.proclapp.utils.StringUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.RequestBody;

public class PostAnswerActivity extends AppCompatActivity implements View.OnClickListener, LinkPreviewCallback {

    private ImageView
            img_back,
            img_upload_post,
            img_upload_post_delete;
    private TextView
            txt_post,
            txt_user_name,
            txt_user_place,
            txt_date,
            txt_description,
            txt_question_value;
    private FrameLayout fl_image;
    private EditText edt_write_ur_answer;
    private CircleImageView img_user_profile;
    private AllPostsModel questionDetails;
    private String openFor = "";
    private AppCompatImageView
            img_photos,
            img_link;
    private AnswerModel answerModel;
    private RecyclerView rv_images;
    private ArrayList<PostAdvUploadModel> imagesList = new ArrayList<>();

    private TextCrawler textCrawler;
    private ViewGroup dropPreview;
    private Bitmap[] currentImageSet;
    private int countBigImages = 0;
    private String currentTitle = "", currentUrl = "", currentCannonicalUrl = "", currentDescription = "", currentLinkImage = "";
    private LinearLayout linearLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_answer);
        textCrawler = new TextCrawler();
        questionDetails = (AllPostsModel) getIntent().getSerializableExtra("questionDetails");

        initView();
        initListener();

        setData();

    }

    private void setData() {

        if (questionDetails.getPostType().equalsIgnoreCase("5")) {
            txt_description.setVisibility(View.VISIBLE);
            img_photos.setVisibility(View.GONE);
            img_link.setVisibility(View.GONE);
        } else {
            txt_description.setVisibility(View.GONE);
            img_photos.setVisibility(View.VISIBLE);
            img_link.setVisibility(View.VISIBLE);

        }

        txt_user_place.setText(questionDetails.getProfessionalQualification());
        txt_question_value.setText(questionDetails.getPostTitle());
        txt_user_name.setText(questionDetails.getAuthor());
        txt_date.setText(DateTimeUtils.changeDateTimeFormat("yyyy-MM-dd HH:mm:ss", "dd MMM", questionDetails.getPostDate()));
        txt_description.setText(questionDetails.getPostDescription());
        ImageLoadUtils.imageLoad(this,
                img_user_profile,
                questionDetails.getProfileImage(),
                R.drawable.menu_user_ph);

        if (StringUtils.isNotEmpty(getIntent().getStringExtra("openfor"))) {
            openFor = getIntent().getStringExtra("openfor");
            answerModel = (AnswerModel) getIntent().getSerializableExtra("postAnswer");

            edt_write_ur_answer.setText(answerModel.answer);

            if (StringUtils.isNotEmpty(answerModel.getAnswerLink().getLink()))
                addLink(answerModel.getAnswerLink().getLink());

        }

    }

    private void initView() {
        img_back = findViewById(R.id.img_back);
        img_upload_post = findViewById(R.id.img_upload_post);
        img_upload_post_delete = findViewById(R.id.img_upload_post_delete);
        img_photos = findViewById(R.id.img_photos);
        img_link = findViewById(R.id.img_link);
        img_user_profile = findViewById(R.id.img_user_profile);

        txt_post = findViewById(R.id.txt_post);
        txt_user_name = findViewById(R.id.txt_user_name);
        txt_user_place = findViewById(R.id.txt_user_place);
        txt_date = findViewById(R.id.txt_date);
        txt_question_value = findViewById(R.id.txt_question_value);
        txt_description = findViewById(R.id.txt_description);

        fl_image = findViewById(R.id.fl_image);

        rv_images = findViewById(R.id.rv_images);
        rv_images.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        rv_images.setAdapter(new PostArticleImagesAdapter(this, imagesList));

        dropPreview = findViewById(R.id.drop_preview);

        edt_write_ur_answer = findViewById(R.id.edt_write_ur_answer);
        edt_write_ur_answer.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                Linkify.addLinks(editable, Linkify.WEB_URLS);
            }
        });

    }

    private void initListener() {

        img_back.setOnClickListener(this);
        img_upload_post.setOnClickListener(this);
        img_upload_post_delete.setOnClickListener(this);
        img_photos.setOnClickListener(this);
        img_link.setOnClickListener(this);
        txt_post.setOnClickListener(this);
    }

    private boolean validation() {

        boolean isValid = true;

        if (edt_write_ur_answer.getText().toString().trim().isEmpty()) {
            isValid = false;
            AppClass.snackBarView.snackBarShow(PostAnswerActivity.this, getResources().getString(R.string.vanswer), ContextCompat.getColor(PostAnswerActivity.this, R.color.red));
            edt_write_ur_answer.requestFocus();
        }

        return isValid;
    }

    @Override
    public void onClick(View view) {

        KeyBoardUtils.closeSoftKeyboard(this);

        switch (view.getId()) {

            case R.id.img_back:

                onBackPressed();

                break;

            case R.id.txt_post:

                KeyBoardUtils.closeSoftKeyboard(PostAnswerActivity.this);

                if (validation()) {

                    if (AppClass.isInternetConnectionAvailable()) {
                        if (answerModel != null) {
                            callAPIEditAnswer();
                        } else {
                            if (getIntent().hasExtra("openform")) {
                                addExpertReply();
                            } else
                                callAPIAddAnswer();
                        }
                    } else {
                        AppClass.snackBarView.snackBarShow(PostAnswerActivity.this, getString(R.string.nonetwork));
                    }
                }

                break;

            case R.id.img_upload_post:

                break;

            case R.id.img_upload_post_delete:

                break;

            case R.id.img_photos:
                new ImagePickerBottomSheet(this, false, new ImagePickerBottomSheet.ImagePickListener() {
                    @Override
                    public void onPickImage(File imageFile) {

                        /*Bitmap myBitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath());
                        img_upload_post.setImageBitmap(myBitmap);
                        imagePath = imageFile;
                        fl_image.setVisibility(View.VISIBLE);*/

                        rv_images.setVisibility(View.VISIBLE);
                        PostAdvUploadModel model = new PostAdvUploadModel();
                        model.setImage(imageFile);

                        imagesList.add(model);
                        rv_images.getAdapter().notifyDataSetChanged();

                    }
                }).show(getSupportFragmentManager(), "");
                break;

            case R.id.img_link:
                new AddLinkDialog(this, new AddLinkDialog.OnViewClicked() {
                    @Override
                    public void onClick(String tag, String link) {

                        if (tag.equalsIgnoreCase("ok"))
                            addLink(link);


                    }
                }).show();
                break;
        }
    }

    private void callAPIAddAnswer() {


        HashMap<String, RequestBody> request = new HashMap<>();
        request.put("user_id", ApiCall.getInstance().getRequestBodyOfString(AppClass.preferences.getUserId()));
        request.put("post_id", ApiCall.getInstance().getRequestBodyOfString(questionDetails.getPostId()));
        request.put("answer", ApiCall.getInstance().getRequestBodyOfString(edt_write_ur_answer.getText().toString().trim()));
        request.put("link", ApiCall.getInstance().getRequestBodyOfString(currentUrl));
        request.put("link_title", ApiCall.getInstance().getRequestBodyOfString(currentTitle));
        request.put("link_description", ApiCall.getInstance().getRequestBodyOfString(currentDescription));
        request.put("link_image", ApiCall.getInstance().getRequestBodyOfString(currentLinkImage));

        ArrayList<String> imagesPaths = new ArrayList<>();
        for (int i = 0; i < imagesList.size(); i++) {
            imagesPaths.add(imagesList.get(i).getImage().getAbsolutePath());
        }

        ApiCall.getInstance().addAnswerWithImage(PostAnswerActivity.this, request, imagesPaths, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {

                String response = (String) data;
                if (questionDetails.getPostType().equalsIgnoreCase("2")) {
                    AppClass.snackBarView.snackBarShow(PostAnswerActivity.this, response);
                } else {
                    AppClass.snackBarView.snackBarShow(PostAnswerActivity.this, "Comment added");
                }
                finish();
                AdvertDetailActivity.isUpdated = true;

            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {
                if (StringUtils.isNotEmpty(errorMessage)) {
                    DialogUtils.openDialog(PostAnswerActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                        @Override
                        public void onNegativeBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }

                        @Override
                        public void onPositiveBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }
                    });
                }

            }
        }, true);

    }


    private void callAPIEditAnswer() {

        HashMap<String, RequestBody> request = new HashMap<>();
        request.put("user_id", ApiCall.getInstance().getRequestBodyOfString(AppClass.preferences.getUserId()));
        request.put("post_answers_id", ApiCall.getInstance().getRequestBodyOfString(answerModel.postAnswersId));
        request.put("answer", ApiCall.getInstance().getRequestBodyOfString(edt_write_ur_answer.getText().toString().trim()));
        request.put("link", ApiCall.getInstance().getRequestBodyOfString(currentUrl));
        request.put("link_title", ApiCall.getInstance().getRequestBodyOfString(currentTitle));
        request.put("link_description", ApiCall.getInstance().getRequestBodyOfString(currentDescription));
        request.put("link_image", ApiCall.getInstance().getRequestBodyOfString(currentLinkImage));

        ApiCall.getInstance().editAnswer(PostAnswerActivity.this, request, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {

                String response = (String) data;

                if (questionDetails.getPostType().equalsIgnoreCase("2")) {
                    AppClass.snackBarView.snackBarShow(PostAnswerActivity.this, response);
                } else {
                    AppClass.snackBarView.snackBarShow(PostAnswerActivity.this, "Comment updated");
                }

                finish();
                AdvertDetailActivity.isUpdated = true;
                AnswerDetailsActivity.isAnswerEdited = true;

            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {
                if (StringUtils.isNotEmpty(errorMessage)) {
                    DialogUtils.openDialog(PostAnswerActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                        @Override
                        public void onNegativeBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }

                        @Override
                        public void onPositiveBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }
                    });
                }

            }
        }, true);

    }

    private void addExpertReply() {

        HashMap<String, RequestBody> request = new HashMap<>();
        request.put("user_id", RequestBody.create(MediaType.parse("multipart/form-data"), AppClass.preferences.getUserId()));
        request.put("post_id", RequestBody.create(MediaType.parse("multipart/form-data"), questionDetails.getPostId()));
        request.put("requested_user_id", RequestBody.create(MediaType.parse("multipart/form-data"), questionDetails.getAuthorId()));
        request.put("vv_type", RequestBody.create(MediaType.parse("multipart/form-data"), "1"));
        request.put("type", RequestBody.create(MediaType.parse("multipart/form-data"), "3"));
        request.put("answer", ApiCall.getInstance().getRequestBodyOfString(edt_write_ur_answer.getText().toString().trim()));
        request.put("link", ApiCall.getInstance().getRequestBodyOfString(currentUrl));
        request.put("link_title", ApiCall.getInstance().getRequestBodyOfString(currentTitle));
        request.put("link_description", ApiCall.getInstance().getRequestBodyOfString(currentDescription));
        request.put("link_image", ApiCall.getInstance().getRequestBodyOfString(currentLinkImage));

        ApiCall.getInstance().expertReply(PostAnswerActivity.this, request, "", new IApiCallback() {
            @Override
            public void onSuccess(Object data) {
                VVExpertReplyActivity.isReplyAdd = true;
                AppClass.toastView.ToastShow(PostAnswerActivity.this, "Replied successfully.");
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        onBackPressed();
                    }
                }, 1000);


            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {

                AppClass.snackBarView.snackBarShow(PostAnswerActivity.this, errorMessage);

            }
        }, true);

    }


    private void addLink(String link) {
        if (StringUtils.isNotEmpty(link))
            textCrawler.makePreview(this, link.startsWith("http") ? link : "https://" + link);

        img_link.setVisibility(View.GONE);
    }

    @Override
    public void onPre() {
        KeyBoardUtils.closeSoftKeyboard(PostAnswerActivity.this);

        currentImageSet = null;
        currentTitle = currentDescription = currentUrl = currentCannonicalUrl = currentLinkImage = "";


        View mainView = getLayoutInflater().inflate(R.layout.link_preview_main_view, null);

        linearLayout = mainView.findViewById(R.id.external);

        getLayoutInflater().inflate(R.layout.loading, linearLayout);

        dropPreview.addView(mainView);
    }

    @Override
    public void onPos(SourceContent sourceContent, boolean b) {
        linearLayout.removeAllViews();

        if (b || sourceContent.getFinalUrl().equals("")) {

            View failed = getLayoutInflater().inflate(R.layout.failed, linearLayout);

            TextView titleTextView = failed.findViewById(R.id.text);
            titleTextView.setText(String.format("%s\n%s", getString(R.string.failed_preview), sourceContent.getFinalUrl()));

            failed.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    dropPreview.removeAllViews();
                    currentTitle = currentDescription = currentUrl = currentCannonicalUrl = currentLinkImage = "";
                }
            });

        } else {

            currentImageSet = new Bitmap[sourceContent.getImages().size()];

            View linkPreview = getLayoutInflater().inflate(R.layout.layout_link_preview, linearLayout);

            AppCompatTextView tvLinkPreviewTitle = linkPreview.findViewById(R.id.tv_linkpreview_title);
            AppCompatTextView tvLinkPreviewDescription = linkPreview.findViewById(R.id.tv_linkpreview_description);
            AppCompatTextView tvLinkPreviewLink = linkPreview.findViewById(R.id.tv_link);
            AppCompatImageView ivLinkPreviewImage = linkPreview.findViewById(R.id.iv_url_image);
            AppCompatImageView ivLinkPreviewClose = linkPreview.findViewById(R.id.iv_close);


            tvLinkPreviewTitle.setText(sourceContent.getTitle());
            tvLinkPreviewDescription.setText(sourceContent.getDescription());
            tvLinkPreviewLink.setText(sourceContent.getCannonicalUrl());

            ivLinkPreviewClose.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    dropPreview.removeAllViews();
                    img_link.setVisibility(View.VISIBLE);
                    currentTitle = currentDescription = currentUrl = currentCannonicalUrl = currentLinkImage = "";
                }
            });

            if (sourceContent.getImages().size() > 0) {

                ImageLoadUtils.imageLoad(PostAnswerActivity.this,
                        ivLinkPreviewImage,
                        sourceContent.getImages().get(0));

                currentLinkImage = sourceContent.getImages().get(0);
            }

            currentTitle = sourceContent.getTitle();
            currentDescription = sourceContent.getDescription();
            currentUrl = sourceContent.getUrl();
            currentCannonicalUrl = sourceContent.getCannonicalUrl();

        }


    }


}
