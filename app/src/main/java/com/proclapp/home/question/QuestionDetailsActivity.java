package com.proclapp.home.question;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.Gravity;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.DownloadListener;
import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.home.CarryOverDialog;
import com.proclapp.home.FlashActivity;
import com.proclapp.home.HomePageOptionsDialog;
import com.proclapp.home.HomePageOptionsForReaderDialog;
import com.proclapp.home.RPPostDiscussActivity;
import com.proclapp.interfaces.RecyclerClickListener;
import com.proclapp.model.AllPostsModel;
import com.proclapp.model.AnswerModel;
import com.proclapp.profile.OtherUserProfileActivity;
import com.proclapp.profile.ProfileActivity;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.AlertDialogInterface;
import com.proclapp.utils.DateTimeUtils;
import com.proclapp.utils.DialogUtils;
import com.proclapp.utils.ImageLoadUtils;
import com.proclapp.utils.Logger;
import com.proclapp.utils.StringUtils;
import com.proclapp.utils.Utils;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class QuestionDetailsActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView
            txt_question_technology_innovation,
            txt_user_name,
            txt_user_place,
            txt_flash_count,
            txt_date,
            txt_question_value,
            txt_comments_count,
            txt_share_count,
            txt_like_count,
            txt_answer,
            txt_additional_info,
            txt_decline,
            txt_carryover,
            tv_seemore_que,
            txt_flash,
            txt_total_answer_count;
    private CircleImageView img_user_profile;
    private ImageView img_follow,
            img_option,
            img_back,
            img_sort_answer;
    private LinearLayout ll_question_answer,
            ll_answer,
            ll_decline,
            ll_carryover,
            ll_flash,
            ll_aq_article,
            ll_rp;
    private RecyclerView rv_answer_list;
    private AnswerListAdapter answerListAdapter;
    private AllPostsModel questionDetails;
    private ArrayList<AnswerModel> answerLists = new ArrayList<>();
    private String openFrom = "";

    private TextView
            txt_download,
            txt_recommend,
            txt_bookmark;

    private LinearLayout
            ll_option,
            ll_doc,
            ll_download, ll_details,
            ll_bookmark;

    private TextView
            txt_post,
            txt_doc_name,
            txt_doc_size;

    private String showAnswer = "";
    private String showAnswers = "";

    private String postId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_details);

        Utils.setStatusBarColor(getWindow(), this, R.color.white);


        initView();
        increaseDrawableSizes();
        initListener();

    }

    @Override
    protected void onResume() {
        super.onResume();
        callAPIGetAnswer();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.img_back:
                onBackPressed();
                break;

            case R.id.ll_bookmark:
                callAPIBookmarkPost(txt_bookmark.isSelected() ? "0" : "1");
                break;

            case R.id.txt_post:
                startActivity(new Intent(this, RPPostDiscussActivity.class)
                        .putExtra("oprnFrom", "rp")
                        .putExtra("researchPaperDetails", questionDetails));
                break;

            case R.id.ll_carryover:
                if (view.isSelected()) {
                    AppClass.snackBarView.snackBarShow(this, "Already carryover");
                } else {
                    new CarryOverDialog(this,
                            questionDetails.getPostId(),
                            0, (carryOverPos) -> {


                    }).show();
                }
                break;

            case R.id.img_sort_answer:

                new SortByForAnswerDialog(QuestionDetailsActivity.this,
                        questionDetails.getPostId(),
                        new SortByForAnswerDialog.UpdateAnswerListBySorting() {
                            @Override
                            public void updateData(ArrayList<AnswerModel> answer_lists) {

                                if (answerLists != null) {
                                    answerLists.clear();
                                    answerLists = answer_lists;

                                    answerListAdapter = new AnswerListAdapter(QuestionDetailsActivity.this, openFrom, answerLists, new RecyclerClickListener() {
                                        @Override
                                        public void onItemClick(int pos, String tag) {

                                            if (tag.equalsIgnoreCase("see_answer")) {
                                                startActivity(new Intent(QuestionDetailsActivity.this, AnswerDetailsActivity.class)
                                                        .putExtra("questionDetails", questionDetails)
                                                        .putExtra("answerLists", answerLists.get(pos)));
                                            } else if (tag.equalsIgnoreCase("delete")) {
                                                if (answerLists.size() > 1) {
                                                    txt_total_answer_count.setText(answerLists.size() + " " + showAnswers);
                                                } else {
                                                    txt_total_answer_count.setText(answerLists.size() + " " + showAnswer);
                                                }

                                            } else {

                                                startActivity(new Intent(QuestionDetailsActivity.this, PostAnswerActivity.class)
                                                        .putExtra("questionDetails", questionDetails)
                                                        .putExtra("openfor", "edit")
                                                        .putExtra("postAnswer", answerLists.get(pos)));
                                            }
                                        }
                                    });
                                    rv_answer_list.setAdapter(answerListAdapter);

                                    if (answerLists.size() > 1) {
                                        txt_total_answer_count.setText(answerLists.size() + " " + showAnswers);
                                    } else {
                                        txt_total_answer_count.setText(answerLists.size() + " " + showAnswer);
                                    }
                                    rv_answer_list.setVisibility(View.VISIBLE);
                                    img_sort_answer.setVisibility(View.VISIBLE);
                                }

                                if (answerLists.size() == 0) {
                                    rv_answer_list.setVisibility(View.GONE);
                                    txt_total_answer_count.setText("0 " + showAnswer);
                                    img_sort_answer.setVisibility(View.INVISIBLE);
                                }
                            }
                        }).show();

                break;

            case R.id.txt_answer:

                startActivity(new Intent(QuestionDetailsActivity.this, PostAnswerActivity.class)
                        .putExtra("questionDetails", questionDetails));

                break;

            case R.id.img_follow:
                String type;
                if (!img_follow.isSelected()) {
                    type = "1";
                } else {
                    type = "0";
                }

                callAPIChangeFollowUnfollowStatus(type, questionDetails.getAuthorId());

                break;

            case R.id.txt_like_count:
                callAPILikeUnlike(txt_like_count.isSelected() ? "0" : "1");
                break;

            case R.id.ll_decline:
                callAPIDeclinePost();
                break;

            case R.id.ll_option:
                if (AppClass.preferences.getUserId().equalsIgnoreCase(questionDetails.getAuthorId())) {

                    new HomePageOptionsDialog(this
                            , questionDetails
                            , questionDetails.getPostId()
                            , new HomePageOptionsDialog.UpdateListData() {
                        @Override
                        public void updateData(String postId) {

                            finish();

                        }
                    }).show();

                } else {
                    new HomePageOptionsForReaderDialog(this,
                            questionDetails,
                            questionDetails.getPostId()).show();
                }
                break;

            case R.id.ll_flash:

                startActivity(new Intent(this, FlashActivity.class)
                        .putExtra("post_id", questionDetails.getPostId()));

                break;

            case R.id.txt_download:
            case R.id.ll_doc:
                /*if (!AppClass.preferences.getUserId().equalsIgnoreCase(questionDetails.getAuthorId()))
                    if (!AppClass.preferences.getUserId().equalsIgnoreCase(questionDetails.getAuthorId()) &&
                            !questionDetails.isSubscribe()) {

                        new AlertDialog(this, getString(R.string.app_name), getString(R.string.not_subscribed), getString(R.string.subscribe), getString(R.string.cancel), new AlertDialog.AlertInterface() {
                            @Override
                            public void onNegativeBtnClicked(Dialog dialog) {
                                dialog.dismiss();
                            }

                            @Override
                            public void onPositiveBtnClicked(Dialog dialog) {
                                dialog.dismiss();
                                startActivity(new Intent(QuestionDetailsActivity.this, SubscribeUserWebViewActivity.class)
                                        .putExtra("post_id", questionDetails.getPostId()));


                            }
                        }).show();

                    } else {
                        downloadFile(questionDetails.getFiles().getFileName());
                    }
                else*/
                downloadFile(questionDetails.getFiles().getFileName());
                break;

            case R.id.tv_seemore_que:
                if (txt_question_value.getLineCount() == 6) {
                    tv_seemore_que.setText(getString(R.string.read_less));
                    txt_question_value.setMaxLines(Integer.MAX_VALUE);
                } else {
                    tv_seemore_que.setText(getString(R.string.read_more));
                    txt_question_value.setMaxLines(6);
                }
                break;

            case R.id.img_user_profile:
            case R.id.ll_details:
                if (AppClass.preferences.getUserId().equalsIgnoreCase(questionDetails.getAuthorId())) {
                    startActivity(new Intent(this, ProfileActivity.class));
                } else {
                    startActivity(new Intent(this, OtherUserProfileActivity.class)
                            .putExtra("user_id", questionDetails.getAuthorId()));
                }

                break;


        }
    }

    private void getIntentData() {
        openFrom = getIntent().getStringExtra("openfrom");

        if (openFrom.equalsIgnoreCase("qa")) {
            openFrom = getString(R.string.question);
            txt_answer.setText(getString(R.string.answer));
            showAnswer = getString(R.string.answer);
            showAnswers = getString(R.string.answers);
            ll_doc.setVisibility(View.GONE);
            txt_answer.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.answer), null, null, null);
            ll_aq_article.setVisibility(View.VISIBLE);
            ll_rp.setVisibility(View.GONE);
            txt_post.setVisibility(View.GONE);
            txt_additional_info.setVisibility(View.VISIBLE);
        } else {
            openFrom = getString(R.string.research_paper);
            showAnswer = getString(R.string.comment);
            showAnswers = getString(R.string.comments);
            ll_aq_article.setVisibility(View.GONE);
            ll_rp.setVisibility(View.VISIBLE);
            ll_doc.setVisibility(View.VISIBLE);
            txt_post.setVisibility(View.VISIBLE);
            txt_additional_info.setVisibility(View.GONE);
        }


        if (getIntent().hasExtra("questionDetails")) {
            questionDetails = (AllPostsModel) getIntent().getSerializableExtra("questionDetails");
            postId = questionDetails.getPostId();
            setData();
        }/* else {

            postId = getIntent().getStringExtra("postId");
            getPostDetails();

        }*/

        if (getIntent().hasExtra("notiType")) {
            startActivity(new Intent(QuestionDetailsActivity.this, AnswerDetailsActivity.class)
                    .putExtra("questionDetails", questionDetails)
                    .putExtra("postId", questionDetails.getPostId())
                    .putExtra("answerLists", getIntent().getStringExtra("answerId")));
        }


    }

    private void setData() {

        txt_question_technology_innovation.setText(String.format("%s | %s", openFrom, questionDetails.getCategoryName()));
        txt_user_place.setText(questionDetails.getProfessionalQualification());
        txt_question_value.setText(questionDetails.getPostTitle());
        if (StringUtils.isNotEmpty(questionDetails.getPostDescription()))
            txt_additional_info.setText(questionDetails.getPostDescription());
        txt_user_name.setText(questionDetails.getAuthor());
        txt_date.setText(DateTimeUtils.calculateTimeBetweenTwoDates(this, questionDetails.getPostDate(), false));

        //txt_date.setText(DateTimeUtils.changeDateTimeFormat("yyyy-MM-dd HH:mm:ss", "dd MMM", questionDetails.getPostDate()));


        if (StringUtils.isNotEmpty(questionDetails.getPostLikeCount()))
            txt_like_count.setText(questionDetails.getPostLikeCount());
        else
            txt_like_count.setText("0");

        if (StringUtils.isNotEmpty(questionDetails.getPostShareCount()))
            txt_share_count.setText(questionDetails.getPostShareCount());
        else
            txt_share_count.setText("0");


        if (StringUtils.isNotEmpty(questionDetails.getTotal_answers()))
            txt_comments_count.setText(questionDetails.getTotal_answers());
        else
            txt_comments_count.setText("0");


        txt_flash_count.setText(questionDetails.getPostFlashedCount());

        ImageLoadUtils.imageLoad(this,
                img_user_profile,
                questionDetails.getProfileImage(),
                R.drawable.menu_user_ph);


        if (questionDetails.getIsPostLiked().equalsIgnoreCase("true")) {
            txt_like_count.setSelected(true);
        } else {
            txt_like_count.setSelected(false);
        }

        if (StringUtils.isNotEmpty(questionDetails.getIs_following()) &&
                questionDetails.getIs_following().equalsIgnoreCase("true")) {
            img_follow.setSelected(true);
        } else {
            img_follow.setSelected(false);
        }


        if (questionDetails.getAuthorId().equalsIgnoreCase(AppClass.preferences.getUserId())) {
            img_follow.setVisibility(View.GONE);
        } else {
            img_follow.setVisibility(View.GONE);
        }

        if (questionDetails.getFiles() != null) {
            if (StringUtils.isNotEmpty(questionDetails.getFiles().getOriFileName()))
                txt_doc_name.setText(questionDetails.getFiles().getOriFileName());

            if (StringUtils.isNotEmpty(questionDetails.getFiles().getFileSize()))
                txt_doc_size.setText(questionDetails.getFiles().getFileSize());

        }


        if (StringUtils.isNotEmpty(questionDetails.getIs_carryover_expire()) &&
                questionDetails.getIs_carryover_expire().equalsIgnoreCase("true")) {
            ll_carryover.setVisibility(View.GONE);
        } else {
            ll_carryover.setVisibility(View.VISIBLE);
            if (StringUtils.isNotEmpty(questionDetails.getIs_carryover()) &&
                    questionDetails.getIs_carryover().equalsIgnoreCase("true")) {
                ll_carryover.setSelected(true);
            } else {
                ll_carryover.setSelected(false);
            }

        }

        txt_question_value.post(new Runnable() {
            @Override
            public void run() {
                if (txt_question_value.getLineCount() < 6) {
                    tv_seemore_que.setVisibility(View.GONE);
                }

            }
        });


    }

    private void increaseDrawableSizes() {
        txt_like_count.setCompoundDrawables(getDrawableAndResize(R.drawable.selector_like_post), null, null, null);
        txt_comments_count.setCompoundDrawables(getDrawableAndResize(R.drawable.comments), null, null, null);
        txt_answer.setCompoundDrawables(getDrawableAndResize(R.drawable.answer), null, null, null);
        txt_decline.setCompoundDrawables(getDrawableAndResize(R.drawable.decline), null, null, null);
        txt_carryover.setCompoundDrawables(getDrawableAndResize(R.drawable.carryover), null, null, null);
        txt_flash.setCompoundDrawables(getDrawableAndResize(R.drawable.flash), null, null, null);
        txt_download.setCompoundDrawables(getDrawableAndResize(R.drawable.download), null, null, null);
        txt_download.setGravity(Gravity.CENTER);
        txt_recommend.setCompoundDrawables(getDrawableAndResize(R.drawable.recommand), null, null, null);
        txt_recommend.setGravity(Gravity.CENTER);
        txt_bookmark.setCompoundDrawables(getDrawableAndResize(R.drawable.selector_bookmark_post), null, null, null);
        txt_bookmark.setGravity(Gravity.CENTER);
    }


    private Drawable getDrawableAndResize(int drawableId) {
        Drawable drawable = ContextCompat.getDrawable(this, drawableId);
        drawable.setBounds(0,
                0,
                (int) (drawable.getIntrinsicWidth() * 1.4),
                (int) (drawable.getIntrinsicHeight() * 1.4));

        return drawable;
    }

    private void initView() {
        txt_question_technology_innovation = findViewById(R.id.txt_question_technology_innovation);
        txt_user_name = findViewById(R.id.txt_user_name);
        txt_user_place = findViewById(R.id.txt_user_place);
        txt_flash_count = findViewById(R.id.txt_flash_count);
        txt_date = findViewById(R.id.txt_date);
        txt_question_value = findViewById(R.id.txt_question_value);
        txt_comments_count = findViewById(R.id.txt_comments_count);
        txt_share_count = findViewById(R.id.txt_share_count);
        txt_like_count = findViewById(R.id.txt_like_count);
        txt_answer = findViewById(R.id.txt_answer);
        txt_decline = findViewById(R.id.txt_decline);
        txt_carryover = findViewById(R.id.txt_carryover);
        txt_flash = findViewById(R.id.txt_flash);
        txt_total_answer_count = findViewById(R.id.txt_total_answer_count);
        txt_download = findViewById(R.id.txt_download);
        txt_recommend = findViewById(R.id.txt_recommend);
        txt_bookmark = findViewById(R.id.txt_bookmark);
        txt_additional_info = findViewById(R.id.txt_additional_info);
        txt_post = findViewById(R.id.txt_post);
        ll_aq_article = findViewById(R.id.ll_aq_article);
        tv_seemore_que = findViewById(R.id.tv_seemore_que);
        ll_rp = findViewById(R.id.ll_rp);
        ll_option = findViewById(R.id.ll_option);
        ll_doc = findViewById(R.id.ll_doc);
        txt_doc_name = findViewById(R.id.txt_doc_name);
        txt_doc_size = findViewById(R.id.txt_doc_size);
        ll_download = findViewById(R.id.ll_download);
        ll_bookmark = findViewById(R.id.ll_bookmark);

        img_user_profile = findViewById(R.id.img_user_profile);
        img_follow = findViewById(R.id.img_follow);
        img_option = findViewById(R.id.img_option);
        img_back = findViewById(R.id.img_back);
        img_sort_answer = findViewById(R.id.img_sort_answer);

        ll_question_answer = findViewById(R.id.ll_question_answer);
        ll_answer = findViewById(R.id.ll_answer);
        ll_decline = findViewById(R.id.ll_decline);
        ll_carryover = findViewById(R.id.ll_carryover);
        ll_flash = findViewById(R.id.ll_flash);
        ll_details = findViewById(R.id.ll_details);

        rv_answer_list = findViewById(R.id.rv_answer_list);
        LinearLayoutManager layoutManager = new LinearLayoutManager(QuestionDetailsActivity.this);
        rv_answer_list.setLayoutManager(layoutManager);

        // set typeface

        txt_question_technology_innovation.setTypeface(AppClass.lato_regular);
        txt_additional_info.setTypeface(AppClass.lato_regular);
        txt_user_name.setTypeface(AppClass.lato_bold);
        txt_user_place.setTypeface(AppClass.lato_regular);
        txt_flash_count.setTypeface(AppClass.lato_regular);
        txt_date.setTypeface(AppClass.lato_regular);
        txt_question_value.setTypeface(AppClass.lato_bold);
        txt_comments_count.setTypeface(AppClass.lato_regular);
        txt_share_count.setTypeface(AppClass.lato_regular);
        txt_like_count.setTypeface(AppClass.lato_regular);
        txt_answer.setTypeface(AppClass.lato_regular);
        txt_decline.setTypeface(AppClass.lato_regular);
        txt_carryover.setTypeface(AppClass.lato_regular);
        txt_flash.setTypeface(AppClass.lato_regular);
        txt_download.setTypeface(AppClass.lato_regular);
        txt_recommend.setTypeface(AppClass.lato_regular);
        txt_bookmark.setTypeface(AppClass.lato_regular);
        txt_total_answer_count.setTypeface(AppClass.lato_bold);
        txt_doc_name.setTypeface(AppClass.lato_heavy);
        txt_doc_size.setTypeface(AppClass.lato_regular);

        getIntentData();

    }

    private void initListener() {
        img_back.setOnClickListener(this);
        img_sort_answer.setOnClickListener(this);
        txt_answer.setOnClickListener(this);
        img_follow.setOnClickListener(this);
        ll_option.setOnClickListener(this);
        txt_like_count.setOnClickListener(this);
        ll_decline.setOnClickListener(this);
        ll_download.setOnClickListener(this);
        ll_bookmark.setOnClickListener(this);
        ll_flash.setOnClickListener(this);
        txt_post.setOnClickListener(this);
        txt_download.setOnClickListener(this);
        ll_doc.setOnClickListener(this);
        tv_seemore_que.setOnClickListener(this);
        ll_carryover.setOnClickListener(this);
        ll_details.setOnClickListener(this);
        img_user_profile.setOnClickListener(this);
    }

    private void callAPIGetAnswer() {

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("post_id", postId);


        ApiCall.getInstance().getAnswerList(QuestionDetailsActivity.this,
                request, new IApiCallback() {
                    @Override
                    public void onSuccess(Object data) {

                        if (answerLists != null) {
                            answerLists.clear();
                            answerLists = (ArrayList<AnswerModel>) data;

                            answerListAdapter = new AnswerListAdapter(QuestionDetailsActivity.this, openFrom, answerLists, new RecyclerClickListener() {
                                @Override
                                public void onItemClick(int pos, String tag) {

                                    if (tag.equalsIgnoreCase("see_answer")) {
                                        startActivity(new Intent(QuestionDetailsActivity.this, AnswerDetailsActivity.class)
                                                .putExtra("questionDetails", questionDetails)
                                                .putExtra("postId", questionDetails.getPostId())
                                                .putExtra("answerLists", answerLists.get(pos).postAnswersId));
                                    } else if (tag.equalsIgnoreCase("delete")) {
                                        if (answerLists.size() > 1) {
                                            txt_total_answer_count.setText(answerLists.size() + " " + showAnswers);
                                        } else {
                                            txt_total_answer_count.setText(answerLists.size() + " " + showAnswer);
                                        }

                                    } else if (tag.equalsIgnoreCase("edit")) {
                                        startActivity(new Intent(QuestionDetailsActivity.this, PostAnswerActivity.class)
                                                .putExtra("questionDetails", questionDetails)
                                                .putExtra("openfor", "edit")
                                                .putExtra("postAnswer", answerLists.get(pos)));
                                    }
                                }
                            });
                            rv_answer_list.setAdapter(answerListAdapter);

                            if (answerLists.size() > 1) {
                                txt_total_answer_count.setText(answerLists.size() + " " + showAnswers);
                            } else {
                                txt_total_answer_count.setText(answerLists.size() + " " + showAnswer);
                            }
                            rv_answer_list.setVisibility(View.VISIBLE);
                            img_sort_answer.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void onFailure(boolean isFalseFromService, String errorMessage) {

                        rv_answer_list.setVisibility(View.GONE);
                        txt_total_answer_count.setText("0 " + showAnswer);
                        img_sort_answer.setVisibility(View.INVISIBLE);

               /* DialogUtils.openDialog(QuestionDetailsActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                    @Override
                    public void onNegativeBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }

                    @Override
                    public void onPositiveBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }
                });*/
                    }


                }, true);

    }

    private void callAPIChangeFollowUnfollowStatus(final String type, String otherUserId) {

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("follower_id", otherUserId);
        request.put("type", type);

        ApiCall.getInstance().changeFollowUnfollowStatus(QuestionDetailsActivity.this, request, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {
                String response = (String) data;

                if (type.equalsIgnoreCase("0")) {
                    // txt_follow.setText(R.string.follow);
                    questionDetails.setIs_following("false");
                } else {
                    //txt_follow.setText(R.string.unfollow);
                    questionDetails.setIs_following("true");
                }

                if (questionDetails.getIs_following().equalsIgnoreCase("true")) {
                    img_follow.setSelected(true);
                } else {
                    img_follow.setSelected(false);
                }

            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {

            }
        }, true);

    }

    private void callAPILikeUnlike(final String status) {

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("post_id", postId);
        request.put("like_unlike", status);

        ApiCall.getInstance().likeUnlike(this, request, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {
                String response = (String) data;

                int count;

                if (status.equalsIgnoreCase("0")) {
                    txt_like_count.setSelected(false);
                    count = Integer.parseInt(questionDetails.getPostLikeCount()) - 1;

                } else {
                    txt_like_count.setSelected(true);
                    count = Integer.parseInt(questionDetails.getPostLikeCount()) + 1;


                }
                questionDetails.setPostLikeCount(String.valueOf(count));
                txt_like_count.setText(String.valueOf(count));
            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {
                DialogUtils.openDialog(QuestionDetailsActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                    @Override
                    public void onNegativeBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }

                    @Override
                    public void onPositiveBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }
                });
            }
        }, true);

    }

    private void callAPIDeclinePost() {

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("post_id", postId);
        request.put("status", "1");

        Logger.d("request:" + request);

        ApiCall.getInstance().declinePost(QuestionDetailsActivity.this,
                request, new IApiCallback() {
                    @Override
                    public void onSuccess(Object data) {


                        finish();

                        /*DialogUtils.openDialog(activity, data.toString(), activity.getString(R.string.ok), "", new AlertDialogInterface() {
                            @Override
                            public void onNegativeBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }

                            @Override
                            public void onPositiveBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();

                            }
                        });*/
                    }

                    @Override
                    public void onFailure(boolean isFalseFromService, String errorMessage) {


                        DialogUtils.openDialog(QuestionDetailsActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                            @Override
                            public void onNegativeBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }

                            @Override
                            public void onPositiveBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }
                        });
                    }


                }, true);

    }

    private void callAPIBookmarkPost(final String status) {

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("post_id", postId);
        request.put("bookmark_unbookmark", status);

        ApiCall.getInstance().bookmarkUnbookmark(this, request, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {
                String response = (String) data;

                if (status.equalsIgnoreCase("0")) {
                    txt_bookmark.setText("Bookmark");
                    txt_bookmark.setSelected(false);
                } else {
                    txt_bookmark.setText("Bookmarked");
                    txt_bookmark.setSelected(true);
                }


            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {
                DialogUtils.openDialog(QuestionDetailsActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                    @Override
                    public void onNegativeBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }

                    @Override
                    public void onPositiveBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }
                });
            }
        }, true);


    }

    private void downloadFile(String fileUrl) {

        final String dirPath = Environment.getExternalStorageDirectory().toString() + "/" + getString(R.string.app_name) + "/";
        final String fileName = "" + questionDetails.getFiles().getOriFileName();

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Downloading");
        progressDialog.setCancelable(true);
        progressDialog.setCanceledOnTouchOutside(false);

        progressDialog.show();

        AndroidNetworking
                .download(fileUrl, dirPath, fileName)
                .build()
                .startDownload(new DownloadListener() {
                    @Override
                    public void onDownloadComplete() {
                        AppClass.snackBarView.snackBarShow(QuestionDetailsActivity.this, "Download Completed");
                        progressDialog.dismiss();

                        try {
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            File file = new File(dirPath + fileName);
                            MimeTypeMap map = MimeTypeMap.getSingleton();
                            String ext = MimeTypeMap.getFileExtensionFromUrl(file.getName());
                            String type = map.getMimeTypeFromExtension(ext);

                            if (type == null)
                                type = "*/*";
                            Uri data = Uri.fromFile(file);
                            intent.setDataAndType(data, type);
                            startActivity(intent);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        AppClass.snackBarView.snackBarShow(QuestionDetailsActivity.this, "Error while downloading");
                        progressDialog.dismiss();
                    }
                });

    }

    private void callAPIDownloadPost() {

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("post_id", postId);

        ApiCall.getInstance().downloadPost(this, request, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {
                String response = (String) data;
                downloadFile(questionDetails.getFiles().getFileName());
            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {
                DialogUtils.openDialog(QuestionDetailsActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                    @Override
                    public void onNegativeBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }

                    @Override
                    public void onPositiveBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }
                });
            }
        }, true);


    }

   /* private void getPostDetails() {

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("post_id", postId);

        ApiCall.getInstance().getPostDetails(this, request, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {
                questionDetails = (AllPostsModel) data;
                setData();

            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {
                DialogUtils.openDialog(QuestionDetailsActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                    @Override
                    public void onNegativeBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }

                    @Override
                    public void onPositiveBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }
                });
            }
        }, true);


    }*/

}
