package com.proclapp.home.question;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.home.AnswerEditDeleteDialog;
import com.proclapp.model.AnswerModel;
import com.proclapp.utils.ImageLoadUtils;
import com.proclapp.utils.KeyBoardUtils;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class ReplyListAdapter extends RecyclerView.Adapter<ReplyListAdapter.ViewHolder> {
    private AppCompatActivity activity;
    private ArrayList<AnswerModel.Comments> replyList;

    public ReplyListAdapter(AppCompatActivity activity, ArrayList<AnswerModel.Comments> replyList) {
        this.activity = activity;
        this.replyList = replyList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_reply_list, parent, false);
        return new ReplyListAdapter.ViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.txt_person_name.setText(replyList.get(holder.getAdapterPosition()).getUser().getName());
        holder.txt_ans_comment.setText(replyList.get(holder.getAdapterPosition()).getComment());
        holder.txt_person_organisation.setText(replyList.get(holder.getAdapterPosition()).getUser().getProfessionalQualification());

        ImageLoadUtils.imageLoad(activity,
                holder.img_user_pic,
                replyList.get(holder.getAdapterPosition()).getUser().getProfileImage(),
                R.drawable.menu_user_ph);

        if (AppClass.preferences.getUserId().equalsIgnoreCase(replyList.get(holder.getAdapterPosition()).getUser().getUserId()))
            holder.img_option.setVisibility(View.VISIBLE);
        else
            holder.img_option.setVisibility(View.GONE);

        holder.img_option.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                KeyBoardUtils.closeSoftKeyboard(activity);
                if (AppClass.preferences.getUserId().equalsIgnoreCase(replyList.get(holder.getAdapterPosition()).getUser().getUserId())) {

                    new AnswerEditDeleteDialog(activity, "comment", new AnswerEditDeleteDialog.OnItemClick() {
                        @Override
                        public void onClick(String tag) {

//                            if (tag.equalsIgnoreCase("delete")) {
//                                callAPIDeletePostAnswerComment(replyList.get(holder.getAdapterPosition()).getComment_id(), holder.getAdapterPosition());
//                            } else {
//                                listener.onItemClick(holder.getAdapterPosition(), "edit");
//                            }

                        }
                    }).show();

                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return replyList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txt_person_name,
                txt_person_organisation,
                txt_ans_comment;
        private CircleImageView img_user_pic;
        private ImageView img_option;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            txt_person_name = itemView.findViewById(R.id.txt_person_name);
            txt_person_organisation = itemView.findViewById(R.id.txt_person_organisation);
            txt_ans_comment = itemView.findViewById(R.id.txt_ans_comment);

            img_user_pic = itemView.findViewById(R.id.img_user_pic);
            img_option = itemView.findViewById(R.id.img_option);
        }
    }
}
