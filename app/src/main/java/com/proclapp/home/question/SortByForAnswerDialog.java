package com.proclapp.home.question;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.model.AnswerModel;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.Logger;

import java.util.ArrayList;
import java.util.HashMap;

public class SortByForAnswerDialog extends Dialog {

    private Activity activity;

    private TextView txt_sort_by,
            txt_upvotes,
            txt_users,
            txt_views;

    private CardView card_view_option;

    private ImageView img_close;
    private RelativeLayout rl_main;

    private String postId = "";
    private UpdateAnswerListBySorting updateAnswerListBySorting;

    public SortByForAnswerDialog(Activity a, String postId,UpdateAnswerListBySorting updateAnswerListBySorting) {
        super(a);
        this.activity = a;
        this.postId = postId;
        this.updateAnswerListBySorting = updateAnswerListBySorting;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_sort_by_for_answer);

        txt_sort_by = findViewById(R.id.txt_sort_by);
        txt_upvotes = findViewById(R.id.txt_upvotes);
        txt_views = findViewById(R.id.txt_views);
        txt_users = findViewById(R.id.txt_users);

        card_view_option = findViewById(R.id.card_view_option);
        card_view_option.setBackgroundResource(R.drawable.rounded_bg_option);

        img_close = findViewById(R.id.img_close);
        rl_main = findViewById(R.id.rl_main);


        getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        //getWindow().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(activity,R.color.color_28black)));
        //getWindow().setBackgroundDrawableResource(R.drawable.trans_bg);

        txt_sort_by.setTypeface(AppClass.lato_medium);
        txt_upvotes.setTypeface(AppClass.lato_medium);
        txt_views.setTypeface(AppClass.lato_medium);

        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dismiss();
            }
        });

        rl_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dismiss();
            }
        });

        txt_upvotes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dismiss();
                callAPIGetAnswer("3");
            }
        });

        txt_users.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dismiss();
                callAPIGetAnswer("4");
            }
        });

        txt_views.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dismiss();
                callAPIGetAnswer("2");
            }
        });

    }

    private void callAPIGetAnswer(String sort_by) {

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("post_id", postId);
        request.put("sort_by", sort_by);

        Logger.d("request:" + request);

        ApiCall.getInstance().getAnswerList(activity,
                request, new IApiCallback() {
                    @Override
                    public void onSuccess(Object data) {

                        ArrayList<AnswerModel> answerLists = new ArrayList<>();
                        answerLists = (ArrayList<AnswerModel>) data;
                        updateAnswerListBySorting.updateData(answerLists);

                    }

                    @Override
                    public void onFailure(boolean isFalseFromService, String errorMessage) {

                        ArrayList<AnswerModel> answerLists = new ArrayList<>();
                        updateAnswerListBySorting.updateData(answerLists);
                    }


                }, true);

    }

    public interface UpdateAnswerListBySorting {
        void updateData(ArrayList<AnswerModel> answerLists);
    }

}