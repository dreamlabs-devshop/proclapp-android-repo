package com.proclapp.home.voice_video;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.home.AnswerEditDeleteDialog;
import com.proclapp.home.SeeDiscussionActivity;
import com.proclapp.interfaces.RecyclerClickListener;
import com.proclapp.model.AnswerModel;
import com.proclapp.payment.WebviewShowAnsActivity;
import com.proclapp.profile.OtherUserProfileActivity;
import com.proclapp.profile.ProfileActivity;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.ImageLoadUtils;
import com.proclapp.utils.KeyBoardUtils;
import com.proclapp.utils.StringUtils;

import java.util.ArrayList;

public class ExpertAnswerAdapter extends RecyclerView.Adapter<ExpertAnswerAdapter.ExpertAnswerHolder> {

    private Activity context;
    private ArrayList<AnswerModel> replyList;
    private MediaPlayer mPlayer;
    private RecyclerClickListener clickListener;

    public ExpertAnswerAdapter(Activity context, ArrayList<AnswerModel> replyList, RecyclerClickListener clickListener) {
        this.context = context;
        this.replyList = replyList;
        this.clickListener = clickListener;
    }

    @NonNull
    @Override
    public ExpertAnswerHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_discussion, viewGroup, false);
        return new ExpertAnswerHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ExpertAnswerHolder holder, int i) {
        holder.txt_person_name.setText(replyList.get(holder.getAdapterPosition()).getUser().name);
        holder.txt_person_organisation.setText(replyList.get(holder.getAdapterPosition()).getUser().professionalQualification);
        ImageLoadUtils.imageLoad(context,
                holder.img_user_pic,
                replyList.get(holder.getAdapterPosition()).getUser().profileImage,
                R.drawable.menu_user_ph);

        if (AppClass.preferences.getUserId().equalsIgnoreCase(replyList.get(holder.getAdapterPosition()).getUser().getUserId()))
            holder.img_option.setVisibility(View.VISIBLE);
        else
            holder.img_option.setVisibility(View.INVISIBLE);

        if (StringUtils.isNotEmpty(replyList.get(holder.getAdapterPosition()).getVv_type())) {

            if (replyList.get(holder.getAdapterPosition()).getVv_type().equalsIgnoreCase("1")) {
                holder.txt_see_answer.setText("Play Audio");
            } else {
                holder.txt_see_answer.setText("Play Video");
            }
        } else {
            holder.txt_see_answer.setText("See Reply");
        }
        AudioManager am = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        am.setMode(AudioManager.MODE_NORMAL);
        mPlayer = new MediaPlayer();

        holder.txt_see_answer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (StringUtils.isNotEmpty(replyList.get(holder.getAdapterPosition()).getShowAns()) &&
                        replyList.get(holder.getAdapterPosition()).getShowAns().equalsIgnoreCase("1")) {

                    if (StringUtils.isNotEmpty(replyList.get(holder.getAdapterPosition()).getVv_type())) {
                        if (replyList.get(holder.getAdapterPosition()).getVv_type().equalsIgnoreCase("1")) {


                            if (holder.txt_see_answer.getText().toString().equalsIgnoreCase("Play Audio")) {
                                if (mPlayer != null)
                                    mPlayer.release();

                                onStreamAudio(replyList.get(holder.getAdapterPosition()).getAudioPost().getAudioUrl(), holder);
                                holder.txt_see_answer.setText("Pause Audio");

                            } else {
                                mPlayer.stop();
                                mPlayer.release();
                                holder.txt_see_answer.setText("Play Audio");
                            }


                        } else {
                            context.startActivity(new Intent(context, VideoPlayActivity.class)
                                    .putExtra("audiovideourl", replyList.get(holder.getAdapterPosition()).getVideosPost().getVideoUrl()));

                        }
                    } else {
                        context.startActivity(new Intent(context, SeeDiscussionActivity.class)
                                .putExtra("answerLists", replyList.get(holder.getAdapterPosition())));
                    }
                } else {
                    context.startActivityForResult(new Intent(context, WebviewShowAnsActivity.class)
                            .putExtra("post_id", replyList.get(holder.getAdapterPosition()).getPostId()), 123);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return replyList.size();
    }

    private void deleteAnswer(final ExpertAnswerHolder holder) {

        ApiCall.getInstance().deleteAnswer(context, AppClass.preferences.getUserId(), replyList.get(holder.getAdapterPosition()).postAnswersId, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {

                replyList.remove(holder.getAdapterPosition());
                notifyDataSetChanged();

            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {

            }
        }, true);

    }

    private void onStreamAudio(String url, ExpertAnswerHolder holder) {
        mPlayer = new MediaPlayer();
        mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        // Listen for if the audio file can't be prepared
        mPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                // ... react appropriately ...
                // The MediaPlayer has moved to the Error state, must be reset!
                return false;
            }
        });
        mPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mPlayer.start();
            }
        });

        try {
            mPlayer.setDataSource(url);
            mPlayer.prepareAsync();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class ExpertAnswerHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView
                img_user_pic,
                img_option;

        TextView
                txt_person_name,
                txt_person_organisation,
                txt_see_answer;


        public ExpertAnswerHolder(@NonNull View itemView) {
            super(itemView);
            initViews();
            setTypeFace();
            setClickListener();
        }

        @Override
        public void onClick(View view) {

            switch (view.getId()) {
                case R.id.img_user_pic:
                case R.id.txt_person_name:
                case R.id.txt_person_organisation:

                    if (AppClass.preferences.getUserId().equalsIgnoreCase(replyList.get(getAdapterPosition()).getUser().getUserId())) {
                        context.startActivity(new Intent(context, ProfileActivity.class));

                    } else {

                        context.startActivity(new Intent(context, OtherUserProfileActivity.class)
                                .putExtra("user_id", replyList.get(getAdapterPosition()).getUser().getUserId()));
                    }
                    break;


                case R.id.img_option:
                    KeyBoardUtils.closeSoftKeyboard(context);
                    if (AppClass.preferences.getUserId().equalsIgnoreCase(replyList.get(getAdapterPosition()).getUser().getUserId())) {

                        new AnswerEditDeleteDialog(context, new AnswerEditDeleteDialog.OnItemClick() {
                            @Override
                            public void onClick(String tag) {

                                if (tag.equalsIgnoreCase("delete")) {
                                    deleteAnswer(ExpertAnswerHolder.this);
                                } else {
                                    clickListener.onItemClick(getAdapterPosition(), "edit");
                                }

                            }
                        }).show();

                    }
                    break;


            }
        }

        private void initViews() {
            img_user_pic = itemView.findViewById(R.id.img_user_pic);
            img_option = itemView.findViewById(R.id.img_option);
            txt_person_name = itemView.findViewById(R.id.txt_person_name);
            txt_person_organisation = itemView.findViewById(R.id.txt_person_organisation);
            txt_see_answer = itemView.findViewById(R.id.txt_see_answer);
        }

        private void setClickListener() {
            img_user_pic.setOnClickListener(this);
            img_option.setOnClickListener(this);
            txt_person_name.setOnClickListener(this);
            txt_person_organisation.setOnClickListener(this);
            txt_see_answer.setOnClickListener(this);

        }

        private void setTypeFace() {
            txt_person_name.setTypeface(AppClass.lato_medium);
            txt_person_organisation.setTypeface(AppClass.lato_regular);

        }

    }

}
