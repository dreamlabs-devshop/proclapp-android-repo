package com.proclapp.home.voice_video;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.home.adapter.VoiceVideoCategoryAdapter;
import com.proclapp.model.CategoryModel;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.AlertDialogInterface;
import com.proclapp.utils.DialogUtils;
import com.proclapp.utils.Logger;
import com.proclapp.utils.StringUtils;
import com.proclapp.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;


public class SelectCategoryActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView img_back;
    private TextView txt_next,
            txt_select_category,
            txt_select_category_make_request;
    private EditText edt_search;
    private RecyclerView rv_select_category;
    private VoiceVideoCategoryAdapter categoryAdapter;
    private ArrayList<CategoryModel> categoryList = new ArrayList<>();

    private String categoryId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_category);
        Utils.setStatusBarColor(getWindow(), this, R.color.white);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        initView();
        initStyle();
        initListener();

        callAPICategoryList();

    }

    private void initView() {

        img_back = findViewById(R.id.img_back);
        txt_next = findViewById(R.id.txt_next);
        txt_select_category = findViewById(R.id.txt_select_category);
        txt_select_category_make_request = findViewById(R.id.txt_select_category_make_request);
        edt_search = findViewById(R.id.edt_search);
        rv_select_category = findViewById(R.id.rv_select_category);
        GridLayoutManager layoutManager = new GridLayoutManager(SelectCategoryActivity.this, 3);
        rv_select_category.setLayoutManager(layoutManager);

        edt_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (categoryAdapter != null) {
                    categoryAdapter.getFilter().filter(charSequence);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    private void initStyle() {
        txt_next.setTypeface(AppClass.lato_regular);
        txt_select_category.setTypeface(AppClass.lato_regular);
        txt_select_category_make_request.setTypeface(AppClass.lato_regular);
        edt_search.setTypeface(AppClass.lato_regular);
    }

    private void initListener() {

        img_back.setOnClickListener(this);
        txt_next.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.img_back:

                onBackPressed();

                break;

            case R.id.txt_next:

                if (StringUtils.isNotEmpty(categoryId)) {
                    startActivity(new Intent(SelectCategoryActivity.this, SelectExpertActivity.class)
                            .putExtra("categoryId", categoryId)
                            .putExtra("openfrom", "category"));
                } else {
                    AppClass.snackBarView.snackBarShow(SelectCategoryActivity.this, getString(R.string.please_select_cateogory));
                }
                break;
        }
    }

    private void callAPICategoryList() {

        if (AppClass.isInternetConnectionAvailable()) {

            HashMap<String, String> request = new HashMap<>();
            request.put("user_id", AppClass.preferences.getUserId());

            ApiCall.getInstance().getCategories(SelectCategoryActivity.this, request, new IApiCallback() {
                @Override
                public void onSuccess(Object data) {

                    categoryList.clear();
                    categoryList.addAll((ArrayList<CategoryModel>) data);

                    setCategoryList();

                }

                @Override
                public void onFailure(boolean isFalseFromService, String errorMessage) {
                    if (StringUtils.isNotEmpty(errorMessage)) {
                        DialogUtils.openDialog(SelectCategoryActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                            @Override
                            public void onNegativeBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }

                            @Override
                            public void onPositiveBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }
                        });
                    }
                }
            }, true);


        } else {
            AppClass.snackBarView.snackBarShow(SelectCategoryActivity.this, getString(R.string.nonetwork));
        }


    }


    private void setCategoryList() {
        categoryAdapter = new VoiceVideoCategoryAdapter(SelectCategoryActivity.this, categoryList, new VoiceVideoCategoryAdapter.CategorySelection() {
            @Override
            public void onCategorySelection(String categoryName, String categoryId) {

                Logger.e("categoryName " + categoryName + " categoryId " + categoryId);

                SelectCategoryActivity.this.categoryId = categoryId;

            }
        });
        rv_select_category.setAdapter(categoryAdapter);
    }
}
