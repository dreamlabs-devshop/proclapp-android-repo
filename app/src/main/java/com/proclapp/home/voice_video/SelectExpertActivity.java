package com.proclapp.home.voice_video;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.expert.ExpertModel;
import com.proclapp.home.adapter.SelectExpertAdapter;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.AlertDialogInterface;
import com.proclapp.utils.DialogUtils;
import com.proclapp.utils.Logger;
import com.proclapp.utils.StringUtils;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class SelectExpertActivity extends AppCompatActivity implements View.OnClickListener {

    ArrayList<ExpertModel> expertList = new ArrayList<>();
    private ImageView img_back;
    private TextView txt_next,
            txt_select_an_expert,
            txt_select_expert_to_make_request;
    private EditText edt_search;
    private RecyclerView rv_select_expert;
    private SelectExpertAdapter selectExpertAdapter;
    private String openFrom = "";
    private String expertId = "", categoryId = "";
    private String postId = "";

    private ArrayList<String> expertIdList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_expert);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        initView();
        initStyle();
        initListener();
    }

    private void initView() {

        img_back = findViewById(R.id.img_back);
        txt_next = findViewById(R.id.txt_next);
        txt_select_an_expert = findViewById(R.id.txt_select_an_expert);
        txt_select_expert_to_make_request = findViewById(R.id.txt_select_expert_to_make_request);
        edt_search = findViewById(R.id.edt_search);
        rv_select_expert = findViewById(R.id.rv_select_expert);

        openFrom = getIntent().getStringExtra("openfrom");
        if (openFrom.equalsIgnoreCase("category")) {
            categoryId = getIntent().getStringExtra("categoryId");
            txt_next.setText(getString(R.string.next));
        } else {
            postId = getIntent().getStringExtra("postId");
            txt_next.setText(getString(R.string.send));
        }
        setUpRecyclerView();

    }

    private void setUpRecyclerView() {

        GridLayoutManager layoutManager = new GridLayoutManager(SelectExpertActivity.this, 3);
        rv_select_expert.setLayoutManager(layoutManager);

        selectExpertAdapter = new SelectExpertAdapter(SelectExpertActivity.this, expertList, expertId -> {
            SelectExpertActivity.this.expertId = expertId;
            expertIdList.clear();
            expertIdList.add(expertId);
        });

        rv_select_expert.setAdapter(selectExpertAdapter);

        getExpertList();
    }

    private void initStyle() {
        txt_next.setTypeface(AppClass.lato_regular);
        txt_select_an_expert.setTypeface(AppClass.lato_regular);
        txt_select_expert_to_make_request.setTypeface(AppClass.lato_regular);
        edt_search.setTypeface(AppClass.lato_regular);
    }

    private void initListener() {

        img_back.setOnClickListener(this);
        txt_next.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.img_back:
                onBackPressed();
                break;

            case R.id.txt_next:
                if (openFrom.equalsIgnoreCase("category")) {
                    if (StringUtils.isNotEmpty(expertId)) {
                        startActivity(new Intent(SelectExpertActivity.this, SelectVoiceVideoActivity.class)
                                .putExtra("categoryId", categoryId)
                                .putExtra("expertId", expertId));
                    } else {
                        AppClass.snackBarView.snackBarShow(SelectExpertActivity.this, getString(R.string.please_select_mentor));
                    }
                } else {
                    if (!expertIdList.isEmpty()) {
                        requestToExpert();
                    }else {
                        AppClass.snackBarView.snackBarShow(SelectExpertActivity.this, getString(R.string.please_select_mentor));
                    }
                }
                break;
        }
    }

    private void getExpertList() {

        ApiCall.getInstance().getExpertList(SelectExpertActivity.this, AppClass.preferences.getUserId(), new IApiCallback() {
            @Override
            public void onSuccess(Object data) {
                JsonArray response = (JsonArray) data;

                ArrayList<ExpertModel> tempExpertList = new Gson().fromJson(response.toString(),
                        new TypeToken<List<ExpertModel>>() {
                        }.getType());


                expertList.clear();
                expertList.addAll(tempExpertList);
                rv_select_expert.getAdapter().notifyDataSetChanged();


            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {

                if (StringUtils.isNotEmpty(errorMessage)) {
                    DialogUtils.openDialog(SelectExpertActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                        @Override
                        public void onNegativeBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }

                        @Override
                        public void onPositiveBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }
                    });
                }

            }
        }, true);


    }

    private void requestToExpert() {

        String userId = AppClass.preferences.getUserId();

        Logger.e("expertIdList "+new JSONArray(expertIdList).toString());

        ApiCall.getInstance().requestToExpert(this, userId, postId, expertId, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {
                DialogUtils.openDialog(SelectExpertActivity.this, (String) data, getString(R.string.ok), "", new AlertDialogInterface() {
                    @Override
                    public void onNegativeBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }

                    @Override
                    public void onPositiveBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                        finish();
                    }
                });
            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {
                DialogUtils.openDialog(SelectExpertActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                    @Override
                    public void onNegativeBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }

                    @Override
                    public void onPositiveBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }
                });
            }
        }, true);

    }

}
