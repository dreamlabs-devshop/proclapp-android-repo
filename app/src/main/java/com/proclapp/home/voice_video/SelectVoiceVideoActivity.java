package com.proclapp.home.voice_video;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.proclapp.AppClass;
import com.proclapp.R;

public class SelectVoiceVideoActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView img_back,
            img_voice,
            img_video;
    private TextView txt_voice_video,
            txt_select_voice_video,
            txt_select_option_for_voice_video,
            txt_voice,
            txt_video;
    private Button btn_next;
    private boolean is_voice_selected = false;
    private boolean is_video_selected = false;
    private String expertId = "", categoryId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_voice_video);

        initView();
        initStyle();
        initListener();
    }

    private void initView() {

        img_back = findViewById(R.id.img_back);
        img_voice = findViewById(R.id.img_voice);
        img_video = findViewById(R.id.img_video);

        txt_voice_video = findViewById(R.id.txt_voice_video);
        txt_select_voice_video = findViewById(R.id.txt_select_voice_video);
        txt_select_option_for_voice_video = findViewById(R.id.txt_select_option_for_voice_video);
        txt_voice = findViewById(R.id.txt_voice);
        txt_video = findViewById(R.id.txt_video);

        btn_next = findViewById(R.id.btn_next);

        categoryId = getIntent().getStringExtra("categoryId");
        expertId = getIntent().getStringExtra("expertId");

    }

    private void initStyle() {

        txt_voice_video.setTypeface(AppClass.lato_regular);
        txt_select_voice_video.setTypeface(AppClass.lato_regular);
        txt_select_option_for_voice_video.setTypeface(AppClass.lato_regular);
        txt_voice.setTypeface(AppClass.lato_medium);
        txt_video.setTypeface(AppClass.lato_medium);
        btn_next.setTypeface(AppClass.lato_semibold);
    }

    private void initListener() {

        img_back.setOnClickListener(this);
        img_voice.setOnClickListener(this);
        img_video.setOnClickListener(this);
        btn_next.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.img_back:

                onBackPressed();

                break;

            case R.id.img_voice:


                is_voice_selected = true;
                img_voice.setImageResource(R.drawable.voice_selected);

                is_video_selected = false;
                img_video.setImageResource(R.drawable.video_unselected);

                break;

            case R.id.img_video:

                is_video_selected = true;
                img_video.setImageResource(R.drawable.video_selected);

                is_voice_selected = false;
                img_voice.setImageResource(R.drawable.voice_unselected);

                break;

            case R.id.btn_next:

                if (is_voice_selected) {
                    startActivity(new Intent(SelectVoiceVideoActivity.this, VoiceRecorderActivity.class)
                            .putExtra("categoryId", categoryId)
                            .putExtra("expertId", expertId));
                }

                if (is_video_selected) {
                    startActivity(new Intent(SelectVoiceVideoActivity.this, VideoRecorderActivity.class)
                            .putExtra("categoryId", categoryId)
                            .putExtra("expertId", expertId));
                }

                break;
        }
    }
}
