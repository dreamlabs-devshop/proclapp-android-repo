package com.proclapp.home.voice_video;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.dialog.ExpertAnswerDialog;
import com.proclapp.model.AllPostsModel;
import com.proclapp.model.AnswerModel;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.DateTimeUtils;
import com.proclapp.utils.ImageLoadUtils;
import com.proclapp.utils.StringUtils;
import com.proclapp.utils.Utils;
import com.zyf.vc.ui.RecorderActivity;

import java.util.ArrayList;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class VVExpertReplyActivity extends AppCompatActivity implements View.OnClickListener {

    public static boolean isReplyAdd = false;
    public RelativeLayout rl_video;
    public LinearLayout ll_voice,
            ll_name,
            ll_option;
    public ImageView
            video_view,
            img_video,
            img_option,
            iv_play,
            img_back,
            iv_pause;
    public CircleImageView img_user_profile;
    public TextView txt_user_name,
            txt_user_place,
            txt_date,
            txt_title_value,
            txt_time,
            txt_answer,
            txt_total_answer_count,
            txt_record_time;
    private AllPostsModel audioVideoData;
    private RecyclerView rv_expert_answer;
    private ArrayList<AnswerModel> discussionList = new ArrayList<>();

    private TextView
            tv_give_answer;
    private MediaPlayer mPlayer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vvexpert_reply);
        Utils.setStatusBarColor(getWindow(), this, R.color.white);
        initViews();
        AudioManager am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        am.setMode(AudioManager.MODE_NORMAL);
        mPlayer = new MediaPlayer();


    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isReplyAdd || RecorderActivity.isVideoRecord) {
            callAPIGetDiscussion();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.img_back:
                onBackPressed();
                break;

            case R.id.tv_give_answer:
                new ExpertAnswerDialog(this, audioVideoData).show();
                break;

            case R.id.rl_video:
                if (!audioVideoData.getVvType().equalsIgnoreCase("1")) {

                    startActivity(new Intent(this, VideoPlayActivity.class)
                            .putExtra("audiovideourl", audioVideoData.getVideosPost().getVideoUrl()));

                }
                break;

            case R.id.iv_play:


                if (mPlayer != null)
                    mPlayer.release();

                onStreamAudio(audioVideoData.getAudioPost().getAudioUrl());
                iv_pause.setVisibility(View.VISIBLE);
                iv_play.setVisibility(View.GONE);

                break;

            case R.id.iv_pause:
                try {
                    iv_pause.setVisibility(View.GONE);
                    iv_play.setVisibility(View.VISIBLE);
                    mPlayer.stop();
                    mPlayer.release();
                } catch (Exception e) {
                    e.printStackTrace();
                }


        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 123) {
                if (data.getStringExtra("isPaymentSuccess").equalsIgnoreCase("1"))
                    callAPIGetDiscussion();
            }
        }

    }

    void initViews() {
        ll_voice = findViewById(R.id.ll_voice);
        ll_name = findViewById(R.id.ll_name);
        ll_option = findViewById(R.id.ll_option);
        rl_video = findViewById(R.id.rl_video);

        video_view = findViewById(R.id.video_view);
        img_back = findViewById(R.id.img_back);
        img_video = findViewById(R.id.img_video);
        img_user_profile = findViewById(R.id.img_user_profile);
        img_option = findViewById(R.id.img_option);
        iv_play = findViewById(R.id.iv_play);
        iv_pause = findViewById(R.id.iv_pause);
        txt_user_name = findViewById(R.id.txt_user_name);
        txt_user_place = findViewById(R.id.txt_user_place);
        txt_date = findViewById(R.id.txt_date);
        txt_title_value = findViewById(R.id.txt_title_value);
        txt_time = findViewById(R.id.txt_time);
        txt_record_time = findViewById(R.id.txt_record_time);
        txt_answer = findViewById(R.id.txt_answer);
        tv_give_answer = findViewById(R.id.tv_give_answer);
        txt_total_answer_count = findViewById(R.id.txt_total_answer_count);
        rv_expert_answer = findViewById(R.id.rv_expert_answer);
        rv_expert_answer.setNestedScrollingEnabled(false);

        txt_user_name.setTypeface(AppClass.lato_regular);
        txt_user_place.setTypeface(AppClass.lato_regular);
        txt_date.setTypeface(AppClass.lato_regular);
        txt_title_value.setTypeface(AppClass.lato_regular);
        txt_time.setTypeface(AppClass.lato_medium);
        txt_record_time.setTypeface(AppClass.lato_medium);

        ll_option.setOnClickListener(this);
        img_user_profile.setOnClickListener(this);
        txt_user_name.setOnClickListener(this);
        img_back.setOnClickListener(this);
        tv_give_answer.setOnClickListener(this);
        rl_video.setOnClickListener(this);
        iv_play.setOnClickListener(this);
        iv_pause.setOnClickListener(this);

        audioVideoData = (AllPostsModel) getIntent().getSerializableExtra("vvdata");

        if (audioVideoData.getVvType().equalsIgnoreCase("1")) {
            rl_video.setVisibility(View.GONE);
            ll_voice.setVisibility(View.VISIBLE);
            txt_answer.setText("Voice");

        } else {
            rl_video.setVisibility(View.VISIBLE);
            ll_voice.setVisibility(View.GONE);
            txt_answer.setText("Video");
            ImageLoadUtils.imageLoad(this,
                    img_video,
                    audioVideoData.getVideosPost().getVideoUrl());
        }

        ImageLoadUtils.imageLoad(this,
                img_user_profile,
                audioVideoData.getProfileImage(),
                R.drawable.menu_user_ph);

        if (StringUtils.isNotEmpty(audioVideoData.getAuthor()))
            txt_user_name.setText(audioVideoData.getAuthor());

        if (StringUtils.isNotEmpty(audioVideoData.getProfessionalQualification()))
            txt_user_place.setText(audioVideoData.getProfessionalQualification());

        if (StringUtils.isNotEmpty(audioVideoData.getCategoryName()))
            txt_title_value.setText(audioVideoData.getCategoryName());

        if (StringUtils.isNotEmpty(audioVideoData.getPostDate()))
            txt_date.setText(DateTimeUtils.changeDateTimeFormat("yyyy-MM-dd HH:mm:ss", "dd MMM", audioVideoData.getPostDate()));


        if (audioVideoData.getRequest_to_reply().equalsIgnoreCase("2") ||
                audioVideoData.getRequest_to_reply().equalsIgnoreCase("3")) {
            tv_give_answer.setVisibility(View.VISIBLE);
        } else {
            tv_give_answer.setVisibility(View.GONE);
        }

        setupRecyclerView();

    }

    private void setupRecyclerView() {
        rv_expert_answer.setLayoutManager(new LinearLayoutManager(this));
        rv_expert_answer.setAdapter(new ExpertAnswerAdapter(this, discussionList, (pos, tag) -> {

        }));
        callAPIGetDiscussion();
    }

    private void callAPIGetDiscussion() {

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("post_id", audioVideoData.getPostId());

        ApiCall.getInstance().getVVAnswerList(this, request, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {

                discussionList.clear();
                discussionList.addAll((ArrayList<AnswerModel>) data);

                txt_total_answer_count.setText(getResources().getQuantityString(R.plurals.reply, discussionList.size(), discussionList.size()));

                rv_expert_answer.setVisibility(View.VISIBLE);
                rv_expert_answer.getAdapter().notifyDataSetChanged();
                isReplyAdd = false;
            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {

                rv_expert_answer.setVisibility(View.GONE);
                txt_total_answer_count.setText(String.format("0 %s", getString(R.string.discussion)));
                isReplyAdd = false;
            }


        }, true);

    }

    private void onStreamAudio(String url) {
        mPlayer = new MediaPlayer();
        mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        // Listen for if the audio file can't be prepared
        mPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                // ... react appropriately ...
                // The MediaPlayer has moved to the Error state, must be reset!
                return false;
            }
        });
        mPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mPlayer.start();

            }
        });

        try {
            mPlayer.setDataSource(url);
            mPlayer.prepareAsync();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
