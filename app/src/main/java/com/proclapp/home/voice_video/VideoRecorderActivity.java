package com.proclapp.home.voice_video;

import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.home.adapter.VoiceRecordingListAdapter;
import com.proclapp.interfaces.RecyclerClickListener;
import com.proclapp.model.AllPostsModel;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.AlertDialogInterface;
import com.proclapp.utils.BottomSheetAskPermission;
import com.proclapp.utils.DialogUtils;
import com.zyf.vc.VideoPreferences;
import com.zyf.vc.ui.RecorderActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.RequestBody;

public class VideoRecorderActivity extends AppCompatActivity implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    private RecyclerView rv_video_recording;
    private VoiceRecordingListAdapter voiceRecordingListAdapter;
    private ImageView img_back;
    private TextView txt_video_recorder, txt_pending_video;
    private EditText edit_txt_description;
    private Button btn_create_video;
    private String expertId = "", categoryId = "";
    private ArrayList<AllPostsModel> videoList = new ArrayList<>();
    private SwipeRefreshLayout swipe_refresh;
    private TextView tv_no_data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_recorder);

        categoryId = getIntent().getStringExtra("categoryId");
        expertId = getIntent().getStringExtra("expertId");

        initView();

        setupRecyclerView();

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (RecorderActivity.isVideoRecord)
            getVideoList();
    }

    @Override
    public void onRefresh() {
        getVideoList();
    }

    private void initView() {

        tv_no_data = findViewById(R.id.tv_no_data);
        txt_pending_video = findViewById(R.id.txt_pending_video);
        img_back = findViewById(R.id.img_back);
        txt_video_recorder = findViewById(R.id.txt_video_recorder);
        btn_create_video = findViewById(R.id.btn_create_video);
        edit_txt_description = findViewById(R.id.edit_txt_description);

        rv_video_recording = findViewById(R.id.rv_video_recording);
        swipe_refresh = findViewById(R.id.swipe_refresh);

        img_back.setOnClickListener(this);
        btn_create_video.setOnClickListener(this);
        swipe_refresh.setOnRefreshListener(this);

        txt_video_recorder.setTypeface(AppClass.lato_semibold);
        btn_create_video.setTypeface(AppClass.lato_semibold);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.img_back:

                onBackPressed();
                break;

            case R.id.btn_create_video:

                new VideoPreferences(VideoRecorderActivity.this).storeToken(AppClass.preferences.getToken());
                new VideoPreferences(VideoRecorderActivity.this).storeUserId(AppClass.preferences.getUserId());

                checkPermissionAndGoNext();
                break;
        }
    }

    private void checkPermissionAndGoNext() {

        new BottomSheetAskPermission(VideoRecorderActivity.this, new BottomSheetAskPermission.PermissionResultListener() {
            @Override
            public void onAllPermissionAllow() {
                startActivity(new Intent(VideoRecorderActivity.this, RecorderActivity.class)
                        .putExtra("description", edit_txt_description.getText().toString())
                        .putExtra("categoryId", categoryId)
                        .putExtra("expertId", expertId));
            }

            @Override
            public void onPermissionDeny() {

            }
        }, BottomSheetAskPermission.WRITE_EXTERNAL_STORAGE, BottomSheetAskPermission.RECORD_AUDIO, BottomSheetAskPermission.CAMERA).show(getSupportFragmentManager(), "");

    }

    private void setupRecyclerView() {

        LinearLayoutManager layoutManager = new LinearLayoutManager(VideoRecorderActivity.this);
        rv_video_recording.setLayoutManager(layoutManager);

        voiceRecordingListAdapter = new VoiceRecordingListAdapter(VideoRecorderActivity.this, videoList, false, new RecyclerClickListener() {
            @Override
            public void onItemClick(int pos, String tag) {
                requestToExpert(videoList.get(pos).getPostId());
            }
        });
        rv_video_recording.setAdapter(voiceRecordingListAdapter);

        getVideoList();

    }

    private void getVideoList() {

        String userId = AppClass.preferences.getUserId();

        ApiCall.getInstance().audioVideoList(this, userId, "2", new IApiCallback() {
            @Override
            public void onSuccess(Object data) {
                videoList.clear();
                videoList.addAll((ArrayList<AllPostsModel>) data);
                voiceRecordingListAdapter.notifyDataSetChanged();

                if (swipe_refresh.isRefreshing())
                    swipe_refresh.setRefreshing(false);

                tv_no_data.setVisibility(View.GONE);
                rv_video_recording.setVisibility(View.VISIBLE);

            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {
                if (swipe_refresh.isRefreshing())
                    swipe_refresh.setRefreshing(false);
                if (errorMessage.equalsIgnoreCase("No data found")) {

                    tv_no_data.setVisibility(View.GONE);
                    rv_video_recording.setVisibility(View.GONE);

                } else {

                    DialogUtils.openDialog(VideoRecorderActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                        @Override
                        public void onNegativeBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }

                        @Override
                        public void onPositiveBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }
                    });
                }
            }
        }, false);


    }

    private void requestToExpert(String postId) {

        String userId = AppClass.preferences.getUserId();

        ApiCall.getInstance().requestToExpert(this, userId, postId, expertId, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {
                DialogUtils.openDialog(VideoRecorderActivity.this, (String) data, getString(R.string.ok), "", new AlertDialogInterface() {
                    @Override
                    public void onNegativeBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }

                    @Override
                    public void onPositiveBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                        finish();
                    }
                });
            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {
                DialogUtils.openDialog(VideoRecorderActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                    @Override
                    public void onNegativeBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }

                    @Override
                    public void onPositiveBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }
                });
            }
        }, true);

    }
}
