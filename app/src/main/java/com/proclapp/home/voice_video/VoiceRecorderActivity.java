package com.proclapp.home.voice_video;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.home.adapter.VoiceRecordingListAdapter;
import com.proclapp.interfaces.RecyclerClickListener;
import com.proclapp.model.AllPostsModel;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.AlertDialogInterface;
import com.proclapp.utils.DialogUtils;

import java.util.ArrayList;

public class VoiceRecorderActivity extends AppCompatActivity implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    private RecyclerView rv_voice_recording;
    private VoiceRecordingListAdapter voiceRecordingListAdapter;
    private ImageView img_back;
    private EditText edit_txt_description;
    private TextView txt_voice_recorder;
    private Button btn_start_recording;
    private String expertId = "", categoryId = "";
    private ArrayList<AllPostsModel> audioList = new ArrayList<>();
    private TextView tv_no_data;
    private SwipeRefreshLayout swipe_refresh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voice_recorder);

        initView();
        initStyle();
        initListener();

    }

    @Override
    public void onRefresh() {
        getAudioList();
    }

    private void initView() {

        img_back = findViewById(R.id.img_back);
        txt_voice_recorder = findViewById(R.id.txt_voice_recorder);
        tv_no_data = findViewById(R.id.tv_no_data);
        btn_start_recording = findViewById(R.id.btn_start_recording);
        swipe_refresh = findViewById(R.id.swipe_refresh);
        edit_txt_description = findViewById(R.id.edit_txt_description);

        categoryId = getIntent().getStringExtra("categoryId");
        expertId = getIntent().getStringExtra("expertId");

        rv_voice_recording = findViewById(R.id.rv_voice_recording);

        swipe_refresh.setOnRefreshListener(this);

        setupRecyclerView();

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (VoiceRecordingActivityNew.isVoiceRecord) {
            getAudioList();
            VoiceRecordingActivityNew.isVoiceRecord = false;
        }

    }

    private void initStyle() {

        txt_voice_recorder.setTypeface(AppClass.lato_semibold);
        btn_start_recording.setTypeface(AppClass.lato_semibold);
    }

    private void initListener() {

        img_back.setOnClickListener(this);
        btn_start_recording.setOnClickListener(this);
    }

    private void setupRecyclerView() {

        LinearLayoutManager layoutManager = new LinearLayoutManager(VoiceRecorderActivity.this);
        rv_voice_recording.setLayoutManager(layoutManager);

        voiceRecordingListAdapter = new VoiceRecordingListAdapter(VoiceRecorderActivity.this, audioList, true, new RecyclerClickListener() {
            @Override
            public void onItemClick(int pos, String tag) {
                requestToExpert(audioList.get(pos).getPostId());
            }
        });
        rv_voice_recording.setAdapter(voiceRecordingListAdapter);

        getAudioList();

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.img_back:
                onBackPressed();
                break;

            case R.id.btn_start_recording:
                startActivity(new Intent(VoiceRecorderActivity.this, VoiceRecordingActivityNew.class)
                        .putExtra("description", edit_txt_description.getText().toString())
                        .putExtra("categoryId", categoryId)
                        .putExtra("expertId", expertId));
                break;
        }
    }

    private void getAudioList() {

        if (AppClass.isInternetConnectionAvailable()) {

            String userId = AppClass.preferences.getUserId();

            ApiCall.getInstance().audioVideoList(this, userId, "1", new IApiCallback() {
                @Override
                public void onSuccess(Object data) {
                    audioList.clear();
                    audioList.addAll((ArrayList<AllPostsModel>) data);
                    voiceRecordingListAdapter.notifyDataSetChanged();
                    tv_no_data.setVisibility(View.GONE);
                    rv_voice_recording.setVisibility(View.VISIBLE);
                    if (swipe_refresh.isRefreshing())
                        swipe_refresh.setRefreshing(false);
                }

                @Override
                public void onFailure(boolean isFalseFromService, String errorMessage) {
                    if (swipe_refresh.isRefreshing())
                        swipe_refresh.setRefreshing(false);
                    if (errorMessage.equalsIgnoreCase("No data found")) {

                        tv_no_data.setVisibility(View.GONE);
                        rv_voice_recording.setVisibility(View.GONE);

                    } else {

                        DialogUtils.openDialog(VoiceRecorderActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                            @Override
                            public void onNegativeBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }

                            @Override
                            public void onPositiveBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }
                        });
                    }

                }
            }, false);

        } else {
            AppClass.snackBarView.snackBarShow(VoiceRecorderActivity.this, getString(R.string.nonetwork));
        }

    }

    private void requestToExpert(String postId) {

        String userId = AppClass.preferences.getUserId();

        ApiCall.getInstance().requestToExpert(this, userId, postId, expertId, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {
                DialogUtils.openDialog(VoiceRecorderActivity.this, (String) data, getString(R.string.ok), "", new AlertDialogInterface() {
                    @Override
                    public void onNegativeBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }

                    @Override
                    public void onPositiveBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                        finish();
                    }
                });
            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {
                DialogUtils.openDialog(VoiceRecorderActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                    @Override
                    public void onNegativeBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }

                    @Override
                    public void onPositiveBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }
                });
            }
        }, true);

    }


}
