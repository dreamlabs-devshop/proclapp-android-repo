package com.proclapp.home.voice_video;

import android.media.MediaRecorder;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.SystemClock;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.BottomSheetAskPermission;
import com.proclapp.utils.Logger;

import java.io.File;
import java.util.HashMap;
import okhttp3.MediaType;
import okhttp3.RequestBody;

public class VoiceRecordingActivity extends AppCompatActivity implements View.OnClickListener {

    private String
            mFilePath,
            mFileName;
    private long
            mStartingTimeMillis,
            mElapsedMillis;
    private ImageView
            img_back,
            img_pause,
            img_stop,
            img_play;
    private TextView
            txt_voice_recording,
            txt_timer;
    private Chronometer chronometer;
    private MediaRecorder mRecorder = null;
    private boolean isPaused = false;
    private long timeWhenPause = 0;
    private boolean isRecordStart = false;
    private String expertId = "", categoryId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voice_recording);
        checkPermissionAndGoNext(false);
        initView();
        initStyle();
        initListener();
    }

    private void initView() {

        img_back = findViewById(R.id.img_back);
        img_pause = findViewById(R.id.img_pause);
        img_stop = findViewById(R.id.img_stop);
        img_play = findViewById(R.id.img_play);
        txt_voice_recording = findViewById(R.id.txt_voice_recording);
        chronometer = findViewById(R.id.chronometer);

        categoryId = getIntent().getStringExtra("categoryId");
        expertId = getIntent().getStringExtra("expertId");

    }

    private void initStyle() {
        txt_voice_recording.setTypeface(AppClass.lato_regular);

    }

    private void initListener() {

        img_back.setOnClickListener(this);
        img_pause.setOnClickListener(this);
        img_stop.setOnClickListener(this);
        img_play.setOnClickListener(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.img_back:
                onBackPressed();
                break;

            case R.id.img_play:
                checkPermissionAndGoNext(true);
                break;

            case R.id.img_pause:
                pauseRecord(isPaused);
                break;

            case R.id.img_stop:
                if (isRecordStart) {
                    stopRecording();
                }
                break;
        }
    }

    private void startRecord() {

        // start recording

        img_play.setVisibility(View.GONE);
        img_pause.setVisibility(View.VISIBLE);

        //mPauseButton.setVisibility(View.VISIBLE);
        Toast.makeText(VoiceRecordingActivity.this, R.string.toast_recording_start, Toast.LENGTH_SHORT).show();
        File folder = new File(Environment.getExternalStorageDirectory().toString() + "/" + getString(R.string.app_name));
        if (!folder.exists()) {
            //folder /SoundRecorder doesn't exist, create the folder
            folder.mkdir();
        }

        //start Chronometer
        chronometer.setBase(SystemClock.elapsedRealtime());
        chronometer.start();

        VoiceRecordingActivity.this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        startRecording();


    }


    //TODO: implement pause recording
    @RequiresApi(api = Build.VERSION_CODES.N)
    private void pauseRecord(boolean pause) {
        if (!pause) {
            //pause recording
            img_pause.setImageDrawable(getResources().getDrawable(R.drawable.play));
            timeWhenPause = chronometer.getBase() - SystemClock.elapsedRealtime();
            chronometer.stop();
            mRecorder.pause();

        } else {
            //resume recording
            img_pause.setImageDrawable(getResources().getDrawable(R.drawable.pause_btn));
            chronometer.setBase(SystemClock.elapsedRealtime() + timeWhenPause);
            chronometer.start();
            mRecorder.resume();

        }
        isPaused = !pause;


    }

    public void startRecording() {
        int count = 0;
        File f;

        String fileStorePath = "";
        do {
            count++;

            mFileName = getString(R.string.app_name) + "_" + System.currentTimeMillis();
            mFilePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + getString(R.string.app_name);
            fileStorePath = mFilePath + "/" + mFileName;


            f = new File(mFilePath);
        } while (f.exists() && !f.isDirectory());

        isRecordStart = true;
        mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.DEFAULT);
        mRecorder.setOutputFile(fileStorePath);
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.DEFAULT);
        mRecorder.setAudioChannels(1);
        mRecorder.setAudioSamplingRate(44100);
        mRecorder.setAudioEncodingBitRate(192000);

        try {
            mRecorder.prepare();
            mRecorder.start();
            mStartingTimeMillis = System.currentTimeMillis();

        } catch (Exception e) {
            Log.e("", "prepare() failed");
        }
    }



    public void stopRecording() {
        chronometer.stop();
        chronometer.setBase(SystemClock.elapsedRealtime());
        mRecorder.stop();
        mElapsedMillis = (System.currentTimeMillis() - mStartingTimeMillis);
        mRecorder.release();
        Toast.makeText(this, getString(R.string.toast_recording_finish), Toast.LENGTH_LONG).show();
        mRecorder = null;

        Logger.e("FilePath " + mFilePath);

        addVoiceVideo();

    }


    private void checkPermissionAndGoNext(final boolean startRecording) {

        new BottomSheetAskPermission(VoiceRecordingActivity.this, new BottomSheetAskPermission.PermissionResultListener() {
            @Override
            public void onAllPermissionAllow() {

                if (startRecording) {
                    startRecord();
                }

            }

            @Override
            public void onPermissionDeny() {

            }
        }, BottomSheetAskPermission.WRITE_EXTERNAL_STORAGE, BottomSheetAskPermission.RECORD_AUDIO, BottomSheetAskPermission.CAMERA).show(getSupportFragmentManager(), "");

    }

    private void addVoiceVideo() {

        HashMap<String, RequestBody> request = new HashMap<>();
        request.put("user_id", RequestBody.create(MediaType.parse("multipart/form-data"), AppClass.preferences.getUserId()));
        request.put("vv_type", RequestBody.create(MediaType.parse("multipart/form-data"), "1"));
        request.put("post_category", RequestBody.create(MediaType.parse("multipart/form-data"), categoryId));
        request.put("expert", RequestBody.create(MediaType.parse("multipart/form-data"), expertId));

        ApiCall.getInstance().addVVPost(VoiceRecordingActivity.this, request, mFilePath, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {

                AppClass.toastView.ToastShow(VoiceRecordingActivity.this, (String) data);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        onBackPressed();
                    }
                }, 1000);


            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {

                AppClass.snackBarView.snackBarShow(VoiceRecordingActivity.this, errorMessage);

            }
        }, true);

    }


}
