package com.proclapp.home.voice_video;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.view.WindowManager;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.czt.mp3recorder.MP3Recorder;
import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.BottomSheetAskPermission;
import com.proclapp.utils.Logger;
import com.shuyu.waveview.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.UUID;

import okhttp3.MediaType;
import okhttp3.RequestBody;

public class VoiceRecordingActivityNew extends AppCompatActivity implements View.OnClickListener {

    public static boolean isVoiceRecord = false;
    boolean mIsRecord = false;
    boolean mIsPlay = false;
    private String
            mFilePath,
            mFileName;
    private long
            mStartingTimeMillis,
            mElapsedMillis;
    private ImageView
            img_back,
            img_pause,
            img_stop,
            img_play;
    private TextView
            txt_voice_recording,
            txt_timer;
    private Chronometer chronometer;
    private MP3Recorder mRecorder = null;
    private boolean isPaused = false;
    private long timeWhenPause = 0;
    private boolean isRecordStart = false;
    private String expertId = "", categoryId = "", postDescription;

    /**
     * dip转为PX
     */
    public static int dip2px(Context context, float dipValue) {
        float fontScale = context.getResources().getDisplayMetrics().density;
        return (int) (dipValue * fontScale + 0.5f);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voice_recording);
        checkPermissionAndGoNext(false);
        initView();
        initStyle();
        initListener();
    }

    private void initView() {

        img_back = findViewById(R.id.img_back);
        img_pause = findViewById(R.id.img_pause);
        img_stop = findViewById(R.id.img_stop);
        img_play = findViewById(R.id.img_play);
        txt_voice_recording = findViewById(R.id.txt_voice_recording);
        chronometer = findViewById(R.id.chronometer);

        categoryId = getIntent().getStringExtra("categoryId");
        expertId = getIntent().getStringExtra("expertId");
        postDescription = getIntent().getStringExtra("description");

    }

    private void initStyle() {
        txt_voice_recording.setTypeface(AppClass.lato_regular);

    }

    private void initListener() {

        img_back.setOnClickListener(this);
        img_pause.setOnClickListener(this);
        img_stop.setOnClickListener(this);
        img_play.setOnClickListener(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.img_back:
                onBackPressed();
                break;

            case R.id.img_play:
                checkPermissionAndGoNext(true);
                break;

            case R.id.img_pause:
                resolvePause();
                break;

            case R.id.img_stop:
                resolveStopRecord();
                break;
        }
    }

    /**
     * 开始录音
     */
    private void resolveRecord() {

        img_play.setVisibility(View.GONE);
        img_pause.setVisibility(View.VISIBLE);

        //start Chronometer
        chronometer.setBase(SystemClock.elapsedRealtime());
        chronometer.start();

        VoiceRecordingActivityNew.this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        mFilePath = FileUtils.getAppPath();
        File file = new File(mFilePath);
        if (!file.exists()) {
            if (!file.mkdirs()) {
                Toast.makeText(this, "Failed to create file", Toast.LENGTH_SHORT).show();
                return;
            }
        }

        int offset = dip2px(VoiceRecordingActivityNew.this, 1);
        mFilePath = FileUtils.getAppPath() + UUID.randomUUID().toString() + ".mp3";
        mRecorder = new MP3Recorder(new File(mFilePath));


        mRecorder.setErrorHandler(new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if (msg.what == MP3Recorder.ERROR_TYPE) {
                    Toast.makeText(VoiceRecordingActivityNew.this, "No microphone permission", Toast.LENGTH_SHORT).show();
                    resolveError();
                }
            }
        });


        try {
            mRecorder.start();
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(VoiceRecordingActivityNew.this, "Recording is abnormal", Toast.LENGTH_SHORT).show();
            resolveError();
            return;
        }
        mIsRecord = true;
    }

    /**
     * 录音异常
     */
    private void resolveError() {
        chronometer.stop();
        chronometer.setBase(SystemClock.elapsedRealtime());
        mElapsedMillis = (System.currentTimeMillis() - mStartingTimeMillis);
        Toast.makeText(this, getString(R.string.toast_recording_finish), Toast.LENGTH_LONG).show();
        Logger.e("FilePath " + mFilePath);

        FileUtils.deleteFile(mFilePath);
        mFilePath = "";
        if (mRecorder != null && mRecorder.isRecording()) {
            mRecorder.stop();
        }
    }

    /**
     * 停止录音
     */
    private void resolveStopRecord() {
        if (mRecorder != null && mRecorder.isRecording()) {
            mRecorder.setPause(false);
            mRecorder.stop();
            chronometer.stop();
            if (getIntent().hasExtra("openfor")) {
                addExpertReply();
            } else {
                addVoiceVideo();
            }
        }
        mIsRecord = false;
    }

    /**
     * 暂停
     */
    private void resolvePause() {
        if (!mIsRecord)
            return;

        if (mRecorder.isPause()) {

            img_pause.setImageDrawable(getResources().getDrawable(R.drawable.pause_btn));
            chronometer.setBase(SystemClock.elapsedRealtime() + timeWhenPause);
            chronometer.start();
            mRecorder.setPause(false);

        } else {
            img_pause.setImageDrawable(getResources().getDrawable(R.drawable.play));
            timeWhenPause = chronometer.getBase() - SystemClock.elapsedRealtime();
            chronometer.stop();
            mRecorder.setPause(true);

        }
    }


    private void checkPermissionAndGoNext(final boolean startRecording) {

        new BottomSheetAskPermission(VoiceRecordingActivityNew.this, new BottomSheetAskPermission.PermissionResultListener() {
            @Override
            public void onAllPermissionAllow() {

                if (startRecording)
                    resolveRecord();
            }

            @Override
            public void onPermissionDeny() {

            }
        }, BottomSheetAskPermission.WRITE_EXTERNAL_STORAGE, BottomSheetAskPermission.RECORD_AUDIO, BottomSheetAskPermission.CAMERA).show(getSupportFragmentManager(), "");

    }

    private void addVoiceVideo() {

        HashMap<String, RequestBody> request = new HashMap<>();
        request.put("user_id", RequestBody.create(MediaType.parse("multipart/form-data"), AppClass.preferences.getUserId()));
        request.put("vv_type", RequestBody.create(MediaType.parse("multipart/form-data"), "1"));
        request.put("post_category", RequestBody.create(MediaType.parse("multipart/form-data"), categoryId));
        request.put("post_description", RequestBody.create(MediaType.parse("multipart/form-data"), postDescription));
        request.put("expert", RequestBody.create(MediaType.parse("multipart/form-data"), expertId));

        ApiCall.getInstance().addVVPost(VoiceRecordingActivityNew.this, request, mFilePath, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {

                isVoiceRecord = true;
                AppClass.toastView.ToastShow(VoiceRecordingActivityNew.this, "Audio uploaded successfully.");
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        onBackPressed();
                    }
                }, 1000);


            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {

                AppClass.snackBarView.snackBarShow(VoiceRecordingActivityNew.this, errorMessage);

            }
        }, true);

    }

    private void addExpertReply() {

        HashMap<String, RequestBody> request = new HashMap<>();
        request.put("user_id", RequestBody.create(MediaType.parse("multipart/form-data"), AppClass.preferences.getUserId()));
        request.put("post_id", RequestBody.create(MediaType.parse("multipart/form-data"), getIntent().getStringExtra("postId")));
        request.put("requested_user_id", RequestBody.create(MediaType.parse("multipart/form-data"), getIntent().getStringExtra("requestedUserId")));
        request.put("vv_type", RequestBody.create(MediaType.parse("multipart/form-data"), "1"));
        request.put("type", RequestBody.create(MediaType.parse("multipart/form-data"), "3"));


        ApiCall.getInstance().expertReply(VoiceRecordingActivityNew.this, request, mFilePath, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {

                VVExpertReplyActivity.isReplyAdd = true;
                AppClass.toastView.ToastShow(VoiceRecordingActivityNew.this, "Replied successfully.");
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        onBackPressed();
                    }
                }, 1000);


            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {

                AppClass.snackBarView.snackBarShow(VoiceRecordingActivityNew.this, errorMessage);

            }
        }, true);

    }


}
