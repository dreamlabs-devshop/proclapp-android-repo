package com.proclapp.home.voice_video;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.home.DiscussActivity;
import com.proclapp.home.PostDiscussionActivity;
import com.proclapp.home.adapter.AllPostsAdapter;
import com.proclapp.home.adapter.DiscussionAdapter;
import com.proclapp.home.adapter.VoiceAndVideoAdapter;
import com.proclapp.home.adapter.viewholder.VoiceVideoViewHolder;
import com.proclapp.interfaces.RecyclerClickListener;
import com.proclapp.model.AllPostsModel;
import com.proclapp.model.AnswerModel;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.DateTimeUtils;
import com.proclapp.utils.ImageLoadUtils;
import com.proclapp.utils.StringUtils;
import com.proclapp.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.proclapp.home.DiscussActivity.isDiscussionPost;

public class VoiceVideoDetails extends AppCompatActivity implements View.OnClickListener {

    public RelativeLayout rl_video;
    public LinearLayout ll_voice,
            ll_name,
            ll_option, ll_discuss;
    public ImageView video_view,
            img_video,
            img_option,
            iv_play,
            iv_pause;
    private ImageView img_sort_answer, img_back;
    public CircleImageView img_user_profile;
    public TextView txt_user_name,
            txt_user_place,
            txt_date,
            txt_title_value,
            txt_time,
            txt_record_time,
            tv_give_answer,
            txt_like,
            txt_description,
            txt_comment,
            txt_share,
            txt_comments_count,
            txt_total_answer_count,
            tv_see_answer;

    private RecyclerView rv_all_comments;


    //VoiceAndVideoAdapter voiceAndVideoAdapter;
    VoiceAndVideoAdapter voiceAndVideoAdapter;
    private AllPostsModel audioVideoData;
    private MediaPlayer mPlayer;
    private ArrayList<AnswerModel> commentList = new ArrayList<>();
    private String postId = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voice_video_details);
        Utils.setStatusBarColor(getWindow(), this, R.color.white);
        initView();
        setData();
        increaseDrawableSizes();
        initListener();


        setUpDiscussionList();
    }

    @Override
    protected void onResume() {
        super.onResume();
        callAPIGetDiscussion();
    }

    private void initView() {
        //this.voiceAndVideoAdapter = new VoiceAndVideoAdapter();

        ll_voice = findViewById(R.id.ll_voice);
        ll_name = findViewById(R.id.ll_name);
        ll_option = findViewById(R.id.ll_option);
        rl_video = findViewById(R.id.rl_video);
        ll_discuss = findViewById(R.id.ll_discuss);


        video_view = findViewById(R.id.video_view);
        img_back = findViewById(R.id.img_back);
        img_video = findViewById(R.id.img_video);
        img_user_profile = findViewById(R.id.img_user_profile);
        img_option = findViewById(R.id.img_option);
        iv_play = findViewById(R.id.iv_play);
        iv_pause = findViewById(R.id.iv_pause);
        txt_user_name = findViewById(R.id.txt_user_name);
        txt_user_place = findViewById(R.id.txt_user_place);
        txt_date = findViewById(R.id.txt_date);
        txt_title_value = findViewById(R.id.txt_title_value);
        txt_time = findViewById(R.id.txt_time);
        txt_record_time = findViewById(R.id.txt_record_time);
        tv_see_answer = findViewById(R.id.tv_see_answer);
        tv_give_answer = findViewById(R.id.tv_give_answer);
        txt_total_answer_count = findViewById(R.id.txt_total_answer_count);

        txt_like = findViewById(R.id.txt_like);
        txt_description = findViewById(R.id.txt_description);
        txt_comment = findViewById(R.id.txt_comment);
        txt_share = findViewById(R.id.txt_share);
        txt_comments_count = findViewById(R.id.txt_comments_count);

        rv_all_comments = findViewById(R.id.rv_all_comments);
        img_sort_answer = findViewById(R.id.img_sort_answer);

            /*RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams((metrics.widthPixels * 448) / 480, (metrics.heightPixels * 140) / 800);
            video_view.setLayoutParams(layoutParams);
            video_view.setLayoutParams(layoutParams);*/
        txt_comment.setTypeface(AppClass.lato_regular);
        txt_like.setTypeface(AppClass.lato_regular);
        txt_comments_count.setTypeface(AppClass.lato_regular);

        txt_user_name.setTypeface(AppClass.lato_regular);
        txt_user_place.setTypeface(AppClass.lato_regular);
        txt_date.setTypeface(AppClass.lato_regular);
        txt_title_value.setTypeface(AppClass.lato_regular);
        txt_time.setTypeface(AppClass.lato_medium);
        txt_record_time.setTypeface(AppClass.lato_medium);
        tv_give_answer.setTypeface(AppClass.lato_medium);
        tv_see_answer.setTypeface(AppClass.lato_regular);

        audioVideoData = (AllPostsModel) getIntent().getSerializableExtra("audioVideoDetails");
        postId = audioVideoData.getPostId();


    }

    private void initListener() {
        ll_option.setOnClickListener(this);
        img_user_profile.setOnClickListener(this);
        txt_user_name.setOnClickListener(this);
        tv_see_answer.setOnClickListener(this);
        tv_give_answer.setOnClickListener(this);
        txt_like.setOnClickListener(this);
        txt_share.setOnClickListener(this);
        iv_play.setOnClickListener(this);
        iv_pause.setOnClickListener(this);
        ll_discuss.setOnClickListener(this);
        img_back.setOnClickListener(this);
        video_view.setOnClickListener(this);
    }

    private void setUpDiscussionList() {

        rv_all_comments.setLayoutManager(new LinearLayoutManager(this));
        rv_all_comments.setAdapter(new DiscussionAdapter(this, commentList, new RecyclerClickListener() {
            @Override
            public void onItemClick(int pos, String tag) {

                if (tag.equalsIgnoreCase("edit")) {
                    startActivity(new Intent(VoiceVideoDetails.this, PostDiscussionActivity.class)
                            .putExtra("postId", audioVideoData.getPostId())
                            .putExtra("discuss", commentList.get(pos)));
                }

            }
        }));

        callAPIGetDiscussion();
    }

    private void setData() {

        //holder.txt_comments_count.setText(voiceVideoList.get(holder.getAdapterPosition()).getAnswer_comment_count());
        if (StringUtils.isNotEmpty(audioVideoData.getIsPostLiked()) &&
                audioVideoData.getIsPostLiked().equalsIgnoreCase("true")) {
            txt_like.setSelected(true);
        } else {
            txt_like.setSelected(false);
        }
        txt_like.setText(audioVideoData.getPostLikeCount());

        if (StringUtils.isNotEmpty(audioVideoData.getTotal_answers()))
            txt_comments_count.setText(audioVideoData.getTotal_answers());
        else
            txt_comments_count.setText("0");


        if (audioVideoData.getVvType().equalsIgnoreCase("1")) {
            rl_video.setVisibility(View.GONE);
            ll_voice.setVisibility(View.VISIBLE);


        } else {
            rl_video.setVisibility(View.VISIBLE);
            ll_voice.setVisibility(View.GONE);

            ImageLoadUtils.imageLoad(this,
                    img_video,
                    audioVideoData.getVideosPost().getVideoUrl());
        }

        ImageLoadUtils.imageLoad(this,
                img_user_profile,
                audioVideoData.getProfileImage(),
                R.drawable.menu_user_ph);

        if (StringUtils.isNotEmpty(audioVideoData.getPostDescription()))
            txt_description.setText(audioVideoData.getPostDescription());

        if (StringUtils.isNotEmpty(audioVideoData.getAuthor()))
            txt_user_name.setText(audioVideoData.getAuthor());

        if (StringUtils.isNotEmpty(audioVideoData.getProfessionalQualification()))
            txt_user_place.setText(audioVideoData.getProfessionalQualification());

        if (StringUtils.isNotEmpty(audioVideoData.getCategoryName()))
            txt_title_value.setText(audioVideoData.getCategoryName());

        if (StringUtils.isNotEmpty(audioVideoData.getPostDate()))
            txt_date.setText(DateTimeUtils.calculateTimeBetweenTwoDates(this, audioVideoData.getPostDate(), false));

        //txt_date.setText(DateTimeUtils.changeDateTimeFormat("yyyy-MM-dd HH:mm:ss", "dd MMM", audioVideoData.getPostDate()));

        if (audioVideoData.getRequest_to_reply().equalsIgnoreCase("2") ||
                audioVideoData.getRequest_to_reply().equalsIgnoreCase("3")) {
            tv_see_answer.setVisibility(View.VISIBLE);
            tv_give_answer.setVisibility(View.VISIBLE);
        } else if (audioVideoData.getRequest_to_reply().equalsIgnoreCase("1") ||
                audioVideoData.getRequest_to_reply().equalsIgnoreCase("4")) {
            tv_see_answer.setVisibility(View.VISIBLE);
            tv_give_answer.setVisibility(View.GONE);
        } else {
            tv_see_answer.setVisibility(View.GONE);
            tv_give_answer.setVisibility(View.GONE);
        }

        AudioManager am = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
        am.setMode(AudioManager.MODE_NORMAL);
        mPlayer = new MediaPlayer();

    }

    private void increaseDrawableSizes() {
        txt_like.setCompoundDrawables(getDrawableAndResize(R.drawable.selector_like_post), null, null, null);
        txt_comment.setCompoundDrawables(getDrawableAndResize(R.drawable.discuss), null, null, null);
        txt_share.setCompoundDrawables(getDrawableAndResize(R.drawable.share_blue), null, null, null);
        txt_comments_count.setCompoundDrawables(getDrawableAndResize(R.drawable.comments), null, null, null);
    }

    private Drawable getDrawableAndResize(int drawableId) {
        Drawable drawable = ContextCompat.getDrawable(this, drawableId);
        drawable.setBounds(0,
                0,
                (int) (drawable.getIntrinsicWidth() * 1.4),
                (int) (drawable.getIntrinsicHeight() * 1.4));

        return drawable;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.video_view:
                startActivity(new Intent(this, VideoPlayActivity.class)
                        .putExtra("audiovideourl", audioVideoData.getVideosPost().getVideoUrl()));
                break;
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.txt_like:
                callAPILikeUnlike(txt_like.isSelected() ? "0" : "1");
                break;
            case R.id.txt_share:
                String path = audioVideoData.getVvType().equals("1") ? audioVideoData.getAudioPost().getAudioUrl() :
                        audioVideoData.getVideosPost().getVideoUrl();

                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("*/*");
                sharingIntent.putExtra(Intent.EXTRA_TEXT, path);
                startActivity(Intent.createChooser(sharingIntent, getString(R.string.share_using)));
                break;
            case R.id.iv_play:
                if (mPlayer != null)
                    mPlayer.release();

                onStreamAudio(audioVideoData.getAudioPost().getAudioUrl());
                iv_pause.setVisibility(View.VISIBLE);
                iv_play.setVisibility(View.GONE);
                break;
            case R.id.iv_pause:
                try {
                    iv_pause.setVisibility(View.GONE);
                    iv_play.setVisibility(View.VISIBLE);
                    mPlayer.stop();
                    mPlayer.release();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.ll_discuss:
                startActivity(new Intent(this, PostDiscussionActivity.class)
                        .putExtra("postId", audioVideoData.getPostId()).putExtra("isPostVV", "true"));
                break;
        }

    }

    private void onStreamAudio(String url) {
        mPlayer = new MediaPlayer();
        mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        // Listen for if the audio file can't be prepared
        mPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                // ... react appropriately ...
                // The MediaPlayer has moved to the Error state, must be reset!
                return false;
            }
        });
        mPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mPlayer.start();

            }
        });

        try {
            mPlayer.setDataSource(url);
            mPlayer.prepareAsync();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void callAPILikeUnlike(final String status) {
        Log.d("Call like unlike", "clicked like button");

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("post_id", audioVideoData.getPostId());
        request.put("like_unlike", status);

        ApiCall.getInstance().likeUnlike(this, request, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {
                String response = (String) data;


                int count;

                if (status.equalsIgnoreCase("0")) {
                    txt_like.setSelected(false);
                    count = Integer.parseInt(audioVideoData.getPostLikeCount()) - 1;

                } else {
                    txt_like.setSelected(true);
                    count = Integer.parseInt(audioVideoData.getPostLikeCount()) + 1;


                }
                audioVideoData.setPostLikeCount(String.valueOf(count));
                txt_like.setText(String.valueOf(count));


            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {
                /*DialogUtils.openDialog(activity, errorMessage, activity.getString(R.string.ok), "", new AlertDialogInterface() {
                    @Override
                    public void onNegativeBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }

                    @Override
                    public void onPositiveBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }
                });*/
            }
        }, false);


//        if (status.equalsIgnoreCase("0")) {
//            audioVideoData.setIsPostLiked("false");
//            int count = Integer.parseInt(audioVideoData.getPostLikeCount()) - 1;
//            audioVideoData.setPostLikeCount("" + count);
//        } else {
//            audioVideoData.setIsPostLiked("true");
//            int count = Integer.parseInt(audioVideoData.getPostLikeCount()) + 1;
//            audioVideoData.setPostLikeCount("" + count);
//        }
    }

    private void callAPIGetDiscussion() {

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("post_id", postId);

        ApiCall.getInstance().getAnswerList(VoiceVideoDetails.this, request, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {

                commentList.clear();
                commentList.addAll((ArrayList<AnswerModel>) data);

                if (commentList.size() > 1) {
                    txt_total_answer_count.setText(String.format("%s %s", String.valueOf(commentList.size()), getString(R.string.comments)));
                } else {
                    txt_total_answer_count.setText(String.format("%s %s", String.valueOf(commentList.size()), getString(R.string.comment)));
                }

                img_sort_answer.setVisibility(View.VISIBLE);
                rv_all_comments.setVisibility(View.VISIBLE);
                rv_all_comments.getAdapter().notifyDataSetChanged();

                isDiscussionPost = false;

            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {

                rv_all_comments.setVisibility(View.GONE);
                txt_total_answer_count.setText(String.format("0 %s", getString(R.string.comments)));
                img_sort_answer.setVisibility(View.INVISIBLE);

            }


        }, true);

    }
}