package com.proclapp.imagecrop;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import com.proclapp.utils.StringUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Calendar;

public class SdcardUtils {

    public static String BROADCAST_IMAGE_ACTION = "";
    public static boolean isImageUploading = false;

    public static final int ACTION_TAKE_PHOTO = 1001;
    public static final int ACTION_PICK_FROM_GALLERY = 1002;
    public static final int RETURN_INTENT = 1003;

    public static Uri CAMERA_IMAGE_URI;
    public static Uri CAMERA_VIDEO_URI;
    public static File MEDIA_FILE_ORIGINAL = null;
    public static String ORIGINAL_IMAGE_PATH = "";
    public static File CROPED_IMAGE_PATH = null;
    public static File CROPED_IMAGE_THUMB = null;

    public static String TRICE_IMAGE_THUMB = "IVIOU/Thumb";
    public static String TRICE_IMAGE_IMAGES = "IVIOU/Images";
    public static String TRICE_VIDEO = "IVIOU/Video";


    /**
     * if folder doesn't exists it creates a folder
     * or if it exists it return its path
     *
     * @param folderPath folder name
     * @return full folder path
     */
    public static String createFolder(String folderPath) {
        String folderName = "";

        folderName = Environment.getExternalStorageDirectory() + "/" + folderPath;
        Log.d("", "folderName : " + folderName);
        File fileName = new File(folderName);
        if (!fileName.exists()) {
            fileName.mkdirs();
        }
        return folderName;
    }

     /**
     * if folder doesn't exists it creates a folder
     * or if it exists it return its path
     *
     * @param folderPath folder name
     * @return full folder path
     */
    public static File createFolderRFile(String folderPath) {
        String folderName = "";

        folderName = Environment.getExternalStorageDirectory() + "/" + folderPath;
        Log.d("", "folderName : " + folderName);
        File fileName = new File(folderName);
        if (!fileName.exists()) {
            fileName.mkdirs();
        }
        return fileName;
    }



    /**
     * Delete Images
     * When profile pic upload at that time temporary image will store in particular path
     * so it is unused image after upload
     * so delete it
     */
    public static void deleteDataFromFolder() {
        // Delete Images inside "Thumbs" folder
        File dir = new File(Environment.getExternalStorageDirectory() + "/" + TRICE_IMAGE_THUMB);
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                new File(dir, children[i]).delete();
            }
        }

        // Delete Images inside "Images" folder
        File dir1 = new File(Environment.getExternalStorageDirectory() + "/" + TRICE_IMAGE_IMAGES);
        if (dir1.isDirectory()) {
            String[] children = dir1.list();
            for (int i = 0; i < children.length; i++) {
                new File(dir1, children[i]).delete();
            }
        }

        // Delete Images inside "video" folder
        File dir3 = new File(Environment.getExternalStorageDirectory() + "/" + TRICE_VIDEO);
        if (dir3.isDirectory()) {
            String[] children = dir3.list();
            for (int i = 0; i < children.length; i++) {
                new File(dir3, children[i]).delete();
            }
        }
    }


    /**
     * @return image file name with .jpeg
     */
    public static File returnImageFileName() {
        File imageFileName;
        String ImageName = "Image_" + Calendar.getInstance().getTimeInMillis() + ".jpeg";
        String Imagefolder = createFolder(TRICE_IMAGE_IMAGES);
        if (!Imagefolder.equals("")) {
            imageFileName = new File(Imagefolder, ImageName);
        } else {
            imageFileName = null;
        }
        return imageFileName;
    }

    /**
     * @return video file name with .mp4
     */
    public static File returnVideoFileName() {
        File imageFileName;
        String ImageName = "Video_" + Calendar.getInstance().getTimeInMillis() + ".mp4";
        String Imagefolder = createFolder(TRICE_VIDEO);
        if (!Imagefolder.equals("")) {
            imageFileName = new File(Imagefolder, ImageName);
        } else {
            imageFileName = null;
        }
        return imageFileName;
    }


    /**
     * @return image thumb file name with .jpeg
     */
    public static File returnThumbImageFileName() {
        File thumbfilename;
        String ImagethumbName = "Image_thumb_" + Calendar.getInstance().getTimeInMillis() + ".jpeg";
        String ImageThumbfolder = createFolder(TRICE_IMAGE_THUMB);
        if (!ImageThumbfolder.equals("")) {
            thumbfilename = new File(ImageThumbfolder, ImagethumbName);
        } else {
            thumbfilename = null;
        }

        return thumbfilename;
    }

    public static String saveImageBitmap(Bitmap bmp) {
        FileOutputStream out = null;
        try {
            String filename = returnThumbImageFileName().getAbsolutePath();
            out = new FileOutputStream(filename);
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, out); // bmp is your Bitmap instance
            // PNG is a lossless format, the compression factor (100) is ignored
            return filename;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return "";
    }


    public static File reSaveBitmap(String path, int rotation) { //help for fix landscape photos
        String extStorageDirectory = path;
        String filename = returnThumbImageFileName().getAbsolutePath();
        OutputStream outStream = null;
        File file = new File(filename);
        if (file.exists()) {
            file.delete();
            file = new File(extStorageDirectory, filename);
        }
        try {
            // make a new bitmap from your file
            Bitmap bitmap = BitmapFactory.decodeFile(path + filename);
            bitmap = checkRotationFromCamera(bitmap, path + filename, rotation);
            bitmap = Bitmap.createScaledBitmap(bitmap, (int) ((float) bitmap.getWidth() * 0.3f), (int) ((float) bitmap.getHeight() * 0.3f), false);
            outStream = new FileOutputStream(path + filename);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outStream);
            outStream.flush();
            outStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return file;
    }

    private static Bitmap checkRotationFromCamera(Bitmap bitmap, String pathToFile, int rotate) {
        Matrix matrix = new Matrix();
        matrix.postRotate(rotate);
        Bitmap rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        return rotatedBitmap;
    }

    public static float getFileSize(String filePath) {
        try {
            File file = new File(filePath);
            long length = file.length();
            length = length / 1024;
            System.out.println("File Path : " + file.getPath() + ", File size : " + length + " KB");
            return length / 1024;
        } catch (Exception e) {
            System.out.println("File not found : " + e.getMessage() + e);
            return -1;
        }
    }

    public static String getFileNameFromUrl(String path) {
        if (StringUtils.isNotEmpty(path)) {
            String[] pathArray = path.split("/");
            return pathArray[pathArray.length - 1];
        }
        return "";
    }

    public static String getFileExtention(String path) {
        try {
            if (StringUtils.isNotEmpty(path)) {
                return path.substring(path.lastIndexOf(".") + 1);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
        return "";
    }

}
