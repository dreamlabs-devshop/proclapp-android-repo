package com.proclapp.interfaces;

/**
 * Created by abhishek on 4/7/18.
 */

public interface RecyclerClickListener {
    void onItemClick(int pos, String tag);
}
