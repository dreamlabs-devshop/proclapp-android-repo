package com.proclapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.proclapp.expert.ExpertModel;

import java.io.Serializable;
import java.util.ArrayList;

public class AllPostsModel implements Serializable {

    private final static long serialVersionUID = 7783047482312024235L;

    @SerializedName("post_id")
    @Expose
    private String postId;
    @SerializedName("post_type")
    @Expose
    private String postType;
    @SerializedName("post_title")
    @Expose
    private String postTitle;
    @SerializedName("post_description")
    @Expose
    private String postDescription;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("expert_id")
    @Expose
    private String expertId;
    @SerializedName("admin_id")
    @Expose
    private String adminId;
    @SerializedName("post_date")
    @Expose
    private String postDate;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("author_id")
    @Expose
    private String authorId;
    @SerializedName("author")
    @Expose
    private String author;
    @SerializedName("professional_qualification")
    @Expose
    private String professionalQualification;
    @SerializedName("files")
    @Expose
    private File files;
    @SerializedName("categories_id")
    @Expose
    private String categoriesId;
    @SerializedName("category_name")
    @Expose
    private String categoryName;
    @SerializedName("post_read_count")
    @Expose
    private String postReadCount;
    @SerializedName("post_like_count")
    @Expose
    private String postLikeCount;
    @SerializedName("post_share_count")
    @Expose
    private String postShareCount;
    @SerializedName("post_comment_count")
    @Expose
    private String postCommentCount;
    @SerializedName("is_post_liked")
    @Expose
    private String isPostLiked;
    @SerializedName("is_post_bookmark")
    @Expose
    private String isPostBookmark;
    @SerializedName("post_flashed_count")
    @Expose
    private String postFlashedCount;
    @SerializedName("profile_image")
    @Expose
    private String profileImage;
    @SerializedName("declined_post_date")
    @Expose
    private String declinedPostDate;
    @SerializedName("is_following")
    @Expose
    private String is_following;
    @SerializedName("total_answers")
    @Expose
    private String total_answers;
    @SerializedName("sfriend")
    @Expose
    private String sFriend = "";
    @SerializedName("sexpert")
    @Expose
    private String sExpert = "";
    @SerializedName("tr_topics")
    @Expose
    private String trTopics = "";
    @SerializedName("images")
    @Expose
    private ArrayList<Images> images;
    @SerializedName("profile_image_thumb")
    @Expose
    private String profileImageThumb;
    @SerializedName("file_type")
    @Expose
    private String file_type;
    @SerializedName("vv_type")
    @Expose
    private String vvType;
    @SerializedName("is_user")
    @Expose
    private String is_user;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("category_label")
    @Expose
    private String categoryLabel;
    @SerializedName("net_amount")
    @Expose
    private String netAmount;
    @SerializedName("advert_time_slot_id")
    @Expose
    private String advertTimeSlotId;
    @SerializedName("no_of_days")
    @Expose
    private String noOfDays;
    @SerializedName("time_in_min")
    @Expose
    private String timeInMin;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("audios")
    @Expose
    private AudioPostModel audioPost;
    @SerializedName("videos")
    @Expose
    private VideosPostModel videosPost;
    @SerializedName("post_link")
    @Expose
    private PostLink postLink;
    @SerializedName("advert_url")
    @Expose
    private String advert_url;
    @SerializedName("is_carryover")
    @Expose
    private String is_carryover;
    @SerializedName("is_carryover_expire")
    @Expose
    private String is_carryover_expire;
    @SerializedName("is_subscribe")
    @Expose
    private boolean isSubscribe;
    @SerializedName("request_to_reply")
    @Expose
    private String request_to_reply;
    @SerializedName("is_requested_to_expert")
    @Expose
    private boolean isRequestedToExpert;

    @SerializedName("trending_topics")
    private ArrayList<CategoryModel> trendingTopics = new ArrayList<>();

    @SerializedName("suggested_experts")
    private ArrayList<ExpertModel> suggestedExperts = new ArrayList<>();

    @SerializedName("suggested_friends")
    private ArrayList<FollowerFollowingModel> suggestedFriends = new ArrayList<>();


    private boolean isAudioVideoSelected = false;

    public String getFile_type() {
        return file_type;
    }

    public String getPostId() {
        return postId;
    }

    public String getPostType() {
        return postType;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public String getPostDescription() {
        return postDescription;
    }

    public Object getUserId() {
        return userId;
    }

    public String getExpertId() {
        return expertId;
    }

    public String getAdminId() {
        return adminId;
    }

    public String getPostDate() {
        return postDate;
    }

    public String getStatus() {
        return status;
    }

    public String getType() {
        return type;
    }

    public String getAuthorId() {
        return authorId;
    }

    public String getAuthor() {
        return author;
    }

    public String getProfessionalQualification() {
        return professionalQualification;
    }

    public File getFiles() {
        return files;
    }

    public String getCategoriesId() {
        return categoriesId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public String getPostReadCount() {
        return postReadCount;
    }

    public void setPostReadCount(String postReadCount) {
        this.postReadCount = postReadCount;
    }

    public String getPostLikeCount() {
        return postLikeCount;
    }

    public void setPostLikeCount(String postLikeCount) {
        this.postLikeCount = postLikeCount;
    }

    public String getPostShareCount() {
        return postShareCount;
    }

    public void setPostShareCount(String postShareCount) {
        this.postShareCount = postShareCount;
    }

    public String getPostCommentCount() {
        return postCommentCount;
    }

    public void setPostCommentCount(String postCommentCount) {
        this.postCommentCount = postCommentCount;
    }

    public String getIsPostLiked() {
        return isPostLiked;
    }

    public void setIsPostLiked(String isPostLiked) {
        this.isPostLiked = isPostLiked;
    }

    public String getIsPostBookmark() {
        return isPostBookmark;
    }

    public void setIsPostBookmark(String isPostBookmark) {
        this.isPostBookmark = isPostBookmark;
    }

    public String getPostFlashedCount() {
        return postFlashedCount;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public String getProfileImageThumb() {
        return profileImageThumb;
    }

    public String getDeclinedPostDate() {
        return declinedPostDate;
    }

    public void setDeclinedPostDate(String declinedPostDate) {
        this.declinedPostDate = declinedPostDate;
    }

    public String getIs_following() {
        return is_following;
    }

    public void setIs_following(String is_following) {
        this.is_following = is_following;
    }

    public String getTotal_answers() {
        return total_answers;
    }

    public void setTotal_answers(String total_answers) {
        this.total_answers = total_answers;
    }

    public String getsFriend() {
        return sFriend;
    }

    public String getsExpert() {
        return sExpert;
    }

    public String getTrTopics() {
        return trTopics;
    }

    public ArrayList<Images> getImages() {
        return images;
    }

    public void setImages(ArrayList<Images> images) {
        this.images = images;
    }

    public String getVvType() {
        return vvType;
    }

    public String getCategory() {
        return category;
    }

    public String getCategoryLabel() {
        return categoryLabel;
    }

    public String getRequest_to_reply() {
        return request_to_reply;
    }

    public AudioPostModel getAudioPost() {
        return audioPost;
    }

    public VideosPostModel getVideosPost() {
        return videosPost;
    }

    public boolean isAudioVideoSelected() {
        return isAudioVideoSelected;
    }

    public void setAudioVideoSelected(boolean audioVideoSelected) {
        isAudioVideoSelected = audioVideoSelected;
    }

    public String getIs_user() {
        return is_user;
    }


    public String getNetAmount() {
        return netAmount;
    }

    public String getAdvertTimeSlotId() {
        return advertTimeSlotId;
    }

    public String getNoOfDays() {
        return noOfDays;
    }

    public String getTimeInMin() {
        return timeInMin;
    }

    public String getAmount() {
        return amount;
    }

    public String getCurrency() {
        return currency;
    }

    public PostLink getPostLink() {
        return postLink;
    }

    public String getAdvert_url() {
        return advert_url;
    }

    public String getIs_carryover() {
        return is_carryover;
    }

    public void setIsCarryover(String isCarryover) {
        this.is_carryover = isCarryover;
    }

    public String getIs_carryover_expire() {
        return is_carryover_expire;
    }

    public ArrayList<CategoryModel> getTrendingTopics() {
        return trendingTopics;
    }

    public void setTrendingTopics(ArrayList<CategoryModel> trendingTopics) {
        this.trendingTopics = trendingTopics;
    }

    public ArrayList<ExpertModel> getSuggestedExperts() {
        return suggestedExperts;
    }

    public void setSuggestedExperts(ArrayList<ExpertModel> suggestedExperts) {
        this.suggestedExperts = suggestedExperts;
    }

    public ArrayList<FollowerFollowingModel> getSuggestedFriends() {
        return suggestedFriends;
    }

    public void setSuggestedFriends(ArrayList<FollowerFollowingModel> suggestedFriends) {
        this.suggestedFriends = suggestedFriends;
    }

    public boolean isSubscribe() {
        return isSubscribe;
    }

    public boolean isRequestedToExpert() {
        return isRequestedToExpert;
    }

    /*public String getImageName() {
        return imageName;
    }

    public String getImageNameThumb() {
        return imageNameThumb;
    }*/

    public static class File implements Serializable {

        private final static long serialVersionUID = 8693735616093096461L;
        @SerializedName("file_name")
        @Expose
        private String fileName;
        @SerializedName("ori_file_name")
        @Expose
        private String oriFileName;
        @SerializedName("file_size")
        @Expose
        private String filesize;

        public String getFileName() {
            return fileName;
        }

        public String getOriFileName() {
            return oriFileName;
        }

        public String getFileSize() {
            return filesize;
        }
    }

    public static class Images implements Serializable {

        @SerializedName("post_img_id")
        @Expose
        private String post_img_id = "";
        @SerializedName("post_id")
        @Expose
        private String post_id;
        @SerializedName("image_name")
        @Expose
        private String image_name = "";
        @SerializedName("date")
        @Expose
        private String date;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("image_name_thumb")
        @Expose
        private String image_name_thumb;

        private java.io.File image;

        public java.io.File getImage() {
            return image;
        }

        public void setImage(java.io.File image) {
            this.image = image;
        }

        public String getPost_img_id() {
            return post_img_id;
        }

        public String getPost_id() {
            return post_id;
        }

        public String getImage_name() {
            return image_name;
        }

        public String getDate() {
            return date;
        }

        public String getStatus() {
            return status;
        }

        public String getImage_name_thumb() {
            return image_name_thumb;
        }
    }

    public class PostLink implements Serializable {

        private String
                post_link_id,
                post_id,
                link,
                date,
                link_title,
                link_description,
                link_image,
                status;


        public String getPost_link_id() {
            return post_link_id;
        }

        public String getPost_id() {
            return post_id;
        }

        public String getLink() {
            return link;
        }

        public String getDate() {
            return date;
        }

        public String getStatus() {
            return status;
        }

        public String getLink_title() {
            return link_title;
        }

        public String getLink_description() {
            return link_description;
        }

        public String getLink_image() {
            return link_image;
        }
    }

}
