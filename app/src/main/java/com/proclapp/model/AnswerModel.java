package com.proclapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class AnswerModel implements Serializable {


    @SerializedName("post_answers_id")
    @Expose
    public String postAnswersId;
    @SerializedName("post_id")
    @Expose
    public String postId;
    @SerializedName("user_id")
    @Expose
    public String userId;
    @SerializedName("expert_id")
    @Expose
    public String expertId;
    @SerializedName("admin_id")
    @Expose
    public String adminId;
    @SerializedName("answer")
    @Expose
    public String answer;
    @SerializedName("date")
    @Expose
    public String date;

    @SerializedName("status")
    @Expose
    public String status;

    @SerializedName("answer_comment_count")
    @Expose
    public String answer_comment_count;

    @SerializedName("is_post_answer_upvote")
    @Expose
    public String is_post_answer_upvote;

    @SerializedName("answer_upvote_comment_count")
    @Expose
    public int answer_upvote_comment_count = 0;

    @SerializedName("is_post_answer_downvote")
    @Expose
    public String is_post_answer_downvote;

    @SerializedName("answer_downvote_comment_count")
    @Expose
    public int answer_downvote_comment_count;

    @SerializedName("post_answer_read_count")
    @Expose
    public int post_answer_read_count;

    @SerializedName("post_answer_like_count")
    @Expose
    public String post_answer_like_count;

    @SerializedName("is_post_answer_liked")
    @Expose
    public String is_post_answer_liked;

    @SerializedName("is_post_answer_bookmark")
    @Expose
    public String is_post_answer_bookmark;
    @SerializedName("user")
    @Expose
    public User user;
    @SerializedName("comments")
    @Expose
    public ArrayList<Comments> comments;
    @SerializedName("is_following")
    @Expose
    private String isFollowing;
    @SerializedName("answer_img")
    @Expose
    private ArrayList<AllPostsModel.Images> answerImg = new ArrayList<>();
    @SerializedName("images")
    @Expose
    private ArrayList<AllPostsModel.Images> images = new ArrayList<>();

    @SerializedName("link")
    @Expose
    private AnswerLink answerLink;

    @SerializedName("audios")
    @Expose
    private AudioPostModel audioPost;

    @SerializedName("videos")
    @Expose
    private VideosPostModel videosPost;

    @SerializedName("vv_type")
    @Expose
    private String vv_type;

    @SerializedName("show_ans")
    @Expose
    private String showAns;

    public String getPostAnswersId() {
        return postAnswersId;
    }

    public void setPostAnswersId(String postAnswersId) {
        this.postAnswersId = postAnswersId;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getExpertId() {
        return expertId;
    }

    public void setExpertId(String expertId) {
        this.expertId = expertId;
    }

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAnswer_comment_count() {
        return answer_comment_count;
    }

    public void setAnswer_comment_count(String answer_comment_count) {
        this.answer_comment_count = answer_comment_count;
    }

    public String getIs_post_answer_upvote() {
        return is_post_answer_upvote;
    }

    public void setIs_post_answer_upvote(String is_post_answer_upvote) {
        this.is_post_answer_upvote = is_post_answer_upvote;
    }

    public int getAnswer_upvote_comment_count() {
        return answer_upvote_comment_count;
    }

    public void setAnswer_upvote_comment_count(int answer_upvote_comment_count) {
        this.answer_upvote_comment_count = answer_upvote_comment_count;
    }

    public String getIs_post_answer_downvote() {
        return is_post_answer_downvote;
    }

    public void setIs_post_answer_downvote(String is_post_answer_downvote) {
        this.is_post_answer_downvote = is_post_answer_downvote;
    }

    public int getAnswer_downvote_comment_count() {
        return answer_downvote_comment_count;
    }

    public void setAnswer_downvote_comment_count(int answer_downvote_comment_count) {
        this.answer_downvote_comment_count = answer_downvote_comment_count;
    }

    public int getPost_answer_read_count() {
        return post_answer_read_count;
    }

    public void setPost_answer_read_count(int post_answer_read_count) {
        this.post_answer_read_count = post_answer_read_count;
    }

    public String getIs_post_answer_bookmark() {
        return is_post_answer_bookmark;
    }

    public void setIs_post_answer_bookmark(String is_post_answer_bookmark) {
        this.is_post_answer_bookmark = is_post_answer_bookmark;
    }

    public String getPost_answer_like_count() {
        return post_answer_like_count;
    }

    public void setPost_answer_like_count(String post_answer_like_count) {
        this.post_answer_like_count = post_answer_like_count;
    }

    public String getIs_post_answer_liked() {
        return is_post_answer_liked;
    }

    public String isFollowing() {
        return isFollowing;
    }

    public void setFollowing(String isFollowing) {
        this.isFollowing = isFollowing;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ArrayList<Comments> getComments() {
        return comments;
    }

    public void setComments(ArrayList<Comments> comments) {
        this.comments = comments;
    }

    public ArrayList<AllPostsModel.Images> getAnswerImg() {
        if (answerImg.size() > 0)
            return answerImg;
        else
            return images;
    }

    public AnswerLink getAnswerLink() {
        return answerLink;
    }

    public AudioPostModel getAudioPost() {
        return audioPost;
    }

    public VideosPostModel getVideosPost() {
        return videosPost;
    }

    public String getVv_type() {
        return vv_type;
    }

    public String getShowAns() {
        return showAns;
    }

    public static class SearchedUser implements Serializable {
        @SerializedName("user_id")
        @Expose
        public String userId;
        @SerializedName("profile_image")
        @Expose
        public String profileImage;
        @SerializedName("email")
        @Expose
        public String email;
        @SerializedName("firstname")
        @Expose
        public String firstName;
        @SerializedName("lastname")
        @Expose
        public String lastName;
        public String name;

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getProfileImage() {
            return profileImage;
        }

        public void setProfileImage(String profileImage) {
            this.profileImage = profileImage;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getName() {
            return firstName + " " + lastName;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public static class User implements Serializable {

        @SerializedName("user_id")
        @Expose
        public String userId;
        @SerializedName("profile_image")
        @Expose
        public String profileImage;
        @SerializedName("name")
        @Expose
        public String name;
        @SerializedName("profession")
        @Expose
        public String professionalQualification;

        @SerializedName("is_following")
        @Expose
        private String isFollowing;

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getProfileImage() {
            return profileImage;
        }

        public void setProfileImage(String profileImage) {
            this.profileImage = profileImage;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getProfessionalQualification() {
            return professionalQualification;
        }

        public void setProfessionalQualification(String professionalQualification) {
            this.professionalQualification = professionalQualification;
        }

        public String getIsFollowing() {
            return isFollowing;
        }
    }

    public static class Comments implements Serializable {

        @SerializedName("comment_id")
        @Expose
        public String comment_id;

        @SerializedName("comment")
        @Expose
        public String comment;

        @SerializedName("answer_id")
        @Expose
        public String answer_id;

        @SerializedName("user_id")
        @Expose
        public String user_id;

        @SerializedName("parent_id")
        @Expose
        public String parent_id;

        @SerializedName("is_answer_comment_upvote")
        @Expose
        public String is_post_answer_upvote;

        @SerializedName("reply")
        @Expose
        public Comments reply;

        @SerializedName("reply_list")
        @Expose
        public ArrayList<Comments> replies = new ArrayList<>();

        public ArrayList<Comments> getReplies() {
            return replies;
        }

        public void setReplies(ArrayList<Comments> replies) {
            this.replies = replies;
        }

        public Comments getReply() {
            return reply;
        }

        public void setReply(Comments reply) {
            this.reply = reply;
        }

        public String getIs_comment_liked() {
            return is_comment_liked;
        }

        public void setIs_comment_liked(String is_comment_liked) {
            this.is_comment_liked = is_comment_liked;
        }

        public int getComment_like_count() {
            return comment_like_count;
        }

        public void setComment_like_count(int comment_like_count) {
            this.comment_like_count = comment_like_count;
        }

        @SerializedName("is_comment_liked")
        @Expose
        public String is_comment_liked;

        @SerializedName("answer_comment_upvote_count")
        @Expose
        public int answer_upvote_comment_count = 0;

        @SerializedName("comment_like_count")
        @Expose
        public int comment_like_count = 0;

        public String getIs_post_answer_upvote() {
            return is_post_answer_upvote;
        }

        public void setIs_post_answer_upvote(String is_post_answer_upvote) {
            this.is_post_answer_upvote = is_post_answer_upvote;
        }

        public int getAnswer_upvote_comment_count() {
            return answer_upvote_comment_count;
        }

        public void setAnswer_upvote_comment_count(int answer_upvote_comment_count) {
            this.answer_upvote_comment_count = answer_upvote_comment_count;
        }

        public String getIs_post_answer_downvote() {
            return is_post_answer_downvote;
        }

        public void setIs_post_answer_downvote(String is_post_answer_downvote) {
            this.is_post_answer_downvote = is_post_answer_downvote;
        }

        public int getAnswer_downvote_comment_count() {
            return answer_downvote_comment_count;
        }

        public void setAnswer_downvote_comment_count(int answer_downvote_comment_count) {
            this.answer_downvote_comment_count = answer_downvote_comment_count;
        }

        @SerializedName("is_answer_comment_downvote")
        @Expose
        public String is_post_answer_downvote;

        @SerializedName("answer_comment_downvote_count")
        @Expose
        public int answer_downvote_comment_count;

        @SerializedName("date")
        @Expose
        public String date;

        @SerializedName("user")
        @Expose
        public User user;

        public String getComment_id() {
            return comment_id;
        }

        public void setComment_id(String comment_id) {
            this.comment_id = comment_id;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public String getAnswer_id() {
            return answer_id;
        }

        public void setAnswer_id(String answer_id) {
            this.answer_id = answer_id;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getParent_id() {
            return parent_id;
        }

        public void setParent_id(String parent_id) {
            this.parent_id = parent_id;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public User getUser() {
            return user;
        }

        public void setUser(User user) {
            this.user = user;
        }
    }

    public class AnswerLink implements Serializable {

        private String
                post_link_id,
                post_id,
                link,
                date,
                link_title,
                link_description,
                link_image,
                status;

        public String getPost_link_id() {
            return post_link_id;
        }

        public String getPost_id() {
            return post_id;
        }

        public String getLink() {
            return link;
        }

        public String getDate() {
            return date;
        }

        public String getStatus() {
            return status;
        }

        public String getLink_title() {
            return link_title;
        }

        public String getLink_description() {
            return link_description;
        }

        public String getLink_image() {
            return link_image;
        }
    }

}
