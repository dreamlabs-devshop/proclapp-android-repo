package com.proclapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class AudioPostModel implements Serializable {

    private final static long serialVersionUID = 8548031591730902964L;
    @SerializedName("post_audio_id")
    @Expose
    private String postAudioId;
    @SerializedName("post_id")
    @Expose
    private String postId;
    @SerializedName("audio_name")
    @Expose
    private String audioName;
    @SerializedName("ori_audio_name")
    @Expose
    private String oriAudioName;
    @SerializedName("audio_size")
    @Expose
    private String audioSize;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("audio_url")
    @Expose
    private String audioUrl;

    public String getPostAudioId() {
        return postAudioId;
    }

    public String getPostId() {
        return postId;
    }

    public String getAudioName() {
        return audioName;
    }

    public String getOriAudioName() {
        return oriAudioName;
    }

    public String getAudioSize() {
        return audioSize;
    }

    public String getDate() {
        return date;
    }

    public String getStatus() {
        return status;
    }

    public String getAudioUrl() {
        return audioUrl;
    }
}
