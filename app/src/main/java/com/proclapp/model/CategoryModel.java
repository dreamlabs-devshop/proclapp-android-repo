package com.proclapp.model;

public class CategoryModel {
    private String category_id,
            category_name,
            category_img,
            date,
            status,
            category_selected_img,
            category_selected_img_thumb,
            total_followers,
            category_img_thumb;

    private int is_selected = 0;

    private boolean isCategorySelected = false;

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getCategory_img() {
        return category_img;
    }

    public void setCategory_img(String category_img) {
        this.category_img = category_img;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCategory_img_thumb() {
        return category_img_thumb;
    }

    public void setCategory_img_thumb(String category_img_thumb) {
        this.category_img_thumb = category_img_thumb;
    }

    public String getCategory_selected_img() {
        return category_selected_img;
    }

    public void setCategory_selected_img(String category_selected_img) {
        this.category_selected_img = category_selected_img;
    }

    public String getCategory_selected_img_thumb() {
        return category_selected_img_thumb;
    }

    public void setCategory_selected_img_thumb(String category_selected_img_thumb) {
        this.category_selected_img_thumb = category_selected_img_thumb;
    }

    public int getIs_selected() {
        return is_selected;
    }

    public void setIs_selected(int is_selected) {
        this.is_selected = is_selected;
    }

    public boolean isCategorySelected() {
        return isCategorySelected;
    }

    public void setCategorySelected(boolean categorySelected) {
        isCategorySelected = categorySelected;
    }

    public String getTotalFollowers() {
        return total_followers;
    }
}
