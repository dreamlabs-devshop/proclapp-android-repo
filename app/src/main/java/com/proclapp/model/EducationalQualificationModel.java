package com.proclapp.model;

public class EducationalQualificationModel {

    private String educational_qualification,
            passing_year;

    public String getEducational_qualification() {
        return educational_qualification;
    }

    public void setEducational_qualification(String educational_qualification) {
        this.educational_qualification = educational_qualification;
    }

    public String getPassing_year() {
        return passing_year;
    }

    public void setPassing_year(String passing_year) {
        this.passing_year = passing_year;
    }
}
