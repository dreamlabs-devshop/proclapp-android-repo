package com.proclapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class FollowerFollowingModel implements Serializable {

    private final static long serialVersionUID = -7836793016344832690L;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("profile_image")
    @Expose
    private String profileImage;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("profile_image_thumb")
    @Expose
    private String profileImageThumb;
    @SerializedName("profession")
    @Expose
    private String profession;

    @SerializedName("flashed")
    @Expose
    private String flashed;

    @SerializedName("request_status")
    @Expose
    private String request_status;

    @SerializedName("sender_id")
    @Expose
    private String sender_id;
    @SerializedName("receiver_id")
    @Expose
    private String receiver_id;

    @SerializedName("is_selected")
    @Expose
    private boolean selected = false;

    @SerializedName("total_followers")
    @Expose
    private String totalFollowers;

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getName() {
        return name;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public String getUserId() {
        return userId;
    }

    public String getProfileImageThumb() {
        return profileImageThumb;
    }

    public String getProfession() {
        return profession;
    }

    public String getFlashed() {
        return flashed;
    }

    public String getRequest_status() {
        return request_status;
    }

    public void setRequest_status(String request_status) {
        this.request_status = request_status;
    }

    public String getSender_id() {
        return sender_id;
    }

    public void setSender_id(String sender_id) {
        this.sender_id = sender_id;
    }

    public String getReceiver_id() {
        return receiver_id;
    }

    public void setReceiver_id(String receiver_id) {
        this.receiver_id = receiver_id;
    }

    public String getTotalFollowers() {
        return totalFollowers;
    }
}
