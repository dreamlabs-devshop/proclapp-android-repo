package com.proclapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class NotificationAlertModel implements Serializable {

    private final static long serialVersionUID = 9018349954937924496L;
    @SerializedName("notification_type_id")
    @Expose
    private String notificationTypeId;
    @SerializedName("notification_type")
    @Expose
    private String notificationType;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("status")
    @Expose
    private String status;


    public String getNotificationTypeId() {
        return notificationTypeId;
    }

    public String getNotificationType() {
        return notificationType;
    }

    public String getDate() {
        return date;
    }

    public String getStatus() {
        return status;
    }
}
