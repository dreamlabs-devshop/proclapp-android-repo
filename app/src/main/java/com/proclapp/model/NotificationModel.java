package com.proclapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class NotificationModel implements Serializable {

    private final static long serialVersionUID = -3803748003839571241L;
    @SerializedName("notification_id")
    @Expose
    private String notificationId;
    @SerializedName("to_user_id")
    @Expose
    private String toUserId;
    @SerializedName("notification_type")
    @Expose
    private String notificationType;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("is_read")
    @Expose
    private String isRead;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("post_answers_id")
    @Expose
    private String postAnswersId;


    private String
            post_title,
            user_id,
            post_id,
            post_type,
            status,
            image,
            image_thumb;


    public String getNotificationId() {
        return notificationId;
    }

    public String getToUserId() {
        return toUserId;
    }

    public String getNotificationType() {
        return notificationType;
    }

    public String getMessage() {
        return message;
    }

    public String getIsRead() {
        return isRead;
    }

    public String getDate() {
        return date;
    }

    public String getPost_title() {
        return post_title;
    }

    public String getUser_id() {
        return user_id;
    }

    public String getPost_id() {
        return post_id;
    }

    public String getPost_type() {
        return post_type;
    }

    public String getStatus() {
        return status;
    }

    public String getImage() {
        return image;
    }

    public String getImage_thumb() {
        return image_thumb;
    }

    public String getPostAnswersId() {
        return postAnswersId;
    }
}
