package com.proclapp.model;

import java.io.File;

public class PostAdvUploadModel {

    private File image;
    private String imageId;
    private String imageUrl;

    public File getImage() {
        return image;
    }

    public String getImageId() {
        return imageId;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImage(File image) {
        this.image = image;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
