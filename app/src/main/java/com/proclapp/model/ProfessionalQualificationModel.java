package com.proclapp.model;

public class ProfessionalQualificationModel {

    private String job_title = "",
            experience = "",
            isCurrentJob = "";

    public String getJob_title() {
        return job_title;
    }

    public void setJob_title(String job_title) {
        this.job_title = job_title;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getIsCurrentJob() {
        return isCurrentJob;
    }

    public void setIsCurrentJob(String isCurrentJob) {
        this.isCurrentJob = isCurrentJob;
    }
}
