package com.proclapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ResearchPaperModel implements Serializable {

    private final static long serialVersionUID = -2055645016718795410L;
    @SerializedName("post_id")
    @Expose
    private String postId;
    @SerializedName("post_type")
    @Expose
    private String postType;
    @SerializedName("post_title")
    @Expose
    private String postTitle;
    @SerializedName("post_description")
    @Expose
    private String postDescription;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("expert_id")
    @Expose
    private String expertId;
    @SerializedName("admin_id")
    @Expose
    private String adminId;
    @SerializedName("post_date")
    @Expose
    private String postDate;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("author_id")
    @Expose
    private String authorId;
    @SerializedName("author")
    @Expose
    private String author;
    @SerializedName("professional_qualification")
    @Expose
    private String professionalQualification;
    @SerializedName("files")
    @Expose
    private List<File> files = null;
    @SerializedName("post_read_count")
    @Expose
    private String postReadCount;
    @SerializedName("post_like_count")
    @Expose
    private String postLikeCount;
    @SerializedName("post_share_count")
    @Expose
    private String postShareCount;
    @SerializedName("post_comment_count")
    @Expose
    private String postCommentCount;
    @SerializedName("is_post_liked")
    @Expose
    private String isPostLiked;
    @SerializedName("is_post_bookmark")
    @Expose
    private String isPostBookmark;
    @SerializedName("category_name")
    @Expose
    private String categoryName;
    @SerializedName("categories_id")
    @Expose
    private String categories_id;

    public String getPostId() {
        return postId;
    }

    public String getPostType() {
        return postType;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public String getPostDescription() {
        return postDescription;
    }

    public String getUserId() {
        return userId;
    }

    public String getExpertId() {
        return expertId;
    }

    public String getAdminId() {
        return adminId;
    }

    public String getPostDate() {
        return postDate;
    }

    public String getStatus() {
        return status;
    }

    public String getAuthorId() {
        return authorId;
    }

    public String getAuthor() {
        return author;
    }

    public String getProfessionalQualification() {
        return professionalQualification;
    }

    public List<File> getFiles() {
        return files;
    }

    public String getPostReadCount() {
        return postReadCount;
    }

    public String getPostLikeCount() {
        return postLikeCount;
    }

    public String getPostShareCount() {
        return postShareCount;
    }

    public String getPostCommentCount() {
        return postCommentCount;
    }

    public String getIsPostLiked() {
        return isPostLiked;
    }

    public String getIsPostBookmark() {
        return isPostBookmark;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public String getCategories_id() {
        return categories_id;
    }

    public static class File implements Serializable {

        private final static long serialVersionUID = 8693735616093096461L;
        @SerializedName("file_name")
        @Expose
        private String fileName;
        @SerializedName("ori_file_name")
        @Expose
        private String oriFileName;

        public String getFileName() {
            return fileName;
        }

        public void setFileName(String fileName) {
            this.fileName = fileName;
        }

        public String getOriFileName() {
            return oriFileName;
        }

        public void setOriFileName(String oriFileName) {
            this.oriFileName = oriFileName;
        }

    }

}



