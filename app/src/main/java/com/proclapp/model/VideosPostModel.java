package com.proclapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class VideosPostModel implements Serializable {

    private final static long serialVersionUID = 5731783102255760580L;
    @SerializedName("post_video_id")
    @Expose
    private String postVideoId;
    @SerializedName("post_id")
    @Expose
    private String postId;
    @SerializedName("video_name")
    @Expose
    private String videoName;
    @SerializedName("ori_video_name")
    @Expose
    private String oriVideoName;
    @SerializedName("video_size")
    @Expose
    private String videoSize;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("video_url")
    @Expose
    private String videoUrl;

    public String getPostVideoId() {
        return postVideoId;
    }

    public String getPostId() {
        return postId;
    }

    public String getVideoName() {
        return videoName;
    }

    public String getOriVideoName() {
        return oriVideoName;
    }

    public String getVideoSize() {
        return videoSize;
    }

    public String getDate() {
        return date;
    }

    public String getStatus() {
        return status;
    }

    public String getVideoUrl() {
        return videoUrl;
    }
}
