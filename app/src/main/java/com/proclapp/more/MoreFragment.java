package com.proclapp.more;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.more.bookmark.BookMarkedPostActivity;
import com.proclapp.more.declinedquestionarticle.DeclinedQuestionArcticlesActivity;
import com.proclapp.more.helpandsupport.HelpAndSupportActivity;
import com.proclapp.more.settings.SettingsActivity;
import com.proclapp.profile.EditCategoriesActivity;
import com.proclapp.wallet.ExpertWalletActivity;

public class MoreFragment extends Fragment implements View.OnClickListener {

    LinearLayout
            ll_settings,
            ll_declined_questions_artical,
            ll_videos,
            ll_bookmark,
            ll_category_topics,
            ll_help_support,
            ll_wallet;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_more, container, false);
        initViews(view);
        setOnClickListener();
        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.ll_settings:
                startActivity(new Intent(getActivity(), SettingsActivity.class));
                break;

            case R.id.ll_help_support:
                startActivity(new Intent(getActivity(), HelpAndSupportActivity.class));
                break;

            case R.id.ll_declined_questions_artical:

                startActivity(new Intent(getActivity(), DeclinedQuestionArcticlesActivity.class));
                break;

            case R.id.ll_videos:
                startActivity(new Intent(getActivity(), VideoActivity.class));
                break;

            case R.id.ll_bookmark:
                startActivity(new Intent(getActivity(), BookMarkedPostActivity.class));
                break;

            case R.id.ll_category_topics:
                startActivity(new Intent(getActivity(), EditCategoriesActivity.class)
                        .putExtra("selectedCategories", AppClass.preferences.getCategoriesId()));
                break;


            case R.id.ll_wallet:
                startActivity(new Intent(getActivity(), ExpertWalletActivity.class));
                break;

        }

    }

    @Override
    public void onResume() {
        super.onResume();
        if (AppClass.preferences.getIsExpert().equalsIgnoreCase("1")) {
            ll_wallet.setVisibility(View.VISIBLE);
        } else {
            ll_wallet.setVisibility(View.GONE);
        }

    }

    private void initViews(View view) {
        ll_settings = view.findViewById(R.id.ll_settings);
        ll_help_support = view.findViewById(R.id.ll_help_support);
        ll_declined_questions_artical = view.findViewById(R.id.ll_declined_questions_artical);
        ll_videos = view.findViewById(R.id.ll_videos);
        ll_bookmark = view.findViewById(R.id.ll_bookmark);
        ll_category_topics = view.findViewById(R.id.ll_category_topics);
        ll_wallet = view.findViewById(R.id.ll_wallet);
    }


    private void setOnClickListener() {
        ll_settings.setOnClickListener(this);
        ll_help_support.setOnClickListener(this);
        ll_declined_questions_artical.setOnClickListener(this);
        ll_videos.setOnClickListener(this);
        ll_bookmark.setOnClickListener(this);
        ll_category_topics.setOnClickListener(this);
        ll_wallet.setOnClickListener(this);
    }

}
