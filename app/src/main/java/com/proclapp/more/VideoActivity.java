package com.proclapp.more;

import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.home.adapter.VoiceAndVideoAdapter;
import com.proclapp.model.AllPostsModel;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.AlertDialogInterface;
import com.proclapp.utils.DialogUtils;
import com.proclapp.utils.StringUtils;
import com.proclapp.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;

public class VideoActivity extends AppCompatActivity implements View.OnClickListener, SwipyRefreshLayout.OnRefreshListener {

    private ImageView img_back;
    private RecyclerView rv_video_list;
    private ArrayList<AllPostsModel> videoList = new ArrayList<>();
    private SwipyRefreshLayout swipe_refresh;
    private TextView tv_no_data;
    private boolean isServiceCalling = false, isLastPage = false;
    private int offset = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);
        Utils.setStatusBarColor(getWindow(), this, R.color.white);
        initViews();
        setUpRecyclerView();

        swipe_refresh.setOnRefreshListener(this);

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.img_back:
                onBackPressed();
                break;

        }

    }

    @Override
    public void onRefresh(SwipyRefreshLayoutDirection direction) {
        if (direction == SwipyRefreshLayoutDirection.TOP) {
            offset = 0;
            callAPIGetVideoList();
        } else {
            offset += 20;
            callAPIGetVideoList();
        }
    }

    private void initViews() {

        img_back = findViewById(R.id.img_back);
        rv_video_list = findViewById(R.id.rv_video_list);
        tv_no_data = findViewById(R.id.tv_no_data);
        swipe_refresh = findViewById(R.id.swipe_refresh);
        img_back.setOnClickListener(this);
    }

    private void setUpRecyclerView() {

        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        rv_video_list.setLayoutManager(layoutManager);
        rv_video_list.setAdapter(new VoiceAndVideoAdapter(this, videoList));

        swipe_refresh.post(new Runnable() {
            @Override
            public void run() {
                swipe_refresh.setRefreshing(true);
                onRefresh(SwipyRefreshLayoutDirection.TOP);
            }
        });

        rv_video_list.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int firstVisibleItemPosition = layoutManager.findLastVisibleItemPosition();

                if (!isServiceCalling && !isLastPage) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0) {
                        swipe_refresh.post(new Runnable() {
                            @Override
                            public void run() {
                                swipe_refresh.setRefreshing(true);
                                onRefresh(SwipyRefreshLayoutDirection.BOTTOM);
                            }
                        });

                    }
                }

            }
        });


    }

    private void callAPIGetVideoList() {
        isServiceCalling = true;
        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("vv_type", "2");
        request.put("offset", String.valueOf(offset));

        ApiCall.getInstance().getVVList(VideoActivity.this, request, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {


                if (offset == 0)
                    videoList.clear();

                videoList.addAll((ArrayList<AllPostsModel>) data);



                        rv_video_list.setVisibility(View.VISIBLE);
                        tv_no_data.setVisibility(View.GONE);

                        isLastPage = false;
                        isServiceCalling = false;
                        rv_video_list.getAdapter().notifyDataSetChanged();

                        swipe_refresh.setRefreshing(false);




            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {

                swipe_refresh.setRefreshing(false);


                        swipe_refresh.setRefreshing(false);
                        isLastPage = true;
                        isServiceCalling = false;



                if (StringUtils.isNotEmpty(errorMessage)) {

                    if (errorMessage.equalsIgnoreCase("No data found")) {

                        if (offset == 0) {
                            tv_no_data.setVisibility(View.VISIBLE);
                            rv_video_list.setVisibility(View.GONE);
                        }

                    } else {

                        DialogUtils.openDialog(VideoActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                            @Override
                            public void onNegativeBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }

                            @Override
                            public void onPositiveBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }
                        });
                    }
                }
            }
        }, false);


    }


}
