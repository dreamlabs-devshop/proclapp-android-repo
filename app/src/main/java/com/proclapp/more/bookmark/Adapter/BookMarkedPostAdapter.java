package com.proclapp.more.bookmark.Adapter;

import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.proclapp.R;
import com.proclapp.model.AllPostsModel;
import com.proclapp.profile.OtherUserProfileActivity;
import com.proclapp.utils.DateTimeUtils;
import com.proclapp.utils.ImageLoadUtils;

import java.util.ArrayList;

public class BookMarkedPostAdapter extends RecyclerView.Adapter<BookMarkedPostViewHolder> {

    AppCompatActivity context;
    private ArrayList<AllPostsModel> bookmarkedPostList;

    public BookMarkedPostAdapter(AppCompatActivity context, ArrayList<AllPostsModel> researchPaperList) {
        this.context = context;
        this.bookmarkedPostList = researchPaperList;
    }

    @NonNull
    @Override
    public BookMarkedPostViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_bookmarked_post, viewGroup, false);
        return new BookMarkedPostViewHolder(view, context, bookmarkedPostList, this);
    }

    @Override
    public void onBindViewHolder(@NonNull BookMarkedPostViewHolder holder, int i) {

        AllPostsModel bookemarkedPostData = bookmarkedPostList.get(holder.getAdapterPosition());

        holder.txt_question_technology_innovation.setText("Research Paper | " + bookemarkedPostData.getCategoryName());
        holder.txt_description_rp.setText(bookemarkedPostData.getPostTitle());
//        holder.txt_description.setText(Html.fromHtml(bookemarkedPostData.getPostDescription()));
        holder.txt_doc_name.setText(bookemarkedPostData.getFiles().getOriFileName());
        holder.txt_doc_size.setText(bookemarkedPostData.getFiles().getFileSize());

        holder.txt_user_name.setText(bookemarkedPostData.getAuthor());

        holder.txt_bookmark.setSelected(true);
        holder.img_user_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, OtherUserProfileActivity.class)
                        .putExtra("user_id", bookemarkedPostData.getAuthorId()));
            }
        });

        holder.txt_date.setText(DateTimeUtils.changeDateTimeFormat("yyyy-MM-dd HH:mm:ss", "dd MMM", bookemarkedPostData.getPostDate()));


        ImageLoadUtils.imageLoad(context,
                holder.img_user_profile,
                bookemarkedPostData.getProfileImage(),
                R.drawable.menu_user_ph);

    }

    @Override
    public int getItemCount() {
        return bookmarkedPostList.size();
    }

}
