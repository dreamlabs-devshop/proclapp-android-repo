package com.proclapp.more.bookmark.Adapter;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Environment;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.DownloadListener;
import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.dialog.AlertDialog;
import com.proclapp.model.AllPostsModel;
import com.proclapp.payment.SubscribeUserWebViewActivity;
import com.proclapp.recommend.RecommendActivity;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.AlertDialogInterface;
import com.proclapp.utils.DialogUtils;

import java.util.ArrayList;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class BookMarkedPostViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView txt_question_technology_innovation,
            txt_user_name,
            txt_user_place,
            txt_date,
            txt_description_rp,
            txt_doc_name,
            txt_doc_size,
            txt_upvote,
            txt_title,
            txt_description_article,
            txt_image_count,
            txt_question_value,
            txt_answer_data,
            txt_your_vote,
            txt_downvote,
            txt_bookmark;

    public ImageView
            img_follow,
            img_option,
            img_article,
            img_doc,
            img_article_shadow,
            img_user_profile;

    public CircleImageView img_voter_pic;

    public LinearLayout
            ll_article,
            ll_answer_details,
            ll_document,
            ll_downvote,
            ll_bookmark,
            ll_doc,
            ll_upvote;
    public FrameLayout fl_image;

    private ArrayList<AllPostsModel> bookmarkedPostList;
    private AppCompatActivity context;
    private BookMarkedPostAdapter bookMarkedPostAdapter;

    public BookMarkedPostViewHolder(@NonNull View itemView, AppCompatActivity context,
                                    ArrayList<AllPostsModel> bookmarkedPostList,
                                    BookMarkedPostAdapter bookMarkedPostAdapter) {
        super(itemView);

        this.context = context;
        this.bookmarkedPostList = bookmarkedPostList;
        this.bookMarkedPostAdapter = bookMarkedPostAdapter;


        initViews();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.txt_bookmark:
                callAPIBookmarkPost();
                break;

            case R.id.txt_upvote:
                break;

            case R.id.txt_download:
            case R.id.ll_doc:
                if (!AppClass.preferences.getUserId().equalsIgnoreCase(bookmarkedPostList.get(getAdapterPosition()).getAuthorId()) &&
                        !bookmarkedPostList.get(getAdapterPosition()).isSubscribe()) {

                    new AlertDialog(context, context.getString(R.string.app_name), context.getString(R.string.not_subscribed), context.getString(R.string.subscribe), context.getString(R.string.cancel), new AlertDialog.AlertInterface() {
                        @Override
                        public void onNegativeBtnClicked(Dialog dialog) {
                            dialog.dismiss();
                        }

                        @Override
                        public void onPositiveBtnClicked(Dialog dialog) {
                            dialog.dismiss();
                            context.startActivity(new Intent(context, SubscribeUserWebViewActivity.class)
                                    .putExtra("post_id", bookmarkedPostList.get(getAdapterPosition()).getPostId()));


                        }
                    }).show();

                } else {
                    downloadFile(bookmarkedPostList.get(getAdapterPosition()).getFiles().getFileName());
                }
                break;

            case R.id.ll_downvote:
                context.startActivity(new Intent(context, RecommendActivity.class)
                        .putExtra("post_id", bookmarkedPostList.get(getAdapterPosition()).getPostId()));
                break;


        }
    }

    private void initViews() {
        txt_question_technology_innovation = itemView.findViewById(R.id.txt_question_technology_innovation);
        txt_user_name = itemView.findViewById(R.id.txt_user_name);
        txt_user_place = itemView.findViewById(R.id.txt_user_place);
        img_follow = itemView.findViewById(R.id.img_follow);
        txt_date = itemView.findViewById(R.id.txt_date);
        img_option = itemView.findViewById(R.id.img_option);
        txt_description_rp = itemView.findViewById(R.id.txt_description_rp);
        txt_doc_name = itemView.findViewById(R.id.txt_doc_name);
        txt_doc_size = itemView.findViewById(R.id.txt_doc_size);
        img_user_profile = itemView.findViewById(R.id.img_user_profile);
        img_voter_pic = itemView.findViewById(R.id.img_voter_pic);
        txt_bookmark = itemView.findViewById(R.id.txt_bookmark);
        ll_doc = itemView.findViewById(R.id.ll_doc);
        txt_upvote = itemView.findViewById(R.id.txt_upvote);

        txt_title = itemView.findViewById(R.id.txt_title);
        txt_description_article = itemView.findViewById(R.id.txt_description_article);
        txt_image_count = itemView.findViewById(R.id.txt_image_count);
        txt_question_value = itemView.findViewById(R.id.txt_question_value);
        txt_answer_data = itemView.findViewById(R.id.txt_answer_data);
        txt_your_vote = itemView.findViewById(R.id.txt_your_vote);
        txt_downvote = itemView.findViewById(R.id.txt_downvote);


        img_article = itemView.findViewById(R.id.img_article);
        img_doc = itemView.findViewById(R.id.img_doc);
        img_article_shadow = itemView.findViewById(R.id.img_article_shadow);

        ll_article = itemView.findViewById(R.id.ll_article);
        ll_answer_details = itemView.findViewById(R.id.ll_answer_details);
        ll_document = itemView.findViewById(R.id.ll_document);
        ll_downvote = itemView.findViewById(R.id.ll_downvote);
        ll_bookmark = itemView.findViewById(R.id.ll_bookmark);
        ll_upvote = itemView.findViewById(R.id.ll_upvote);
        fl_image = itemView.findViewById(R.id.fl_image);

        txt_bookmark.setOnClickListener(this);
        ll_upvote.setOnClickListener(this);
        ll_doc.setOnClickListener(this);
        ll_downvote.setOnClickListener(this);

        txt_upvote.setText(context.getString(R.string.download));
        txt_downvote.setText(context.getString(R.string.recommand));
    }

    private void callAPIBookmarkPost() {

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("post_id", bookmarkedPostList.get(getAdapterPosition()).getPostId());
        request.put("bookmark_unbookmark", "0");

        ApiCall.getInstance().bookmarkUnbookmark(context, request, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {

                bookmarkedPostList.remove(getAdapterPosition());
                bookMarkedPostAdapter.notifyItemRemoved(getAdapterPosition());

            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {
                DialogUtils.openDialog(context, errorMessage, context.getString(R.string.ok), "", new AlertDialogInterface() {
                    @Override
                    public void onNegativeBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }

                    @Override
                    public void onPositiveBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                    }
                });
            }
        }, true);


    }


    private void downloadFile(String fileUrl) {

        String dirPath = Environment.getExternalStorageDirectory().toString() + "/" + context.getString(R.string.app_name) + "/";
        String fileName = "" + bookmarkedPostList.get(getAdapterPosition()).getFiles().getOriFileName();

        final ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Downloading");
        progressDialog.setCancelable(true);
        progressDialog.setCanceledOnTouchOutside(false);

        progressDialog.show();

        AndroidNetworking
                .download(fileUrl, dirPath, fileName)
                .build()
                .startDownload(new DownloadListener() {
                    @Override
                    public void onDownloadComplete() {
                        AppClass.snackBarView.snackBarShow(context, "Download Completed");
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onError(ANError anError) {
                        AppClass.snackBarView.snackBarShow(context, "Error while downloading");
                        progressDialog.dismiss();
                    }
                });

    }
}
