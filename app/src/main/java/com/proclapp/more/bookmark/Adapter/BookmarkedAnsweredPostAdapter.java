package com.proclapp.more.bookmark.Adapter;

import android.app.Activity;
import android.content.Intent;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.model.AnswerModel;
import com.proclapp.signup.StartupActivity;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 *
 */

public class BookmarkedAnsweredPostAdapter extends RecyclerView.Adapter<BookmarkedAnsweredPostAdapter.MyViewHolder> {

    Activity activity;
    private ArrayList<AnswerModel> bookmarkedAnsweredPostList;

    public BookmarkedAnsweredPostAdapter(Activity activity, ArrayList<AnswerModel> bookmarkedAnsweredPostList) {
        this.activity = activity;
        this.bookmarkedAnsweredPostList = bookmarkedAnsweredPostList;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_bookmarked_post, parent, false);
        return new MyViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        holder.txt_answer_data.setText(bookmarkedAnsweredPostList.get(holder.getAdapterPosition()).getAnswer());

    }

    @Override
    public int getItemCount() {
        return bookmarkedAnsweredPostList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        private LinearLayout ll_answer_details,
                ll_document;

        private TextView
                txt_question_value,
                txt_answer_data,
                txt_your_vote;

        private CircleImageView img_voter_pic;

        public MyViewHolder(View itemView) {
            super(itemView);
            initViews();
            setTypeFace();


        }


        @Override
        public void onClick(View view) {

            if (!AppClass.preferences.getUserId().isEmpty()) {

                switch (view.getId()) {

                }
            } else {
                activity.finishAffinity();
                activity.startActivity(new Intent(activity, StartupActivity.class));
            }

        }

        private void initViews() {

            ll_answer_details = itemView.findViewById(R.id.ll_answer_details);
            ll_document = itemView.findViewById(R.id.ll_document);

            txt_question_value = itemView.findViewById(R.id.txt_question_value);
            txt_answer_data = itemView.findViewById(R.id.txt_answer_data);
            txt_your_vote = itemView.findViewById(R.id.txt_your_vote);


            ll_answer_details.setVisibility(View.VISIBLE);
            ll_document.setVisibility(View.GONE);

        }

        private void setTypeFace() {

            txt_question_value.setTypeface(AppClass.lato_regular);
            txt_answer_data.setTypeface(AppClass.lato_regular);
            txt_your_vote.setTypeface(AppClass.lato_regular);

        }



    }






}
