package com.proclapp.more.bookmark;

import android.content.DialogInterface;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.model.AllPostsModel;
import com.proclapp.model.AnswerModel;
import com.proclapp.more.bookmark.Adapter.BookMarkedPostAdapter;
import com.proclapp.more.bookmark.Adapter.BookmarkedAnsweredPostAdapter;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.AlertDialogInterface;
import com.proclapp.utils.DialogUtils;
import com.proclapp.utils.StringUtils;
import com.proclapp.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class BookMarkedPostActivity extends AppCompatActivity {

    private RecyclerView rv_bookmarked_post;
    private RecyclerView rv_answered_bookmarked_post;
    private ArrayList<AllPostsModel> bookmarkedPostList = new ArrayList<>();
    private ArrayList<AnswerModel> bookmarkedAnsweredPostList = new ArrayList<>();
    private String dirPath = "",
            fileName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_marked_post);
        Utils.setStatusBarColor(getWindow(), this, R.color.white);
        rv_bookmarked_post = findViewById(R.id.rv_bookmarked_post);
        rv_answered_bookmarked_post = findViewById(R.id.rv_answered_bookmarked_post);
        ImageView img_back = findViewById(R.id.img_back);

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        setUpRecyclerView();

    }

    private void setUpRecyclerView() {


        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        rv_bookmarked_post.setLayoutManager(layoutManager);

        rv_answered_bookmarked_post.setLayoutManager(new LinearLayoutManager(this));

        rv_bookmarked_post.setAdapter(new BookMarkedPostAdapter(this, bookmarkedPostList));
        rv_answered_bookmarked_post.setAdapter(new BookmarkedAnsweredPostAdapter(this, bookmarkedAnsweredPostList));

        callAPIBookmarkedPosts();
    }

    private void callAPIBookmarkedPosts() {

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());

        ApiCall.getInstance().bookmarkedPostAnsweredList(this, request, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {

                ArrayList<AllPostsModel> bookmarkedPostListCopy = new Gson().fromJson(((JsonObject) data).getAsJsonArray("posts").toString(),
                        new TypeToken<List<AllPostsModel>>() {
                        }.getType());

                ArrayList<AnswerModel> bookmarkedAnsweredPostListCopy = new Gson().fromJson(((JsonObject) data).getAsJsonArray("post_answers").toString(),
                        new TypeToken<List<AnswerModel>>() {
                        }.getType());

                bookmarkedPostList.addAll(bookmarkedPostListCopy);
                bookmarkedAnsweredPostList.addAll(bookmarkedAnsweredPostListCopy);

                rv_bookmarked_post.getAdapter().notifyDataSetChanged();
                // rv_answered_bookmarked_post.getAdapter().notifyDataSetChanged();

            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {
                if (StringUtils.isNotEmpty(errorMessage)) {
                    DialogUtils.openDialog(BookMarkedPostActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                        @Override
                        public void onNegativeBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }

                        @Override
                        public void onPositiveBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }
                    });
                }
            }
        }, true);


    }


}
