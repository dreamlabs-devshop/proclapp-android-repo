package com.proclapp.more.declinedquestionarticle;

import android.app.Activity;
import android.content.DialogInterface;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.model.AllPostsModel;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.AlertDialogInterface;
import com.proclapp.utils.DateTimeUtils;
import com.proclapp.utils.DialogUtils;
import com.proclapp.utils.ImageLoadUtils;
import com.proclapp.utils.Logger;
import com.proclapp.utils.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class DeclinedQuestionAndArticlesAdapter extends RecyclerView.Adapter<DeclinedQuestionAndArticlesAdapter.MyViewHolder> {

    private Activity activity;
    private ArrayList<AllPostsModel> declinedPostList;
    private DisplayMetrics metrics;

    DeclinedQuestionAndArticlesAdapter(Activity activity, ArrayList<AllPostsModel> declinedPostList) {
        this.activity = activity;
        this.declinedPostList = declinedPostList;
        metrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(activity).inflate(R.layout.row_declined_question_articles, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {

        final AllPostsModel declinedPostData = declinedPostList.get(position);

        if (declinedPostData.getPostType().equalsIgnoreCase("1")) {

            holder.txt_type.setText(activity.getString(R.string.article));

        } else {
            holder.txt_type.setText(activity.getString(R.string.question));
        }

        holder.txt_user_name.setText(declinedPostData.getAuthor());
        holder.txt_title.setText(Html.fromHtml(declinedPostData.getPostTitle()));
        holder.txt_description.setText(Html.fromHtml(declinedPostData.getPostDescription()));
        if (StringUtils.isNotEmpty(declinedPostData.getPostDate()))
            holder.txt_post_date.setText(DateTimeUtils.changeDateTimeFormat("yyyy-MM-dd HH:mm:ss", "dd MMM", declinedPostData.getPostDate()));
        if (StringUtils.isNotEmpty(declinedPostData.getDeclinedPostDate()))
            holder.txt_date.setText(DateTimeUtils.changeDateTimeFormat("yyyy-MM-dd HH:mm:ss", "MMM dd yyyy", declinedPostData.getDeclinedPostDate()));
        holder.txt_user_place.setText(declinedPostData.getProfessionalQualification());

        try {

            if (declinedPostData.getImages().size() > 0) {

                if (declinedPostData.getImages().get(0).getImage_name().isEmpty()) {

                    holder.img_article.setVisibility(View.GONE);
                    holder.img_article_shadow.setVisibility(View.GONE);
                    holder.txt_image_count.setVisibility(View.GONE);

                } else {

                    ImageLoadUtils.imageLoad(activity,
                            holder.img_article,
                            declinedPostData.getImages().get(0).getImage_name());

                    holder.img_article.setVisibility(View.VISIBLE);

                    if (declinedPostData.getImages().size() > 1) {
                        holder.img_article_shadow.setVisibility(View.VISIBLE);
                        holder.txt_image_count.setVisibility(View.VISIBLE);
                        holder.txt_image_count.setText("+" + (declinedPostData.getImages().size() - 1));
                    } else {
                        holder.img_article_shadow.setVisibility(View.GONE);
                        holder.txt_image_count.setVisibility(View.GONE);
                    }
                }
            } else {
                holder.img_article_shadow.setVisibility(View.GONE);
                holder.txt_image_count.setVisibility(View.GONE);
                holder.img_article.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        ImageLoadUtils.imageLoad(activity,
                holder.img_user_profile,
                declinedPostData.getProfileImage(),
                R.drawable.user_img_ph);


        holder.img_dropdown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.ll_article_details.getVisibility() == View.VISIBLE) {
                    holder.ll_article_details.setVisibility(View.GONE);
                    holder.img_dropdown.setImageResource(R.drawable.drop_decline);
                } else {
                    holder.ll_article_details.setVisibility(View.VISIBLE);
                    holder.img_dropdown.setImageResource(R.drawable.up_decline);
                }
            }
        });

        holder.img_undo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callAPIUndoDeclinedPost(declinedPostData.getPostId(), holder.getAdapterPosition());
            }
        });

        holder.img_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callAPIDeleteDeclinedPost(declinedPostData.getPostId(), holder.getAdapterPosition());
            }
        });

    }

    @Override
    public int getItemCount() {
        return declinedPostList.size();
    }

    private void callAPIUndoDeclinedPost(String post_id, final int position) {

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("post_id", post_id);

        Logger.d("request:" + request);

        ApiCall.getInstance().undoDeclinedPostList(activity,
                request, new IApiCallback() {
                    @Override
                    public void onSuccess(Object data) {

                        declinedPostList.remove(position);
                        notifyDataSetChanged();

                    }

                    @Override
                    public void onFailure(boolean isFalseFromService, String errorMessage) {


                        DialogUtils.openDialog(activity, errorMessage, activity.getString(R.string.ok), "", new AlertDialogInterface() {
                            @Override
                            public void onNegativeBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }

                            @Override
                            public void onPositiveBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }
                        });
                    }


                }, true);

    }

    private void callAPIDeleteDeclinedPost(String post_id, final int position) {

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("post_id", post_id);

        Logger.d("request:" + request);

        ApiCall.getInstance().deleteDeclinedPost(activity,
                request, new IApiCallback() {
                    @Override
                    public void onSuccess(Object data) {

                        declinedPostList.remove(position);
                        notifyDataSetChanged();

                    }

                    @Override
                    public void onFailure(boolean isFalseFromService, String errorMessage) {


                        DialogUtils.openDialog(activity, errorMessage, activity.getString(R.string.ok), "", new AlertDialogInterface() {
                            @Override
                            public void onNegativeBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }

                            @Override
                            public void onPositiveBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }
                        });
                    }


                }, true);

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView
                txt_date,
                txt_type,
                txt_user_name,
                txt_user_place,
                txt_post_date,
                txt_title,
                txt_image_count,
                txt_description;
        ImageView
                img_undo,
                img_delete,
                img_option,
                img_article,
                img_article_shadow,
                img_dropdown;

        LinearLayout ll_article_details;
        CircleImageView img_user_profile;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            txt_date = itemView.findViewById(R.id.txt_date);
            txt_type = itemView.findViewById(R.id.txt_type);
            txt_user_name = itemView.findViewById(R.id.txt_user_name);
            txt_user_place = itemView.findViewById(R.id.txt_user_place);
            txt_post_date = itemView.findViewById(R.id.txt_post_date);
            txt_title = itemView.findViewById(R.id.txt_title);
            txt_description = itemView.findViewById(R.id.txt_description);
            txt_image_count = itemView.findViewById(R.id.txt_image_count);

            img_undo = itemView.findViewById(R.id.img_undo);
            img_delete = itemView.findViewById(R.id.img_delete);
            img_option = itemView.findViewById(R.id.img_option);
            img_article = itemView.findViewById(R.id.img_article);
            img_article_shadow = itemView.findViewById(R.id.img_article_shadow);
            img_dropdown = itemView.findViewById(R.id.img_dropdown);

            ll_article_details = itemView.findViewById(R.id.ll_article_details);

            img_user_profile = itemView.findViewById(R.id.img_user_profile);

            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams((metrics.widthPixels * 124) / 480
                    , (metrics.heightPixels * 124) / 800);
            img_article.setLayoutParams(params);
            img_article.setScaleType(ImageView.ScaleType.CENTER_CROP);
            img_article_shadow.setLayoutParams(params);
        }
    }


}
