package com.proclapp.more.declinedquestionarticle;

import android.content.DialogInterface;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.model.AllPostsModel;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.AlertDialogInterface;
import com.proclapp.utils.DialogUtils;
import com.proclapp.utils.Logger;
import com.proclapp.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;

public class DeclinedQuestionArcticlesActivity extends AppCompatActivity implements View.OnClickListener, SwipyRefreshLayout.OnRefreshListener {

    private ImageView img_back;

    private TextView
            txt_title,
            txt_undo_all,
            tv_no_data,
            txt_delete_all;
    private RecyclerView rv_decline_question_articles;
    private ArrayList<AllPostsModel> declinedPostList = new ArrayList<>();
    private int offset = 0;
    private SwipyRefreshLayout sr_decline_question_articles;
    private boolean isServiceCalling = false, isLastPage = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_declined_question_arcticles);
        Utils.setStatusBarColor(getWindow(), this, R.color.white);
        Utils.setStatusBarColor(getWindow(), this, R.color.white);

        initView();
        initListener();
        setupRecyclerView();

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.img_back:

                onBackPressed();

                break;

            case R.id.txt_undo_all:

                if (declinedPostList.size() > 0) {

                    StringBuilder builder = new StringBuilder();
                    String prefix = "";

                    for (int i = 0; i < declinedPostList.size(); i++) {

                        builder.append(prefix + declinedPostList.get(i).getPostId());
                        prefix = ",";

                    }

                    callAPIUndoDeclinedPost(builder.toString());
                }

                break;

            case R.id.txt_delete_all:

                if (declinedPostList.size() > 0) {

                    StringBuilder builder = new StringBuilder();
                    String prefix = "";

                    for (int i = 0; i < declinedPostList.size(); i++) {

                        builder.append(prefix + declinedPostList.get(i).getPostId());
                        prefix = ",";

                    }

                    callAPIDeleteDeclinedPost(builder.toString());
                }

                break;

        }
    }


    @Override
    public void onRefresh(SwipyRefreshLayoutDirection direction) {
        if (direction == SwipyRefreshLayoutDirection.TOP) {
            offset = 0;
            callAPIDeclinedPostList();
        } else {
            offset += 20;
            callAPIDeclinedPostList();
        }
    }

    private void initView() {

        img_back = findViewById(R.id.img_back);

        txt_title = findViewById(R.id.txt_title);
        tv_no_data = findViewById(R.id.tv_no_data);
        txt_undo_all = findViewById(R.id.txt_undo_all);
        txt_delete_all = findViewById(R.id.txt_delete_all);
        sr_decline_question_articles = findViewById(R.id.sr_decline_question_articles);
        sr_decline_question_articles.setOnRefreshListener(this);
        sr_decline_question_articles.setColorSchemeColors(getResources().getColor(R.color.color_0c6984), getResources().getColor(R.color.black));

    }

    private void setupRecyclerView() {
        rv_decline_question_articles = findViewById(R.id.rv_decline_question_articles);

        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);

        rv_decline_question_articles.setLayoutManager(layoutManager);
        rv_decline_question_articles.setAdapter(new DeclinedQuestionAndArticlesAdapter(this, declinedPostList));

        sr_decline_question_articles.post(new Runnable() {
            @Override
            public void run() {
                sr_decline_question_articles.setRefreshing(true);
                onRefresh(SwipyRefreshLayoutDirection.TOP);
            }
        });

        rv_decline_question_articles.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                int visibleItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int firstVisibleItemPosition = layoutManager.findLastVisibleItemPosition();

                if (!isServiceCalling && !isLastPage) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0) {
                        sr_decline_question_articles.post(new Runnable() {
                            @Override
                            public void run() {
                                sr_decline_question_articles.setRefreshing(true);
                                onRefresh(SwipyRefreshLayoutDirection.BOTTOM);
                            }
                        });

                    }
                }

            }
        });

    }

    private void initListener() {

        img_back.setOnClickListener(this);
        txt_undo_all.setOnClickListener(this);
        txt_delete_all.setOnClickListener(this);
    }


    private void callAPIDeclinedPostList() {
        isServiceCalling = true;
        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("offset", String.valueOf(offset));

        Logger.d("request:" + request);

        ApiCall.getInstance().declinedPostList(DeclinedQuestionArcticlesActivity.this,
                request, new IApiCallback() {
                    @Override
                    public void onSuccess(Object data) {
                        if (offset == 0)
                            declinedPostList.clear();
                        declinedPostList.addAll((ArrayList<AllPostsModel>) data);

                        isLastPage = false;
                        isServiceCalling = false;

                        tv_no_data.setVisibility(View.GONE);
                        rv_decline_question_articles.setVisibility(View.VISIBLE);
                        rv_decline_question_articles.getAdapter().notifyDataSetChanged();

                        sr_decline_question_articles.setRefreshing(false);


                    }

                    @Override
                    public void onFailure(boolean isFalseFromService, String errorMessage) {

                        sr_decline_question_articles.setRefreshing(false);
                        isLastPage = true;
                        isServiceCalling = false;


                        if (errorMessage.equalsIgnoreCase("No data found")) {

                            if (offset == 0) {
                                tv_no_data.setVisibility(View.VISIBLE);
                                rv_decline_question_articles.setVisibility(View.GONE);
                            }

                        } else {

                            DialogUtils.openDialog(DeclinedQuestionArcticlesActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                                @Override
                                public void onNegativeBtnClicked(DialogInterface alrt) {
                                    alrt.dismiss();
                                }

                                @Override
                                public void onPositiveBtnClicked(DialogInterface alrt) {
                                    alrt.dismiss();
                                }
                            });

                        }
                    }


                }, false);

    }

    private void callAPIUndoDeclinedPost(String post_id) {

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("post_id", post_id);

        Logger.d("request:" + request);

        ApiCall.getInstance().undoDeclinedPostList(DeclinedQuestionArcticlesActivity.this,
                request, new IApiCallback() {
                    @Override
                    public void onSuccess(Object data) {

                        declinedPostList.clear();
                        rv_decline_question_articles.getAdapter().notifyDataSetChanged();

                        tv_no_data.setVisibility(View.VISIBLE);
                        rv_decline_question_articles.setVisibility(View.GONE);
                    }

                    @Override
                    public void onFailure(boolean isFalseFromService, String errorMessage) {


                        DialogUtils.openDialog(DeclinedQuestionArcticlesActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                            @Override
                            public void onNegativeBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }

                            @Override
                            public void onPositiveBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }
                        });
                    }


                }, true);

    }


    private void callAPIDeleteDeclinedPost(String post_id) {

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("post_id", post_id);

        Logger.d("request:" + request);

        ApiCall.getInstance().deleteDeclinedPost(DeclinedQuestionArcticlesActivity.this,
                request, new IApiCallback() {
                    @Override
                    public void onSuccess(Object data) {

                        declinedPostList.clear();
                        rv_decline_question_articles.getAdapter().notifyDataSetChanged();

                        tv_no_data.setVisibility(View.VISIBLE);
                        rv_decline_question_articles.setVisibility(View.GONE);
                    }

                    @Override
                    public void onFailure(boolean isFalseFromService, String errorMessage) {


                        DialogUtils.openDialog(DeclinedQuestionArcticlesActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                            @Override
                            public void onNegativeBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }

                            @Override
                            public void onPositiveBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }
                        });
                    }


                }, true);

    }
}
