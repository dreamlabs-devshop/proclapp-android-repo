package com.proclapp.more.helpandsupport;

import android.content.DialogInterface;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.proclapp.R;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.AlertDialogInterface;
import com.proclapp.utils.DialogUtils;
import com.proclapp.utils.Logger;
import com.proclapp.utils.StringUtils;
import com.proclapp.utils.Utils;

public class AboutAppPrivacyTCActivity extends AppCompatActivity implements View.OnClickListener {

    String url = "";
    ImageView img_back;
    TextView txt_title, tv_text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_app_privacy_tc);
        Utils.setStatusBarColor(getWindow(), this, R.color.white);
        initViews();
        setOnClickListener();
        url = getIntent().getStringExtra("url");
        Logger.e("url " + url);

        if (url.equalsIgnoreCase("about_us")) {
            txt_title.setText(getString(R.string.about_us));
        } else if (url.equalsIgnoreCase("privacy_policy")) {
            txt_title.setText(getString(R.string.privacy));
        } else {
            txt_title.setText(getString(R.string.term_condition));
        }

        callAPIAboutAppPrivacyTC();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.img_back:
                onBackPressed();
                break;
        }
    }

    private void initViews() {

        img_back = findViewById(R.id.img_back);
        txt_title = findViewById(R.id.txt_title);
        tv_text = findViewById(R.id.tv_text);

        tv_text.setMovementMethod(new ScrollingMovementMethod());


    }


    private void setOnClickListener() {
        img_back.setOnClickListener(this);


    }


    private void callAPIAboutAppPrivacyTC() {


        ApiCall.getInstance().aboutAppPrivacyTC(this, url, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {

                String response = (String) data;
                tv_text.setText(Html.fromHtml(response));

            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {
                if (StringUtils.isNotEmpty(errorMessage)) {
                    DialogUtils.openDialog(AboutAppPrivacyTCActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                        @Override
                        public void onNegativeBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }

                        @Override
                        public void onPositiveBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }
                    });
                }
            }
        }, true);

    }

}
