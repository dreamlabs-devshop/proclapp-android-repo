package com.proclapp.more.helpandsupport;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.proclapp.R;
import com.proclapp.utils.Utils;

public class HelpAndSupportActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView img_back;
    RelativeLayout
            rl_about_app,
            rl_term_condition,
            rl_privacy_policy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help_and_support);
        Utils.setStatusBarColor(getWindow(), this, R.color.white);
        initViews();
        setOnClickListener();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.img_back:
                onBackPressed();
                break;


            case R.id.rl_about_app:
                startActivity(new Intent(HelpAndSupportActivity.this, AboutAppPrivacyTCActivity.class)
                        .putExtra("url", "about_us"));
                break;

            case R.id.rl_term_condition:
                startActivity(new Intent(HelpAndSupportActivity.this, AboutAppPrivacyTCActivity.class)
                        .putExtra("url", "term_and_conditions"));
                break;

            case R.id.rl_privacy_policy:
                startActivity(new Intent(HelpAndSupportActivity.this, AboutAppPrivacyTCActivity.class)
                        .putExtra("url", "privacy_policy"));
                break;


        }
    }

    private void initViews() {
        img_back = findViewById(R.id.img_back);
        rl_about_app = findViewById(R.id.rl_about_app);
        rl_term_condition = findViewById(R.id.rl_term_condition);
        rl_privacy_policy = findViewById(R.id.rl_privacy_policy);

    }


    private void setOnClickListener() {
        img_back.setOnClickListener(this);
        rl_about_app.setOnClickListener(this);
        rl_term_condition.setOnClickListener(this);
        rl_privacy_policy.setOnClickListener(this);

    }

}
