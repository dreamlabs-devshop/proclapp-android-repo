package com.proclapp.more.settings.Adapter;

import android.content.DialogInterface;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.model.NotificationAlertModel;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.AlertDialogInterface;
import com.proclapp.utils.DialogUtils;
import com.proclapp.utils.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;

public class NotificationAlertAdapter extends RecyclerView.Adapter<NotificationAlertAdapter.AlertViewHolder> {

    AppCompatActivity context;
    ArrayList<NotificationAlertModel> alertList;

    public NotificationAlertAdapter(AppCompatActivity context, ArrayList<NotificationAlertModel> alertList) {
        this.context = context;
        this.alertList = alertList;

    }

    @NonNull
    @Override
    public AlertViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_notification_alert, viewGroup, false);
        return new AlertViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final AlertViewHolder holder, int position) {

        final NotificationAlertModel alertData = alertList.get(position);

        if (StringUtils.isNotEmpty(alertData.getNotificationType())) {
            holder.tv_alert_name.setText(alertData.getNotificationType());
        } else {
            holder.tv_alert_name.setText("");
        }

        if (alertData.getStatus().equalsIgnoreCase("1")) {
            holder.switch_alert_name.setChecked(true);
        } else {
            holder.switch_alert_name.setChecked(false);
        }

        holder.ll_notification_alert_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (holder.switch_alert_name.isChecked()) {
                    callAPIUpdateNotificationStatus(alertData.getNotificationTypeId(), "0", holder.switch_alert_name);
                } else {
                    callAPIUpdateNotificationStatus(alertData.getNotificationTypeId(), "1", holder.switch_alert_name);
                }

            }
        });

        holder.switch_alert_name.setClickable(false);

    }

    @Override
    public int getItemCount() {
        return alertList.size();
    }

    private void callAPIUpdateNotificationStatus(String notiType, final String status, final Switch switch_alert_name) {

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("notification_type", notiType);
        request.put("status", status);

        ApiCall.getInstance().updateNotificationStatus(context, request, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {
                String response = (String) data;
                if (status.equalsIgnoreCase("0")) {
                    switch_alert_name.setChecked(false);
                } else {
                    switch_alert_name.setChecked(true);
                }
            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {
                if (StringUtils.isNotEmpty(errorMessage)) {
                    DialogUtils.openDialog(context, errorMessage, context.getString(R.string.ok), "", new AlertDialogInterface() {
                        @Override
                        public void onNegativeBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }

                        @Override
                        public void onPositiveBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }
                    });
                }
            }
        }, true);


    }

    public class AlertViewHolder extends RecyclerView.ViewHolder {
        TextView tv_alert_name;
        Switch switch_alert_name;
        LinearLayout ll_notification_alert_main;

        public AlertViewHolder(@NonNull View itemView) {
            super(itemView);

            tv_alert_name = itemView.findViewById(R.id.tv_alert_name);
            switch_alert_name = itemView.findViewById(R.id.switch_alert_name);
            ll_notification_alert_main = itemView.findViewById(R.id.ll_notification_alert_main);

        }
    }

}
