package com.proclapp.more.settings;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.home.HomeActivity;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.AlertDialogInterface;
import com.proclapp.utils.DialogUtils;
import com.proclapp.utils.KeyBoardUtils;
import com.proclapp.utils.Logger;
import com.proclapp.utils.ScreenCode;
import com.proclapp.utils.StringUtils;
import com.proclapp.utils.Utils;

import java.util.HashMap;

public class ChangePasswordActivity extends AppCompatActivity implements View.OnClickListener {

    EditText edt_password,
            edt_conf_password;

    Button btn_change;

    ImageView img_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        Utils.setStatusBarColor(getWindow(), this, R.color.white);
        initViews();
        setOnClickListener();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.img_back:

                onBackPressed();

                break;

            case R.id.btn_change:

                KeyBoardUtils.closeSoftKeyboard(ChangePasswordActivity.this);
                if (isValid()) {
                    callAPIChangePasword();
                }
                break;

        }
    }

    private void initViews() {

        edt_password = findViewById(R.id.edt_password);
        edt_conf_password = findViewById(R.id.edt_conf_password);
        btn_change = findViewById(R.id.btn_change);
        img_back = findViewById(R.id.img_back);


    }


    private void setOnClickListener() {

        btn_change.setOnClickListener(this);
        img_back.setOnClickListener(this);

    }

    private void callAPIChangePasword() {

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("password", edt_password.getText().toString().trim());

        ApiCall.getInstance().changePassword(ChangePasswordActivity.this, request, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {

                String response = (String) data;

                DialogUtils.openDialog(ChangePasswordActivity.this, response, getString(R.string.ok), "", new AlertDialogInterface() {
                    @Override
                    public void onNegativeBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                        finishAffinity();
                        AppClass.preferences.storeScreeCode(ScreenCode.NormalScreen);
                        startActivity(new Intent(ChangePasswordActivity.this, HomeActivity.class));
                    }

                    @Override
                    public void onPositiveBtnClicked(DialogInterface alrt) {
                        alrt.dismiss();
                        finishAffinity();
                        AppClass.preferences.storeScreeCode(ScreenCode.NormalScreen);
                        startActivity(new Intent(ChangePasswordActivity.this, HomeActivity.class));
                    }
                });

            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {
                Logger.e("responseFail " + errorMessage);
                if (StringUtils.isNotEmpty(errorMessage)) {
                    DialogUtils.openDialog(ChangePasswordActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                        @Override
                        public void onNegativeBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }

                        @Override
                        public void onPositiveBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }
                    });
                }
            }
        }, true);

    }

    private boolean isValid() {

        boolean flag = true;
        String password = edt_password.getText().toString().trim();
        String confPassword = edt_conf_password.getText().toString().trim();


        if (!StringUtils.isNotEmpty(password)) {
            flag = false;
            AppClass.snackBarView.snackBarShow(ChangePasswordActivity.this, getResources().getString(R.string.vpswd), ContextCompat.getColor(ChangePasswordActivity.this, R.color.red));
            edt_password.requestFocus();
        } else if (password.length() < 6 || password.length() > 15) {
            flag = false;
            edt_password.requestFocus();
            AppClass.snackBarView.snackBarShow(ChangePasswordActivity.this, getResources().getString(R.string.vpswd_6), ContextCompat.getColor(ChangePasswordActivity.this, R.color.red));
        } else if (!StringUtils.isNotEmpty(confPassword)) {
            flag = false;
            edt_conf_password.requestFocus();
            AppClass.snackBarView.snackBarShow(ChangePasswordActivity.this, getResources().getString(R.string.vcpswd), ContextCompat.getColor(ChangePasswordActivity.this, R.color.red));
        } else if (!confPassword.equals(password)) {
            flag = false;
            AppClass.snackBarView.snackBarShow(ChangePasswordActivity.this, getResources().getString(R.string.vpswd_match), ContextCompat.getColor(ChangePasswordActivity.this, R.color.red));
        }


        return flag;
    }


}
