package com.proclapp.more.settings;

import android.content.DialogInterface;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.model.NotificationAlertModel;
import com.proclapp.more.settings.Adapter.NotificationAlertAdapter;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.AlertDialogInterface;
import com.proclapp.utils.DialogUtils;
import com.proclapp.utils.StringUtils;
import com.proclapp.utils.Utils;

import java.util.ArrayList;

public class NotificationAlertActivity extends AppCompatActivity {

    ArrayList<NotificationAlertModel> alertList = new ArrayList<>();
    private RecyclerView rv_notification_alert;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_alert);
        Utils.setStatusBarColor(getWindow(), this, R.color.white);
        rv_notification_alert = findViewById(R.id.rv_notification_alert);
        ImageView img_back = findViewById(R.id.img_back);

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        setUpRecyclerView();
    }


    private void setUpRecyclerView() {

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        rv_notification_alert.setLayoutManager(layoutManager);
        rv_notification_alert.setAdapter(new NotificationAlertAdapter(this, alertList));

        callAPIGetNotificationAlertType();
    }

    private void callAPIGetNotificationAlertType() {


        ApiCall.getInstance().getNotificationAlertType(this, AppClass.preferences.getUserId(), new IApiCallback() {
            @Override
            public void onSuccess(Object data) {

                alertList.clear();
                alertList.addAll((ArrayList<NotificationAlertModel>) data);
                rv_notification_alert.getAdapter().notifyDataSetChanged();
            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {
                if (StringUtils.isNotEmpty(errorMessage)) {
                    DialogUtils.openDialog(NotificationAlertActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                        @Override
                        public void onNegativeBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }

                        @Override
                        public void onPositiveBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }
                    });
                }
            }
        }, true);


    }
}
