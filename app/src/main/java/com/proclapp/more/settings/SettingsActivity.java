package com.proclapp.more.settings;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.proclapp.R;
import com.proclapp.utils.Utils;

public class SettingsActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView img_back;
    RelativeLayout rl_change_password, rl_notification_alert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        Utils.setStatusBarColor(getWindow(), this, R.color.white);
        initViews();
        setOnClickListener();

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.img_back:
                onBackPressed();
                break;

            case R.id.rl_change_password:
                startActivity(new Intent(SettingsActivity.this, ChangePasswordActivity.class));
                break;

            case R.id.rl_notification_alert:
                startActivity(new Intent(SettingsActivity.this, NotificationAlertActivity.class));
                break;
        }


    }

    private void initViews() {
        img_back = findViewById(R.id.img_back);
        rl_change_password = findViewById(R.id.rl_change_password);
        rl_notification_alert = findViewById(R.id.rl_notification_alert);

    }


    private void setOnClickListener() {
        img_back.setOnClickListener(this);
        rl_notification_alert.setOnClickListener(this);
        rl_change_password.setOnClickListener(this);

    }

}
