package com.proclapp.payment;

import android.graphics.Bitmap;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.utils.StaticData;
import com.proclapp.utils.Utils;

public class SubscribeUserWebViewActivity extends AppCompatActivity {

    WebView webview;
    RelativeLayout rl_progress_bar;
    ImageView img_back;
    private String postId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);
        postId = getIntent().getStringExtra("post_id");
        Utils.setStatusBarColor(getWindow(), this, R.color.white);
        initViews();
        loadWebView();


    }

    private void initViews() {
        webview = findViewById(R.id.webview);
        rl_progress_bar = findViewById(R.id.rl_progress_bar);
        img_back = findViewById(R.id.img_back);

        img_back.setOnClickListener(view -> onBackPressed());
    }


    private void loadWebView() {

        String url = "http://35.181.75.245/ws/v1/api/do_payment_subscribe_user?user_id=" + AppClass.preferences.getUserId() + "&post_id=" + postId;
        http:
//35.181.75.245/ws/v1/api/do_payment_subscribe_user?user_id=1&

        if (isFinishing()) {
            onBackPressed();
        } else {

            webview.getSettings().setLoadsImagesAutomatically(true);
            webview.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
            webview.getSettings().setJavaScriptEnabled(true);
            webview.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
            webview.getSettings().setDomStorageEnabled(true);

            WebViewClient webViewClient = new WebViewClient() {

                @Override
                public void onPageStarted(WebView view, String url, Bitmap favicon) {
                    super.onPageStarted(view, url, favicon);
                    rl_progress_bar.setVisibility(View.VISIBLE);
                }

                @Override
                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(view, url);
                    rl_progress_bar.setVisibility(View.GONE);
                    if (url.equalsIgnoreCase("http://35.181.75.245/ws/v1/api/payment_success")) {
                        StaticData.isEdited = true;
                        AppClass.toastView.showMessage("Payment Success");
                        finish();
                    } else if (url.equalsIgnoreCase("http://35.181.75.245/ws/v1/api/payment_failed")) {
                        AppClass.toastView.showMessage("Payment Fail");
                        finish();
                    }

                }
            };

            webview.setWebViewClient(webViewClient);

            webview.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
            webview.loadUrl(url);
        }

    }

}
