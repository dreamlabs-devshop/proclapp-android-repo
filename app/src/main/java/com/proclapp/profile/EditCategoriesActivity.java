package com.proclapp.profile;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.model.CategoryModel;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.signup.adapter.CategoryAdapter;
import com.proclapp.utils.AlertDialogInterface;
import com.proclapp.utils.DialogUtils;
import com.proclapp.utils.Logger;
import com.proclapp.utils.StringUtils;
import com.proclapp.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;

public class EditCategoriesActivity extends AppCompatActivity implements View.OnClickListener {

    ArrayList<String> selectedCategories = new ArrayList<>();
    private CategoryAdapter categoryAdapter;
    private TextView txt_select_interested_category, txt_done;
    private ImageView img_back;
    private RecyclerView rv_category_list;
    private Activity activity;
    private ProgressDialog progressDialog;
    private ArrayList<CategoryModel> categoryList = new ArrayList<>();
    private EditText edt_search;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_categories);
        Utils.setStatusBarColor(getWindow(), this, R.color.white);
        activity = this;
        initView();
        getIntentData();
        callAPICategoryList();

    }

    private void getIntentData() {

        if (StringUtils.isNotEmpty(getIntent().getStringExtra("selectedCategories"))) {

//        ArrayList<String> selectedCategories = getIntent().getStringArrayListExtra("data");
            selectedCategories = new Gson().fromJson(getIntent().getStringExtra("selectedCategories"), new TypeToken<ArrayList<String>>() {
            }.getType());
            Logger.e("StringArray " + selectedCategories.toString());
        }
    }

    private void initView() {
        txt_select_interested_category = findViewById(R.id.txt_select_interested_category);
        txt_done = findViewById(R.id.txt_done);
        rv_category_list = findViewById(R.id.rv_category_list);
        edt_search = findViewById(R.id.edt_search);
        img_back = findViewById(R.id.img_back);
        GridLayoutManager layoutManager = new GridLayoutManager(activity, 3);
        rv_category_list.setLayoutManager(layoutManager);

        txt_select_interested_category.setTypeface(AppClass.lato_regular);
        edt_search.setTypeface(AppClass.lato_regular);
        txt_done.setTypeface(AppClass.lato_regular);
        txt_done.setOnClickListener(this);
        img_back.setOnClickListener(this);

        edt_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (categoryAdapter != null) {
                    categoryAdapter.getFilter().filter(charSequence);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


    }

    private void setCategoryList() {


        /*for (int i = 0; i < categoryList.size(); i++) {

            // for (int j = 0; j < selectedCategories.size(); j++) {

            if (categoryList.get(i).getIs_selected() == 1) {
                categoryList.get(i).setIs_selected(1);
            }
            // }
        }*/

        categoryAdapter = new CategoryAdapter(activity, categoryList, new CategoryAdapter.CategorySelection() {
            @Override
            public void onCategorySelection(String countryCode, String countryName) {

            }
        }, selectedCategories);
        rv_category_list.setAdapter(categoryAdapter);
    }


    private void callAPICategoryList() {

        if (AppClass.isInternetConnectionAvailable()) {

            HashMap<String, String> request = new HashMap<>();
            request.put("user_id", AppClass.preferences.getUserId());

            ApiCall.getInstance().getCategories(EditCategoriesActivity.this, request, new IApiCallback() {
                @Override
                public void onSuccess(Object data) {

                    categoryList.clear();
                    categoryList.addAll((ArrayList<CategoryModel>) data);

                    setCategoryList();

                }

                @Override
                public void onFailure(boolean isFalseFromService, String errorMessage) {
                    if (StringUtils.isNotEmpty(errorMessage)) {
                        DialogUtils.openDialog(EditCategoriesActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                            @Override
                            public void onNegativeBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }

                            @Override
                            public void onPositiveBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }
                        });
                    }
                }
            }, true);


        } else {
            AppClass.snackBarView.snackBarShow(activity, getString(R.string.nonetwork));
        }


    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.txt_done:

                StringBuilder stringBuilder = new StringBuilder();
                String prefix = "";

                for (int i = 0; i < categoryList.size(); i++) {

                    if (categoryList.get(i).getIs_selected() == 1) {
                        stringBuilder.append(prefix + categoryList.get(i).getCategory_id());
                        prefix = ",";
                    }
                }

                Logger.e("selected category ==>" + stringBuilder);

//                finalSignUp.callSignUp(stringBuilder.toString());

                updateCategoryToUserProfile(stringBuilder.toString());

                break;


            case R.id.img_back:
                onBackPressed();
                break;
        }
    }


    private void updateCategoryToUserProfile(String categories) {

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("category", categories);

        ApiCall.getInstance().updateCategoryToUserProfile(activity, request, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {
                String responce = (String) data;
                AppClass.snackBarView.snackBarShow(activity, responce);
                onBackPressed();

            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {
                if (StringUtils.isNotEmpty(errorMessage)) {
                    DialogUtils.openDialog(EditCategoriesActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                        @Override
                        public void onNegativeBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }

                        @Override
                        public void onPositiveBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }
                    });
                }
            }
        }, true);


    }

}
