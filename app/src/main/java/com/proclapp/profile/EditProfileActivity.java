package com.proclapp.profile;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.home.HomeActivity;
import com.proclapp.model.CategoryModel;
import com.proclapp.model.CountryModel;
import com.proclapp.model.EducationalQualificationModel;
import com.proclapp.model.ProfessionalQualificationModel;
import com.proclapp.payment.WebviewActivity;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.signup.adapter.CountryAdapter;
import com.proclapp.utils.AlertDialogInterface;
import com.proclapp.utils.DialogUtils;
import com.proclapp.utils.ImageLoadUtils;
import com.proclapp.utils.ImagePickerUtils.ImagePickerBottomSheet;
import com.proclapp.utils.Logger;
import com.proclapp.utils.ScreenCode;
import com.proclapp.utils.StringUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.RequestBody;

public class EditProfileActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int PAYMENT_REQUEST = 123;
    // educational qualification data
    public static ArrayList<EducationalQualificationModel> qualificationArrayList = new ArrayList<>();
    public static ArrayList<ProfessionalQualificationModel> professionalQuaArrayList = new ArrayList<>();
    private ImageView
            img_cover,
            img_back,
            img_dropdown_gender,
            img_dropdown_category;
    private CircleImageView img_profile;
    private TextView
            txt_edit_profile,
            txt_save,
            txt_edit_cover,
            txt_personal_info,
            txt_firstname,
            txt_lastname,
            txt_email,
            txt_mobile_number,
            txt_country_code,
            txt_gender,
            txt_select_category,
            txt_about_me,
            txt_educational_qualification,
            txt_professional_qualification,
            txt_about_my_professional,
            txt_add_professional,
            txt_add_edu,
            txt_residential_address;
    private EditText
            edt_firstname,
            edt_lastname,
            edt_email_address,
            edt_mobile_number,
            edt_about_me_value,
            edt_about_my_professional_value,
            edt_residential_address;
    private RecyclerView rv_edu_qualification,
            rv_professional_qualification;
    private EduQualificationAdapter eduQualificationAdapter;
    private ProfessionalQualificationAdapter professionalQualificationAdapter;
    private CollapsingToolbarLayout collapsing_toolbar;
    private AppBarLayout app_bar_layout;
    private Toolbar toolbar;
    private Spinner
            spinner_select_gender,
            spinner_select_category;
    private DisplayMetrics metrics;
    private String phone_code = "";
    private String gender = "1";
    private String isExpert = "0";
    private File file_cover, file_profile;
    private ArrayList<String> yearList;
    private ArrayList<String> experience_year_list = new ArrayList<>();
    private ProgressDialog progressDialog;
    private ArrayAdapter<String> spinnerGenderArrayAdapter;
    private ArrayList<String> spinnerGenderArray;
    private ArrayList<CategoryModel> categoryList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.loading));

        initView();
        initStyle();
        initListener();

        setGenderData();
//        callAPIGetProfile();
        callAPIGetProfile();
        setSpinnerCategoryList();
        //setEducationalQualificationData();
        //setProfessionalQualificationData();

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.img_cover:

                new ImagePickerBottomSheet(EditProfileActivity.this, new ImagePickerBottomSheet.ImagePickListener() {
                    @Override
                    public void onPickImage(File imageFile) {

                        file_cover = imageFile;


                        Bitmap myBitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath());
                        img_cover.setImageBitmap(myBitmap);
                    }
                }).show(getSupportFragmentManager(), "");

                break;

            case R.id.img_back:

                onBackPressed();

                break;

            case R.id.img_profile:

                new ImagePickerBottomSheet(EditProfileActivity.this, new ImagePickerBottomSheet.ImagePickListener() {
                    @Override
                    public void onPickImage(File imageFile) {

                        file_profile = imageFile;

                        Bitmap myBitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath());
                        img_profile.setImageBitmap(myBitmap);
                    }
                }).show(getSupportFragmentManager(), "");

                break;

            case R.id.txt_add_edu:
                if (qualificationArrayList.size() > 0) {

                    EducationalQualificationModel model = new EducationalQualificationModel();
                    model.setEducational_qualification("");
                    model.setPassing_year("");
                    Logger.d("getItemCount==" + (eduQualificationAdapter.getItemCount()));
                    qualificationArrayList.add(eduQualificationAdapter.getItemCount(), model);


                    if (eduQualificationAdapter.getItemCount() > 2) {
                        eduQualificationAdapter.notifyItemInserted(eduQualificationAdapter.getItemCount());
                    } else {
                        eduQualificationAdapter.notifyDataSetChanged();
                    }

                    rv_edu_qualification.scrollToPosition(qualificationArrayList.size());
                } else {
                    qualificationArrayList = new ArrayList<>();
                    EducationalQualificationModel model = new EducationalQualificationModel();
                    model.setEducational_qualification("");
                    model.setPassing_year("");
                    qualificationArrayList.add(0, model);
                }

                break;

            case R.id.txt_add_professional:

                if (professionalQuaArrayList.size() > 0) {


                    ProfessionalQualificationModel professionalQualificationModel = new ProfessionalQualificationModel();
                    professionalQualificationModel.setJob_title("");
                    professionalQualificationModel.setExperience("");
                    professionalQualificationModel.setIsCurrentJob("0");
                    Logger.d("getItemCount==" + (professionalQualificationAdapter.getItemCount()));
                    professionalQuaArrayList.add(professionalQualificationAdapter.getItemCount(), professionalQualificationModel);


                    if (professionalQualificationAdapter.getItemCount() > 2) {
                        professionalQualificationAdapter.notifyItemInserted(professionalQualificationAdapter.getItemCount());
                    } else {
                        professionalQualificationAdapter.notifyDataSetChanged();
                    }

                    rv_professional_qualification.scrollToPosition(professionalQuaArrayList.size());

                } else {
                    professionalQuaArrayList = new ArrayList<>();
                    ProfessionalQualificationModel professionalQualificationModel = new ProfessionalQualificationModel();
                    professionalQualificationModel.setJob_title("");
                    professionalQualificationModel.setExperience("");
                    professionalQualificationModel.setIsCurrentJob("0");
                    professionalQuaArrayList.add(0, professionalQualificationModel);
                }

                break;

            case R.id.img_dropdown_gender:

                spinner_select_gender.performClick();

                break;

            case R.id.img_dropdown_category:

                spinner_select_category.performClick();

                break;

            case R.id.txt_save:
                if (AppClass.isInternetConnectionAvailable()) {
                    if (validation()) {
                        callAPIUpdateProfile();
                    }
                } else {
                    AppClass.snackBarView.snackBarShow(EditProfileActivity.this, getResources().getString(R.string.nonetwork));
                }

                break;

            case R.id.txt_country_code:
                new DialogCountryCode(EditProfileActivity.this).show();
                break;
        }
    }

    private void initView() {

        img_cover = findViewById(R.id.img_cover);
        img_back = findViewById(R.id.img_back);
        img_profile = findViewById(R.id.img_profile);

        img_dropdown_gender = findViewById(R.id.img_dropdown_gender);
        img_dropdown_category = findViewById(R.id.img_dropdown_category);

        txt_edit_profile = findViewById(R.id.txt_edit_profile);
        txt_save = findViewById(R.id.txt_save);
        txt_edit_cover = findViewById(R.id.txt_edit_cover);
        txt_personal_info = findViewById(R.id.txt_personal_info);
        txt_firstname = findViewById(R.id.txt_firstname);
        txt_lastname = findViewById(R.id.txt_lastname);
        txt_email = findViewById(R.id.txt_email);
        txt_mobile_number = findViewById(R.id.txt_mobile_number);
        txt_country_code = findViewById(R.id.txt_country_code);
        txt_gender = findViewById(R.id.txt_gender);
        txt_select_category = findViewById(R.id.txt_select_category);
        txt_about_me = findViewById(R.id.txt_about_me);
        txt_educational_qualification = findViewById(R.id.txt_educational_qualification);
        txt_professional_qualification = findViewById(R.id.txt_professional_qualification);
        txt_about_my_professional = findViewById(R.id.txt_about_my_professional);
        txt_add_professional = findViewById(R.id.txt_add_professional);
        txt_add_edu = findViewById(R.id.txt_add_edu);
        txt_residential_address = findViewById(R.id.txt_residential_address);

        spinner_select_gender = findViewById(R.id.spinner_select_gender);
        spinner_select_category = findViewById(R.id.spinner_select_category);


        edt_firstname = findViewById(R.id.edt_firstname);
        edt_lastname = findViewById(R.id.edt_lastname);
        edt_email_address = findViewById(R.id.edt_email_address);
        edt_residential_address = findViewById(R.id.edt_residential_address);
        edt_mobile_number = findViewById(R.id.edt_mobile_number);
        edt_about_me_value = findViewById(R.id.edt_about_me_value);
        edt_about_my_professional_value = findViewById(R.id.edt_about_my_professional_value);

        rv_edu_qualification = findViewById(R.id.rv_edu_qualification);
        rv_professional_qualification = findViewById(R.id.rv_professional_qualification);

        LinearLayoutManager layoutManager = new LinearLayoutManager(EditProfileActivity.this);
        rv_edu_qualification.setLayoutManager(layoutManager);

        LinearLayoutManager manager = new LinearLayoutManager(EditProfileActivity.this);
        rv_professional_qualification.setLayoutManager(manager);


        app_bar_layout = findViewById(R.id.app_bar_layout);
        collapsing_toolbar = findViewById(R.id.collapsing_toolbar);
        toolbar = findViewById(R.id.toolbar);

        app_bar_layout.addOnOffsetChangedListener(new ProfileActivity.AppBarStateChangeListener() {
            @Override
            public void onStateChanged(AppBarLayout appBarLayout, State state) {
                Logger.d("STATE" + state.name());
                if (state == State.COLLAPSED) {
                    toolbar.setBackgroundColor(ContextCompat.getColor(EditProfileActivity.this, R.color.white));
                    img_back.setImageResource(R.drawable.back);
                    txt_edit_profile.setTextColor(ContextCompat.getColor(EditProfileActivity.this, R.color.color_333333));
                    txt_save.setTextColor(ContextCompat.getColor(EditProfileActivity.this, R.color.color_333333));

                } else {
                    toolbar.setBackgroundColor(Color.TRANSPARENT);
                    img_back.setImageResource(R.drawable.back_icon_wt);
                    txt_edit_profile.setTextColor(ContextCompat.getColor(EditProfileActivity.this, R.color.white));
                    txt_save.setTextColor(ContextCompat.getColor(EditProfileActivity.this, R.color.white));

                }
            }
        });

        metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams((metrics.widthPixels * 104) / 480, (metrics.heightPixels * 104) / 800);
        layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
        //layoutParams.setMargins(60,60,60,60);
        img_profile.setLayoutParams(layoutParams);

        RelativeLayout.LayoutParams layoutParams1 = new RelativeLayout.LayoutParams(metrics.widthPixels, (metrics.heightPixels * 280) / 800);
        img_cover.setLayoutParams(layoutParams1);


    }

    private void setData() {

        edt_firstname.setText(AppClass.preferences.getUserFirstName());
        edt_lastname.setText(AppClass.preferences.getUserLastName());
        edt_email_address.setText(AppClass.preferences.getEmailId());
        edt_about_me_value.setText(AppClass.preferences.getAboutMe());
        edt_about_my_professional_value.setText(AppClass.preferences.getAboutMyProfession());
        edt_mobile_number.setText(AppClass.preferences.getMobileNumber());
        edt_residential_address.setText(AppClass.preferences.getAddress());


        if (AppClass.preferences.getIsExpert().equalsIgnoreCase("0")) {
            spinner_select_category.setSelection(1);
        } else {
            spinner_select_category.setSelection(0);
        }

        ImageLoadUtils.imageLoad(EditProfileActivity.this,
                img_profile,
                AppClass.preferences.getProfileImage(),
                R.drawable.edit_profile_ph);


        ImageLoadUtils.imageLoad(EditProfileActivity.this,
                img_cover,
                AppClass.preferences.getCoverImage(),
                R.drawable.profile_bg);

        String[] countryCodeList = this.getResources().getStringArray(R.array.mobile_code);
        String[] countryCode = this.getResources().getStringArray(R.array.country_code);


        if (StringUtils.isNotEmpty(AppClass.preferences.getCountryCode())) {
            if (AppClass.preferences.getCountryCode().startsWith("+"))
                txt_country_code.setText(AppClass.preferences.getCountryCode());
            else
                txt_country_code.setText(String.format("+%s", AppClass.preferences.getCountryCode()));
        }

        String gender = "";
        if (AppClass.preferences.getGender().equals("1")) {
            gender = getResources().getString(R.string.male);
        } else {
            gender = getResources().getString(R.string.female);
        }
        spinner_select_gender.setSelection(spinnerGenderArrayAdapter.getPosition(gender));


        setEducationalQualificationData();

        setProfessionalQualificationData();

        if (!edt_email_address.getText().toString().trim().isEmpty())
            edt_email_address.setEnabled(false);
        else
            edt_email_address.setEnabled(true);


    }

    private void setGenderData() {

        spinnerGenderArray = new ArrayList<>();
        spinnerGenderArray.add(getResources().getString(R.string.male));
        spinnerGenderArray.add(getResources().getString(R.string.female));
        spinnerGenderArrayAdapter = new ArrayAdapter<String>
                (this, android.R.layout.simple_spinner_item, spinnerGenderArray) {
            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {

                TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                tv.setTypeface(AppClass.lato_regular);
                tv.setTextSize(12);
                return tv;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                TextView tv = (TextView) super.getView(position, convertView, parent);
                tv.setTypeface(AppClass.lato_regular);
                tv.setTextSize(12);
                return tv;
            }
        };

        spinnerGenderArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_select_gender.setAdapter(spinnerGenderArrayAdapter);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            spinner_select_gender.setPopupBackgroundResource(R.color.white);
        }

        spinner_select_gender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if (spinnerGenderArray.get(adapterView.getSelectedItemPosition()).equals(getResources().getString(R.string.male))) {
                    gender = "1";
                } else {
                    gender = "2";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void initStyle() {

        txt_edit_profile.setTypeface(AppClass.lato_regular);
        txt_save.setTypeface(AppClass.lato_regular);
        txt_edit_cover.setTypeface(AppClass.lato_regular);
        txt_personal_info.setTypeface(AppClass.lato_black);
        txt_firstname.setTypeface(AppClass.lato_regular);
        txt_lastname.setTypeface(AppClass.lato_regular);
        txt_email.setTypeface(AppClass.lato_regular);
        txt_mobile_number.setTypeface(AppClass.lato_regular);
        txt_country_code.setTypeface(AppClass.lato_regular);
        txt_residential_address.setTypeface(AppClass.lato_regular);
        txt_gender.setTypeface(AppClass.lato_regular);
        txt_select_category.setTypeface(AppClass.lato_regular);
        txt_about_me.setTypeface(AppClass.lato_regular);
        txt_educational_qualification.setTypeface(AppClass.lato_black);
        txt_professional_qualification.setTypeface(AppClass.lato_black);
        txt_about_my_professional.setTypeface(AppClass.lato_black);
        txt_add_professional.setTypeface(AppClass.lato_regular);
        txt_add_edu.setTypeface(AppClass.lato_regular);

        edt_firstname.setTypeface(AppClass.lato_regular);
        edt_lastname.setTypeface(AppClass.lato_regular);
        edt_email_address.setTypeface(AppClass.lato_regular);
        edt_residential_address.setTypeface(AppClass.lato_regular);
        edt_mobile_number.setTypeface(AppClass.lato_regular);
        edt_about_me_value.setTypeface(AppClass.lato_regular);
        edt_about_my_professional_value.setTypeface(AppClass.lato_regular);
    }

    private void initListener() {

        img_cover.setOnClickListener(this);
        img_back.setOnClickListener(this);
        img_profile.setOnClickListener(this);
        txt_save.setOnClickListener(this);
        img_dropdown_gender.setOnClickListener(this);
        img_dropdown_category.setOnClickListener(this);
        txt_add_edu.setOnClickListener(this);
        txt_add_professional.setOnClickListener(this);
        txt_country_code.setOnClickListener(this);
    }

    private void setEducationalQualificationData() {
        yearList = new ArrayList<>();

        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
        int currentYear = calendar.get(Calendar.YEAR);

        for (int i = 0; i < 100; i++) {

            yearList.add(String.valueOf(currentYear));
            currentYear--;
        }

        if (qualificationArrayList.size() == 0) {
            qualificationArrayList = new ArrayList<>();
            EducationalQualificationModel model = new EducationalQualificationModel();
            model.setEducational_qualification("");
            model.setPassing_year("");
            qualificationArrayList.add(0, model);
        }
        eduQualificationAdapter = new EduQualificationAdapter(this, yearList);
        rv_edu_qualification.setAdapter(eduQualificationAdapter);
    }

    private void setProfessionalQualificationData() {

        experience_year_list = new ArrayList<>();
        experience_year_list.add(getResources().getString(R.string.one_year));
        experience_year_list.add(getResources().getString(R.string.two_year));
        experience_year_list.add(getResources().getString(R.string.three_year));
        experience_year_list.add(getResources().getString(R.string.four_year));
        experience_year_list.add(getResources().getString(R.string.more_than_four_year));

        if (professionalQuaArrayList.size() == 0) {
            professionalQuaArrayList = new ArrayList<>();
            ProfessionalQualificationModel model = new ProfessionalQualificationModel();
            model.setJob_title("");
            model.setExperience("");
            professionalQuaArrayList.add(0, model);
        }

        professionalQualificationAdapter = new ProfessionalQualificationAdapter(this, experience_year_list);
        rv_professional_qualification.setAdapter(professionalQualificationAdapter);
    }

    private boolean validation() {
        boolean isValid = true;

        if (edt_firstname.getText().toString().isEmpty()) {
            isValid = false;
            AppClass.snackBarView.snackBarShow(this, getResources().getString(R.string.vfirstname));
        } else if (edt_lastname.getText().toString().isEmpty()) {
            isValid = false;
            AppClass.snackBarView.snackBarShow(this, getResources().getString(R.string.vlastname));
        } else if (edt_email_address.getText().toString().isEmpty()) {
            isValid = false;
            AppClass.snackBarView.snackBarShow(this, getResources().getString(R.string.vemail));
        }/* else if (edt_mobile_number.getText().toString().isEmpty()) {
            isValid = false;
            AppClass.snackBarView.snackBarShow(this, getResources().getString(R.string.vmobile_number));
        } else if (phone_code.isEmpty()) {
            isValid = false;
            AppClass.snackBarView.snackBarShow(this, getResources().getString(R.string.vcountrycode));
        }*/

        return isValid;
    }

    private void setSpinnerCategoryList() {

        if (AppClass.isInternetConnectionAvailable()) {

            ArrayList<String> category_name_list = new ArrayList<>();
            category_name_list.add("Yes");
            category_name_list.add("No");

            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>
                    (EditProfileActivity.this,
                            android.R.layout.simple_spinner_item, category_name_list) {
                @Override
                public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    tv.setTypeface(AppClass.lato_regular);
                    tv.setTextSize(12);
                    return tv;
                }

                @NonNull
                @Override
                public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

                    TextView tv = (TextView) super.getView(position, convertView, parent);
                    tv.setTypeface(AppClass.lato_regular);
                    tv.setTextSize(12);
                    return tv;
                }
            };

            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner_select_category.setAdapter(spinnerArrayAdapter);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                spinner_select_category.setPopupBackgroundResource(R.color.white);
            }

            spinner_select_category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
                    if (pos == 0) {
                        if (isExpert.equalsIgnoreCase("0")) {
                            startActivityForResult(new Intent(EditProfileActivity.this, WebviewActivity.class),
                                    PAYMENT_REQUEST);
                            spinner_select_category.setSelection(1);
                        }


                    }

                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            spinner_select_category.setSelection(1);


        } else {
            AppClass.snackBarView.snackBarShow(EditProfileActivity.this, getString(R.string.nonetwork));
        }
    }

    private void callAPIGetProfile() {
        if (AppClass.isInternetConnectionAvailable()) {

            try {

                HashMap<String, String> request = new HashMap<>();
                request.put("user_id", AppClass.preferences.getUserId());
                request.put("profile_id", AppClass.preferences.getUserId());
                Logger.d("request:" + request);

                ApiCall.getInstance().getProfile(this, request, new IApiCallback() {
                    @Override
                    public void onSuccess(Object data) {
                        try {
                            JsonObject response = (JsonObject) data;

                            AppClass.preferences.storeUserId(response.get("user_id").getAsString());
                            AppClass.preferences.storeFirstName(response.get("firstname").getAsString());
                            AppClass.preferences.storeUSerLastName(response.get("lastname").getAsString());
                            AppClass.preferences.storeEmailId(response.get("email").getAsString());
                            AppClass.preferences.storeAddress(response.get("address").getAsString());
                            AppClass.preferences.storeGender(response.get("gender").getAsString());
                            AppClass.preferences.storeMobileNumber(response.get("phone").getAsString());
                            AppClass.preferences.storeCountryCode(response.get("phone_code").getAsString());
                            AppClass.preferences.storeAboutMe(response.get("about_me").getAsString());
                            AppClass.preferences.storeAboutMyProfession(response.get("about_my_profession").getAsString());
                            AppClass.preferences.storeProfileImage(response.get("profile_image").getAsString());
                            AppClass.preferences.storeCoverImage(response.get("cover_image").getAsString());
                            AppClass.preferences.storeIsExpert(response.get("is_expert").getAsString());

                            isExpert = response.get("is_expert").getAsString();

                            JsonArray educational_qualification = response.getAsJsonArray("educational_qualification");

                            if (educational_qualification.size() > 0) {

                                qualificationArrayList = new ArrayList<>();
                                for (int i = 0; i < educational_qualification.size(); i++) {

                                    EducationalQualificationModel model = new EducationalQualificationModel();
                                    model.setEducational_qualification(educational_qualification.get(i).getAsJsonObject().get("qualification").getAsString());
                                    model.setPassing_year(educational_qualification.get(i).getAsJsonObject().get("year").getAsString());
                                    qualificationArrayList.add(model);
                                }
                            }

                            JsonArray professional_qualification = response.getAsJsonArray("professional_qualification");

                            if (professional_qualification.size() > 0) {

                                professionalQuaArrayList = new ArrayList<>();
                                for (int i = 0; i < professional_qualification.size(); i++) {

                                    ProfessionalQualificationModel model = new ProfessionalQualificationModel();
                                    model.setJob_title(professional_qualification.get(i).getAsJsonObject().get("job_title").getAsString());
                                    model.setExperience(professional_qualification.get(i).getAsJsonObject().get("experience").getAsString());
                                    model.setIsCurrentJob(professional_qualification.get(i).getAsJsonObject().get("is_current_job").getAsString());
                                    professionalQuaArrayList.add(model);
                                }
                            }

                            phone_code = response.get("phone_code").getAsString();

                            setData();
                            Logger.e("UserData " + response.get("firstname").getAsString());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(boolean isFalseFromService, String errorMessage) {
                        if (StringUtils.isNotEmpty(errorMessage)) {
                            DialogUtils.openDialog(EditProfileActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                                @Override
                                public void onNegativeBtnClicked(DialogInterface alrt) {
                                    alrt.dismiss();
                                }

                                @Override
                                public void onPositiveBtnClicked(DialogInterface alrt) {
                                    alrt.dismiss();
                                }
                            });
                        }
                    }
                }, true);

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            AppClass.snackBarView.snackBarShow(EditProfileActivity.this, getString(R.string.nonetwork));
        }

    }

    private void callAPIUpdateProfile() {

        try {


            RequestBody rbUserId = RequestBody.create(MediaType.parse("multipart/form-data"), AppClass.preferences.getUserId());
            RequestBody rbFirstName = RequestBody.create(MediaType.parse("multipart/form-data"), edt_firstname.getText().toString().trim());
            RequestBody rbLastName = RequestBody.create(MediaType.parse("multipart/form-data"), edt_lastname.getText().toString().trim());
            RequestBody rbEmailAddress = RequestBody.create(MediaType.parse("multipart/form-data"), edt_email_address.getText().toString().trim());
            RequestBody rbMobileNumber = RequestBody.create(MediaType.parse("multipart/form-data"), edt_mobile_number.getText().toString().trim());
            RequestBody rbPhoneCode = RequestBody.create(MediaType.parse("multipart/form-data"), phone_code.trim());
            RequestBody rbGender = RequestBody.create(MediaType.parse("multipart/form-data"), gender.trim());
            RequestBody rbAddress = RequestBody.create(MediaType.parse("multipart/form-data"), edt_residential_address.getText().toString().trim());
            RequestBody rbAboutMe = RequestBody.create(MediaType.parse("multipart/form-data"), edt_about_me_value.getText().toString().trim());
            RequestBody rbAboutMyProfession = RequestBody.create(MediaType.parse("multipart/form-data"), edt_about_my_professional_value.getText().toString().trim());
            RequestBody rbIsExpert = RequestBody.create(MediaType.parse("multipart/form-data"), spinner_select_category.getSelectedItemPosition() == 0 ? "1" : "0");

            HashMap<String, RequestBody> request = new HashMap<>();
            request.put("user_id", rbUserId);
            request.put("firstname", rbFirstName);
            request.put("lastname", rbLastName);
            request.put("email", rbEmailAddress);
            request.put("phone", rbMobileNumber);
            request.put("phone_code", rbPhoneCode);
            request.put("gender", rbGender);
            request.put("address", rbAddress);
            request.put("about_me", rbAboutMe);
            request.put("about_my_profession", rbAboutMyProfession);
            request.put("is_expert", rbIsExpert);

            JsonArray edu_array = new JsonArray();

            Logger.d("qualificationArrayList" + qualificationArrayList.size());
            if (qualificationArrayList.size() > 0) {
                for (int i = 0; i < qualificationArrayList.size(); i++) {
                    try {
                        JsonObject jsonObject = new JsonObject();

                        if (!qualificationArrayList.get(i).getEducational_qualification().isEmpty()) {
                            jsonObject.add("edu_qua", new JsonPrimitive(qualificationArrayList.get(i).getEducational_qualification()));
                            jsonObject.add("year", new JsonPrimitive(qualificationArrayList.get(i).getPassing_year()));

                            edu_array.add(jsonObject);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                Logger.d("array ==>" + edu_array);

                RequestBody rbEduArray = null;
                if (edu_array.size() > 0) {
                    rbEduArray = RequestBody.create(MediaType.parse("multipart/form-data"), edu_array.toString());
                    request.put("education", rbEduArray);
                }


            }

            JsonArray profession_array = new JsonArray();

            Logger.d("professionalQuaArrayList" + professionalQuaArrayList.size());
            if (professionalQuaArrayList.size() > 0) {
                for (int i = 0; i < professionalQuaArrayList.size(); i++) {
                    try {
                        JsonObject jsonObject = new JsonObject();

                        if (!professionalQuaArrayList.get(i).getJob_title().isEmpty()) {
                            jsonObject.add("job_title", new JsonPrimitive(professionalQuaArrayList.get(i).getJob_title()));
                            jsonObject.add("experience", new JsonPrimitive(professionalQuaArrayList.get(i).getExperience()));
                            jsonObject.add("is_current_job", new JsonPrimitive(professionalQuaArrayList.get(i).getIsCurrentJob()));
                            profession_array.add(jsonObject);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                Logger.d("array ==>" + profession_array);


                RequestBody rbProfessionArray = null;

                if (profession_array.size() > 0) {
                    rbProfessionArray = RequestBody.create(MediaType.parse("multipart/form-data"), profession_array.toString());
                    request.put("profession", rbProfessionArray);
                }
            }

            String profileImagePath = "", coverImagePath = "";
            if (file_profile != null && file_profile.length() > 0) {
                profileImagePath = file_profile.getAbsolutePath();
            }

            if (file_cover != null && file_cover.length() > 0) {
                coverImagePath = file_cover.getAbsolutePath();
            }

            ApiCall.getInstance().updateUserProfile(EditProfileActivity.this, request, profileImagePath, coverImagePath, new IApiCallback() {
                @Override
                public void onSuccess(Object data) {

                    if (AppClass.preferences.getScreeCode().equalsIgnoreCase(ScreenCode.ChangeProfile)) {
                        AppClass.preferences.storeScreeCode(ScreenCode.NormalScreen);
                        finish();
                        startActivity(new Intent(EditProfileActivity.this, HomeActivity.class));
                    } else {
                        finish();
                    }

                }

                @Override
                public void onFailure(boolean isFalseFromService, String errorMessage) {
                    if (StringUtils.isNotEmpty(errorMessage)) {
                        DialogUtils.openDialog(EditProfileActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                            @Override
                            public void onNegativeBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }

                            @Override
                            public void onPositiveBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }
                        });
                    }
                }
            }, true);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && data != null) {
            if (requestCode == PAYMENT_REQUEST) {
                if (data.getStringExtra("isPaymentSuccess").equalsIgnoreCase("1")) {
                    isExpert = "1";
                    spinner_select_category.setSelection(0);
                    spinner_select_category.setEnabled(false);
                } else {
                    spinner_select_category.setSelection(1);
                }


            }


        }


    }

    class DialogCountryCode extends Dialog {

        private Activity activity;

        private EditText et_search_country;
        private RecyclerView recyclerview_country;

        private DialogCountryCode(Activity a) {
            super(a);
            this.activity = a;
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.dialog_country_code);
            getWindow().setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.MATCH_PARENT);

            WindowManager.LayoutParams params = this.getWindow().getAttributes();
            //params.x = -100;
            params.y = -100;
            this.getWindow().setAttributes(params);


            et_search_country = findViewById(R.id.et_search_country);
            et_search_country.setTypeface(AppClass.lato_regular);

            recyclerview_country = findViewById(R.id.recyclerview_country);
            LinearLayoutManager layoutManager = new LinearLayoutManager(activity);
            recyclerview_country.setLayoutManager(layoutManager);

            final ArrayList<CountryModel> countryData;
            final String[] countryNameList = getResources().getStringArray(R.array.country);
            String[] countryCodeList = getResources().getStringArray(R.array.mobile_code);
            String[] countryCode = getResources().getStringArray(R.array.country_code);

            countryData = new ArrayList<>();

            for (int i = 0; i < countryCodeList.length; i++) {
                CountryModel model = new CountryModel();
                model.setCode(countryCodeList[i]);
                model.setName(countryNameList[i]);
                model.setCountry_code(countryCode[i]);
                countryData.add(model);
            }

            final CountryAdapter countryAdapter = new CountryAdapter(activity, countryData, new CountryAdapter.CountrySelection() {
                @Override
                public void onCountrySelection(String countryCode, String countryName, String code) {

                    dismiss();
                    phone_code = countryCode;
                    txt_country_code.setText("+" + countryCode);
                }
            });
            recyclerview_country.setAdapter(countryAdapter);

            et_search_country.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    countryAdapter.getFilter().filter(s.toString());
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });


        }


    }
}
