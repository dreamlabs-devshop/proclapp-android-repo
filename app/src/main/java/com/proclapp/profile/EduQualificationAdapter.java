package com.proclapp.profile;

import android.app.Activity;
import android.os.Build;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.proclapp.AppClass;
import com.proclapp.R;

import java.util.ArrayList;

/**
 */

public class EduQualificationAdapter extends RecyclerView.Adapter<EduQualificationAdapter.MyViewHolder> {

    private Activity activity;
    private ArrayList<String> yearList;
    private ArrayAdapter<String> spinnerArrayAdapter;

    EduQualificationAdapter(Activity activity, ArrayList<String> yearList) {
        this.activity = activity;
        this.yearList = yearList;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_edu_qualification, parent, false);
        return new MyViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        if (EditProfileActivity.qualificationArrayList.size() > 1) {
            holder.img_cross.setVisibility(View.VISIBLE);
        } else {
            holder.img_cross.setVisibility(View.GONE);
        }

        holder.edt_edu_qualification.setText(EditProfileActivity.qualificationArrayList.get(position).getEducational_qualification());
        holder.spinner_select_passing_year.setSelection(spinnerArrayAdapter.getPosition(EditProfileActivity.qualificationArrayList.get(position).getPassing_year()));

        holder.edt_edu_qualification.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                EditProfileActivity.qualificationArrayList.get(position).setEducational_qualification(s.toString());
            }
        });


        holder.img_dropdown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.spinner_select_passing_year.performClick();
            }
        });

        holder.img_cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (EditProfileActivity.qualificationArrayList.size() > 1) {
                    EditProfileActivity.qualificationArrayList.remove(holder.getAdapterPosition());
                    notifyItemRemoved(holder.getAdapterPosition());

                    if (EditProfileActivity.qualificationArrayList.size() == 1) {
                        holder.img_cross.setVisibility(View.GONE);
                        notifyDataSetChanged();
                    }
                }
            }
        });

        holder.spinner_select_passing_year.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                EditProfileActivity.qualificationArrayList.get(position).setPassing_year(yearList.get(adapterView.getSelectedItemPosition()));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    @Override
    public int getItemCount() {
        return EditProfileActivity.qualificationArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txt_edu_qualification,
                txt_passing_year;
        EditText edt_edu_qualification;
        Spinner spinner_select_passing_year;
        ImageView img_dropdown,
                img_cross;

        public MyViewHolder(View itemView) {
            super(itemView);
            txt_edu_qualification = itemView.findViewById(R.id.txt_edu_qualification);
            txt_passing_year = itemView.findViewById(R.id.txt_passing_year);
            edt_edu_qualification = itemView.findViewById(R.id.edt_edu_qualification);
            img_dropdown = itemView.findViewById(R.id.img_dropdown);
            img_cross = itemView.findViewById(R.id.img_cross);
            spinner_select_passing_year = itemView.findViewById(R.id.spinner_select_passing_year);

            txt_edu_qualification.setTypeface(AppClass.lato_regular);
            txt_passing_year.setTypeface(AppClass.lato_regular);
            edt_edu_qualification.setTypeface(AppClass.lato_regular);

            // set passing year spinner data

            spinnerArrayAdapter = new ArrayAdapter<String>
                    (activity, android.R.layout.simple_spinner_item, yearList) {
                @Override
                public View getDropDownView(int position, View convertView, ViewGroup parent) {

                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    tv.setTypeface(AppClass.lato_regular);
                    tv.setTextSize(12);
                    return tv;
                }

                @Override
                public View getView(int position, View convertView, ViewGroup parent) {

                    TextView tv = (TextView) super.getView(position, convertView, parent);
                    tv.setTypeface(AppClass.lato_regular);
                    tv.setTextSize(12);
                    return tv;
                }


            };

            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner_select_passing_year.setAdapter(spinnerArrayAdapter);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                spinner_select_passing_year.setPopupBackgroundResource(R.color.white);
            }

        }
    }
}
