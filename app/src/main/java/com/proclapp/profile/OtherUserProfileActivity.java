package com.proclapp.profile;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.chat.ChatActivity;
import com.proclapp.home.adapter.AllPostsAdapter;
import com.proclapp.interfaces.RecyclerClickListener;
import com.proclapp.model.AllPostsModel;
import com.proclapp.model.CategoryModel;
import com.proclapp.model.FollowerFollowingModel;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.AlertDialogInterface;
import com.proclapp.utils.DialogUtils;
import com.proclapp.utils.ImageLoadUtils;
import com.proclapp.utils.Logger;
import com.proclapp.utils.StaticData;
import com.proclapp.utils.StringUtils;
import com.zhy.view.flowlayout.FlowLayout;
import com.zhy.view.flowlayout.TagAdapter;
import com.zhy.view.flowlayout.TagFlowLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class OtherUserProfileActivity extends AppCompatActivity implements View.OnClickListener {

    private RelativeLayout rv_friend;
    private RelativeLayout rl_progress_post;
    private TagFlowLayout tagflow_interest_category;
    private String otherUserId;
    private RecyclerView rv_user_post;
    private CollapsingToolbarLayout collapsing_toolbar;
    private AppBarLayout app_bar_layout;
    private Toolbar toolbar;
    private NestedScrollView nested;
    private ImageView img_back, img_cover, iv_chat;
    private TextView
            txt_followers,
            txt_followers_count,
            txt_following,
            txt_following_count,
            txt_user_name,
            txt_expert,
            txt_user_designation,
            txt_follow,
            txt_about_me,
            txt_about_my_profession,
            txt_about_my_profession_value,
            txt_app_joining,
            txt_app_joining_value,
            txt_profile,
            txt_categories,
            txt_add_friend,
            tv_accept_request,
            tv_cancle_request,
            txt_about_education_value,
            txt_user_location,
            tv_readmore_about_me,
            tv_readmore_about_profession,
            tv_show_more,
            txt_friend;
    private CircleImageView img_user_profile;
    private ProfileFriendAdapter profileFriendAdapter;
    private boolean isAllCategories = false;

    private LinearLayout ll_friend_request;

    private ArrayList<AllPostsModel> allPostsLists = new ArrayList<>();
    private AllPostsAdapter allPostsAdapter;

    private RelativeLayout rl_progress_bar;

    private String userName = "", userImage = "";
    private int offset = 0;
    private boolean
            isRecyclerViewWaitingtoLoadData = false,
            loadedAllItems = false;
    private LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_other_user_profile);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        initView();
        initStyle();
        initListener();

        otherUserId = getIntent().getStringExtra("user_id");
    }

    @Override
    protected void onResume() {
        super.onResume();
        callAPIGetOtherUserProfile();
    }

    private void initView() {

        img_back = findViewById(R.id.img_back);
        img_cover = findViewById(R.id.img_cover);
        txt_followers = findViewById(R.id.txt_followers);
        txt_followers_count = findViewById(R.id.txt_followers_count);
        txt_following = findViewById(R.id.txt_following);
        txt_following_count = findViewById(R.id.txt_following_count);
        txt_user_name = findViewById(R.id.txt_user_name);
        txt_expert = findViewById(R.id.txt_expert);
        txt_user_designation = findViewById(R.id.txt_user_designation);
        txt_follow = findViewById(R.id.txt_follow);
        txt_about_me = findViewById(R.id.txt_about_me);
        txt_about_my_profession = findViewById(R.id.txt_about_my_profession);
        txt_about_my_profession_value = findViewById(R.id.txt_about_my_profession_value);
        txt_app_joining = findViewById(R.id.txt_app_joining);
        txt_app_joining_value = findViewById(R.id.txt_app_joining_value);
        txt_profile = findViewById(R.id.txt_profile);
        txt_friend = findViewById(R.id.txt_friend);
        tv_accept_request = findViewById(R.id.tv_accept_request);
        tv_cancle_request = findViewById(R.id.tv_cancle_request);
        txt_user_location = findViewById(R.id.txt_user_location);
        img_user_profile = findViewById(R.id.img_user_profile);
        txt_categories = findViewById(R.id.txt_categories);
        tagflow_interest_category = findViewById(R.id.tagflow_interest_category);
        txt_add_friend = findViewById(R.id.txt_add_friend);
        txt_about_education_value = findViewById(R.id.txt_about_education_value);
        iv_chat = findViewById(R.id.iv_chat);
        ll_friend_request = findViewById(R.id.ll_friend_request);
        tv_readmore_about_me = findViewById(R.id.tv_readmore_about_me);
        tv_readmore_about_profession = findViewById(R.id.tv_readmore_about_profession);
        tv_show_more = findViewById(R.id.tv_show_more);
        nested = findViewById(R.id.nested);

        rv_user_post = findViewById(R.id.rv_user_post);
        rv_friend = findViewById(R.id.rv_friend);
        toolbar = findViewById(R.id.toolbar);
        rl_progress_bar = findViewById(R.id.rl_progress_bar);
        rl_progress_post = findViewById(R.id.rl_progress_post);

        layoutManager = new LinearLayoutManager(this);
        rv_user_post.setLayoutManager(layoutManager);


        /*LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rv_friend.setLayoutManager(linearLayoutManager);*/

        app_bar_layout = findViewById(R.id.app_bar_layout);
        collapsing_toolbar = findViewById(R.id.collapsing_toolbar);

        /*profileFriendAdapter = new ProfileFriendAdapter(OtherUserProfileActivity.this);
        rv_friend.setAdapter(profileFriendAdapter);*/


        app_bar_layout.addOnOffsetChangedListener(new AppBarStateChangeListener() {
            @Override
            public void onStateChanged(AppBarLayout appBarLayout, State state) {
                Logger.d("STATE" + state.name());
                if (state == State.COLLAPSED) {
                    toolbar.setBackgroundColor(ContextCompat.getColor(OtherUserProfileActivity.this, R.color.white));
                    txt_profile.setVisibility(View.VISIBLE);
                    img_back.setImageResource(R.drawable.back);
                } else {
                    toolbar.setBackgroundColor(Color.TRANSPARENT);
                    txt_profile.setVisibility(View.GONE);
                    img_back.setImageResource(R.drawable.back_icon_wt);
                }
            }
        });

    }

    private void initStyle() {
        txt_followers.setTypeface(AppClass.lato_semibold);
        txt_followers_count.setTypeface(AppClass.lato_black);
        txt_following.setTypeface(AppClass.lato_semibold);
        txt_following_count.setTypeface(AppClass.lato_black);
        txt_user_name.setTypeface(AppClass.lato_heavy);
        txt_expert.setTypeface(AppClass.lato_regular);
        txt_user_designation.setTypeface(AppClass.lato_regular);
        txt_follow.setTypeface(AppClass.lato_semibold);
        txt_add_friend.setTypeface(AppClass.lato_semibold);
        txt_about_me.setTypeface(AppClass.lato_medium);
        txt_about_my_profession.setTypeface(AppClass.lato_black);
        txt_about_my_profession_value.setTypeface(AppClass.lato_medium);
        txt_app_joining.setTypeface(AppClass.lato_black);
        txt_app_joining_value.setTypeface(AppClass.lato_light);
        txt_profile.setTypeface(AppClass.lato_regular);
        txt_friend.setTypeface(AppClass.lato_regular);
        tv_show_more.setTypeface(AppClass.lato_regular);
        txt_categories.setTypeface(AppClass.lato_black);
    }

    private void initListener() {
        img_back.setOnClickListener(this);
        txt_follow.setOnClickListener(this);
        iv_chat.setOnClickListener(this);
        txt_add_friend.setOnClickListener(this);
        tv_accept_request.setOnClickListener(this);
        tv_cancle_request.setOnClickListener(this);
        tv_readmore_about_me.setOnClickListener(this);
        tv_readmore_about_profession.setOnClickListener(this);
        tv_show_more.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.img_back:
                onBackPressed();
                break;

            case R.id.iv_chat:
                startActivity(new Intent(this, ChatActivity.class)
                        .putExtra("friend_id", otherUserId)
                        .putExtra("profileimage", userImage)
                        .putExtra("name", userName));

                break;

            case R.id.txt_add_friend:
                callAPIfriendUnfriend(txt_add_friend.getText().toString().trim().equalsIgnoreCase("Add Friend") ? "1" : "0");
                break;

            case R.id.txt_follow:
                callAPIChangeFollowUnfollowStatus(txt_follow.getText().toString().trim().equalsIgnoreCase("follow") ? "1" : "0");
                break;

            case R.id.tv_accept_request:
                acceptRejectFriendRequest("1");
                break;

            case R.id.tv_cancle_request:
                acceptRejectFriendRequest("2");
                break;

            case R.id.tv_readmore_about_me:
                if (txt_about_me.getMaxLines() == 3) {
                    tv_readmore_about_me.setText(getString(R.string.read_less));
                    txt_about_me.setMaxLines(Integer.MAX_VALUE);
                } else {
                    tv_readmore_about_me.setText(R.string.read_more);
                    txt_about_me.setMaxLines(3);
                }
                break;

            case R.id.tv_readmore_about_profession:
                if (txt_about_my_profession_value.getMaxLines() == 4) {
                    tv_readmore_about_profession.setText(getString(R.string.read_less));
                    txt_about_my_profession_value.setMaxLines(Integer.MAX_VALUE);
                } else {
                    tv_readmore_about_profession.setText(R.string.read_more);
                    txt_about_my_profession_value.setMaxLines(4);
                }
                break;

            case R.id.tv_show_more:
                if (tv_show_more.getText().toString().equalsIgnoreCase(getString(R.string.show_more))) {
                    isAllCategories = true;
                    tagflow_interest_category.getAdapter().notifyDataChanged();
                    tv_show_more.setText(getString(R.string.show_less));
                } else {
                    isAllCategories = false;
                    tagflow_interest_category.getAdapter().notifyDataChanged();
                    tv_show_more.setText(getString(R.string.show_more));
                }
                break;


        }


    }

    private void callAPIGetOtherUserProfile() {

        if (AppClass.isInternetConnectionAvailable()) {

            rl_progress_bar.setVisibility(View.VISIBLE);
            rl_progress_post.setVisibility(View.VISIBLE);

            HashMap<String, String> request = new HashMap<>();
            request.put("user_id", AppClass.preferences.getUserId());
            request.put("follower_id", otherUserId);


            ApiCall.getInstance().getOtherUserProfile(OtherUserProfileActivity.this, request, new IApiCallback() {
                @Override
                public void onSuccess(Object data) {
                    JsonObject response = (JsonObject) data;
                    setData(response);
                    setupRecyclerView();
                }

                @Override
                public void onFailure(boolean isFalseFromService, String errorMessage) {

                    if (StringUtils.isNotEmpty(errorMessage)) {
                        DialogUtils.openDialog(OtherUserProfileActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                            @Override
                            public void onNegativeBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }

                            @Override
                            public void onPositiveBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }
                        });
                    }

                }
            }, false);

        } else {
            AppClass.snackBarView.snackBarShow(OtherUserProfileActivity.this, getString(R.string.nonetwork));
        }
    }


    private void setupRecyclerView() {

        allPostsAdapter = new AllPostsAdapter(OtherUserProfileActivity.this, allPostsLists, new RecyclerClickListener() {
            @Override
            public void onItemClick(int pos, String tag) {

            }
        });
        rv_user_post.setAdapter(allPostsAdapter);

        getUserPosts();

    }

    private void getUserPosts() {
        if (AppClass.isInternetConnectionAvailable()) {


            if (offset != 0) {
                rl_progress_post.setVisibility(View.VISIBLE);
            }

            HashMap<String, String> request = new HashMap<>();
            request.put("user_id", AppClass.preferences.getUserId());
            request.put("other_person_id", otherUserId);
            request.put("offset", String.valueOf(allPostsLists.size()));


            ApiCall.getInstance().getUserPosts(this, request, new IApiCallback() {
                @Override
                public void onSuccess(Object data) {

                    isRecyclerViewWaitingtoLoadData = false;
                    loadedAllItems = true;

                    if (allPostsLists.size() == 0)
                        allPostsLists.clear();

                    allPostsLists.addAll((ArrayList<AllPostsModel>) data);
                    rv_user_post.getAdapter().notifyDataSetChanged();


                }

                @Override
                public void onFailure(boolean isFalseFromService, String errorMessage) {
                    isRecyclerViewWaitingtoLoadData = true;
                    loadedAllItems = true;
                    if (StringUtils.isNotEmpty(errorMessage)) {

                        if (errorMessage.equalsIgnoreCase("no data found")) {
                            if (allPostsLists.size() == 0) {

                            }
                        } else {

                            DialogUtils.openDialog(OtherUserProfileActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                                @Override
                                public void onNegativeBtnClicked(DialogInterface alrt) {
                                    alrt.dismiss();
                                }

                                @Override
                                public void onPositiveBtnClicked(DialogInterface alrt) {
                                    alrt.dismiss();
                                }
                            });
                        }
                    }
                    rl_progress_post.setVisibility(View.GONE);
                }
            }, false);


        } else {
            AppClass.snackBarView.snackBarShow(OtherUserProfileActivity.this, getString(R.string.nonetwork));
        }

    }


    private void setData(JsonObject response) {

        txt_user_name.setText(String.format("%s %s", response.get("firstname").getAsString(), response.get("lastname").getAsString()));
        txt_about_me.setText(response.get("about_me").getAsString());
        txt_about_my_profession_value.setText(response.get("about_my_profession").getAsString());

        if (response.getAsJsonArray("educational_qualification").size() > 0)
            txt_about_education_value.setText(response.getAsJsonArray("educational_qualification").get(0).getAsJsonObject().get("qualification").getAsString());

        txt_following_count.setText(response.get("total_following").getAsString());
        txt_followers_count.setText(response.get("followers").getAsString());
        txt_user_location.setText(response.get("address").getAsString());

        if (txt_about_me.getLineCount() > 3) {
            tv_readmore_about_me.setVisibility(View.VISIBLE);
        } else {
            tv_readmore_about_me.setVisibility(View.GONE);
        }

        if (txt_about_my_profession_value.getLineCount() > 3) {
            tv_readmore_about_profession.setVisibility(View.VISIBLE);
        } else {
            tv_readmore_about_profession.setVisibility(View.GONE);
        }

        txt_user_designation.setText(response.get("professional_qualification").getAsString());

        userName = response.get("firstname").getAsString() + " " + response.get("lastname").getAsString();
        userImage = response.get("profile_image").getAsString();

        if (response.get("is_friend").getAsString().equalsIgnoreCase("true")) {
            txt_add_friend.setText(getString(R.string.unfriend));
        } else {
            txt_add_friend.setText(getString(R.string.add_friend));
        }

        if (response.has("request_status")) {

            if (response.get("request_status").getAsString().equalsIgnoreCase("0http://180.211.113.98:8080/clean_my_ride/ws/v1/api_sp/")) {

                if (response.get("sender_id").getAsString().equalsIgnoreCase(AppClass.preferences.getUserId())) {
                    ll_friend_request.setVisibility(View.GONE);
                    txt_add_friend.setText(R.string.requested);
                } else {
                    ll_friend_request.setVisibility(View.VISIBLE);
                    txt_add_friend.setVisibility(View.GONE);
                }
            } else {
                ll_friend_request.setVisibility(View.GONE);
            }

        }

        if (AppClass.preferences.getIsExpert().equalsIgnoreCase("0")) {
            txt_expert.setVisibility(View.GONE);
        } else {
            txt_expert.setVisibility(View.VISIBLE);
        }

        if (response.get("is_following").getAsString().equalsIgnoreCase("true")) {
            txt_follow.setText("Unfollow");
        } else {
            txt_follow.setText("Follow");
        }


        txt_app_joining_value.setText(StaticData.formatDate("yyyy-MM-dd HH:mm:ss", "MMM. dd yyyy", AppClass.preferences.getMemberSince()));

        ImageLoadUtils.imageLoad(OtherUserProfileActivity.this,
                img_user_profile,
                response.get("profile_image").getAsString(),
                R.drawable.profile_pic_ph);

        ImageLoadUtils.imageLoad(OtherUserProfileActivity.this,
                img_cover,
                response.get("cover_image").getAsString());


        ArrayList<CategoryModel> tempCategoryList = new Gson().fromJson(response.getAsJsonArray("interest_categories").toString(),
                new TypeToken<List<CategoryModel>>() {
                }.getType());

        setInterestCategory(tempCategoryList);


        ArrayList<FollowerFollowingModel> tempotherUserFriendList = new Gson().fromJson(response.getAsJsonArray("my_friends").toString(),
                new TypeToken<List<FollowerFollowingModel>>() {
                }.getType());

        setFriendList(tempotherUserFriendList);

        nested.setOnScrollChangeListener((NestedScrollView.OnScrollChangeListener) (v, scrollX, scrollY, oldScrollX, oldScrollY) -> {
            if (v.getChildAt(v.getChildCount() - 1) != null) {
                if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                        scrollY > oldScrollY) {

                    int visibleItemCount = layoutManager.getChildCount();
                    int totalItemCount = layoutManager.getItemCount();
                    int pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();

                    if (!isRecyclerViewWaitingtoLoadData && loadedAllItems) {

                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            getUserPosts();

                        }
                    }
                }
            }

        });

        rl_progress_bar.setVisibility(View.GONE);
    }

    private void setInterestCategory(ArrayList<CategoryModel> categoryList) {
        try {

            if (categoryList.size() > 4) {
                tv_show_more.setVisibility(View.VISIBLE);
            } else {
                tv_show_more.setVisibility(View.GONE);
            }

            final LayoutInflater mInflater = LayoutInflater.from(OtherUserProfileActivity.this);
            tagflow_interest_category.setAdapter(new TagAdapter<CategoryModel>(categoryList) {

                @Override
                public View getView(FlowLayout parent, int position, CategoryModel categoryModel) {
                    TextView tv = (TextView) mInflater.inflate(R.layout.row_interests_category, tagflow_interest_category, false);
                    tv.setText(categoryModel.getCategory_name());
                    return tv;
                }

                @Override
                public int getCount() {
                    if (!isAllCategories) {
                        return 4;
                    } else {
                        return super.getCount();
                    }

                }


            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * for follow- unfollow other user
     *
     * @param type - 1 for follow , 0 for unfollow
     */
    private void callAPIChangeFollowUnfollowStatus(final String type) {

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("follower_id", otherUserId);
        request.put("type", type);

        ApiCall.getInstance().changeFollowUnfollowStatus(OtherUserProfileActivity.this, request, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {
                String response = (String) data;

                if (type.equalsIgnoreCase("0")) {
                    txt_follow.setText(R.string.follow);
                } else {
                    txt_follow.setText(R.string.unfollow);
                }

            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {
                if (StringUtils.isNotEmpty(errorMessage)) {
                    DialogUtils.openDialog(OtherUserProfileActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                        @Override
                        public void onNegativeBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }

                        @Override
                        public void onPositiveBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }
                    });
                }
            }
        }, true);


    }

    private void callAPIfriendUnfriend(final String type) {


        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("friend_id", otherUserId);
        request.put("type", type);

        ApiCall.getInstance().friendUnfriend(OtherUserProfileActivity.this, request, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {

                String response = (String) data;

                if (type.equalsIgnoreCase("1")) {
                    txt_add_friend.setText(getString(R.string.requested));
                } else {
                    txt_add_friend.setText(getString(R.string.add_friend));
                }

//                AppClass.snackBarView.snackBarShow(OtherUserProfileActivity.this, response);

            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {
                if (StringUtils.isNotEmpty(errorMessage)) {
                    DialogUtils.openDialog(OtherUserProfileActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                        @Override
                        public void onNegativeBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }

                        @Override
                        public void onPositiveBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }
                    });
                }

            }
        }, true);

    }

    private void acceptRejectFriendRequest(final String status) {

        String userId = AppClass.preferences.getUserId();

        ApiCall.getInstance().acceptRejectRequest(OtherUserProfileActivity.this, userId, otherUserId, status, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {

                ll_friend_request.setVisibility(View.GONE);
                txt_add_friend.setVisibility(View.VISIBLE);

                if (status.equalsIgnoreCase("1")) {
                    txt_add_friend.setText(R.string.unfriend);
                    ll_friend_request.setVisibility(View.GONE);
                } else {
                    txt_add_friend.setText(R.string.add_friend);
                }

            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {

                Logger.e("Fail " + errorMessage);

                if (StringUtils.isNotEmpty(errorMessage)) {
                    DialogUtils.openDialog(OtherUserProfileActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                        @Override
                        public void onNegativeBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }

                        @Override
                        public void onPositiveBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }
                    });
                }

            }
        }, true);

    }


    private void setFriendList(ArrayList<FollowerFollowingModel> otherUserFriendsList) {

        int sizeForLoop;

        if (otherUserFriendsList.size() > 3) {
            sizeForLoop = 3;
        } else {
            sizeForLoop = otherUserFriendsList.size();
        }

        for (int i = 0; i < sizeForLoop; i++) {
            View view = LayoutInflater.from(OtherUserProfileActivity.this).inflate(R.layout.row_profile_friend, null);
            ImageView iv_image = view.findViewById(R.id.img_friend_profile_pic);
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) iv_image.getLayoutParams();
            lp.setMargins(10 + (i * 70), 0, 0, 0);
            iv_image.setLayoutParams(lp);
            if (otherUserFriendsList.get(i)!= null) {

                ImageLoadUtils.imageLoad(this,
                        iv_image,
                        otherUserFriendsList.get(i).getProfileImage(),
                        R.drawable.menu_user_ph);
            } else {

                Glide.with(this)
                        .load(R.drawable.menu_user_ph)
                        .into(iv_image);
            }


            rv_friend.addView(view);
        }


        if (otherUserFriendsList.size() > 3) {
            View view = LayoutInflater.from(OtherUserProfileActivity.this).inflate(R.layout.row_profile_friend, null);
            ImageView iv_image = view.findViewById(R.id.img_friend_profile_pic);
            iv_image.setVisibility(View.GONE);

            TextView txt_friends_count = view.findViewById(R.id.txt_friends_count);
            txt_friends_count.setVisibility(View.VISIBLE);

            txt_friends_count.setText("+" + (otherUserFriendsList.size() - 3));

            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) txt_friends_count.getLayoutParams();
            lp.setMargins(10 + (3 * 70), 0, 0, 0);
            txt_friends_count.setLayoutParams(lp);

            rv_friend.addView(view);
        }


    }

    public abstract static class AppBarStateChangeListener implements AppBarLayout.OnOffsetChangedListener {

        private AppBarStateChangeListener.State mCurrentState = AppBarStateChangeListener.State.IDLE;

        @Override
        public final void onOffsetChanged(AppBarLayout appBarLayout, int i) {
            if (i == 0) {
                if (mCurrentState != AppBarStateChangeListener.State.EXPANDED) {
                    onStateChanged(appBarLayout, AppBarStateChangeListener.State.EXPANDED);
                }
                mCurrentState = AppBarStateChangeListener.State.EXPANDED;
            } else if (Math.abs(i) >= appBarLayout.getTotalScrollRange()) {
                if (mCurrentState != AppBarStateChangeListener.State.COLLAPSED) {
                    onStateChanged(appBarLayout, AppBarStateChangeListener.State.COLLAPSED);
                }
                mCurrentState = AppBarStateChangeListener.State.COLLAPSED;
            } else {
                if (mCurrentState != AppBarStateChangeListener.State.IDLE) {
                    onStateChanged(appBarLayout, AppBarStateChangeListener.State.IDLE);
                }
                mCurrentState = AppBarStateChangeListener.State.IDLE;
            }
        }

        public abstract void onStateChanged(AppBarLayout appBarLayout, AppBarStateChangeListener.State state);

        public enum State {
            EXPANDED,
            COLLAPSED,
            IDLE
        }
    }

}
