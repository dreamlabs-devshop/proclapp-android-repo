package com.proclapp.profile;

import android.app.Activity;
import android.os.Build;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.proclapp.AppClass;
import com.proclapp.R;

import java.util.ArrayList;

/**
 */

public class ProfessionalQualificationAdapter extends RecyclerView.Adapter<ProfessionalQualificationAdapter.MyViewHolder> {

    Activity activity;
    ArrayList<String> experience_year_list;
    View oldSelectedSpinner;
    int oldPos;
    private ArrayAdapter<String> spinnerArrayAdapter;

    public ProfessionalQualificationAdapter(Activity activity, ArrayList<String> experience_year_list) {
        this.activity = activity;
        this.experience_year_list = experience_year_list;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_professional_qualification, parent, false);
        return new MyViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        if (EditProfileActivity.professionalQuaArrayList.size() > 1) {
            holder.img_cross.setVisibility(View.VISIBLE);
        } else {
            holder.img_cross.setVisibility(View.GONE);
        }

        holder.edt_job_title.setText(EditProfileActivity.professionalQuaArrayList.get(position).getJob_title());
        holder.spinner_select_experience.setSelection(spinnerArrayAdapter.getPosition(EditProfileActivity.professionalQuaArrayList.get(position).getExperience()));


        if (EditProfileActivity.professionalQuaArrayList.get(position).getIsCurrentJob().equalsIgnoreCase("0")) {
            holder.spinner_currentjob.setSelection(1);
        } else {
            holder.spinner_currentjob.setSelection(0);
        }

        holder.edt_job_title.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                EditProfileActivity.professionalQuaArrayList.get(position).setJob_title(s.toString());
            }
        });

        holder.img_dropdown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.spinner_select_experience.performClick();
            }
        });

        holder.img_cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (EditProfileActivity.professionalQuaArrayList.size() > 1) {
                    EditProfileActivity.professionalQuaArrayList.remove(holder.getAdapterPosition());
                    notifyItemRemoved(holder.getAdapterPosition());

                    if (EditProfileActivity.professionalQuaArrayList.size() == 1) {
                        holder.img_cross.setVisibility(View.GONE);
                        notifyDataSetChanged();
                    }
                }
            }
        });

        holder.spinner_select_experience.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                EditProfileActivity.professionalQuaArrayList.get(position).setExperience(experience_year_list.get(adapterView.getSelectedItemPosition()));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        holder.spinner_currentjob.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
                if (pos == 1) {
                    EditProfileActivity.professionalQuaArrayList.get(holder.getAdapterPosition()).setIsCurrentJob("0");

                } else {
                    if (oldSelectedSpinner != null) {
                        ((Spinner) oldSelectedSpinner).setSelection(1);
                    }
                    EditProfileActivity.professionalQuaArrayList.get(holder.getAdapterPosition()).setIsCurrentJob("1");
                    oldSelectedSpinner = adapterView;

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    @Override
    public int getItemCount() {
        return EditProfileActivity.professionalQuaArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txt_job_title,
                txt_experience;
        EditText edt_job_title;
        Spinner spinner_select_experience,
                spinner_currentjob;
        ImageView img_dropdown,
                img_cross;

        public MyViewHolder(View itemView) {
            super(itemView);
            txt_job_title = itemView.findViewById(R.id.txt_job_title);
            txt_experience = itemView.findViewById(R.id.txt_experience);
            edt_job_title = itemView.findViewById(R.id.edt_job_title);
            img_dropdown = itemView.findViewById(R.id.img_dropdown);
            img_cross = itemView.findViewById(R.id.img_cross);
            spinner_select_experience = itemView.findViewById(R.id.spinner_select_experience);
            spinner_currentjob = itemView.findViewById(R.id.spinner_currentjob);

            txt_job_title.setTypeface(AppClass.lato_regular);
            txt_experience.setTypeface(AppClass.lato_regular);
            edt_job_title.setTypeface(AppClass.lato_regular);


            setExperienceSpinner();
            setCurrentJobSpinner();

        }

        // set experience spinner data
        private void setExperienceSpinner() {

            spinnerArrayAdapter = new ArrayAdapter<String>
                    (activity, android.R.layout.simple_spinner_item, experience_year_list) {
                @Override
                public View getDropDownView(int position, View convertView, ViewGroup parent) {

                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    tv.setTypeface(AppClass.lato_regular);
                    tv.setTextSize(12);
                    return tv;
                }

                @Override
                public View getView(int position, View convertView, ViewGroup parent) {

                    TextView tv = (TextView) super.getView(position, convertView, parent);
                    tv.setTypeface(AppClass.lato_regular);
                    tv.setTextSize(12);
                    return tv;
                }


            };

            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner_select_experience.setAdapter(spinnerArrayAdapter);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                spinner_select_experience.setPopupBackgroundResource(R.color.white);
            }

        }

        private void setCurrentJobSpinner() {

            ArrayList<String> currentJob = new ArrayList<>();
            currentJob.add("Yes");
            currentJob.add("No");

            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>
                    (activity, android.R.layout.simple_spinner_item, currentJob) {
                @Override
                public View getDropDownView(int position, View convertView, ViewGroup parent) {

                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    tv.setTypeface(AppClass.lato_regular);
                    tv.setTextSize(12);
                    return tv;
                }

                @Override
                public View getView(int position, View convertView, ViewGroup parent) {

                    TextView tv = (TextView) super.getView(position, convertView, parent);
                    tv.setTypeface(AppClass.lato_regular);
                    tv.setTextSize(12);
                    return tv;
                }


            };

            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner_currentjob.setAdapter(spinnerArrayAdapter);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                spinner_select_experience.setPopupBackgroundResource(R.color.white);
            }

        }

    }
}
