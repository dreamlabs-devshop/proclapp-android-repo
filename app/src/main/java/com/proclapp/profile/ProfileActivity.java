package com.proclapp.profile;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.home.adapter.AllPostsAdapter;
import com.proclapp.home.friends.FriendsActivity;
import com.proclapp.interfaces.RecyclerClickListener;
import com.proclapp.model.AllPostsModel;
import com.proclapp.model.CategoryModel;
import com.proclapp.model.FollowerFollowingModel;
import com.proclapp.profile.followerfollowing.FollowerActivity;
import com.proclapp.profile.followerfollowing.FollowingActivity;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.AlertDialogInterface;
import com.proclapp.utils.DialogUtils;
import com.proclapp.utils.ImageLoadUtils;
import com.proclapp.utils.Logger;
import com.proclapp.utils.StaticData;
import com.proclapp.utils.StringUtils;
import com.zhy.view.flowlayout.FlowLayout;
import com.zhy.view.flowlayout.TagAdapter;
import com.zhy.view.flowlayout.TagFlowLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener {
    TagFlowLayout tagflow_interest_category;
    ArrayList<String> selectedCategories = new ArrayList<>();
    LinearLayoutManager layoutManager;
    private RecyclerView rv_user_post;
    /*rv_friend;*/
    private CollapsingToolbarLayout collapsing_toolbar;
    private AppBarLayout app_bar_layout;
    private Toolbar toolbar;
    private ImageView img_back, img_cover;
    private TextView txt_edit1,
            txt_followers,
            txt_followers_count,
            txt_following,
            txt_following_count,
            txt_user_name,
            txt_expert,
            txt_user_designation,
            txt_follow,
            txt_about_me,
            txt_about_my_profession,
            txt_about_my_profession_value,
            txt_app_joining,
            txt_app_joining_value,
            txt_profile,
            tv_readmore_about_me,
            tv_readmore_about_profession,
            txt_categories,
            txt_categories_edit,
            txt_about_education_value,
            txt_user_location,
            tv_show_more,
            tv_no_post,
            txt_friend;
    private RelativeLayout rv_friend;
    private CircleImageView img_user_profile;
    private ProfileFriendAdapter profileFriendAdapter;
    private ProgressDialog progressDialog;
    private ArrayList<String> categories = new ArrayList<>();
    private LinearLayout ll_following, ll_follower;
    private ArrayList<FollowerFollowingModel> friendsList = new ArrayList<>();
    private ArrayList<AllPostsModel> allPostsLists = new ArrayList<>();
    private AllPostsAdapter allPostsAdapter;
    private NestedScrollView nested;
    private RelativeLayout rl_progress_post;
    private RelativeLayout rl_progress_bar;
    private boolean isAllCategories = false;
    private boolean
            isRecyclerViewWaitingtoLoadData = false,
            loadedAllItems = false;
//    private int offset = 0;

    private LinearLayout
            ll_friends,
            ll_education,
            ll_profession;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.loading));

        initView();
        initStyle();
        initListener();

    }

    @Override
    protected void onResume() {
        super.onResume();
        callAPIGetProfile();
    }

    private void initView() {

        img_back = findViewById(R.id.img_back);
        img_cover = findViewById(R.id.img_cover);
        txt_edit1 = findViewById(R.id.txt_edit1);
        txt_followers = findViewById(R.id.txt_followers);
        txt_followers_count = findViewById(R.id.txt_followers_count);
        txt_following = findViewById(R.id.txt_following);
        txt_following_count = findViewById(R.id.txt_following_count);
        txt_user_name = findViewById(R.id.txt_user_name);
        txt_expert = findViewById(R.id.txt_expert);
        txt_user_designation = findViewById(R.id.txt_user_designation);
        txt_follow = findViewById(R.id.txt_follow);
        txt_about_me = findViewById(R.id.txt_about_me);
        txt_about_my_profession = findViewById(R.id.txt_about_my_profession);
        txt_about_my_profession_value = findViewById(R.id.txt_about_my_profession_value);
        txt_app_joining = findViewById(R.id.txt_app_joining);
        txt_app_joining_value = findViewById(R.id.txt_app_joining_value);
        txt_user_location = findViewById(R.id.txt_user_location);
        txt_about_education_value = findViewById(R.id.txt_about_education_value);
        txt_profile = findViewById(R.id.txt_profile);
        txt_friend = findViewById(R.id.txt_friend);
        tv_readmore_about_me = findViewById(R.id.tv_readmore_about_me);
        tv_readmore_about_profession = findViewById(R.id.tv_readmore_about_profession);
        tv_show_more = findViewById(R.id.tv_show_more);
        tv_no_post = findViewById(R.id.tv_no_post);

        img_user_profile = findViewById(R.id.img_user_profile);
        txt_categories = findViewById(R.id.txt_categories);
        txt_categories_edit = findViewById(R.id.txt_categories_edit);
        tagflow_interest_category = findViewById(R.id.tagflow_interest_category);
        ll_following = findViewById(R.id.ll_following);
        ll_follower = findViewById(R.id.ll_follower);
        ll_friends = findViewById(R.id.ll_friends);
        ll_education = findViewById(R.id.ll_education);
        ll_profession = findViewById(R.id.ll_profession);
        rv_friend = findViewById(R.id.rv_friend);
        rl_progress_bar = findViewById(R.id.rl_progress_bar);
        rl_progress_post = findViewById(R.id.rl_progress_post);

        nested = findViewById(R.id.nested);
        rv_user_post = findViewById(R.id.rv_user_post);
        toolbar = findViewById(R.id.toolbar);

        layoutManager = new LinearLayoutManager(this);
        rv_user_post.setLayoutManager(layoutManager);


        app_bar_layout = findViewById(R.id.app_bar_layout);
        collapsing_toolbar = findViewById(R.id.collapsing_toolbar);

        profileFriendAdapter = new ProfileFriendAdapter(ProfileActivity.this);
//        rv_friend.setAdapter(profileFriendAdapter);


        app_bar_layout.addOnOffsetChangedListener(new AppBarStateChangeListener() {
            @Override
            public void onStateChanged(AppBarLayout appBarLayout, State state) {
                Logger.d("STATE" + state.name());
                if (state == State.COLLAPSED) {
                    toolbar.setBackgroundColor(ContextCompat.getColor(ProfileActivity.this, R.color.white));
                    txt_profile.setVisibility(View.VISIBLE);
                    img_back.setImageResource(R.drawable.back);
                    txt_edit1.setTextColor(ContextCompat.getColor(ProfileActivity.this, R.color.color_333333));
                } else {
                    toolbar.setBackgroundColor(Color.TRANSPARENT);
                    txt_profile.setVisibility(View.GONE);
                    img_back.setImageResource(R.drawable.back_icon_wt);
                    txt_edit1.setTextColor(ContextCompat.getColor(ProfileActivity.this, R.color.white));
                }
            }
        });

    }

    private void initStyle() {
        txt_edit1.setTypeface(AppClass.lato_regular);
        txt_followers.setTypeface(AppClass.lato_semibold);
        txt_followers_count.setTypeface(AppClass.lato_black);
        txt_following.setTypeface(AppClass.lato_semibold);
        txt_following_count.setTypeface(AppClass.lato_black);
        txt_user_name.setTypeface(AppClass.lato_heavy);
        txt_expert.setTypeface(AppClass.lato_regular);
        tv_show_more.setTypeface(AppClass.lato_regular);
        txt_user_designation.setTypeface(AppClass.lato_regular);
        txt_follow.setTypeface(AppClass.lato_semibold);
        txt_about_me.setTypeface(AppClass.lato_medium);
        txt_about_my_profession.setTypeface(AppClass.lato_black);
        txt_about_my_profession_value.setTypeface(AppClass.lato_medium);
        txt_app_joining.setTypeface(AppClass.lato_black);
        txt_app_joining_value.setTypeface(AppClass.lato_light);
        txt_profile.setTypeface(AppClass.lato_regular);
        txt_friend.setTypeface(AppClass.lato_regular);
        txt_categories.setTypeface(AppClass.lato_black);
        txt_categories_edit.setTypeface(AppClass.lato_regular);
    }

    private void initListener() {
        img_back.setOnClickListener(this);
        txt_edit1.setOnClickListener(this);
        txt_categories_edit.setOnClickListener(this);
        ll_following.setOnClickListener(this);
        ll_follower.setOnClickListener(this);
        tv_readmore_about_me.setOnClickListener(this);
        tv_readmore_about_profession.setOnClickListener(this);
        tv_show_more.setOnClickListener(this);
        ll_friends.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.img_back:
                onBackPressed();
                break;

            case R.id.txt_edit1:
                startActivity(new Intent(ProfileActivity.this, EditProfileActivity.class));
                break;

            case R.id.ll_follower:
                startActivity(new Intent(ProfileActivity.this, FollowerActivity.class));
                break;

            case R.id.ll_following:
                startActivity(new Intent(ProfileActivity.this, FollowingActivity.class));
                break;

            case R.id.txt_categories_edit:
                startActivity(new Intent(ProfileActivity.this, EditCategoriesActivity.class)
                        .putExtra("selectedCategories", new Gson().toJson(selectedCategories)));
                break;

            case R.id.ll_friends:
                startActivity(new Intent(this, FriendsActivity.class)
                        .putExtra("openfrom", "notification"));
                break;

            case R.id.tv_show_more:
                if (tv_show_more.getText().toString().equalsIgnoreCase(getString(R.string.show_more))) {
                    isAllCategories = true;
                    tagflow_interest_category.getAdapter().notifyDataChanged();
                    tv_show_more.setText(getString(R.string.show_less));
                } else {
                    isAllCategories = false;
                    tagflow_interest_category.getAdapter().notifyDataChanged();
                    tv_show_more.setText(getString(R.string.show_more));
                }
                break;

            case R.id.tv_readmore_about_me:
                if (txt_about_me.getMaxLines() == 3) {
                    tv_readmore_about_me.setText(getString(R.string.read_less));
                    txt_about_me.setMaxLines(Integer.MAX_VALUE);
                } else {
                    tv_readmore_about_me.setText(R.string.read_more);
                    txt_about_me.setMaxLines(4);
                }
                break;

            case R.id.tv_readmore_about_profession:
                if (txt_about_my_profession_value.getMaxLines() == 4) {
                    tv_readmore_about_profession.setText(getString(R.string.read_less));
                    txt_about_my_profession_value.setMaxLines(Integer.MAX_VALUE);
                } else {
                    tv_readmore_about_profession.setText(R.string.read_more);
                    txt_about_my_profession_value.setMaxLines(4);
                }
                break;
        }
    }

    private void callAPIGetProfile() {
        if (AppClass.isInternetConnectionAvailable()) {

            rl_progress_bar.setVisibility(View.VISIBLE);

            HashMap<String, String> request = new HashMap<>();
            request.put("user_id", AppClass.preferences.getUserId());
            request.put("profile_id", AppClass.preferences.getUserId());

            ApiCall.getInstance().getProfile(this, request, new IApiCallback() {
                @Override
                public void onSuccess(Object data) {
                    JsonObject response = (JsonObject) data;
                    setData(response);
                    setupRecyclerView();


                }

                @Override
                public void onFailure(boolean isFalseFromService, String errorMessage) {

                    if (StringUtils.isNotEmpty(errorMessage)) {


                        DialogUtils.openDialog(ProfileActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                            @Override
                            public void onNegativeBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }

                            @Override
                            public void onPositiveBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }
                        });

                    }
                }
            }, false);


        } else {
            AppClass.snackBarView.snackBarShow(ProfileActivity.this, getString(R.string.nonetwork));
        }

    }


    private void setupRecyclerView() {

        allPostsAdapter = new AllPostsAdapter(ProfileActivity.this, allPostsLists, new RecyclerClickListener() {
            @Override
            public void onItemClick(int pos, String tag) {

            }
        });
        rv_user_post.setAdapter(allPostsAdapter);

        getUserPosts();

    }

    private void getUserPosts() {
        if (AppClass.isInternetConnectionAvailable()) {

            if (allPostsLists.size() != 0)
                rl_progress_post.setVisibility(View.VISIBLE);

            HashMap<String, String> request = new HashMap<>();
            request.put("user_id", AppClass.preferences.getUserId());
            request.put("other_person_id", AppClass.preferences.getUserId());
            request.put("offset", String.valueOf(allPostsLists.size()));


            ApiCall.getInstance().getUserPosts(this, request, new IApiCallback() {
                @Override
                public void onSuccess(Object data) {

                    isRecyclerViewWaitingtoLoadData = false;
                    loadedAllItems = true;
                    if (allPostsLists.size() == 0)
                        allPostsLists.clear();

                    allPostsLists.addAll((ArrayList<AllPostsModel>) data);
                    rv_user_post.getAdapter().notifyDataSetChanged();
                    tv_no_post.setVisibility(View.GONE);

                }

                @Override
                public void onFailure(boolean isFalseFromService, String errorMessage) {
                    isRecyclerViewWaitingtoLoadData = true;
                    loadedAllItems = true;
                    if (StringUtils.isNotEmpty(errorMessage)) {

                        if (errorMessage.equalsIgnoreCase("no data found")) {
                            if (allPostsLists.size() == 0) {
                                tv_no_post.setVisibility(View.VISIBLE);
                            }
                        } else {

                            DialogUtils.openDialog(ProfileActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                                @Override
                                public void onNegativeBtnClicked(DialogInterface alrt) {
                                    alrt.dismiss();
                                }

                                @Override
                                public void onPositiveBtnClicked(DialogInterface alrt) {
                                    alrt.dismiss();
                                }
                            });
                        }
                    }
                    rl_progress_post.setVisibility(View.GONE);
                }
            }, false);


        } else {
            AppClass.snackBarView.snackBarShow(ProfileActivity.this, getString(R.string.nonetwork));
        }

    }

    private void setData(JsonObject userData) {

        try {

            AppClass.preferences.storeUserId(userData.get("user_id").getAsString());
            AppClass.preferences.storeFirstName(userData.get("firstname").getAsString());
            AppClass.preferences.storeUSerLastName(userData.get("lastname").getAsString());
            AppClass.preferences.storeEmailId(userData.get("email").getAsString());
            AppClass.preferences.storeAddress(userData.get("address").getAsString());
            AppClass.preferences.storeGender(userData.get("gender").getAsString());
            AppClass.preferences.storeMobileNumber(userData.get("phone").getAsString());
            AppClass.preferences.storeCountryCode(userData.get("phone_code").getAsString());
            AppClass.preferences.storeAboutMe(userData.get("about_me").getAsString());
            AppClass.preferences.storeAboutMyProfession(userData.get("about_my_profession").getAsString());
            AppClass.preferences.storeProfileImage(userData.get("profile_image").getAsString());
            AppClass.preferences.storeCoverImage(userData.get("cover_image").getAsString());
            AppClass.preferences.storeMemberSince(userData.get("member_since").getAsString());
            AppClass.preferences.storeIsExpert(userData.get("is_expert").getAsString());
            AppClass.preferences.storeFollowers(userData.get("followers").getAsString());
            AppClass.preferences.storeFollowing(userData.get("total_following").getAsString());


            if (StringUtils.isNotEmpty(userData.get("address").getAsString())) {
                txt_user_location.setVisibility(View.VISIBLE);
                txt_user_location.setText(userData.get("address").getAsString());
            } else {
                txt_user_location.setVisibility(View.GONE);
            }
            txt_user_name.setText(String.format("%s %s", AppClass.preferences.getUserFirstName(), AppClass.preferences.getUserLastName()));
            txt_about_me.setText(AppClass.preferences.getAboutMe());
            if (StringUtils.isNotEmpty(AppClass.preferences.getAboutMyProfession())) {
                ll_profession.setVisibility(View.VISIBLE);
                txt_about_my_profession_value.setText(AppClass.preferences.getAboutMyProfession());
            } else {
                ll_profession.setVisibility(View.GONE);
            }
            txt_following_count.setText(AppClass.preferences.getFollowing());
            txt_followers_count.setText(AppClass.preferences.getFollowers());


            if (userData.getAsJsonArray("educational_qualification").size() > 0 &&
                    StringUtils.isNotEmpty(userData.getAsJsonArray("educational_qualification").get(0).getAsJsonObject().get("qualification").getAsString())) {
                ll_education.setVisibility(View.VISIBLE);
                txt_about_education_value.setText(userData.getAsJsonArray("educational_qualification").get(0).getAsJsonObject().get("qualification").getAsString());
            } else {
                ll_education.setVisibility(View.GONE);
            }

            if (AppClass.preferences.getIsExpert().equalsIgnoreCase("0")) {
                txt_expert.setVisibility(View.GONE);
            } else {
                txt_expert.setVisibility(View.VISIBLE);
            }


            txt_app_joining_value.setText(StaticData.formatDate("yyyy-MM-dd HH:mm:ss", "MMM dd, yyyy", AppClass.preferences.getMemberSince()));


            ImageLoadUtils.imageLoad(ProfileActivity.this,
                    img_user_profile,
                    AppClass.preferences.getProfileImage(),
                    R.drawable.profile_pic_ph);

            ImageLoadUtils.imageLoad(ProfileActivity.this,
                    img_cover,
                    AppClass.preferences.getCoverImage(),
                    R.drawable.profile_bg);

            tv_readmore_about_profession.post(new Runnable() {
                @Override
                public void run() {
                    tv_readmore_about_profession.setVisibility(txt_about_my_profession_value.getLineCount() > 3 ? View.VISIBLE : View.GONE);
                }
            });

            tv_readmore_about_me.post(new Runnable() {
                @Override
                public void run() {
                    tv_readmore_about_me.setVisibility(txt_about_me.getLineCount() > 2 ? View.VISIBLE : View.GONE);
                }
            });


            ArrayList<CategoryModel> tempCategoryList = new Gson().fromJson(userData.getAsJsonArray("interest_categories").toString(),
                    new TypeToken<List<CategoryModel>>() {
                    }.getType());

            categories.clear();
            for (int i = 0; i < tempCategoryList.size(); i++) {
                categories.add(tempCategoryList.get(i).getCategory_id());
            }

            AppClass.preferences.storeCategoriesId(categories.toString());

            tv_show_more.setVisibility(tempCategoryList.size() > 4 ? View.VISIBLE : View.GONE);


            setInterestCategory(tempCategoryList);


            JsonArray professionalQualification = userData.getAsJsonArray("professional_qualification");

            if (professionalQualification.size() > 0) {
                txt_user_designation.setVisibility(View.VISIBLE);
                for (int i = 0; i < professionalQualification.size(); i++) {

                    if (professionalQualification.get(i).getAsJsonObject().get("is_current_job").getAsString().equalsIgnoreCase("1")) {

                        AppClass.preferences.storeCurrentJob(professionalQualification.get(i).getAsJsonObject().get("job_title").getAsString());
                        txt_user_designation.setText(AppClass.preferences.getCurrentJob());
                        break;
                    }

                }

            } else {
                txt_user_designation.setVisibility(View.GONE);
            }
            Logger.e("professionalQualification " + professionalQualification.size());

//            callAPIGetFriendsList();

            ArrayList<FollowerFollowingModel> tempFriendList = new Gson().fromJson(userData.getAsJsonArray("my_friends").toString(),
                    new TypeToken<List<FollowerFollowingModel>>() {
                    }.getType());

            friendsList.clear();
            friendsList.addAll(tempFriendList);
            setFriendList(friendsList);

            nested.setOnScrollChangeListener((NestedScrollView.OnScrollChangeListener) (v, scrollX, scrollY, oldScrollX, oldScrollY) -> {
                if (v.getChildAt(v.getChildCount() - 1) != null) {
                    if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                            scrollY > oldScrollY) {

                        int visibleItemCount = layoutManager.getChildCount();
                        int totalItemCount = layoutManager.getItemCount();
                        int pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();

                        if (!isRecyclerViewWaitingtoLoadData && loadedAllItems) {

                            if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                                getUserPosts();

                            }
                        }
                    }
                }

            });


            rl_progress_bar.setVisibility(View.GONE);

        } catch (Exception e) {
            e.printStackTrace();
            rl_progress_bar.setVisibility(View.GONE);
        }

    }

    private void setInterestCategory(ArrayList<CategoryModel> categoryList) {
        try {

            final LayoutInflater mInflater = LayoutInflater.from(ProfileActivity.this);
            selectedCategories.clear();
            tagflow_interest_category.setAdapter(new TagAdapter<CategoryModel>(categoryList) {
                @Override
                public View getView(FlowLayout parent, int position, CategoryModel categoryModel) {
                    TextView tv = (TextView) mInflater.inflate(R.layout.row_interests_category, tagflow_interest_category, false);
                    tv.setText(categoryModel.getCategory_name());
                    selectedCategories.add(categoryModel.getCategory_id());
                    return tv;
                }

                @Override
                public int getCount() {
                    if (!isAllCategories && categoryList.size() > 4) {
                        return 4;
                    } else {
                        return super.getCount();
                    }

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setFriendList(ArrayList<FollowerFollowingModel> friendsList) {

        if (friendsList.isEmpty()) {
            ll_friends.setVisibility(View.GONE);
        } else {
            for (int i = 0; i < Math.min(friendsList.size(), 3); i++) {
                View view = LayoutInflater.from(ProfileActivity.this).inflate(R.layout.row_profile_friend, null);
                ImageView iv_image = view.findViewById(R.id.img_friend_profile_pic);
                RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) iv_image.getLayoutParams();
                lp.setMargins(10 + (i * 70), 0, 0, 0);
                iv_image.setLayoutParams(lp);

                ImageLoadUtils.imageLoad(ProfileActivity.this,
                        iv_image,
                        friendsList.get(i).getProfileImage(),
                        R.drawable.profile_pic_ph);

                rv_friend.addView(view);
            }


            if (friendsList.size() > 3) {
                View view = LayoutInflater.from(ProfileActivity.this).inflate(R.layout.row_profile_friend, null);
                ImageView iv_image = view.findViewById(R.id.img_friend_profile_pic);
                iv_image.setVisibility(View.GONE);

                TextView txt_friends_count = view.findViewById(R.id.txt_friends_count);
                txt_friends_count.setVisibility(View.VISIBLE);

                txt_friends_count.setText(String.format(Locale.getDefault(), "+%d", friendsList.size() - 3));

                RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) txt_friends_count.getLayoutParams();
                lp.setMargins(10 + (3 * 70), 0, 0, 0);
                txt_friends_count.setLayoutParams(lp);

                rv_friend.addView(view);
            }
        }

    }

    public abstract static class AppBarStateChangeListener implements AppBarLayout.OnOffsetChangedListener {

        private State mCurrentState = State.IDLE;

        @Override
        public final void onOffsetChanged(AppBarLayout appBarLayout, int i) {
            if (i == 0) {
                if (mCurrentState != State.EXPANDED) {
                    onStateChanged(appBarLayout, State.EXPANDED);
                }
                mCurrentState = State.EXPANDED;
            } else if (Math.abs(i) >= appBarLayout.getTotalScrollRange()) {
                if (mCurrentState != State.COLLAPSED) {
                    onStateChanged(appBarLayout, State.COLLAPSED);
                }
                mCurrentState = State.COLLAPSED;
            } else {
                if (mCurrentState != State.IDLE) {
                    onStateChanged(appBarLayout, State.IDLE);
                }
                mCurrentState = State.IDLE;
            }
        }

        public abstract void onStateChanged(AppBarLayout appBarLayout, State state);

        public enum State {
            EXPANDED,
            COLLAPSED,
            IDLE
        }
    }

}
