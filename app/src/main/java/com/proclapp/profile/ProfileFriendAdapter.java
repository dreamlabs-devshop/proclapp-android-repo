package com.proclapp.profile;

import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.proclapp.AppClass;
import com.proclapp.R;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileFriendAdapter extends RecyclerView.Adapter<ProfileFriendAdapter.MyViewHolder> {

    Activity activity;


    public ProfileFriendAdapter(Activity activity) {
        this.activity = activity;


    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_profile_friend, parent, false);
        return new MyViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(final ProfileFriendAdapter.MyViewHolder holder, final int position) {


        if (position > 2) {
            holder.img_friend_profile_pic.setVisibility(View.GONE);
            holder.txt_friends_count.setVisibility(View.VISIBLE);
        } else {
            holder.img_friend_profile_pic.setVisibility(View.VISIBLE);
            holder.txt_friends_count.setVisibility(View.GONE);
        }

        if (position > 0) {
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(holder.ll_friend.getLayoutParams());
            params.setMargins(-25, 0, -25, 0);
            holder.ll_friend.setLayoutParams(params);
        }else {
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(holder.ll_friend.getLayoutParams());
            params.setMargins(0, 0, -25, 0);
            holder.ll_friend.setLayoutParams(params);
        }

    }

    @Override
    public int getItemCount() {
        return 4;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private CircleImageView img_friend_profile_pic;
        private TextView txt_friends_count;
        private RelativeLayout ll_friend;

        public MyViewHolder(View itemView) {
            super(itemView);

            img_friend_profile_pic = itemView.findViewById(R.id.img_friend_profile_pic);
            txt_friends_count = itemView.findViewById(R.id.txt_friends_count);
            ll_friend = itemView.findViewById(R.id.ll_friend);

            txt_friends_count.setTypeface(AppClass.lato_black);
        }
    }


}
