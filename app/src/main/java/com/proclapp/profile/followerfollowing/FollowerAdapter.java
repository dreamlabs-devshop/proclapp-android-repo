package com.proclapp.profile.followerfollowing;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.proclapp.R;
import com.proclapp.interfaces.RecyclerClickListener;
import com.proclapp.model.FollowerFollowingModel;
import com.proclapp.utils.ImageLoadUtils;

import java.util.ArrayList;

public class FollowerAdapter extends RecyclerView.Adapter<FollowerAdapter.FollowerViewHolder> {

    private AppCompatActivity context;
    private ArrayList<FollowerFollowingModel> followerList;
    private RecyclerClickListener clickListener;

    public FollowerAdapter(AppCompatActivity context, ArrayList<FollowerFollowingModel> followerList,RecyclerClickListener clickListener) {
        this.context = context;
        this.followerList = followerList;
        this.clickListener = clickListener;
    }

    @NonNull
    @Override
    public FollowerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_follower, viewGroup, false);
        return new FollowerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FollowerViewHolder holder, int position) {

        FollowerFollowingModel followerData = followerList.get(position);

        holder.tv_follower_name.setText(followerData.getName());
        holder.tv_follower_profession.setText(followerData.getProfession());

        ImageLoadUtils.imageLoad(context,
                holder.iv_follower_proflie,
                followerData.getProfileImage(),
                R.drawable.profile_pic_ph);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickListener.onItemClick(holder.getAdapterPosition(), "itemClick");
            }
        });
    }

    @Override
    public int getItemCount() {
        return followerList.size();
    }

    public class FollowerViewHolder extends RecyclerView.ViewHolder {
        ImageView iv_follower_proflie;
        TextView
                tv_follower_name,
                tv_follower_profession;

        public FollowerViewHolder(@NonNull View itemView) {
            super(itemView);
            iv_follower_proflie = itemView.findViewById(R.id.iv_follower_proflie);
            tv_follower_name = itemView.findViewById(R.id.tv_follower_name);
            tv_follower_profession = itemView.findViewById(R.id.tv_follower_profession);
        }
    }
}
