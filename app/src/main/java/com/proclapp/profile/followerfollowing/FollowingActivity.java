package com.proclapp.profile.followerfollowing;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.model.FollowerFollowingModel;
import com.proclapp.profile.OtherUserProfileActivity;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.AlertDialogInterface;
import com.proclapp.utils.DialogUtils;
import com.proclapp.utils.StringUtils;
import com.proclapp.utils.Utils;

import java.util.ArrayList;

public class FollowingActivity extends AppCompatActivity implements View.OnClickListener {

    private RecyclerView rv_following_list;
    private ArrayList<FollowerFollowingModel> followingList = new ArrayList<>();
    private ImageView img_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_following);
        Utils.setStatusBarColor(getWindow(), this, R.color.white);
        initViews();
        setpRecyclerView();

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.img_back:
                onBackPressed();
                break;

        }

    }

    private void initViews() {
        rv_following_list = findViewById(R.id.rv_following_list);
        img_back = findViewById(R.id.img_back);
        img_back.setOnClickListener(this);


    }

    private void setpRecyclerView() {

        rv_following_list = findViewById(R.id.rv_following_list);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rv_following_list.setLayoutManager(linearLayoutManager);
        rv_following_list.setAdapter(new FollowingAdapter(this, followingList, (pos, tag) -> {
            if (tag.equalsIgnoreCase("itemClick")) {

                startActivity(new Intent(FollowingActivity.this, OtherUserProfileActivity.class)
                        .putExtra("user_id", followingList.get(pos).getUserId()));

            }

        }));

        callAPIGetFollowing();
    }


    private void callAPIGetFollowing() {

        ApiCall.getInstance().getFollowingList(this, AppClass.preferences.getUserId(), new IApiCallback() {
            @Override
            public void onSuccess(Object data) {


                followingList.clear();
                followingList.addAll((ArrayList<FollowerFollowingModel>) data);
                rv_following_list.getAdapter().notifyDataSetChanged();
            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {
                if (StringUtils.isNotEmpty(errorMessage)) {
                    DialogUtils.openDialog(FollowingActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                        @Override
                        public void onNegativeBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }

                        @Override
                        public void onPositiveBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }
                    });
                }
            }
        }, true);


    }


}
