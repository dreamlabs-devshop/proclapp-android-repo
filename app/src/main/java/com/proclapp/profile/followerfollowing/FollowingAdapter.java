package com.proclapp.profile.followerfollowing;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.proclapp.R;
import com.proclapp.interfaces.RecyclerClickListener;
import com.proclapp.model.FollowerFollowingModel;
import com.proclapp.utils.ImageLoadUtils;

import java.util.ArrayList;

public class FollowingAdapter extends RecyclerView.Adapter<FollowingAdapter.FollowingViewHolder> {

    AppCompatActivity context;
    ArrayList<FollowerFollowingModel> followingList;
    RecyclerClickListener clickListener;

    public FollowingAdapter(AppCompatActivity context, ArrayList<FollowerFollowingModel> followingList, RecyclerClickListener clickListener) {
        this.context = context;
        this.followingList = followingList;
        this.clickListener = clickListener;
    }

    @NonNull
    @Override
    public FollowingViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_following, viewGroup, false);
        return new FollowingViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final FollowingViewHolder holder, int position) {

        FollowerFollowingModel followingData = followingList.get(position);
        holder.tv_following_name.setText(followingData.getName());
        holder.tv_following_profession.setText(followingData.getProfession());

        ImageLoadUtils.imageLoad(context,
                holder.iv_following_proflie,
                followingData.getProfileImage(),
                R.drawable.profile_pic_ph);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickListener.onItemClick(holder.getAdapterPosition(), "itemClick");
            }
        });

    }

    @Override
    public int getItemCount() {
        return followingList.size();
    }

    public class FollowingViewHolder extends RecyclerView.ViewHolder {

        ImageView iv_following_proflie;
        TextView
                tv_following_name,
                tv_following_profession,
                txt_follow;

        public FollowingViewHolder(@NonNull View itemView) {
            super(itemView);
            iv_following_proflie = itemView.findViewById(R.id.iv_following_proflie);
            tv_following_name = itemView.findViewById(R.id.tv_following_name);
            tv_following_profession = itemView.findViewById(R.id.tv_following_profession);
            txt_follow = itemView.findViewById(R.id.txt_follow);
        }
    }
}
