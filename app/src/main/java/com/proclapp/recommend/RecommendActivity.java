package com.proclapp.recommend;

import android.content.DialogInterface;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.model.FollowerFollowingModel;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.AlertDialogInterface;
import com.proclapp.utils.DialogUtils;
import com.proclapp.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;

public class RecommendActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView img_back;
    private TextView txt_flash_your_post_to,
            txt_flash_user_to_remind,
            txt_done;
    private EditText edt_search;
    private RecyclerView rv_flash_to_user;
    private RecommendToUserAdapter recommendToUserAdapter;

    private ArrayList<FollowerFollowingModel> userLists = new ArrayList<>();
    private String post_id = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recommend);

        Utils.setStatusBarColor(getWindow(), this, R.color.white);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        post_id = getIntent().getStringExtra("post_id");

        initView();
        initStyle();
        initListener();

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.img_back:

                onBackPressed();

                break;

            case R.id.txt_done:

                StringBuilder stringBuilder = new StringBuilder();
                String prefix = "";

                for (int i = 0; i < userLists.size(); i++) {
                    if (userLists.get(i).isSelected()) {
                        stringBuilder.append(prefix + userLists.get(i).getUserId());
                        prefix = ",";
                    }
                }

                if (stringBuilder.length() == 0) {
                    AppClass.snackBarView.snackBarShow(RecommendActivity.this, getString(R.string.vuser));
                } else {
                    callAPIRecommendPostToUser(stringBuilder.toString());
                }
                break;
        }

    }

    private void initView() {

        img_back = findViewById(R.id.img_back);
        txt_flash_your_post_to = findViewById(R.id.txt_flash_your_post_to);
        txt_flash_user_to_remind = findViewById(R.id.txt_flash_user_to_remind);
        txt_done = findViewById(R.id.txt_done);
        edt_search = findViewById(R.id.edt_search);
        rv_flash_to_user = findViewById(R.id.rv_flash_to_user);

        setupRecyclerView();

        edt_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                recommendToUserAdapter.getFilter().filter(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    private void initStyle() {
        txt_flash_your_post_to.setTypeface(AppClass.lato_regular);
        txt_flash_user_to_remind.setTypeface(AppClass.lato_regular);
        txt_done.setTypeface(AppClass.lato_regular);
        edt_search.setTypeface(AppClass.lato_regular);
    }

    private void setupRecyclerView() {

        GridLayoutManager layoutManager = new GridLayoutManager(RecommendActivity.this, 3);
        rv_flash_to_user.setLayoutManager(layoutManager);

        recommendToUserAdapter = new RecommendToUserAdapter(RecommendActivity.this, userLists);
        rv_flash_to_user.setAdapter(recommendToUserAdapter);
        callAPIGetUserList();
    }

    private void initListener() {

        img_back.setOnClickListener(this);
        txt_done.setOnClickListener(this);
    }

    private void callAPIGetUserList() {

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());

        ApiCall.getInstance().getUserList(RecommendActivity.this,
                request, new IApiCallback() {
                    @Override
                    public void onSuccess(Object data) {

                        userLists.clear();
                        userLists.addAll((ArrayList<FollowerFollowingModel>) data);
                        rv_flash_to_user.getAdapter().notifyDataSetChanged();

                    }

                    @Override
                    public void onFailure(boolean isFalseFromService, String errorMessage) {
                        AppClass.snackBarView.snackBarShow(RecommendActivity.this, errorMessage);
                    }


                }, true);

    }

    private void callAPIRecommendPostToUser(String user) {

        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("post_id", post_id);
        request.put("user", user);

        ApiCall.getInstance().recommendedPost(RecommendActivity.this,
                request, new IApiCallback() {
                    @Override
                    public void onSuccess(Object data) {

                        DialogUtils.openDialog(RecommendActivity.this, data.toString(), getString(R.string.ok), "", new AlertDialogInterface() {
                            @Override
                            public void onNegativeBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }

                            @Override
                            public void onPositiveBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                                finish();
                            }
                        });
                    }

                    @Override
                    public void onFailure(boolean isFalseFromService, String errorMessage) {


                        DialogUtils.openDialog(RecommendActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                            @Override
                            public void onNegativeBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }

                            @Override
                            public void onPositiveBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }
                        });
                    }


                }, true);

    }


}
