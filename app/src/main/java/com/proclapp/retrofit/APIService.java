package com.proclapp.retrofit;


import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Url;

public interface APIService {

    @FormUrlEncoded
    @POST("signup")
    Call<JsonObject> signup(@FieldMap HashMap<String, String> request);

    @FormUrlEncoded
    @POST("signin")
    Call<JsonObject> signin(@FieldMap HashMap<String, String> request);

    @FormUrlEncoded
    @POST("update_device_token")
    Call<JsonObject> updateDeviceToken(@FieldMap HashMap<String, String> request);

    @FormUrlEncoded
    @POST("update_category_to_user_profile")
    Call<JsonObject> updateCategoryToUserProfile(@FieldMap HashMap<String, String> request);

    @FormUrlEncoded
    @POST("get_profile")
    Call<JsonObject> getProfile(@FieldMap HashMap<String, String> request);

    @FormUrlEncoded
    @POST("profile")
    Call<JsonObject> getOtherUserProfile(@FieldMap HashMap<String, String> request);

    @Multipart
    @POST("update_profile")
    Call<JsonObject> updateUserProfile(@PartMap HashMap<String, RequestBody> request,
                                       @Part List<MultipartBody.Part> imagesRequest);

    @FormUrlEncoded
    @POST("change_password")
    Call<JsonObject> changePassword(@FieldMap HashMap<String, String> request);

    @FormUrlEncoded
    @POST("get_notification_list")
    Call<JsonObject> getNotificationList(@FieldMap HashMap<String, String> request);

    @FormUrlEncoded
    @POST("update_notification_status")
    Call<JsonObject> updateNotificationStatus(@FieldMap HashMap<String, String> request);

    @FormUrlEncoded
    @POST("follow_status")
    Call<JsonObject> changeFollowUnfollowStatus(@FieldMap HashMap<String, String> request);

    @FormUrlEncoded
    @POST("friend_unfriend")
    Call<JsonObject> friendUnfriend(@FieldMap HashMap<String, String> request);

    @FormUrlEncoded
    @POST("bookmark_unbookmark")
    Call<JsonObject> bookmarkUnbookmark(@FieldMap HashMap<String, String> request);

    @FormUrlEncoded
    @POST("bookmark_unbookmark_post_answer")
    Call<JsonObject> bookmarkUnbookmarkPostAnswer(@FieldMap HashMap<String, String> request);

    @FormUrlEncoded
    @POST("like_unlike")
    Call<JsonObject> likeUnlike(@FieldMap HashMap<String, String> request);

    @FormUrlEncoded
    @POST("post_read")
    Call<JsonObject> postRead(@FieldMap HashMap<String, String> request);

    @FormUrlEncoded
    @POST("post_upvote_downvote")
    Call<JsonObject> postUpVoteDownVote(@FieldMap HashMap<String, String> request);

    @FormUrlEncoded
    @POST("comment_upvote_downvote")
    Call<JsonObject> postCommentUpVoteDownVote(@FieldMap HashMap<String, String> request);

    @FormUrlEncoded
    @POST("like_unlike_comments")
    Call<JsonObject> postCommentLikeUnlike(@FieldMap HashMap<String, String> request);

    @FormUrlEncoded
    @POST("rp_list")
    Call<JsonObject> getFilteredList(@FieldMap HashMap<String, String> request);

    @FormUrlEncoded
    @POST("vv_list")
    Call<JsonObject> getVVList(@FieldMap HashMap<String, String> request);

    @FormUrlEncoded
    @POST("get_answered_qa_posts")
    Call<JsonObject> getAnsweredQuestionList(@FieldMap HashMap<String, String> request);

    @FormUrlEncoded
    @POST("get_unanswered_qa_posts")
    Call<JsonObject> getUnAnsweredQuestionList(@FieldMap HashMap<String, String> request);

    @FormUrlEncoded
    @POST("discussed_articles")
    Call<JsonObject> getDiscussedArticleList(@FieldMap HashMap<String, String> request);

    @FormUrlEncoded
    @POST("nondiscussed_articles")
    Call<JsonObject> getNonDiscussedArticleList(@FieldMap HashMap<String, String> request);

    @FormUrlEncoded
    @POST("expert_list")
    Call<JsonObject> getExpertList(@Field("user_id") String userId);

    @FormUrlEncoded
    @POST("get_notification_status")
    Call<JsonObject> getNotificationAlertType(@Field("user_id") String userId);

    @FormUrlEncoded
    @POST("follower_list")
    Call<JsonObject> getFollowerList(@Field("user_id") String userId);

    @FormUrlEncoded
    @POST("following_list")
    Call<JsonObject> getFollowingList(@Field("user_id") String userId);

    @FormUrlEncoded
    @POST("get_friends")
    Call<JsonObject> getFriendsList(
            @Field("user_id") String userId,
            @Field("offset") String offset);

    @FormUrlEncoded
    @POST("get_posts")
    Call<JsonObject> getPosts(
            @Field("user_id") String userId,
            @Field("chr") String chr,
            @Field("offset") String offset);

    @FormUrlEncoded
    @POST("forgot_password")
    Call<JsonObject> forgotPassword(@Field("email") String email);

    @FormUrlEncoded
    @POST("add_QA_post")
    Call<JsonObject> addQuestionAnswer(@FieldMap HashMap<String, String> request);

    @FormUrlEncoded
    @POST("interested_category_list")
    Call<JsonObject> getCategories(@FieldMap HashMap<String, String> request);

    @FormUrlEncoded
    @POST("check_social_id")
    Call<JsonObject> checkSocialId(@FieldMap HashMap<String, String> request);

    @FormUrlEncoded
    @POST("signin_with_social")
    Call<JsonObject> signInWithSicial(@FieldMap HashMap<String, String> request);

    @FormUrlEncoded
    @POST("answer_list")
    Call<JsonObject> getAnswerList(@FieldMap HashMap<String, String> request);

    @FormUrlEncoded
    @POST("vv_answer_list")
    Call<JsonObject> getVVAnswerList(@FieldMap HashMap<String, String> request);

    @FormUrlEncoded
    @POST("get_answer_details")
    Call<JsonObject> getAnswerDetails(@FieldMap HashMap<String, String> request);

    @FormUrlEncoded
    @POST("user_list")
    Call<JsonObject> getUserList(@FieldMap HashMap<String, String> request);

    @FormUrlEncoded
    @POST("add_answer")
    Call<JsonObject> addAnswer(@FieldMap HashMap<String, String> request);

    @FormUrlEncoded
    @POST("add_post_comment")
    Call<JsonObject> addVVComment(@FieldMap HashMap<String, String> request);

    @Multipart
    @POST("add_answer")
    Call<JsonObject> addAnswer(@PartMap HashMap<String, RequestBody> request,
                               @Part MultipartBody.Part[] imagesRequest);

    @FormUrlEncoded
    @POST("suggest_edit_post")
    Call<JsonObject> suggestEditPost(@FieldMap HashMap<String, String> request);

    @FormUrlEncoded
    @POST("flash_post")
    Call<JsonObject> flashPost(@FieldMap HashMap<String, String> request);

    @FormUrlEncoded
    @POST("recommended_post")
    Call<JsonObject> recommendedPost(@FieldMap HashMap<String, String> request);

    @FormUrlEncoded
    @POST("delete_post")
    Call<JsonObject> deletePost(@FieldMap HashMap<String, String> request);

    @FormUrlEncoded
    @POST("share_post")
    Call<JsonObject> sharePost(@FieldMap HashMap<String, String> request);

    @FormUrlEncoded
    @POST("decline_question")
    Call<JsonObject> declinePost(@FieldMap HashMap<String, String> request);

    @FormUrlEncoded
    @POST("declined_post_list")
    Call<JsonObject> declinedPostList(@FieldMap HashMap<String, String> request);

    @FormUrlEncoded
    @POST("download_post")
    Call<JsonObject> downloadPost(@FieldMap HashMap<String, String> request);

    @FormUrlEncoded
    @POST("get_post_by_id")
    Call<JsonObject> getPostDetails(@FieldMap HashMap<String, String> request);

    @FormUrlEncoded
    @POST("undo_decline_post")
    Call<JsonObject> undoDeclinedPost(@FieldMap HashMap<String, String> request);

    @FormUrlEncoded
    @POST("delete_decline_post")
    Call<JsonObject> deleteDeclinePost(@FieldMap HashMap<String, String> request);

    @FormUrlEncoded
    @POST("add_post_answer_comment")
    Call<JsonObject> addPostAnswerComment(@FieldMap HashMap<String, String> request);

    @FormUrlEncoded
    @POST("delete_post_answer_comment")
    Call<JsonObject> deletePostAnswerComment(@FieldMap HashMap<String, String> request);

    @FormUrlEncoded
    @POST("edit_post_answer_comment")
    Call<JsonObject> editPostAnswerComment(@FieldMap HashMap<String, String> request);

    @FormUrlEncoded
    @POST("logout")
    Call<JsonObject> logout(@FieldMap HashMap<String, String> request);

    @FormUrlEncoded
    @POST("trending_topics")
    Call<JsonObject> trendingTopics(@FieldMap HashMap<String, String> request);

    @FormUrlEncoded
    @POST("user_research_papers")
    Call<JsonObject> userResearchPaper(@FieldMap HashMap<String, String> request);

    @FormUrlEncoded
    @POST("bookmarked_post_list")
    Call<JsonObject> bookmarkedPostAnswerList(@FieldMap HashMap<String, String> request);

    @Multipart
    @POST("add_RP_post")
    Call<JsonObject> addResearchPaper(@PartMap HashMap<String, RequestBody> request,
                                      @Part MultipartBody.Part file);

    @Multipart
    @POST("add_article_post")
    Call<JsonObject> addArticlePost(@PartMap HashMap<String, RequestBody> request,
                                    @Part MultipartBody.Part[] file);

    @Multipart
    @POST("edit_post")
    Call<JsonObject> editPostForArticle(@PartMap HashMap<String, RequestBody> request,
                                        @Part MultipartBody.Part[] file);

    @Multipart
    @POST("edit_post")
    Call<JsonObject> editPostForOther(@PartMap HashMap<String, RequestBody> request,
                                      @Part MultipartBody.Part file);

    @Multipart
    @POST("add_VV_post")
    Call<JsonObject> addVVPost(
            @PartMap HashMap<String, RequestBody> request,
            @Part MultipartBody.Part file);

    @FormUrlEncoded
    @POST("vv_list")
    Call<JsonObject> audioVideoList(
            @Field("user_id") String userId,
            @Field("vv_type") String vvType);

    @FormUrlEncoded
    @POST("accept_reject_friend")
    Call<JsonObject> acceptRejectRequest(
            @Field("user_id") String userId,
            @Field("sender_id") String senderId,
            @Field("status") String vvType);

    @FormUrlEncoded
    @POST("friend_suggestion_list")
    Call<JsonObject> friendSuggestion(
            @Field("user_id") String userId,
            @Field("offset") String offset);

    @Multipart
    @POST("edit_answer")
    Call<JsonObject> editAnswer(
            @PartMap HashMap<String, RequestBody> request);

    @FormUrlEncoded
    @POST("delete_answer")
    Call<JsonObject> deleteAnswer(
            @Field("user_id") String userId,
            @Field("post_answers_id") String post_answers_id);

    @FormUrlEncoded
    @POST("request_to_expert")
    Call<JsonObject> requestToExpert(
            @Field("user_id") String userId,
            @Field("expert_id") String expert_id,
            @Field("post_id") String postId);

    @FormUrlEncoded
    @POST("post_report_abuse")
    Call<JsonObject> postReportAbuse(
            @Field("user_id") String userId,
            @Field("post_id") String postId,
            @Field("description") String description);

    @FormUrlEncoded
    @POST("advert_time_slot")
    Call<JsonObject> advertTimeSlot(
            @Field("user_id") String userId,
            @Field("offset") String offset);

    @FormUrlEncoded
    @POST("edit_post_comment")
    Call<JsonObject> editPostComment(
            @Field("user_id") String userId,
            @Field("comment_id") String comment_id,
            @Field("comment") String comment);

    @Multipart
    @POST("add_advert")
    Call<JsonObject> addAdvert(@PartMap HashMap<String, RequestBody> request,
                               @Part MultipartBody.Part[] file);

    @Multipart
    @POST("update_advert")
    Call<JsonObject> updateAdvert(@PartMap HashMap<String, RequestBody> request,
                                  @Part MultipartBody.Part[] file);

    @FormUrlEncoded
    @POST("advert_list")
    Call<JsonObject> advertList(
            @Field("user_id") String userId,
            @Field("offset") String offset);

    @FormUrlEncoded
    @POST("get_conversation_msg")
    Call<JsonObject> getConversationMsg(
            @Field("user_id") String userId,
            @Field("to_user_id") String toUserId,
            @Field("offset") String offset);

    @FormUrlEncoded
    @POST("get_conversation_list")
    Call<JsonObject> getConversationList(
            @Field("user_id") String userId);

    @FormUrlEncoded
    @POST("like_unlike_post_answer")
    Call<JsonObject> likeUnlikePostAnswer(
            @Field("user_id") String userId,
            @Field("post_id") String postId,
            @Field("post_answers_id") String postAnswersId,
            @Field("like_unlike") String likeUnlike
    );

    @FormUrlEncoded
    @POST("carryover_post")
    Call<JsonObject> carryoverPost(
            @Field("user_id") String userId,
            @Field("post_id") String postId,
            @Field("carryover_date") String postAnswersId);

    @FormUrlEncoded
    @POST("broadcast_post")
    Call<JsonObject> broadcastPost(
            @Field("user_id") String userId,
            @Field("post_id") String postId);

    @FormUrlEncoded
    @POST("wallet")
    Call<JsonObject> wallet(
            @Field("user_id") String userId,
            @Field("month") String month,
            @Field("year") String year);

    @FormUrlEncoded
    @POST("update_post_from_suggestion")
    Call<JsonObject> updatePostFromSuggestion(
            @Field("user_id") String userId,
            @Field("notification_id") String notificationId,
            @Field("status") String status);

    @FormUrlEncoded
    @POST("get_user_posts")
    Call<JsonObject> getUserPosts(@FieldMap HashMap<String, String> request);

    @Multipart
    @POST("expert_reply")
    Call<JsonObject> expertReply(@PartMap HashMap<String, RequestBody> request,
                                 @Part MultipartBody.Part voiceVideoReply);

    @FormUrlEncoded
    @POST("check_email_exists")
    Call<JsonObject> checkEmailExists(@Field("email") String email);


    @POST
    Call<JsonObject> aboutAppPrivacyTC(@Url String url);

}




