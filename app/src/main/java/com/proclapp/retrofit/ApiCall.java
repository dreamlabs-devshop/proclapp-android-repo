package com.proclapp.retrofit;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;

import androidx.annotation.NonNull;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.advert.AdvertDurationsModel;
import com.proclapp.chat.ChatModel;
import com.proclapp.chat.ConversationModel;
import com.proclapp.model.AllPostsModel;
import com.proclapp.model.AnswerModel;
import com.proclapp.model.CategoryModel;
import com.proclapp.model.FollowerFollowingModel;
import com.proclapp.model.NotificationAlertModel;
import com.proclapp.signup.StartupActivity;
import com.proclapp.utils.LogOutUtils;
import com.proclapp.voice.ExpertWalletModel;

import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.FieldMap;

public class ApiCall {
    //Do Not Change error code otherwise you will be in trouble
    public static final int OK = 200;
    public static final int NETWORK_ERROR = 9001;
    public static final int SERVER_ERROR = 9002;
    public static final int AUTH_FAILURE_ERROR = 9003;
    public static final int PARSE_ERROR = 9004;
    public static final int NO_CONNECTION_ERROR = 9005;
    public static final int TIME_OUT_ERROR = 9006;
    public static final int UNKNOWN_ERROR = 9007;
    private static APIService service;
    private ProgressDialog progressDialog;

    public static ApiCall getInstance() {
        if (service == null) {
            service = RestClient.getClient();
        }

        return new ApiCall();
    }

    /**
     * Get error message from error code
     *
     * @param activity  calling activity
     * @param errorCode
     * @param activity
     */
    private static String getErrorMessage(Activity activity, int errorCode) {
        switch (errorCode) {
            case NETWORK_ERROR:
                return activity.getString(R.string.network_error_occurred);
            case NO_CONNECTION_ERROR:
                return activity.getString(R.string.nonetwork);
            case TIME_OUT_ERROR:
                return activity.getString(R.string.timeout_error);
            default:
                return activity.getString(R.string.somethingWentWrong);
        }
    }

    public RequestBody getRequestBodyOfString(String string) {
        return RequestBody.create(MediaType.parse("multipart/form-data"), string);
    }

    /**
     * signup service
     *
     * @param activity
     * @param request
     * @param iApiCallback
     * @param showDialog
     */
    public void signup(final Activity activity, HashMap<String, String> request, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.signup(request);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            //Object type String
                            iApiCallback.onSuccess(response_msg);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * signin service
     *
     * @param activity
     * @param request
     * @param iApiCallback
     * @param showDialog
     */
    public void signin(final Activity activity, HashMap<String, String> request, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.signin(request);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            //Object type String
                            iApiCallback.onSuccess(resObject);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * update device token service
     *
     * @param activity
     * @param request
     * @param iApiCallback
     * @param showDialog
     */
    public void updateDeviceToken(final Activity activity, HashMap<String, String> request, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.updateDeviceToken(request);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            //Object type String
                            iApiCallback.onSuccess(resObject);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * updateCategoryToUserProfile service
     *
     * @param activity
     * @param request
     * @param iApiCallback
     * @param showDialog
     */
    public void updateCategoryToUserProfile(final Activity activity, HashMap<String, String> request, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.updateCategoryToUserProfile(request);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            //Object type String
                            iApiCallback.onSuccess(response_msg);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * changePassword service
     *
     * @param activity
     * @param request
     * @param iApiCallback
     * @param showDialog
     */
    public void changePassword(final Activity activity, HashMap<String, String> request, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.changePassword(request);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            //Object type String
                            iApiCallback.onSuccess(response_msg);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * getOtherUserProfile service
     *
     * @param activity
     * @param request
     * @param iApiCallback
     * @param showDialog
     */
    public void getOtherUserProfile(final Activity activity, HashMap<String, String> request, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.getOtherUserProfile(request);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            //Object type String
                            iApiCallback.onSuccess(resObject.getAsJsonObject("profile"));
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * getOtherUserProfile service
     *
     * @param activity
     * @param request
     * @param iApiCallback
     * @param showDialog
     */
    public void getProfile(final Activity activity, HashMap<String, String> request, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.getProfile(request);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            //Object type String
                            iApiCallback.onSuccess(resObject.getAsJsonObject("user_data"));
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }


    public void getUserPosts(final Activity activity, HashMap<String, String> request, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.getUserPosts(request);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }


                            ArrayList<AllPostsModel> tempAllPost = new Gson().fromJson(resObject.getAsJsonArray("detail").toString(),
                                    new TypeToken<List<AllPostsModel>>() {
                                    }.getType());

                            //Object type String
                            iApiCallback.onSuccess(tempAllPost);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }


    public void expertReply(final Activity activity, HashMap<String, RequestBody> request, String pathVoiceVideo, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.expertReply(request, getMultipartBody(pathVoiceVideo, "file"));
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }


                            //Object type String
                            iApiCallback.onSuccess(response_msg);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }


    public void checkEmailExists(final Activity activity, String email, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.checkEmailExists(email);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }


                            //Object type String
                            iApiCallback.onSuccess(response_msg);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * updateUserProfile service
     *
     * @param activity
     * @param request
     * @param iApiCallback
     * @param showDialog
     */
    public void updateUserProfile(final Activity activity, HashMap<String, RequestBody> request,
                                  String profileImagePath,
                                  String coverImagePath,
                                  final IApiCallback iApiCallback, final boolean showDialog) {


        if (showDialog) {
            showProgressDialog(activity);
        }

        ArrayList<MultipartBody.Part> imagesRequest = new ArrayList<>();
        if (StringUtils.isNotEmpty(profileImagePath))
            imagesRequest.add(getMultipartBody(profileImagePath, "profile_image"));

        if (StringUtils.isNotEmpty(coverImagePath))
            imagesRequest.add(getMultipartBody(coverImagePath, "cover_image"));

        Call<JsonObject> call = service.updateUserProfile(request, imagesRequest);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            //Object type String
                            iApiCallback.onSuccess(resObject.getAsJsonObject("user_data"));
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, "Failed to connect server , Please try after some time");
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * getFilteredList service
     *
     * @param activity
     * @param request
     * @param iApiCallback
     * @param showDialog
     */
    public void getFilteredList(final Activity activity, @FieldMap HashMap<String, String> request, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.getFilteredList(request);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity == null &&
                        activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }

                            iApiCallback.onSuccess(resObject);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }


    /**
     * getFilteredList service
     *
     * @param activity
     * @param request
     * @param iApiCallback
     * @param showDialog
     */
    public void getVVList(final Activity activity, @FieldMap HashMap<String, String> request, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.getVVList(request);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }

                            ArrayList<AllPostsModel> tempFilteredList = new Gson().fromJson(resObject.getAsJsonArray("vv_list").toString(),
                                    new TypeToken<List<AllPostsModel>>() {
                                    }.getType());

                            iApiCallback.onSuccess(tempFilteredList);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * getanswered question list service
     *
     * @param activity
     * @param request
     * @param iApiCallback
     * @param showDialog
     */
    public void getAnsweredQuestionList(final Activity activity, @FieldMap HashMap<String, String> request, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.getAnsweredQuestionList(request);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }

                            ArrayList<AllPostsModel> tempFilteredList = new Gson().fromJson(resObject.getAsJsonArray("post_list").toString(),
                                    new TypeToken<List<AllPostsModel>>() {
                                    }.getType());

                            iApiCallback.onSuccess(tempFilteredList);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * get unanswered question list service
     *
     * @param activity
     * @param request
     * @param iApiCallback
     * @param showDialog
     */
    public void getUnAnsweredQuestionList(final Activity activity, @FieldMap HashMap<String, String> request, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.getUnAnsweredQuestionList(request);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }

                            ArrayList<AllPostsModel> tempFilteredList = new Gson().fromJson(resObject.getAsJsonArray("post_list").toString(),
                                    new TypeToken<List<AllPostsModel>>() {
                                    }.getType());

                            iApiCallback.onSuccess(tempFilteredList);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * get DiscussedArticle list service
     *
     * @param activity
     * @param request
     * @param iApiCallback
     * @param showDialog
     */
    public void getDiscussedArticleList(final Activity activity, @FieldMap HashMap<String, String> request, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.getDiscussedArticleList(request);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }

                            ArrayList<AllPostsModel> tempFilteredList = new Gson().fromJson(resObject.getAsJsonArray("posts").toString(),
                                    new TypeToken<List<AllPostsModel>>() {
                                    }.getType());

                            iApiCallback.onSuccess(tempFilteredList);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * get NonDiscussedArticle list service
     *
     * @param activity
     * @param request
     * @param iApiCallback
     * @param showDialog
     */
    public void getNonDiscussedArticleList(final Activity activity, @FieldMap HashMap<String, String> request, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.getNonDiscussedArticleList(request);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }

                            ArrayList<AllPostsModel> tempFilteredList = new Gson().fromJson(resObject.getAsJsonArray("posts").toString(),
                                    new TypeToken<List<AllPostsModel>>() {
                                    }.getType());

                            iApiCallback.onSuccess(tempFilteredList);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * getNotificationList service
     *
     * @param activity
     * @param request
     * @param iApiCallback
     * @param showDialog
     */
    public void getNotificationList(final Activity activity, HashMap<String, String> request, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.getNotificationList(request);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            //Object type String
                            iApiCallback.onSuccess(resObject.getAsJsonArray("notification_list"));
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * getNotificationAlertType service
     *
     * @param activity
     * @param userId
     * @param iApiCallback
     * @param showDialog
     */
    public void getNotificationAlertType(final Activity activity, String userId, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.getNotificationAlertType(userId);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }

                            ArrayList<NotificationAlertModel> tempNotificationAlertList = new Gson().fromJson(resObject.getAsJsonObject("notification_type").getAsJsonArray("notifications").toString(),
                                    new TypeToken<List<NotificationAlertModel>>() {
                                    }.getType());

                            //Object type String
                            iApiCallback.onSuccess(tempNotificationAlertList);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * updateNotificationStatus service
     *
     * @param activity
     * @param request
     * @param iApiCallback
     * @param showDialog
     */
    public void updateNotificationStatus(final Activity activity, HashMap<String, String> request, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.updateNotificationStatus(request);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            //Object type String
                            iApiCallback.onSuccess(response_msg);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * changeFollowUnfollowStatus service
     *
     * @param activity
     * @param request
     * @param iApiCallback
     * @param showDialog
     */
    public void changeFollowUnfollowStatus(final Activity activity, HashMap<String, String> request, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.changeFollowUnfollowStatus(request);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            //Object type String
                            iApiCallback.onSuccess(response_msg);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * friendUnfriend service
     *
     * @param activity
     * @param request
     * @param iApiCallback
     * @param showDialog
     */
    public void friendUnfriend(final Activity activity, HashMap<String, String> request, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.friendUnfriend(request);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            //Object type String
                            iApiCallback.onSuccess(response_msg);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * bookmarkunbookmark service
     *
     * @param activity
     * @param request
     * @param iApiCallback
     * @param showDialog
     */
    public void bookmarkUnbookmark(final Activity activity, HashMap<String, String> request, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.bookmarkUnbookmark(request);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            //Object type String
                            iApiCallback.onSuccess(response_msg);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * bookmark unbookmark post answer service
     *
     * @param activity
     * @param request
     * @param iApiCallback
     * @param showDialog
     */
    public void bookmarkUnbookmarkPostAnswer(final Activity activity, HashMap<String, String> request, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.bookmarkUnbookmarkPostAnswer(request);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            //Object type String
                            iApiCallback.onSuccess(response_msg);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * friendUnfriend service
     *
     * @param activity
     * @param request
     * @param iApiCallback
     * @param showDialog
     */
    public void likeUnlike(final Activity activity, HashMap<String, String> request, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.likeUnlike(request);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            //Object type String
                            iApiCallback.onSuccess(response_msg);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * postRead service
     *
     * @param activity
     * @param request
     * @param iApiCallback
     * @param showDialog
     */
    public void postRead(final Activity activity, HashMap<String, String> request, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.postRead(request);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            //Object type String
                            iApiCallback.onSuccess(response_msg);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * post upvote_selected downvote service
     *
     * @param activity
     * @param request
     * @param iApiCallback
     * @param showDialog
     */
    public void postUpVoteDownVote(final Activity activity, HashMap<String, String> request, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.postUpVoteDownVote(request);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            //Object type String
                            iApiCallback.onSuccess(response_msg);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    public void postCommentUpVoteDownVote(final Activity activity, HashMap<String, String> request, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.postCommentUpVoteDownVote(request);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            //Object type String
                            iApiCallback.onSuccess(response_msg);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    public void postCommentLikeUnlike(final Activity activity, HashMap<String, String> request, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.postCommentLikeUnlike(request);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            //Object type String
                            iApiCallback.onSuccess(response_msg);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * getFriendsList service
     *
     * @param activity
     * @param userId
     * @param iApiCallback
     * @param showDialog
     */
    public void getFriendsList(final Activity activity, String userId, String offset, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.getFriendsList(userId, offset);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            ArrayList<FollowerFollowingModel> tempFriendList = new Gson().fromJson(resObject.getAsJsonArray("friends_list").toString(),
                                    new TypeToken<List<FollowerFollowingModel>>() {
                                    }.getType());
                            iApiCallback.onSuccess(tempFriendList);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * getPosts service
     *
     * @param activity
     * @param userId
     * @param iApiCallback
     * @param showDialog
     */
    public Call<JsonObject> getPosts(final Activity activity, String userId, String searchChar, String offset, final IApiCallback iApiCallback, final boolean showDialog) {

        if (activity.isFinishing())
            return null;

        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.getPosts(userId, searchChar, offset);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            ArrayList<AllPostsModel> tempPostsList = new Gson().fromJson(resObject.getAsJsonArray("posts_list").toString(),
                                    new TypeToken<List<AllPostsModel>>() {
                                    }.getType());
                            iApiCallback.onSuccess(resObject);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });

        return call;
    }

    /**
     * getExpertList service
     *
     * @param activity
     * @param userId
     * @param iApiCallback
     * @param showDialog
     */
    public void getExpertList(final Activity activity, String userId, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.getExpertList(userId);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            //Object type String
                            iApiCallback.onSuccess(resObject.getAsJsonArray("expert_list"));
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * getFollowerList service
     *
     * @param activity
     * @param userId
     * @param iApiCallback
     * @param showDialog
     */
    public void getFollowerList(final Activity activity, String userId, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.getFollowerList(userId);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            ArrayList<FollowerFollowingModel> tempFollowerList = new Gson().fromJson(resObject.getAsJsonArray("follower_list").toString(),
                                    new TypeToken<List<FollowerFollowingModel>>() {
                                    }.getType());

                            iApiCallback.onSuccess(tempFollowerList);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * getFollowingList service
     *
     * @param activity
     * @param userId
     * @param iApiCallback
     * @param showDialog
     */
    public void getFollowingList(final Activity activity, String userId, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.getFollowingList(userId);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }

                            ArrayList<FollowerFollowingModel> tempFollowingList = new Gson().fromJson(resObject.getAsJsonArray("following_list").toString(),
                                    new TypeToken<List<FollowerFollowingModel>>() {
                                    }.getType());


                            iApiCallback.onSuccess(tempFollowingList);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * aboutAppPrivacyTC service
     *
     * @param activity
     * @param url
     * @param iApiCallback
     * @param showDialog
     */
    public void aboutAppPrivacyTC(final Activity activity, final String url, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.aboutAppPrivacyTC(url);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            //Object type String
                            iApiCallback.onSuccess(resObject.get(url).getAsString());
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * Add Question answer service
     *
     * @param activity
     * @param request
     * @param iApiCallback
     * @param showDialog
     */
    public void addQuestionAnswerPost(final Activity activity, HashMap<String, String> request, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.addQuestionAnswer(request);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            //Object type String
                            iApiCallback.onSuccess(response_msg);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * Add Article Post service
     *
     * @param activity
     * @param request
     * @param iApiCallback
     * @param showDialog
     */
    public void addArticlePost(final Activity activity, HashMap<String, RequestBody> request,
                               ArrayList<String> file_path, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.addArticlePost(request, getMultipartBodyArrayList(file_path, "post_img"));
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            //Object type String
                            iApiCallback.onSuccess(response_msg);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * Add Research Paper service
     *
     * @param activity
     * @param request
     * @param iApiCallback
     * @param showDialog
     */
    public void addResearchPaper(final Activity activity, HashMap<String, RequestBody> request, String file_path, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.addResearchPaper(request, getMultipartBody(file_path, "file"));
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            //Object type String
                            iApiCallback.onSuccess(response_msg);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * getOtherUserProfile service
     *
     * @param activity
     * @param iApiCallback
     * @param showDialog
     */
    public void getCategories(final Activity activity, HashMap<String, String> request, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.getCategories(request);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            ArrayList<CategoryModel> tempCategoriesList = new Gson().fromJson(resObject.getAsJsonArray("interested_category").toString(),
                                    new TypeToken<List<CategoryModel>>() {
                                    }.getType());
                            iApiCallback.onSuccess(tempCategoriesList);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * check social id service
     *
     * @param activity
     * @param request
     * @param iApiCallback
     * @param showDialog
     */
    public void checksocialId(final Activity activity, HashMap<String, String> request, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.checkSocialId(request);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            //Object type String
                            iApiCallback.onSuccess(resObject);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * check sign in with social service
     *
     * @param activity
     * @param request
     * @param iApiCallback
     * @param showDialog
     */
    public void signInWithSocial(final Activity activity, HashMap<String, String> request, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.signInWithSicial(request);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {

                            iApiCallback.onSuccess(resObject);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * getanswerlist service
     *
     * @param activity
     * @param request
     * @param iApiCallback
     * @param showDialog
     */
    public void getAnswerList(final Activity activity, HashMap<String, String> request, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.getAnswerList(request);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            ArrayList<AnswerModel> tempPostsList = new Gson().fromJson(resObject.getAsJsonArray("answer_list").toString(),
                                    new TypeToken<List<AnswerModel>>() {
                                    }.getType());
                            iApiCallback.onSuccess(tempPostsList);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * getVVAnswerList service
     *
     * @param activity
     * @param request
     * @param iApiCallback
     * @param showDialog
     */
    public void getVVAnswerList(final Activity activity, HashMap<String, String> request, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.getVVAnswerList(request);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            ArrayList<AnswerModel> tempPostsList = new Gson().fromJson(resObject.getAsJsonArray("answer_list").toString(),
                                    new TypeToken<List<AnswerModel>>() {
                                    }.getType());
                            iApiCallback.onSuccess(tempPostsList);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * getanswerdetails service
     *
     * @param activity
     * @param request
     * @param iApiCallback
     * @param showDialog
     */
    public void getAnswerDetails(final Activity activity, HashMap<String, String> request, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.getAnswerDetails(request);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onSuccess(resObject);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * Add Answer service
     *
     * @param activity
     * @param request
     * @param iApiCallback
     * @param showDialog
     */
    public void addAnswerPost(final Activity activity, HashMap<String, String> request, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.addAnswer(request);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            //Object type String
                            iApiCallback.onSuccess(response_msg);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    public void addVVComment(final Activity activity, HashMap<String, String> request, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.addVVComment(request);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            //Object type String
                            iApiCallback.onSuccess(response_msg);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * Add Answer service
     *
     * @param activity
     * @param request
     * @param iApiCallback
     * @param showDialog
     */
    public void addAnswerWithImage(final Activity activity, HashMap<String, RequestBody> request, ArrayList<String> imagePaths, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.addAnswer(request, getMultipartBodyArrayList(imagePaths, "image"));
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            //Object type String
                            iApiCallback.onSuccess(response_msg);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * Suggest edit service
     *
     * @param activity
     * @param request
     * @param iApiCallback
     * @param showDialog
     */
    public void suggestEdit(final Activity activity, HashMap<String, String> request, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.suggestEditPost(request);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            //Object type String
                            iApiCallback.onSuccess(response_msg);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * get flash post service
     *
     * @param activity
     * @param request
     * @param iApiCallback
     * @param showDialog
     */
    public void flashPostToUser(final Activity activity, final HashMap<String, String> request, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.flashPost(request);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }

                            iApiCallback.onSuccess(response_msg);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * get flash post service
     *
     * @param activity
     * @param request
     * @param iApiCallback
     * @param showDialog
     */
    public void recommendedPost(final Activity activity, final HashMap<String, String> request, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.recommendedPost(request);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }

                            iApiCallback.onSuccess(response_msg);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * delete post service
     *
     * @param activity
     * @param request
     * @param iApiCallback
     * @param showDialog
     */
    public void deletePost(final Activity activity, final HashMap<String, String> request, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.deletePost(request);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }

                            iApiCallback.onSuccess(response_msg);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * share post service
     *
     * @param activity
     * @param request
     * @param iApiCallback
     * @param showDialog
     */
    public void sharePost(final Activity activity, final HashMap<String, String> request, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.sharePost(request);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }

                            iApiCallback.onSuccess(response_msg);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * decline post service
     *
     * @param activity
     * @param request
     * @param iApiCallback
     * @param showDialog
     */
    public void declinePost(final Activity activity, final HashMap<String, String> request, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.declinePost(request);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }

                            iApiCallback.onSuccess(response_msg);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * declined post list service
     *
     * @param activity
     * @param request
     * @param iApiCallback
     * @param showDialog
     */
    public void declinedPostList(final Activity activity, final HashMap<String, String> request, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.declinedPostList(request);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }

                            ArrayList<AllPostsModel> declinedPostList = new Gson().fromJson(resObject.getAsJsonArray("post_list").toString(),
                                    new TypeToken<List<AllPostsModel>>() {
                                    }.getType());

                            iApiCallback.onSuccess(declinedPostList);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * download_post service
     *
     * @param activity
     * @param request
     * @param iApiCallback
     * @param showDialog
     */
    public void downloadPost(final Activity activity, final HashMap<String, String> request, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.downloadPost(request);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }

                            iApiCallback.onSuccess(response_msg);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * download_post service
     *
     * @param activity
     * @param request
     * @param iApiCallback
     * @param showDialog
     */
    public void getPostDetails(final Activity activity, final HashMap<String, String> request, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.getPostDetails(request);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }

                            AllPostsModel model = new Gson().fromJson(resObject.getAsJsonObject("detail").toString(), AllPostsModel.class);

                            iApiCallback.onSuccess(model);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * undo declined post service
     *
     * @param activity
     * @param request
     * @param iApiCallback
     * @param showDialog
     */
    public void undoDeclinedPostList(final Activity activity, final HashMap<String, String> request, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.undoDeclinedPost(request);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }

                            iApiCallback.onSuccess(response_msg);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * Delete an already decline post service
     *
     * @param activity
     * @param request
     * @param iApiCallback
     * @param showDialog
     */
    public void deleteDeclinedPost(final Activity activity, final HashMap<String, String> request, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.deleteDeclinePost(request);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }

                            iApiCallback.onSuccess(response_msg);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * add post answer comment service
     *
     * @param activity
     * @param request
     * @param iApiCallback
     * @param showDialog
     */
    public void addPostAnswerComment(final Activity activity, final HashMap<String, String> request, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.addPostAnswerComment(request);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }

                            iApiCallback.onSuccess(response_msg);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * delete post answer comment service
     *
     * @param activity
     * @param request
     * @param iApiCallback
     * @param showDialog
     */
    public void deletePostAnswerComment(final Activity activity, final HashMap<String, String> request, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.deletePostAnswerComment(request);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }

                            iApiCallback.onSuccess(response_msg);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }


    /**
     * delete post answer comment service
     *
     * @param activity
     * @param request
     * @param iApiCallback
     * @param showDialog
     */
    public void editPostAnswerComment(final Activity activity, final HashMap<String, String> request, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.editPostAnswerComment(request);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }

                            iApiCallback.onSuccess(response_msg);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * trending topics service
     *
     * @param activity
     * @param request
     * @param iApiCallback
     * @param showDialog
     */
    public void trendingTopics(final Activity activity, final HashMap<String, String> request, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.trendingTopics(request);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }

                            ArrayList<CategoryModel> trendingTopicList = new Gson().fromJson(resObject.getAsJsonArray("topics").toString(),
                                    new TypeToken<List<CategoryModel>>() {
                                    }.getType());

                            iApiCallback.onSuccess(trendingTopicList);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * user research paper service
     *
     * @param activity
     * @param request
     * @param iApiCallback
     * @param showDialog
     */
    public void userResearchPaper(final Activity activity, final HashMap<String, String> request, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.userResearchPaper(request);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }

                            ArrayList<AllPostsModel> userResearchPaperList = new Gson().fromJson(resObject.getAsJsonArray("posts").toString(),
                                    new TypeToken<List<AllPostsModel>>() {
                                    }.getType());

                            iApiCallback.onSuccess(userResearchPaperList);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * bookmarkedpostanswerlist service
     *
     * @param activity
     * @param request
     * @param iApiCallback
     * @param showDialog
     */
    public void bookmarkedPostAnsweredList(final Activity activity, final HashMap<String, String> request, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.bookmarkedPostAnswerList(request);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }

                            iApiCallback.onSuccess(resObject);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * edit post for article service
     *
     * @param activity
     * @param request
     * @param iApiCallback
     * @param showDialog
     */
    public void editPostForArticle(final Activity activity, HashMap<String, RequestBody> request,
                                   ArrayList<String> file_path, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.editPostForArticle(request, getMultipartBodyArrayList(file_path, "post_img"));
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            //Object type String
                            iApiCallback.onSuccess(response_msg);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * edit post other service
     *
     * @param activity
     * @param request
     * @param iApiCallback
     * @param showDialog
     */
    public void editPostOther(final Activity activity, HashMap<String, RequestBody> request,
                              String file_path, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.editPostForOther(request, getMultipartBody(file_path, "file"));
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            //Object type String
                            iApiCallback.onSuccess(response_msg);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * get userlist service
     *
     * @param activity
     * @param request
     * @param iApiCallback
     * @param showDialog
     */
    public void getUserList(final Activity activity, HashMap<String, String> request, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.getUserList(request);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            ArrayList<FollowerFollowingModel> tempPostsList = new Gson().fromJson(resObject.getAsJsonArray("users").toString(),
                                    new TypeToken<List<FollowerFollowingModel>>() {
                                    }.getType());
                            iApiCallback.onSuccess(tempPostsList);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * forgotPassword service
     *
     * @param activity
     * @param email
     * @param iApiCallback
     * @param showDialog
     */
    public void forgotPassword(final Activity activity, String email, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.forgotPassword(email);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }

                            iApiCallback.onSuccess(response_msg);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * add Voice and Video Post
     *
     * @param activity
     * @param request
     * @param filePath
     * @param iApiCallback
     * @param showDialog
     */
    public void addVVPost(final Activity activity, HashMap<String, RequestBody> request, String filePath, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.addVVPost(request, getAudioMultipartBody(filePath, "file"));
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }

                            iApiCallback.onSuccess(response_msg);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * audioVideoList Post
     *
     * @param activity
     * @param userId
     * @param vvType
     * @param iApiCallback
     * @param showDialog
     */
    public void audioVideoList(final Activity activity, String userId, String vvType, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.audioVideoList(userId, vvType);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }

                            ArrayList<AllPostsModel> tempAudioVideoPostsList = new Gson().fromJson(resObject.getAsJsonArray("vv_list").toString(),
                                    new TypeToken<List<AllPostsModel>>() {
                                    }.getType());
                            iApiCallback.onSuccess(tempAudioVideoPostsList);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * acceptRejectRequest
     *
     * @param activity
     * @param userId
     * @param senderId
     * @param status
     * @param iApiCallback
     * @param showDialog
     */
    public void acceptRejectRequest(final Activity activity, String userId, String senderId, String status, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.acceptRejectRequest(userId, senderId, status);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }


                            iApiCallback.onSuccess(response_msg);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * friendSuggestion
     *
     * @param activity
     * @param userId
     * @param offset
     * @param iApiCallback
     * @param showDialog
     */
    public void friendSuggestion(final Activity activity, String userId, String offset, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.friendSuggestion(userId, offset);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }


                            iApiCallback.onSuccess(resObject);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * Add Question answer service
     *
     * @param activity
     * @param request
     * @param iApiCallback
     * @param showDialog
     */
    public void logout(final Activity activity, HashMap<String, String> request, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.logout(request);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            //Object type String
                            iApiCallback.onSuccess(response_msg);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * edit Answer service
     *
     * @param activity
     * @param request
     * @param iApiCallback
     * @param showDialog
     */
    public void editAnswer(final Activity activity, @FieldMap HashMap<String, RequestBody> request, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.editAnswer(request);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            //Object type String
                            iApiCallback.onSuccess(response_msg);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * delete Answer service
     *
     * @param activity
     * @param userId
     * @param postAnswerId
     * @param iApiCallback
     * @param showDialog
     */
    public void deleteAnswer(final Activity activity, String userId, String postAnswerId, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.deleteAnswer(userId, postAnswerId);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            //Object type String
                            iApiCallback.onSuccess(response_msg);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * requestToExpert service
     *
     * @param activity
     * @param userId
     * @param postId
     * @param expert_id
     * @param iApiCallback
     * @param showDialog
     */
    public void requestToExpert(final Activity activity, String userId, String postId, String expert_id, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.requestToExpert(userId, expert_id, postId);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            //Object type String
                            iApiCallback.onSuccess(response_msg);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * postReportAbuse service
     *
     * @param activity
     * @param userId
     * @param postId
     * @param description
     * @param iApiCallback
     * @param showDialog
     */
    public void postReportAbuse(final Activity activity, String userId, String postId, String description, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.postReportAbuse(userId, postId, description);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            //Object type String
                            iApiCallback.onSuccess(response_msg);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * advertTimeSlot service
     *
     * @param activity
     * @param userId
     * @param offset
     * @param iApiCallback
     * @param showDialog
     */
    public void advertTimeSlot(final Activity activity, String userId, String offset, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.advertTimeSlot(userId, offset);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }

                            ArrayList<AdvertDurationsModel> tempAdvertDurationsModelList = new Gson().fromJson(resObject.getAsJsonArray("slot_list").toString(),
                                    new TypeToken<List<AdvertDurationsModel>>() {
                                    }.getType());

                            iApiCallback.onSuccess(tempAdvertDurationsModelList);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * editPostComment service
     *
     * @param activity
     * @param userId
     * @param comment_id
     * @param comment
     * @param iApiCallback
     * @param showDialog
     */
    public void editPostComment(final Activity activity, String userId, String comment_id, String comment, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.editPostComment(userId, comment_id, comment);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }

                            iApiCallback.onSuccess(response_msg);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * addAdvert service
     *
     * @param activity
     * @param request
     * @param imagesPath
     * @param iApiCallback
     * @param showDialog
     */
    public void addAdvert(final Activity activity, HashMap<String, RequestBody> request, ArrayList<String> imagesPath, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.addAdvert(request, getMultipartBodyArrayList(imagesPath, "post_images"));
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("advert_id")) {
                                response_msg = resObject.get("advert_id").getAsString();
                            }

                            iApiCallback.onSuccess(response_msg);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }


    public void updateAdvert(final Activity activity, HashMap<String, RequestBody> request, ArrayList<String> imagesPath, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.updateAdvert(request, getMultipartBodyArrayList(imagesPath, "post_images"));
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }

                            iApiCallback.onSuccess(response_msg);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * advertList service
     *
     * @param activity
     * @param userId
     * @param offset
     * @param iApiCallback
     * @param showDialog
     */
    public void advertList(final Activity activity, String userId, String offset, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.advertList(userId, offset);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }

                            ArrayList<AllPostsModel> tempAdvertList = new Gson().fromJson(resObject.getAsJsonArray("advert_list").toString(),
                                    new TypeToken<List<AllPostsModel>>() {
                                    }.getType());

                            iApiCallback.onSuccess(tempAdvertList);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * advertList service
     *
     * @param activity
     * @param userId
     * @param offset
     * @param iApiCallback
     * @param showDialog
     */
    public void getConversationMsg(final Activity activity, String userId, String toUserId, String offset, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.getConversationMsg(userId, toUserId, offset);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }

                            ArrayList<ChatModel> tempList = new Gson().fromJson(resObject.getAsJsonArray("conversation_msgs").toString(),
                                    new TypeToken<List<ChatModel>>() {
                                    }.getType());

                            iApiCallback.onSuccess(tempList);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    /**
     * getConversationList service
     *
     * @param activity
     * @param userId
     * @param iApiCallback
     * @param showDialog
     */
    public void getConversationList(final Activity activity, String userId, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.getConversationList(userId);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }

                            ArrayList<ConversationModel> tempList = new Gson().fromJson(resObject.getAsJsonArray("conversation_list").toString(),
                                    new TypeToken<List<ConversationModel>>() {
                                    }.getType());

                            iApiCallback.onSuccess(tempList);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    public void likeUnlikePostAnswer(final Activity activity, String userId, String postId, String postAnswersId, String likeUnlike, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.likeUnlikePostAnswer(userId, postId,
                postAnswersId,
                likeUnlike);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }

                            iApiCallback.onSuccess(response_msg);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }


    public void carryoverPost(final Activity activity, String userId, String postId, String carryOverDate, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.carryoverPost(userId, postId, carryOverDate);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }

                            iApiCallback.onSuccess(response_msg);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    public void broadCastPost(final Activity activity, String userId, String postId, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.broadcastPost(userId, postId);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }

                            iApiCallback.onSuccess(response_msg);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    public void wallet(final Activity activity, String userId, String month, String year, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.wallet(userId, month, year);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }


                            ExpertWalletModel model = new Gson().fromJson(resObject.getAsJsonObject("detail"), ExpertWalletModel.class);

                            iApiCallback.onSuccess(model);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    public void updatePostFromSuggestion(final Activity activity, String userId, String notificationId, String status, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.updatePostFromSuggestion(userId, notificationId, status);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }

                            iApiCallback.onSuccess(response_msg);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, activity.getString(R.string.failed_to_connect));
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }


    /**
     * show a progress dialog
     *
     * @param activity
     */
    private void showProgressDialog(Activity activity) {
        if (activity.isFinishing())
            return;

        progressDialog = new ProgressDialog(activity);
//        progressDialog.setIndeterminateDrawable(ContextCompat.getDrawable(activity, R.drawable.progress_drawable));
        progressDialog.setMessage(activity.getResources().getString(R.string.please_wait));
        progressDialog.setCancelable(true);
        progressDialog.setCanceledOnTouchOutside(false);

        progressDialog.show();
    }

    /**
     * get json object with response
     *
     * @param object
     * @return
     */
    private JsonObject getResponse(JsonObject object, final Activity activity) {

        if (object.get("response").getAsJsonArray().get(0).getAsJsonObject().has("screen_code")) {
            if (object.get("response").getAsJsonArray().get(0).getAsJsonObject().get("screen_code").getAsString().equalsIgnoreCase("1001")) {

                HashMap<String, String> requests = new HashMap<>();
                requests.put("user_id", AppClass.preferences.getUserId());
                requests.put("device_token", AppClass.preferences.getFCMToken());

                Call<JsonObject> call = service.logout(requests);
                call.enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {

                        try {
                            if (response.isSuccessful()) {
                                LogOutUtils.LogoutUser(activity);
                                Intent intent = new Intent(activity, StartupActivity.class);
                                activity.startActivity(intent);
                            } else {
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {

                    }
                });

                return null;
            } else {

                try {
                    return object.get("response").getAsJsonArray().get(0).getAsJsonObject();
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        } else {
            try {
                return object.get("response").getAsJsonArray().get(0).getAsJsonObject();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
    }


    /**
     * returns a multipart body for given file and key
     *
     * @param filePath path of file
     * @param key      a name of param to be passed in service
     * @return
     */
    private MultipartBody.Part getMultipartBody(String filePath, String key) {
        MultipartBody.Part bodyImage = null;
        if (StringUtils.isNotEmpty(filePath)) {
            File file = new File(filePath);
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            bodyImage = MultipartBody.Part.createFormData(key, file.getName(), requestFile);
        }
        return bodyImage;
    }

    /**
     * returns a multipart body for given file and key
     *
     * @param filePath path of file
     * @param key      a name of param to be passed in service
     * @return
     */
    private MultipartBody.Part getAudioMultipartBody(String filePath, String key) {
        MultipartBody.Part bodyImage = null;
        if (StringUtils.isNotEmpty(filePath)) {
            File file = new File(filePath);
            RequestBody requestFile = RequestBody.create(MediaType.parse("audio/mp3"), file);
            bodyImage = MultipartBody.Part.createFormData(key, file.getName(), requestFile);
        }
        return bodyImage;
    }

    /**
     * @param imagePathList array of multilepath
     * @param key           a name of param to be passed in service
     * @return
     */
    private MultipartBody.Part[] getMultipartBodyArrayList(ArrayList<String> imagePathList, String key) {

        MultipartBody.Part[] imagesArrayParts = new MultipartBody.Part[imagePathList.size()];

        for (int i = 0; i < imagePathList.size(); i++) {
            Log.d("", "requestUploadSurvey: " + i + "  " + imagePathList.get(i));
            File file = new File(imagePathList.get(i));
            RequestBody surveyBody = RequestBody.create(MediaType.parse("image/*"), file);
            imagesArrayParts[i] = MultipartBody.Part.createFormData(key + "[" + i + "]", file.getName(), surveyBody);
        }

        return imagesArrayParts;

    }

}
