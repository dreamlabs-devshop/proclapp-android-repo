package com.proclapp.retrofit;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParser;

import okhttp3.internal.platform.Platform;
import okhttp3.logging.HttpLoggingInterceptor;

import static android.util.Log.INFO;
import static android.util.Log.WARN;

public class PrettyLogger implements HttpLoggingInterceptor.Logger {
    private Gson mGson = new GsonBuilder().setPrettyPrinting().create();
    private JsonParser mJsonParser = new JsonParser();

    @Override
    public void log(String message) {
        String trimMessage = message.trim();
        if ((trimMessage.startsWith("{") && trimMessage.endsWith("}"))
                || (trimMessage.startsWith("[") && trimMessage.endsWith("]"))) {
            try {
                String prettyJson = mGson.toJson(mJsonParser.parse(message));
                Platform.get().log(INFO, prettyJson, null);
            } catch (Exception e) {
                Platform.get().log(WARN, message, e);
            }
        } else {
            Platform.get().log(INFO, message, null);
        }
    }
}
