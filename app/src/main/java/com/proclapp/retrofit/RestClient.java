package com.proclapp.retrofit;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.proclapp.AppClass;
import com.proclapp.BuildConfig;

import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestClient {

    /*developement url*/
    public static final String BASE_URL = "https://www.proclapp.com/ws/v1/api/";

    private static APIService apiRestInterfaces;

    public static APIService getClient() {


        final OkHttpClient.Builder okHttpClient = new OkHttpClient.Builder();
        okHttpClient.readTimeout(90, TimeUnit.SECONDS).connectTimeout(90, TimeUnit.SECONDS);

        okHttpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request.Builder builder = chain.request().newBuilder();
                if (StringUtils.isNotEmpty(AppClass.preferences.getToken())) {
                    builder.addHeader("Authorization", AppClass.preferences.getToken());
                }
                return chain.proceed(builder.build());
            }
        });

        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor(new PrettyLogger());
            interceptor.level(HttpLoggingInterceptor.Level.BODY);
            okHttpClient.addInterceptor(interceptor);
        }

        Gson gson = new GsonBuilder()
                .setLenient()
                .setPrettyPrinting()
                .create();

        if (apiRestInterfaces == null) {
            Retrofit client = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(okHttpClient.build())
                    .build();
            apiRestInterfaces = client.create(APIService.class);
        }
        return apiRestInterfaces;
    }


}
