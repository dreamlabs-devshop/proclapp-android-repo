package com.proclapp.root;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import android.util.Log;

import com.proclapp.R;


public class BaseContainerFragment extends Fragment {

	//public Fragment mContent;

	public void replaceFragment(Fragment fragment, boolean addToBackStack) {
		//mContent=fragment;
		FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
		if (addToBackStack) {
			transaction.addToBackStack(null);
		}
		transaction.replace(R.id.container_framelayout, fragment);
		//transaction.commit();
		transaction.commitAllowingStateLoss();
		getChildFragmentManager().executePendingTransactions();
	}
	
	public boolean popFragment() {
		Log.e("BaseContainerFragment", "pop fragment: " + getChildFragmentManager().getBackStackEntryCount());
		boolean isPop = false;
		if (getChildFragmentManager().getBackStackEntryCount() > 0) {
			isPop = true;
			getChildFragmentManager().popBackStack();
		}
		return isPop;
	}


	public int getBackStackEntryCount(){
		int entryCount= getChildFragmentManager().getBackStackEntryCount();
		return entryCount;
	}

	public boolean popAllFragment() {
		Log.e("BaseContainerFragment", "pop fragment: " + getChildFragmentManager().getBackStackEntryCount());
		boolean isPop = false;
		if (getChildFragmentManager().getBackStackEntryCount() > 0) {
			for(int i=0;i<getChildFragmentManager().getBackStackEntryCount();i++) {
				getChildFragmentManager().popBackStack();
			}
			isPop = true;
		}
		return isPop;
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
	}

	@Override
	public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
		super.onViewStateRestored(savedInstanceState);


	}
}
