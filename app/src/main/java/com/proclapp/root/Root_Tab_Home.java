package com.proclapp.root;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.proclapp.R;
import com.proclapp.home.HomeActivity;
import com.proclapp.home.fragment.HomeFragment;

@SuppressLint("ValidFragment")
public class Root_Tab_Home extends BaseContainerFragment {

    // ===========================================================
    // Widgets
    // ===========================================================
    View tabFragmentView;
    // ===========================================================
    // Methods
    // ===========================================================
    HomeActivity homeActivity;
    onTabClickListener tabClickListener;
    // ===========================================================
    // Constructors
    // ===========================================================
    // ===========================================================
    // Fields/Variables
    // ===========================================================
    private boolean mIsViewInited;

    /*  public Root_Tab_Home(onTabClickListener tabClickListener) {
          this.tabClickListener = tabClickListener;
      }
  */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        homeActivity = (HomeActivity) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        tabFragmentView = inflater.inflate(R.layout.container_fragment, container, false);
        Log.e("Root_Tab_home", "Root_Tab_home 1 oncreateview");
        homeActivity.currentTabPosition = 0;
        Log.d("Current tab", String.valueOf(homeActivity.currentTabPosition));
        return tabFragmentView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.e("Root_Tab_home", "Root_Tab_home 1 container on activity created");
        if (!mIsViewInited) {
            mIsViewInited = true;
            initView();
        }
    }

    private void initView() {
        Log.e("Root_Tab_home", "Root_Tab_home 1 init view");

        replaceFragment(new HomeFragment(), false);
    }


    public interface onTabClickListener {
        void onTabSelected();
    }

}
