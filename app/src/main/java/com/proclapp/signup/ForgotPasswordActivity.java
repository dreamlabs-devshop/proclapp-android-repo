package com.proclapp.signup;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.AlertDialogInterface;
import com.proclapp.utils.DialogUtils;
import com.proclapp.utils.Logger;
import com.proclapp.utils.StringUtils;
import com.proclapp.utils.Validation;

import java.util.HashMap;

public class ForgotPasswordActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView img_back;
    private TextView txt_forgot_password,
            txt_forgot_password_data;

    private EditText edt_email;
    private ProgressDialog progressDialog;
    private Button btn_submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.loading));

        initView();
        initStyle();
        initListener();
    }

    private void initView() {
        img_back = findViewById(R.id.img_back);
        txt_forgot_password = findViewById(R.id.txt_forgot_password);
        txt_forgot_password_data = findViewById(R.id.txt_forgot_password_data);
        edt_email = findViewById(R.id.edt_email);
        btn_submit = findViewById(R.id.btn_submit);
    }

    private void initStyle() {

        txt_forgot_password.setTypeface(AppClass.lato_regular);
        edt_email.setTypeface(AppClass.lato_regular);
        txt_forgot_password_data.setTypeface(AppClass.lato_light);
        btn_submit.setTypeface(AppClass.lato_semibold);
    }

    private void initListener() {
        btn_submit.setOnClickListener(this);
        img_back.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.btn_submit:

                if (AppClass.isInternetConnectionAvailable()) {
                    if (validation()) {
//                        callAPIForgotPassword();
                        forgotPassword();
                    }
                } else {
                    AppClass.snackBarView.snackBarShow(ForgotPasswordActivity.this, getString(R.string.nonetwork));
                }

                break;

            case R.id.img_back:
                onBackPressed();
                break;

        }
    }



    private void forgotPassword() {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("email", edt_email.getText().toString());

        Logger.d("hashMap:" + hashMap);

        ApiCall.getInstance().forgotPassword(ForgotPasswordActivity.this, edt_email.getText().toString(), new IApiCallback() {
            @Override
            public void onSuccess(Object data) {

                String response = (String) data;
                AppClass.snackBarView.snackBarShow(ForgotPasswordActivity.this, response);

            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {
                if (StringUtils.isNotEmpty(errorMessage)) {
                    DialogUtils.openDialog(ForgotPasswordActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                        @Override
                        public void onNegativeBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }

                        @Override
                        public void onPositiveBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }
                    });
                }
            }
        }, true);


    }

    private boolean validation() {

        boolean isValid = true;

        if (edt_email.getText().toString().isEmpty()) {
            isValid = false;
            AppClass.snackBarView.snackBarShow(ForgotPasswordActivity.this, getResources().getString(R.string.vemail));
        } else if (!Validation.isValidEmail(edt_email.getText().toString())) {
            isValid = false;
            AppClass.snackBarView.snackBarShow(ForgotPasswordActivity.this, getResources().getString(R.string.vvemail));
        }

        return isValid;
    }
}
