package com.proclapp.signup;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.home.HomeActivity;
import com.proclapp.more.settings.ChangePasswordActivity;
import com.proclapp.profile.EditProfileActivity;
import com.proclapp.utils.ScreenCode;
import com.proclapp.utils.Utils;

public class GetStartedActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView img_back;
    private TextView txt_ask_and_answer,
            txt_write_and_discuss,
            txt_send_req,
            txt_download_and_read_research,
            txt_lets_get_started;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_started);

        Utils.setStatusBarColor(getWindow(), this, R.color.white);

        initView();
        initStyle();
        initListener();

        AppClass.preferences.storeGetStartedFirstTime(true);

    }

    private void initView() {
        img_back = findViewById(R.id.img_back);

        txt_ask_and_answer = findViewById(R.id.txt_ask_and_answer);
        txt_write_and_discuss = findViewById(R.id.txt_write_and_discuss);
        txt_send_req = findViewById(R.id.txt_send_req);
        txt_download_and_read_research = findViewById(R.id.txt_download_and_read_research);
        txt_lets_get_started = findViewById(R.id.txt_lets_get_started);
    }

    private void initStyle() {
        txt_ask_and_answer.setTypeface(AppClass.lato_medium);
        txt_write_and_discuss.setTypeface(AppClass.lato_medium);
        txt_send_req.setTypeface(AppClass.lato_medium);
        txt_download_and_read_research.setTypeface(AppClass.lato_medium);
        txt_lets_get_started.setTypeface(AppClass.lato_regular);
    }

    private void initListener() {
        txt_lets_get_started.setOnClickListener(this);
        img_back.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.img_back:

                onBackPressed();

                break;

            case R.id.txt_lets_get_started:

                if (AppClass.preferences.getScreeCode().equalsIgnoreCase(ScreenCode.ChangeProfile)) {
                    startActivity(new Intent(GetStartedActivity.this, EditProfileActivity.class));
                } else if (AppClass.preferences.getScreeCode().equalsIgnoreCase(ScreenCode.UpdatePassword)) {
                    startActivity(new Intent(GetStartedActivity.this, ChangePasswordActivity.class));
                } else {
                    startActivity(new Intent(GetStartedActivity.this, HomeActivity.class));
                }
                finishAffinity();


                break;
        }
    }
}
