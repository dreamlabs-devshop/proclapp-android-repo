package com.proclapp.signup;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.chinalwb.are.ArePreferences;
import com.google.gson.JsonObject;
import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.home.HomeActivity;
import com.proclapp.more.settings.ChangePasswordActivity;
import com.proclapp.profile.EditProfileActivity;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.AlertDialogInterface;
import com.proclapp.utils.DeviceUtils;
import com.proclapp.utils.DialogUtils;
import com.proclapp.utils.KeyBoardUtils;
import com.proclapp.utils.ScreenCode;
import com.proclapp.utils.StringUtils;
import com.proclapp.utils.Validation;
import com.zyf.vc.VideoPreferences;

import java.util.HashMap;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView txt_login,
            txt_forgot_password,
            txt_create_account;
    private EditText edt_email,
            edt_password;
    private ImageView img_back;

    private Button btn_login;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.loading));

        initView();
        initStyle();
        initListener();


    }

    private void initView() {

        txt_login = findViewById(R.id.txt_login);
        txt_forgot_password = findViewById(R.id.txt_forgot_password);
        txt_create_account = findViewById(R.id.txt_create_account);
        edt_email = findViewById(R.id.edt_email);
        edt_password = findViewById(R.id.edt_password);
        btn_login = findViewById(R.id.btn_login);
        img_back = findViewById(R.id.img_back);

    }

    private void initStyle() {

        txt_login.setTypeface(AppClass.lato_regular);
        txt_forgot_password.setTypeface(AppClass.lato_regular);
        txt_create_account.setTypeface(AppClass.lato_regular);
        edt_email.setTypeface(AppClass.lato_regular);
        edt_password.setTypeface(AppClass.lato_regular);
        btn_login.setTypeface(AppClass.lato_regular);

    }

    private void initListener() {

        btn_login.setOnClickListener(this);
        txt_forgot_password.setOnClickListener(this);
        txt_create_account.setOnClickListener(this);
        img_back.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.img_back:

                onBackPressed();

                break;

            case R.id.txt_create_account:

                startActivity(new Intent(LoginActivity.this, SignUpActivity.class));

                break;

            case R.id.txt_forgot_password:

                startActivity(new Intent(LoginActivity.this, ForgotPasswordActivity.class));

                break;

            case R.id.btn_login:

                KeyBoardUtils.closeSoftKeyboard(LoginActivity.this);

                if (AppClass.isInternetConnectionAvailable()) {

                    if (validation()) {
                        callAPISignIn();
                    }

                } else {
                    AppClass.snackBarView.snackBarShow(LoginActivity.this, getResources().getString(R.string.nonetwork));
                }

                break;
        }
    }

    private void callAPISignIn() {

        try {


            HashMap<String, String> requests = new HashMap<>();
            requests.put("email", edt_email.getText().toString());
            requests.put("password", edt_password.getText().toString());
            requests.put("device_token", AppClass.preferences.getFCMToken());
            requests.put("device_id", DeviceUtils.getDeviceIdWithoutPermission(LoginActivity.this));
            requests.put("device_name", DeviceUtils.getDeviceName());
            requests.put("device_type", DeviceUtils.getDeviceType());

            ApiCall.getInstance().signin(this, requests, new IApiCallback() {
                @Override
                public void onSuccess(Object data) {

                    try {


                        JsonObject response = (JsonObject) data;

                        AppClass.preferences.storeToken(response.get("token").getAsString());
                        AppClass.preferences.storeUserId(response.get("user_id").getAsString());
                        AppClass.preferences.storeEmailId(response.get("email").getAsString());
                        AppClass.preferences.storeFirstName(response.get("firstname").getAsString());
                        AppClass.preferences.storeUSerLastName(response.get("lastname").getAsString());
                        AppClass.preferences.storeScreeCode(response.get("screen_code").getAsString());
                        AppClass.preferences.storeIsExpert(response.get("is_expert").getAsString());

                        new VideoPreferences(getApplicationContext()).storeUserId(response.get("user_id").getAsString());
                        new VideoPreferences(getApplicationContext()).storeToken(response.get("token").getAsString());

                        new ArePreferences(getApplicationContext()).storeUserId(response.get("user_id").getAsString());
                        new ArePreferences(getApplicationContext()).storeToken(response.get("token").getAsString());

                        if (AppClass.preferences.getGetStartedFirstTime()) {

                            if (AppClass.preferences.getScreeCode().equalsIgnoreCase(ScreenCode.ChangeProfile)) {
                                startActivity(new Intent(LoginActivity.this, EditProfileActivity.class));
                            } else if (AppClass.preferences.getScreeCode().equalsIgnoreCase(ScreenCode.UpdatePassword)) {
                                startActivity(new Intent(LoginActivity.this, ChangePasswordActivity.class));
                            } else {
                                startActivity(new Intent(LoginActivity.this, HomeActivity.class));
                            }

                        } else {
                            startActivity(new Intent(LoginActivity.this, GetStartedActivity.class));
                        }
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            finishAffinity();
                        }

                        AppClass.preferences.setSkip(false);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(boolean isFalseFromService, String errorMessage) {
                    if (StringUtils.isNotEmpty(errorMessage)) {
                        DialogUtils.openDialog(LoginActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                            @Override
                            public void onNegativeBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }

                            @Override
                            public void onPositiveBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }
                        });
                    }
                }
            }, true);


        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void openResponseAlert(Activity activity, String message) {
        AlertDialog.Builder ald = new AlertDialog.Builder(activity);
        ald.setMessage(message);
        ald.setCancelable(false);
        ald.setPositiveButton(activity.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
            }
        });

        ald.show();
    }

    private boolean validation() {

        boolean isValid = true;

        if (edt_email.getText().toString().trim().isEmpty()) {
            isValid = false;
            AppClass.snackBarView.snackBarShow(LoginActivity.this, getResources().getString(R.string.vemail), ContextCompat.getColor(LoginActivity.this, R.color.red));
            edt_email.requestFocus();
        } else if (!Validation.isValidEmail(edt_email.getText().toString())) {
            isValid = false;
            AppClass.snackBarView.snackBarShow(LoginActivity.this, getResources().getString(R.string.vvemail), ContextCompat.getColor(LoginActivity.this, R.color.red));
            edt_email.requestFocus();
        } else if (edt_password.getText().toString().trim().isEmpty()) {
            isValid = false;
            AppClass.snackBarView.snackBarShow(LoginActivity.this, getResources().getString(R.string.vpswd), ContextCompat.getColor(LoginActivity.this, R.color.red));
            edt_password.requestFocus();
        }

        return isValid;
    }
}
