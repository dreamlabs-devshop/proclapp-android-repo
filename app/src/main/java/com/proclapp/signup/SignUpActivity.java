package com.proclapp.signup;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.signup.adapter.SignUpViewPagerAdapter;
import com.proclapp.signup.fragment.SignUpStep1Fragment;
import com.proclapp.signup.fragment.SignUpStep3Fragment;
import com.proclapp.utils.AlertDialogInterface;
import com.proclapp.utils.CustomViewPager;
import com.proclapp.utils.DialogUtils;
import com.proclapp.utils.Logger;
import com.proclapp.utils.StringUtils;
import com.proclapp.utils.Utils;

import org.json.JSONArray;

import java.util.HashMap;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener {

    public TextView
            txt_1,
            txt_2,
            txt_3,
            txt_skip;
    private ImageView img_back;
    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;
    private CustomViewPager vp_sign_up;
    private SignUpViewPagerAdapter signUpViewPagerAdapter;

    private String first_name, last_name, email,
            country_code, mobile_number,
            gender, residential_address, password;
    private JSONArray educational_data = new JSONArray();
    private String selectedCategoryIds = "";
    private ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        Utils.setStatusBarColor(getWindow(), this, R.color.white);

        progressDialog = new ProgressDialog(SignUpActivity.this);
        progressDialog.setMessage(getResources().getString(R.string.loading));

        initView();
        initStyle();
        initListener();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        setViewPager();
        //setUpFragment();
    }

    private void setUpFragment() {

        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        SignUpStep1Fragment signUpStep1Fragment = new SignUpStep1Fragment();
        fragmentTransaction.add(R.id.fragment_container, signUpStep1Fragment);
        fragmentTransaction.commit();
    }

    private void setViewPager() {
        vp_sign_up.setPagingEnabled(false);
        vp_sign_up.setOffscreenPageLimit(3);

        signUpViewPagerAdapter = new SignUpViewPagerAdapter(getSupportFragmentManager());

        signUpViewPagerAdapter.addFragment(new SignUpStep1Fragment());
//        signUpViewPagerAdapter.addFragment(new SignUpStep2Fragment());
        signUpViewPagerAdapter.addFragment(new SignUpStep3Fragment(new SignUpStep3Fragment.FinalSignUp() {
            @Override
            public void callSignUp(String selected_category_ids) {
                selectedCategoryIds = selected_category_ids;
                callAPISignUp();
            }
        }));

        vp_sign_up.setAdapter(signUpViewPagerAdapter);

        vp_sign_up.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {

                if (i == 0) {
                    txt_1.setTextColor(ContextCompat.getColor(SignUpActivity.this, R.color.color_36cfd9));
                    txt_2.setTextColor(ContextCompat.getColor(SignUpActivity.this, R.color.color_666666));
                    txt_3.setTextColor(ContextCompat.getColor(SignUpActivity.this, R.color.color_666666));

                    txt_1.setBackgroundResource(R.drawable.signup_round_1);
                    txt_2.setBackgroundResource(R.drawable.signup_round_2);
                    txt_3.setBackgroundResource(R.drawable.signup_round_2);

                    txt_2.setEnabled(false);
                    txt_3.setEnabled(false);

                    txt_skip.setVisibility(View.GONE);

                } else if (i == 1) {

                    txt_1.setTextColor(ContextCompat.getColor(SignUpActivity.this, R.color.color_666666));
                    txt_2.setTextColor(ContextCompat.getColor(SignUpActivity.this, R.color.color_36cfd9));
                    txt_3.setTextColor(ContextCompat.getColor(SignUpActivity.this, R.color.color_666666));

                    txt_1.setBackgroundResource(R.drawable.signup_round_2);
                    txt_2.setBackgroundResource(R.drawable.signup_round_1);
                    txt_3.setBackgroundResource(R.drawable.signup_round_2);
                    txt_3.setEnabled(false);

                    txt_skip.setVisibility(View.VISIBLE);

                } else if (i == 2) {

                    txt_1.setTextColor(ContextCompat.getColor(SignUpActivity.this, R.color.color_666666));
                    txt_2.setTextColor(ContextCompat.getColor(SignUpActivity.this, R.color.color_666666));
                    txt_3.setTextColor(ContextCompat.getColor(SignUpActivity.this, R.color.color_36cfd9));

                    txt_1.setBackgroundResource(R.drawable.signup_round_2);
                    txt_2.setBackgroundResource(R.drawable.signup_round_2);
                    txt_3.setBackgroundResource(R.drawable.signup_round_1);

                    txt_skip.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

    private void initView() {

        img_back = findViewById(R.id.img_back);
        txt_1 = findViewById(R.id.txt_1);
        txt_2 = findViewById(R.id.txt_2);
        txt_3 = findViewById(R.id.txt_3);
        txt_skip = findViewById(R.id.txt_skip);
        vp_sign_up = findViewById(R.id.vp_sign_up);

        txt_skip.setVisibility(View.GONE);

        txt_2.setEnabled(false);
        txt_3.setEnabled(false);

    }

    private void initStyle() {
        txt_1.setTypeface(AppClass.lato_bold);
        txt_2.setTypeface(AppClass.lato_regular);
        txt_3.setTypeface(AppClass.lato_regular);
        txt_skip.setTypeface(AppClass.lato_semibold);
    }

    private void initListener() {
        img_back.setOnClickListener(this);
        txt_1.setOnClickListener(this);
        txt_2.setOnClickListener(this);
        txt_3.setOnClickListener(this);
        txt_skip.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.img_back:

                onBackPressed();

                break;

            case R.id.txt_1:

                vp_sign_up.setCurrentItem(0);

                break;

            case R.id.txt_2:

                vp_sign_up.setCurrentItem(1);

                break;

            case R.id.txt_3:

                vp_sign_up.setCurrentItem(2);

                break;

            case R.id.txt_skip:

                callAPISignUp();

                break;
        }
    }

    public void saveStep1Data(String first_name, String last_name, String email,
                              String country_code, String mobile_number,
                              String gender, String residential_address, String password) {

        this.first_name = first_name;
        this.last_name = last_name;
        this.email = email;
        this.country_code = country_code;
        this.mobile_number = mobile_number;
        this.gender = gender;
        this.residential_address = residential_address;
        this.password = password;

        vp_sign_up.setCurrentItem(1);

    }

    public void saveStep2Data(JSONArray educational_data) {
        this.educational_data = educational_data;
        vp_sign_up.setCurrentItem(2);
    }

    private void callAPISignUp() {

        if (AppClass.isInternetConnectionAvailable()) {

            HashMap<String, String> requests = new HashMap<>();
            requests.put("firstname", first_name);
            requests.put("lastname", last_name);
            requests.put("email", email);
            requests.put("gender", gender);
            requests.put("phone", mobile_number);
            requests.put("phone_code", country_code);
            requests.put("address", residential_address);
            requests.put("password", password);

            if (educational_data.length() > 0) {
                requests.put("education", educational_data.toString());
            }

            if (!selectedCategoryIds.isEmpty()) {
                requests.put("category", selectedCategoryIds);
            }
            Logger.d("hashMap:" + requests);

            ApiCall.getInstance().signup(SignUpActivity.this, requests, new IApiCallback() {
                @Override
                public void onSuccess(Object data) {

                    String response = (String) data;
                    AppClass.preferences.setSkip(false);
                    openResponseAlert(SignUpActivity.this, response);

                }

                @Override
                public void onFailure(boolean isFalseFromService, String errorMessage) {
                    if (StringUtils.isNotEmpty(errorMessage)) {
                        DialogUtils.openDialog(SignUpActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                            @Override
                            public void onNegativeBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }

                            @Override
                            public void onPositiveBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }
                        });
                    }
                }
            }, true);


        } else {
            AppClass.snackBarView.snackBarShow(SignUpActivity.this, getString(R.string.nonetwork));
        }

    }


    private void openResponseAlert(Activity activity, String message) {
        final AlertDialog.Builder ald = new AlertDialog.Builder(activity);
        ald.setMessage(message);
        ald.setCancelable(false);
        ald.setPositiveButton(activity.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

                finishAffinity();
                startActivity(new Intent(SignUpActivity.this, LoginActivity.class));

            }
        });

        ald.show();
    }
}
