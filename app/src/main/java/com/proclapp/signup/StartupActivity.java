package com.proclapp.signup;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.chinalwb.are.ArePreferences;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.gson.JsonObject;
import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.home.HomeActivity;
import com.proclapp.more.settings.ChangePasswordActivity;
import com.proclapp.platform.APIHelper;
import com.proclapp.platform.LISessionManager;
import com.proclapp.platform.errors.LIApiError;
import com.proclapp.platform.errors.LIAuthError;
import com.proclapp.platform.listeners.ApiListener;
import com.proclapp.platform.listeners.ApiResponse;
import com.proclapp.platform.listeners.AuthListener;
import com.proclapp.platform.utils.Scope;
import com.proclapp.profile.EditProfileActivity;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.AlertDialogInterface;
import com.proclapp.utils.DeviceUtils;
import com.proclapp.utils.DialogUtils;
import com.proclapp.utils.Logger;
import com.proclapp.utils.ScreenCode;
import com.proclapp.utils.StringUtils;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
import com.zyf.vc.VideoPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;

public class StartupActivity extends AppCompatActivity implements View.OnClickListener {


    private static int RC_SIGN_IN = 12345;
    private static String TAG = StartupActivity.class.getSimpleName();
    TwitterLoginButton loginButton;
    private TextView txt_skip,
            txt_best_platform,
            txt_exchange_view,
            txt_intellectual_idea,
            txt_start_up_data,
            txt_login_with;
    private ImageView img_email,
            img_facebook,
            img_linkedin,
            img_google,
            img_twitter;
    private LoginButton
            btn_Signup_fb;
    private CallbackManager callbackManager;
    private ProgressDialog progressDialog;
    private GoogleSignInClient mGoogleSignInClient;

    private static Scope buildScope() {
        return Scope.build(Scope.R_BASICPROFILE, Scope.W_SHARE, Scope.R_EMAILADDRESS);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(StartupActivity.this);
        Twitter.initialize(this);

        setContentView(R.layout.activity_startup);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.loading));

        initView();
        initStyle();
        initListener();


        setFaceBookButton();
        setTwitterButton();
    }

    private void setFaceBookButton() {
        btn_Signup_fb = findViewById(R.id.btn_Signup_fb);
        callbackManager = CallbackManager.Factory.create();
        btn_Signup_fb.setReadPermissions(Arrays.asList("public_profile", "email"/*, "user_birthday"*/));
        btn_Signup_fb.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                Logger.d("loginResult===>" + loginResult);
                AccessToken accessToken = loginResult.getAccessToken();
                setUserProfileGetRequest(accessToken);
            }

            @Override
            public void onCancel() {
                Logger.d("onCancel===>");
            }

            @Override
            public void onError(FacebookException error) {
                error.printStackTrace();
            }
        });
    }

    private void setFaceBookLoginClickEvent() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        if (accessToken != null) {
            setUserProfileGetRequest(accessToken);
        } else {
            btn_Signup_fb.performClick();
            btn_Signup_fb.setPressed(true);
            btn_Signup_fb.invalidate();
            btn_Signup_fb.setPressed(false);
            btn_Signup_fb.invalidate();
        }
    }

    private void setUserProfileGetRequest(AccessToken accessToken) {

        GraphRequest graphRequest = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {

                Logger.d("response==>" + response);
                HashMap<String, String> fb_data_hashmap = new HashMap<String, String>();

                try {
                    fb_data_hashmap.put("id", object.getString("id"));
                } catch (Exception e) {
                    fb_data_hashmap.put("id", "");
                }

                try {
                    fb_data_hashmap.put("first_name", object.getString("first_name"));
                } catch (Exception e) {
                    fb_data_hashmap.put("first_name", "");
                }


                try {
                    fb_data_hashmap.put("last_name", object.getString("last_name"));
                } catch (Exception e) {
                    fb_data_hashmap.put("last_name", "");
                }

                try {
                    fb_data_hashmap.put("email", object.getString("email"));
                } catch (Exception e) {
                    fb_data_hashmap.put("email", "");
                }

//                callAPICheckSocialId(fb_data_hashmap.get("id"));
                callAPICheckSocialMediaID(fb_data_hashmap.get("email"), fb_data_hashmap.get("id"), "facebook");
            }
        });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "picture.type(large),id,name,link,email,gender,birthday,location,first_name,last_name");
        graphRequest.setParameters(parameters);
        graphRequest.executeAsync();
    }

    private void setTwitterButton() {
        loginButton = findViewById(R.id.login_button);


        loginButton.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                // Do something with result, which provides a TwitterSession for making API calls
                TwitterSession session = TwitterCore.getInstance().getSessionManager().getActiveSession();
                TwitterAuthToken authToken = session.getAuthToken();

                String token = authToken.token;
                String secret = authToken.secret;

                login(session);
            }

            @Override
            public void failure(TwitterException exception) {
                // Do something on failure
                Toast.makeText(StartupActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void login(TwitterSession session) {
        long userId = session.getUserId();

//        callAPICheckSocialId(String.valueOf(userId));
    }

    private void handleLogin() {
        LISessionManager.getInstance(getApplicationContext()).init(this, buildScope(), new AuthListener() {
            @Override
            public void onAuthSuccess() {
                // Authentication was successful.  You can now do
                // other calls with the SDK.
                fetchPersonalDetail();
            }

            @Override
            public void onAuthError(LIAuthError error) {
                // Handle authentication errors
                Log.e("Error", error.toString());
            }
        }, true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        callbackManager.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN && resultCode == RESULT_OK) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void fetchPersonalDetail() {
        String url = "https://api.linkedin.com/v1/people/~:(id,first-name,last-name)";

        APIHelper apiHelper = APIHelper.getInstance(getApplicationContext());
        apiHelper.getRequest(this, url, new ApiListener() {
            @Override
            public void onApiSuccess(ApiResponse apiResponse) {
                // Success!
                JSONObject jsonObject = apiResponse.getResponseDataAsJson();
                try {
                    String id = jsonObject.getString("id");
                    String firstname = jsonObject.getString("firstName");
                    String lastname = jsonObject.getString("lastName");
                    //  StringBuilder stringBuilder=new StringBuilder();
                    //  stringBuilder.append(firstname);
                    //  stringBuilder.append(lastname);
                    // stringBuilder.append(id);
                    //  txtDetails.setText(stringBuilder);

//                    callAPICheckSocialId(id);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onApiError(LIApiError liApiError) {
                // Error making GET request!
                Log.e("Error", liApiError.toString());
            }
        });
    }

    private void googleSignIn() {
        GoogleSignInOptions options = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestProfile()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, options);
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);

    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            callAPICheckSocialMediaID(account.getEmail(), account.getId(), "google");

            // Signed in successfully, show authenticated UI.
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
        }
    }

    private void initView() {

        txt_skip = findViewById(R.id.txt_skip);
        txt_best_platform = findViewById(R.id.txt_best_platform);
        txt_exchange_view = findViewById(R.id.txt_exchange_view);
        txt_intellectual_idea = findViewById(R.id.txt_intellectual_idea);
        txt_start_up_data = findViewById(R.id.txt_start_up_data);
        txt_login_with = findViewById(R.id.txt_login_with);

        img_email = findViewById(R.id.img_email);
        img_facebook = findViewById(R.id.img_facebook);
        img_linkedin = findViewById(R.id.img_linkedin);
        img_twitter = findViewById(R.id.img_twitter);
        img_google = findViewById(R.id.img_google);


    }

    private void initStyle() {
        txt_skip.setTypeface(AppClass.lato_regular);
        txt_best_platform.setTypeface(AppClass.lato_light);
        txt_exchange_view.setTypeface(AppClass.lato_light);
        txt_intellectual_idea.setTypeface(AppClass.lato_light);
        txt_start_up_data.setTypeface(AppClass.lato_regular);
        txt_login_with.setTypeface(AppClass.lato_regular);
    }

    private void initListener() {
        img_email.setOnClickListener(this);
        img_facebook.setOnClickListener(this);
        img_linkedin.setOnClickListener(this);
        img_twitter.setOnClickListener(this);
        txt_skip.setOnClickListener(this);
        img_google.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.txt_skip:

                AppClass.preferences.setSkip(true);
                startActivity(new Intent(StartupActivity.this, HomeActivity.class));

                break;

            case R.id.img_email:

                startActivity(new Intent(StartupActivity.this, LoginActivity.class));

                break;

            case R.id.img_facebook:

                setFaceBookLoginClickEvent();
                break;

            case R.id.img_linkedin:

                //handleLogin();

                break;

            case R.id.img_twitter:

//                loginButton.performClick();

                break;
            case R.id.img_google:
                googleSignIn();
        }
    }

    private void callAPICheckSocialMediaID(final String email, final String media_id, final String media_type) {


        HashMap<String, String> request = new HashMap<>();
        request.put("media_id", media_id);

        ApiCall.getInstance().checksocialId(StartupActivity.this, request, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {
                loginToHome((JsonObject) data);
            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {

                callAPISignInWithSocial(email, media_id, media_type);

            }
        }, true);

    }

    private void callAPISignInWithSocial(String email, String media_id, String media_type) {

        HashMap<String, String> request = new HashMap<>();
        request.put("email", email);
        request.put("media_id", media_id);
        request.put("media_type", media_type);
        request.put("device_token", "");
        request.put("device_id", DeviceUtils.getDeviceIdWithoutPermission(StartupActivity.this));
        request.put("device_name", DeviceUtils.getDeviceName());
        request.put("device_type", DeviceUtils.getDeviceType());

        ApiCall.getInstance().signInWithSocial(StartupActivity.this, request, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {

                loginToHome((JsonObject) data);


            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {
                if (StringUtils.isNotEmpty(errorMessage)) {
                    DialogUtils.openDialog(StartupActivity.this, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                        @Override
                        public void onNegativeBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }

                        @Override
                        public void onPositiveBtnClicked(DialogInterface alrt) {
                            alrt.dismiss();
                        }
                    });
                }

            }
        }, true);

    }

    private void loginToHome(JsonObject response) {
        AppClass.preferences.storeToken(response.get("token").getAsString());
        AppClass.preferences.storeUserId(response.get("user_id").getAsString());
        AppClass.preferences.storeEmailId(response.get("email").getAsString());
        AppClass.preferences.storeFirstName(response.get("firstname").getAsString());
        AppClass.preferences.storeUSerLastName(response.get("lastname").getAsString());
        AppClass.preferences.storeScreeCode(response.get("screen_code").getAsString());
        AppClass.preferences.storeIsExpert(response.get("is_expert").getAsString());

        new VideoPreferences(getApplicationContext()).storeUserId(response.get("user_id").getAsString());
        new VideoPreferences(getApplicationContext()).storeToken(response.get("token").getAsString());

        new ArePreferences(getApplicationContext()).storeUserId(response.get("user_id").getAsString());
        new ArePreferences(getApplicationContext()).storeToken(response.get("token").getAsString());

//        AppClass.preferences.storeToken(response.get("token").getAsString());
//        AppClass.preferences.storeUserId(response.get("user_id").getAsString());
//        AppClass.preferences.storeScreeCode(response.get("screen_code").getAsString());

        AppClass.preferences.setSkip(false);

        if (AppClass.preferences.getGetStartedFirstTime()) {

            if (AppClass.preferences.getScreeCode().equalsIgnoreCase(ScreenCode.ChangeProfile)) {
                startActivity(new Intent(StartupActivity.this, EditProfileActivity.class));
            } else if (AppClass.preferences.getScreeCode().equalsIgnoreCase(ScreenCode.UpdatePassword)) {
                startActivity(new Intent(StartupActivity.this, ChangePasswordActivity.class));
            } else {
                startActivity(new Intent(StartupActivity.this, HomeActivity.class));
            }

        } else {
            startActivity(new Intent(StartupActivity.this, GetStartedActivity.class));
        }

    }

}
