package com.proclapp.signup.adapter;

import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.model.CategoryModel;
import com.proclapp.utils.ImageLoadUtils;

import java.util.ArrayList;

/**
 *
 */

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.MyViewHolder> implements Filterable {

    Activity activity;
    ArrayList<CategoryModel> list = new ArrayList<>();
    ArrayList<CategoryModel> categoryListFiltered = new ArrayList<>();
    CategorySelection categorySelection;
    DisplayMetrics metrics;
    ArrayList<String> selectedCategories;

    public CategoryAdapter(Activity activity, ArrayList<CategoryModel> list, CategorySelection selection, ArrayList<String> selectedCategories) {
        this.activity = activity;
        this.list = list;
        categoryListFiltered = list;
        this.categorySelection = selection;
        this.selectedCategories = selectedCategories;

        metrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_signup_step3_category, parent, false);
        return new MyViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.txt_category_name.setText(categoryListFiltered.get(position).getCategory_name());


        if (categoryListFiltered.get(position).getIs_selected() == 1) {

            ImageLoadUtils.imageLoad(activity,
                    holder.img_category,
                    categoryListFiltered.get(position).getCategory_selected_img());

        } else {

            ImageLoadUtils.imageLoad(activity,
                    holder.img_category,
                    categoryListFiltered.get(position).getCategory_img());

        }


        holder.img_category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (categoryListFiltered.get(position).getIs_selected() == 1) {
                    categoryListFiltered.get(position).setIs_selected(0);
                } else {
                    categoryListFiltered.get(position).setIs_selected(1);
                }

                notifyDataSetChanged();
            }
        });


    }

    @Override
    public int getItemCount() {
        return categoryListFiltered.size();
    }

    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                Log.d("getFilter", "adapter" + charSequence);

                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    categoryListFiltered = list;
                } else {
                    ArrayList<CategoryModel> filteredList = new ArrayList<>();
                    for (CategoryModel row : list) {

                        if (row.getCategory_name().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    categoryListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = categoryListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                categoryListFiltered = (ArrayList<CategoryModel>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public interface CategorySelection {
        void onCategorySelection(String categoryName, String categoryId);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txt_category_name;
        ImageView img_category;

        public MyViewHolder(View itemView) {
            super(itemView);
            txt_category_name = itemView.findViewById(R.id.txt_category_name);
            img_category = itemView.findViewById(R.id.img_category);

            txt_category_name.setTypeface(AppClass.lato_regular);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams((metrics.widthPixels * 83) / 480, (metrics.heightPixels * 83) / 800, Gravity.CENTER);
            img_category.setLayoutParams(params);

        }
    }
}
