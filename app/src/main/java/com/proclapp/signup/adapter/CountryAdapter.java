package com.proclapp.signup.adapter;

import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;


import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.model.CountryModel;

import java.util.ArrayList;

/**
 */

public class CountryAdapter extends RecyclerView.Adapter<CountryAdapter.MyViewHolder> implements Filterable {

    Activity activity;
    ArrayList<CountryModel> list = new ArrayList<>();
    ArrayList<CountryModel> countryListFiltered = new ArrayList<>();
    CountrySelection countrySelection;

    public CountryAdapter(Activity activity, ArrayList<CountryModel> list, CountrySelection selection) {
        this.activity = activity;
        this.list = list;
        countryListFiltered = list;
        this.countrySelection = selection;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_country, parent, false);
        return new MyViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.txtCountryName.setText(countryListFiltered.get(position).getName());

        holder.txtCountryName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                countrySelection.onCountrySelection(countryListFiltered.get(position).getCode(),
                        countryListFiltered.get(position).getName(),
                        countryListFiltered.get(position).getCountry_code());
            }
        });

    }

    @Override
    public int getItemCount() {
        return countryListFiltered.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txtCountryName;

        public MyViewHolder(View itemView) {
            super(itemView);
            txtCountryName = (TextView) itemView.findViewById(R.id.txtCountryName);
            txtCountryName.setTypeface(AppClass.lato_regular);

        }
    }

    public interface CountrySelection {
        public void onCountrySelection(String countryCode, String countryName,String code);
    }

    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                Log.d("getFilter", "adapter" + charSequence);

                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    countryListFiltered = list;
                } else {
                    ArrayList<CountryModel> filteredList = new ArrayList<>();
                    for (CountryModel row : list) {

                        if (row.getName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    countryListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = countryListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                countryListFiltered = (ArrayList<CountryModel>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
