package com.proclapp.signup.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.signup.fragment.SignUpStep2Fragment;

import java.util.ArrayList;

/**
 */

public class EducationalQualificationAdapter extends RecyclerView.Adapter<EducationalQualificationAdapter.MyViewHolder> {

    Activity activity;
    ArrayList<String> yearList;

    PassingYearAdapter passingYearAdapter;
    SignUpStep2Fragment signUpStep2Fragment;

    public EducationalQualificationAdapter(Activity activity, SignUpStep2Fragment signUpStep2Fragment, ArrayList<String> yearList) {
        this.activity = activity;
        this.yearList = yearList;
        this.signUpStep2Fragment = signUpStep2Fragment;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_educational_qualifications, parent, false);
        return new MyViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        holder.txt_passing_year_value.setText(SignUpStep2Fragment.qualificationArrayList.get(holder.getAdapterPosition()).getPassing_year());

        if (SignUpStep2Fragment.qualificationArrayList.size() > 1) {
            holder.txt_remove.setVisibility(View.VISIBLE);
        } else {
            holder.txt_remove.setVisibility(View.GONE);
        }

        holder.edt_qualifications.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                SignUpStep2Fragment.qualificationArrayList.get(holder.getAdapterPosition()).setEducational_qualification(s.toString());
            }
        });

        holder.ll_passing_year.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DialogCountryCode(activity, holder, holder.getAdapterPosition()).show();
            }
        });

        holder.txt_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (SignUpStep2Fragment.qualificationArrayList.size() > 1) {
                    holder.edt_qualifications.setText("");
                    SignUpStep2Fragment.qualificationArrayList.remove(holder.getAdapterPosition());
                    notifyItemRemoved(holder.getAdapterPosition());
                    //notifyDataSetChanged();

                    if(SignUpStep2Fragment.qualificationArrayList.size() == 1){
                        holder.txt_remove.setVisibility(View.GONE);
                        notifyDataSetChanged();
                    }
                }

            }
        });

    }

    @Override
    public int getItemCount() {
        return SignUpStep2Fragment.qualificationArrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        EditText edt_qualifications;
        LinearLayout ll_passing_year;
        TextView txt_passing_year,
                txt_passing_year_value,
                txt_remove;

        public MyViewHolder(View itemView) {
            super(itemView);
            edt_qualifications = itemView.findViewById(R.id.edt_qualifications);
            ll_passing_year = itemView.findViewById(R.id.ll_passing_year);
            txt_passing_year = itemView.findViewById(R.id.txt_passing_year);
            txt_passing_year_value = itemView.findViewById(R.id.txt_passing_year_value);
            txt_remove = itemView.findViewById(R.id.txt_remove);

            edt_qualifications.setTypeface(AppClass.lato_regular);
            txt_passing_year.setTypeface(AppClass.lato_regular);
            txt_passing_year_value.setTypeface(AppClass.lato_regular);
            txt_remove.setTypeface(AppClass.lato_regular);

        }
    }


    class DialogCountryCode extends Dialog {

        private Activity activity;

        private EditText et_search_year;
        private RecyclerView recyclerview_year;
        private MyViewHolder viewHolder;
        private int position;

        private DialogCountryCode(Activity a, MyViewHolder viewHolder, int position) {
            super(a);
            this.activity = a;
            this.viewHolder = viewHolder;
            this.position = position;
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.dialog_country_code);
            getWindow().setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.MATCH_PARENT);

            WindowManager.LayoutParams params = this.getWindow().getAttributes();
            //params.x = -100;
            params.y = -100;
            this.getWindow().setAttributes(params);


            et_search_year = findViewById(R.id.et_search_country);
            et_search_year.setTypeface(AppClass.lato_regular);

            recyclerview_year = findViewById(R.id.recyclerview_country);
            LinearLayoutManager layoutManager = new LinearLayoutManager(activity);
            recyclerview_year.setLayoutManager(layoutManager);
            passingYearAdapter = new PassingYearAdapter(activity, yearList, new PassingYearAdapter.YearSelection() {
                @Override
                public void onYearSelection(String year) {

                    dismiss();
                    SignUpStep2Fragment.qualificationArrayList.get(position).setPassing_year(year);
                    //SignUpStep2Fragment.qualificationArrayList.get(position).setEducational_qualification(viewHolder.edt_qualifications.getText().toString());
                    viewHolder.txt_passing_year_value.setText(year);

                }
            });
            recyclerview_year.setAdapter(passingYearAdapter);

            et_search_year.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    passingYearAdapter.getFilter().filter(s.toString());
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });


        }


    }

}
