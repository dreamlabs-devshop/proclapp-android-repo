package com.proclapp.signup.adapter;

import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.proclapp.AppClass;
import com.proclapp.R;

import java.util.ArrayList;

/**
 */

public class PassingYearAdapter extends RecyclerView.Adapter<PassingYearAdapter.MyViewHolder> implements Filterable {

    Activity activity;
    ArrayList<String> list = new ArrayList<>();
    ArrayList<String> yearListFiltered = new ArrayList<>();
    YearSelection yearSelection;

    public PassingYearAdapter(Activity activity, ArrayList<String> list, YearSelection yearSelection) {
        this.activity = activity;
        this.list = list;
        yearListFiltered = list;
        this.yearSelection = yearSelection;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_country, parent, false);
        return new MyViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.txtCountryName.setText(yearListFiltered.get(position));

        holder.txtCountryName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                yearSelection.onYearSelection(yearListFiltered.get(position));
            }
        });

    }

    @Override
    public int getItemCount() {
        return yearListFiltered.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txtCountryName;

        public MyViewHolder(View itemView) {
            super(itemView);
            txtCountryName = (TextView) itemView.findViewById(R.id.txtCountryName);
            txtCountryName.setTypeface(AppClass.lato_regular);

        }
    }

    public interface YearSelection {
        public void onYearSelection(String year);
    }

    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                Log.d("getFilter", "adapter" + charSequence);

                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    yearListFiltered = list;
                } else {
                    ArrayList<String> filteredList = new ArrayList<>();
                    for (String row : list) {

                        if (row.toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    yearListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = yearListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                yearListFiltered = (ArrayList<String>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
