package com.proclapp.signup.adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

public class ProfessionalQualificationAdapter extends RecyclerView.Adapter<ProfessionalQualificationAdapter.ProfessionalQualificationHolder> {
    @NonNull
    @Override
    public ProfessionalQualificationHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull ProfessionalQualificationHolder professionalQualificationHolder, int i) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public class ProfessionalQualificationHolder extends RecyclerView.ViewHolder {
        public ProfessionalQualificationHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
