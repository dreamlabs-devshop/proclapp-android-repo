package com.proclapp.signup.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.model.CountryModel;
import com.proclapp.more.helpandsupport.AboutAppPrivacyTCActivity;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.signup.SignUpActivity;
import com.proclapp.signup.adapter.CountryAdapter;
import com.proclapp.utils.KeyBoardUtils;
import com.proclapp.utils.Validation;

import java.util.ArrayList;

public class SignUpStep1Fragment extends Fragment implements View.OnClickListener {

    private View view;

    private TextView txt_signup,
            txt_country_code,
            txt_gender,
            txt_gender_value,
            txt_by_signing_up,
            txt_by_terms_n_condition,
            txt_and,
            txt_privacy_policy;
    private EditText edt_firstname,
            edt_lastname,
            edt_email,
            edt_mobile_number,
            edt_residential_address,
            edt_password,
            edt_confirm_password;

    private ImageView img_map;

    private TextView btn_signup;

    private LinearLayout ll_gender;

    private SignUpActivity activity;
    private ArrayList<CountryModel> countryData;
    private CountryAdapter countryAdapter;
    private ProgressDialog progressDialog;
    private String phone_code = "";
    private String gender = "1";
    private boolean isValidEmail = true;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_sign_up_step1, container, false);

        activity = (SignUpActivity) getActivity();

        progressDialog = new ProgressDialog(activity);
        progressDialog.setMessage(activity.getResources().getString(R.string.loading));

        initView();
        initStyle();
        initListener();
        setCountryCode();

        return view;
    }


    private void initView() {
        txt_signup = view.findViewById(R.id.txt_signup);
        txt_country_code = view.findViewById(R.id.txt_country_code);
        txt_gender = view.findViewById(R.id.txt_gender);
        txt_gender_value = view.findViewById(R.id.txt_gender_value);
        txt_by_signing_up = view.findViewById(R.id.txt_by_signing_up);
        txt_by_terms_n_condition = view.findViewById(R.id.txt_by_terms_n_condition);
        txt_and = view.findViewById(R.id.txt_and);
        txt_privacy_policy = view.findViewById(R.id.txt_privacy_policy);

        img_map = view.findViewById(R.id.img_map);

        ll_gender = view.findViewById(R.id.ll_gender);

        edt_firstname = view.findViewById(R.id.edt_firstname);
        edt_lastname = view.findViewById(R.id.edt_lastname);
        edt_email = view.findViewById(R.id.edt_email);
        edt_mobile_number = view.findViewById(R.id.edt_mobile_number);
        edt_residential_address = view.findViewById(R.id.edt_residential_address);
        edt_password = view.findViewById(R.id.edt_password);
        edt_confirm_password = view.findViewById(R.id.edt_confirm_password);

        btn_signup = view.findViewById(R.id.btn_signup);

        edt_email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                isValidEmail = true;
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void setCountryCode() {

        String[] countryNameList = this.getResources().getStringArray(R.array.country);
        String[] countryCodeList = this.getResources().getStringArray(R.array.mobile_code);
        String[] countryCode = this.getResources().getStringArray(R.array.country_code);

        countryData = new ArrayList<>();

        for (int i = 0; i < countryCodeList.length; i++) {
            CountryModel model = new CountryModel();
            model.setCode(countryCodeList[i]);
            model.setName(countryNameList[i]);
            model.setCountry_code(countryCode[i]);
            countryData.add(model);
        }

    }

    private void initStyle() {

        txt_signup.setTypeface(AppClass.lato_regular);
        txt_country_code.setTypeface(AppClass.lato_regular);
        txt_gender.setTypeface(AppClass.lato_regular);
        txt_gender_value.setTypeface(AppClass.lato_regular);
        txt_by_signing_up.setTypeface(AppClass.lato_semibold);
        txt_by_terms_n_condition.setTypeface(AppClass.lato_semibold);
        txt_and.setTypeface(AppClass.lato_semibold);
        txt_privacy_policy.setTypeface(AppClass.lato_semibold);

        edt_firstname.setTypeface(AppClass.lato_regular);
        edt_lastname.setTypeface(AppClass.lato_regular);
        edt_email.setTypeface(AppClass.lato_regular);
        edt_mobile_number.setTypeface(AppClass.lato_regular);
        edt_residential_address.setTypeface(AppClass.lato_regular);
        edt_password.setTypeface(AppClass.lato_regular);
        edt_confirm_password.setTypeface(AppClass.lato_regular);

        btn_signup.setTypeface(AppClass.lato_regular);

    }

    private void initListener() {

        btn_signup.setOnClickListener(this);
        txt_by_terms_n_condition.setOnClickListener(this);
        txt_privacy_policy.setOnClickListener(this);
        img_map.setOnClickListener(this);
        ll_gender.setOnClickListener(this);
        txt_country_code.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {

        KeyBoardUtils.closeSoftKeyboard(activity);

        switch (view.getId()) {

            case R.id.btn_signup:

                KeyBoardUtils.hideSoftKeyboard(view, activity);

                if (AppClass.isInternetConnectionAvailable()) {

                    if (validation()) {

                        checkEmailExists();

                        //callAPISignUp();
                    }

                } else {
                    AppClass.snackBarView.snackBarShow(activity, activity.getResources().getString(R.string.nonetwork));
                }

                break;

            case R.id.txt_privacy_policy:

                startActivity(new Intent(activity, AboutAppPrivacyTCActivity.class)
                        .putExtra("url", "privacy_policy"));

                break;

            case R.id.txt_by_terms_n_condition:

                startActivity(new Intent(activity, AboutAppPrivacyTCActivity.class)
                        .putExtra("url", "term_and_conditions"));

                break;

            case R.id.img_map:

                break;

            case R.id.ll_gender:
                new DialogGender(activity).show();
                break;

            case R.id.txt_country_code:

                new DialogCountryCode(activity).show();

                break;
        }
    }

    private boolean validation() {

        boolean isValid = true;

        if (edt_firstname.getText().toString().trim().isEmpty()) {
            isValid = false;
            AppClass.snackBarView.snackBarShow(activity, activity.getResources().getString(R.string.vfirstname), ContextCompat.getColor(activity, R.color.red));
            edt_firstname.requestFocus();
        } else if (edt_lastname.getText().toString().trim().isEmpty()) {
            isValid = false;
            AppClass.snackBarView.snackBarShow(activity, activity.getResources().getString(R.string.vlastname), ContextCompat.getColor(activity, R.color.red));
            edt_lastname.requestFocus();
        } else if (edt_email.getText().toString().trim().isEmpty()) {
            isValid = false;
            AppClass.snackBarView.snackBarShow(activity, activity.getResources().getString(R.string.vemail), ContextCompat.getColor(activity, R.color.red));
            edt_email.requestFocus();
        } else if (!Validation.isValidEmail(edt_email.getText().toString())) {
            isValid = false;
            AppClass.snackBarView.snackBarShow(activity, activity.getResources().getString(R.string.vvemail), ContextCompat.getColor(activity, R.color.red));
            edt_email.requestFocus();
        } else if (!isValidEmail) {
            isValid = false;
            AppClass.snackBarView.snackBarShow(activity, activity.getResources().getString(R.string.email_reg), ContextCompat.getColor(activity, R.color.red));
            edt_email.requestFocus();
        }/* else if (edt_mobile_number.getText().toString().trim().isEmpty()) {
            isValid = false;
            AppClass.snackBarView.snackBarShow(activity, activity.getResources().getString(R.string.vmobile_number), ContextCompat.getColor(activity,R.color.red));
            edt_mobile_number.requestFocus();
        } else if (phone_code.isEmpty()) {
            isValid = false;
            AppClass.snackBarView.snackBarShow(activity, activity.getResources().getString(R.string.vcountrycode), ContextCompat.getColor(activity,R.color.red));
        } */ else if (edt_password.getText().toString().trim().isEmpty()) {
            isValid = false;
            AppClass.snackBarView.snackBarShow(activity, activity.getResources().getString(R.string.vpswd), ContextCompat.getColor(activity, R.color.red));
            edt_password.requestFocus();
        } else if (edt_confirm_password.getText().toString().trim().isEmpty()) {
            isValid = false;
            AppClass.snackBarView.snackBarShow(activity, activity.getResources().getString(R.string.vcpswd), ContextCompat.getColor(activity, R.color.red));
            edt_confirm_password.requestFocus();
        } else if (edt_password.getText().toString().trim().length() < 6 || edt_confirm_password.getText().toString().trim().length() < 6) {
            isValid = false;
            AppClass.snackBarView.snackBarShow(activity, activity.getResources().getString(R.string.vpswd_6), ContextCompat.getColor(activity, R.color.red));
        } else if (!edt_password.getText().toString().trim().equals(edt_confirm_password.getText().toString().trim())) {
            isValid = false;
            AppClass.snackBarView.snackBarShow(activity, activity.getResources().getString(R.string.vpswd_match), ContextCompat.getColor(activity, R.color.red));
        }

        return isValid;
    }

    private void checkEmailExists() {

        if (AppClass.isInternetConnectionAvailable()) {

            ApiCall.getInstance().checkEmailExists(activity, edt_email.getText().toString(), new IApiCallback() {
                @Override
                public void onSuccess(Object data) {

                    isValidEmail = true;
                    activity.saveStep1Data(edt_firstname.getText().toString(),
                            edt_lastname.getText().toString(),
                            edt_email.getText().toString(),
                            phone_code,
                            edt_mobile_number.getText().toString(),
                            gender,
                            edt_residential_address.getText().toString(),
                            edt_password.getText().toString());
                }

                @Override
                public void onFailure(boolean isFalseFromService, String errorMessage) {

                    isValidEmail = false;
                    AppClass.snackBarView.snackBarShow(activity, errorMessage, ContextCompat.getColor(activity, R.color.red));

                    /*if (StringUtils.isNotEmpty(errorMessage)) {
                        DialogUtils.openDialog(activity, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                            @Override
                            public void onNegativeBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }

                            @Override
                            public void onPositiveBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }
                        });
                    }*/
                }
            }, true);


        } else {
            AppClass.snackBarView.snackBarShow(activity, getString(R.string.nonetwork));
        }


    }

    class DialogGender extends Dialog {

        private Activity activity;

        private TextView txt_male,
                txt_female;

        private DialogGender(Activity a) {
            super(a);
            this.activity = a;
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.dialog_gender);

            txt_male = findViewById(R.id.txt_male);
            txt_female = findViewById(R.id.txt_female);

            getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

            txt_male.setTypeface(AppClass.lato_regular);
            txt_female.setTypeface(AppClass.lato_regular);

            txt_male.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    txt_gender_value.setText(activity.getResources().getString(R.string.male));
                    gender = "1";
                    dismiss();
                }
            });

            txt_female.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    txt_gender_value.setText(activity.getResources().getString(R.string.female));
                    gender = "2";
                    dismiss();
                }
            });

        }


    }

    class DialogCountryCode extends Dialog {

        private Activity activity;

        private EditText et_search_country;
        private RecyclerView recyclerview_country;

        private DialogCountryCode(Activity a) {
            super(a);
            this.activity = a;
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.dialog_country_code);
            getWindow().setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.MATCH_PARENT);

            WindowManager.LayoutParams params = this.getWindow().getAttributes();
            //params.x = -100;
            params.y = -100;
            this.getWindow().setAttributes(params);


            et_search_country = findViewById(R.id.et_search_country);
            et_search_country.setTypeface(AppClass.lato_regular);

            recyclerview_country = findViewById(R.id.recyclerview_country);
            LinearLayoutManager layoutManager = new LinearLayoutManager(activity);
            recyclerview_country.setLayoutManager(layoutManager);
            countryAdapter = new CountryAdapter(activity, countryData, new CountryAdapter.CountrySelection() {
                @Override
                public void onCountrySelection(String countryCode, String countryName, String code) {

                    dismiss();
                    phone_code = code;
                    txt_country_code.setText("+" + countryCode);
                }
            });
            recyclerview_country.setAdapter(countryAdapter);

            et_search_country.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    countryAdapter.getFilter().filter(s.toString());
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });


        }


    }


}
