package com.proclapp.signup.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.model.EducationalQualificationModel;
import com.proclapp.signup.SignUpActivity;
import com.proclapp.signup.adapter.EducationalQualificationAdapter;
import com.proclapp.utils.KeyBoardUtils;
import com.proclapp.utils.Logger;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;

public class SignUpStep2Fragment extends Fragment implements View.OnClickListener {

    public static ArrayList<EducationalQualificationModel> qualificationArrayList;
    private View view;
    private TextView txt_educational_qualifications,
            txt_add,
            btn_add_professional_info,
            btn_save;
    private RecyclerView rv_educational_qualifications;
    private EducationalQualificationAdapter qualificationAdapter;
    private SignUpActivity activity;
    private ArrayList<String> yearList;
    private ProgressDialog progressDialog;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_sign_up_step2, container, false);

        activity = (SignUpActivity) getActivity();

        progressDialog = new ProgressDialog(activity);
        progressDialog.setMessage(activity.getResources().getString(R.string.loading));

        initView();
        initStyle();
        initListener();


        return view;
    }

    private void initView() {
        txt_educational_qualifications = view.findViewById(R.id.txt_educational_qualifications);
        txt_add = view.findViewById(R.id.txt_add);
        btn_add_professional_info = view.findViewById(R.id.btn_add_professional_info);
        btn_save = view.findViewById(R.id.btn_save);
        rv_educational_qualifications = view.findViewById(R.id.rv_educational_qualifications);
        rv_educational_qualifications.setNestedScrollingEnabled(false);

        LinearLayoutManager layoutManager = new LinearLayoutManager(activity);
        rv_educational_qualifications.setLayoutManager(layoutManager);

        yearList = new ArrayList<>();

        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
        int currentYear = calendar.get(Calendar.YEAR);

        for (int i = 0; i < 100; i++) {

            yearList.add(String.valueOf(currentYear));
            currentYear--;
        }

        qualificationArrayList = new ArrayList<>();
        EducationalQualificationModel model = new EducationalQualificationModel();
        model.setEducational_qualification("");
        model.setPassing_year("");
        qualificationArrayList.add(0, model);

        qualificationAdapter = new EducationalQualificationAdapter(activity, this, yearList);
        rv_educational_qualifications.setAdapter(qualificationAdapter);
    }


    private void initStyle() {

        txt_educational_qualifications.setTypeface(AppClass.lato_light);
        txt_add.setTypeface(AppClass.lato_regular);
        btn_add_professional_info.setTypeface(AppClass.lato_regular);
        btn_save.setTypeface(AppClass.lato_regular);
    }

    private void initListener() {
        txt_add.setOnClickListener(this);
        btn_add_professional_info.setOnClickListener(this);
        btn_save.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        KeyBoardUtils.closeSoftKeyboard(activity);
        switch (view.getId()) {

            case R.id.txt_add:

                EducationalQualificationModel model = new EducationalQualificationModel();
                model.setEducational_qualification("");
                model.setPassing_year("");
                Logger.d("getItemCount==" + (qualificationAdapter.getItemCount()));
                qualificationArrayList.add(qualificationAdapter.getItemCount(), model);


                if (qualificationAdapter.getItemCount() > 2) {
                    qualificationAdapter.notifyItemInserted(qualificationAdapter.getItemCount());
                } else {
                    qualificationAdapter.notifyDataSetChanged();
                }

                rv_educational_qualifications.scrollToPosition(qualificationArrayList.size());
                break;

            case R.id.btn_add_professional_info:

                break;

            case R.id.btn_save:

                JSONArray array = new JSONArray();

                for (int i = 0; i < qualificationArrayList.size(); i++) {
                    try {
                        JSONObject jsonObject = new JSONObject();

                        if (!qualificationArrayList.get(i).getEducational_qualification().isEmpty()) {
                            jsonObject.put("edu_qua", qualificationArrayList.get(i).getEducational_qualification());
                            jsonObject.put("year", qualificationArrayList.get(i).getPassing_year());

                            array.put(jsonObject);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                Logger.d("array ==>" + array);
                if (array.length() > 0) {
                    activity.saveStep2Data(array);
                } else {
                    AppClass.snackBarView.snackBarShow(activity, getString(R.string.please_add_atleast_one_education_detail));
                }
                break;
        }
    }

}
