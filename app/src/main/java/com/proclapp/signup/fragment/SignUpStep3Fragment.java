package com.proclapp.signup.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.model.CategoryModel;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.signup.adapter.CategoryAdapter;
import com.proclapp.utils.AlertDialogInterface;
import com.proclapp.utils.DialogUtils;
import com.proclapp.utils.Logger;
import com.proclapp.utils.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;

@SuppressLint("ValidFragment")
public class SignUpStep3Fragment extends Fragment implements View.OnClickListener {

    private View view;
    private CategoryAdapter categoryAdapter;
    private TextView txt_select_interested_category;
    private RecyclerView rv_category_list;
    private Button btn_save;
    private Activity activity;
    private ProgressDialog progressDialog;
    private ArrayList<CategoryModel> categoryList = new ArrayList<>();
    private EditText edt_search;
    private FinalSignUp finalSignUp;

    @SuppressLint("ValidFragment")
    public SignUpStep3Fragment(FinalSignUp finalSignUp) {
        this.finalSignUp = finalSignUp;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_sign_up_step3, container, false);
        this.activity = getActivity();
        progressDialog = new ProgressDialog(activity);
        progressDialog.setMessage(activity.getResources().getString(R.string.loading));

        initView();
        callAPICategoryList();

        return view;
    }

    private void initView() {
        txt_select_interested_category = view.findViewById(R.id.txt_select_interested_category);
        btn_save = view.findViewById(R.id.btn_save);
        rv_category_list = view.findViewById(R.id.rv_category_list);
        edt_search = view.findViewById(R.id.edt_search);
        GridLayoutManager layoutManager = new GridLayoutManager(activity, 3);
        rv_category_list.setLayoutManager(layoutManager);

        txt_select_interested_category.setTypeface(AppClass.lato_regular);
        edt_search.setTypeface(AppClass.lato_regular);
        btn_save.setTypeface(AppClass.lato_regular);
        btn_save.setOnClickListener(this);

        edt_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (categoryAdapter != null) {
                    categoryAdapter.getFilter().filter(charSequence);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    private void setCategoryList() {
        categoryAdapter = new CategoryAdapter(activity, categoryList, new CategoryAdapter.CategorySelection() {
            @Override
            public void onCategorySelection(String countryCode, String countryName) {

            }
        }, new ArrayList<String>());
        rv_category_list.setAdapter(categoryAdapter);
    }

    private void callAPICategoryList() {

        if (AppClass.isInternetConnectionAvailable()) {

            HashMap<String, String> request = new HashMap<>();
            request.put("user_id", AppClass.preferences.getUserId());

            ApiCall.getInstance().getCategories(activity, request, new IApiCallback() {
                @Override
                public void onSuccess(Object data) {

                    categoryList.clear();
                    categoryList.addAll((ArrayList<CategoryModel>) data);

                    setCategoryList();

                }

                @Override
                public void onFailure(boolean isFalseFromService, String errorMessage) {
                    if (StringUtils.isNotEmpty(errorMessage)) {
                        DialogUtils.openDialog(activity, errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                            @Override
                            public void onNegativeBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }

                            @Override
                            public void onPositiveBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }
                        });
                    }
                }
            }, true);


        } else {
            AppClass.snackBarView.snackBarShow(activity, getString(R.string.nonetwork));
        }


    }

    @Override
    public void onClick(View view) {



        switch (view.getId()) {

            case R.id.btn_save:

                StringBuilder stringBuilder = new StringBuilder();
                String prefix = "";

                for (int i = 0; i < categoryList.size(); i++) {

                    if (categoryList.get(i).getIs_selected() == 1) {
                        stringBuilder.append(prefix + categoryList.get(i).getCategory_id());
                        prefix = ",";
                    }
                }

                Logger.d("selected category ==>" + stringBuilder);

                if (StringUtils.isNotEmpty(stringBuilder.toString()))
                    finalSignUp.callSignUp(stringBuilder.toString());
                else
                    AppClass.snackBarView.snackBarShow(activity, getString(R.string.please_select_atleast_one_category));

                break;
        }
    }

    public interface FinalSignUp {
        void callSignUp(String selected_category_ids);
    }
}
