package com.proclapp.socket;

/**
 * Created by Sachin on 11/5/19.
 * Prismetric Technology, Gandhinagar, Gujarat
 */
public enum SocketEmitType {
    get_conversations("get_conversations"),
    send_msg("send_msg"),
    msg_seen_all("msg_seen_all");

    public String NAME;

    SocketEmitType(String name) {
        this.NAME = name;
    }
}
