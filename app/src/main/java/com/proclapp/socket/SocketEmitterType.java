package com.proclapp.socket;

/**
 * Created by Sachin on 11/5/19.
 * Prismetric Technology, Gandhinagar, Gujarat
 */
public enum SocketEmitterType {
    _coversation("_conversations"),
    new_msg("new_msg");


    public String NAME;

    SocketEmitterType(String name) {
        this.NAME = name;
    }
}
