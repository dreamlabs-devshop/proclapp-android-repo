package com.proclapp.socket;


import android.app.ProgressDialog;
import android.content.Context;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.utils.Logger;

import org.json.JSONException;
import org.json.JSONObject;

import io.socket.client.Ack;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

/**
 * Created by Sachin on 10/5/19.
 * Prismetric Technology, Gandhinagar, Gujarat
 */
public class SocketIOConnectionHelper {

    private final Context context;
    private String CONNECTED = "connected";
    private Socket socket;
    private String url = "https://www.proclapp.com:1333?user_id=";
    private OnSocketResponseListerner onSocketResponseListerner;
    private ProgressDialog progressDialog;

    public SocketIOConnectionHelper(Context context) {
        this.context = context;
        try {
            socket = IO.socket(url + AppClass.preferences.getUserId());
            socket.connect();
            setBasicListerner();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setBasicListerner() {
        socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Logger.e("Socket.io : connect call");
            }
        }).on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Logger.e("Socket.io : disconnect call");
            }
        }).on(CONNECTED, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Logger.e("Socket.io : connected call");
            }
        }).on(Socket.EVENT_CONNECT_ERROR, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Logger.e("Socket.io : connection error call");
            }
        }).on(Socket.EVENT_CONNECT_TIMEOUT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Logger.e("Socket.io : time out call");
            }
        }).on(Socket.EVENT_RECONNECT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Logger.e("Socket.io : reconnect call");
            }
        });
    }

    private void removeBasicListerner() {
        socket.off(Socket.EVENT_CONNECT)
                .off(Socket.EVENT_DISCONNECT)
                .off(CONNECTED)
                .off(Socket.EVENT_CONNECT_ERROR)
                .off(Socket.EVENT_CONNECT_TIMEOUT)
                .off(Socket.EVENT_RECONNECT);
    }


    public void setAppListerner() {
        if (!socket.hasListeners(SocketEmitterType._coversation.NAME)) {
            socket.on(SocketEmitterType._coversation.NAME, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    if (onSocketResponseListerner != null) {
                        onSocketResponseListerner.onSocketResponse(SocketEmitterType._coversation, args[0]);
                    }
                    Logger.e(SocketEmitterType._coversation.NAME + " : " + args[0].toString());
                }
            }).on(SocketEmitterType.new_msg.NAME, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    if (onSocketResponseListerner != null) {
                        onSocketResponseListerner.onSocketResponse(SocketEmitterType.new_msg, args[0]);
                    }
                    Logger.e(SocketEmitterType.new_msg.NAME + " : " + args[0].toString());
                }
            });
        }
    }

    public void removeAppListerner() {
        socket.off(SocketEmitterType._coversation.NAME);
    }

    public void removeNewMessageListerner() {
        socket.off(SocketEmitterType.new_msg.NAME);
    }

    public void setEmitData(final SocketEmitType type, Object object, final OnSocketAckListerner onSocketAckListerner) {
        this.setEmitData(type, object, onSocketAckListerner, false);
    }

    public void setEmitData(final SocketEmitType type, Object object, final OnSocketAckListerner onSocketAckListerner, boolean isProgress) {
        if (isProgress)
            showProgress();
        try {
            socket.emit(type.NAME, new JSONObject(object.toString()), new Ack() {
                @Override
                public void call(Object... args) {
                    if (onSocketAckListerner != null) {
                        onSocketAckListerner.onSocketAck(type, args[0]);
                    }
                    //    Logger.e(type.NAME + " ACK : " + args[0].toString());
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (isProgress)
            dismissProgress();

    }

    public void disconnectAllConnection() {
        socket.disconnect();
        removeBasicListerner();
        removeAppListerner();
    }


    public void setOnRideResponseListerner(OnSocketResponseListerner onSocketResponseListerner) {
        this.onSocketResponseListerner = onSocketResponseListerner;
    }

    public void recheckConnection() {
        socket.connect();
    }

    private void showProgress() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage(context.getString(R.string.please_wait));
        }
        progressDialog.show();
    }

    private void dismissProgress() {
        if (progressDialog != null)
            progressDialog.dismiss();
    }

    public interface OnSocketResponseListerner {
        void onSocketResponse(SocketEmitterType type, Object object);
    }

    public interface OnSocketAckListerner {
        void onSocketAck(SocketEmitType type, Object object);
    }
}

