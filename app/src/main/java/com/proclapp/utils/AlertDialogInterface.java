package com.proclapp.utils;

import android.content.DialogInterface;

/**
 * Created by Androiddev on 3/7/2017.
 */

public interface AlertDialogInterface {
    void onNegativeBtnClicked(DialogInterface alrt);

    void onPositiveBtnClicked(DialogInterface alrt);
}
