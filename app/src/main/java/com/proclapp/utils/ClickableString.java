package com.proclapp.utils;

import androidx.annotation.NonNull;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;

public class ClickableString extends ClickableSpan {
    private View.OnClickListener mListener;
    private String textToHighlight;

    public ClickableString(String textToHighlight, View.OnClickListener listener) {
        this.textToHighlight = textToHighlight;
        mListener = listener;
    }

    @Override
    public void onClick(View v) {
        if (mListener != null) {
            v.setTag(textToHighlight);
            mListener.onClick(v);
        }
    }

    @Override
    public void updateDrawState(@NonNull TextPaint ds) {
        //super.updateDrawState(ds);
        ds.setUnderlineText(false); // set to false to remove underline
    }
}