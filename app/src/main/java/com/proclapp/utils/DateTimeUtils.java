package com.proclapp.utils;

import android.app.TimePickerDialog;
import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import android.widget.NumberPicker;

import com.proclapp.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import static com.bumptech.glide.util.Preconditions.checkArgument;

public class DateTimeUtils {


    /**
     * For chang date pattern
     *
     * @param inputPattern  : date pattern of date
     * @param outputPattern : in which pattern u want
     * @param value         : date in string
     * @return date string in outputPattern
     */
    public static String changeDateTimeFormat(String inputPattern, String outputPattern, String value) {

        try {
            SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern, Locale.getDefault());
            SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern, Locale.getDefault());
            Date date = inputFormat.parse(value);
            return outputFormat.format(date);

        } catch (ParseException e) {
            e.printStackTrace();
            return "";

        }
    }


    /**
     * get current date and time
     *
     * @param dateFormat : pattern
     * @return : date in string
     */
    public static String getCurrentDate(String dateFormat) {
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, Locale.getDefault());
        return sdf.format(c);
    }

    public static String convertUTCtoLocalTimeZone(String date, String inputPattern) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(inputPattern, Locale.getDefault());
        Date myDate = null;
        try {
            simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            myDate = simpleDateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return new SimpleDateFormat(inputPattern, Locale.getDefault()).format(myDate); // Note: Use new DateFormat
    }

    /**
     * get current UTC date and time
     *
     * @param dateFormat :pattern
     * @return date/time in string
     */
    public static String getCurrentUTCDateTime(String dateFormat) {
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, Locale.getDefault());
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        return sdf.format(new Date());
    }


    /**
     * get default date and time from UTC to default locale
     *
     * @param inputdate    : UTC time in string
     * @param inputFormat  : UTC time pattern
     * @param outputFormat : output pattern
     * @return : date string in outputFormat
     */
    public static String changeUTCDateTimeFormat(String inputdate, String inputFormat, String outputFormat) {

        try {

            SimpleDateFormat inputdf = new SimpleDateFormat(inputFormat, Locale.getDefault());
            SimpleDateFormat outputdf = new SimpleDateFormat(outputFormat, Locale.getDefault());
            inputdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date date = inputdf.parse(inputdate);
            outputdf.setTimeZone(TimeZone.getDefault());
            return outputdf.format(date);

        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }

    }

    public static boolean compareTwoDates(String datePattern, String strDate1, String strDate2) {

        boolean flag = false;

        try {
            SimpleDateFormat sdf = new SimpleDateFormat(datePattern, Locale.getDefault());
            Date d1 = sdf.parse(strDate1);
            Date d2 = sdf.parse(strDate2);

            long date = d1.getTime() - d2.getTime();

            System.out.println("1. " + sdf.format(d1).toUpperCase());
            System.out.println("2. " + sdf.format(d2).toUpperCase());

            if (date < 0) {
                System.out.println("d1 is before d2");
                flag = true;
            } else if (date > 0) {
                System.out.println("d1 is after d2");
            } else {
                System.out.println("d1 is equal to d2");
                flag = true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return flag;
    }


    public static boolean compareTwoTimes(String timePattern, String startTime, String endTime) {

        boolean flag = false;

        try {
            SimpleDateFormat sdf = new SimpleDateFormat(timePattern, Locale.getDefault());
            Date d1 = sdf.parse(startTime);
            Date d2 = sdf.parse(endTime);

            long date = d1.getTime() - d2.getTime();

            System.out.println("1. " + sdf.format(d1).toUpperCase());
            System.out.println("2. " + sdf.format(d2).toUpperCase());

            if (date < 0) {
                System.out.println("d1 is before d2");
                flag = true;
            } else {
                System.out.println("d1 is after d2");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return flag;
    }


    public static long calculateTimeBetweenTwoDate(String startDate, String endDate, String format) {

        SimpleDateFormat myFormat = new SimpleDateFormat(format, Locale.getDefault());

        long diff = 0;
        try {
            Date date1 = myFormat.parse(startDate);
            Date date2 = myFormat.parse(endDate);
            diff = date2.getTime() - date1.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return diff;
    }

    /**
     * set interval in timepicker dialog
     *
     * @param timePicker      - time picker dialog
     * @param interval_minute - interval in minute
     */
    public static void setTimePickerInterval(TimePickerDialog timePicker, int interval_minute) {
        try {

            NumberPicker minutePicker = (NumberPicker) timePicker.findViewById(Resources.getSystem().getIdentifier(
                    "minute", "id", "android"));
            minutePicker.setMinValue(0);
            minutePicker.setMaxValue((60 / interval_minute) - 1);
            List<String> displayedValues = new ArrayList<String>();
            for (int i = 0; i < 60; i += interval_minute) {
                displayedValues.add(String.format("%02d", i));
            }
            minutePicker.setDisplayedValues(displayedValues.toArray(new String[0]));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * calculate hours b/w two times ,  both time pattern wiill be same
     *
     * @param timePattern - input time format
     * @param startTime   - start time
     * @param endTime     - end time
     * @return hours in string
     */
    public static String calculateHoursBetWeenTwoTimes(String timePattern, String startTime, String endTime) {

        try {

            SimpleDateFormat sdf = new SimpleDateFormat(timePattern, Locale.getDefault());
            Date d1 = sdf.parse(startTime);
            Date d2 = sdf.parse(endTime);

            long mills = d2.getTime() - d1.getTime();
            long hours = (mills / (1000 * 60 * 60));
            long mins = (mills / (1000 * 60)) % 60;

            String m = "0";

            if (mins == 30) {
                m = "5";
            }

            return hours + "." + m;

        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }

    }

    /**
     * checking is selected time is between two times or not
     * all input time pattern will be same
     *
     * @param pattern           - input string pattern
     * @param selectedTime      - seleted time
     * @param selectedStartTime - selected start time
     * @param selectedEndTime   - selected end time
     * @return true(if time between two times) / false(if time is not between two times)
     */
    public static boolean isTimeBetweenTwoTimes(String pattern, String selectedTime, String selectedStartTime, String selectedEndTime) {

        try {

            SimpleDateFormat sdf = new SimpleDateFormat(pattern, Locale.getDefault());

            Date time = sdf.parse(selectedTime);
            Date startTime = sdf.parse(selectedStartTime);
            Date endTime = sdf.parse(selectedEndTime);

            return (time.before(startTime)) || (time.after(endTime));

        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }

    }

    public static String convert24HourTo12Hour(String _24HourTime) {
        return changeDateTimeFormat("HH:mm", "hh:mm a", _24HourTime);
    }

    public static String convert12HourTo24Hour(String _12HourTime) {
        return changeDateTimeFormat("hh:mm a", "HH:mm", _12HourTime);
    }

    public static String getDayOfMonthSuffix(int n) {
        checkArgument(n >= 1 && n <= 31, "illegal day of month: " + n);
        if (n >= 11 && n <= 13) {
            return "th";
        }
        switch (n % 10) {
            case 1:
                return "st";
            case 2:
                return "nd";
            case 3:
                return "rd";
            default:
                return "th";
        }
    }

    public static long converDateToMili(String dateFormate, String strDate) {

        SimpleDateFormat formatter = new SimpleDateFormat(dateFormate, Locale.getDefault());
        formatter.setLenient(false);
        Date d = null;
        long curMillis;
        try {
            d = formatter.parse(strDate);
            curMillis = d.getTime();
        } catch (Exception ex) {
            Log.v("Exception", ex.getLocalizedMessage());
            curMillis = 12345678910L;
        }

        return curMillis;

    }

    public static String getDateInStringFromMilis(long milliSeconds, String dateFormat) {
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat, Locale.getDefault());
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    public static String calculateTimeBetweenTwoDates(Context context, String startDate, boolean isShort) {

        String finalTimeString = "";

        SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()); // 24 hours format
        myFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        try {
            Date date1 = myFormat.parse(startDate);
            Date date2 = myFormat.parse(myFormat.format(Calendar.getInstance().getTime()));
            long diff = date2.getTime() - date1.getTime();

            long seconds = TimeUnit.MILLISECONDS.toSeconds(diff);
            long minutes = TimeUnit.MILLISECONDS.toMinutes(diff);
            long hours = TimeUnit.MILLISECONDS.toHours(diff);
            long days = TimeUnit.MILLISECONDS.toDays(diff);
            long months = days / 30;
            long year = months / 12;

            if (seconds < 60) {
                finalTimeString = context.getResources().getString(R.string.Now);
            } else if (minutes < 60) {
                if (!isShort) {
                    if (minutes == 1) {
                        finalTimeString = minutes + " " + context.getResources().getString(R.string.minute_ago);
                    } else {
                        finalTimeString = minutes + " " + context.getResources().getString(R.string.minutes_ago);
                    }
                } else {
                    finalTimeString = minutes + " " + context.getResources().getString(R.string.m_short);
                }
            } else if (hours < 24) {
                if (!isShort) {
                    if (hours == 1) {
                        finalTimeString = hours + " " + context.getResources().getString(R.string.hour_ago);
                    } else {
                        finalTimeString = hours + " " + context.getResources().getString(R.string.hours_ago);
                    }
                } else {
                    finalTimeString = hours + " " + context.getResources().getString(R.string.h_short);
                }
            } else if (days < 30) {
                if (!isShort) {
                    if (days == 1) {
                        finalTimeString = days + " " + context.getResources().getString(R.string.day_ago);
                    } else {
                        finalTimeString = days + " " + context.getResources().getString(R.string.days_ago);
                    }
                } else {
                    finalTimeString = days + " " + context.getResources().getString(R.string.d_short);
                }
            } else if (months < 12) {

                if (months == 1) {
                    if (!isShort) {
                        finalTimeString = months + " " + context.getResources().getString(R.string.month_ago);
                    } else {
                        finalTimeString = months + " " + context.getResources().getString(R.string.mth_short);
                    }
                } else {
                    if (!isShort) {
                        finalTimeString = months + " " + context.getResources().getString(R.string.months_ago);
                    } else {
                        finalTimeString = months + " " + context.getResources().getString(R.string.mths_short);
                    }
                }
            } else {
                if (year == 1) {
                    if (!isShort) {
                        finalTimeString = year + " " + context.getResources().getString(R.string.year_ago);
                    } else {
                        finalTimeString = year + " " + context.getResources().getString(R.string.yr_short);
                    }
                } else {
                    if (!isShort) {
                        finalTimeString = year + " " + context.getResources().getString(R.string.years_ago);
                    } else {
                        finalTimeString = year + " " + context.getResources().getString(R.string.yrs_short);
                    }
                }
            }

        } catch (
                ParseException e) {
            e.printStackTrace();
        }

        return finalTimeString;
    }


    public static String getTimeAgo(String data) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date d = dateFormat.parse(data);
            long time = d.getTime();
            int SECOND_MILLIS = 1000;
            int MINUTE_MILLIS = 60 * SECOND_MILLIS;
            int HOUR_MILLIS = 60 * MINUTE_MILLIS;
            int DAY_MILLIS = 24 * HOUR_MILLIS;
            if (time < 1000000000000L) {
                // if timestamp given in seconds, convert to millis
                time *= 1000;
            }

            long now = System.currentTimeMillis();
            if (time > now || time <= 0) {
                return null;
            }

            // TODO: localize
            final long diff = now - time;
            if (diff < MINUTE_MILLIS) {
                return "just now";
            } else if (diff < 2 * MINUTE_MILLIS) {
                return "a minute ago";
            } else if (diff < 50 * MINUTE_MILLIS) {
                return diff / MINUTE_MILLIS + " minutes ago";
            } else if (diff < 90 * MINUTE_MILLIS) {
                return "an hour ago";
            } else if (diff < 24 * HOUR_MILLIS) {
                return diff / HOUR_MILLIS + " hours ago";
            } else if (diff < 48 * HOUR_MILLIS) {
                return "yesterday";
            } else {
                return diff / DAY_MILLIS + " days ago";
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "";

        }
    }
}
