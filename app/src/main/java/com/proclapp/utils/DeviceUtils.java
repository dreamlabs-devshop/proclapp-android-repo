package com.proclapp.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;

import com.proclapp.BuildConfig;


/**
 * Created by Prismetric on 1/18/2016.
 */
public class DeviceUtils {

    public static String AndroidDeviceId = "";

    @SuppressLint("MissingPermission")
    public static String DeviceId(Context context) {
        if (PermissionData.isPhonePermission(context)) {
            final TelephonyManager mTelephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            if (mTelephony.getDeviceId() != null) {
                AndroidDeviceId = mTelephony.getDeviceId();
            } else {
                AndroidDeviceId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID); //*** use for tablets
            }
        }
        return AndroidDeviceId;
    }

    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }
    }


    private static String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }

    public static boolean isAppInReleaseMode() {
        if (BuildConfig.DEBUG) {
            return false;
        } else {
            return true;
        }
    }

    public static String BuildType() {
        return BuildConfig.BUILD_TYPE;
    }

    public static int VersionCode() {
        return BuildConfig.VERSION_CODE;
    }

    public static String VersionName() {
        return BuildConfig.VERSION_NAME;
    }

    public static String getDeviceType() {
        return "android";
    }

    public static String getDeviceId(Context context) {
        final TelephonyManager mTelephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

        if (mTelephony.getDeviceId() != null) {
            return mTelephony.getDeviceId();  // use for mobiles
        } else {
            return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID); //use for tablets
        }
    }

    public static String getDeviceIdWithoutPermission(Activity activity) {
        String AndroidDeviceId = "";
        AndroidDeviceId = Settings.Secure.getString(activity.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        if (AndroidDeviceId == null) {
            AndroidDeviceId = "";
        }
        return AndroidDeviceId;
    }


    public static int getCurrentVersionCode(Context context) {
        int version = 0;
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            version = pInfo.versionCode;
            Logger.e("getCurrentVersionName" + pInfo.versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }


        Logger.e("getCurrentVersionCode" + version);

        return version;
    }

    public static String getDeviceManufacturerName() {
        Logger.e("getDeviceManufacturerName" + Build.MANUFACTURER);
        return Build.MANUFACTURER;
    }
}


