package com.proclapp.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.Gravity;
import android.widget.Button;
import android.widget.LinearLayout;

import com.proclapp.R;


/**
 * Created by Androiddev on 7/31/2017.
 */

public class DialogUtils {

    public static void openDialog(Activity activity, String msg, String positive_btn_txt, String negative_btn_txt, final AlertDialogInterface alertInterface) {
        try {
            final AlertDialog.Builder ald = new AlertDialog.Builder(activity);
            ald.setMessage(msg);
            ald.setCancelable(false);


            if (StringUtils.isNotEmpty(positive_btn_txt)) {
                ald.setPositiveButton(positive_btn_txt, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        alertInterface.onPositiveBtnClicked(dialog);

                    }
                });
            }
            if (StringUtils.isNotEmpty(negative_btn_txt)) {
                ald.setNegativeButton(negative_btn_txt, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        alertInterface.onNegativeBtnClicked(dialog);
                    }
                });
            }
            //   final AlertDialog dialog_alt = ald.create();

            // ald.show(); //show() should be called before dialog.getButton().
            AlertDialog dialog = ald.create();
            dialog.show();
            // dialog.setCanceledOnTouchOutside(true);
            //  dialog.getButton(dialog.BUTTON_NEGATIVE).setTextColor(neededColor);
            //  dialog.getButton(dialog.BUTTON_POSITIVE).setTextColor(neededColor);

            if (StringUtils.isNotEmpty(positive_btn_txt)) {
                final Button positiveButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
                positiveButton.setTextColor(activity.getResources().getColor(R.color.colorPrimary));
                LinearLayout.LayoutParams positiveButtonLL = (LinearLayout.LayoutParams) positiveButton.getLayoutParams();
                positiveButtonLL.gravity = Gravity.CENTER;
                positiveButton.setLayoutParams(positiveButtonLL);
            }
            if (StringUtils.isNotEmpty(negative_btn_txt)) {
                final Button negativeButton = dialog.getButton(AlertDialog.BUTTON_NEGATIVE);
                negativeButton.setTextColor(activity.getResources().getColor(R.color.colorAccent));
                LinearLayout.LayoutParams positiveButtonLL = (LinearLayout.LayoutParams) negativeButton.getLayoutParams();
                positiveButtonLL.gravity = Gravity.CENTER;
                negativeButton.setLayoutParams(positiveButtonLL);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
