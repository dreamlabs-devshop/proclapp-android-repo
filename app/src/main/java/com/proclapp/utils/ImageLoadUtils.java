package com.proclapp.utils;

import android.content.Context;

import androidx.appcompat.app.AppCompatActivity;

import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

public class ImageLoadUtils {

    public static void imageLoad(Context context, ImageView imageView, String imagePath, int placeHolder) {


        if (!((AppCompatActivity) context).isFinishing())

            Glide.with(context)
                    .load(imagePath).centerCrop()
                    .apply(new RequestOptions().placeholder(placeHolder))
                    .into(imageView);


    }


    public static void imageLoad(Context context, ImageView imageView, String imagePath) {

        if (!((AppCompatActivity) context).isFinishing())
            if (imagePath != null && !imagePath.isEmpty()) {

                Glide.with(context)
                        .load(imagePath)
                        .into(imageView);
            }

    }

}
