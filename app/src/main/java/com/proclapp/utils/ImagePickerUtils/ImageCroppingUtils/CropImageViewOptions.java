package com.proclapp.utils.ImagePickerUtils.ImageCroppingUtils;

import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.Serializable;

/**
 * Created by WINDOWS-D20 on 26-10-2017.
 * The crop image view options that can be changed live.
 */
public final class CropImageViewOptions implements Serializable {

    public CropImageView.ScaleType scaleType = CropImageView.ScaleType.CENTER_INSIDE;

    public CropImageView.CropShape cropShape = CropImageView.CropShape.RECTANGLE;

    public CropImageView.Guidelines guidelines = CropImageView.Guidelines.ON_TOUCH;

    public int aspectRatioX = 1;

    public int aspectRatioY = 1;

    public boolean autoZoomEnabled = true;

    public int maxZoomLevel;

    public boolean fixAspectRatio = true;

    public boolean multitouch = true;

    public boolean showCropOverlay = true;

    public boolean showProgressBar = true;

    public boolean flipHorizontally = false;

    public boolean flipVertically = false;
}
