package com.proclapp.utils.ImagePickerUtils.ImageCroppingUtils;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import androidx.annotation.NonNull;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.proclapp.R;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.proclapp.utils.ImagePickerUtils.ImagePickerBottomSheet;
import com.proclapp.utils.ImagePickerUtils.Utils.SdcardUtils;

import java.io.File;
import java.io.FileOutputStream;

/**
 * Created by Bhavesh on 2-11-2017.
 */
@SuppressLint("ValidFragment")
public class ImageCroppingBottomSheet extends BottomSheetDialogFragment implements View.OnClickListener {
    private View rootView;
    private Context context;

    private ImageView iv_ImageCroppingBottomSheet_rotate;
    private CropImageView civ_ImageCroppingBottomSheet_image;
    private TextView tv_ImageCroppingBottomSheet_crop;

    private ImageCroppingRequest mCroppingRequest;
    private ImageCropListener listener;

    private BottomSheetBehavior mBottomSheetBehavior;

    public ImageCroppingBottomSheet(Context context, ImageCroppingRequest mCroppingRequest, ImageCropListener listener) {
        this.context = context;
        this.mCroppingRequest = mCroppingRequest;
        this.listener = listener;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public static void removeOnGlobalLayoutListener(View v, ViewTreeObserver.OnGlobalLayoutListener listener) {
        if (Build.VERSION.SDK_INT < 16) {
            v.getViewTreeObserver().removeGlobalOnLayoutListener(listener);
        } else {
            v.getViewTreeObserver().removeOnGlobalLayoutListener(listener);
        }
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        rootView = View.inflate(getContext(), R.layout.bottom_sheet_image_cropping, null);
        dialog.setContentView(rootView);

        if (mCroppingRequest != null) {
            if (mCroppingRequest.getImageFile() != null) {
                setBottomSheetLayout();
                initialize();
                showImageForCropping();
                setCropImageViewOptions();

                // Enter Animation
                startEnterAnimation();
            }
        } else {
            dismiss();
        }
    }

    private void setBottomSheetLayout() {
        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) rootView.getParent()).getLayoutParams();
        CoordinatorLayout.Behavior behavior = params.getBehavior();

        if (behavior != null && behavior instanceof BottomSheetBehavior) {
            mBottomSheetBehavior = (BottomSheetBehavior) behavior;
            mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                @Override
                public void onStateChanged(@NonNull View bottomSheet, int newState) {
                    switch (newState) {
                        case BottomSheetBehavior.STATE_COLLAPSED:
                            break;
                        case BottomSheetBehavior.STATE_SETTLING:
                            break;
                        case BottomSheetBehavior.STATE_EXPANDED:
                            break;
                        case BottomSheetBehavior.STATE_HIDDEN:
                            dismiss();
                            break;
                        case BottomSheetBehavior.STATE_DRAGGING:
                            break;
                        default:
                            break;
                    }
                }

                @Override
                public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                    // Log.d("BSB", "sliding " + slideOffset);
                }
            });

            // Used for Show full layout (Height need to measure)
            rootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    removeOnGlobalLayoutListener(rootView, this);
                    int height = rootView.getMeasuredHeight();
                    mBottomSheetBehavior.setPeekHeight(height);
                }
            });
        }
    }

    private void initialize() {
        iv_ImageCroppingBottomSheet_rotate = (ImageView) rootView.findViewById(R.id.iv_ImageCroppingBottomSheet_rotate);
        civ_ImageCroppingBottomSheet_image = (CropImageView) rootView.findViewById(R.id.civ_ImageCroppingBottomSheet_image);
        tv_ImageCroppingBottomSheet_crop = (TextView) rootView.findViewById(R.id.tv_ImageCroppingBottomSheet_crop);

        iv_ImageCroppingBottomSheet_rotate.setOnClickListener(this);
        tv_ImageCroppingBottomSheet_crop.setOnClickListener(this);
    }

    private void showImageForCropping() {
        Bitmap myBitmap = BitmapFactory.decodeFile(mCroppingRequest.getImageFile().getAbsolutePath());
        civ_ImageCroppingBottomSheet_image.setImageBitmap(myBitmap);

        civ_ImageCroppingBottomSheet_image.setOnCropImageCompleteListener(new CropImageView.OnCropImageCompleteListener() {
            @Override
            public void onCropImageComplete(CropImageView view, CropImageView.CropResult result) {
                handleCropResult(result);
            }
        });
    }

    /**
     * Set the options of the crop image view to the given values.
     */
    public void setCropImageViewOptions() {
        CropImageViewOptions options = mCroppingRequest.getCropImageViewOptions();

        civ_ImageCroppingBottomSheet_image.setScaleType(options.scaleType);
        civ_ImageCroppingBottomSheet_image.setCropShape(options.cropShape);
        civ_ImageCroppingBottomSheet_image.setGuidelines(options.guidelines);
        civ_ImageCroppingBottomSheet_image.setAspectRatio(options.aspectRatioX, options.aspectRatioY);
        civ_ImageCroppingBottomSheet_image.setFixedAspectRatio(options.fixAspectRatio);
        civ_ImageCroppingBottomSheet_image.setMultiTouchEnabled(options.multitouch);
        civ_ImageCroppingBottomSheet_image.setShowCropOverlay(options.showCropOverlay);
        civ_ImageCroppingBottomSheet_image.setShowProgressBar(options.showProgressBar);
        civ_ImageCroppingBottomSheet_image.setAutoZoomEnabled(options.autoZoomEnabled);
        civ_ImageCroppingBottomSheet_image.setMaxZoom(options.maxZoomLevel);
        civ_ImageCroppingBottomSheet_image.setFlippedHorizontally(options.flipHorizontally);
        civ_ImageCroppingBottomSheet_image.setFlippedVertically(options.flipVertically);
    }

    private void startEnterAnimation() {
        ImagePickerBottomSheet.ViewAnimImagePicker.sequentialZoomInAnimation(
                300,
                500,
                civ_ImageCroppingBottomSheet_image,
                tv_ImageCroppingBottomSheet_crop,
                iv_ImageCroppingBottomSheet_rotate
        );
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_ImageCroppingBottomSheet_rotate:
                civ_ImageCroppingBottomSheet_image.rotateImage(90);
                break;
            case R.id.tv_ImageCroppingBottomSheet_crop:
                civ_ImageCroppingBottomSheet_image.getCroppedImageAsync(); // Check setOnCropImageCompleteListener()
                break;
            default:
                break;
        }
    }

    /**
     * By Bhavesh
     * <br/>
     * <br/>
     * Cropped image result handle by this method<p/>
     * <p>
     * Handle two type of cropped image<br/>
     * 1. {@link CropImageView.CropShape#RECTANGLE}<br/>
     * 2. {@link CropImageView.CropShape#OVAL}<br/>
     *
     * @param result
     */
    private void handleCropResult(CropImageView.CropResult result) {
        if (result.getError() == null) {
            Bitmap bitmap;
            if (civ_ImageCroppingBottomSheet_image.getCropShape() == CropImageView.CropShape.OVAL) {
                bitmap = CropImage.toOvalBitmap(result.getBitmap());
            } else {
                bitmap = result.getBitmap();
            }

            if (bitmap != null) {
                File croppedImageFile = SdcardUtils.returnImageFileName();
                try {
                    FileOutputStream out = new FileOutputStream(croppedImageFile);
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);

                    listener.onImageCropDone(croppedImageFile);
                    dismiss();

                    out.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                dismiss();
            }
        } else {
            dismiss();

            Log.e("AIC", "Failed to crop image", result.getError());
            Toast.makeText(context, "Image crop failed: " + result.getError().getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public interface ImageCropListener {

        /*
        -> File to Bitmap convert

        Bitmap myBitmap = BitmapFactory.decodeFile(croppedImageFile.getAbsolutePath());
        myImage.setImageBitmap(myBitmap);
        */

        void onImageCropDone(File imageFile);
    }
}