package com.proclapp.utils.ImagePickerUtils.ImageCroppingUtils;

import androidx.annotation.NonNull;

import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;

/**
 * Created By Bhavesh on 31-10-2017.
 */

public class ImageCroppingRequest {
    /**
     * Image File which you want to crop
     */
    private File imageFile = null;

    /**
     * Cropping options for image cropping (Default value already set)
     */
    private CropImageViewOptions cropImageViewOptions = new CropImageViewOptions();

    /**
     * By Bhavesh<br/>
     * <p/>
     * <p>
     * Empty constructor
     */
    public ImageCroppingRequest() {
    }

    /**
     * By Bhavesh<br/>
     * <p/>
     *
     * @param imageFile Must be not null
     */
    public ImageCroppingRequest(@NonNull File imageFile) {
        this.imageFile = imageFile;
    }

    /**
     * By Bhavesh<br/>
     * <p/>
     * <p>
     * Get the Image file for cropping
     *
     * @return
     */
    public File getImageFile() {
        return imageFile;
    }

    /**
     * By Bhavesh<br/>
     * <p/>
     * <p>
     * Set the Image file for cropping
     *
     * @return
     */
    public void setImageFile(@NonNull File imageFile) {
        this.imageFile = imageFile;
    }

    /**
     * By Bhavesh<br/>
     * <p/>
     * <p>
     * Get all options of cropping.
     * <br/>If user not set then it will use default options
     *
     * @return
     */
    public CropImageViewOptions getCropImageViewOptions() {
        return cropImageViewOptions;
    }

    /**
     * By Bhavesh<br/>
     * <p/>
     *
     * @param scaleType
     * @return
     */
    public ImageCroppingRequest setScaleType(CropImageView.ScaleType scaleType) {
        cropImageViewOptions.scaleType = scaleType;
        return this;
    }

    /**
     * By Bhavesh<br/>
     * <p/>
     * <p>
     * Default shape is RECTANGLE
     *
     * @param cropShape RECTANGLE or OVAL
     * @return
     */
    public ImageCroppingRequest setCropShape(CropImageView.CropShape cropShape) {
        cropImageViewOptions.cropShape = cropShape;
        return this;
    }

    /**
     * By Bhavesh<br/>
     * <p/>
     * <p>
     * For e.g. 1:1 (SQUARE), 2:5 etc
     * <p>
     * Sets the both the X and Y values of the aspectRatio.<br>
     * Sets fixed aspect ratio to TRUE.
     *
     * @param aspectRatioX int that specifies the new X value of the aspect ratio
     * @param aspectRatioY int that specifies the new Y value of the aspect ratio
     * @return
     */
    public ImageCroppingRequest setFixAspectRatio(int aspectRatioX, int aspectRatioY) {
        if (aspectRatioX > 0) {
            cropImageViewOptions.aspectRatioX = aspectRatioX;
        }
        if (aspectRatioY > 0) {
            cropImageViewOptions.aspectRatioY = aspectRatioY;
        }

        cropImageViewOptions.fixAspectRatio = true;

        return this;
    }

    /**
     * By Bhavesh<br/>
     * <p/>
     * <p>
     * Default Multi touch is enable
     *
     * @param multitouch true for enable, false for disable
     * @return
     */
    public ImageCroppingRequest setMultitouch(boolean multitouch) {
        cropImageViewOptions.multitouch = multitouch;
        return this;
    }
}
