package com.proclapp.utils.ImagePickerUtils;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import androidx.annotation.NonNull;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.proclapp.R;
import com.proclapp.utils.ImagePickerUtils.ImageCroppingUtils.ImageCroppingBottomSheet;
import com.proclapp.utils.ImagePickerUtils.ImageCroppingUtils.ImageCroppingRequest;
import com.proclapp.utils.ImagePickerUtils.Utils.ContentResolverUtils;
import com.proclapp.utils.ImagePickerUtils.Utils.ImageUtils;
import com.proclapp.utils.ImagePickerUtils.Utils.ScalingUtilities;
import com.proclapp.utils.ImagePickerUtils.Utils.SdcardUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Bhavesh on 31-10-2017.
 */
@SuppressLint("ValidFragment")
public class ImagePickerBottomSheet extends BottomSheetDialogFragment implements View.OnClickListener {

    private final int RESIZE_BITMAP_SIZE = 1000;
    private final int REQUEST_PERMISSION_ALL = 10;
    private final int
            CODE_PICK_PHOTO_CAMERA = 1,
            CODE_PICK_PHOTO_GALLERY = 2;

    /**
     * How to use
     * <p>
     * <p>
     * new ImagePickerBottomSheet(context, new ImagePickerBottomSheet.ImagePickListener() {
     * @Override public void onPickImage(File imageFile) {
     * if (imageFile != null) {
     * Bitmap myBitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath());
     * iv_MainActivity_selectedImage.setImageBitmap(myBitmap);
     * }
     * }
     * }).show(getSupportFragmentManager(), "");
     */

    private View rootView;
    private AppCompatActivity context;
    private ImagePickListener pickListener;
    private BottomSheetBehavior mBottomSheetBehavior;
    private ImageView
            iv_ImagePickerBottomSheet_gallery,
            iv_ImagePickerBottomSheet_camera;
    private String[] PERMISSIONS_LIST = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
    private RelativeLayout
            rl_ImagePickerBottomSheet_gallery,
            rl_ImagePickerBottomSheet_camera;

    private Uri CAMERA_IMAGE_URI;

    private boolean isImageCroppingEnable = true;
    private ImageCroppingRequest croppingReqOptions;

    private ValueAnimator colorAnimator;

    /**
     * By Bhavesh
     * <br/>
     * Pick image from Gallery or Camera <br/>
     * Also support for cropping ({@link ImagePickerBottomSheet#isImageCroppingEnable})
     *
     * @param context
     * @param pickListener return final image which selected by user
     */
    public ImagePickerBottomSheet(AppCompatActivity context, ImagePickListener pickListener) {
        this.context = context;
        this.pickListener = pickListener;
    }

    /**
     * By Bhavesh
     * <br/>
     * Pick image from Gallery or Camera <br/>
     * Also support for cropping
     *
     * @param context
     * @param isImageCroppingEnable If not set then it will use pre-define value
     * @param pickListener          return final image which selected by user
     */
    public ImagePickerBottomSheet(AppCompatActivity context, boolean isImageCroppingEnable, ImagePickListener pickListener) {
        this.context = context;
        this.isImageCroppingEnable = isImageCroppingEnable;
        this.pickListener = pickListener;
    }

    /**
     * By Bhavesh
     * <br/>
     * Pick image from Gallery or Camera <br/>
     * Also support for cropping
     *
     * @param context
     * @param croppingReqOptions Image will be Crop as per options
     * @param pickListener       return final image which selected by user
     */
    public ImagePickerBottomSheet(AppCompatActivity context, ImageCroppingRequest croppingReqOptions, ImagePickListener pickListener) {
        this.context = context;
        this.croppingReqOptions = croppingReqOptions;
        this.pickListener = pickListener;
        this.isImageCroppingEnable = true; // Cropping enable
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public static void removeOnGlobalLayoutListener(View v, ViewTreeObserver.OnGlobalLayoutListener listener) {
        if (Build.VERSION.SDK_INT < 16) {
            v.getViewTreeObserver().removeGlobalOnLayoutListener(listener);
        } else {
            v.getViewTreeObserver().removeOnGlobalLayoutListener(listener);
        }
    }

    /**
     * Returns true if the Activity has access to all given permissions.
     * Always returns true on platforms below M.
     *
     * @see Activity#checkSelfPermission(String)
     */
    @TargetApi(Build.VERSION_CODES.M)
    public static boolean hasSelfPermission(Activity activity, String[] permissions) {
        // Below Android M all permissions are granted at install time and are already available.
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }

        // Verify that all required permissions have been granted
        for (String permission : permissions) {
            if (activity.checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        rootView = View.inflate(getContext(), R.layout.bottom_sheet_image_picker, null);
        dialog.setContentView(rootView);

        // Transparent background
        // dialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);

        setBottomSheetLayout();
        initialize();
        if (isPermissionForCamAndStorage()) {
            showGalleryAndCamera();
        }
    }

    private void setBottomSheetLayout() {
        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) rootView.getParent()).getLayoutParams();
        CoordinatorLayout.Behavior behavior = params.getBehavior();

        if (behavior != null && behavior instanceof BottomSheetBehavior) {
            mBottomSheetBehavior = (BottomSheetBehavior) behavior;
            mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                @Override
                public void onStateChanged(@NonNull View bottomSheet, int newState) {
                    switch (newState) {
                        case BottomSheetBehavior.STATE_COLLAPSED:
                            break;
                        case BottomSheetBehavior.STATE_SETTLING:
                            break;
                        case BottomSheetBehavior.STATE_EXPANDED:
                            break;
                        case BottomSheetBehavior.STATE_HIDDEN:
                            dismiss();
                            break;
                        case BottomSheetBehavior.STATE_DRAGGING:
                            break;
                        default:
                            break;
                    }
                }

                @Override
                public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                    Log.d("BSB", "sliding " + slideOffset);

                    float offset = Math.abs(slideOffset);       // Minus to Plus conversion
                    if (offset < 0.5f) {
                        // For e.g. offset=0.1f
                        float originalOffset = offset;
                        offset = offset + 1.0f;                 // 0.1f + 1.0f = 1.1f
                        offset = offset - (originalOffset * 2); // 11.0f - (0.1f * 2) = 0.9f
                    } else {
                        // For e.g. offset=0.9f
                        offset = Math.abs(offset - 1.0f);       // 0.9f - 1.0f = 0.1f
                    }

                    /*iv_ImagePickerBottomSheet_gallery.setScaleX(offset);
                    iv_ImagePickerBottomSheet_gallery.setScaleY(offset);
                    iv_ImagePickerBottomSheet_camera.setScaleX(offset);
                    iv_ImagePickerBottomSheet_camera.setScaleY(offset);*/
                }
            });

            // Used for Show full layout (Height need to measure)
            rootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    removeOnGlobalLayoutListener(rootView, this);
                    int height = rootView.getMeasuredHeight();
                    mBottomSheetBehavior.setPeekHeight(height);
                }
            });
        }
    }

    private void initialize() {
        rl_ImagePickerBottomSheet_gallery = (RelativeLayout) rootView.findViewById(R.id.rl_ImagePickerBottomSheet_gallery);
        rl_ImagePickerBottomSheet_camera = (RelativeLayout) rootView.findViewById(R.id.rl_ImagePickerBottomSheet_camera);
        iv_ImagePickerBottomSheet_gallery = (ImageView) rootView.findViewById(R.id.iv_ImagePickerBottomSheet_gallery);
        iv_ImagePickerBottomSheet_camera = (ImageView) rootView.findViewById(R.id.iv_ImagePickerBottomSheet_camera);

        rl_ImagePickerBottomSheet_gallery.setOnClickListener(this);
        rl_ImagePickerBottomSheet_camera.setOnClickListener(this);

        // Default
        rl_ImagePickerBottomSheet_gallery.setVisibility(View.INVISIBLE);
        rl_ImagePickerBottomSheet_camera.setVisibility(View.INVISIBLE);
    }

    private void showGalleryAndCamera() {
        rl_ImagePickerBottomSheet_gallery.setVisibility(View.VISIBLE);
        rl_ImagePickerBottomSheet_camera.setVisibility(View.VISIBLE);

        // Start Animation
        startEnterAnimation();
    }

    private void startEnterAnimation() {
        // Zoom Anim
        ViewAnimImagePicker.sequentialZoomInAnimation(
                100,
                300,
                rl_ImagePickerBottomSheet_gallery,
                rl_ImagePickerBottomSheet_camera
        );

        // Color Change Anim
        colorAnimator = ViewAnimImagePicker.setVectorDrawableColorChangeAnimation(
                iv_ImagePickerBottomSheet_gallery,
                1500,
                true,
                false,
                ContextCompat.getColor(context, R.color.colorPrimary),
                ContextCompat.getColor(context, R.color.colorPrimaryDark)
        );

        // Color Change Anim
        colorAnimator = ViewAnimImagePicker.setVectorDrawableColorChangeAnimation(
                iv_ImagePickerBottomSheet_camera,
                1500,
                true,
                false,
                ContextCompat.getColor(context, R.color.colorPrimary),
                ContextCompat.getColor(context, R.color.colorPrimaryDark)
        );
    }

    @TargetApi(Build.VERSION_CODES.M)
    private boolean isPermissionForCamAndStorage() {
        if (hasSelfPermission(context, PERMISSIONS_LIST)) {
            return true;
        } else {
            requestPermissions(PERMISSIONS_LIST, REQUEST_PERMISSION_ALL);
            return false;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_PERMISSION_ALL) {
            boolean allPermissionsGranted = true;
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    allPermissionsGranted = false;
                    break;
                }
            }

            if (allPermissionsGranted) {
                // Logger.i(TAG + "permission was granted.");
                showGalleryAndCamera();
            } else {
                // Logger.i(TAG + "permission was NOT granted.");
                openSettingsDialog();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public void openSettingsDialog() {
        AlertDialog.Builder ald = new AlertDialog.Builder(context);
        ald.setMessage(context.getResources().getString(R.string.allow_permission_settings));
        ald.setPositiveButton(context.getResources().getString(R.string.settings), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                intent.setData(Uri.parse("package:" + context.getPackageName()));
                context.startActivity(intent);
            }
        });
        ald.setNegativeButton(context.getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                dismissBottomSheet(); // Permission not granted
            }
        });
        ald.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_ImagePickerBottomSheet_gallery:

                // Multiple image
                /*Intent intent = new Intent();
                intent.setType("image/*");
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent,"Select Picture"), CODE_PICK_PHOTO_GALLERY);*/

                startActivityForResult(
                        new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI),
                        CODE_PICK_PHOTO_GALLERY
                );
                break;
            case R.id.rl_ImagePickerBottomSheet_camera:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    // Do something for lollipop and above versions
                    CAMERA_IMAGE_URI = FileProvider.getUriForFile(context, context.getPackageName() + ".provider", SdcardUtils.returnImageFileName());
                } else {
                    // do something for phones running an SDK before nougat
                    CAMERA_IMAGE_URI = Uri.fromFile(SdcardUtils.returnImageFileName());
                }

                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                cameraIntent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, CAMERA_IMAGE_URI);
                startActivityForResult(cameraIntent, CODE_PICK_PHOTO_CAMERA);
                break;
            default:
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case CODE_PICK_PHOTO_CAMERA:
                    //Camera Take Photo
                    if (CAMERA_IMAGE_URI != null) {
                        String originalImagePath;
                        originalImagePath = CAMERA_IMAGE_URI.getPath();
                        getBitmapRotation(originalImagePath);
                        if (!originalImagePath.equals("")) {
                            handleSelectedImages(originalImagePath);
                        }
                    }
                    break;
                case CODE_PICK_PHOTO_GALLERY:
                    Uri selectedImage = data.getData();
                    String originalImagePath = "";
                    originalImagePath = ContentResolverUtils.getRealPathFromURI(context, selectedImage);
                    if (!originalImagePath.equals("")) {
                        handleSelectedImages(originalImagePath);
                    }
                    break;
                default:
                    break;
            }
        } else if (resultCode == Activity.RESULT_CANCELED) {
            //Write your code if there's no result
        }
    }

    /**
     * By Bhavesh
     * <br/>
     * <p>
     * Handle the selected image from gallery or camera
     *
     * @param originalImagePath Full image path
     */
    private void handleSelectedImages(String originalImagePath) {
        Bitmap bitmap = ImageUtils.getBitmap(originalImagePath);
        if (bitmap != null) {
            // Scaling without Stretching
            bitmap = scalingBitmapWithoutStretching(bitmap);

            File pickedImageFile = SdcardUtils.returnImageFileName();
            try {
                FileOutputStream out = new FileOutputStream(pickedImageFile);
                if (bitmap != null) {
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);

                    if (isImageCroppingEnable) {
                        if (croppingReqOptions == null) {
                            croppingReqOptions = new ImageCroppingRequest(pickedImageFile); // "croppingReqOptions" Not set from constructor, So set here image
                        }
                        croppingReqOptions.setImageFile(pickedImageFile);

                        new ImageCroppingBottomSheet(context, croppingReqOptions, new ImageCroppingBottomSheet.ImageCropListener() {
                            @Override
                            public void onImageCropDone(File imageFile) {
                                pickListener.onPickImage(imageFile);
                                dismissBottomSheet(); // Image Cropped done
                            }
                        }).show(getFragmentManager(), "");

                    } else {
                        pickListener.onPickImage(pickedImageFile);
                        dismissBottomSheet(); // Image picked from Gallery/Camera
                    }
                }
                out.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void dismissBottomSheet() {
        dismissAllowingStateLoss();
    }

    private Bitmap scalingBitmapWithoutStretching(Bitmap b) {
        // RESIZE_BITMAP_SIZE = 1000
        if (b.getWidth() > RESIZE_BITMAP_SIZE && b.getHeight() > RESIZE_BITMAP_SIZE) {
            Log.v("", "In Resize Function");
            if (b.getWidth() > b.getHeight()) {
                int new_height = (b.getHeight() * RESIZE_BITMAP_SIZE) / b.getWidth();
                Log.v("", "=new_height=" + new_height);
                Bitmap scaledBitmap = ScalingUtilities.createScaledBitmap(b, (int) RESIZE_BITMAP_SIZE, (int) new_height, ScalingUtilities.ScalingLogic.FIT);
                b.recycle();
                b = scaledBitmap;
            } else {
                int new_width = (b.getWidth() * RESIZE_BITMAP_SIZE) / b.getHeight();
                Log.v("", "=new_width=" + new_width);
                Bitmap scaledBitmap = ScalingUtilities.createScaledBitmap(b, (int) new_width, (int) RESIZE_BITMAP_SIZE, ScalingUtilities.ScalingLogic.FIT);
                b.recycle();
                b = scaledBitmap;
            }
        }

        return b;
    }

    private int getBitmapRotation(String originalImagePath) {
        int rotation = 0;
        int orientation = 0;

        ExifInterface exif;
        try {
            exif = new ExifInterface(originalImagePath);
            orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_180:
                rotation = 180;
                break;
            case ExifInterface.ORIENTATION_ROTATE_90:
                rotation = 90;
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                rotation = 270;
                break;
        }
        return rotation;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (colorAnimator != null) {
            colorAnimator.cancel();
        }
    }

    public interface ImagePickListener {

        /*
        -> File to Bitmap convert

        Bitmap myBitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath());
        myImage.setImageBitmap(myBitmap);
        */

        void onPickImage(File imageFile);
    }

    /**
     * Created By Bhavesh on 31-10-2017
     * <br/>
     * Used for Animating views
     */
    public static class ViewAnimImagePicker {
        /**
         * By Bhavesh<br/>
         * <p/>
         * <p>
         * E.g.
         * <br/>startTimeGapOfAnimation = 50
         * <br/>
         * <br/>1st view animation start time = 0
         * <br/>2nd view animation start time = 50
         * <br/>3rd view animation start time = 100
         * <br/>4th view animation start time = 150
         * <br/>5th view animation start time = 200
         * <br/>Continue...
         *
         * @param startTimeGapOfAnimation
         * @param zoomSpeedInMilli        Zoom speed in millisecond
         * @param views                   Multiple views passed through animating sequentially
         */
        public static void sequentialZoomInAnimation(long startTimeGapOfAnimation, long zoomSpeedInMilli, View... views) {
            if (views != null && views.length > 0) {
                long timeInMilli = 0;

                boolean isFirstTime = true;

                ArrayList<Animation> animationList = new ArrayList<>();
                for (int i = 0; i < views.length; i++) {
                    float
                            startX = 0,
                            startY = 0;

                    final Animation animation = new ScaleAnimation(
                            startX, 1f,                          // Start and end values for the X axis scaling
                            startY, 1f,                          // Start and end values for the Y axis scaling
                            Animation.RELATIVE_TO_SELF, 0.5f,  // Center from X scaling
                            Animation.RELATIVE_TO_SELF, 0.5f); // Center from Y scaling

                    if (isFirstTime) {
                        isFirstTime = false;
                    } else {
                        timeInMilli = timeInMilli + startTimeGapOfAnimation;
                    }

                    animation.setFillAfter(true);                   // Need for keep the result of the animation
                    animation.setInterpolator(new DecelerateInterpolator());
                    animation.setStartOffset(timeInMilli);
                    animation.setDuration(zoomSpeedInMilli);

                    animationList.add(animation);
                }

                for (int i = 0; i < views.length; i++) {
                    views[i].setAnimation(animationList.get(i));
                }
            }
        }

        /**
         * By Bhavesh
         * <br/>
         * <br/>Useful for highlight view
         * <br/>Color list smooth animation
         * <br/>
         * <br/>If <b>infinity=true</b>, then Set first and last color same for linear animation
         * <br/>Use <b>{@link ValueAnimator#cancel}</b> for stop animation
         *
         * @param view1
         * @param animTime  Time in MilliSecond
         * @param reverse
         * @param infinity
         * @param colorList E.g. List of color to be animate ContextCompat.getColor(context, R.color.colorAccent)
         * @return
         */
        public static ValueAnimator setVectorDrawableColorChangeAnimation(final View view1, long animTime, boolean reverse, final boolean infinity, Object... colorList) {
            if (view1 != null && view1 instanceof ImageView) {
                final ImageView view = (ImageView) view1;

                final ValueAnimator colorAnim = ValueAnimator.ofObject(new ArgbEvaluator(), colorList);
                colorAnim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            view.getDrawable().setTint((int) animation.getAnimatedValue());
                        }
                    }
                });

                if (reverse) {
                    colorAnim.setRepeatMode(ValueAnimator.REVERSE);
                    colorAnim.setRepeatCount(1);
                }
                if (infinity) {
                    colorAnim.setRepeatCount(ObjectAnimator.INFINITE);
                    colorAnim.setInterpolator(new LinearInterpolator());
                }

                colorAnim.setDuration(animTime);
                colorAnim.start();

                colorAnim.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        if (!infinity) {
                            colorAnim.cancel();
                        }
                    }

                    @Override
                    public void onAnimationPause(Animator animation) {
                        super.onAnimationPause(animation);
                        if (!infinity) {
                            colorAnim.cancel();
                        }
                    }
                });

                return colorAnim;
            }

            return null;
        }

        /**
         * By Bhavesh<br/>
         * <p/>
         * <p>
         * Set alpha color visible first and then invisible
         *
         * @param color
         * @param factor
         * @return
         */
        private static int adjustAlpha(int color, float factor) {
            int alpha = Math.round(Color.alpha(color) * factor);
            int red = Color.red(color);
            int green = Color.green(color);
            int blue = Color.blue(color);
            return Color.argb(alpha, red, green, blue);
        }

        /**
         * By Bhavesh<br/>
         * <p/>
         * <p>
         * Zoom in animation from 0 to 1 scaling (X,Y will be Scale)
         *
         * @param view
         * @param zoomSpeedInMilli
         */
        public static void zoomInAnimation(View view, long zoomSpeedInMilli) {
            if (view != null) {
                float
                        startX = 0,
                        startY = 0;

                final Animation animation = new ScaleAnimation(
                        startX, 1f,                          // Start and end values for the X axis scaling
                        startY, 1f,                          // Start and end values for the Y axis scaling
                        Animation.RELATIVE_TO_SELF, 0.5f,  // Center from X scaling
                        Animation.RELATIVE_TO_SELF, 0.5f); // Center from Y scaling

                animation.setFillAfter(true);                   // Needed to keep the result of the animation
                animation.setInterpolator(new DecelerateInterpolator());
                animation.setDuration(zoomSpeedInMilli);

                view.startAnimation(animation); // Zoom out Animation here
            }
        }
    }
}