package com.proclapp.utils.ImagePickerUtils.Utils;

import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.util.Calendar;

public class SdcardUtils {
    public static String PUTILS_IMAGE_THUMB = "ImageUpload/Thumb";
    public static String PUTILS_IMAGE_IMAGES = "ImageUpload/Images";

    public static String CreateFolder(String folderPath) {
        String folderName = "";

        folderName = Environment.getExternalStorageDirectory() + "/" + folderPath;
        Log.d("", "folderName : " + folderName);
        File fileName = new File(folderName);
        if (!fileName.exists()) {
            fileName.mkdirs();
        }
        return folderName;
    }

    public static File returnImageFileName() {
        File imageFileName;
        String ImageName = "Image_" + Calendar.getInstance().getTimeInMillis() + ".jpeg";
        String Imagefolder = CreateFolder(PUTILS_IMAGE_IMAGES);
        if (!Imagefolder.equals("")) {
            imageFileName = new File(Imagefolder, ImageName);
        } else {
            imageFileName = null;
        }
        return imageFileName;
    }

    public static File returnThumbImageFileName() {
        File thumbfilename;
        String ImagethumbName = "Image_thumb_" + Calendar.getInstance().getTimeInMillis() + ".jpeg";
        String ImageThumbfolder = CreateFolder(PUTILS_IMAGE_THUMB);
        if (!ImageThumbfolder.equals("")) {
            thumbfilename = new File(ImageThumbfolder, ImagethumbName);
        } else {
            thumbfilename = null;
        }

        return thumbfilename;
    }

    public static void deleteDataFromFolder() {
        // Delete Images inside "Images" folder
        File dir = new File(Environment.getExternalStorageDirectory() + "/" + PUTILS_IMAGE_IMAGES);
        if (dir.isDirectory()) {
            String[] children = dir.list();
            if (children != null) {
                for (int i = 0; i < children.length; i++) {
                    new File(dir, children[i]).delete();
                }
            }
        }

        // Delete Folder of "Images"
        /*File dir1 = new File(Environment.getExternalStorageDirectory() + "/" + "Consumer Hero");
        if (dir1.isDirectory()) {
            String[] children = dir1.list();
            if (children != null) {
                for (int i = 0; i < children.length; i++) {
                    new File(dir1, children[i]).delete();
                }
            }
        }*/
    }



}
