package com.proclapp.utils;

import android.content.Context;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;

import com.chinalwb.are.ArePreferences;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.proclapp.AppClass;
import com.zyf.vc.VideoPreferences;


/**
 * Created by Prismetric on 2/2/2016.
 */
public class LogOutUtils {

    public static void LogoutUser(Context context) {

        Logger.e("Prefe " + AppClass.preferences.getGetStartedFirstTime());

        boolean is_first_time = AppClass.preferences.getGetStartedFirstTime();

        AppClass.preferences.clearPrefs();
        AppClass.preferences.storeGetStartedFirstTime(is_first_time);

        ((AppClass) context.getApplicationContext()).removeSocketConnection();

        new VideoPreferences(context).clearPrefs();
        new ArePreferences(context).clearPrefs();

        AppCompatActivity activity = (AppCompatActivity) context;

        activity.getIntent().addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        activity.finish();
        /*  activity.startActivity(new Intent(activity, LoginScreenTwoActivity.class));*/
    }
}
