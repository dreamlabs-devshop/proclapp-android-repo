package com.proclapp.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class NetworkChangeReceiver extends BroadcastReceiver {

    private OnInternetConnect internetConnect;

    public NetworkChangeReceiver(OnInternetConnect internetConnect) {
        this.internetConnect = internetConnect;
    }

    @Override
    public void onReceive(final Context context, final Intent intent) {

        int status = NetworkUtils.getConnectivityStatusString(context);
        if ("android.net.conn.CONNECTIVITY_CHANGE".equals(intent.getAction())) {
            if (status == NetworkUtils.NETWORK_STATUS_NOT_CONNECTED) {
                Logger.e("not connect");
            } else {
                internetConnect.onconnected(intent);
            }
        }
    }

    public interface OnInternetConnect {
        void onconnected(Intent intent);
    }

}