package com.proclapp.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Admin on 06-11-2015.
 */
public class Preferences {
    private static final String
            PREF_NAME = "glamapp",
            FCM_TOKEN = "FCM_TOKEN",
            USERID = "USERID",
            USER_NAME = "USER_NAME",
            FIRST_NAE = "FIRST_NAE",
            LAST_NAME = "LAST_NAME",
            GENDER = "GENDER",
            MOBILE_NUMBER = "MOBILE_NUMBER",
            COUNTRY_CODE = "COUNTRY_CODE",
            PROFILE_IMAGE = "PROFILE_IMAGE",
            COVER_IMAGE = "COVER_IMAGE",
            IS_EXPERT = "IS_EXPERT",
            MEMBER_SINCE = "MEMBER_SINCE",
            ABOUT_ME = "ABOUT_ME",
            ABOUT_MY_PROFESSION = "ABOUT_MY_PROFESSION",
            ADDRESS = "ADDRESS",
            NOTIFICATION_BADGE_COUNT = "NOTIFICATION_BADGE_COUNT",
            TOKEN = "TOKEN",
            GETSTARTED_FIRSTTIME = "GETSTARTED_FIRSTTIME",
            CURRENT_JOB = "CURRENT_JOB",
            SKIP = "SKIP",
            FOLLOWER = "FOLLOWER",
            FOLLOWING = "FOLLOWING",
            CATEGORYID = "CATEGORYID",
            SCREEN_CODE = "SCREEN_CODE",
            EMAIL = "EMAIL";
    public static String
            TOTAL_FRIEND = "total_friends",
            UNREAD_NOTIFICATION = "unread_notifications",
            UNREAD_MESSAGE = "unread_messages",
            FRIEND_REQUESTS = "friend_requests";
    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    Context context;
    int PRIVATE_MODE = 0;

    public Preferences(Context context) {
        this.context = context;
        prefs = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
    }

    public void clearPrefs() {
        editor = prefs.edit();
        editor.clear();
        editor.apply();
    }


    public void storeUserName(String userName) {
        editor = prefs.edit();
        editor.putString(USER_NAME, userName);
        editor.apply();
    }

    public String getUserName() {
        return prefs.getString(USER_NAME, "");
    }

    public void storeFirstName(String firstname) {
        editor = prefs.edit();
        editor.putString(FIRST_NAE, firstname);
        editor.apply();
    }

    public String getUserFirstName() {
        return prefs.getString(FIRST_NAE, "");
    }

    public void storeUSerLastName(String lastname) {
        editor = prefs.edit();
        editor.putString(LAST_NAME, lastname);
        editor.apply();
    }

    public String getUserLastName() {
        return prefs.getString(LAST_NAME, "");
    }

    public void storeAddress(String address) {
        editor = prefs.edit();
        editor.putString(ADDRESS, address);
        editor.apply();
    }

    public String getAddress() {
        return prefs.getString(ADDRESS, "");
    }


    public void storeGender(String gender) {
        editor = prefs.edit();
        editor.putString(GENDER, gender);
        editor.apply();
    }

    public String getGender() {
        return prefs.getString(GENDER, "");
    }

    public void storeMobileNumber(String no) {
        editor = prefs.edit();
        editor.putString(MOBILE_NUMBER, no);
        editor.apply();
    }

    public String getMobileNumber() {
        return prefs.getString(MOBILE_NUMBER, "");
    }

    public void storeCountryCode(String code) {
        editor = prefs.edit();
        editor.putString(COUNTRY_CODE, code);
        editor.apply();
    }

    public String getCountryCode() {
        return prefs.getString(COUNTRY_CODE, "");
    }

    public void storeAboutMe(String data) {
        editor = prefs.edit();
        editor.putString(ABOUT_ME, data);
        editor.apply();
    }

    public String getAboutMe() {
        return prefs.getString(ABOUT_ME, "");
    }

    public void storeProfileImage(String data) {
        editor = prefs.edit();
        editor.putString(PROFILE_IMAGE, data);
        editor.apply();
    }

    public String getProfileImage() {
        return prefs.getString(PROFILE_IMAGE, "");
    }

    public void storeCoverImage(String data) {
        editor = prefs.edit();
        editor.putString(COVER_IMAGE, data);
        editor.apply();
    }

    public String getCoverImage() {
        return prefs.getString(COVER_IMAGE, "");
    }


    public void storeIsExpert(String data) {
        editor = prefs.edit();
        editor.putString(IS_EXPERT, data);
        editor.apply();
    }

    public String getIsExpert() {
        return prefs.getString(IS_EXPERT, "");
    }

    public void storeFollowing(String data) {
        editor = prefs.edit();
        editor.putString(FOLLOWING, data);
        editor.apply();
    }

    public String getFollowing() {
        return prefs.getString(FOLLOWING, "");
    }


    public void storeFollowers(String data) {
        editor = prefs.edit();
        editor.putString(FOLLOWER, data);
        editor.apply();
    }

    public String getFollowers() {
        return prefs.getString(FOLLOWER, "");
    }

    public void storeCategoriesId(String data) {
        editor = prefs.edit();
        editor.putString(CATEGORYID, data);
        editor.apply();
    }

    public String getCategoriesId() {
        return prefs.getString(CATEGORYID, "");
    }

    public void storeMemberSince(String data) {
        editor = prefs.edit();
        editor.putString(MEMBER_SINCE, data);
        editor.apply();
    }

    public String getMemberSince() {
        return prefs.getString(MEMBER_SINCE, "");
    }

    public void storeAboutMyProfession(String data) {
        editor = prefs.edit();
        editor.putString(ABOUT_MY_PROFESSION, data);
        editor.apply();
    }

    public String getAboutMyProfession() {
        return prefs.getString(ABOUT_MY_PROFESSION, "");
    }

    public void storeEmailId(String email) {
        editor = prefs.edit();
        editor.putString(EMAIL, email);
        editor.apply();
    }

    public String getEmailId() {
        return prefs.getString(EMAIL, "");
    }


    public void storeUserId(String userId) {
        editor = prefs.edit();
        editor.putString(USERID, userId);
        editor.apply();
    }

    public String getUserId() {
        return prefs.getString(USERID, "");
    }


    public void storeToken(String deviceToken) {
        editor = prefs.edit();
        editor.putString(TOKEN, deviceToken);
        editor.apply();
    }

    public String getToken() {
        return prefs.getString(TOKEN, "");
    }


    public void storeScreeCode(String screeCode) {
        editor = prefs.edit();
        editor.putString(SCREEN_CODE, screeCode);
        editor.apply();
    }

    public String getScreeCode() {
        return prefs.getString(SCREEN_CODE, "");
    }


    public void storeFCMToken(String fcmToken) {
        editor = prefs.edit();
        editor.putString(FCM_TOKEN, fcmToken);
        editor.commit();
    }

    public String getFCMToken() {
        return prefs.getString(FCM_TOKEN, "");
    }


    public void storeNotificationBadgeCount(int count) {
        editor = prefs.edit();
        editor.putInt(NOTIFICATION_BADGE_COUNT, count);
        editor.apply();
    }

    public int getNotificationBadgeCount() {
        return prefs.getInt(NOTIFICATION_BADGE_COUNT, 0);
    }

    public void storeGetStartedFirstTime(boolean state) {
        editor = prefs.edit();
        editor.putBoolean(GETSTARTED_FIRSTTIME, state);
        editor.apply();
    }

    public boolean getGetStartedFirstTime() {
        return prefs.getBoolean(GETSTARTED_FIRSTTIME, false);
    }

    public void storeCurrentJob(String state) {
        editor = prefs.edit();
        editor.putString(CURRENT_JOB, state);
        editor.apply();
    }


    public String getValue(String key) {
        return prefs.getString(key, "");
    }

    public void setValue(String key, String state) {
        editor = prefs.edit();
        editor.putString(key, state);
        editor.apply();
    }

    public String getCurrentJob() {
        return prefs.getString(CURRENT_JOB, "");
    }

    public boolean isSkip() {
        return prefs.getBoolean(SKIP, false);
    }

    public void setSkip(boolean isSkip) {
        editor = prefs.edit();
        editor.putBoolean(SKIP, isSkip);
        editor.apply();
    }


}
