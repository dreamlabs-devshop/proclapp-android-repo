package com.proclapp.utils;

/**
 * Created by Androiddev on 7/5/2017.
 */

public interface ScreenCode {
    String
            NormalScreen = "000",
            UpdatePassword = "111",
            ChangeProfile = "222",
            ResendVerificationEmail = "333",
            EmailAlreadyRegistered = "777",
            UserAuthenticationFailedLogout = "10001";
}


