package com.proclapp.utils;


import android.app.Activity;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;

import java.util.HashMap;

public class ServiceLogOut {

    private static final String TAG = "ServiceLogOut";


    private Activity activity;
    private ServiceDoneListener listener;

    public ServiceLogOut(Activity activity, ServiceDoneListener listener) {
        this.activity = activity;
        this.listener = listener;
        if (activity.isFinishing())
            return;
        callLogoutService();
    }

    private void callLogoutService() {

        if (AppClass.isInternetConnectionAvailable()) {
            HashMap<String, String> requests = new HashMap<>();
            requests.put("user_id", AppClass.preferences.getUserId());
            requests.put("device_token", AppClass.preferences.getFCMToken());

            ApiCall.getInstance().logout(activity, requests, new IApiCallback() {
                @Override
                public void onSuccess(Object data) {

                    ((AppClass) activity.getApplication()).socketNull();
                    String response = (String) data;
                    listener.onSuccess(response);
                }

                @Override
                public void onFailure(boolean isFalseFromService, String errorMessage) {
                    listener.onFail(false, errorMessage);
                }
            }, true);


        } else {
            listener.onFail(false, activity.getResources().getString(R.string.nonetwork));
        }

    }


    public interface ServiceDoneListener {
        void onSuccess(String message);

        void onFail(boolean isResponseFalseFromService, String message);
    }
}