package com.proclapp.utils;

import android.app.Activity;
import android.content.Context;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.content.ContextCompat;

import android.view.View;
import android.widget.TextView;

import com.proclapp.R;


public class SnackBarView {
    public void snackBarShow(Context context, String text) {
        Snackbar snackbar = Snackbar.make(((Activity) context).findViewById(android.R.id.content), text, Snackbar.LENGTH_LONG);
        //snackbar.setActionTextColor(R.color.profileDivider);
        View view = snackbar.getView();
        view.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
        TextView textView = (TextView) view.findViewById(R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(context, R.color.white));
        snackbar.show();
    }

    public void snackBarShow(Context context, String text,int color) {
        Snackbar snackbar = Snackbar.make(((Activity) context).findViewById(android.R.id.content), text, Snackbar.LENGTH_LONG);
        //snackbar.setActionTextColor(R.color.profileDivider);
        View view = snackbar.getView();
        view.setBackgroundColor(color);
        TextView textView = (TextView) view.findViewById(R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(context, R.color.white));
        /*FrameLayout.LayoutParams params =(FrameLayout.LayoutParams)view.getLayoutParams();
        params.gravity = Gravity.TOP;
        view.setLayoutParams(params);*/
        snackbar.show();
    }
}
