package com.proclapp.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import androidx.appcompat.app.AlertDialog;

import com.proclapp.R;
import com.proclapp.expert.ExpertModel;
import com.proclapp.model.CategoryModel;
import com.proclapp.model.FollowerFollowingModel;
import com.proclapp.model.PostAdvUploadModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class StaticData {

    public static final int RESULT_CODE = 3;
    // for post adv
    public static String post_adv_category = "";
    public static String post_adv_title = "";
    public static String post_adv_description = "";
    public static String post_adv_url = "";
    public static String post_adv_delete_image_id = "";
    public static ArrayList<PostAdvUploadModel> postAdvUploadModelList = new ArrayList<>();
    public static boolean is_post_adv_summary_image_edit = false;
    public static String question = "Question";
    public static String article = "Article";
    public static String research_paper = "Research Paper";
    public static String voice_video = "Voice and video";
    public static boolean photofromcamera = false;
    public static String crop_imagepath = "";

    public static boolean isEdited = false;

    public static ArrayList<CategoryModel> trendingTopics = new ArrayList<>();
    public static ArrayList<ExpertModel> suggestedExperts = new ArrayList<>();
    public static ArrayList<FollowerFollowingModel> suggestedFriends = new ArrayList<>();

    public static String formatDate(String format1, String format2, String dateInParse) {
        SimpleDateFormat sdf = new SimpleDateFormat(format1);
        Date date = null;
        try {
            date = sdf.parse(dateInParse);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        SimpleDateFormat formatter = new SimpleDateFormat(format2);
        String newFormat = formatter.format(date);

        return newFormat;
    }

    public static void openSettingsDialog(final Context context) {
        android.app.AlertDialog.Builder ald = new android.app.AlertDialog.Builder(context);
        ald.setMessage(context.getResources().getString(R.string.allow_permission_settings));
        ald.setPositiveButton(context.getResources().getString(R.string.settings), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                intent.setData(Uri.parse("package:" + context.getPackageName()));
                context.startActivity(intent);
            }
        });
        ald.setNegativeButton(context.getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        ald.show();
    }

    private void openResponseAlert(Activity activity, String message) {
        final AlertDialog.Builder ald = new AlertDialog.Builder(activity);
        ald.setMessage(message);
        ald.setPositiveButton(activity.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                // finish();
            }
        });

        ald.show();
    }
}
