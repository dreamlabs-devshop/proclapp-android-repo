package com.proclapp.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Prismetric on 9/4/2015.
 */
public class StringUtils {

    private final static String tagStart =
            "\\<\\w+((\\s+\\w+(\\s*\\=\\s*(?:\".*?\"|'.*?'|[^'\"\\>\\s]+))?)+\\s*|\\s*)\\>";
    private final static String tagEnd =
            "\\</\\w+\\>";
    private final static String tagSelfClosing =
            "\\<\\w+((\\s+\\w+(\\s*\\=\\s*(?:\".*?\"|'.*?'|[^'\"\\>\\s]+))?)+\\s*|\\s*)/\\>";
    private final static String htmlEntity =
            "&[a-zA-Z][a-zA-Z0-9]+;";

    private final static Pattern htmlPattern = Pattern.compile(
            "(" + tagStart + ".*" + tagEnd + ")|(" + tagSelfClosing + ")|(" + htmlEntity + ")",
            Pattern.DOTALL
    );


    public static boolean isNotEmpty(CharSequence str) {
        if (str == null || str.length() == 0 || str.equals("null") || str.equals(" ") || str.equals("none") || str.equals("") || str.equals("(null)")) {
            return false;
        } else {
            return true;
        }
    }

    public static int findPatternRepeatNumber(String main_str, String match_string) {
        Pattern pattern = Pattern.compile(match_string); //Pattern string you want to be matched
        Matcher matcher = pattern.matcher(main_str);

        int count = 0;
        while (matcher.find())
            count++; //count any matched pattern

        return count;
    }

    public static boolean isNullOrBlank(String s) {
        return (s == null || s.trim().equals(""));
    }

    public static boolean isHtml(String s) {
        boolean ret = false;
        if (s != null) {
            ret = htmlPattern.matcher(s).find();
        }
        return ret;
    }



}
