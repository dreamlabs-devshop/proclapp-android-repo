package com.proclapp.utils;

import android.content.Context;
import android.widget.Toast;

/**
 *
 */
public class ToastView {
    Context context;

    public ToastView() {

    }

    public ToastView(Context context) {
        this.context = context;
    }

    public void ToastShow(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }

    public void showMessage(String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }
}
