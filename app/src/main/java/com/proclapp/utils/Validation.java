package com.proclapp.utils;

import android.content.Context;
import android.widget.EditText;

import java.util.List;

public class Validation {
    Context context;

    public Validation(Context context) {
        this.context = context;
    }

    public boolean edt_validation(List<EditText> edittext) {
        for (EditText edt : edittext) {
            if (edt.getText().toString().length() == 0) {
                return false;
            }
        }

        return true;
    }

    public final static boolean isValidEmail(String target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

}
