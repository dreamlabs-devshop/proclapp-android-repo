package com.proclapp.voice;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ExpertWalletModel implements Serializable {

    private String wallet_amount;
    private String currency;
    private List<WalletHistory> history = new ArrayList<>();

    public String getWalletAmount() {
        return wallet_amount;
    }

    public String getCurrency() {
        return currency;
    }

    public List<WalletHistory> getHistory() {
        return history;
    }

    public static class WalletHistory implements Serializable {

        private String
                id,
                debit_credit_amount,
                post_id,
                balance_amount,
                note,
                transaction_id,
                date,
                post_title,
                name,
                profile_image,
                profile_image_thumb,
                currency;

        public String getId() {
            return id;
        }

        public String getDebit_credit_amount() {
            return debit_credit_amount;
        }

        public String getPost_id() {
            return post_id;
        }

        public String getBalance_amount() {
            return balance_amount;
        }

        public String getNote() {
            return note;
        }

        public String getTransaction_id() {
            return transaction_id;
        }

        public String getDate() {
            return date;
        }

        public String getPost_title() {
            return post_title;
        }

        public String getName() {
            return name;
        }

        public String getProfile_image() {
            return profile_image;
        }

        public String getProfile_image_thumb() {
            return profile_image_thumb;
        }

        public String getCurrency() {
            return currency;
        }
    }


}
