package com.proclapp.voice;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.home.HomeActivity;
import com.proclapp.home.adapter.VoiceAndVideoAdapter;
import com.proclapp.model.AllPostsModel;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.AlertDialogInterface;
import com.proclapp.utils.DialogUtils;
import com.proclapp.utils.StaticData;
import com.proclapp.utils.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;

public class VoiceFragment extends Fragment implements SwipyRefreshLayout.OnRefreshListener {

    private final int REQUEST_PERMISSION_ALL = 10;
    private RecyclerView rv_voice_list;
    private HomeActivity mHomeActivity;
    private SwipyRefreshLayout swipe_refresh;
    private ArrayList<AllPostsModel> audioList = new ArrayList<>();
    private TextView tv_no_data;
    private String[] PERMISSIONS_LIST = {Manifest.permission.RECORD_AUDIO};
    private int offset = 0;
    private boolean isServiceCalling = false, isLastPage = false;

    public static boolean hasSelfPermission(Activity activity, String[] permissions) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }

        for (String permission : permissions) {
            if (activity.checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_voice, container, false);

        mHomeActivity = (HomeActivity) getActivity();
        swipe_refresh = view.findViewById(R.id.swipe_refresh);
        rv_voice_list = view.findViewById(R.id.rv_voice_list);
        tv_no_data = view.findViewById(R.id.tv_no_data);

        swipe_refresh.setOnRefreshListener(this);
        swipe_refresh.setColorSchemeColors(getResources().getColor(R.color.color_0c6984), getResources().getColor(R.color.black));
        setUpRecyclerView();
        return view;
    }

    @Override
    public void onRefresh(final SwipyRefreshLayoutDirection direction) {
        if (direction == SwipyRefreshLayoutDirection.TOP) {
            offset = 0;
            callAPIGetVoiceList();
        } else {
            offset += 20;
            callAPIGetVoiceList();
        }
    }

    private void setUpRecyclerView() {

        final LinearLayoutManager layoutManager = new LinearLayoutManager(mHomeActivity);
        rv_voice_list.setLayoutManager(layoutManager);
        rv_voice_list.setAdapter(new VoiceAndVideoAdapter(mHomeActivity, audioList));

        swipe_refresh.post(new Runnable() {
            @Override
            public void run() {
                swipe_refresh.setRefreshing(true);
                onRefresh(SwipyRefreshLayoutDirection.TOP);
            }
        });

        rv_voice_list.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                int visibleItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int firstVisibleItemPosition = layoutManager.findLastVisibleItemPosition();

                if (!isServiceCalling && !isLastPage) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0) {
                        swipe_refresh.post(new Runnable() {
                            @Override
                            public void run() {
                                swipe_refresh.setRefreshing(true);
                                onRefresh(SwipyRefreshLayoutDirection.BOTTOM);
                            }
                        });

                    }
                }
            }
        });

    }

    private void callAPIGetVoiceList() {


        HashMap<String, String> request = new HashMap<>();
        request.put("user_id", AppClass.preferences.getUserId());
        request.put("vv_type", "1");
        request.put("offset", String.valueOf(offset));

        ApiCall.getInstance().getVVList(getActivity(), request, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {

                if (offset == 0)
                    audioList.clear();

                audioList.addAll((ArrayList<AllPostsModel>) data);


                        swipe_refresh.setRefreshing(false);
                        isLastPage = false;
                        isServiceCalling = false;
                        rv_voice_list.setVisibility(View.VISIBLE);
                        tv_no_data.setVisibility(View.GONE);
                        rv_voice_list.getAdapter().notifyDataSetChanged();

            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {

                        swipe_refresh.setRefreshing(false);
                        isLastPage = true;
                        isServiceCalling = false;


                if (StringUtils.isNotEmpty(errorMessage)) {

                    if (errorMessage.equalsIgnoreCase("No data found")) {

                        if (offset == 0) {
                            tv_no_data.setVisibility(View.VISIBLE);
                            rv_voice_list.setVisibility(View.GONE);
                        }

                    } else {

                        DialogUtils.openDialog(getActivity(), errorMessage, getString(R.string.ok), "", new AlertDialogInterface() {
                            @Override
                            public void onNegativeBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }

                            @Override
                            public void onPositiveBtnClicked(DialogInterface alrt) {
                                alrt.dismiss();
                            }
                        });
                    }
                }
            }
        }, false);


    }

    @TargetApi(Build.VERSION_CODES.M)
    private boolean checkPermission() {

        if (hasSelfPermission(mHomeActivity, PERMISSIONS_LIST)) {
            return true;
        } else {
            requestPermissions(PERMISSIONS_LIST, REQUEST_PERMISSION_ALL);
            return false;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_PERMISSION_ALL) {
            boolean allPermissionsGranted = true;
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    allPermissionsGranted = false;
                    break;
                }
            }

            if (allPermissionsGranted) {

            } else {
                StaticData.openSettingsDialog(mHomeActivity);
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


}
