package com.proclapp.wallet;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.proclapp.AppClass;
import com.proclapp.R;
import com.proclapp.retrofit.ApiCall;
import com.proclapp.retrofit.IApiCallback;
import com.proclapp.utils.DateTimeUtils;
import com.proclapp.voice.ExpertWalletModel;
import com.whiteelephant.monthpicker.MonthPickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class ExpertWalletActivity extends AppCompatActivity implements View.OnClickListener {

    private List<ExpertWalletModel.WalletHistory> history = new ArrayList<>();

    private String
            month = DateTimeUtils.changeDateTimeFormat("M", "MM", String.valueOf((Calendar.getInstance().get(Calendar.MONTH) + 1))),
            year = String.valueOf(Calendar.getInstance().get(Calendar.YEAR));
    private RecyclerView rv_redeem_list;
    private TextView tv_month_year, tv_amount, tv_no_data;
    private ImageView iv_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expert_wallet);
        initViews();
        setUpRecyclerView();
    }

    private void initViews() {
        rv_redeem_list = findViewById(R.id.rv_redeem_list);
        iv_back = findViewById(R.id.iv_back);
        tv_month_year = findViewById(R.id.tv_month_year);
        tv_amount = findViewById(R.id.tv_amount);
        tv_no_data = findViewById(R.id.tv_no_data);

        tv_month_year.setText(String.format("%s, %s", DateTimeUtils.changeDateTimeFormat("MM", "MMM", month), year));

        iv_back.setOnClickListener(this);
        tv_month_year.setOnClickListener(this);
    }

    private void setUpRecyclerView() {

        rv_redeem_list.setLayoutManager(new LinearLayoutManager(this));
        rv_redeem_list.setAdapter(new ExpertWalletAdapter(this, history));
        rv_redeem_list.setFocusable(false);

        getExpertWallet();

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.iv_back:
                onBackPressed();
                break;

            case R.id.tv_month_year:
                monthPickerDialog();
                break;

        }

    }

    private void monthPickerDialog() {

        new MonthPickerDialog.Builder(this, (selectedMonth, selectedYear) -> {

            month = DateTimeUtils.changeDateTimeFormat("M", "MM", String.valueOf(selectedMonth + 1));
            year = String.valueOf(selectedYear);
            tv_month_year.setText(String.format(Locale.getDefault(), "%s, %d", DateTimeUtils.changeDateTimeFormat("MM", "MMM", String.valueOf(selectedMonth + 1)), selectedYear));

            getExpertWallet();

        }, Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH))
                .setActivatedMonth(Calendar.getInstance().get(Calendar.MONTH))
                .setMinYear(2018)
                .setMaxYear(Calendar.getInstance().get(Calendar.YEAR) + 10)
                .setTitle("Select Month and Year")
                .build()
                .show();

    }

    private void getExpertWallet() {

        ApiCall.getInstance().wallet(this, AppClass.preferences.getUserId(), month, year, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {


                ExpertWalletModel walletModel = (ExpertWalletModel) data;
                tv_amount.setText(String.format("%s %s", walletModel.getCurrency(), walletModel.getWalletAmount()));
                history.clear();
                history.addAll(walletModel.getHistory());
                rv_redeem_list.getAdapter().notifyDataSetChanged();

                if (history.isEmpty()) {
                    tv_no_data.setVisibility(View.VISIBLE);
                    rv_redeem_list.setVisibility(View.GONE);
                } else {
                    tv_no_data.setVisibility(View.GONE);
                    rv_redeem_list.setVisibility(View.VISIBLE);
                }


            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {

                tv_no_data.setVisibility(View.VISIBLE);
                rv_redeem_list.setVisibility(View.GONE);


            }
        }, false);

    }

}
