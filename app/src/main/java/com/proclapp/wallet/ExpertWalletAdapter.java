package com.proclapp.wallet;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.proclapp.R;
import com.proclapp.utils.DateTimeUtils;
import com.proclapp.utils.ImageLoadUtils;
import com.proclapp.voice.ExpertWalletModel;

import java.util.List;

public class ExpertWalletAdapter extends RecyclerView.Adapter<ExpertWalletAdapter.ExpertWalletHolder> {

    Context context;
    private List<ExpertWalletModel.WalletHistory> history;

    public ExpertWalletAdapter(Context context, List<ExpertWalletModel.WalletHistory> history) {
        this.context = context;
        this.history = history;
    }

    @NonNull
    @Override
    public ExpertWalletHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_expert_wallet, viewGroup, false);
        return new ExpertWalletHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ExpertWalletHolder holder, int i) {

        ExpertWalletModel.WalletHistory walletHistory = history.get(i);

        ImageLoadUtils.imageLoad(context, holder.iv_profile, walletHistory.getProfile_image(), R.drawable.profile_pic_ph);
        holder.tv_name.setText(walletHistory.getName());
        holder.tv_price.setText(String.format("%s%s", walletHistory.getCurrency(), walletHistory.getDebit_credit_amount()));
        holder.tv_title.setText(walletHistory.getPost_title());
        holder.tv_date.setText(DateTimeUtils.changeDateTimeFormat("yyyy-MM-dd HH:mm:ss", "MMM, dd yyyy, hh:mm a", walletHistory.getDate()));


    }

    @Override
    public int getItemCount() {
        return history.size();
    }

    public class ExpertWalletHolder extends RecyclerView.ViewHolder {

        ImageView iv_profile;
        TextView tv_name;
        TextView tv_title;
        TextView tv_price;
        TextView tv_date;

        public ExpertWalletHolder(@NonNull View itemView) {
            super(itemView);

            iv_profile = itemView.findViewById(R.id.iv_profile);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_title = itemView.findViewById(R.id.tv_title);
            tv_price = itemView.findViewById(R.id.tv_price);
            tv_date = itemView.findViewById(R.id.tv_date);

        }
    }
}
