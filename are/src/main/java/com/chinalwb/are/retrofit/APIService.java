package com.chinalwb.are.retrofit;


import com.google.gson.JsonObject;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface APIService {

    @Multipart
    @POST("post_content_image")
    Call<JsonObject> addArticleImage(
            @Part("user_id") RequestBody rbuserid ,
            @Part MultipartBody.Part file);

}




