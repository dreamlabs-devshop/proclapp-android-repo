package com.chinalwb.are.retrofit;

/**
 * Interface is used for common purpose in Application.
 *
 * @author and15031989
 */
public interface IApiCallback {
    /**
     * Method for getting the type and data.
     *
     * @param data Actual data
     */
    void onSuccess(Object data);


    /**
     * Failure Reason
     *
     * @param isFalseFromService
     * @param errorMessage
     */
    void onFailure(boolean isFalseFromService, String errorMessage);

}
