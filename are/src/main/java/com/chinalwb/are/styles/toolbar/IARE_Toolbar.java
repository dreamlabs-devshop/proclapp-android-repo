package com.chinalwb.are.styles.toolbar;

import android.content.Intent;

import com.chinalwb.are.AREditText;
import com.chinalwb.are.styles.toolitems.IARE_ToolItem;

import java.util.List;

/**
 * Created by wliu on 13/08/2018.
 */

public interface IARE_Toolbar {

    void addToolbarItem(IARE_ToolItem toolbarItem);

    List<IARE_ToolItem> getToolItems();

    AREditText getEditText();

    void setEditText(AREditText editText);

    void onActivityResult(int requestCode, int resultCode, Intent data);
}
