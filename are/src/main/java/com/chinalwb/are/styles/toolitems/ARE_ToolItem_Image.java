package com.chinalwb.are.styles.toolitems;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.chinalwb.are.AREditText;
import com.chinalwb.are.ArePreferences;
import com.chinalwb.are.R;
import com.chinalwb.are.RealPathUtil;
import com.chinalwb.are.Util;
import com.chinalwb.are.retrofit.ApiCall;
import com.chinalwb.are.retrofit.IApiCallback;
import com.chinalwb.are.spans.AreImageSpan;
import com.chinalwb.are.strategies.ImageStrategy;
import com.chinalwb.are.styles.IARE_Style;
import com.chinalwb.are.styles.toolitems.styles.ARE_Style_Image;
import com.google.gson.JsonObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

import okhttp3.MediaType;
import okhttp3.RequestBody;


public class ARE_ToolItem_Image extends ARE_ToolItem_Abstract {

    Context context;

    @Override
    public IARE_ToolItem_Updater getToolItemUpdater() {
        return null;
    }

    @Override
    public IARE_Style getStyle() {
        if (mStyle == null) {
            AREditText editText = this.getEditText();
            mStyle = new ARE_Style_Image(editText, (ImageView) mToolItemView);
        }
        return mStyle;
    }

    @Override
    public View getView(Context context) {
        if (null == context) {
            return mToolItemView;
        }

        this.context = context;
        if (mToolItemView == null) {
            ImageView imageView = new ImageView(context);
            int size = Util.getPixelByDp(context, 40);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(size, size);
            imageView.setLayoutParams(params);
            imageView.setImageResource(R.drawable.image);
            imageView.bringToFront();
            mToolItemView = imageView;
        }

        return mToolItemView;
    }

    @Override
    public void onSelectionChanged(int selStart, int selEnd) {
        return;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (ARE_Style_Image.REQUEST_CODE == requestCode) {
                ARE_Style_Image imageStyle = (ARE_Style_Image) getStyle();
                Uri selectedImageUri = data.getData();

                ImageStrategy imageStrategy = this.getEditText().getImageStrategy();
                if (imageStrategy != null) {
                    imageStrategy.uploadAndInsertImage(selectedImageUri, imageStyle);
                    return;
                }

//                imageStyle.insertImage(selectedImageUri, AreImageSpan.ImageType.URI);

                storeArticleImage(RealPathUtil.getRealPath(context, selectedImageUri), imageStyle);


            }
        }
    }

    private void storeArticleImage(final String uri, final ARE_Style_Image imageStyle) {

        String userId = new ArePreferences(context).getUserId();


        Log.e("UserId ", userId);

        ApiCall.getInstance(context).addArticleImage((Activity) context, RequestBody.create(MediaType.parse("multipart/form-data"), userId), uri, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {

                try {

                    JsonObject response = (JsonObject) data;

                    String url = response.get("text").getAsString();

                    Log.e("url ", url);

                    imageStyle.insertImage(url, AreImageSpan.ImageType.URL);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {
                Log.e("UserId ", errorMessage);
                Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
            }
        }, true);


    }


}
