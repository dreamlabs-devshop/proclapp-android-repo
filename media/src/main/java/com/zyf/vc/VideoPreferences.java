package com.zyf.vc;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Admin on 06-11-2015.
 */
public class VideoPreferences {
    private static final String
            PREF_NAME = "pro",
            TOKEN = "TOKEN",
            USERID = "USERID";
    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    Context context;
    int PRIVATE_MODE = 0;

    public VideoPreferences(Context context) {
        this.context = context;
        prefs = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
    }

    public void clearPrefs() {
        editor = prefs.edit();
        editor.clear();
        editor.apply();
    }


    public void storeToken(String fcmToken) {
        editor = prefs.edit();
        editor.putString(TOKEN, fcmToken);
        editor.apply();
    }

    public String getToken() {
        return prefs.getString(TOKEN, "");
    }

    public void storeUserId(String userId) {
        editor = prefs.edit();
        editor.putString(USERID, userId);
        editor.apply();
    }

    public String getUserId() {
        return prefs.getString(USERID, "");
    }



}
