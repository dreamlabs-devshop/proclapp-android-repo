package com.zyf.vc.retrofit;


import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Url;

public interface APIService {


    @Multipart
    @POST("add_VV_post")
    Call<JsonObject> addVVPost(
            @PartMap HashMap<String, RequestBody> request,
            @Part MultipartBody.Part file);

    @Multipart
    @POST("expert_reply")
    Call<JsonObject> expertReply(@PartMap HashMap<String, RequestBody> request,
                                 @Part MultipartBody.Part voiceVideoReply);

}




