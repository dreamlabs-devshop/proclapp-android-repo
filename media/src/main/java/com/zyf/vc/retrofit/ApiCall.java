package com.zyf.vc.retrofit;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;

import androidx.annotation.NonNull;

import com.google.gson.JsonObject;
import com.zyf.vc.R;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ApiCall {
    //Do Not Change error code otherwise you will be in trouble
    public static final int NETWORK_ERROR = 9001;
    public static final int NO_CONNECTION_ERROR = 9005;
    public static final int TIME_OUT_ERROR = 9006;
    private static APIService service;
    private ProgressDialog progressDialog;

    public static ApiCall getInstance(Context context) {
        if (service == null) {
            service = RestClient.getClient(context);
        }
        return new ApiCall();
    }


    /**
     * Get error message from error code
     *
     * @param activity  calling activity
     * @param errorCode
     * @param activity
     */
    private static String getErrorMessage(Activity activity, int errorCode) {
        switch (errorCode) {
            case NETWORK_ERROR:
                return activity.getString(R.string.network_error_occurred);
            case NO_CONNECTION_ERROR:
                return activity.getString(R.string.nonetwork);
            case TIME_OUT_ERROR:
                return activity.getString(R.string.timeout_error);
            default:
                return activity.getString(R.string.somethingWentWrong);
        }
    }


    /**
     * add Voice and Video Post
     *
     * @param activity
     * @param request
     * @param filePath
     * @param iApiCallback
     * @param showDialog
     */
    public void addVVPost(final Activity activity, HashMap<String, RequestBody> request, String filePath, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.addVVPost(request, getMultipartBody(filePath));
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }

                            iApiCallback.onSuccess(response_msg);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, "Server error , Please try after some time");
                    return;
                }


                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }

    public void expertReply(final Activity activity, HashMap<String, RequestBody> request, String pathVoiceVideo, final IApiCallback iApiCallback, final boolean showDialog) {
        if (showDialog) {
            showProgressDialog(activity);
        }
        Call<JsonObject> call = service.expertReply(request, getMultipartBody(pathVoiceVideo));
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (activity.isFinishing())
                    return;
                try {

                    if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (response.isSuccessful()) {
                        JsonObject resObject = getResponse(response.body(), activity);
                        if (resObject != null && resObject.has("status") && resObject.get("status").getAsString().equals("true")) {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            //Object type String
                            iApiCallback.onSuccess(response_msg);
                        } else {
                            String response_msg = "";
                            if (resObject.has("response_msg")) {
                                response_msg = resObject.get("response_msg").getAsString();
                            }
                            iApiCallback.onFailure(true, response_msg);
                        }
                    } else {
                        iApiCallback.onFailure(false, getErrorMessage(activity, response.code()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (showDialog && progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (t instanceof IOException) {
                    iApiCallback.onFailure(false, "Faild to connect server , Please try after some time");
                    return;
                }

                iApiCallback.onFailure(false, t.getMessage());
            }
        });
    }


    /**
     * show a progress dialog
     *
     * @param activity
     */
    private void showProgressDialog(Activity activity) {
        if (activity.isFinishing())
            return;

        progressDialog = new ProgressDialog(activity);
//        progressDialog.setIndeterminateDrawable(ContextCompat.getDrawable(activity, R.drawable.progress_drawable));
        progressDialog.setMessage(activity.getResources().getString(R.string.please_wait));
        progressDialog.setCancelable(true);
        progressDialog.setCanceledOnTouchOutside(false);

        progressDialog.show();
    }

    /**
     * get json object with response
     *
     * @param object
     * @return
     */
    private JsonObject getResponse(JsonObject object, final Activity activity) {


        try {
            return object.get("response").getAsJsonArray().get(0).getAsJsonObject();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }


    /**
     * returns a multipart body for given file and key
     *
     * @param filePath path of file
     * @return
     */
    private MultipartBody.Part getMultipartBody(String filePath) {
        MultipartBody.Part bodyImage = null;

        if (filePath != null && !filePath.isEmpty()) {
            File file = new File(filePath);
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            bodyImage = MultipartBody.Part.createFormData("file", file.getName(), requestFile);
        }
        return bodyImage;
    }


}
