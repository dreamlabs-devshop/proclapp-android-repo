package com.zyf.vc.ui;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.snackbar.Snackbar;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.zyf.vc.IVideoRecorder;
import com.zyf.vc.R;
import com.zyf.vc.VideoPreferences;
import com.zyf.vc.module.VideoObject;
import com.zyf.vc.retrofit.ApiCall;
import com.zyf.vc.retrofit.IApiCallback;
import com.zyf.vc.utils.FileUtil;
import com.zyf.vc.utils.MediaUtil;
import com.zyf.vc.view.CameraPreview;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.RequestBody;

/**
 * Created by zhangyf on 2016/7/17.
 */
public class RecorderActivity extends AppCompatActivity implements IVideoRecorder {


    private static final String TAG = "RecorderActivity";
    private static final int PLAY_VIDEO_REQUEST_CODE = 100;
    public static boolean isVideoRecord = false;
    private static int MAX_TIME = 300;
    Chronometer chronometer;

    private Camera mCamera;
    private MediaRecorder mMediaRecorder;
    private CameraPreview mPreview;
    private boolean isRecording = false;
    //    private ImageView button_start;
    private ImageView change_camera;
    private ImageView open_light;
    private String recordPath;
    private ImageView
            iv_start,
            iv_pause,
            iv_stop;
    private boolean isPaused = false;
    private long timeWhenPause = 0;

    private String
            categoryId = "",
            expertId = "",
            postDescription;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recorder);

        categoryId = getIntent().getStringExtra("categoryId");
        expertId = getIntent().getStringExtra("expertId");
        postDescription = getIntent().getStringExtra("description");

        Log.e("Tag ", new VideoPreferences(RecorderActivity.this).getToken());

        checkPermission();

        mPreview = findViewById(R.id.camera_preview);
//        button_start = findViewById(R.id.button_start);
        change_camera = findViewById(R.id.change_camera);
        open_light = findViewById(R.id.open_light);
        chronometer = findViewById(R.id.chronometer);
        iv_start = findViewById(R.id.iv_start);
        iv_stop = findViewById(R.id.iv_stop);
        iv_pause = findViewById(R.id.iv_pause);

        open_light.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPreview.toggleFlashMode();
                if (mPreview.getLightState() == 0) {
                    open_light.setImageResource(R.mipmap.icon_light_off);
                } else {
                    open_light.setImageResource(R.mipmap.icon_light_on);
                }
            }
        });

        change_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPreview.getCameraState() == 0) {
                    mPreview.changeToFront();
                } else {
                    mPreview.changeToBack();
                }
            }
        });


        iv_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (checkPermission()) {

                    if (isRecording) {
                        stopRecord();

                    } else {
                        startRecord();

                    }
                }

            }
        });

        iv_pause.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View view) {
                pauseRecording(isPaused);
            }
        });

        iv_stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stopRecord();
            }
        });

    }

    public VideoObject startRecord() {
        if (isRecording) {
            return null;
        }

        // initialize video camera
        if (prepareVideoRecorder()) {
            change_camera.setVisibility(View.GONE);

            iv_start.setVisibility(View.GONE);
            iv_pause.setVisibility(View.VISIBLE);
            // Camera is available and unlocked, MediaRecorder is prepared,
            // now you can start recording
            mMediaRecorder.start();

            chronometer.setBase(SystemClock.elapsedRealtime());
            chronometer.start();

            isRecording = true;
        } else {
            // prepare didn't work, release the camera
            releaseMediaRecorder();
            // inform user
        }
        return null;
    }

    public void stopRecord() {
        if (isRecording) {
            // stop recording and release camera
            try {
                //change_camera.setEnabled(true);
                change_camera.setVisibility(View.VISIBLE);

                mMediaRecorder.setOnErrorListener(null);
                mMediaRecorder.setPreviewDisplay(null);

                mMediaRecorder.stop();  // stop the recording

                chronometer.stop();
                chronometer.setBase(SystemClock.elapsedRealtime());
                if (getIntent().hasExtra("openfor")) {
                    addExpertReply();
                } else {
                    addVideo();
                }
            } catch (Exception e) {
                // TODO 删除已经创建的视频文件
                e.printStackTrace();
            }
            releaseMediaRecorder(); // release the MediaRecorder object
            MediaUtil.notifyGallery(this, recordPath);
            isRecording = false;


        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        // 页面销毁时要停止录制
        stopRecord();
        releaseMediaRecorder();       // if you are using MediaRecorder, release it first
        releaseCamera();              // release the camera immediately on pause event
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    /**
     * 准备视频录制器
     *
     * @return
     */
    private boolean prepareVideoRecorder() {
        if (!FileUtil.isSDCardMounted()) {
            return false;
        }

        File file = FileUtil.getOutputMediaFile(FileUtil.MEDIA_TYPE_VIDEO);
        if (null == file) {
            return false;
        }
        recordPath = file.getAbsolutePath();
//        recordPath = file.toString();
        mMediaRecorder = new MediaRecorder();
        // 获取最新对象，因为当mPreview中切换相机之后对象指向会有问题
        mCamera = mPreview.getmCamera();

        mCamera.setDisplayOrientation(90);
        mMediaRecorder.setOrientationHint(90);
        // Step 1: Unlock and set camera to MediaRecorder
        // 解锁相机以让其他进程能够访问相机，4.0以后系统自动管理调用，但是实际使用中，不调用的话，MediaRecorder.start()报错闪退
        mCamera.unlock();
        mMediaRecorder.setCamera(mCamera);

        // Step 2: Set sources
        mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
        mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);

//        mMediaRecorder.setAudioEncodingBitRate(44100);

        // Step 3: Set a CamcorderProfile (requires API Level 8 or higher)
        if (CamcorderProfile.hasProfile(mPreview.getCurrentCameraId(), CamcorderProfile.QUALITY_720P)) {
            mMediaRecorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_720P));
        } else if (CamcorderProfile.hasProfile(mPreview.getCurrentCameraId(), CamcorderProfile.QUALITY_HIGH)) {
            mMediaRecorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH));
        } else if (CamcorderProfile.hasProfile(mPreview.getCurrentCameraId(), CamcorderProfile.QUALITY_QVGA)) {
            mMediaRecorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_QVGA));
        } else if (CamcorderProfile.hasProfile(mPreview.getCurrentCameraId(), CamcorderProfile.QUALITY_480P)) {
            mMediaRecorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_480P));
        }
//        mMediaRecorder.setVideoSize(mVideoSize.width, mVideoSize.height);\
//        mMediaRecorder.setCaptureRate(20);
        mMediaRecorder.setVideoEncodingBitRate(2 * 1024 * 1024);

        // Step 4: Set output file
        mMediaRecorder.setOutputFile(recordPath);

        // Step 5: Set the preview output
        mMediaRecorder.setPreviewDisplay(mPreview.getHolder().getSurface());

        mMediaRecorder.setMaxDuration(MAX_TIME * 1000);


//        if (mPreview.getCameraState() == 0) {
//        mMediaRecorder.setOrientationHint(90);
//        } else {
//            mMediaRecorder.setOrientationHint(270);
//        }

        // Step 6: Prepare configured MediaRecorder
        try {
            mMediaRecorder.prepare();
        } catch (IllegalStateException e) {
            Log.d(TAG, "IllegalStateException preparing MediaRecorder: " + e.getMessage());
            releaseMediaRecorder();
            return false;
        } catch (IOException e) {
            Log.d(TAG, "IOException preparing MediaRecorder: " + e.getMessage());
            releaseMediaRecorder();
            return false;
        }
        return true;
    }

    private void releaseMediaRecorder() {
        if (mMediaRecorder != null) {
            mMediaRecorder.reset();   // clear recorder configuration
            mMediaRecorder.release(); // release the recorder object
            mMediaRecorder = null;
            // 锁相机，4.0以后系统自动管理调用，但若录制器prepare()方法失败，必须调用
            mCamera.lock();           // lock camera for later use
        }
    }

    private void releaseCamera() {
        mCamera = mPreview.getmCamera();
        if (mCamera != null) {
            // 虽然我之前并没有setPreviewCallback，但不加这句的话，
            // 后面要用到Camera时，调用Camera随便一个方法，都会报
            // Method called after release() error闪退，推测可能
            // Camera内存泄露无法真正释放，加上这句可以规避该问题
            mCamera.setPreviewCallback(null);
            // 释放前先停止预览
            mCamera.stopPreview();
            mCamera.release();        // release the camera for other applications
            mCamera = null;
        }
    }

    /**
     * 返回true 表示可以使用  返回false表示不可以使用
     */
    public boolean cameraIsCanUse() {
        boolean isCanUse = true;
        Camera mCamera = null;
        try {
            mCamera = Camera.open();
            Camera.Parameters mParameters = mCamera.getParameters(); //针对魅族手机
            mCamera.setParameters(mParameters);
        } catch (Exception e) {
            isCanUse = false;
        }

        if (mCamera != null) {
            try {
                mCamera.release();
            } catch (Exception e) {
                e.printStackTrace();
                return isCanUse;
            }
        }
        return isCanUse;
    }

    private boolean checkPermission() {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= android.os.Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(RecorderActivity.this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED
                    && ContextCompat.checkSelfPermission(RecorderActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                    && ContextCompat.checkSelfPermission(RecorderActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(RecorderActivity.this, Manifest.permission.RECORD_AUDIO)
                        && ActivityCompat.shouldShowRequestPermissionRationale(RecorderActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        && ActivityCompat.shouldShowRequestPermissionRationale(RecorderActivity.this, Manifest.permission.CAMERA)) {
                    ActivityCompat.requestPermissions(RecorderActivity.this, new String[]{Manifest.permission.RECORD_AUDIO, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, 1234);
                } else {
                    ActivityCompat.requestPermissions(RecorderActivity.this, new String[]{Manifest.permission.RECORD_AUDIO, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, 1234);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void pauseRecording(boolean pause) {

        if (!pause) {
            //pause recording
            iv_pause.setImageDrawable(getResources().getDrawable(R.drawable.play));
            timeWhenPause = chronometer.getBase() - SystemClock.elapsedRealtime();
            chronometer.stop();
            mMediaRecorder.pause();

        } else {
            //resume recording
            iv_pause.setImageDrawable(getResources().getDrawable(R.drawable.pause_btn));
            chronometer.setBase(SystemClock.elapsedRealtime() + timeWhenPause);
            chronometer.start();
            mMediaRecorder.resume();

        }

        isPaused = !pause;

    }

    private void addVideo() {

        Log.e("Duration timeWhenPause ", chronometer.getBase() + "");

        MediaPlayer mp = MediaPlayer.create(this, Uri.parse(recordPath));
        int duration = mp.getDuration();
        mp.release();

        String hour, min, sec;

        if (((int) TimeUnit.MILLISECONDS.toHours(duration)) < 10) {
            hour = "0" + TimeUnit.MILLISECONDS.toHours(duration);
        } else {
            hour = "" + TimeUnit.MILLISECONDS.toHours(duration);
        }

        if (((int) TimeUnit.MILLISECONDS.toMinutes(duration)) < 10) {
            min = "0" + TimeUnit.MILLISECONDS.toMinutes(duration);
        } else {
            min = "" + TimeUnit.MILLISECONDS.toMinutes(duration);
        }

        if (((int) TimeUnit.MILLISECONDS.toMinutes(duration)) < 10) {
            sec = "0" + (TimeUnit.MILLISECONDS.toSeconds(duration) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(duration)));
        } else {
            sec = "" + (TimeUnit.MILLISECONDS.toSeconds(duration) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(duration)));
        }

        String time;
        if (((int) TimeUnit.MILLISECONDS.toHours(duration)) > 0) {
            time = hour + ":" + min + ":" + sec;
        } else {
            time = min + ":" + sec;
        }


        Log.e("Duration ", time);

        String userId = new VideoPreferences(RecorderActivity.this).getUserId();

        HashMap<String, RequestBody> request = new HashMap<>();
        request.put("user_id", RequestBody.create(MediaType.parse("multipart/form-data"), userId));
        request.put("vv_type", RequestBody.create(MediaType.parse("multipart/form-data"), "2"));
        request.put("post_category", RequestBody.create(MediaType.parse("multipart/form-data"), categoryId));
        request.put("expert", RequestBody.create(MediaType.parse("multipart/form-data"), expertId));
        request.put("post_description", RequestBody.create(MediaType.parse("multipart/form-data"), postDescription));
        request.put("post_type", RequestBody.create(MediaType.parse("multipart/form-data"), "3"));

        ApiCall.getInstance(RecorderActivity.this).addVVPost(RecorderActivity.this, request, recordPath, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {

                isVideoRecord = true;

                Toast.makeText(RecorderActivity.this, "Video uploded successfully.", Toast.LENGTH_LONG).show();

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        onBackPressed();
                    }
                }, 1000);


            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {

                snackBarShow(errorMessage);

            }
        }, true);

    }

    private void addExpertReply() {

        HashMap<String, RequestBody> request = new HashMap<>();
        request.put("user_id", RequestBody.create(MediaType.parse("multipart/form-data"), new VideoPreferences(RecorderActivity.this).getUserId()));
        request.put("post_id", RequestBody.create(MediaType.parse("multipart/form-data"), getIntent().getStringExtra("postId")));
        request.put("requested_user_id", RequestBody.create(MediaType.parse("multipart/form-data"), getIntent().getStringExtra("requestedUserId")));
        request.put("vv_type", RequestBody.create(MediaType.parse("multipart/form-data"), "2"));
        request.put("type", RequestBody.create(MediaType.parse("multipart/form-data"), "3"));


        ApiCall.getInstance(this).expertReply(this, request, recordPath, new IApiCallback() {
            @Override
            public void onSuccess(Object data) {
                isVideoRecord = true;
                Toast.makeText(RecorderActivity.this, "Replied successfully.", Toast.LENGTH_LONG).show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        onBackPressed();
                    }
                }, 1000);


            }

            @Override
            public void onFailure(boolean isFalseFromService, String errorMessage) {

                snackBarShow(errorMessage);

            }
        }, true);

    }


    public void snackBarShow(String text) {
        Snackbar snackbar = Snackbar.make(RecorderActivity.this.findViewById(android.R.id.content), text, Snackbar.LENGTH_LONG);
        //snackbar.setActionTextColor(R.color.profileDivider);
        View view = snackbar.getView();
        view.setBackgroundColor(ContextCompat.getColor(RecorderActivity.this, R.color.colorPrimary));
        TextView textView = (TextView) view.findViewById(R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(RecorderActivity.this, android.R.color.white));
        /*FrameLayout.LayoutParams params =(FrameLayout.LayoutParams)view.getLayoutParams();
        params.gravity = Gravity.TOP;
        view.setLayoutParams(params);*/
        snackbar.show();
    }
}
